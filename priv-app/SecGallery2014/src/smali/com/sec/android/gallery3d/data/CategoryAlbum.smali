.class public Lcom/sec/android/gallery3d/data/CategoryAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "CategoryAlbum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryComparator;,
        Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;
    }
.end annotation


# static fields
.field private static final INVALID_TYPE:I = -0x1

.field private static final TAG:Ljava/lang/String; = "CategoryAlbum"

.field public static final TOP_PATH:Ljava/lang/String; = "/local/categoryalbum"

.field public static final UPDATE_URI:Landroid/net/Uri;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mBaseUri:Landroid/net/Uri;

.field private final mCateogryType:Ljava/lang/String;

.field private final mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mSortType:I

.field private mWatchUri:[Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "category://categoryupdate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->UPDATE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cateogryType"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/android/gallery3d/data/CategoryAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    .line 45
    sget-object v0, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_IMAGE_TABLE_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mBaseUri:Landroid/net/Uri;

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mBaseUri:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/gallery3d/data/CategoryAlbum;->UPDATE_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mWatchUri:[Landroid/net/Uri;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mSortType:I

    .line 57
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 58
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mCateogryType:Ljava/lang/String;

    .line 59
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mWatchUri:[Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 61
    return-void
.end method

.method private getSubList(Ljava/util/ArrayList;II)Ljava/util/ArrayList;
    .locals 3
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;",
            ">;II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v2, p3, :cond_1

    .line 237
    :cond_0
    return-object v1

    .line 233
    :cond_1
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 234
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;

    iget-object v2, v2, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public delete()V
    .locals 0

    .prologue
    .line 190
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 192
    return-void
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mCateogryType:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 12
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    if-nez v8, :cond_1

    .line 100
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 130
    :cond_0
    :goto_0
    return-object v5

    .line 101
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 102
    .local v3, "existingCount":I
    add-int/lit8 v8, v3, 0x1

    if-lt p1, v8, :cond_2

    .line 103
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 106
    :cond_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v8}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v6

    .line 107
    .local v6, "sortType":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    monitor-enter v9

    .line 108
    :try_start_0
    iget v8, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mSortType:I

    if-eq v8, v6, :cond_3

    .line 109
    iput v6, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mSortType:I

    .line 110
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryComparator;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v11}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryComparator;-><init>(Landroid/content/Context;)V

    invoke-static {v8, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 112
    :cond_3
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    add-int v8, p1, p2

    invoke-static {v8, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 115
    .local v2, "end":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    invoke-direct {p0, v8, p1, v2}, Lcom/sec/android/gallery3d/data/CategoryAlbum;->getSubList(Ljava/util/ArrayList;II)Ljava/util/ArrayList;

    move-result-object v7

    .line 117
    .local v7, "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    sub-int v8, v2, p1

    new-array v0, v8, [Lcom/sec/android/gallery3d/data/MediaItem;

    .line 118
    .local v0, "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v1, Lcom/sec/android/gallery3d/data/CategoryAlbum$1;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/gallery3d/data/CategoryAlbum$1;-><init>(Lcom/sec/android/gallery3d/data/CategoryAlbum;[Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 125
    .local v1, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v8}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v1, v9}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 126
    new-instance v5, Ljava/util/ArrayList;

    sub-int v8, v2, p1

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 127
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v8, v0

    if-ge v4, v8, :cond_0

    .line 128
    aget-object v8, v0, v4

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 112
    .end local v0    # "buf":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v1    # "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .end local v2    # "end":I
    .end local v4    # "i":I
    .end local v5    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v7    # "subset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 138
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 206
    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mCateogryType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 207
    .local v0, "stringID":I
    if-lez v0, :cond_0

    .line 208
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 184
    const-wide/16 v0, 0x405

    .line 185
    .local v0, "operation":J
    return-wide v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 3

    .prologue
    .line 220
    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_TYPE_MAP:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mCateogryType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/TabTagType;

    .line 221
    .local v0, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    if-eqz v0, :cond_0

    .line 224
    .end local v0    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :goto_0
    return-object v0

    .restart local v0    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    goto :goto_0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x1

    return v0
.end method

.method public loadItems()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;>;"
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 145
    const/4 v6, 0x0

    .line 146
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 147
    .local v8, "imageId":I
    const-string v10, ""

    .line 148
    .local v10, "itemUri":Ljava/lang/String;
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 149
    .local v7, "date":Ljava/lang/Long;
    const/4 v12, 0x0

    .line 151
    .local v12, "path":Lcom/sec/android/gallery3d/data/Path;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mBaseUri:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "uri"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "date"

    aput-object v4, v2, v3

    const-string v3, "scene_type =? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v13, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mCateogryType:Ljava/lang/String;

    aput-object v13, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 155
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 158
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 159
    const-string v0, "/"

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v12

    .line 161
    new-instance v9, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;

    invoke-direct {v9, v12, v7}, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/Long;)V

    .line 162
    .local v9, "item":Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 167
    .end local v9    # "item":Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;
    :cond_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 169
    return-object v11

    .line 167
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/sec/android/gallery3d/data/CategoryAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mDataVersion:J

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/CategoryAlbum;->loadItems()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mItems:Ljava/util/ArrayList;

    .line 177
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mSortType:I

    .line 179
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum;->mDataVersion:J

    return-wide v0
.end method
