.class public Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
.super Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;
.source "TileImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/TileImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Tile"
.end annotation


# instance fields
.field mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

.field public mDecodedTile:Landroid/graphics/Bitmap;

.field public mNext:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

.field public mTileLevel:I

.field public volatile mTileState:I

.field public mX:I

.field public mY:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/TileImageView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/TileImageView;III)V
    .locals 5
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "level"    # I

    .prologue
    .line 825
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;-><init>()V

    .line 820
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 826
    iput p2, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    .line 827
    iput p3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    .line 828
    iput p4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    .line 829
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$500(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_0

    .line 830
    new-instance v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$600(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->getAlphablendingAnimationInterface()Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x43960000    # 300.0f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;-><init>(Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;FFF)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    .line 832
    :cond_0
    return-void
.end method


# virtual methods
.method decode()Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    .line 843
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v4, :cond_1

    .line 845
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v4

    add-int/lit8 v2, v4, 0x2

    .line 846
    .local v2, "tileLength":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    shl-int v0, v3, v4

    .line 850
    .local v0, "borderLength":I
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$500(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    sub-int/2addr v6, v0

    iget v7, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    sub-int/2addr v7, v0

    invoke-interface {v4, v5, v6, v7, v2}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getTile(IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/DecodeUtils;->ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 867
    .end local v0    # "borderLength":I
    .end local v2    # "tileLength":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    :goto_1
    return v3

    .line 852
    .restart local v0    # "borderLength":I
    .restart local v2    # "tileLength":I
    :catch_0
    move-exception v1

    .line 853
    .local v1, "t":Ljava/lang/Throwable;
    const-string v4, "TileImageView"

    const-string v5, "fail to decode tile"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 860
    .end local v0    # "borderLength":I
    .end local v1    # "t":Ljava/lang/Throwable;
    .end local v2    # "tileLength":I
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$500(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v8

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getTile(IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/DecodeUtils;->ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 862
    :catch_1
    move-exception v1

    .line 863
    .restart local v1    # "t":Ljava/lang/Throwable;
    const-string v4, "TileImageView"

    const-string v5, "fail to decode tile"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 867
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getParentTile()Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    .locals 5

    .prologue
    .line 922
    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v4, v4, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    if-ne v3, v4, :cond_0

    const/4 v3, 0x0

    .line 926
    :goto_0
    return-object v3

    .line 923
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v3

    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    add-int/lit8 v4, v4, 0x1

    shl-int v0, v3, v4

    .line 924
    .local v0, "size":I
    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    div-int/2addr v3, v0

    mul-int v1, v0, v3

    .line 925
    .local v1, "x":I
    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    div-int/2addr v3, v0

    mul-int v2, v0, v3

    .line 926
    .local v2, "y":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    add-int/lit8 v4, v4, 0x1

    # invokes: Lcom/sec/android/gallery3d/ui/TileImageView;->getTile(III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    invoke-static {v3, v1, v2, v4}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$800(Lcom/sec/android/gallery3d/ui/TileImageView;III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v3

    goto :goto_0
.end method

.method public getTextureHeight()I
    .locals 1

    .prologue
    .line 907
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_1

    .line 908
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 910
    :goto_0
    return v0

    :cond_1
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v0

    goto :goto_0
.end method

.method public getTextureWidth()I
    .locals 1

    .prologue
    .line 898
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_1

    .line 899
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 901
    :goto_0
    return v0

    :cond_1
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v0

    goto :goto_0
.end method

.method protected onFreeBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 838
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$600(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BitmapReuseFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 840
    return-void
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 873
    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 874
    const-string v4, "TileImageView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mTileState (value:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") is NOT State_DECODED."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 889
    :goto_0
    return-object v0

    .line 879
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v4, v4, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    shr-int v2, v4, v5

    .line 880
    .local v2, "rightEdge":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v4, v4, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    shr-int v1, v4, v5

    .line 881
    .local v1, "bottomEdge":I
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v4, :cond_1

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v4, :cond_2

    .line 882
    :cond_1
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->setSize(II)V

    .line 886
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 887
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 888
    const/4 v3, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    goto :goto_0

    .line 884
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v4

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v5

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->setSize(II)V

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 931
    const-string/jumbo v0, "tile(%s, %s, %s / %s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$700()I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$900(Lcom/sec/android/gallery3d/ui/TileImageView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    iget v3, v3, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(III)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I

    .prologue
    .line 915
    iput p1, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    .line 916
    iput p2, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    .line 917
    iput p3, p0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    .line 918
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->invalidateContent()V

    .line 919
    return-void
.end method
