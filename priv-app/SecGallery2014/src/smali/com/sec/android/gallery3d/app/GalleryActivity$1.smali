.class Lcom/sec/android/gallery3d/app/GalleryActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "GalleryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/GalleryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 390
    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "storage removed."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v3, :cond_1

    .line 392
    const/4 v1, 0x0

    .line 393
    .local v1, "storage":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 394
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 395
    .local v2, "uri":Landroid/net/Uri;
    if-eqz v2, :cond_0

    .line 396
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "//"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "splitStrings":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-le v3, v5, :cond_0

    .line 398
    aget-object v1, v0, v5

    .line 402
    .end local v0    # "splitStrings":[Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    if-eqz v1, :cond_1

    .line 403
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->deleteSDcardItems(Ljava/lang/String;)I

    .line 406
    .end local v1    # "storage":Ljava/lang/String;
    :cond_1
    return-void
.end method
