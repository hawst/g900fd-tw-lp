.class Lcom/sec/android/gallery3d/ui/SideMirrorView$1;
.super Lcom/sec/android/gallery3d/ui/SlotView$SimpleListener;
.source "SideMirrorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/SideMirrorView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;IIIIIIIIILcom/sec/android/gallery3d/util/KeyBoardManager;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/SideMirrorView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/SideMirrorView;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;->this$0:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SlotView$SimpleListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;->this$0:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    # getter for: Lcom/sec/android/gallery3d/ui/SideMirrorView;->mHelpMode:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->access$000(Lcom/sec/android/gallery3d/ui/SideMirrorView;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;->this$0:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    # invokes: Lcom/sec/android/gallery3d/ui/SideMirrorView;->onDown(I)V
    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->access$100(Lcom/sec/android/gallery3d/ui/SideMirrorView;I)V

    .line 111
    :cond_0
    return-void
.end method

.method public onLongTap(I)V
    .locals 0
    .param p1, "slotIndex"    # I

    .prologue
    .line 127
    return-void
.end method

.method public onSingleTapUp(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;->this$0:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    # invokes: Lcom/sec/android/gallery3d/ui/SideMirrorView;->onSingleTapUp(I)V
    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->access$300(Lcom/sec/android/gallery3d/ui/SideMirrorView;I)V

    .line 123
    return-void
.end method

.method public onUp(Z)V
    .locals 2
    .param p1, "followedByLongPress"    # Z

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;->this$0:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    # getter for: Lcom/sec/android/gallery3d/ui/SideMirrorView;->mHelpMode:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->access$000(Lcom/sec/android/gallery3d/ui/SideMirrorView;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;->this$0:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    # invokes: Lcom/sec/android/gallery3d/ui/SideMirrorView;->onUp(Z)V
    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->access$200(Lcom/sec/android/gallery3d/ui/SideMirrorView;Z)V

    .line 118
    :cond_0
    return-void
.end method
