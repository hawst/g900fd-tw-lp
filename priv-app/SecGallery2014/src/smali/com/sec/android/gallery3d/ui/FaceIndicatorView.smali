.class public Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "FaceIndicatorView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;,
        Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;,
        Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;,
        Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;,
        Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceState;
    }
.end annotation


# static fields
.field private static final BASE_CHARACTER:Ljava/lang/String; = "a"

.field private static final COLOR_FACE_OUTLINE_CONFIRMED:I = -0x10000

.field private static final COLOR_FACE_OUTLINE_SELECTED:I = -0xd03901

.field private static final COLOR_FACE_POPUP_ITEM_LINE:I

.field private static final COLOR_FACE_POPUP_ITEM_TEXT:I

.field private static final COLOR_FACE_POPUP_ITEM_TEXT_RECOMMEND:I = -0xff3f01

.field private static final COLOR_FACE_TAGGING_CONFIRMED_NAME:I = -0xecceab

.field public static final FACEBOOK_ID:Ljava/lang/String; = "facebook"

.field private static final HIDE_ANIMATION_DURATION:I = 0x12c

.field private static final INVALID_ID:I = -0x2

.field private static final KEY_CONTACT_POPUP_HEAD_POS:Ljava/lang/String; = "head_pos"

.field private static final KEY_CONTACT_POPUP_POS:Ljava/lang/String; = "contact_popup_pos"

.field private static final KEY_SHOW_CONTACT_POPUP:Ljava/lang/String; = "show"

.field private static final MAX_NAME_TEXT_LENGTH:I = 0x10

.field private static final MENU_FIRST_ITEM:I = -0x1

.field private static final MENU_LAST_ITEM:I = 0x1

.field private static final ME_NAME:Ljava/lang/String; = "profile/Me"

.field private static final OUTLINE_WIDTH:F = 2.0f

.field private static final REFRESH_CONTACT_POPUP_MSG:I = 0x1

.field private static final RELAYOUT_CONTACT_POPUP_MSG:I = 0x0

.field private static final TABLET_STATUSBAR_HEIGHT:I = 0x30

.field private static final TAG:Ljava/lang/String; = "FaceIndicatorView"

.field private static final TAG_POSITION_DOWN:I = 0x0

.field private static final TAG_POSITION_LEFT:I = 0x1

.field private static final TAG_POSITION_RIGHT:I = 0x0

.field private static final TAG_POSITION_UP:I = 0x10

.field private static final TEST_DRAW:Z = false

.field public static final TYPE_CHANNEL_CHATON:I = 0x3

.field public static final TYPE_CHANNEL_EMAIL:I = 0x1

.field public static final TYPE_CHANNEL_MMS:I = 0x2

.field public static final TYPE_CHANNEL_NONE:I = 0x0

.field private static final UPDATE_FACE_DATA:I = 0x2


# instance fields
.field private final FACE_POPUP_MENU_WIDTH:I

.field private final FACE_POPUP_TEXT_SIZE:I

.field private final FACE_TAG_NAME_TEXT_SIZE:I

.field private final HEIGHT_FACE_POPUP_ITEM_TEXT:I

.field private final LEFT_ALIGN_FACE_POPUP_ITEM_TEXT:I

.field private isResume:Z

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAddContactsDialog:Landroid/app/AlertDialog;

.field private mAddTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field private mAssignedName:Ljava/lang/String;

.field private mBindFaceBookService:Z

.field private mChannelDialog:Landroid/app/AlertDialog;

.field private mConfirmDialog:Landroid/app/AlertDialog;

.field mContactDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/access/contact/ContactData;",
            ">;"
        }
    .end annotation
.end field

.field private mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

.field private mContactPopupHeight:I

.field private mContactPopupVisible:Z

.field private mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

.field private mContainer:Landroid/view/ViewGroup;

.field private mContext:Landroid/content/Context;

.field private mCurContactLookupKey:Ljava/lang/String;

.field private mCurFace:Lcom/sec/android/gallery3d/data/Face;

.field private mCurFaceId:I

.field private mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mCurMediaSetKey:Ljava/lang/String;

.field private mCurMediaSetName:Ljava/lang/String;

.field private final mDelFaceId:I

.field private mFaceIndexShowContact:I

.field private mFaceInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

.field private mFaceTagToggle:Z

.field private mFaces:[Lcom/sec/android/gallery3d/data/Face;

.field private mFilmStripHeight:I

.field private mFilmStripStatus:Z

.field private final mFocusFaceTag:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFocusItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mGenericFocusIndex:I

.field private mGenericFocusItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mGenericFocusOnFaceId:I

.field private final mHandler:Landroid/os/Handler;

.field private mHeadPos:[I

.field private mImageBounds:Landroid/graphics/Rect;

.field private mIsChangedFromContact:Z

.field private mIsManualFDResumed:Z

.field private final mIsTablet:Z

.field private mLanguage:Ljava/lang/String;

.field private mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

.field private final mLock:Ljava/lang/Object;

.field private mLookupKeyMe:Ljava/lang/String;

.field private mMainConfirmedDL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mMainConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mMainConfirmedUL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mMainConfirmedUR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mMainFaceModified:Z

.field private final mMarkTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field private final mMarkTextureForES:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field private mMaxPopItemWidth:I

.field private mMePersonId:I

.field private mMediaSetKey:Ljava/lang/String;

.field private mMediaWidth:I

.field private mMoreDialog:Landroid/app/AlertDialog;

.field private mNameMenuPopupVisible:Z

.field private mNoRecommendBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mPopupItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

.field private mPressedItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mPressedItemIndex:I

.field private mRecommendedBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mRectScale:Landroid/graphics/RectF;

.field private mRedrawContactPopup:Z

.field private final mRes:Landroid/content/res/Resources;

.field private mResourceHeight:I

.field private mResourceWidth:I

.field private mSavedFaceNum:I

.field private mScale:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSetAsCallIdDialog:Landroid/app/AlertDialog;

.field private mSubConfirmedDL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mSubConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mSubConfirmedUL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mSubConfirmedUR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private final mUnnamedFaceIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    const v0, -0x50506

    :goto_0
    sput v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_TEXT:I

    .line 83
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_1

    const v0, -0x313132

    :goto_1
    sput v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_LINE:I

    return-void

    .line 81
    :cond_0
    const v0, -0x20203

    goto :goto_0

    .line 83
    :cond_1
    const v0, -0xb9b9ba

    goto :goto_1
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 7
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const v6, -0xecceab

    const/4 v5, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 271
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 131
    const/4 v1, 0x3

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    .line 163
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContainer:Landroid/view/ViewGroup;

    .line 164
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mVibrator:Landroid/os/Vibrator;

    .line 170
    new-instance v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$1;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    .line 257
    const/4 v1, -0x2

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMePersonId:I

    .line 261
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    .line 262
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    .line 263
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mConfirmDialog:Landroid/app/AlertDialog;

    .line 264
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAddContactsDialog:Landroid/app/AlertDialog;

    .line 265
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mChannelDialog:Landroid/app/AlertDialog;

    .line 268
    iput v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusOnFaceId:I

    .line 837
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    .line 838
    iput v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSavedFaceNum:I

    .line 957
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;

    .line 1061
    iput v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaWidth:I

    .line 1087
    iput v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    .line 1088
    iput v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceWidth:I

    iput v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceHeight:I

    .line 1297
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    .line 1563
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 1564
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    .line 1565
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRedrawContactPopup:Z

    .line 1863
    iput v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    .line 2279
    iput v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 2280
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    .line 2367
    iput v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mDelFaceId:I

    .line 2757
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripStatus:Z

    .line 2803
    iput v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    .line 2804
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    .line 2843
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isResume:Z

    .line 2844
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mBindFaceBookService:Z

    .line 2895
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAssignedName:Ljava/lang/String;

    .line 2896
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaSetKey:Ljava/lang/String;

    .line 2906
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    .line 2907
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetName:Ljava/lang/String;

    .line 2914
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainFaceModified:Z

    .line 2915
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsChangedFromContact:Z

    .line 3115
    iput v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    .line 3309
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScale:F

    .line 272
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 273
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    .line 274
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsTablet:Z

    .line 275
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z

    .line 276
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    .line 278
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f0d03c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    .line 279
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f0d03c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->LEFT_ALIGN_FACE_POPUP_ITEM_TEXT:I

    .line 281
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f0d03d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_TEXT_SIZE:I

    .line 282
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f0d03da

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_TAG_NAME_TEXT_SIZE:I

    .line 283
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f0d03c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    .line 285
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getContactProvider()Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    .line 286
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    if-eqz v1, :cond_1

    .line 287
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v1

    if-nez v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    new-instance v2, Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;-><init>()V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->setSnsService(Lcom/sec/android/gallery3d/remote/sns/FacebookService;)V

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getProfileLookupKey()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    .line 292
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const-string v2, "profile/Me"

    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMePersonId:I

    .line 297
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201c3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPopupItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 300
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_2

    .line 301
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201c2

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 308
    :goto_0
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f020078

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 310
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f020431

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 313
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f02021e

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRecommendedBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 315
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f02021f

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNoRecommendBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 318
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201bb

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 320
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201bc

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedDL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 322
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201bf

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedUR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 324
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201c0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedUL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 326
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201b9

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 328
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201ba

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedDL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 330
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201bd

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedUR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 332
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201be

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedUL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 335
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00b0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_TEXT_SIZE:I

    int-to-float v2, v2

    sget v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_TEXT:I

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAddTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 338
    const-string v1, "?"

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_TAG_NAME_TEXT_SIZE:I

    int-to-float v2, v2

    invoke-static {v1, v2, v6, v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMarkTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 340
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00ce

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_TAG_NAME_TEXT_SIZE:I

    int-to-float v2, v2

    invoke-static {v1, v2, v6, v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMarkTextureForES:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 343
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    .line 345
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceWidth:I

    .line 346
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceHeight:I

    .line 348
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 349
    .local v0, "size":Landroid/graphics/Point;
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    .line 350
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    .line 355
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLanguage:Ljava/lang/String;

    .line 356
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsManualFDResumed:Z

    .line 357
    return-void

    .line 304
    .end local v0    # "size":Landroid/graphics/Point;
    :cond_2
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0201c1

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/ContactPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/PositionController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "x2"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/Face;)Lcom/sec/android/gallery3d/data/Face;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/Face;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)[Lcom/sec/android/gallery3d/data/Face;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainFaceModified:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->removeFace(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setFaceUnknown(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/graphics/RectF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;[Lcom/sec/android/gallery3d/data/Face;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "x2"    # [Lcom/sec/android/gallery3d/data/Face;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFace(Lcom/sec/android/gallery3d/data/MediaItem;[Lcom/sec/android/gallery3d/data/Face;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkNameTagOverlap(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkContactData(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method private assignName(Ljava/lang/String;IZ)I
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "faceId"    # I
    .param p3, "recommeded"    # Z

    .prologue
    const/4 v2, 0x1

    .line 2371
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v9

    .line 2375
    .local v9, "personId":I
    if-le v9, v2, :cond_1

    .line 2376
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2377
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v1, p2, v9}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setPerson(Landroid/content/Context;II)V

    .line 2378
    const/4 v6, 0x0

    .line 2379
    .local v6, "autoGroup":I
    const/4 v7, 0x0

    .line 2381
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "auto_group"

    aput-object v4, v2, v3

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2386
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2387
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 2392
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 2395
    :goto_0
    if-lez v6, :cond_1

    .line 2396
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v2, p2, v9}, Lcom/sec/samsung/gallery/access/face/FaceList;->recommendFaces(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V

    .line 2400
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "autoGroup":I
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    return v9

    .line 2389
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "autoGroup":I
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 2390
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2392
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private calculateDistance(Landroid/graphics/Rect;II)D
    .locals 4
    .param p1, "area"    # Landroid/graphics/Rect;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 2342
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    sub-int v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 2343
    .local v0, "offX":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v2, p3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2345
    .local v1, "offY":I
    mul-int v2, v0, v0

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    return-wide v2
.end method

.method private calculateSNSArea(II)Landroid/graphics/Rect;
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I

    .prologue
    .line 3197
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 3198
    .local v0, "area":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-nez v1, :cond_0

    .line 3206
    :goto_0
    return-object v0

    .line 3201
    :cond_0
    iput p1, v0, Landroid/graphics/Rect;->left:I

    .line 3202
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 3203
    iput p2, v0, Landroid/graphics/Rect;->top:I

    .line 3204
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private canShowInUpMode(I)Z
    .locals 6
    .param p1, "top"    # I

    .prologue
    .line 3186
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03cd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 3187
    .local v2, "offTopMargin":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03cf

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 3188
    .local v3, "offY":I
    sub-int v1, v2, v3

    .line 3189
    .local v1, "off":I
    add-int v4, p1, v1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceHeight:I

    sub-int p1, v4, v5

    .line 3190
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 3191
    .local v0, "actionbarHeight":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceHeight:I

    add-int/2addr v4, v0

    if-ge p1, v4, :cond_0

    .line 3192
    const/4 v4, 0x0

    .line 3193
    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;
    .locals 5
    .param p1, "faceRectOnScreen"    # Landroid/graphics/Rect;
    .param p2, "state"    # I
    .param p3, "position"    # I

    .prologue
    .line 2211
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 2212
    .local v0, "pt":Landroid/graphics/Point;
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 2213
    .local v1, "x":I
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 2214
    .local v2, "y":I
    and-int/lit8 v3, p3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_0

    .line 2215
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 2218
    :cond_0
    if-nez p2, :cond_1

    .line 2219
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v1

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    .line 2224
    :goto_0
    return-object v0

    .line 2221
    :cond_1
    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Point;->set(II)V

    goto :goto_0
.end method

.method private declared-synchronized checkContactData(Z)V
    .locals 17
    .param p1, "onlyCheckCache"    # Z

    .prologue
    .line 461
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    if-eqz v12, :cond_0

    .line 462
    const-string v12, "FaceIndicatorView"

    const-string v13, "enter checkContactData"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gez v12, :cond_1

    .line 562
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 467
    :cond_1
    const/4 v1, 0x0

    .line 469
    .local v1, "curName":Ljava/lang/String;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v12, :cond_0

    .line 472
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    aget-object v5, v12, v13

    .line 473
    .local v5, "face":Lcom/sec/android/gallery3d/data/Face;
    if-eqz v5, :cond_0

    .line 477
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 478
    .local v7, "pID":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v12, v7}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 480
    if-nez v1, :cond_3

    .line 481
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    invoke-virtual {v12}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 482
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    invoke-virtual {v12}, Landroid/app/AlertDialog;->dismiss()V

    .line 485
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v12, :cond_0

    .line 486
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/ui/ContactPopup;->showMoreDialog(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 461
    .end local v1    # "curName":Ljava/lang/String;
    .end local v5    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v7    # "pID":I
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 490
    .restart local v1    # "curName":Ljava/lang/String;
    .restart local v5    # "face":Lcom/sec/android/gallery3d/data/Face;
    .restart local v7    # "pID":I
    :cond_3
    const/4 v6, 0x0

    .line 491
    .local v6, "lookupKey":Ljava/lang/String;
    :try_start_2
    const-string v12, "profile/Me"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 492
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    .line 502
    :cond_4
    :goto_1
    if-eqz v6, :cond_0

    .line 503
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v12, v6}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getPhoneNumbers(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 504
    .local v8, "phoneNums":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v12, v6}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getEmailAddresses(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 506
    .local v4, "emails":[Ljava/lang/String;
    new-instance v2, Lcom/sec/samsung/gallery/access/contact/ContactData;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/access/contact/ContactData;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 508
    .local v2, "data":Lcom/sec/samsung/gallery/access/contact/ContactData;
    :try_start_3
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v12

    iput v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    .line 509
    iput-object v1, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->contactName:Ljava/lang/String;

    .line 510
    if-nez v8, :cond_8

    const/4 v12, 0x0

    :goto_2
    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->phoneNum:Ljava/lang/String;

    .line 511
    if-nez v4, :cond_9

    const/4 v12, 0x0

    :goto_3
    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->email:Ljava/lang/String;

    .line 512
    iput-object v6, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    .line 513
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Face;->getAutoGroup()I

    move-result v12

    iput v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->mAutoGroup:I

    .line 514
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurContactLookupKey:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 520
    const/4 v9, 0x0

    .line 522
    .local v9, "sd":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    if-eqz p1, :cond_a

    .line 523
    :try_start_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 524
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->hasInfo(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 525
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->getInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    move-result-object v9

    .line 533
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-nez v12, :cond_6

    .line 534
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->addContactPopupView()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 538
    :cond_6
    if-nez v9, :cond_b

    .line 539
    :try_start_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setPopupType(Z)V

    .line 553
    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v12, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setContactData(Lcom/sec/samsung/gallery/access/contact/ContactData;)V

    .line 554
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 555
    :catch_0
    move-exception v3

    .line 556
    .local v3, "e":Ljava/lang/NullPointerException;
    :try_start_6
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 494
    .end local v2    # "data":Lcom/sec/samsung/gallery/access/contact/ContactData;
    .end local v3    # "e":Ljava/lang/NullPointerException;
    .end local v4    # "emails":[Ljava/lang/String;
    .end local v8    # "phoneNums":[Ljava/lang/String;
    .end local v9    # "sd":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    :cond_7
    const-string v12, "/"

    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 495
    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 496
    .local v11, "values":[Ljava/lang/String;
    const/4 v12, 0x0

    aget-object v12, v11, v12

    if-eqz v12, :cond_4

    .line 497
    const/4 v12, 0x0

    aget-object v6, v11, v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 510
    .end local v11    # "values":[Ljava/lang/String;
    .restart local v2    # "data":Lcom/sec/samsung/gallery/access/contact/ContactData;
    .restart local v4    # "emails":[Ljava/lang/String;
    .restart local v8    # "phoneNums":[Ljava/lang/String;
    :cond_8
    const/4 v12, 0x0

    :try_start_7
    aget-object v12, v8, v12

    goto/16 :goto_2

    .line 511
    :cond_9
    const/4 v12, 0x0

    aget-object v12, v4, v12
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 515
    :catch_1
    move-exception v3

    .line 516
    .restart local v3    # "e":Ljava/lang/NullPointerException;
    :try_start_8
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 529
    .end local v3    # "e":Ljava/lang/NullPointerException;
    .restart local v9    # "sd":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v13, v6}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSNSInformation(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 530
    .local v10, "snsInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getLatestSnsData(Ljava/util/ArrayList;)Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v9

    goto :goto_4

    .line 540
    .end local v10    # "snsInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;>;"
    :cond_b
    :try_start_9
    iget-object v12, v9, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountName:Ljava/lang/String;

    if-nez v12, :cond_c

    .line 541
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setPopupType(Z)V
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5

    .line 557
    :catch_2
    move-exception v3

    .line 558
    .local v3, "e":Ljava/lang/Exception;
    :try_start_a
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 543
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_c
    :try_start_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setPopupType(Z)V

    .line 544
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v14, 0x0

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v15, 0x1

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/16 v16, 0x2

    aget v15, v15, v16

    invoke-virtual {v12, v13, v14, v15}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setHeadPosition(III)V

    .line 545
    iget-object v12, v9, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mMessageId:Ljava/lang/String;

    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->messageId:Ljava/lang/String;

    .line 546
    iget-object v12, v9, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsMsg:Ljava/lang/String;

    .line 547
    iget-object v12, v9, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mDescription:Ljava/lang/String;

    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->description:Ljava/lang/String;

    .line 548
    iget-object v12, v9, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mStory:Ljava/lang/String;

    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->story:Ljava/lang/String;

    .line 549
    iget-object v12, v9, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mTime:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsTimeStamp:Ljava/lang/String;

    .line 550
    iget-object v12, v9, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mResPackage:Ljava/lang/String;

    iput-object v12, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsPackage:Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_5
.end method

.method private checkFaceRect(II)Z
    .locals 11
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2283
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-eqz v9, :cond_0

    iget-boolean v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z

    if-nez v9, :cond_1

    .line 2284
    :cond_0
    const/4 v9, 0x0

    .line 2337
    :goto_0
    return v9

    .line 2287
    :cond_1
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2288
    .local v3, "faceCnt":I
    if-lez v3, :cond_b

    .line 2289
    const/4 v5, -0x1

    .line 2290
    .local v5, "nearerFaceIndex":I
    const-wide v6, 0x4086800000000000L    # 720.0

    .line 2291
    .local v6, "nearestDistance":D
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v3, :cond_4

    .line 2292
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 2293
    .local v2, "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    iget v9, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    if-nez v9, :cond_3

    iget-boolean v9, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v9, :cond_3

    iget-boolean v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-nez v9, :cond_3

    .line 2291
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2297
    :cond_3
    iget-object v9, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    invoke-virtual {v9, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2298
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v9, v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    invoke-direct {p0, v9, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->calculateDistance(Landroid/graphics/Rect;II)D

    move-result-wide v0

    .line 2299
    .local v0, "distance":D
    cmpl-double v9, v6, v0

    if-lez v9, :cond_2

    .line 2300
    move-wide v6, v0

    .line 2301
    move v5, v4

    goto :goto_2

    .line 2306
    .end local v0    # "distance":D
    .end local v2    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_4
    const/4 v9, -0x1

    if-eq v5, v9, :cond_b

    .line 2307
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v8, v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    .line 2308
    .local v8, "vis":Z
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    if-nez v8, :cond_7

    const/4 v10, 0x1

    :goto_3
    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 2311
    iget v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_8

    iget-boolean v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v9, :cond_8

    .line 2312
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 2313
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 2314
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hideContactPopupView()V

    .line 2321
    :cond_5
    :goto_4
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v9, v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    if-nez v9, :cond_9

    .line 2322
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 2323
    iget v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-eq v9, v5, :cond_6

    .line 2324
    iput v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 2327
    :cond_6
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkContactData(Z)V

    .line 2333
    :goto_5
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 2334
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 2308
    :cond_7
    const/4 v10, 0x0

    goto :goto_3

    .line 2315
    :cond_8
    iget v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    iget v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-eq v9, v5, :cond_5

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v9, v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v9, v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v9, :cond_5

    .line 2318
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    goto :goto_4

    .line 2329
    :cond_9
    iput v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 2330
    if-nez v8, :cond_a

    const/4 v9, 0x1

    :goto_6
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    goto :goto_5

    :cond_a
    const/4 v9, 0x0

    goto :goto_6

    .line 2337
    .end local v4    # "i":I
    .end local v5    # "nearerFaceIndex":I
    .end local v6    # "nearestDistance":D
    .end local v8    # "vis":Z
    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method private declared-synchronized checkMeContactData(Z)V
    .locals 12
    .param p1, "onlyCheckCache"    # Z

    .prologue
    const/4 v9, 0x0

    .line 594
    monitor-enter p0

    :try_start_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    if-eqz v10, :cond_0

    .line 595
    const-string v10, "FaceIndicatorView"

    const-string v11, "enter checkContactData"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    iget v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gez v10, :cond_1

    .line 675
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 600
    :cond_1
    const/4 v0, 0x0

    .line 602
    .local v0, "curName":Ljava/lang/String;
    :try_start_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v10, :cond_0

    .line 605
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    iget v11, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    aget-object v3, v10, v11

    .line 606
    .local v3, "face":Lcom/sec/android/gallery3d/data/Face;
    if-eqz v3, :cond_0

    .line 610
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 611
    .local v5, "pID":I
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v10, v5}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 613
    if-nez v0, :cond_2

    .line 614
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 616
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 594
    .end local v0    # "curName":Ljava/lang/String;
    .end local v3    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v5    # "pID":I
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 620
    .restart local v0    # "curName":Ljava/lang/String;
    .restart local v3    # "face":Lcom/sec/android/gallery3d/data/Face;
    .restart local v5    # "pID":I
    :cond_2
    :try_start_2
    const-string v10, "profile/Me"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 623
    const/4 v4, 0x0

    .line 625
    .local v4, "lookupKey":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    .line 627
    if-eqz v4, :cond_0

    .line 628
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v10, v4}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getPhoneNumbers(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 629
    .local v6, "phoneNums":[Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v10, v4}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getEmailAddresses(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 630
    .local v2, "emails":[Ljava/lang/String;
    new-instance v1, Lcom/sec/samsung/gallery/access/contact/ContactData;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/access/contact/ContactData;-><init>()V

    .line 631
    .local v1, "data":Lcom/sec/samsung/gallery/access/contact/ContactData;
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    iget v11, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v10

    iput v10, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    .line 632
    iput-object v0, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->contactName:Ljava/lang/String;

    .line 633
    if-nez v6, :cond_5

    move-object v10, v9

    :goto_1
    iput-object v10, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->phoneNum:Ljava/lang/String;

    .line 634
    if-nez v2, :cond_6

    :goto_2
    iput-object v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->email:Ljava/lang/String;

    .line 635
    iput-object v4, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    .line 636
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    iget v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    aget-object v9, v9, v10

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Face;->getAutoGroup()I

    move-result v9

    iput v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->mAutoGroup:I

    .line 637
    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurContactLookupKey:Ljava/lang/String;

    .line 639
    const/4 v7, 0x0

    .line 640
    .local v7, "sd":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    if-eqz p1, :cond_7

    .line 641
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 642
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->hasInfo(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 643
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->getInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    move-result-object v7

    .line 651
    :cond_3
    :goto_3
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-nez v9, :cond_4

    .line 652
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->addContactPopupView()V

    .line 655
    :cond_4
    if-nez v7, :cond_8

    .line 656
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setPopupType(Z)V

    .line 669
    :goto_4
    const-string v9, "profile/Me"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 670
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->createSettingPopupForMe(Lcom/sec/samsung/gallery/access/contact/ContactData;)V

    .line 671
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    goto/16 :goto_0

    .line 633
    .end local v7    # "sd":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    :cond_5
    const/4 v10, 0x0

    aget-object v10, v6, v10

    goto :goto_1

    .line 634
    :cond_6
    const/4 v9, 0x0

    aget-object v9, v2, v9

    goto :goto_2

    .line 647
    .restart local v7    # "sd":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    :cond_7
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v10, v4}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSNSInformation(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 648
    .local v8, "snsInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;>;"
    invoke-direct {p0, v8}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getLatestSnsData(Ljava/util/ArrayList;)Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    move-result-object v7

    goto :goto_3

    .line 657
    .end local v8    # "snsInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;>;"
    :cond_8
    iget-object v9, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountName:Ljava/lang/String;

    if-nez v9, :cond_9

    .line 658
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setPopupType(Z)V

    goto :goto_4

    .line 660
    :cond_9
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setPopupType(Z)V

    .line 661
    iget-object v9, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mMessageId:Ljava/lang/String;

    iput-object v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->messageId:Ljava/lang/String;

    .line 662
    iget-object v9, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    iput-object v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsMsg:Ljava/lang/String;

    .line 663
    iget-object v9, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mDescription:Ljava/lang/String;

    iput-object v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->description:Ljava/lang/String;

    .line 664
    iget-object v9, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mStory:Ljava/lang/String;

    iput-object v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->story:Ljava/lang/String;

    .line 665
    iget-object v9, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mTime:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsTimeStamp:Ljava/lang/String;

    .line 666
    iget-object v9, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mResPackage:Ljava/lang/String;

    iput-object v9, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsPackage:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method private checkNameTagClick(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2764
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkTopFaceTagClicked(II)I

    move-result v0

    .line 2765
    .local v0, "faceIndex":I
    if-gez v0, :cond_0

    move v2, v3

    .line 2793
    :goto_0
    return v2

    .line 2768
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v1, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    .line 2769
    .local v1, "faceState":I
    packed-switch v1, :pswitch_data_0

    :goto_1
    move v2, v4

    .line 2793
    goto :goto_0

    .line 2771
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v2, :cond_1

    .line 2772
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupHeight:I

    .line 2774
    :cond_1
    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 2775
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurFaceId(I)V

    .line 2776
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isMe()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2777
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkMeContactData(Z)V

    .line 2783
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto :goto_1

    .line 2779
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 2780
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 2781
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkContactData(Z)V

    goto :goto_2

    .line 2786
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    .line 2787
    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 2788
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 2789
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto :goto_1

    .line 2769
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkNameTagOverlap(II)Z
    .locals 7
    .param p1, "left"    # I
    .param p2, "top"    # I

    .prologue
    .line 3165
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3166
    .local v0, "faceCnt":I
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->calculateSNSArea(II)Landroid/graphics/Rect;

    move-result-object v4

    .line 3167
    .local v4, "popupArea":Landroid/graphics/Rect;
    const/4 v3, 0x0

    .line 3169
    .local v3, "isIntersected":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 3170
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v5, v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    if-eqz v5, :cond_0

    .line 3171
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v1, v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    .line 3172
    .local v1, "faceTagArea":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v5, v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    and-int/lit8 v5, v5, 0x10

    if-nez v5, :cond_0

    invoke-static {v1, v4}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3174
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v5, v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->canShowInUpMode(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3175
    const/4 v3, 0x1

    .line 3176
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v6, v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    .line 3169
    .end local v1    # "faceTagArea":Landroid/graphics/Rect;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3182
    :cond_1
    return v3
.end method

.method private checkNameTagROrL(II)I
    .locals 3
    .param p1, "texPX"    # I
    .param p2, "texWidth"    # I

    .prologue
    .line 1175
    const/4 v0, 0x0

    .line 1177
    .local v0, "pos":I
    add-int v1, p1, p2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    if-le v1, v2, :cond_0

    .line 1178
    const/4 v0, 0x1

    .line 1180
    :cond_0
    return v0
.end method

.method private checkPopupItem(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 2419
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v3, :cond_1

    :cond_0
    move v3, v4

    .line 2443
    :goto_0
    return v3

    .line 2423
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2424
    .local v0, "faceCnt":I
    if-lez v0, :cond_5

    .line 2425
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_5

    .line 2426
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v3, v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v3, v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v3, :cond_2

    .line 2427
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v3, v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    invoke-direct {p0, v3, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkPopupItemClickedIndex([Landroid/graphics/Rect;II)I

    move-result v2

    .line 2429
    .local v2, "index":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 2430
    invoke-direct {p0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->clickPopupItem(II)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2431
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    if-eqz v3, :cond_3

    .line 2425
    .end local v2    # "index":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .restart local v2    # "index":I
    :cond_3
    move v3, v4

    .line 2434
    goto :goto_0

    .line 2437
    :cond_4
    const/4 v3, 0x1

    goto :goto_0

    .end local v1    # "i":I
    .end local v2    # "index":I
    :cond_5
    move v3, v4

    .line 2443
    goto :goto_0
.end method

.method private checkPopupItemClickedIndex([Landroid/graphics/Rect;II)I
    .locals 3
    .param p1, "popupItemRect"    # [Landroid/graphics/Rect;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 2356
    const/4 v1, -0x1

    .line 2357
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 2358
    aget-object v2, p1, v0

    if-eqz v2, :cond_1

    aget-object v2, p1, v0

    invoke-virtual {v2, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2359
    move v1, v0

    .line 2364
    :cond_0
    return v1

    .line 2357
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private checkTagMain(I)Z
    .locals 9
    .param p1, "index"    # I

    .prologue
    const v8, 0x7f0e0068

    .line 1532
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Face;->getGroupId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1533
    .local v1, "key":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    .line 1534
    .local v0, "curMediaSetKey":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1535
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1536
    .local v4, "vs":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v0, v4, v5

    .line 1540
    .end local v4    # "vs":[Ljava/lang/String;
    :goto_0
    const/4 v2, 0x0

    .line 1541
    .local v2, "main":Z
    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetName:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1543
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1546
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1547
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->checkInGroupCluster()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1548
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetName:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v7, 0x7f0e00c3

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1549
    const/4 v2, 0x1

    .line 1560
    :cond_1
    :goto_1
    return v2

    .line 1538
    .end local v2    # "main":Z
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Face;->getRecommendedId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1550
    .restart local v2    # "main":Z
    :cond_3
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetName:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1551
    const/4 v2, 0x0

    goto :goto_1

    .line 1553
    :cond_4
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v5, v5, p1

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1554
    .local v3, "personId":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    invoke-interface {v5, v3, v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->checkFaceInCurrentGroup(ILjava/lang/String;)Z

    move-result v2

    goto :goto_1
.end method

.method private declared-synchronized checkTopFaceTagClicked(II)I
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2643
    monitor-enter p0

    const/4 v4, -0x1

    .line 2644
    .local v4, "index":I
    const/4 v1, -0x1

    .line 2645
    .local v1, "faceIndex":I
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2646
    .local v2, "faceSize":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    .line 2647
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v6, v7, -0x1

    .local v6, "j":I
    :goto_0
    if-ltz v6, :cond_2

    .line 2648
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2649
    const/4 v7, -0x1

    if-eq v1, v7, :cond_0

    if-lt v1, v2, :cond_1

    .line 2647
    :cond_0
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    .line 2651
    :cond_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 2652
    .local v0, "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    if-eqz v7, :cond_0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    invoke-virtual {v7, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2653
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    move v5, v4

    .line 2666
    .end local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .end local v4    # "index":I
    .end local v6    # "j":I
    .local v5, "index":I
    :goto_1
    monitor-exit p0

    return v5

    .line 2659
    .end local v5    # "index":I
    .restart local v4    # "index":I
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_4

    .line 2660
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 2661
    .restart local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    if-eqz v7, :cond_3

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    invoke-virtual {v7, p1, p2}, Landroid/graphics/Rect;->contains(II)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    if-eqz v7, :cond_3

    .line 2662
    move v4, v3

    .line 2659
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_4
    move v5, v4

    .line 2666
    .end local v4    # "index":I
    .restart local v5    # "index":I
    goto :goto_1

    .line 2643
    .end local v2    # "faceSize":I
    .end local v3    # "i":I
    .end local v5    # "index":I
    .restart local v4    # "index":I
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method private clickPopupItem(II)Z
    .locals 21
    .param p1, "faceIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 2447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 2448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    move/from16 v19, v0

    if-nez v19, :cond_2

    const/16 v19, 0x1

    :goto_0
    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 2449
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    .line 2451
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->soundAndVibrator()V

    .line 2453
    const/4 v9, -0x1

    .line 2454
    .local v9, "faceState":I
    if-ltz p1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, p1

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    .line 2455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v18

    iget v9, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    .line 2456
    :cond_1
    const/4 v12, 0x0

    .line 2458
    .local v12, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v18, v0

    if-nez v18, :cond_3

    .line 2459
    const/16 v18, 0x0

    .line 2622
    :goto_1
    return v18

    .line 2448
    .end local v9    # "faceState":I
    .end local v12    # "key":Ljava/lang/String;
    :cond_2
    const/16 v19, 0x0

    goto :goto_0

    .line 2461
    .restart local v9    # "faceState":I
    .restart local v12    # "key":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v18, v0

    aget-object v8, v18, p1

    .line 2463
    .local v8, "face":Lcom/sec/android/gallery3d/data/Face;
    if-nez v8, :cond_4

    .line 2464
    const/16 v18, 0x0

    goto :goto_1

    .line 2467
    :cond_4
    packed-switch v9, :pswitch_data_0

    .line 2620
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 2621
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 2622
    const/16 v18, 0x1

    goto :goto_1

    .line 2470
    :pswitch_0
    const/4 v4, 0x0

    .line 2471
    .local v4, "candidatesCnt":I
    const-string v18, "FaceIndicatorView"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "index "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " clicked!!"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    move-object/from16 v18, v0

    if-eqz v18, :cond_9

    .line 2473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    .line 2474
    .local v10, "ids":[I
    array-length v4, v10

    .line 2475
    const/4 v6, 0x0

    .line 2476
    .local v6, "changedName":Ljava/lang/String;
    if-lez v4, :cond_9

    move/from16 v0, p2

    if-ge v0, v4, :cond_9

    .line 2478
    aget v18, v10, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMePersonId:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 2480
    const-string v6, "profile/Me"

    .line 2481
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v18

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v6, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->assignName(Ljava/lang/String;IZ)I

    goto/16 :goto_2

    .line 2485
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    aget v19, v10, p2

    invoke-static/range {v18 .. v19}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 2487
    if-eqz v6, :cond_5

    .line 2489
    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 2492
    .local v17, "values":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_5

    .line 2496
    const/16 v18, 0x1

    aget-object v14, v17, v18

    .line 2497
    .local v14, "name":Ljava/lang/String;
    const/16 v18, 0x0

    aget-object v18, v17, v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurContactLookupKey:Ljava/lang/String;

    .line 2501
    new-instance v7, Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;-><init>(Landroid/content/Context;)V

    .line 2503
    .local v7, "contactProvider":Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurContactLookupKey:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->checkCallerIdPhoto(Ljava/lang/String;)Z

    move-result v11

    .line 2506
    .local v11, "isPhotoNeeded":Z
    if-eqz v11, :cond_8

    .line 2507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V

    .line 2509
    const/16 v18, 0x0

    aget-object v18, v17, v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurContactLookupKey:Ljava/lang/String;

    .line 2510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    const v19, 0x7f0e00b2

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v14, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 2514
    .local v13, "message":Ljava/lang/String;
    const v16, 0x7f0e00b4

    .line 2515
    .local v16, "title":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPhone(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_7

    .line 2516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    const v19, 0x7f0e00b3

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v14, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 2520
    const v16, 0x7f0e00b6

    .line 2522
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-direct {v0, v1, v2, v13}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->showAlertDialog(Landroid/content/Context;ILjava/lang/String;)V

    .line 2528
    .end local v13    # "message":Ljava/lang/String;
    .end local v16    # "title":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v19

    aget v20, v10, p2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setPerson(Landroid/content/Context;II)V

    goto/16 :goto_2

    .line 2536
    .end local v6    # "changedName":Ljava/lang/String;
    .end local v7    # "contactProvider":Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    .end local v10    # "ids":[I
    .end local v11    # "isPhotoNeeded":Z
    .end local v14    # "name":Ljava/lang/String;
    .end local v17    # "values":[Ljava/lang/String;
    :cond_9
    sub-int p2, p2, v4

    .line 2537
    if-nez p2, :cond_a

    .line 2539
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    .line 2540
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    .line 2541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V

    .line 2542
    sget-object v18, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->NOT_INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V

    goto/16 :goto_2

    .line 2543
    :cond_a
    const/16 v18, 0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 2544
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getAutoGroup()I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 2545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->removeFace(Landroid/content/Context;I)V

    .line 2551
    :goto_3
    const/16 v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    goto/16 :goto_2

    .line 2548
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setFaceUnknown(Landroid/content/Context;I)V

    goto :goto_3

    .line 2557
    .end local v4    # "candidatesCnt":I
    :pswitch_1
    const/4 v5, 0x0

    .line 2558
    .local v5, "changed":Z
    const/4 v6, 0x0

    .line 2559
    .restart local v6    # "changedName":Ljava/lang/String;
    const/4 v15, 0x0

    .line 2560
    .local v15, "personId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 2561
    if-nez p2, :cond_c

    .line 2563
    const-string v6, "profile/Me"

    .line 2564
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v18

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v6, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->assignName(Ljava/lang/String;IZ)I

    move-result v15

    .line 2565
    const/4 v5, 0x1

    .line 2568
    :cond_c
    const/16 v18, 0x1

    move/from16 v0, v18

    move/from16 v1, p2

    if-ne v0, v1, :cond_d

    .line 2570
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    .line 2571
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    .line 2572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V

    .line 2573
    sget-object v18, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->NOT_INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V

    .line 2574
    const/4 v5, 0x1

    .line 2589
    :cond_d
    :goto_4
    if-eqz v5, :cond_5

    .line 2591
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getRecommendedId()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getGroupId()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 2592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "1/0"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 2593
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainFaceModified:Z

    .line 2594
    if-eqz v6, :cond_5

    .line 2595
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAssignedName:Ljava/lang/String;

    .line 2596
    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaSetKey:Ljava/lang/String;

    goto/16 :goto_2

    .line 2578
    :cond_e
    if-nez p2, :cond_d

    .line 2580
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    .line 2581
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    .line 2582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V

    .line 2583
    sget-object v18, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->NOT_INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V

    .line 2584
    const/4 v5, 0x1

    goto/16 :goto_4

    .line 2604
    .end local v5    # "changed":Z
    .end local v6    # "changedName":Ljava/lang/String;
    .end local v15    # "personId":I
    :pswitch_2
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 2467
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private createSettingPopupForMe(Lcom/sec/samsung/gallery/access/contact/ContactData;)V
    .locals 6
    .param p1, "contactData"    # Lcom/sec/samsung/gallery/access/contact/ContactData;

    .prologue
    .line 678
    const-string v2, "FaceIndicatorView"

    const-string v3, "createSettingPopupForMe"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    if-nez p1, :cond_0

    .line 793
    :goto_0
    return-void

    .line 683
    :cond_0
    const/4 v0, 0x0

    .line 685
    .local v0, "id":I
    const v0, 0x7f090038

    .line 687
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 688
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_1

    .line 689
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 691
    :cond_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$3;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/samsung/gallery/access/contact/ContactData;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    .line 763
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    new-instance v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$4;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$4;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 771
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    new-instance v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$5;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$5;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 779
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    const/4 v3, -0x2

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0046

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$6;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 791
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private dismissDialogs()V
    .locals 7

    .prologue
    .line 448
    const/4 v5, 0x5

    new-array v2, v5, [Landroid/app/AlertDialog;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    aput-object v6, v2, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMoreDialog:Landroid/app/AlertDialog;

    aput-object v6, v2, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mConfirmDialog:Landroid/app/AlertDialog;

    aput-object v6, v2, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAddContactsDialog:Landroid/app/AlertDialog;

    aput-object v6, v2, v5

    const/4 v5, 0x4

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mChannelDialog:Landroid/app/AlertDialog;

    aput-object v6, v2, v5

    .line 452
    .local v2, "dialogs":[Landroid/app/AlertDialog;
    move-object v0, v2

    .local v0, "arr$":[Landroid/app/AlertDialog;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 453
    .local v1, "d":Landroid/app/AlertDialog;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 454
    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 452
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 458
    .end local v1    # "d":Landroid/app/AlertDialog;
    :cond_1
    return-void
.end method

.method private drawCandidatesList(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;I)[Landroid/graphics/Rect;
    .locals 45
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "pt"    # Landroid/graphics/Point;
    .param p3, "index"    # I

    .prologue
    .line 2019
    if-ltz p3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v2, v2

    move/from16 v0, p3

    if-le v0, v2, :cond_1

    .line 2020
    :cond_0
    const/16 v44, 0x0

    .line 2157
    :goto_0
    return-object v44

    .line 2023
    :cond_1
    const/16 v30, 0x0

    .line 2024
    .local v30, "count":I
    const/16 v38, 0x0

    .line 2025
    .local v38, "itemCnt":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v31

    .line 2028
    .local v31, "divideThickness":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    if-eqz v2, :cond_2

    .line 2029
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    array-length v2, v2

    add-int v38, v38, v2

    .line 2032
    :cond_2
    add-int/lit8 v38, v38, 0x1

    .line 2034
    add-int/lit8 v38, v38, 0x1

    .line 2036
    move/from16 v0, v38

    new-array v0, v0, [Landroid/graphics/Rect;

    move-object/from16 v44, v0

    .line 2038
    .local v44, "tempRect":[Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    sub-int/2addr v2, v3

    div-int/lit8 v4, v2, 0x2

    .line 2039
    .local v4, "left":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    mul-int v3, v3, v38

    sub-int/2addr v2, v3

    div-int/lit8 v5, v2, 0x2

    .line 2041
    .local v5, "top":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v0, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    move/from16 v34, v0

    .line 2042
    .local v34, "focus":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->LEFT_ALIGN_FACE_POPUP_ITEM_TEXT:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v39, v2, v3

    .line 2043
    .local v39, "maxWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    if-eqz v2, :cond_11

    .line 2044
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v0, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    move-object/from16 v37, v0

    .line 2045
    .local v37, "ids":[I
    move-object/from16 v0, v37

    array-length v0, v0

    move/from16 v28, v0

    .line 2046
    .local v28, "cnt":I
    if-lez v28, :cond_11

    .line 2047
    const/16 v40, 0x0

    .line 2048
    .local v40, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v2, v2, p3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Face;->getRecommendedId()I

    move-result v41

    .line 2049
    .local v41, "recommendedId":I
    new-instance v43, Ljava/util/ArrayList;

    invoke-direct/range {v43 .. v43}, Ljava/util/ArrayList;-><init>()V

    .line 2050
    .local v43, "strTextures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glrenderer/StringTexture;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAddTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    .line 2052
    const/16 v35, 0x0

    .local v35, "i":I
    :goto_1
    move-object/from16 v0, v37

    array-length v2, v0

    move/from16 v0, v35

    if-ge v0, v2, :cond_6

    .line 2053
    const/16 v32, 0x0

    .line 2054
    .local v32, "dsn":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    if-eqz v2, :cond_5

    if-nez v35, :cond_5

    .line 2055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 2060
    :goto_2
    sget v29, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_TEXT:I

    .line 2061
    .local v29, "color":I
    aget v2, v37, v35

    move/from16 v0, v41

    if-ne v0, v2, :cond_3

    .line 2062
    const v29, -0xff3f01

    .line 2069
    :cond_3
    if-eqz v32, :cond_4

    .line 2071
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_TEXT_SIZE:I

    int-to-float v2, v2

    move/from16 v0, v39

    int-to-float v3, v0

    const/4 v6, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v29

    invoke-static {v0, v2, v1, v3, v6}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v12

    .line 2074
    .local v12, "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    .line 2075
    move-object/from16 v0, v43

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2052
    .end local v12    # "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    :cond_4
    add-int/lit8 v35, v35, 0x1

    goto :goto_1

    .line 2057
    .end local v29    # "color":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    aget v3, v37, v35

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v40

    .line 2058
    invoke-static/range {v40 .. v40}, Lcom/sec/android/gallery3d/util/GalleryUtils;->splitDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    goto :goto_2

    .line 2081
    .end local v32    # "dsn":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v33

    .line 2082
    .local v33, "extraHeight":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    mul-int v2, v2, v38

    add-int/lit8 v3, v38, -0x1

    mul-int v3, v3, v31

    add-int/2addr v2, v3

    add-int v7, v2, v33

    .line 2083
    .local v7, "menuHeight":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsTablet:Z

    if-eqz v2, :cond_8

    .line 2084
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->right:I

    .line 2085
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    sub-int v5, v3, v2

    .line 2087
    if-gez v4, :cond_9

    .line 2088
    const/4 v4, 0x0

    .line 2093
    :cond_7
    :goto_3
    if-gez v5, :cond_a

    .line 2094
    const/4 v5, 0x0

    .line 2099
    :cond_8
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPopupItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 2110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v5, v2

    .line 2111
    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v36

    .local v36, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 2112
    .restart local v12    # "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    if-nez v30, :cond_b

    const/4 v13, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    move/from16 v0, v30

    if-ne v2, v0, :cond_c

    const/4 v14, 0x1

    :goto_7
    move/from16 v0, v34

    move/from16 v1, v30

    if-ne v0, v1, :cond_d

    const/4 v15, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    move/from16 v0, v30

    if-ne v2, v0, :cond_e

    const/16 v16, 0x1

    :goto_9
    if-nez v30, :cond_f

    const/16 v17, -0x1

    :goto_a
    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v10, v4

    move v11, v5

    invoke-direct/range {v8 .. v17}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopupItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILcom/sec/android/gallery3d/glrenderer/StringTexture;ZZZZI)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v44, v30

    .line 2114
    add-int/lit8 v30, v30, 0x1

    .line 2115
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v2, v2, v31

    add-int/2addr v5, v2

    .line 2116
    goto :goto_5

    .line 2089
    .end local v12    # "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .end local v36    # "i$":Ljava/util/Iterator;
    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    if-le v2, v3, :cond_7

    .line 2090
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    sub-int v4, v2, v3

    goto :goto_3

    .line 2095
    :cond_a
    add-int v2, v5, v7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    if-le v2, v3, :cond_8

    .line 2096
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    sub-int v5, v2, v7

    goto/16 :goto_4

    .line 2112
    .restart local v12    # "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .restart local v36    # "i$":Ljava/util/Iterator;
    :cond_b
    const/4 v13, 0x1

    goto :goto_6

    :cond_c
    const/4 v14, 0x0

    goto :goto_7

    :cond_d
    const/4 v15, 0x0

    goto :goto_8

    :cond_e
    const/16 v16, 0x0

    goto :goto_9

    :cond_f
    const/16 v17, 0x2

    goto :goto_a

    .line 2118
    .end local v12    # "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    :cond_10
    if-eqz v43, :cond_11

    .line 2119
    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->clear()V

    .line 2125
    .end local v7    # "menuHeight":I
    .end local v28    # "cnt":I
    .end local v33    # "extraHeight":I
    .end local v35    # "i":I
    .end local v36    # "i$":Ljava/util/Iterator;
    .end local v37    # "ids":[I
    .end local v40    # "name":Ljava/lang/String;
    .end local v41    # "recommendedId":I
    .end local v43    # "strTextures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glrenderer/StringTexture;>;"
    :cond_11
    if-nez v30, :cond_12

    .line 2126
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/graphics/Rect;

    move-object/from16 v44, v0

    .line 2128
    :cond_12
    add-int/lit8 v2, v30, 0x2

    move/from16 v0, v34

    if-lt v0, v2, :cond_13

    .line 2129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    add-int/lit8 v34, v30, 0x1

    move/from16 v0, v34

    iput v0, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 2132
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00b0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_TEXT_SIZE:I

    int-to-float v3, v3

    sget v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_TEXT:I

    move/from16 v0, v39

    int-to-float v8, v0

    const/4 v9, 0x0

    invoke-static {v2, v3, v6, v8, v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v17

    .line 2135
    .local v17, "addTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00be

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_TEXT_SIZE:I

    int-to-float v3, v3

    sget v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_TEXT:I

    move/from16 v0, v39

    int-to-float v8, v0

    const/4 v9, 0x0

    invoke-static {v2, v3, v6, v8, v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v42

    .line 2139
    .local v42, "removeTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    if-nez v30, :cond_14

    const/16 v18, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    move/from16 v0, v30

    if-ne v0, v2, :cond_15

    const/16 v19, 0x1

    :goto_c
    move/from16 v0, v30

    move/from16 v1, v34

    if-ne v0, v1, :cond_16

    const/16 v20, 0x1

    :goto_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    move/from16 v0, v30

    if-ne v2, v0, :cond_17

    const/16 v21, 0x1

    :goto_e
    if-nez v30, :cond_18

    const/16 v22, -0x1

    :goto_f
    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move v15, v4

    move/from16 v16, v5

    invoke-direct/range {v13 .. v22}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopupItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILcom/sec/android/gallery3d/glrenderer/StringTexture;ZZZZI)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v44, v30

    .line 2142
    add-int/lit8 v30, v30, 0x1

    .line 2143
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v2, v2, v31

    add-int/2addr v5, v2

    .line 2144
    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    move/from16 v0, v30

    if-ne v0, v2, :cond_19

    const/16 v24, 0x1

    :goto_10
    move/from16 v0, v30

    move/from16 v1, v34

    if-ne v0, v1, :cond_1a

    const/16 v25, 0x1

    :goto_11
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    move/from16 v0, v30

    if-ne v2, v0, :cond_1b

    const/16 v26, 0x1

    :goto_12
    const/16 v27, 0x1

    move-object/from16 v18, p0

    move-object/from16 v19, p1

    move/from16 v20, v4

    move/from16 v21, v5

    move-object/from16 v22, v42

    invoke-direct/range {v18 .. v27}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopupItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILcom/sec/android/gallery3d/glrenderer/StringTexture;ZZZZI)Landroid/graphics/Rect;

    move-result-object v2

    aput-object v2, v44, v30

    .line 2154
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->recycle()V

    .line 2155
    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->recycle()V

    goto/16 :goto_0

    .line 2139
    :cond_14
    const/16 v18, 0x1

    goto :goto_b

    :cond_15
    const/16 v19, 0x0

    goto :goto_c

    :cond_16
    const/16 v20, 0x0

    goto :goto_d

    :cond_17
    const/16 v21, 0x0

    goto :goto_e

    :cond_18
    const/16 v22, 0x2

    goto :goto_f

    .line 2144
    :cond_19
    const/16 v24, 0x0

    goto :goto_10

    :cond_1a
    const/16 v25, 0x0

    goto :goto_11

    :cond_1b
    const/16 v26, 0x0

    goto :goto_12
.end method

.method private drawFaceIndicatorView(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 50
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 1587
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v4, v4

    if-lez v4, :cond_0

    .line 1588
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v0, v4

    move/from16 v18, v0

    .line 1589
    .local v18, "count":I
    const/16 v33, 0x0

    .line 1590
    .local v33, "rtScreen":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mImageBounds:Landroid/graphics/Rect;

    .line 1591
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceRectList()V

    .line 1593
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1861
    .end local v18    # "count":I
    .end local v33    # "rtScreen":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return-void

    .line 1596
    .restart local v18    # "count":I
    .restart local v33    # "rtScreen":Landroid/graphics/Rect;
    :cond_1
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v15, v4, :cond_9

    .line 1598
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    .line 1600
    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/Rect;->left:I

    if-gez v4, :cond_3

    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/Rect;->right:I

    if-gez v4, :cond_3

    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/Rect;->top:I

    if-gez v4, :cond_3

    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    if-gez v4, :cond_3

    .line 1596
    :cond_2
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 1606
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-nez v4, :cond_4

    .line 1607
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v6, 0x0

    iput v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    .line 1610
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v40, v0

    .line 1614
    .local v40, "state":I
    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v4

    move/from16 v48, v0

    .line 1615
    .local v48, "x":F
    move-object/from16 v0, v33

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v4

    move/from16 v49, v0

    .line 1616
    .local v49, "y":F
    invoke-virtual/range {v33 .. v33}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v0, v4

    move/from16 v46, v0

    .line 1617
    .local v46, "w":F
    invoke-virtual/range {v33 .. v33}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v0, v4

    move/from16 v23, v0

    .line 1618
    .local v23, "h":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const-string/jumbo v6, "window"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Landroid/view/WindowManager;

    .line 1619
    .local v47, "wm":Landroid/view/WindowManager;
    invoke-interface/range {v47 .. v47}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v20

    .line 1620
    .local v20, "display":Landroid/view/Display;
    new-instance v37, Landroid/graphics/Point;

    invoke-direct/range {v37 .. v37}, Landroid/graphics/Point;-><init>()V

    .line 1621
    .local v37, "size":Landroid/graphics/Point;
    move-object/from16 v0, v20

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1622
    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v36, v0

    .line 1623
    .local v36, "screenWidth":I
    move-object/from16 v0, v37

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v35, v0

    .line 1624
    .local v35, "screenHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScale:F

    move/from16 v34, v0

    .line 1625
    .local v34, "s":F
    mul-float v4, v48, v34

    move/from16 v0, v36

    int-to-float v6, v0

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v7, v7, v34

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float v43, v4, v6

    .line 1626
    .local v43, "targetX":F
    mul-float v4, v49, v34

    move/from16 v0, v35

    int-to-float v6, v0

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v7, v7, v34

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float v44, v4, v6

    .line 1627
    .local v44, "targetY":F
    if-eqz v40, :cond_2

    .line 1630
    const/4 v4, 0x2

    move/from16 v0, v40

    if-ne v0, v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v4, v4

    if-ge v15, v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v4, v4, v15

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v4

    if-eqz v4, :cond_7

    .line 1632
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCircleFace:Z

    if-eqz v4, :cond_6

    .line 1633
    new-instance v28, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct/range {v28 .. v28}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    .line 1635
    .local v28, "paint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v4, :cond_5

    .line 1636
    const v4, -0xd03901

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 1640
    :goto_3
    const/4 v4, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setAntiAlias(Z)V

    .line 1641
    const/high16 v4, 0x40a00000    # 5.0f

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 1642
    sget-object v4, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setStyle(Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;)V

    .line 1643
    move/from16 v0, v43

    float-to-int v4, v0

    int-to-float v4, v4

    mul-float v6, v46, v34

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v4, v6

    move/from16 v0, v44

    float-to-int v6, v0

    int-to-float v6, v6

    mul-float v7, v23, v34

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    mul-float v7, v46, v34

    mul-float v8, v23, v34

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v4, v6, v7, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawCircle(FFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    goto/16 :goto_2

    .line 1638
    :cond_5
    const/16 v4, -0x100

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    goto :goto_3

    .line 1648
    .end local v28    # "paint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNoRecommendBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move/from16 v0, v43

    float-to-int v5, v0

    move/from16 v0, v44

    float-to-int v6, v0

    mul-float v4, v46, v34

    float-to-int v7, v4

    mul-float v4, v23, v34

    float-to-int v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_2

    .line 1651
    :cond_7
    const/4 v4, 0x1

    move/from16 v0, v40

    if-ne v0, v4, :cond_2

    .line 1652
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCircleFace:Z

    if-eqz v4, :cond_8

    .line 1653
    new-instance v28, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct/range {v28 .. v28}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    .line 1654
    .restart local v28    # "paint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    const v4, -0xff0001

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 1655
    const/4 v4, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setAntiAlias(Z)V

    .line 1656
    const/high16 v4, 0x40a00000    # 5.0f

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 1657
    sget-object v4, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setStyle(Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;)V

    .line 1658
    move/from16 v0, v43

    float-to-int v4, v0

    int-to-float v4, v4

    mul-float v6, v46, v34

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v4, v6

    move/from16 v0, v44

    float-to-int v6, v0

    int-to-float v6, v6

    mul-float v7, v23, v34

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    mul-float v7, v46, v34

    mul-float v8, v23, v34

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-interface {v0, v4, v6, v7, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawCircle(FFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    goto/16 :goto_2

    .line 1661
    .end local v28    # "paint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRecommendedBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move/from16 v0, v43

    float-to-int v5, v0

    move/from16 v0, v44

    float-to-int v6, v0

    mul-float v4, v46, v34

    float-to-int v7, v4

    mul-float v4, v23, v34

    float-to-int v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_2

    .line 1669
    .end local v20    # "display":Landroid/view/Display;
    .end local v23    # "h":F
    .end local v34    # "s":F
    .end local v35    # "screenHeight":I
    .end local v36    # "screenWidth":I
    .end local v37    # "size":Landroid/graphics/Point;
    .end local v40    # "state":I
    .end local v43    # "targetX":F
    .end local v44    # "targetY":F
    .end local v46    # "w":F
    .end local v47    # "wm":Landroid/view/WindowManager;
    .end local v48    # "x":F
    .end local v49    # "y":F
    :cond_9
    const/4 v4, 0x2

    move/from16 v0, v18

    if-ne v0, v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x2

    if-ne v4, v6, :cond_e

    .line 1670
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 1671
    .local v21, "face_1":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 1673
    .local v22, "face_2":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v41, v0

    .line 1674
    .local v41, "state1":I
    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v42, v0

    .line 1676
    .local v42, "state2":I
    if-nez v41, :cond_e

    if-nez v42, :cond_e

    .line 1677
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-nez v4, :cond_a

    .line 1678
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getNameTexture(I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v4

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1679
    :cond_a
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-nez v4, :cond_b

    .line 1680
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getNameTexture(I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v4

    move-object/from16 v0, v22

    iput-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1682
    :cond_b
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-eqz v4, :cond_e

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-eqz v4, :cond_e

    .line 1683
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v38

    .line 1684
    .local v38, "size1":I
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v39

    .line 1686
    .local v39, "size2":I
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v0, v21

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move-object/from16 v0, v21

    iget v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v30

    .line 1687
    .local v30, "position1":Landroid/graphics/Point;
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v0, v22

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move-object/from16 v0, v22

    iget v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v31

    .line 1689
    .local v31, "position2":Landroid/graphics/Point;
    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->x:I

    if-ge v4, v6, :cond_11

    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int v4, v4, v38

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->x:I

    if-le v4, v6, :cond_11

    .line 1690
    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-lt v4, v6, :cond_c

    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-gt v4, v6, :cond_d

    :cond_c
    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-ge v4, v6, :cond_e

    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    move-object/from16 v0, v30

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-le v4, v6, :cond_e

    .line 1692
    :cond_d
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->canShowInUpMode(I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1693
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/16 v6, 0x10

    iput v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    .line 1708
    .end local v21    # "face_1":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .end local v22    # "face_2":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .end local v30    # "position1":Landroid/graphics/Point;
    .end local v31    # "position2":Landroid/graphics/Point;
    .end local v38    # "size1":I
    .end local v39    # "size2":I
    .end local v41    # "state1":I
    .end local v42    # "state2":I
    :cond_e
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    if-eqz v4, :cond_f

    .line 1709
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1712
    :cond_f
    const/4 v15, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v15, v4, :cond_1c

    .line 1713
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    .line 1714
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v40, v0

    .line 1717
    .restart local v40    # "state":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v4, v4, v15

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v4

    if-eqz v4, :cond_17

    .line 1719
    if-nez v40, :cond_10

    .line 1721
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkTagMain(I)Z

    move-result v4

    if-eqz v4, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    if-eqz v4, :cond_14

    .line 1722
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1712
    :cond_10
    :goto_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 1696
    .end local v40    # "state":I
    .restart local v21    # "face_1":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .restart local v22    # "face_2":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .restart local v30    # "position1":Landroid/graphics/Point;
    .restart local v31    # "position2":Landroid/graphics/Point;
    .restart local v38    # "size1":I
    .restart local v39    # "size2":I
    .restart local v41    # "state1":I
    .restart local v42    # "state2":I
    :cond_11
    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->x:I

    if-le v4, v6, :cond_e

    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int v4, v4, v39

    move-object/from16 v0, v30

    iget v6, v0, Landroid/graphics/Point;->x:I

    if-le v4, v6, :cond_e

    .line 1697
    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-lt v4, v6, :cond_12

    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-gt v4, v6, :cond_13

    :cond_12
    move-object/from16 v0, v30

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v31

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-ge v4, v6, :cond_e

    move-object/from16 v0, v31

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    move-object/from16 v0, v30

    iget v6, v0, Landroid/graphics/Point;->y:I

    if-le v4, v6, :cond_e

    .line 1699
    :cond_13
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->canShowInUpMode(I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1700
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/16 v6, 0x10

    iput v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    goto/16 :goto_4

    .line 1726
    .end local v21    # "face_1":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .end local v22    # "face_2":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .end local v30    # "position1":Landroid/graphics/Point;
    .end local v31    # "position2":Landroid/graphics/Point;
    .end local v38    # "size1":I
    .end local v39    # "size2":I
    .end local v41    # "state1":I
    .end local v42    # "state2":I
    .restart local v40    # "state":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-nez v4, :cond_15

    .line 1727
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getNameTexture(I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1730
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move/from16 v2, v40

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v8, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusOnFaceId:I

    if-ne v15, v4, :cond_16

    const/4 v10, 0x1

    :goto_7
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v10}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawFaceTagging(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;Lcom/sec/android/gallery3d/glrenderer/StringTexture;ZIZZ)Landroid/graphics/Rect;

    move-result-object v4

    iput-object v4, v11, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    goto/16 :goto_6

    :cond_16
    const/4 v10, 0x0

    goto :goto_7

    .line 1737
    :cond_17
    const/4 v4, 0x1

    move/from16 v0, v40

    if-ne v0, v4, :cond_10

    .line 1738
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mName:Ljava/lang/String;

    if-nez v4, :cond_18

    .line 1739
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v7, v7, v15

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Face;->getRecommendedId()I

    move-result v7

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mName:Ljava/lang/String;

    .line 1742
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v3, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mName:Ljava/lang/String;

    .line 1743
    .local v3, "name":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1744
    .local v9, "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    if-eqz v3, :cond_1a

    .line 1745
    const v5, -0xecceab

    .line 1746
    .local v5, "color":I
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 1747
    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v45

    .line 1748
    .local v45, "values":[Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v3, v45, v4

    .line 1749
    const-string v4, "Me"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 1750
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0e00c0

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1757
    .end local v45    # "values":[Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_TAG_NAME_TEXT_SIZE:I

    int-to-float v4, v4

    const/4 v6, 0x1

    const-string v7, "a"

    const/16 v8, 0x10

    invoke-static/range {v3 .. v8}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstanceWithAutoFadeOut(Ljava/lang/String;FIZLjava/lang/String;I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v9

    .line 1761
    .end local v5    # "color":I
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v6, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v8

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v11, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusOnFaceId:I

    if-ne v15, v6, :cond_1b

    const/4 v13, 0x1

    :goto_8
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    invoke-direct/range {v6 .. v13}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawFaceTagging(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;Lcom/sec/android/gallery3d/glrenderer/StringTexture;ZIZZ)Landroid/graphics/Rect;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    goto/16 :goto_6

    :cond_1b
    const/4 v13, 0x0

    goto :goto_8

    .line 1769
    .end local v3    # "name":Ljava/lang/String;
    .end local v9    # "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .end local v40    # "state":I
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    if-eqz v4, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_21

    .line 1770
    const/4 v15, 0x0

    move/from16 v25, v15

    .end local v15    # "i":I
    .local v25, "i":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v25

    if-ge v0, v4, :cond_20

    .line 1771
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusFaceTag:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 1772
    .local v26, "index":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    .line 1773
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v40, v0

    .line 1775
    .restart local v40    # "state":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v4, :cond_1e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v4, v4, v26

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v4

    if-eqz v4, :cond_1e

    .line 1777
    if-nez v40, :cond_1e

    .line 1778
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-nez v4, :cond_1d

    .line 1779
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getNameTexture(I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1782
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v6, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move/from16 v2, v40

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v13, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v15, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusOnFaceId:I

    move/from16 v0, v25

    if-ne v0, v6, :cond_1f

    const/16 v17, 0x1

    :goto_a
    move-object/from16 v10, p0

    move-object/from16 v11, p1

    invoke-direct/range {v10 .. v17}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawFaceTagging(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;Lcom/sec/android/gallery3d/glrenderer/StringTexture;ZIZZ)Landroid/graphics/Rect;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupTagRect:Landroid/graphics/Rect;

    .line 1787
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mNameTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-nez v4, :cond_1e

    .line 1788
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v6, 0x2

    iput v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    .line 1770
    :cond_1e
    add-int/lit8 v15, v25, 0x1

    .end local v25    # "i":I
    .restart local v15    # "i":I
    move/from16 v25, v15

    .end local v15    # "i":I
    .restart local v25    # "i":I
    goto/16 :goto_9

    .line 1782
    :cond_1f
    const/16 v17, 0x0

    goto :goto_a

    .end local v26    # "index":I
    .end local v40    # "state":I
    :cond_20
    move/from16 v15, v25

    .line 1795
    .end local v25    # "i":I
    .restart local v15    # "i":I
    :cond_21
    const/4 v15, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v15, v4, :cond_27

    .line 1796
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v33, v0

    .line 1797
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v0, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v40, v0

    .line 1800
    .restart local v40    # "state":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v4, :cond_22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v4, v4

    if-ge v15, v4, :cond_22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v4, v4, v15

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v4

    if-eqz v4, :cond_22

    .line 1802
    const/4 v4, 0x1

    move/from16 v0, v40

    if-ne v0, v4, :cond_25

    .line 1803
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    if-eqz v4, :cond_24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v4, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    array-length v4, v4

    if-lez v4, :cond_24

    .line 1805
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v6, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move/from16 v2, v40

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v15}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawCandidatesList(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;I)[Landroid/graphics/Rect;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    .line 1826
    :cond_22
    :goto_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v4, :cond_26

    if-nez v40, :cond_26

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-ne v15, v4, :cond_26

    .line 1828
    new-instance v19, Landroid/os/Bundle;

    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    .line 1829
    .local v19, "data":Landroid/os/Bundle;
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->fitPopupOnScreen(Landroid/graphics/Rect;)[Landroid/graphics/Point;

    move-result-object v32

    .line 1831
    .local v32, "pt":[Landroid/graphics/Point;
    if-eqz v32, :cond_23

    .line 1832
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 v29, v0

    const/4 v4, 0x0

    const/4 v6, 0x0

    aget-object v6, v32, v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    aput v6, v29, v4

    const/4 v4, 0x1

    const/4 v6, 0x0

    aget-object v6, v32, v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    aput v6, v29, v4

    .line 1833
    .local v29, "pos":[I
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 v24, v0

    const/4 v4, 0x0

    const/4 v6, 0x1

    aget-object v6, v32, v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    aput v6, v24, v4

    const/4 v4, 0x1

    const/4 v6, 0x1

    aget-object v6, v32, v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    aput v6, v24, v4

    .line 1834
    .local v24, "headPos":[I
    const-string v4, "contact_popup_pos"

    move-object/from16 v0, v19

    move-object/from16 v1, v29

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1835
    const-string v4, "head_pos"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1836
    const-string/jumbo v4, "show"

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1838
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v27

    .line 1839
    .local v27, "msg":Landroid/os/Message;
    const/4 v4, 0x0

    move-object/from16 v0, v27

    iput v4, v0, Landroid/os/Message;->what:I

    .line 1840
    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1841
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1795
    .end local v19    # "data":Landroid/os/Bundle;
    .end local v24    # "headPos":[I
    .end local v27    # "msg":Landroid/os/Message;
    .end local v29    # "pos":[I
    .end local v32    # "pt":[Landroid/graphics/Point;
    :cond_23
    :goto_d
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_b

    .line 1810
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v6, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v12

    const/4 v13, 0x2

    const/4 v14, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    invoke-direct/range {v10 .. v15}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopup(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;ILcom/sec/android/gallery3d/glrenderer/StringTexture;I)[Landroid/graphics/Rect;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    goto/16 :goto_c

    .line 1816
    :cond_25
    const/4 v4, 0x2

    move/from16 v0, v40

    if-ne v0, v4, :cond_22

    .line 1817
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v6, v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mTagPosition:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move/from16 v2, v40

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkAvaivablePositionForPopup(Landroid/graphics/Rect;II)Landroid/graphics/Point;

    move-result-object v12

    const/4 v14, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move/from16 v13, v40

    invoke-direct/range {v10 .. v15}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopup(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;ILcom/sec/android/gallery3d/glrenderer/StringTexture;I)[Landroid/graphics/Rect;

    move-result-object v6

    iput-object v6, v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    goto/16 :goto_c

    .line 1843
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v4, :cond_23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-ne v15, v4, :cond_23

    .line 1844
    new-instance v19, Landroid/os/Bundle;

    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    .line 1845
    .restart local v19    # "data":Landroid/os/Bundle;
    const-string/jumbo v4, "show"

    const/4 v6, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1846
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v27

    .line 1847
    .restart local v27    # "msg":Landroid/os/Message;
    const/4 v4, 0x0

    move-object/from16 v0, v27

    iput v4, v0, Landroid/os/Message;->what:I

    .line 1848
    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1849
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_d

    .line 1852
    .end local v19    # "data":Landroid/os/Bundle;
    .end local v27    # "msg":Landroid/os/Message;
    .end local v40    # "state":I
    :cond_27
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_0

    .line 1853
    new-instance v19, Landroid/os/Bundle;

    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    .line 1854
    .restart local v19    # "data":Landroid/os/Bundle;
    const-string/jumbo v4, "show"

    const/4 v6, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1855
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v27

    .line 1856
    .restart local v27    # "msg":Landroid/os/Message;
    const/4 v4, 0x0

    move-object/from16 v0, v27

    iput v4, v0, Landroid/os/Message;->what:I

    .line 1857
    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1858
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method

.method private drawFaceTagging(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;Lcom/sec/android/gallery3d/glrenderer/StringTexture;ZIZZ)Landroid/graphics/Rect;
    .locals 28
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "startPt"    # Landroid/graphics/Point;
    .param p3, "strTex"    # Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .param p4, "main"    # Z
    .param p5, "position"    # I
    .param p6, "hasMark"    # Z
    .param p7, "hasGenericFocus"    # Z

    .prologue
    .line 1185
    if-nez p3, :cond_1

    .line 1186
    const/16 v25, 0x0

    .line 1294
    :cond_0
    :goto_0
    return-object v25

    .line 1188
    :cond_1
    new-instance v25, Landroid/graphics/Rect;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Rect;-><init>()V

    .line 1189
    .local v25, "rt":Landroid/graphics/Rect;
    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Point;->x:I

    .line 1190
    .local v4, "left":I
    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Point;->y:I

    .line 1192
    .local v5, "top":I
    if-eqz p6, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMarkTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v19

    .line 1194
    .local v19, "markStrWidth":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLanguage:Ljava/lang/String;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLanguage:Ljava/lang/String;

    const-string v8, "es"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1195
    mul-int/lit8 v19, v19, 0x2

    .line 1198
    :cond_2
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceWidth:I

    .line 1199
    .local v6, "resourceWidth":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v8, 0x7f0d03d3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 1200
    .local v7, "resourceHeight":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v8, 0x7f0d03ce

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    .line 1201
    .local v20, "offX":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v8, 0x7f0d03cf

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 1202
    .local v22, "offYY":I
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v3

    add-int v3, v3, v19

    add-int v3, v3, v20

    if-le v3, v6, :cond_3

    .line 1203
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v3

    add-int v3, v3, v19

    add-int v6, v3, v20

    .line 1205
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    if-le v6, v3, :cond_4

    .line 1206
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    .line 1208
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    if-le v7, v3, :cond_5

    .line 1209
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    .line 1211
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkNameTagROrL(II)I

    move-result v23

    .line 1212
    .local v23, "pos":I
    or-int v23, v23, p5

    .line 1214
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v8, 0x7f0d03d0

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 1215
    .local v16, "headLeftMargin":I
    const/4 v2, 0x0

    .line 1217
    .local v2, "tx":Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;
    div-int/lit8 v3, v7, 0x2

    add-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    if-le v3, v8, :cond_6

    .line 1218
    or-int/lit8 v23, v23, 0x10

    .line 1220
    :cond_6
    if-nez v23, :cond_12

    .line 1221
    if-eqz p4, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 1222
    :goto_2
    sub-int v4, v4, v16

    .line 1240
    :cond_7
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v17

    .line 1241
    .local v17, "imageBound":Landroid/graphics/Rect;
    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Rect;->left:I

    const/4 v8, 0x0

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v0, v3

    move/from16 v18, v0

    .line 1242
    .local v18, "leftMin":F
    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Rect;->top:I

    const/4 v8, 0x0

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v0, v3

    move/from16 v26, v0

    .line 1243
    .local v26, "topMin":F
    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v0, v3

    move/from16 v24, v0

    .line 1244
    .local v24, "rightMax":F
    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v15, v3

    .line 1246
    .local v15, "bottomMax":F
    int-to-float v3, v4

    cmpg-float v3, v3, v18

    if-gez v3, :cond_18

    .line 1247
    move/from16 v0, v18

    float-to-int v4, v0

    .line 1251
    :cond_8
    :goto_4
    int-to-float v3, v5

    cmpg-float v3, v3, v26

    if-gez v3, :cond_19

    .line 1252
    move/from16 v0, v26

    float-to-int v5, v0

    .line 1256
    :cond_9
    :goto_5
    add-int v3, v5, v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    if-le v3, v8, :cond_a

    .line 1257
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    sub-int v5, v3, v7

    .line 1259
    :cond_a
    if-eqz v2, :cond_c

    if-eqz p3, :cond_c

    move-object/from16 v3, p1

    .line 1260
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1262
    const/4 v14, 0x0

    .line 1263
    .local v14, "arrowHeight":I
    and-int/lit8 v3, v23, 0x10

    if-nez v3, :cond_b

    .line 1264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v8, 0x7f0d03d2

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 1266
    :cond_b
    add-int v3, v5, v14

    add-int v8, v4, v6

    add-int v9, v5, v7

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v3, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 1269
    .end local v14    # "arrowHeight":I
    :cond_c
    if-eqz p3, :cond_0

    .line 1270
    sub-int v3, v7, v22

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v8

    sub-int/2addr v3, v8

    div-int/lit8 v21, v3, 0x2

    .line 1271
    .local v21, "offY":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v8, 0x7f0d03d1

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v27

    .line 1272
    .local v27, "upPosOffY":I
    and-int/lit8 v3, v23, 0x10

    if-nez v3, :cond_d

    .line 1273
    add-int v3, v22, v27

    add-int v21, v21, v3

    .line 1275
    :cond_d
    if-eqz p6, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLanguage:Ljava/lang/String;

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLanguage:Ljava/lang/String;

    const-string v8, "es"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMarkTextureForES:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v8

    sub-int v8, v6, v8

    sub-int v8, v8, v19

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v4

    add-int v9, v5, v21

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v8, v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1277
    const/16 v19, 0x0

    .line 1279
    :cond_e
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v3

    sub-int v3, v6, v3

    sub-int v3, v3, v19

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v4

    add-int v8, v5, v21

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3, v8}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1286
    if-eqz p6, :cond_f

    .line 1287
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMarkTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v8

    add-int/2addr v8, v6

    sub-int v8, v8, v19

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v4

    add-int v9, v5, v21

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v8, v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1289
    :cond_f
    if-eqz p7, :cond_0

    .line 1290
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    add-int/lit8 v10, v4, -0x2

    add-int/lit8 v11, v5, -0x2

    add-int/lit8 v12, v6, 0x4

    move-object/from16 v9, p1

    move v13, v7

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_0

    .line 1192
    .end local v2    # "tx":Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;
    .end local v6    # "resourceWidth":I
    .end local v7    # "resourceHeight":I
    .end local v15    # "bottomMax":F
    .end local v16    # "headLeftMargin":I
    .end local v17    # "imageBound":Landroid/graphics/Rect;
    .end local v18    # "leftMin":F
    .end local v19    # "markStrWidth":I
    .end local v20    # "offX":I
    .end local v21    # "offY":I
    .end local v22    # "offYY":I
    .end local v23    # "pos":I
    .end local v24    # "rightMax":F
    .end local v26    # "topMin":F
    .end local v27    # "upPosOffY":I
    :cond_10
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 1221
    .restart local v2    # "tx":Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;
    .restart local v6    # "resourceWidth":I
    .restart local v7    # "resourceHeight":I
    .restart local v16    # "headLeftMargin":I
    .restart local v19    # "markStrWidth":I
    .restart local v20    # "offX":I
    .restart local v22    # "offYY":I
    .restart local v23    # "pos":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedDR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    goto/16 :goto_2

    .line 1224
    :cond_12
    const/16 v3, 0x10

    move/from16 v0, v23

    if-ne v0, v3, :cond_14

    .line 1225
    if-eqz p4, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedUR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 1226
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceHeight:I

    sub-int/2addr v5, v3

    .line 1227
    sub-int v4, v4, v16

    goto/16 :goto_3

    .line 1225
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedUR:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    goto :goto_6

    .line 1229
    :cond_14
    const/4 v3, 0x1

    move/from16 v0, v23

    if-ne v0, v3, :cond_16

    .line 1230
    if-eqz p4, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedDL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 1231
    :goto_7
    sub-int v3, v6, v16

    sub-int/2addr v4, v3

    goto/16 :goto_3

    .line 1230
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedDL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    goto :goto_7

    .line 1233
    :cond_16
    const/16 v3, 0x11

    move/from16 v0, v23

    if-ne v0, v3, :cond_7

    .line 1234
    if-eqz p4, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainConfirmedUL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 1235
    :goto_8
    sub-int v3, v6, v16

    sub-int/2addr v4, v3

    .line 1236
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mResourceHeight:I

    sub-int/2addr v5, v3

    goto/16 :goto_3

    .line 1234
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSubConfirmedUL:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    goto :goto_8

    .line 1248
    .restart local v15    # "bottomMax":F
    .restart local v17    # "imageBound":Landroid/graphics/Rect;
    .restart local v18    # "leftMin":F
    .restart local v24    # "rightMax":F
    .restart local v26    # "topMin":F
    :cond_18
    int-to-float v3, v4

    int-to-float v8, v6

    sub-float v8, v24, v8

    cmpl-float v3, v3, v8

    if-lez v3, :cond_8

    .line 1249
    move/from16 v0, v24

    float-to-int v3, v0

    sub-int v4, v3, v6

    goto/16 :goto_4

    .line 1253
    :cond_19
    int-to-float v3, v5

    int-to-float v8, v7

    sub-float v8, v15, v8

    cmpl-float v3, v3, v8

    if-lez v3, :cond_9

    .line 1254
    float-to-int v3, v15

    sub-int v5, v3, v7

    goto/16 :goto_5
.end method

.method private drawPopup(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Point;ILcom/sec/android/gallery3d/glrenderer/StringTexture;I)[Landroid/graphics/Rect;
    .locals 28
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "startPt"    # Landroid/graphics/Point;
    .param p3, "state"    # I
    .param p4, "strTex"    # Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .param p5, "index"    # I

    .prologue
    .line 1302
    const/16 v27, 0x0

    .line 1304
    .local v27, "tempRect":[Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v0, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    move/from16 v25, v0

    .line 1305
    .local v25, "focus":I
    packed-switch p3, :pswitch_data_0

    .line 1472
    :cond_0
    :goto_0
    :pswitch_0
    return-object v27

    .line 1307
    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-nez v2, :cond_0

    goto :goto_0

    .line 1314
    :pswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAddTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    .line 1316
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    sub-int/2addr v2, v3

    div-int/lit8 v4, v2, 0x2

    .line 1317
    .local v4, "left":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsTablet:Z

    if-eqz v2, :cond_1

    .line 1318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->right:I

    .line 1319
    if-gez v4, :cond_4

    .line 1320
    const/4 v4, 0x0

    .line 1324
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->LEFT_ALIGN_FACE_POPUP_ITEM_TEXT:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v26, v2, v3

    .line 1325
    .local v26, "maxWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00c0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_TEXT_SIZE:I

    int-to-float v3, v3

    sget v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_TEXT:I

    const/4 v8, 0x0

    move/from16 v0, v26

    invoke-static {v2, v3, v6, v8, v0}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v12

    .line 1327
    .local v12, "meTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00b0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_TEXT_SIZE:I

    int-to-float v3, v3

    sget v6, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_TEXT:I

    move/from16 v0, v26

    int-to-float v8, v0

    const/4 v9, 0x0

    invoke-static {v2, v3, v6, v8, v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAddTexture:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1331
    .local v23, "addTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    .line 1332
    .local v24, "extraHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 1333
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    div-int/lit8 v5, v2, 0x2

    .line 1334
    .local v5, "top":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    mul-int/lit8 v2, v2, 0x2

    add-int v2, v2, v24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v6, 0x7f0d03c9

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int v7, v2, v3

    .line 1337
    .local v7, "menuHeight":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsTablet:Z

    if-eqz v2, :cond_2

    .line 1338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    sub-int v5, v3, v2

    .line 1340
    if-gez v5, :cond_5

    .line 1341
    const/4 v5, 0x0

    .line 1364
    :cond_2
    :goto_2
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/graphics/Rect;

    move-object/from16 v27, v0

    .line 1365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPopupItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1373
    const/4 v2, 0x1

    move/from16 v0, v25

    if-le v0, v2, :cond_3

    .line 1374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/16 v25, 0x1

    move/from16 v0, v25

    iput v0, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 1376
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v5, v2

    .line 1377
    const/4 v2, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    if-nez v3, :cond_6

    const/4 v14, 0x1

    :goto_3
    if-nez v25, :cond_7

    const/4 v15, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    if-nez v3, :cond_8

    const/16 v16, 0x1

    :goto_5
    const/16 v17, -0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v10, v4

    move v11, v5

    invoke-direct/range {v8 .. v17}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopupItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILcom/sec/android/gallery3d/glrenderer/StringTexture;ZZZZI)Landroid/graphics/Rect;

    move-result-object v3

    aput-object v3, v27, v2

    .line 1378
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v6, 0x7f0d03c9

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v5, v2

    .line 1379
    const/4 v2, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_9

    const/16 v19, 0x1

    :goto_6
    const/4 v3, 0x1

    move/from16 v0, v25

    if-ne v0, v3, :cond_a

    const/16 v20, 0x1

    :goto_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_b

    const/16 v21, 0x1

    :goto_8
    const/16 v22, 0x1

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move v15, v4

    move/from16 v16, v5

    move-object/from16 v17, v23

    invoke-direct/range {v13 .. v22}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopupItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILcom/sec/android/gallery3d/glrenderer/StringTexture;ZZZZI)Landroid/graphics/Rect;

    move-result-object v3

    aput-object v3, v27, v2

    goto/16 :goto_0

    .line 1321
    .end local v5    # "top":I
    .end local v7    # "menuHeight":I
    .end local v12    # "meTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .end local v23    # "addTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .end local v24    # "extraHeight":I
    .end local v26    # "maxWidth":I
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    if-le v2, v3, :cond_1

    .line 1322
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    sub-int v4, v2, v3

    goto/16 :goto_1

    .line 1342
    .restart local v5    # "top":I
    .restart local v7    # "menuHeight":I
    .restart local v12    # "meTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .restart local v23    # "addTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .restart local v24    # "extraHeight":I
    .restart local v26    # "maxWidth":I
    :cond_5
    add-int v2, v5, v7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    if-le v2, v3, :cond_2

    .line 1343
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    sub-int v5, v2, v7

    goto/16 :goto_2

    .line 1377
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_3

    :cond_7
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_8
    const/16 v16, 0x0

    goto :goto_5

    .line 1379
    :cond_9
    const/16 v19, 0x0

    goto :goto_6

    :cond_a
    const/16 v20, 0x0

    goto :goto_7

    :cond_b
    const/16 v21, 0x0

    goto :goto_8

    .line 1388
    .end local v5    # "top":I
    .end local v7    # "menuHeight":I
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    sub-int/2addr v2, v3

    div-int/lit8 v5, v2, 0x2

    .line 1389
    .restart local v5    # "top":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v7, v2, v24

    .line 1390
    .restart local v7    # "menuHeight":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsTablet:Z

    if-eqz v2, :cond_d

    .line 1391
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    sub-int v5, v3, v2

    .line 1393
    if-gez v5, :cond_f

    .line 1394
    const/4 v5, 0x0

    .line 1417
    :cond_d
    :goto_9
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/graphics/Rect;

    move-object/from16 v27, v0

    .line 1418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPopupItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1427
    if-lez v25, :cond_e

    .line 1428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/16 v25, 0x0

    move/from16 v0, v25

    iput v0, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 1430
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v5, v2

    .line 1431
    const/4 v2, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    if-nez v3, :cond_10

    const/16 v19, 0x1

    :goto_a
    if-nez v25, :cond_11

    const/16 v20, 0x1

    :goto_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    if-nez v3, :cond_12

    const/16 v21, 0x1

    :goto_c
    const/16 v22, 0x0

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move v15, v4

    move/from16 v16, v5

    move-object/from16 v17, v23

    invoke-direct/range {v13 .. v22}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawPopupItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILcom/sec/android/gallery3d/glrenderer/StringTexture;ZZZZI)Landroid/graphics/Rect;

    move-result-object v3

    aput-object v3, v27, v2

    goto/16 :goto_0

    .line 1395
    :cond_f
    add-int v2, v5, v7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    if-le v2, v3, :cond_d

    .line 1396
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    sub-int v5, v2, v7

    goto :goto_9

    .line 1431
    :cond_10
    const/16 v19, 0x0

    goto :goto_a

    :cond_11
    const/16 v20, 0x0

    goto :goto_b

    :cond_12
    const/16 v21, 0x0

    goto :goto_c

    .line 1305
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private drawPopupItem(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILcom/sec/android/gallery3d/glrenderer/StringTexture;ZZZZI)Landroid/graphics/Rect;
    .locals 28
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "strTex"    # Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .param p5, "darwDivide"    # Z
    .param p6, "isPressed"    # Z
    .param p7, "hasFocus"    # Z
    .param p8, "genericFocus"    # Z
    .param p9, "position"    # I

    .prologue
    .line 1093
    new-instance v22, Landroid/graphics/Rect;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Rect;-><init>()V

    .line 1094
    .local v22, "itemRect":Landroid/graphics/Rect;
    if-nez p4, :cond_0

    move-object/from16 v23, v22

    .line 1171
    .end local v22    # "itemRect":Landroid/graphics/Rect;
    .local v23, "itemRect":Ljava/lang/Object;
    :goto_0
    return-object v23

    .line 1098
    .end local v23    # "itemRect":Ljava/lang/Object;
    .restart local v22    # "itemRect":Landroid/graphics/Rect;
    :cond_0
    new-instance v9, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v9}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    .line 1099
    .local v9, "paint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    sget v4, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->COLOR_FACE_POPUP_ITEM_LINE:I

    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 1100
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03c9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 1101
    .local v18, "divideLineThickness":I
    move/from16 v0, v18

    int-to-float v4, v0

    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 1102
    const v4, 0x3f19999a    # 0.6f

    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setAlpha(F)V

    .line 1104
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03ca

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 1105
    .local v17, "divideLeftMargin":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03cb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 1108
    .local v19, "divideRightMargin":I
    if-nez p5, :cond_4

    .line 1113
    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    invoke-virtual/range {p4 .. p4}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMaxPopItemWidth:I

    .line 1121
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03c3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 1122
    .local v21, "itemLeftMargin":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03c4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    .line 1125
    .local v24, "itemRightMargin":I
    if-eqz p6, :cond_1

    .line 1126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03c5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    .line 1127
    .local v25, "offY":I
    const/4 v4, -0x1

    move/from16 v0, p9

    if-ne v0, v4, :cond_5

    .line 1128
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    sub-int v13, p3, v25

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v15, v4, v25

    move-object/from16 v11, p1

    move/from16 v12, p2

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 1141
    .end local v25    # "offY":I
    :cond_1
    :goto_2
    new-instance v22, Landroid/graphics/Rect;

    .end local v22    # "itemRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    add-int v4, v4, p2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v5, v5, p3

    move-object/from16 v0, v22

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1145
    .restart local v22    # "itemRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f0d03cc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    .line 1146
    .local v20, "fixOffY":I
    add-int v4, p3, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    invoke-virtual/range {p4 .. p4}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    add-int v27, v4, v20

    .line 1154
    .local v27, "strTop":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    add-int v4, v4, p2

    invoke-virtual/range {p4 .. p4}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->LEFT_ALIGN_FACE_POPUP_ITEM_TEXT:I

    sub-int v26, v4, v5

    .line 1155
    .local v26, "rightAlign":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v16

    .line 1157
    .local v16, "currentLanguage":Ljava/lang/String;
    const-string v4, "ar"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "iw"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "he"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1158
    :cond_2
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1166
    :goto_3
    if-eqz p7, :cond_9

    .line 1167
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFocusItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    move-object/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p3

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    :cond_3
    :goto_4
    move-object/from16 v23, v22

    .line 1171
    .restart local v23    # "itemRect":Ljava/lang/Object;
    goto/16 :goto_0

    .line 1109
    .end local v16    # "currentLanguage":Ljava/lang/String;
    .end local v20    # "fixOffY":I
    .end local v21    # "itemLeftMargin":I
    .end local v23    # "itemRect":Ljava/lang/Object;
    .end local v24    # "itemRightMargin":I
    .end local v26    # "rightAlign":I
    .end local v27    # "strTop":I
    :cond_4
    add-int v4, p2, v17

    int-to-float v5, v4

    move/from16 v0, p3

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    add-int v4, v4, p2

    sub-int v4, v4, v19

    int-to-float v7, v4

    move/from16 v0, p3

    int-to-float v8, v0

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    goto/16 :goto_1

    .line 1130
    .restart local v21    # "itemLeftMargin":I
    .restart local v24    # "itemRightMargin":I
    .restart local v25    # "offY":I
    :cond_5
    const/4 v4, 0x1

    move/from16 v0, p9

    if-ne v0, v4, :cond_6

    .line 1131
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    sub-int v13, p3, v25

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v15, v4, v25

    move-object/from16 v11, p1

    move/from16 v12, p2

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_2

    .line 1133
    :cond_6
    if-nez p9, :cond_7

    .line 1134
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    sub-int v13, p3, v25

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v15, v4, v25

    move-object/from16 v11, p1

    move/from16 v12, p2

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_2

    .line 1137
    :cond_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    add-int v15, v4, v25

    move-object/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p3

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_2

    .line 1161
    .end local v25    # "offY":I
    .restart local v16    # "currentLanguage":Ljava/lang/String;
    .restart local v20    # "fixOffY":I
    .restart local v26    # "rightAlign":I
    .restart local v27    # "strTop":I
    :cond_8
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->LEFT_ALIGN_FACE_POPUP_ITEM_TEXT:I

    add-int v4, v4, p2

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, v27

    invoke-virtual {v0, v1, v4, v2}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto/16 :goto_3

    .line 1168
    :cond_9
    if-eqz p8, :cond_3

    .line 1169
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusItem:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_POPUP_MENU_WIDTH:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->HEIGHT_FACE_POPUP_ITEM_TEXT:I

    move-object/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p3

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_4
.end method

.method private fitPopupOnScreen(Landroid/graphics/Rect;)[Landroid/graphics/Point;
    .locals 20
    .param p1, "faceRectOnScreen"    # Landroid/graphics/Rect;

    .prologue
    .line 2161
    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11}, Landroid/graphics/Point;-><init>()V

    .line 2162
    .local v11, "pt":Landroid/graphics/Point;
    new-instance v12, Landroid/graphics/Point;

    invoke-direct {v12}, Landroid/graphics/Point;-><init>()V

    .line 2163
    .local v12, "ptHead":Landroid/graphics/Point;
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v13, v0, [Landroid/graphics/Point;

    const/16 v17, 0x0

    aput-object v11, v13, v17

    const/16 v17, 0x1

    aput-object v12, v13, v17

    .line 2165
    .local v13, "pts":[Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    .line 2167
    .local v3, "d_w":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0d03d4

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 2168
    .local v10, "popup_w":I
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v17, :cond_0

    .line 2169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0d03d5

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 2170
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/ContactPopup;->withSNS()Z

    move-result v17

    if-eqz v17, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0d03d7

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 2172
    .local v9, "popup_h":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0d03d8

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2173
    .local v5, "head_w":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v4, v17, v18

    .line 2174
    .local v4, "head_l":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f0d03db

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    sub-int v15, v17, v18

    .line 2177
    .local v15, "t":I
    sub-int v17, v3, v10

    div-int/lit8 v7, v17, 0x2

    .line 2178
    .local v7, "l":I
    div-int/lit8 v17, v5, 0x2

    sub-int v17, v4, v17

    move/from16 v0, v17

    if-le v7, v0, :cond_6

    .line 2179
    div-int/lit8 v17, v5, 0x2

    sub-int v7, v4, v17

    .line 2184
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 2185
    .local v6, "imageBound":Landroid/graphics/Rect;
    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v17

    move/from16 v0, v17

    int-to-float v8, v0

    .line 2186
    .local v8, "leftMin":F
    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v16, v0

    .line 2187
    .local v16, "topMin":F
    iget v0, v6, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v17

    move/from16 v0, v17

    int-to-float v14, v0

    .line 2188
    .local v14, "rightMax":F
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v17

    move/from16 v0, v17

    int-to-float v2, v0

    .line 2189
    .local v2, "bottomMax":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v17

    if-eqz v17, :cond_2

    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-nez v17, :cond_2

    .line 2190
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v17

    move/from16 v0, v17

    int-to-float v2, v0

    .line 2193
    :cond_2
    int-to-float v0, v7

    move/from16 v17, v0

    cmpg-float v17, v17, v8

    if-gez v17, :cond_7

    .line 2194
    float-to-int v7, v8

    .line 2198
    :cond_3
    :goto_2
    int-to-float v0, v15

    move/from16 v17, v0

    cmpg-float v17, v17, v16

    if-gez v17, :cond_8

    .line 2199
    move/from16 v0, v16

    float-to-int v15, v0

    .line 2203
    :cond_4
    :goto_3
    invoke-virtual {v12, v4, v15}, Landroid/graphics/Point;->set(II)V

    .line 2204
    invoke-virtual {v11, v7, v15}, Landroid/graphics/Point;->set(II)V

    .line 2206
    return-object v13

    .line 2170
    .end local v2    # "bottomMax":F
    .end local v4    # "head_l":I
    .end local v5    # "head_w":I
    .end local v6    # "imageBound":Landroid/graphics/Rect;
    .end local v7    # "l":I
    .end local v8    # "leftMin":F
    .end local v9    # "popup_h":I
    .end local v14    # "rightMax":F
    .end local v15    # "t":I
    .end local v16    # "topMin":F
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRes:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0d03d6

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    goto/16 :goto_0

    .line 2180
    .restart local v4    # "head_l":I
    .restart local v5    # "head_w":I
    .restart local v7    # "l":I
    .restart local v9    # "popup_h":I
    .restart local v15    # "t":I
    :cond_6
    add-int v17, v7, v10

    mul-int/lit8 v18, v5, 0x3

    div-int/lit8 v18, v18, 0x2

    add-int v18, v18, v4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    .line 2181
    sub-int v17, v4, v10

    mul-int/lit8 v18, v5, 0x3

    div-int/lit8 v18, v18, 0x2

    add-int v7, v17, v18

    goto/16 :goto_1

    .line 2195
    .restart local v2    # "bottomMax":F
    .restart local v6    # "imageBound":Landroid/graphics/Rect;
    .restart local v8    # "leftMin":F
    .restart local v14    # "rightMax":F
    .restart local v16    # "topMin":F
    :cond_7
    add-int v17, v7, v10

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpl-float v17, v17, v14

    if-lez v17, :cond_3

    .line 2196
    float-to-int v0, v14

    move/from16 v17, v0

    sub-int v7, v17, v10

    goto :goto_2

    .line 2200
    :cond_8
    add-int v17, v15, v9

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpl-float v17, v17, v2

    if-lez v17, :cond_4

    .line 2201
    float-to-int v0, v2

    move/from16 v17, v0

    sub-int v15, v17, v9

    goto :goto_3
.end method

.method private getFaceRectOnScreen(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 16
    .param p1, "rtOnMedia"    # Landroid/graphics/Rect;
    .param p2, "imageBounds"    # Landroid/graphics/Rect;

    .prologue
    .line 1064
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1066
    .local v5, "faceRT":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaWidth:I

    int-to-float v9, v9

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    div-float v8, v9, v10

    .line 1067
    .local v8, "ratio":F
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    move-object/from16 v0, p1

    iget v10, v0, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    div-float/2addr v10, v8

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->left:I

    .line 1068
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    move-object/from16 v0, p1

    iget v10, v0, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    div-float/2addr v10, v8

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->top:I

    .line 1069
    iget v9, v5, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v10, v8

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->right:I

    .line 1070
    iget v9, v5, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v10, v8

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->bottom:I

    .line 1072
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCircleFace:Z

    if-eqz v9, :cond_0

    .line 1074
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-double v10, v9

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-double v12, v9

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    add-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float v4, v9, v10

    .line 1075
    .local v4, "circumscribedRadius":F
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v9

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float v7, v9, v10

    .line 1076
    .local v7, "inscribedRadius":F
    add-float v9, v4, v7

    const/high16 v10, 0x40000000    # 2.0f

    div-float v6, v9, v10

    .line 1077
    .local v6, "halfRaius":F
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v9

    int-to-float v2, v9

    .line 1078
    .local v2, "centerX":F
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    int-to-float v3, v9

    .line 1079
    .local v3, "centerY":F
    sub-float v9, v2, v6

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->left:I

    .line 1080
    sub-float v9, v3, v6

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->top:I

    .line 1081
    add-float v9, v2, v6

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->right:I

    .line 1082
    add-float v9, v3, v6

    float-to-int v9, v9

    iput v9, v5, Landroid/graphics/Rect;->bottom:I

    .line 1084
    .end local v2    # "centerX":F
    .end local v3    # "centerY":F
    .end local v4    # "circumscribedRadius":F
    .end local v6    # "halfRaius":F
    .end local v7    # "inscribedRadius":F
    :cond_0
    return-object v5
.end method

.method private getLatestSnsData(Ljava/util/ArrayList;)Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;",
            ">;)",
            "Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;"
        }
    .end annotation

    .prologue
    .line 796
    .local p1, "snsDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 797
    :cond_0
    new-instance v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    invoke-direct {v7}, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;-><init>()V

    .line 808
    :goto_0
    return-object v7

    .line 798
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->prepareAccountsInfo(Ljava/util/ArrayList;)V

    .line 799
    const-wide/16 v4, 0x0

    .line 800
    .local v4, "latestTime":J
    const/4 v3, 0x0

    .line 801
    .local v3, "latestIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .local v6, "n":I
    :goto_1
    if-ge v2, v6, :cond_3

    .line 802
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    iget-object v7, v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mTime:Ljava/lang/String;

    const-wide/16 v8, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->parseDate(Ljava/lang/String;J)J

    move-result-wide v0

    .line 803
    .local v0, "curTime":J
    cmp-long v7, v0, v4

    if-lez v7, :cond_2

    .line 804
    move-wide v4, v0

    .line 805
    move v3, v2

    .line 801
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 808
    .end local v0    # "curTime":J
    :cond_3
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    goto :goto_0
.end method

.method private getListItemName([II)Ljava/lang/String;
    .locals 5
    .param p1, "ids"    # [I
    .param p2, "listIndex"    # I

    .prologue
    .line 1962
    const/4 v1, 0x0

    .line 1963
    .local v1, "itemName":Ljava/lang/String;
    array-length v0, p1

    .line 1964
    .local v0, "cnt":I
    if-lez v0, :cond_0

    if-ltz p2, :cond_0

    if-ge p2, v0, :cond_0

    .line 1965
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    if-eqz v3, :cond_1

    if-nez p2, :cond_1

    .line 1966
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e00c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1972
    :cond_0
    :goto_0
    return-object v1

    .line 1968
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    aget v4, p1, p2

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 1969
    .local v2, "name":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->splitDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getNameTexture(I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 1521
    const/4 v6, 0x0

    .line 1522
    .local v6, "strTex":Lcom/sec/android/gallery3d/glrenderer/StringTexture;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getPersonName(I)Ljava/lang/String;

    move-result-object v0

    .line 1523
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1524
    const v2, -0xecceab

    .line 1525
    .local v2, "color":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->FACE_TAG_NAME_TEXT_SIZE:I

    int-to-float v1, v1

    const/4 v3, 0x1

    const-string v4, "a"

    const/16 v5, 0x10

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstanceWithAutoFadeOut(Ljava/lang/String;FIZLjava/lang/String;I)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v6

    .line 1528
    .end local v2    # "color":I
    :cond_0
    return-object v6
.end method

.method private getPersonName(I)Ljava/lang/String;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 1503
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1504
    .local v1, "personID":I
    const/4 v0, 0x0

    .line 1506
    .local v0, "name":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1507
    if-eqz v0, :cond_0

    const-string v3, "profile/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1508
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "profile/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e00c0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1511
    :cond_0
    if-eqz v0, :cond_1

    .line 1512
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1513
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1514
    .local v2, "values":[Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v0, v2, v3

    .line 1517
    .end local v2    # "values":[Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private getPopupFaceIndex()I
    .locals 5

    .prologue
    .line 2002
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 2003
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 2004
    .local v0, "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    if-eqz v0, :cond_1

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v3, v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v3

    if-eqz v3, :cond_1

    .line 2006
    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 2013
    .end local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 2002
    .restart local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .restart local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2013
    .end local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_2
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private isMe()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 566
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    if-eqz v4, :cond_0

    .line 567
    const-string v4, "FaceIndicatorView"

    const-string v5, "enter checkContactData"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-gez v4, :cond_1

    .line 589
    :cond_0
    :goto_0
    return v3

    .line 572
    :cond_1
    const/4 v0, 0x0

    .line 574
    .local v0, "curName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v4, :cond_0

    .line 577
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    iget v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    aget-object v1, v4, v5

    .line 578
    .local v1, "face":Lcom/sec/android/gallery3d/data/Face;
    if-eqz v1, :cond_0

    .line 582
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 583
    .local v2, "pID":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 585
    if-eqz v0, :cond_0

    const-string v4, "profile/Me"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 586
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isNoFeatureFace(I)Z
    .locals 11
    .param p1, "faceId"    # I

    .prologue
    const/4 v10, 0x0

    .line 2964
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2966
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->RAW_SQL_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "main"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 2968
    .local v6, "RAW_SQL_MAIN":Landroid/net/Uri;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select face_data from faces where _id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2970
    .local v9, "sqlString":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2973
    .local v1, "sqlUri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 2974
    .local v8, "faceData":I
    const/4 v7, 0x0

    .line 2976
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2977
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2978
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 2981
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 2984
    if-gez v8, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    .line 2981
    :catchall_0
    move-exception v2

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    :cond_1
    move v2, v10

    .line 2984
    goto :goto_0
.end method

.method private loadCandidates([I)V
    .locals 6
    .param p1, "faceIds"    # [I

    .prologue
    .line 959
    if-eqz p1, :cond_0

    .line 960
    array-length v0, p1

    .line 961
    .local v0, "cnt":I
    if-lez v0, :cond_0

    .line 962
    new-instance v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;[I)V

    .line 984
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 986
    const-wide/16 v4, 0x5

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 990
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 991
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 992
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 995
    .end local v0    # "cnt":I
    .end local v2    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void

    .line 987
    .restart local v0    # "cnt":I
    .restart local v2    # "thread":Ljava/lang/Thread;
    :catch_0
    move-exception v1

    .line 988
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 992
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method private prepareAccountsInfo(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "snsDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;>;"
    const/4 v4, 0x0

    .line 812
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 813
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountType:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 812
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 818
    :cond_1
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    const-string v3, "<br>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 819
    .local v1, "index":I
    if-lez v1, :cond_2

    .line 820
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    .line 822
    :cond_2
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    const-string v3, "<br/>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 823
    if-lez v1, :cond_0

    .line 824
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    goto :goto_1

    .line 827
    .end local v1    # "index":I
    :cond_3
    return-void
.end method

.method private removeFace(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 2409
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    .line 2410
    invoke-static {p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceList;->remove(Landroid/content/Context;I)V

    .line 2411
    return-void
.end method

.method private setCurFaceId(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2815
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 2816
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    .line 2817
    :cond_0
    return-void
.end method

.method private setFaceUnknown(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 2414
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    .line 2415
    invoke-static {p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknown(Landroid/content/Context;I)V

    .line 2416
    return-void
.end method

.method private setPerson(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "faceId"    # I
    .param p3, "personId"    # I

    .prologue
    .line 2404
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    .line 2405
    invoke-static {p1, p2, p3}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    .line 2406
    return-void
.end method

.method private showAlertDialog(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleId"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 2670
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0046

    new-instance v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$9;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$9;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$8;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 2710
    .local v0, "dialog":Landroid/app/AlertDialog;
    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSetAsCallIdDialog:Landroid/app/AlertDialog;

    .line 2712
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 2713
    return-void
.end method

.method private soundAndVibrator()V
    .locals 4

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mVibrator:Landroid/os/Vibrator;

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 368
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 371
    :cond_1
    return-void
.end method

.method private declared-synchronized updateFace(Lcom/sec/android/gallery3d/data/MediaItem;[Lcom/sec/android/gallery3d/data/Face;)V
    .locals 10
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "faces"    # [Lcom/sec/android/gallery3d/data/Face;

    .prologue
    const/4 v9, 0x1

    .line 879
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 880
    .local v0, "faceInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;>;"
    const/4 v1, 0x0

    .line 881
    .local v1, "faceNum":I
    if-eqz p2, :cond_0

    .line 882
    array-length v1, p2

    .line 885
    :cond_0
    iget v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSavedFaceNum:I

    if-eq v7, v1, :cond_1

    .line 886
    const/4 v7, -0x1

    iput v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 888
    :cond_1
    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mSavedFaceNum:I

    .line 890
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 891
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    .line 892
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 894
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v7, :cond_3

    .line 895
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    .line 898
    :cond_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    if-eqz v7, :cond_4

    .line 899
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 901
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 903
    if-eqz p2, :cond_b

    .line 904
    if-lez v1, :cond_b

    .line 905
    const/4 v4, 0x1

    .line 906
    .local v4, "personId":I
    const/4 v5, 0x1

    .line 908
    .local v5, "recommendId":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_9

    :try_start_2
    aget-object v7, p2, v3

    if-eqz v7, :cond_9

    .line 909
    aget-object v7, p2, v3

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 910
    aget-object v7, p2, v3

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Face;->getRecommendedId()I

    move-result v5

    .line 912
    new-instance v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    aget-object v7, p2, v3

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mImageBounds:Landroid/graphics/Rect;

    invoke-direct {p0, v7, v8}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getFaceRectOnScreen(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct {v2, p0, v7, v8}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Landroid/graphics/Rect;Z)V

    .line 915
    .local v2, "fi":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    if-ne v5, v9, :cond_6

    .line 916
    const/4 v7, 0x2

    iput v7, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    .line 930
    :cond_5
    :goto_1
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 908
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 901
    .end local v2    # "fi":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .end local v3    # "i":I
    .end local v4    # "personId":I
    .end local v5    # "recommendId":I
    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 879
    .end local v0    # "faceInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;>;"
    .end local v1    # "faceNum":I
    :catchall_1
    move-exception v7

    monitor-exit p0

    throw v7

    .line 918
    .restart local v0    # "faceInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;>;"
    .restart local v1    # "faceNum":I
    .restart local v2    # "fi":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    .restart local v3    # "i":I
    .restart local v4    # "personId":I
    .restart local v5    # "recommendId":I
    :cond_6
    if-ne v4, v9, :cond_7

    .line 919
    const/4 v7, 0x1

    :try_start_5
    iput v7, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    .line 920
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    if-eqz v7, :cond_5

    .line 921
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    aget-object v8, p2, v3

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 923
    :cond_7
    const/4 v7, 0x0

    iput v7, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    .line 924
    iget v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-ne v7, v3, :cond_8

    iget-boolean v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v7, :cond_8

    .line 925
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    goto :goto_1

    .line 927
    :cond_8
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    goto :goto_1

    .line 933
    .end local v2    # "fi":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_9
    const/4 v6, 0x0

    .line 934
    .local v6, "unNamedIds":[I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_a

    .line 935
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v6, v7, [I

    .line 936
    const/4 v3, 0x0

    :goto_2
    array-length v7, v6

    if-ge v3, v7, :cond_a

    .line 937
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v6, v3

    .line 936
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 941
    :cond_a
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->loadCandidates([I)V

    .line 945
    .end local v3    # "i":I
    .end local v4    # "personId":I
    .end local v5    # "recommendId":I
    .end local v6    # "unNamedIds":[I
    :cond_b
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    .line 946
    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 947
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "permitPreUpdate"    # Z

    .prologue
    .line 841
    monitor-enter p0

    if-nez p1, :cond_1

    .line 854
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 845
    :cond_1
    :try_start_0
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    new-instance v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 851
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 852
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;->preUpdate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 841
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized updateFaceDataFromResume(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 858
    monitor-enter p0

    if-nez p1, :cond_1

    .line 876
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 862
    :cond_1
    :try_start_0
    instance-of v2, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_0

    .line 866
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    if-eqz v2, :cond_2

    .line 867
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;->preUpdate()V

    .line 870
    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    move-object v2, v0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getIsManualFDResumed()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/LocalImage;->loadFace(Z)[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v1

    .line 871
    .local v1, "faces":[Lcom/sec/android/gallery3d/data/Face;
    invoke-direct {p0, p1, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFace(Lcom/sec/android/gallery3d/data/MediaItem;[Lcom/sec/android/gallery3d/data/Face;)V

    .line 873
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    if-eqz v2, :cond_0

    .line 874
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    invoke-interface {v2, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;->onUpdate(Lcom/sec/android/gallery3d/data/MediaItem;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 858
    .end local v1    # "faces":[Lcom/sec/android/gallery3d/data/Face;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized updateFaceRectList()V
    .locals 7

    .prologue
    .line 1476
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v5, :cond_0

    .line 1477
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v0, v5

    .line 1478
    .local v0, "cnt":I
    if-lez v0, :cond_0

    .line 1480
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 1499
    .end local v0    # "cnt":I
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1483
    .restart local v0    # "cnt":I
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v0, :cond_0

    .line 1484
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 1485
    .local v3, "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v2, v5, v4

    .line 1486
    .local v2, "face":Lcom/sec/android/gallery3d/data/Face;
    if-eqz v3, :cond_2

    if-nez v2, :cond_3

    .line 1483
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1490
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mImageBounds:Landroid/graphics/Rect;

    invoke-direct {p0, v5, v6}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getFaceRectOnScreen(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v5

    iput-object v5, v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1494
    .end local v2    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v3    # "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :catch_0
    move-exception v1

    .line 1495
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1476
    .end local v0    # "cnt":I
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "i":I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method


# virtual methods
.method public addContactPopupView()V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_1

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0175

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContainer:Landroid/view/ViewGroup;

    .line 378
    new-instance v0, Lcom/sec/android/gallery3d/ui/ContactPopup;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    .line 379
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setVisibility(I)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_2

    .line 381
    const-string v0, "FaceIndicatorView"

    const-string v1, "add sns view"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    new-instance v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$2;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setBtnClickedListener(Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;)V

    goto :goto_0
.end method

.method public assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    .prologue
    .line 2349
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 2352
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "START_CONTACT_PICK"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2353
    return-void
.end method

.method public clearCurFace()V
    .locals 1

    .prologue
    .line 2930
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    .line 2931
    return-void
.end method

.method public clickPopupFocus()Z
    .locals 3

    .prologue
    .line 1990
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getPopupFaceIndex()I

    move-result v0

    .line 1991
    .local v0, "faceIndex":I
    if-ltz v0, :cond_0

    .line 1992
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v1, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 1993
    .local v1, "focus":I
    if-ltz v1, :cond_0

    .line 1994
    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->clickPopupItem(II)Z

    move-result v2

    .line 1998
    .end local v1    # "focus":I
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getAssignedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2899
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAssignedName:Ljava/lang/String;

    return-object v0
.end method

.method protected getContactPopupButtonState()Z
    .locals 1

    .prologue
    .line 2934
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_0

    .line 2935
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getButtonState()Z

    move-result v0

    .line 2937
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getCurContactLookupKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3212
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurContactLookupKey:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCurFaceId()I
    .locals 1

    .prologue
    .line 2807
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    return v0
.end method

.method public getIsManualFDResumed()Z
    .locals 1

    .prologue
    .line 3362
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsManualFDResumed:Z

    return v0
.end method

.method public getMediaSetKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2903
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaSetKey:Ljava/lang/String;

    return-object v0
.end method

.method public getNameMenuPopupVisible()Z
    .locals 1

    .prologue
    .line 3221
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    return v0
.end method

.method public getScaledFaceRectForCrop()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 3234
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;

    return-object v0
.end method

.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2265
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 2274
    :cond_0
    :goto_0
    return-void

    .line 2272
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-nez v0, :cond_0

    .line 2273
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setVisibility(I)V

    goto :goto_0
.end method

.method public hideContactPopupView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2941
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_1

    .line 2942
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 2943
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v0, :cond_0

    .line 2944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRedrawContactPopup:Z

    .line 2945
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 2946
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonState(Z)V

    .line 2949
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2950
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2951
    :cond_2
    return-void
.end method

.method public hidePopupMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3225
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 3231
    :cond_0
    :goto_0
    return-void

    .line 3228
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 3229
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    .line 3230
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto :goto_0
.end method

.method public isCurFaceExistName()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 3283
    iget v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-gez v4, :cond_1

    .line 3299
    :cond_0
    :goto_0
    return v3

    .line 3286
    :cond_1
    const/4 v0, 0x0

    .line 3288
    .local v0, "curName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-eqz v4, :cond_0

    .line 3291
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    iget v5, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    aget-object v1, v4, v5

    .line 3292
    .local v1, "face":Lcom/sec/android/gallery3d/data/Face;
    if-eqz v1, :cond_0

    .line 3296
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 3297
    .local v2, "pID":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 3299
    if-eqz v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isFaceIndicatorArea(II)Z
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 3240
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-nez v8, :cond_2

    :cond_0
    move v6, v7

    .line 3274
    :cond_1
    :goto_0
    return v6

    .line 3245
    :cond_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v8, :cond_3

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v8, :cond_3

    .line 3246
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v8, p1, p2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->contains(II)Z

    move-result v8

    if-nez v8, :cond_1

    .line 3252
    :cond_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3253
    .local v0, "cnt":I
    if-lez v0, :cond_6

    .line 3254
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_6

    .line 3255
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 3256
    .local v2, "info":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    iget-object v8, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    invoke-virtual {v8, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-nez v8, :cond_1

    .line 3260
    iget-object v8, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    if-eqz v8, :cond_5

    .line 3261
    iget-object v8, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    array-length v5, v8

    .line 3262
    .local v5, "size":I
    if-lez v5, :cond_5

    .line 3263
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    if-ge v3, v5, :cond_5

    .line 3264
    iget-object v8, v2, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    aget-object v4, v8, v3

    .line 3265
    .local v4, "rt":Landroid/graphics/Rect;
    if-eqz v4, :cond_4

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-nez v8, :cond_1

    .line 3263
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 3254
    .end local v3    # "j":I
    .end local v4    # "rt":Landroid/graphics/Rect;
    .end local v5    # "size":I
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v1    # "i":I
    .end local v2    # "info":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_6
    move v6, v7

    .line 3274
    goto :goto_0
.end method

.method public lockNotify()V
    .locals 2

    .prologue
    .line 950
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 951
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 952
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 953
    monitor-exit v1

    .line 955
    :cond_0
    return-void

    .line 953
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public movePopupFocus(Z)Z
    .locals 6
    .param p1, "toNext"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1976
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getPopupFaceIndex()I

    move-result v1

    .line 1977
    .local v1, "faceIndex":I
    if-ltz v1, :cond_2

    .line 1978
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 1979
    .local v0, "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    iget v5, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    if-eqz p1, :cond_1

    move v2, v3

    :goto_0
    add-int/2addr v2, v5

    iput v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 1980
    iget v2, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    if-gez v2, :cond_0

    .line 1981
    iput v4, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupFocusIndex:I

    .line 1983
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 1986
    .end local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :goto_1
    return v3

    .line 1979
    .restart local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_1
    const/4 v2, -0x1

    goto :goto_0

    .end local v0    # "face":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_2
    move v3, v4

    .line 1986
    goto :goto_1
.end method

.method public nameChanged(Z)V
    .locals 0
    .param p1, "changed"    # Z

    .prologue
    .line 2926
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsChangedFromContact:Z

    .line 2927
    return-void
.end method

.method public onContactsChange()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3303
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_0

    .line 3304
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->showMoreDialog(Z)V

    .line 3305
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 3307
    :cond_0
    return-void
.end method

.method public onGenericMotionEnter(Landroid/view/MotionEvent;)Z
    .locals 24
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1874
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z

    move/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    move/from16 v20, v0

    if-nez v20, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getVisibility()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 1877
    :cond_0
    const/16 v20, 0x0

    .line 1958
    :goto_0
    return v20

    .line 1879
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v20

    move/from16 v0, v20

    float-to-int v4, v0

    .line 1880
    .local v4, "ex":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v20

    move/from16 v0, v20

    float-to-int v5, v0

    .line 1881
    .local v5, "ey":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1882
    .local v6, "faceCnt":I
    const/4 v8, 0x0

    .line 1883
    .local v8, "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_9

    .line 1884
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v6, :cond_8

    .line 1885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    check-cast v8, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 1886
    .restart local v8    # "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    iget-object v0, v8, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    move-object/from16 v20, v0

    if-eqz v20, :cond_7

    iget-boolean v0, v8, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 1887
    iget-object v0, v8, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v4, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkPopupItemClickedIndex([Landroid/graphics/Rect;II)I

    move-result v13

    .line 1888
    .local v13, "index":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-eq v0, v13, :cond_2

    .line 1889
    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    .line 1890
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 1892
    :cond_2
    const/16 v20, -0x1

    move/from16 v0, v20

    if-eq v13, v0, :cond_7

    .line 1893
    iget v9, v8, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    .line 1894
    .local v9, "faceState":I
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v9, v0, :cond_4

    .line 1895
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a1

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1914
    :cond_3
    :goto_2
    const/16 v20, 0x1

    goto :goto_0

    .line 1896
    :cond_4
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v9, v0, :cond_3

    .line 1897
    iget-object v11, v8, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    .line 1898
    .local v11, "ids":[I
    if-eqz v11, :cond_5

    array-length v0, v11

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v13, v0, :cond_5

    .line 1899
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getListItemName([II)Ljava/lang/String;

    move-result-object v15

    .line 1900
    .local v15, "name":Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a0

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v15, v22, v23

    const/16 v23, 0x1

    aput-object v15, v22, v23

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_2

    .line 1902
    .end local v15    # "name":Ljava/lang/String;
    :cond_5
    if-nez v11, :cond_6

    const/4 v14, 0x0

    .line 1903
    .local v14, "len":I
    :goto_3
    sub-int v12, v13, v14

    .line 1904
    .local v12, "idx":I
    packed-switch v12, :pswitch_data_0

    goto :goto_2

    .line 1906
    :pswitch_0
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a1

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_2

    .line 1902
    .end local v12    # "idx":I
    .end local v14    # "len":I
    :cond_6
    array-length v14, v11

    goto :goto_3

    .line 1909
    .restart local v12    # "idx":I
    .restart local v14    # "len":I
    :pswitch_1
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a2

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_2

    .line 1884
    .end local v9    # "faceState":I
    .end local v11    # "ids":[I
    .end local v12    # "idx":I
    .end local v13    # "index":I
    .end local v14    # "len":I
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1918
    :cond_8
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 1920
    .end local v10    # "i":I
    :cond_9
    const/16 v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    .line 1922
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkTopFaceTagClicked(II)I

    move-result v7

    .line 1923
    .local v7, "faceIndex":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusOnFaceId:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-eq v7, v0, :cond_a

    .line 1924
    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusOnFaceId:I

    .line 1925
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 1927
    :cond_a
    if-ltz v7, :cond_d

    .line 1928
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v17, v0

    .line 1929
    .local v17, "state":I
    const/16 v20, 0x1

    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 1930
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a3

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1935
    :cond_b
    :goto_4
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 1931
    :cond_c
    if-nez v17, :cond_b

    .line 1932
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getPersonName(I)Ljava/lang/String;

    move-result-object v15

    .line 1933
    .restart local v15    # "name":Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a5

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v15, v22, v23

    const/16 v23, 0x1

    aput-object v15, v22, v23

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_4

    .line 1938
    .end local v15    # "name":Ljava/lang/String;
    .end local v17    # "state":I
    :cond_d
    const/16 v16, -0x1

    .line 1939
    .local v16, "nearerFaceIndex":I
    const-wide v18, 0x4086800000000000L    # 720.0

    .line 1940
    .local v18, "nearestDistance":D
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_5
    if-ge v10, v6, :cond_f

    .line 1941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 1942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceRectScreen:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v4, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->calculateDistance(Landroid/graphics/Rect;II)D

    move-result-wide v2

    .line 1943
    .local v2, "distance":D
    cmpl-double v20, v18, v2

    if-lez v20, :cond_e

    .line 1944
    move-wide/from16 v18, v2

    .line 1945
    move/from16 v16, v10

    .line 1940
    .end local v2    # "distance":D
    :cond_e
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 1949
    :cond_f
    const/16 v20, -0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_12

    .line 1950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    move/from16 v17, v0

    .line 1951
    .restart local v17    # "state":I
    const/16 v20, 0x2

    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_11

    .line 1952
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a4

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1956
    :cond_10
    :goto_6
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 1953
    :cond_11
    const/16 v20, 0x1

    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 1954
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v20

    const v21, 0x7f0e02a3

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_6

    .line 1958
    .end local v17    # "state":I
    :cond_12
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 1904
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onItemPressed(FF)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 3118
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v3, :cond_1

    .line 3135
    :cond_0
    :goto_0
    return-void

    .line 3122
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3123
    .local v0, "faceCnt":I
    const/4 v2, -0x1

    .line 3124
    .local v2, "index":I
    if-lez v0, :cond_3

    .line 3125
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_3

    .line 3126
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v3, v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-boolean v3, v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupVis:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-ne v1, v3, :cond_2

    .line 3128
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget-object v3, v3, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mPopupItemsRect:[Landroid/graphics/Rect;

    float-to-int v4, p1

    float-to-int v5, p2

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkPopupItemClickedIndex([Landroid/graphics/Rect;II)I

    move-result v2

    .line 3129
    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 3130
    iput v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    .line 3125
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3134
    .end local v1    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 2230
    invoke-super/range {p0 .. p5}, Lcom/sec/android/gallery3d/ui/GLView;->onLayout(ZIIII)V

    .line 2231
    return-void
.end method

.method public declared-synchronized pause()V
    .locals 2

    .prologue
    .line 2885
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v0

    .line 2886
    .local v0, "snsService":Lcom/sec/android/gallery3d/remote/sns/FacebookService;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mBindFaceBookService:Z

    if-eqz v1, :cond_0

    .line 2887
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->FBDisConnection(Landroid/content/Context;)Z

    .line 2888
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->unregisterSNSRefreshListener()V

    .line 2889
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mBindFaceBookService:Z

    .line 2891
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hide()V

    .line 2892
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hideContactPopupView()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2893
    monitor-exit p0

    return-void

    .line 2885
    .end local v0    # "snsService":Lcom/sec/android/gallery3d/remote/sns/FacebookService;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public removeContactPopupView()V
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0175

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContainer:Landroid/view/ViewGroup;

    .line 439
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_0

    .line 440
    const-string v0, "FaceIndicatorView"

    const-string v1, "remove sns view"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 444
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->dismissDialogs()V

    .line 445
    return-void
.end method

.method public declared-synchronized render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 4
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 1569
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 1584
    :goto_0
    monitor-exit p0

    return-void

    .line 1572
    :cond_0
    :try_start_1
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    .line 1573
    .local v1, "glPaint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 1574
    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1578
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->drawFaceIndicatorView(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1582
    :try_start_3
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1569
    .end local v1    # "glPaint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1579
    .restart local v1    # "glPaint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    :catch_0
    move-exception v0

    .line 1580
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v2, "FaceIndicatorView"

    const-string v3, "Error to drawing face indicator"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1582
    :try_start_5
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public resetPressedIndex()V
    .locals 1

    .prologue
    .line 3138
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPressedItemIndex:I

    .line 3139
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 3140
    return-void
.end method

.method public resetView()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 3143
    iget v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-eq v0, v3, :cond_1

    .line 3144
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3145
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mFaceState:I

    if-nez v0, :cond_4

    .line 3146
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 3150
    :cond_0
    :goto_0
    iput v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 3153
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    if-eqz v0, :cond_2

    .line 3154
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    .line 3156
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v0, :cond_3

    .line 3157
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_3

    .line 3158
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 3159
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 3162
    :cond_3
    return-void

    .line 3148
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    goto :goto_0
.end method

.method public declared-synchronized resume()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 2846
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isResume:Z

    .line 2848
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-result-object v0

    .line 2849
    .local v0, "snsService":Lcom/sec/android/gallery3d/remote/sns/FacebookService;
    if-eqz v0, :cond_0

    .line 2850
    new-instance v1, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$10;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$10;-><init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->registerSNSRefreshListener(Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;)V

    .line 2856
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->FBConnection(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mBindFaceBookService:Z

    .line 2859
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getProfileLookupKey()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    .line 2860
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceDataFromResume(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 2861
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkContactData(Z)V

    .line 2863
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    .line 2864
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    .line 2866
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v1, :cond_3

    .line 2867
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isContactsChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2868
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->showMoreDialog(Z)V

    .line 2870
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v1, :cond_2

    .line 2871
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setHeadPosition(III)V

    .line 2872
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonState(Z)V

    .line 2876
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsChangedFromContact:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getAutoGroup()I

    move-result v1

    if-ne v1, v6, :cond_4

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isMe()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2877
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->removeFace(Landroid/content/Context;I)V

    .line 2878
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    .line 2879
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 2881
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsChangedFromContact:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2882
    monitor-exit p0

    return-void

    .line 2846
    .end local v0    # "snsService":Lcom/sec/android/gallery3d/remote/sns/FacebookService;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setAboveFilmStripHeight(I)V
    .locals 0
    .param p1, "filmStripHeight"    # I

    .prologue
    .line 3323
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripHeight:I

    .line 3324
    return-void
.end method

.method public setAssignedName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 2918
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainFaceModified:Z

    if-eqz v0, :cond_0

    .line 2919
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mAssignedName:Ljava/lang/String;

    .line 2920
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMainFaceModified:Z

    .line 2921
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaSetKey:Ljava/lang/String;

    .line 2923
    :cond_0
    return-void
.end method

.method public declared-synchronized setCurContactLookupKey(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 3216
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkContactData(Z)V

    .line 3217
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurContactLookupKey:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3218
    monitor-exit p0

    return-void

    .line 3216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 831
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 832
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcWidth()I

    move-result v1

    .line 833
    .local v1, "w":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcHeight()I

    move-result v0

    .line 834
    .local v0, "h":I
    new-instance v2, Landroid/graphics/RectF;

    iget v3, p2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    int-to-float v5, v0

    div-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    int-to-float v6, v1

    div-float/2addr v5, v6

    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    int-to-float v7, v0

    div-float/2addr v6, v7

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRectScale:Landroid/graphics/RectF;

    .line 835
    return-void
.end method

.method public setCurMediaSetKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 2910
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetKey:Ljava/lang/String;

    .line 2911
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaSetName:Ljava/lang/String;

    .line 2912
    return-void
.end method

.method public setCurrentFaceId(I)V
    .locals 0
    .param p1, "curFaceId"    # I

    .prologue
    .line 2811
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    .line 2812
    return-void
.end method

.method public setFaceFeature(Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;)V
    .locals 0
    .param p1, "mFaceTagFeature"    # Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    .prologue
    .line 3330
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    .line 3331
    return-void
.end method

.method public setFaceTagToggle(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2243
    if-eqz p1, :cond_1

    .line 2244
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->show()V

    .line 2253
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceTagToggle:Z

    .line 2254
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 2255
    return-void

    .line 2246
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hide()V

    .line 2247
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_0

    .line 2248
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 2249
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public setGenericFocusIndex(IZ)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "update"    # Z

    .prologue
    .line 1866
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusIndex:I

    .line 1867
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mGenericFocusOnFaceId:I

    .line 1868
    if-eqz p2, :cond_0

    .line 1869
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 1871
    :cond_0
    return-void
.end method

.method public setHeadPos(III)V
    .locals 2
    .param p1, "first"    # I
    .param p2, "second"    # I
    .param p3, "third"    # I

    .prologue
    .line 3317
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 3318
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 3319
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHeadPos:[I

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 3320
    return-void
.end method

.method public setImageBounds(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "imageBounds"    # Landroid/graphics/Rect;

    .prologue
    .line 2236
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mImageBounds:Landroid/graphics/Rect;

    .line 2237
    return-void
.end method

.method public setIsManualFDResumed(Z)V
    .locals 0
    .param p1, "isManualFD"    # Z

    .prologue
    .line 3359
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mIsManualFDResumed:Z

    .line 3360
    return-void
.end method

.method public setManualFace(Landroid/net/Uri;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v6, -0x1

    .line 2821
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v2, v7

    .line 2822
    .local v2, "faceCount":I
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2823
    .local v5, "strUri":Ljava/lang/String;
    const/16 v7, 0x2f

    invoke-virtual {v5, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 2825
    .local v4, "id":I
    add-int/lit8 v3, v2, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_0

    .line 2826
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v1, v7, v3

    .line 2827
    .local v1, "face":Lcom/sec/android/gallery3d/data/Face;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v7

    if-ne v4, v7, :cond_1

    .line 2828
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFace:Lcom/sec/android/gallery3d/data/Face;

    .line 2829
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I

    .line 2830
    iput v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 2831
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V

    .line 2832
    iget v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurFaceId:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2840
    .end local v1    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v2    # "faceCount":I
    .end local v3    # "i":I
    .end local v4    # "id":I
    .end local v5    # "strUri":Ljava/lang/String;
    :cond_0
    :goto_1
    return v6

    .line 2825
    .restart local v1    # "face":Lcom/sec/android/gallery3d/data/Face;
    .restart local v2    # "faceCount":I
    .restart local v3    # "i":I
    .restart local v4    # "id":I
    .restart local v5    # "strUri":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 2835
    .end local v1    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v2    # "faceCount":I
    .end local v3    # "i":I
    .end local v4    # "id":I
    .end local v5    # "strUri":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2836
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public setPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x0

    .line 999
    if-nez p1, :cond_0

    .line 1059
    :goto_0
    return-void

    .line 1002
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1003
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_1

    .line 1004
    const-string v0, "FaceIndicatorView"

    const-string v1, "Same media path and invalidate only excute."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaWidth:I

    .line 1006
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 1007
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto :goto_0

    .line 1012
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1013
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->dismissDialogs()V

    .line 1014
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_2

    .line 1015
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->dismissDialog()V

    .line 1018
    :cond_2
    monitor-enter p0

    .line 1019
    :try_start_0
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v0, :cond_5

    .line 1020
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1022
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    .line 1024
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1025
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1028
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1029
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mUnnamedFaceIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1031
    :cond_4
    monitor-exit p0

    goto :goto_0

    .line 1033
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1035
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1036
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMediaWidth:I

    .line 1045
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isResume:Z

    if-nez v0, :cond_6

    .line 1046
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    .line 1047
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hideContactPopupView()V

    .line 1052
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFaceData(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 1058
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    goto/16 :goto_0

    .line 1050
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->isResume:Z

    goto :goto_1
.end method

.method public setPositionController(Lcom/sec/android/gallery3d/ui/PositionController;)V
    .locals 1
    .param p1, "positionController"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 2799
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 2800
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mImageBounds:Landroid/graphics/Rect;

    .line 2801
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 3311
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScale:F

    .line 3312
    return-void
.end method

.method public setScreenSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 3278
    iput p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenWidth:I

    .line 3279
    iput p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mScreenHeight:I

    .line 3280
    return-void
.end method

.method public setStripStatus(Z)V
    .locals 0
    .param p1, "status"    # Z

    .prologue
    .line 2760
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripStatus:Z

    .line 2761
    return-void
.end method

.method public setUnnamedCandidates(I[I)V
    .locals 25
    .param p1, "faceId"    # I
    .param p2, "candidatePersonIds"    # [I

    .prologue
    .line 2988
    if-lez p1, :cond_0

    if-nez p2, :cond_1

    .line 3113
    :cond_0
    :goto_0
    return-void

    .line 2992
    :cond_1
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v23, v0

    if-lez v23, :cond_0

    .line 2997
    move-object/from16 v10, p2

    .line 2998
    .local v10, "ids":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/access/face/FaceList;->getRecommendedId(Landroid/content/Context;I)I

    move-result v21

    .line 3001
    .local v21, "personId":I
    move-object/from16 v0, p2

    array-length v3, v0

    .line 3002
    .local v3, "candiLength":I
    const/4 v12, 0x0

    .line 3003
    .local v12, "inCandidates":Z
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v3, :cond_2

    .line 3004
    aget v23, p2, v9

    move/from16 v0, v21

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    .line 3005
    const/4 v12, 0x1

    .line 3011
    :cond_2
    array-length v0, v10

    move/from16 v16, v0

    .line 3012
    .local v16, "l":I
    if-lez v16, :cond_0

    .line 3016
    const/16 v18, 0x0

    .line 3017
    .local v18, "lth":I
    const/16 v20, 0x0

    .line 3018
    .local v20, "name":Ljava/lang/String;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 3019
    .local v19, "meIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v9, v0, :cond_5

    .line 3020
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    aget v24, v10, v9

    invoke-static/range {v23 .. v24}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v20

    .line 3021
    if-eqz v20, :cond_4

    const-string v23, "profile/Me"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 3022
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3019
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 3003
    .end local v16    # "l":I
    .end local v18    # "lth":I
    .end local v19    # "meIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v20    # "name":Ljava/lang/String;
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 3025
    .restart local v16    # "l":I
    .restart local v18    # "lth":I
    .restart local v19    # "meIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v20    # "name":Ljava/lang/String;
    :cond_4
    add-int/lit8 v18, v18, 0x1

    goto :goto_3

    .line 3028
    :cond_5
    const/4 v2, 0x0

    .line 3029
    .local v2, "IDs":[I
    move/from16 v0, v18

    move/from16 v1, v16

    if-ne v0, v1, :cond_9

    .line 3030
    move-object v2, v10

    .line 3042
    :cond_6
    if-eqz v12, :cond_8

    .line 3043
    const/4 v13, 0x0

    .line 3044
    .local v13, "indexOfCurFace":I
    const/4 v9, 0x0

    :goto_4
    array-length v0, v2

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v9, v0, :cond_7

    .line 3045
    aget v23, v2, v9

    move/from16 v0, v21

    move/from16 v1, v23

    if-ne v0, v1, :cond_b

    .line 3046
    move v13, v9

    .line 3051
    :cond_7
    if-eqz v13, :cond_8

    .line 3052
    aget v22, v2, v13

    .line 3053
    .local v22, "tmp":I
    const/16 v23, 0x0

    aget v23, v2, v23

    aput v23, v2, v13

    .line 3054
    const/16 v23, 0x0

    aput v22, v2, v23

    .line 3060
    .end local v13    # "indexOfCurFace":I
    .end local v22    # "tmp":I
    :cond_8
    const/4 v11, 0x0

    .line 3061
    .local v11, "idz":[I
    array-length v0, v2

    move/from16 v17, v0

    .line 3062
    .local v17, "length":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLookupKeyMe:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_c

    .line 3063
    add-int/lit8 v23, v17, 0x1

    move/from16 v0, v23

    new-array v11, v0, [I

    .line 3065
    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMePersonId:I

    move/from16 v24, v0

    aput v24, v11, v23

    .line 3066
    const/4 v9, 0x1

    :goto_5
    array-length v0, v11

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v9, v0, :cond_d

    .line 3067
    add-int/lit8 v23, v9, -0x1

    aget v23, v2, v23

    aput v23, v11, v9

    .line 3066
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 3032
    .end local v11    # "idz":[I
    .end local v17    # "length":I
    :cond_9
    move/from16 v0, v18

    new-array v2, v0, [I

    .line 3033
    const/4 v15, 0x0

    .line 3034
    .local v15, "j":I
    const/4 v9, 0x0

    :goto_6
    move/from16 v0, v16

    if-ge v9, v0, :cond_6

    .line 3035
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_a

    .line 3036
    aget v23, v10, v9

    aput v23, v2, v15

    .line 3037
    add-int/lit8 v15, v15, 0x1

    .line 3034
    :cond_a
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 3044
    .end local v15    # "j":I
    .restart local v13    # "indexOfCurFace":I
    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 3070
    .end local v13    # "indexOfCurFace":I
    .restart local v11    # "idz":[I
    .restart local v17    # "length":I
    :cond_c
    move-object v11, v2

    .line 3074
    :cond_d
    if-nez v12, :cond_e

    const/16 v23, 0x1

    move/from16 v0, v17

    move/from16 v1, v23

    if-le v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mMePersonId:I

    move/from16 v23, v0

    move/from16 v0, v21

    move/from16 v1, v23

    if-eq v0, v1, :cond_e

    .line 3075
    const/16 v23, 0x1

    aput v21, v11, v23

    .line 3080
    :cond_e
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3081
    .local v5, "candidateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v11, :cond_10

    .line 3082
    const/4 v14, 0x0

    .local v14, "ix":I
    :goto_7
    array-length v0, v11

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v14, v0, :cond_10

    .line 3083
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    aget v24, v11, v14

    invoke-static/range {v23 .. v24}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 3084
    .local v6, "checkName":Ljava/lang/String;
    if-eqz v6, :cond_f

    .line 3085
    aget v23, v11, v14

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3082
    :cond_f
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    .line 3090
    .end local v6    # "checkName":Ljava/lang/String;
    .end local v14    # "ix":I
    :cond_10
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 3091
    .local v4, "candidataeListSize":I
    if-lez v4, :cond_11

    .line 3092
    new-array v11, v4, [I

    .line 3093
    const/4 v14, 0x0

    .restart local v14    # "ix":I
    :goto_8
    if-ge v14, v4, :cond_11

    .line 3094
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    aput v23, v11, v14

    .line 3093
    add-int/lit8 v14, v14, 0x1

    goto :goto_8

    .line 3098
    .end local v14    # "ix":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    .line 3099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 3100
    .local v7, "cnt":I
    if-lez v7, :cond_0

    .line 3101
    const/4 v9, 0x0

    :goto_9
    if-ge v9, v7, :cond_0

    .line 3102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v23, v0

    if-eqz v23, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ne v0, v7, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v23, v0

    aget-object v23, v23, v9

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/data/Face;->getFaceId()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, p1

    if-ne v0, v1, :cond_12

    .line 3103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    move-object/from16 v0, v23

    iput-object v11, v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->mUnnamedCandidates:[I

    .line 3104
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3110
    .end local v4    # "candidataeListSize":I
    .end local v5    # "candidateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v7    # "cnt":I
    :catch_0
    move-exception v8

    .line 3111
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 3101
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v4    # "candidataeListSize":I
    .restart local v5    # "candidateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v7    # "cnt":I
    :cond_12
    add-int/lit8 v9, v9, 0x1

    goto :goto_9
.end method

.method public setUpdateFaceListener(Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    .prologue
    .line 3350
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;

    .line 3351
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 2258
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2262
    :goto_0
    return-void

    .line 2260
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 2261
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showContactPopupView()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2954
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRedrawContactPopup:Z

    if-eqz v0, :cond_0

    .line 2955
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->show(Z)V

    .line 2956
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 2957
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonState(Z)V

    .line 2958
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mRedrawContactPopup:Z

    .line 2960
    :cond_0
    return-void
.end method

.method public singleTapUp(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2716
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->resetPressedIndex()V

    .line 2717
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->getVisibility()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 2754
    :cond_0
    :goto_0
    return v1

    .line 2720
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-eq v3, v4, :cond_4

    .line 2721
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    if-eqz v3, :cond_2

    .line 2722
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopup:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-virtual {v3, p1, p2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 2723
    goto :goto_0

    .line 2726
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 2727
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 2728
    .local v0, "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    if-eqz v0, :cond_3

    .line 2729
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 2731
    .end local v0    # "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_3
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContactPopupVisible:Z

    .line 2732
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hideContactPopupView()V

    .line 2733
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    move v1, v2

    .line 2734
    goto :goto_0

    .line 2735
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkPopupItem(II)Z

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    .line 2736
    goto :goto_0

    .line 2737
    :cond_5
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    if-eqz v3, :cond_8

    iget v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    if-eq v3, v4, :cond_8

    .line 2739
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 2740
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceInfos:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFaceIndexShowContact:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;

    .line 2741
    .restart local v0    # "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    if-eqz v0, :cond_6

    .line 2742
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;->setPopupVisible(Z)V

    .line 2744
    .end local v0    # "faceInfo":Lcom/sec/android/gallery3d/ui/FaceIndicatorView$FaceInfo;
    :cond_6
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mNameMenuPopupVisible:Z

    .line 2745
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->invalidate()V

    .line 2746
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2747
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hide()V

    :cond_7
    move v1, v2

    .line 2748
    goto :goto_0

    .line 2749
    :cond_8
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkNameTagClick(II)Z

    move-result v3

    if-eqz v3, :cond_9

    move v1, v2

    .line 2750
    goto :goto_0

    .line 2751
    :cond_9
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mFilmStripStatus:Z

    if-eqz v3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->checkFaceRect(II)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 2752
    goto/16 :goto_0
.end method
