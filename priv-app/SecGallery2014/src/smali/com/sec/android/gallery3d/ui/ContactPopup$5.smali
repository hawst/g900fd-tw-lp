.class Lcom/sec/android/gallery3d/ui/ContactPopup$5;
.super Ljava/lang/Object;
.source "ContactPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContactPopup;->reloadWidget()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 566
    const-string v2, "ContactPopup"

    const-string v3, "Email button clicked!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 568
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->email:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 569
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$600(Lcom/sec/android/gallery3d/ui/ContactPopup;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$602(Lcom/sec/android/gallery3d/ui/ContactPopup;Z)Z

    .line 573
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$1000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 574
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$1000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    # invokes: Lcom/sec/android/gallery3d/ui/ContactPopup;->resetButtonsClickable(IZ)V
    invoke-static {v2, v3, v5}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$800(Lcom/sec/android/gallery3d/ui/ContactPopup;IZ)V

    .line 575
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mailto:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/samsung/gallery/access/contact/ContactData;->email:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 578
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0062

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 581
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$5;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # setter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v2, v5}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$602(Lcom/sec/android/gallery3d/ui/ContactPopup;Z)Z

    .line 582
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
