.class public Lcom/sec/android/gallery3d/ui/SelectionManager;
.super Ljava/lang/Object;
.source "SelectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;,
        Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;
    }
.end annotation


# static fields
.field public static final ENTER_SELECTION_MODE:I = 0x1

.field public static final LEAVE_SELECTION_MODE:I = 0x2

.field public static final SELECT_ALL_MODE:I = 0x3

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private indexMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAutoLeave:Z

.field private mClickedSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentIndex:I

.field private mCurrentMediaSetSelectedCount:I

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mExpMediaItem:Lcom/sec/android/gallery3d/data/MediaObject;

.field private mInDeleteSelectionMode:Z

.field private mInExpansionMode:Z

.field private mInSelectionMode:Z

.field private mInverseSelection:Z

.field private mIsAlbumSet:Z

.field private mIsEnterEventSelectionMode:Z

.field private mIsHideItemInSelectionMode:Z

.field private mIsPenSelectionMode:Z

.field private mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

.field public mSelectedCountMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mShare:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;",
            ">;"
        }
    .end annotation
.end field

.field private mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mTotal:I

.field private mVibrator:Landroid/os/Vibrator;

.field private final mediaList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/SelectionManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)V
    .locals 3
    .param p1, "galleryApp"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .param p2, "isAlbumSet"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInDeleteSelectionMode:Z

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mAutoLeave:Z

    .line 80
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mExpMediaItem:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 82
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsHideItemInSelectionMode:Z

    .line 83
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsEnterEventSelectionMode:Z

    .line 84
    iput v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mCurrentIndex:I

    .line 87
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mVibrator:Landroid/os/Vibrator;

    .line 88
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mContext:Landroid/content/Context;

    .line 97
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 98
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    .line 99
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsAlbumSet:Z

    .line 100
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mTotal:I

    .line 103
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    .line 106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->indexMap:Ljava/util/HashMap;

    .line 107
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mContext:Landroid/content/Context;

    .line 109
    return-void
.end method

.method private static expandMediaSet(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;I)Z
    .locals 12
    .param p1, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "maxSelection"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "I)Z"
        }
    .end annotation

    .prologue
    .local p0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v9, 0x0

    .line 243
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v7

    .line 244
    .local v7, "subCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_2

    .line 245
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    invoke-static {p0, v10, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->expandMediaSet(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;I)Z

    move-result v10

    if-nez v10, :cond_1

    .line 269
    :cond_0
    :goto_1
    return v9

    .line 244
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 249
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    .line 250
    .local v8, "total":I
    const/16 v0, 0x32

    .line 251
    .local v0, "batch":I
    const/4 v4, 0x0

    .line 253
    .local v4, "index":I
    :goto_2
    if-ge v4, v8, :cond_6

    .line 254
    add-int v10, v4, v0

    if-ge v10, v8, :cond_4

    move v1, v0

    .line 257
    .local v1, "count":I
    :goto_3
    invoke-virtual {p1, v4, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v6

    .line 258
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v11

    sub-int v11, p2, v11

    if-gt v10, v11, :cond_0

    .line 262
    :cond_3
    if-eqz v6, :cond_5

    .line 263
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 264
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    invoke-virtual {p0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 254
    .end local v1    # "count":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_4
    sub-int v1, v8, v4

    goto :goto_3

    .line 267
    .restart local v1    # "count":I
    .restart local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_5
    add-int/2addr v4, v0

    .line 268
    goto :goto_2

    .line 269
    .end local v1    # "count":I
    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_6
    const/4 v9, 0x1

    goto :goto_1
.end method

.method private findAgainByPathAndRemove(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 583
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/Path;)V

    .line 584
    return-void
.end method

.method private playHaptic()V
    .locals 4

    .prologue
    .line 1147
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    .line 1148
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mVibrator:Landroid/os/Vibrator;

    .line 1150
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1151
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 1154
    :cond_1
    return-void
.end method

.method private remove(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 5
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 568
    if-nez p1, :cond_0

    .line 580
    :goto_0
    return-void

    .line 571
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v4

    .line 572
    const/4 v0, 0x0

    .local v0, "i":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v2

    .local v2, "size":I
    :goto_1
    if-ge v0, v2, :cond_2

    .line 573
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 574
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 575
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 576
    monitor-exit v4

    goto :goto_0

    .line 579
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v2    # "size":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 572
    .restart local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v2    # "size":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 579
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 2
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 659
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 669
    :cond_0
    :goto_0
    return-void

    .line 662
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 663
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 664
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 666
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 664
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 12
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 409
    const/4 v9, 0x0

    .line 411
    .local v9, "tempSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeAllSelectMode:Z

    if-eqz v10, :cond_7

    instance-of v10, p1, Lcom/sec/android/gallery3d/data/TimeAllAlbum;

    if-eqz v10, :cond_7

    .line 412
    const/4 v1, 0x0

    .line 413
    .local v1, "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v0, 0x0

    .local v0, "bFound":Z
    move-object v10, p2

    .line 414
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getParentSetPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 416
    .local v3, "currentMediaParentPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 417
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 418
    .restart local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 419
    const/4 v0, 0x1

    .line 423
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    :cond_1
    if-eqz v0, :cond_4

    .line 424
    move-object v9, v1

    .line 440
    .end local v0    # "bFound":Z
    .end local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "currentMediaParentPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    if-nez v9, :cond_8

    .line 453
    :cond_3
    :goto_1
    return-void

    .restart local v0    # "bFound":Z
    .restart local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v3    # "currentMediaParentPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_4
    move-object v10, p1

    .line 426
    check-cast v10, Lcom/sec/android/gallery3d/data/TimeAllAlbum;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v8

    .line 427
    .local v8, "subSetSize":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v8, :cond_5

    move-object v10, p1

    .line 428
    check-cast v10, Lcom/sec/android/gallery3d/data/TimeAllAlbum;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    invoke-virtual {v10, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 429
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 430
    const/4 v0, 0x1

    .line 434
    :cond_5
    if-eqz v0, :cond_2

    .line 435
    move-object v9, v1

    goto :goto_0

    .line 427
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 438
    .end local v0    # "bFound":Z
    .end local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "currentMediaParentPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v5    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "subSetSize":I
    :cond_7
    move-object v9, p1

    goto :goto_0

    .line 442
    :cond_8
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v10, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 443
    .local v7, "obj":Ljava/lang/Integer;
    if-eqz v7, :cond_a

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 445
    .local v2, "count":I
    :goto_3
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v10, p2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 446
    instance-of v10, p2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v10, :cond_9

    move-object v10, p2

    .line 447
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/MediaItem;->setParentSetPath(Lcom/sec/android/gallery3d/data/Path;)V

    .line 449
    :cond_9
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 450
    add-int/lit8 v2, v2, 0x1

    .line 451
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 443
    .end local v2    # "count":I
    :cond_a
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;I)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p3, "index"    # I

    .prologue
    .line 1120
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1121
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->indexMap:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1122
    return-void
.end method

.method public add(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V
    .locals 6
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<+",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 390
    .local p2, "mediaItemList":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 394
    .local v3, "obj":Ljava/lang/Integer;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 396
    .local v0, "count":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 397
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v4, v2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_3

    move-object v4, v2

    .line 398
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->setParentSetPath(Lcom/sec/android/gallery3d/data/Path;)V

    .line 400
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 401
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 402
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 394
    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 405
    .restart local v0    # "count":I
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public add(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "mediaItemList":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez p1, :cond_1

    .line 387
    :cond_0
    return-void

    .line 379
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 380
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v2, v1, Lcom/sec/android/gallery3d/data/ActionImage;

    if-nez v2, :cond_2

    .line 383
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 384
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0
.end method

.method public add(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 672
    .local p1, "mapToAdd":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 673
    .local v1, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    goto :goto_0

    .line 675
    .end local v1    # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    :cond_0
    return-void
.end method

.method public addExpMediaItem(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 0
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 1135
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mExpMediaItem:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1136
    return-void
.end method

.method public addSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 3
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    .line 1079
    if-eqz p1, :cond_0

    .line 1080
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 1082
    .local v0, "itemsCnt":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    add-int v2, v0, p2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084
    .end local v0    # "itemsCnt":I
    :cond_0
    return-void
.end method

.method public addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 18
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 776
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v14, :cond_1

    .line 869
    :cond_0
    :goto_0
    return-void

    .line 778
    :cond_1
    new-instance v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;-><init>(Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 779
    .local v11, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    move-object/from16 v0, p1

    iput-object v0, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->media:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 780
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v15

    .line 781
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v14

    if-ge v4, v14, :cond_3

    .line 783
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 784
    .local v9, "shareProperty":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v9, :cond_2

    iget-object v14, v9, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->media:Lcom/sec/android/gallery3d/data/MediaObject;

    if-eqz v14, :cond_2

    iget-object v14, v9, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->media:Lcom/sec/android/gallery3d/data/MediaObject;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v14

    if-eqz v14, :cond_2

    .line 785
    :try_start_2
    monitor-exit v15

    goto :goto_0

    .line 790
    .end local v9    # "shareProperty":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :catchall_0
    move-exception v14

    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v14

    .line 786
    :catch_0
    move-exception v2

    .line 787
    .local v2, "aie":Ljava/lang/ArrayIndexOutOfBoundsException;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 781
    .end local v2    # "aie":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 790
    :cond_3
    monitor-exit v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 793
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v14, :cond_d

    move-object/from16 v14, p1

    .line 794
    check-cast v14, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemCount()I

    move-result v10

    .local v10, "size":I
    move-object/from16 v14, p1

    .line 795
    check-cast v14, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v10}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 796
    .local v8, "mItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v8, :cond_0

    .line 798
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 799
    .local v7, "m":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v14, v7, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v14, :cond_a

    .line 800
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsVideo:Z

    .line 805
    :cond_5
    :goto_3
    instance-of v14, v7, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v14, :cond_6

    move-object v14, v7

    check-cast v14, Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v16, 0x10

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 806
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsSoundShotImage:Z

    .line 808
    :cond_6
    instance-of v14, v7, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 809
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsGolfShotImage:Z

    .line 811
    :cond_7
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v12

    .line 812
    .local v12, "supportedOperation":J
    const-wide/16 v14, 0x4

    and-long/2addr v14, v12

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_b

    const/4 v6, 0x1

    .line 813
    .local v6, "isItemSharable":Z
    :goto_4
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v14

    if-eqz v14, :cond_c

    if-eqz v6, :cond_c

    const/4 v5, 0x1

    .line 815
    .local v5, "isDrmNotForwardLock":Z
    :goto_5
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v14

    if-eqz v14, :cond_8

    if-eqz v5, :cond_9

    :cond_8
    if-nez v6, :cond_4

    .line 816
    :cond_9
    const/4 v14, 0x0

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsSupportShare:Z

    goto :goto_2

    .line 802
    .end local v5    # "isDrmNotForwardLock":Z
    .end local v6    # "isItemSharable":Z
    .end local v12    # "supportedOperation":J
    :cond_a
    instance-of v14, v7, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v14, :cond_5

    .line 803
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsImage:Z

    goto :goto_3

    .line 812
    .restart local v12    # "supportedOperation":J
    :cond_b
    const/4 v6, 0x0

    goto :goto_4

    .line 813
    .restart local v6    # "isItemSharable":Z
    :cond_c
    const/4 v5, 0x0

    goto :goto_5

    .line 820
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "isItemSharable":Z
    .end local v7    # "m":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v8    # "mItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v10    # "size":I
    .end local v12    # "supportedOperation":J
    :cond_d
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-eqz v14, :cond_f

    .line 821
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsDropbaxItems:Z

    move-object/from16 v14, p1

    .line 822
    check-cast v14, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;->getMediaItemCount()I

    move-result v10

    .restart local v10    # "size":I
    move-object/from16 v14, p1

    .line 823
    check-cast v14, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v10}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 824
    .restart local v8    # "mItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v8, :cond_0

    .line 826
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_e
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 827
    .restart local v7    # "m":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v14, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v14, :cond_e

    .line 828
    const/4 v14, 0x0

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsSupportShare:Z

    .line 829
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsDropboxVideo:Z

    goto :goto_6

    .line 833
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v7    # "m":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v8    # "mItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v10    # "size":I
    :cond_f
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v14, :cond_1d

    .line 834
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsVideo:Z

    .line 840
    :cond_10
    :goto_7
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v14, :cond_11

    move-object/from16 v14, p1

    check-cast v14, Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v16, 0x10

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v14

    if-eqz v14, :cond_11

    .line 841
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsSoundShotImage:Z

    .line 843
    :cond_11
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_12

    move-object/from16 v14, p1

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v14

    if-eqz v14, :cond_12

    .line 844
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsGolfShotImage:Z

    .line 846
    :cond_12
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v14, :cond_13

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v14, :cond_13

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v14, :cond_13

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-eqz v14, :cond_14

    .line 848
    :cond_13
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsPicasa:Z

    .line 851
    :cond_14
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v14, :cond_15

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-nez v14, :cond_15

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v14, :cond_15

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v14, :cond_16

    .line 852
    :cond_15
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsLocal:Z

    .line 854
    :cond_16
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v14, :cond_17

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-eqz v14, :cond_18

    .line 855
    :cond_17
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsSNS:Z

    .line 857
    :cond_18
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v14, :cond_19

    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v14, :cond_1a

    .line 858
    :cond_19
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsDropbaxItems:Z

    .line 860
    :cond_1a
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v14, :cond_1b

    .line 861
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsDropboxVideo:Z

    .line 863
    :cond_1b
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-eqz v14, :cond_1c

    .line 864
    const/4 v14, 0x0

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsSupportShare:Z

    .line 866
    :cond_1c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v15

    .line 867
    :try_start_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v14, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 868
    monitor-exit v15

    goto/16 :goto_0

    :catchall_1
    move-exception v14

    monitor-exit v15
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v14

    .line 836
    :cond_1d
    move-object/from16 v0, p1

    instance-of v14, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v14, :cond_10

    .line 837
    const/4 v14, 0x1

    iput-boolean v14, v11, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsImage:Z

    goto/16 :goto_7
.end method

.method public addShareProperty(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 462
    .local p1, "mediaItemList":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v2, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    if-eqz p1, :cond_0

    .line 466
    monitor-enter p1

    .line 467
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 468
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v2, v1, Lcom/sec/android/gallery3d/data/ActionImage;

    if-nez v2, :cond_2

    .line 471
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1

    .line 473
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public addShareProperty(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 456
    .local p1, "mapToAdd":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 457
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/List;)V

    goto :goto_0

    .line 459
    .end local v1    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    return-void
.end method

.method public addUnchek(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 2
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 678
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 679
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 680
    monitor-exit v1

    .line 681
    return-void

    .line 680
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearExpMediaItem()V
    .locals 1

    .prologue
    .line 1143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mExpMediaItem:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1144
    return-void
.end method

.method public clearSelectedCount()V
    .locals 2

    .prologue
    .line 1101
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 1102
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1103
    monitor-exit v1

    .line 1104
    return-void

    .line 1103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public containsDropboxItems()Z
    .locals 4

    .prologue
    .line 923
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 924
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 925
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 926
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsDropbaxItems:Z

    if-eqz v2, :cond_0

    .line 927
    const/4 v2, 0x1

    monitor-exit v3

    .line 931
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 924
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 930
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 931
    const/4 v2, 0x0

    goto :goto_1

    .line 930
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public containsDropboxVideos()Z
    .locals 4

    .prologue
    .line 946
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 947
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 948
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 949
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsDropboxVideo:Z

    if-eqz v2, :cond_0

    .line 950
    const/4 v2, 0x1

    monitor-exit v3

    .line 954
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 947
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 953
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 954
    const/4 v2, 0x0

    goto :goto_1

    .line 953
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public containsLocal()Z
    .locals 4

    .prologue
    .line 993
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 994
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 995
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 996
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsLocal:Z

    if-eqz v2, :cond_0

    .line 997
    const/4 v2, 0x1

    monitor-exit v3

    .line 1001
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 994
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1000
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 1001
    const/4 v2, 0x0

    goto :goto_1

    .line 1000
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public containsPicasa()Z
    .locals 4

    .prologue
    .line 911
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 912
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 913
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 914
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsPicasa:Z

    if-eqz v2, :cond_0

    .line 915
    const/4 v2, 0x1

    monitor-exit v3

    .line 919
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 912
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 918
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 919
    const/4 v2, 0x0

    goto :goto_1

    .line 918
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public containsSNS()Z
    .locals 4

    .prologue
    .line 934
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 935
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 936
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 937
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsSNS:Z

    if-eqz v2, :cond_0

    .line 938
    const/4 v2, 0x1

    monitor-exit v3

    .line 942
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 935
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 941
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 942
    const/4 v2, 0x0

    goto :goto_1

    .line 941
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public deSelectAll()V
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    .line 132
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 133
    return-void
.end method

.method public enterEventSelectionMode()V
    .locals 1

    .prologue
    .line 603
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsEnterEventSelectionMode:Z

    .line 604
    return-void
.end method

.method public enterHideItemInSelectionMode()V
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsHideItemInSelectionMode:Z

    .line 600
    return-void
.end method

.method public enterSelectionMode(Z)V
    .locals 2
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v1, 0x1

    .line 160
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInSelectionMode:Z

    if-eqz v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    if-eqz p1, :cond_2

    .line 164
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->playHaptic()V

    .line 166
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInSelectionMode:Z

    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;->onSelectionModeChange(I)V

    goto :goto_0
.end method

.method public exitEventFromSelectionMode()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 607
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsEnterEventSelectionMode:Z

    .line 608
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsHideItemInSelectionMode:Z

    .line 609
    return-void
.end method

.method public get(I)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 694
    const/4 v2, 0x0

    .line 696
    .local v2, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v2

    .line 700
    :goto_0
    return-object v3

    .line 698
    :catch_0
    move-exception v1

    .line 699
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/gallery3d/ui/SelectionManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : get "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCloneMediaList()Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 757
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    monitor-exit v1

    return-object v0

    .line 758
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 1046
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mCurrentIndex:I

    return v0
.end method

.method public getCurrentMediaSetSelectedCount()I
    .locals 1

    .prologue
    .line 1112
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mCurrentMediaSetSelectedCount:I

    return v0
.end method

.method public getExpMediaItem()Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 1

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mExpMediaItem:Lcom/sec/android/gallery3d/data/MediaObject;

    return-object v0
.end method

.method public getList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getMediaItemArrayList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 728
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 730
    .local v2, "selectedKeyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v4

    .line 731
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 732
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v3, v1, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_0

    .line 733
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 736
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 737
    return-object v2
.end method

.method public getMediaList()Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 710
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-exit v1

    return-object v0

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaSetArrayList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 715
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 717
    .local v2, "selectedKeyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v4

    .line 718
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 719
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v3, v1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v3, :cond_0

    .line 720
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 723
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 724
    return-object v2
.end method

.method public getNumberItemOfMarkedAsSelected()I
    .locals 6

    .prologue
    .line 643
    const/4 v0, 0x0

    .line 645
    .local v0, "count":I
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 647
    .local v3, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v4, v3, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_0

    .line 648
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v3    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    .line 651
    :cond_1
    monitor-exit v5

    .line 655
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    return v0

    .line 651
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/util/ConcurrentModificationException; {:try_start_2 .. :try_end_2} :catch_0

    .line 652
    :catch_0
    move-exception v1

    .line 653
    .local v1, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v1}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    goto :goto_1
.end method

.method public getNumberOfMarkedAsSelected()I
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public getSelected(Z)Ljava/util/ArrayList;
    .locals 1
    .param p1, "expandSet"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelected(ZI)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getSelected(ZI)Ljava/util/ArrayList;
    .locals 13
    .param p1, "expandSet"    # Z
    .param p2, "maxSelection"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 277
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 278
    .local v7, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-boolean v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsAlbumSet:Z

    if-eqz v11, :cond_6

    .line 279
    iget-boolean v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    if-eqz v11, :cond_3

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalCount()I

    move-result v9

    .line 281
    .local v9, "total":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v9, :cond_0

    .line 282
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v11, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 283
    .local v8, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 284
    .local v3, "id":Lcom/sec/android/gallery3d/data/Path;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v11, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 285
    if-eqz p1, :cond_1

    .line 286
    invoke-static {v7, v8, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->expandMediaSet(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;I)Z

    move-result v11

    if-nez v11, :cond_2

    move-object v7, v10

    .line 342
    .end local v1    # "i":I
    .end local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    .end local v7    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v8    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v9    # "total":I
    :cond_0
    :goto_1
    return-object v7

    .line 290
    .restart local v1    # "i":I
    .restart local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    .restart local v7    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v8    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v9    # "total":I
    :cond_1
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-le v11, p2, :cond_2

    move-object v7, v10

    .line 292
    goto :goto_1

    .line 281
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 298
    .end local v1    # "i":I
    .end local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    .end local v8    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v9    # "total":I
    :cond_3
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 299
    .restart local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    if-eqz p1, :cond_5

    .line 300
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v11, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 301
    .restart local v8    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v8, :cond_4

    .line 302
    invoke-static {v7, v8, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->expandMediaSet(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;I)Z

    move-result v11

    if-nez v11, :cond_4

    move-object v7, v10

    .line 304
    goto :goto_1

    .line 308
    .end local v8    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-le v11, p2, :cond_4

    move-object v7, v10

    .line 310
    goto :goto_1

    .line 316
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    :cond_6
    iget-boolean v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    if-eqz v11, :cond_9

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalCount()I

    move-result v9

    .line 318
    .restart local v9    # "total":I
    const/4 v4, 0x0

    .line 319
    .local v4, "index":I
    :goto_2
    if-ge v4, v9, :cond_0

    .line 320
    sub-int v11, v9, v4

    const/16 v12, 0x20

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 321
    .local v0, "count":I
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v11, v4, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v6

    .line 322
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 323
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 324
    .restart local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v11, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 325
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-le v11, p2, :cond_7

    move-object v7, v10

    .line 327
    goto :goto_1

    .line 331
    .end local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    .end local v5    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_8
    add-int/2addr v4, v0

    .line 332
    goto :goto_2

    .line 334
    .end local v0    # "count":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "index":I
    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v9    # "total":I
    :cond_9
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 335
    .restart local v3    # "id":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-le v11, p2, :cond_a

    move-object v7, v10

    .line 337
    goto/16 :goto_1
.end method

.method public getSelectedCount()I
    .locals 2

    .prologue
    .line 209
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    .line 210
    .local v0, "count":I
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    if-eqz v1, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalCount()I

    move-result v1

    sub-int v0, v1, v0

    .line 213
    :cond_0
    return v0
.end method

.method public getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 6
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1062
    const/4 v4, 0x0

    .line 1063
    .local v4, "selCnt":I
    if-eqz p1, :cond_1

    .line 1064
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeAllSelectMode:Z

    if-eqz v5, :cond_0

    instance-of v5, p1, Lcom/sec/android/gallery3d/data/TimeAllAlbum;

    if-eqz v5, :cond_0

    .line 1065
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1066
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1067
    .local v0, "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1068
    goto :goto_0

    .line 1070
    .end local v0    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 1071
    .local v3, "obj":Ljava/lang/Integer;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1074
    .end local v3    # "obj":Ljava/lang/Integer;
    :cond_1
    :goto_1
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setCurrentMediaSetSelectedCount(I)V

    .line 1075
    return v4

    .line 1071
    .restart local v3    # "obj":Ljava/lang/Integer;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getSelectedIndex(Lcom/sec/android/gallery3d/data/MediaObject;)I
    .locals 2
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 1130
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->indexMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1131
    .local v0, "index":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getSelectedMediaSetMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1050
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 3
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v1, 0x0

    .line 1054
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    if-nez v2, :cond_0

    .line 1058
    :goto_0
    return v1

    .line 1056
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1057
    .local v0, "obj":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1058
    .local v1, "selCnt":I
    :cond_1
    goto :goto_0
.end method

.method public getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 205
    :goto_0
    return v0

    .line 200
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mTotal:I

    if-gez v0, :cond_1

    .line 201
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsAlbumSet:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mTotal:I

    .line 205
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mTotal:I

    goto :goto_0

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    goto :goto_1
.end method

.method public getTotalItemList()Ljava/util/LinkedList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 742
    .local v1, "itemList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v4

    .line 743
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 744
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_1

    .line 745
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 743
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 746
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v2, :cond_0

    .line 747
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    invoke-virtual {v2, v5, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 750
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 751
    return-object v1
.end method

.method public getTotalSelectedItems()I
    .locals 7

    .prologue
    .line 621
    const/4 v0, 0x0

    .line 623
    .local v0, "count":I
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 625
    .local v3, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v4, v3, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_0

    .line 626
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v3    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    .line 629
    .restart local v3    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 631
    .end local v3    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    monitor-exit v5

    .line 635
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    return v0

    .line 631
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 632
    :catch_0
    move-exception v1

    .line 633
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/gallery3d/ui/SelectionManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public inDeleteSelectionMode()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInDeleteSelectionMode:Z

    return v0
.end method

.method public inExpansionMode()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInExpansionMode:Z

    return v0
.end method

.method public inSelectAllMode()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    return v0
.end method

.method public inSelectionMode()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInSelectionMode:Z

    return v0
.end method

.method public isAnyBrokenImageSelected()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1018
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1027
    :goto_0
    return v2

    .line 1021
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v3

    .line 1022
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1023
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1024
    const/4 v2, 0x1

    monitor-exit v3

    goto :goto_0

    .line 1026
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public isEnterEventSelectionMode()Z
    .locals 1

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsEnterEventSelectionMode:Z

    return v0
.end method

.method public isGolfShotImagePresentInSelectedItems()Z
    .locals 4

    .prologue
    .line 981
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 982
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 983
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 984
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsGolfShotImage:Z

    if-eqz v2, :cond_0

    .line 985
    const/4 v2, 0x1

    monitor-exit v3

    .line 989
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 982
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 988
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 989
    const/4 v2, 0x0

    goto :goto_1

    .line 988
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isHideItemInSelectionMode()Z
    .locals 1

    .prologue
    .line 616
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsHideItemInSelectionMode:Z

    return v0
.end method

.method public isImagePresentInSelectedItems()Z
    .locals 4

    .prologue
    .line 957
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 958
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 959
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 960
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsImage:Z

    if-eqz v2, :cond_0

    .line 961
    const/4 v2, 0x1

    monitor-exit v3

    .line 965
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 958
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 964
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 965
    const/4 v2, 0x0

    goto :goto_1

    .line 964
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isItemSelected(Lcom/sec/android/gallery3d/data/Path;)Z
    .locals 2
    .param p1, "itemId"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isOnlyJpegSelected()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1005
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1014
    :goto_0
    return v2

    .line 1008
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v3

    .line 1009
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1010
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "jpeg"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1011
    monitor-exit v3

    goto :goto_0

    .line 1013
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1014
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isPenSelectionMode()Z
    .locals 1

    .prologue
    .line 1211
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsPenSelectionMode:Z

    return v0
.end method

.method public isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSoundShotImagePresentInSelectedItems()Z
    .locals 4

    .prologue
    .line 969
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 970
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 971
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 972
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsSoundShotImage:Z

    if-eqz v2, :cond_0

    .line 973
    const/4 v2, 0x1

    monitor-exit v3

    .line 977
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 970
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 976
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 977
    const/4 v2, 0x0

    goto :goto_1

    .line 976
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isSupportShare()Z
    .locals 4

    .prologue
    .line 1031
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1032
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1033
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 1034
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsSupportShare:Z

    if-nez v2, :cond_0

    .line 1035
    const/4 v2, 0x0

    monitor-exit v3

    .line 1039
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 1032
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1038
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 1039
    const/4 v2, 0x1

    goto :goto_1

    .line 1038
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isVideoPresentInSelectedItems()Z
    .locals 4

    .prologue
    .line 899
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v3

    .line 900
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 901
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 902
    .local v1, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->mIsContainsVideo:Z

    if-eqz v2, :cond_0

    .line 903
    const/4 v2, 0x1

    monitor-exit v3

    .line 907
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :goto_1
    return v2

    .line 900
    .restart local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 906
    .end local v1    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    monitor-exit v3

    .line 907
    const/4 v2, 0x0

    goto :goto_1

    .line 906
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public leaveSelectionMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 172
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setExpansionMode(Z)V

    .line 173
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setDeleteSelectMode(Z)V

    .line 174
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setPenSelectionMode(Z)V

    .line 175
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInSelectionMode:Z

    if-nez v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInSelectionMode:Z

    .line 180
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->exitEventFromSelectionMode()V

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 186
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;->onSelectionModeChange(I)V

    goto :goto_0
.end method

.method public remove(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 2
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 684
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 685
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 686
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 688
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 691
    :cond_0
    return-void

    .line 686
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public remove(Lcom/sec/android/gallery3d/data/MediaObject;I)V
    .locals 0
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "index"    # I

    .prologue
    .line 1116
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1117
    return-void
.end method

.method public remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 10
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v9, 0x0

    .line 493
    const/4 v7, 0x0

    .line 494
    .local v7, "tempSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeAllSelectMode:Z

    if-eqz v8, :cond_4

    instance-of v8, p1, Lcom/sec/android/gallery3d/data/TimeAllAlbum;

    if-eqz v8, :cond_4

    .line 495
    const/4 v0, 0x0

    .line 496
    .local v0, "bFound":Z
    const/4 v1, 0x0

    .local v1, "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object v8, p2

    .line 497
    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getParentSetPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 498
    .local v3, "currentMediaParentPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 499
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 500
    .restart local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 501
    const/4 v0, 0x1

    .line 505
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    :cond_1
    if-eqz v0, :cond_2

    .line 506
    move-object v7, v1

    .line 511
    .end local v0    # "bFound":Z
    .end local v1    # "clusterAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "currentMediaParentPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    if-nez v7, :cond_5

    .line 522
    :cond_3
    :goto_1
    return-void

    .line 509
    :cond_4
    move-object v7, p1

    goto :goto_0

    .line 513
    :cond_5
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 514
    .local v6, "obj":Ljava/lang/Integer;
    if-eqz v6, :cond_7

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 516
    .local v2, "count":I
    :goto_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v8, p2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 517
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 518
    add-int/lit8 v2, v2, -0x1

    .line 519
    if-gez v2, :cond_6

    move v2, v9

    .line 520
    :cond_6
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .end local v2    # "count":I
    :cond_7
    move v2, v9

    .line 514
    goto :goto_2
.end method

.method public remove(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V
    .locals 6
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<+",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 477
    .local p2, "mediaItemList":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez p2, :cond_0

    .line 490
    :goto_0
    return-void

    .line 480
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 481
    .local v3, "obj":Ljava/lang/Integer;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 483
    .local v0, "count":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 484
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 485
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 486
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 481
    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 489
    .restart local v0    # "count":I
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public remove(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 525
    .local p1, "mediaItemList":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez p1, :cond_1

    .line 535
    :cond_0
    return-void

    .line 527
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 528
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 529
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0

    .line 531
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 532
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->findAgainByPathAndRemove(Lcom/sec/android/gallery3d/data/Path;)V

    goto :goto_0
.end method

.method public removeAll()V
    .locals 2

    .prologue
    .line 587
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 588
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 589
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 590
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v1

    .line 591
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 592
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 593
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 594
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 595
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 596
    return-void

    .line 589
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 592
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 595
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method

.method public removeAllItemInMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 10
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 538
    if-nez p1, :cond_1

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    instance-of v7, p1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v7, :cond_4

    .line 541
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    monitor-enter v8

    .line 542
    :try_start_0
    new-instance v7, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 543
    .local v5, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v5, :cond_2

    instance-of v7, v5, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v7, :cond_2

    .line 544
    move-object v0, v5

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    move-object v7, v0

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v1

    .line 545
    .local v1, "bucketId":I
    const/4 v7, -0x1

    if-eq v1, v7, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v7

    if-ne v1, v7, :cond_2

    .line 546
    invoke-virtual {p0, p1, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1

    .line 550
    .end local v1    # "bucketId":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 552
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    invoke-virtual {p1, v7, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 553
    .local v4, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 554
    .local v3, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v7, v3}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 555
    invoke-virtual {p0, p1, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_2

    .line 557
    :cond_6
    if-eqz v3, :cond_5

    .line 558
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 559
    .local v6, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->findAgainByPathAndRemove(Lcom/sec/android/gallery3d/data/Path;)V

    goto :goto_2
.end method

.method public removeSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    .line 1087
    if-eqz p1, :cond_0

    .line 1088
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 1089
    .local v0, "itemsCnt":I
    sub-int v1, v0, p2

    .line 1090
    .local v1, "remainCnt":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    if-lez v1, :cond_1

    .end local v1    # "remainCnt":I
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1092
    .end local v0    # "itemsCnt":I
    :cond_0
    return-void

    .line 1090
    .restart local v0    # "itemsCnt":I
    .restart local v1    # "remainCnt":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 6
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 873
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 874
    .local v2, "share":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;

    .line 875
    .local v3, "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->media:Lcom/sec/android/gallery3d/data/MediaObject;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;->media:Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 876
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mShare:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 878
    monitor-exit v5

    .line 886
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "share":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;>;"
    .end local v3    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :cond_1
    :goto_0
    return-void

    .line 878
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "share":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;>;"
    .restart local v3    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    .line 882
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "share":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;>;"
    .end local v3    # "sp":Lcom/sec/android/gallery3d/ui/SelectionManager$ShareProperty;
    :catch_0
    move-exception v0

    .line 883
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v4, Lcom/sec/android/gallery3d/ui/SelectionManager;->TAG:Ljava/lang/String;

    const-string v5, "ArrayIndexOutOfBoundsException at removeShareProperty"

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeShareProperty(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 889
    .local p1, "mediaItemList":Ljava/util/List;, "Ljava/util/List<+Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez p1, :cond_0

    .line 896
    :goto_0
    return-void

    .line 891
    :cond_0
    monitor-enter p1

    .line 892
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 893
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1

    .line 895
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public selectAll()V
    .locals 2

    .prologue
    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInverseSelection:Z

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mClickedSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 124
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mTotal:I

    .line 125
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;->onSelectionModeChange(I)V

    .line 127
    :cond_0
    return-void
.end method

.method public setAlbumMode(Z)V
    .locals 0
    .param p1, "isAlbumMode"    # Z

    .prologue
    .line 351
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsAlbumSet:Z

    .line 352
    return-void
.end method

.method public setAutoLeaveSelectionMode(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mAutoLeave:Z

    .line 115
    return-void
.end method

.method public setCurrentIndex(I)V
    .locals 0
    .param p1, "currentPosition"    # I

    .prologue
    .line 1042
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mCurrentIndex:I

    .line 1043
    return-void
.end method

.method public setCurrentMediaSetSelectedCount(I)V
    .locals 0
    .param p1, "currentMediaSetSelectedCount"    # I

    .prologue
    .line 1108
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mCurrentMediaSetSelectedCount:I

    .line 1109
    return-void
.end method

.method public setDeleteSelectMode(Z)V
    .locals 0
    .param p1, "isDeleteMode"    # Z

    .prologue
    .line 156
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInDeleteSelectionMode:Z

    .line 157
    return-void
.end method

.method public setExpansionMode(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mInExpansionMode:Z

    .line 149
    return-void
.end method

.method public setPenSelectionMode(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1207
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mIsPenSelectionMode:Z

    .line 1208
    return-void
.end method

.method public setSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    .line 1095
    if-eqz p1, :cond_0

    .line 1096
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1098
    :cond_0
    return-void
.end method

.method public setSelectionListener(Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mListener:Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;

    .line 119
    return-void
.end method

.method public setSourceMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p1, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 346
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSourceMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 347
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mTotal:I

    .line 348
    return-void
.end method

.method public toggle(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 3
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 362
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 364
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 365
    const/4 v0, 0x0

    .line 371
    .local v0, "count":I
    :goto_0
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_0

    .line 372
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    :cond_0
    return-void

    .line 367
    .end local v0    # "count":I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 368
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 369
    const/4 v0, 0x1

    .restart local v0    # "count":I
    goto :goto_0
.end method

.method public update(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V
    .locals 8
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1158
    .local p2, "newDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v6

    .line 1160
    .local v6, "tempList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1161
    .local v4, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v3, 0x1

    .line 1163
    .local v3, "isRemoved":Z
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1165
    .local v5, "path":Ljava/lang/String;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1166
    .local v0, "data":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1167
    const/4 v3, 0x0

    .line 1173
    .end local v0    # "data":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    if-eqz v3, :cond_0

    .line 1174
    invoke-virtual {p0, p1, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0

    .line 1177
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "isRemoved":Z
    .end local v4    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v5    # "path":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public update(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;)Z
    .locals 9
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1180
    .local p2, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x0

    .line 1182
    .local v4, "isUpdated":Z
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v7

    .line 1184
    .local v7, "tempList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1185
    .local v5, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v3, 0x1

    .line 1187
    .local v3, "isRemoved":Z
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1189
    .local v6, "path":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1190
    .local v0, "data":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1191
    const/4 v3, 0x0

    .line 1197
    .end local v0    # "data":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    if-eqz v3, :cond_0

    .line 1198
    invoke-virtual {p0, p1, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1199
    const/4 v4, 0x1

    goto :goto_0

    .line 1203
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "isRemoved":Z
    .end local v5    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v6    # "path":Ljava/lang/String;
    :cond_3
    return v4
.end method

.method public final updateSelectedCountMap(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)Z
    .locals 13
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1215
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v12}, Ljava/util/HashMap;->size()I

    move-result v12

    if-eq v11, v12, :cond_1

    move v2, v9

    .line 1216
    .local v2, "isChanged":Z
    :goto_0
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->clear()V

    .line 1217
    const/4 v8, 0x0

    .local v8, "setIndex":I
    :goto_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v11

    if-ge v8, v11, :cond_7

    .line 1218
    invoke-virtual {p1, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 1219
    .local v5, "mSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v5, :cond_4

    .line 1220
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v11

    invoke-virtual {v5, v10, v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 1221
    .local v4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v0, 0x0

    .line 1222
    .local v0, "count":I
    if-eqz v4, :cond_2

    .line 1223
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1224
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mediaList:Ljava/util/LinkedList;

    invoke-virtual {v11, v3}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1225
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "isChanged":Z
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "mSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v8    # "setIndex":I
    :cond_1
    move v2, v10

    .line 1215
    goto :goto_0

    .line 1229
    .restart local v0    # "count":I
    .restart local v2    # "isChanged":Z
    .restart local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v5    # "mSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v8    # "setIndex":I
    :cond_2
    if-nez v2, :cond_3

    .line 1230
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-virtual {v11, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 1231
    .local v7, "selectedCount":Ljava/lang/Integer;
    if-nez v7, :cond_5

    move v6, v10

    .line 1232
    .local v6, "oldCount":I
    :goto_3
    if-eq v6, v0, :cond_6

    move v2, v9

    .line 1234
    .end local v6    # "oldCount":I
    .end local v7    # "selectedCount":Ljava/lang/Integer;
    :cond_3
    :goto_4
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/SelectionManager;->mSelectedCountMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v5, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1217
    .end local v0    # "count":I
    .end local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1231
    .restart local v0    # "count":I
    .restart local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v7    # "selectedCount":Ljava/lang/Integer;
    :cond_5
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_3

    .restart local v6    # "oldCount":I
    :cond_6
    move v2, v10

    .line 1232
    goto :goto_4

    .line 1237
    .end local v0    # "count":I
    .end local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "mSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "oldCount":I
    .end local v7    # "selectedCount":Ljava/lang/Integer;
    :cond_7
    return v2
.end method
