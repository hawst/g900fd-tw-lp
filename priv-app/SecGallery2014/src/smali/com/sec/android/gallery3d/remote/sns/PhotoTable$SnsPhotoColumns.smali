.class public interface abstract Lcom/sec/android/gallery3d/remote/sns/PhotoTable$SnsPhotoColumns;
.super Ljava/lang/Object;
.source "PhotoTable.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/RemotePhotoEntry$PhotoColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsPhotoColumns"
.end annotation


# static fields
.field public static final COMMENTS_COUNT:Ljava/lang/String; = "comments_count"

.field public static final LIKES_COUNT:Ljava/lang/String; = "likes_count"
