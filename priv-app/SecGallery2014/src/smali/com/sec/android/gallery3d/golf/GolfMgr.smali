.class public Lcom/sec/android/gallery3d/golf/GolfMgr;
.super Ljava/lang/Object;
.source "GolfMgr.java"


# static fields
.field public static final GOLF_TEMP_PATH:Ljava/lang/String;

.field public static final GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.thumbnails/golf/tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/golf/GolfMgr;->GOLF_TEMP_PATH:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Golf/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/golf/GolfMgr;->GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/gallery3d/golf/GolfMgr;->GOLF_TEMP_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 41
    .local v0, "tmpDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 43
    return-void
.end method

.method public static getJpgTempFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p0, "folder"    # Ljava/lang/String;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "allFrame"    # Z

    .prologue
    .line 61
    :try_start_0
    const-string v6, ".golf"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 63
    .local v3, "indexEnd":I
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 65
    .local v2, "indexBegin":I
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "fileName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    .local v4, "strBuild":Ljava/lang/StringBuilder;
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 75
    .local v5, "tempPath":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    const-string v6, ".jpg"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-static {v5, p1, p2}, Lcom/sec/android/gallery3d/golf/GolfDecoder;->generateGolfFile(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 81
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 89
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "indexBegin":I
    .end local v3    # "indexEnd":I
    .end local v4    # "strBuild":Ljava/lang/StringBuilder;
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 83
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 89
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static isGolfFileSelected(Landroid/content/Context;)Z
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x1

    const/4 v14, 0x0

    .line 97
    move-object v12, p0

    check-cast v12, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v11

    .line 98
    .local v11, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 100
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v12, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v12, :cond_2

    .line 101
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v9

    .line 102
    .local v9, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 104
    .local v8, "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v8, :cond_0

    .line 108
    instance-of v12, v8, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v12, :cond_0

    move-object v10, v8

    .line 109
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 110
    .local v10, "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 111
    .local v0, "count":I
    invoke-virtual {v10, v14, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 113
    .local v5, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 114
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v12, v6, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v12, :cond_1

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v12

    if-eqz v12, :cond_1

    move v12, v13

    .line 128
    .end local v0    # "count":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v6    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v8    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v9    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v10    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return v12

    .line 120
    :cond_2
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    .line 121
    .local v7, "selectedCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v7, :cond_4

    .line 122
    invoke-virtual {v11, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    .line 123
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v12, v6, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v12, :cond_3

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v6    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v12

    if-eqz v12, :cond_3

    move v12, v13

    .line 124
    goto :goto_0

    .line 121
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v2    # "i":I
    .end local v7    # "selectedCount":I
    :cond_4
    move v12, v14

    .line 128
    goto :goto_0
.end method


# virtual methods
.method public CreateThumbnails(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "golfFilePath"    # Ljava/lang/String;

    .prologue
    .line 49
    sget-object v1, Lcom/sec/android/gallery3d/golf/GolfMgr;->GOLF_TEMP_PATH:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Lcom/sec/android/gallery3d/golf/GolfMgr;->getJpgTempFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "tempFilePath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method
