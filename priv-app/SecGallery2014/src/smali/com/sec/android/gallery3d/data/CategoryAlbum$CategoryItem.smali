.class public Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;
.super Ljava/lang/Object;
.source "CategoryAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/CategoryAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CategoryItem"
.end annotation


# instance fields
.field mDate:Ljava/lang/Long;

.field mID:I

.field mPath:Lcom/sec/android/gallery3d/data/Path;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/Long;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "date"    # Ljava/lang/Long;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 70
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;->mDate:Ljava/lang/Long;

    .line 71
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbum$CategoryItem;->mID:I

    .line 72
    return-void
.end method
