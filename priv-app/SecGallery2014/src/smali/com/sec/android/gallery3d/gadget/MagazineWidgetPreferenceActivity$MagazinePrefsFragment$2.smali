.class Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;
.super Ljava/lang/Object;
.source "MagazineWidgetPreferenceActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

.field final synthetic val$entry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

    iput-object p2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;->val$entry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 181
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "interval selected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->access$200()Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    move-result-object v3

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalSelected:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->access$100(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->access$200()Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 183
    .local v0, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;->val$entry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->access$200()Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    move-result-object v2

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalSelected:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->access$100(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;)I

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    .line 184
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;->val$entry:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 185
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 186
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 187
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

    # invokes: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updateIntervalSummary()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->access$300(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;)V

    .line 188
    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

    # invokes: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updateCurrentWidget()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->access$400(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;)V

    .line 189
    return-void
.end method
