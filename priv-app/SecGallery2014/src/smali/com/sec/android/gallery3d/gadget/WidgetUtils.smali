.class public Lcom/sec/android/gallery3d/gadget/WidgetUtils;
.super Ljava/lang/Object;
.source "WidgetUtils.java"


# static fields
.field private static final DEFAULT_IMAGE_PATH:Ljava/lang/String; = "/local/image/item/"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SCREEN_SIZE_FOR_LARGE_THUMBNAIL:I = 0x1fa400

.field public static final SET_BACKGOURND_RESOURCE:Ljava/lang/String; = "setBackgroundResource"

.field public static final SHOW_WIDGET_TYPE_DIALOG:Ljava/lang/String; = "SHOW_WIDGET_TYPE"

.field private static final TAG:Ljava/lang/String; = "WidgetUtils"

.field private static final WHERE_PERSON:Ljava/lang/String; = "person_id="

.field private static final WIDGET_BITMAP_HEIGHT_MAX:I = 0x320

.field private static final WIDGET_BITMAP_WIDTH_MAX:I = 0x280

.field private static mFrameHeightMax:I

.field private static mFrameWidthMax:I

.field private static sStackPhotoHeight:I

.field private static sStackPhotoWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 67
    const/16 v0, 0xdc

    sput v0, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoWidth:I

    .line 68
    const/16 v0, 0xaa

    sput v0, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoHeight:I

    .line 75
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->PROJECTION:[Ljava/lang/String;

    .line 272
    sput v3, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameWidthMax:I

    .line 273
    sput v3, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameHeightMax:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method

.method private static calcFrameMaxSize(Landroid/content/Context;)[I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 335
    const/4 v2, 0x2

    new-array v1, v2, [I

    .line 337
    .local v1, "result":[I
    invoke-static {p0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    .line 339
    .local v0, "displayPixels":I
    const v2, 0x1fa400

    if-le v0, v2, :cond_0

    .line 340
    const/16 v2, 0x500

    aput v2, v1, v3

    .line 341
    const/16 v2, 0x640

    aput v2, v1, v4

    .line 350
    :goto_0
    return-object v1

    .line 342
    :cond_0
    const v2, 0x7e900

    if-lt v0, v2, :cond_1

    .line 343
    const/16 v2, 0x280

    aput v2, v1, v3

    .line 344
    const/16 v2, 0x320

    aput v2, v1, v4

    goto :goto_0

    .line 346
    :cond_1
    const/16 v2, 0x1c0

    aput v2, v1, v3

    .line 347
    const/16 v2, 0x230

    aput v2, v1, v4

    goto :goto_0
.end method

.method private static calcFrameSubSize(Landroid/content/Context;II)[I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "frameType"    # I
    .param p2, "index"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 297
    invoke-static {p0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->calcFrameMaxSize(Landroid/content/Context;)[I

    move-result-object v0

    .line 298
    .local v0, "frameSize":[I
    const/4 v1, 0x0

    .line 299
    .local v1, "isLandscapeMode":Z
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFonbletPictureFrame:Z

    if-eqz v4, :cond_0

    .line 300
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    move v1, v2

    .line 301
    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 326
    :cond_1
    :goto_1
    return-object v0

    :cond_2
    move v1, v3

    .line 300
    goto :goto_0

    .line 303
    :pswitch_0
    if-nez v1, :cond_1

    .line 304
    aget v3, v0, v2

    div-int/lit8 v3, v3, 0x2

    aput v3, v0, v2

    goto :goto_1

    .line 307
    :pswitch_1
    if-nez v1, :cond_1

    .line 308
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFonbletPictureFrame:Z

    if-eqz v4, :cond_3

    .line 309
    aget v4, v0, v2

    mul-int/lit8 v4, v4, 0x2

    div-int/lit8 v4, v4, 0x3

    aput v4, v0, v2

    .line 312
    :goto_2
    if-eqz p2, :cond_1

    .line 313
    aget v2, v0, v3

    div-int/lit8 v2, v2, 0x2

    aput v2, v0, v3

    goto :goto_1

    .line 311
    :cond_3
    aget v4, v0, v2

    div-int/lit8 v4, v4, 0x2

    aput v4, v0, v2

    goto :goto_2

    .line 317
    :pswitch_2
    if-nez v1, :cond_1

    .line 318
    aget v4, v0, v3

    div-int/lit8 v4, v4, 0x2

    aput v4, v0, v3

    .line 319
    if-eqz p2, :cond_1

    .line 320
    aget v3, v0, v2

    div-int/lit8 v3, v3, 0x2

    aput v3, v0, v2

    goto :goto_1

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static createEasyWidgetPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 381
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 382
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 383
    const/high16 v1, 0x10000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static createMagazineCameraPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 409
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 410
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 412
    const-string v1, "return-uri"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 413
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static createMagazinePreferencePIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 397
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 398
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 401
    const-string v1, ":android:no_headers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 402
    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static createMagazineWidgetPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 388
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 389
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 390
    const-string v1, "SHOW_WIDGET_TYPE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 392
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static createPreferencePIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 425
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 426
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 429
    const-string v1, ":android:no_headers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 430
    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 431
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 433
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static createSamsungWidgetBitmap(Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "image"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 361
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 362
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 363
    const-string v1, "WidgetUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to get image of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/4 v0, 0x0

    .line 366
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object v0
.end method

.method public static createTabToAddPIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 371
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 373
    const-string v1, "SHOW_WIDGET_TYPE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 374
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 376
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static createWidgetBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "rotation"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 100
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 101
    .local v5, "w":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 104
    .local v1, "h":I
    div-int/lit8 v6, p1, 0x5a

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_0

    .line 105
    sget v6, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoWidth:I

    int-to-float v6, v6

    int-to-float v7, v5

    div-float/2addr v6, v7

    sget v7, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoHeight:I

    int-to-float v7, v7

    int-to-float v8, v1

    div-float/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 112
    .local v3, "scale":F
    :goto_0
    sget v6, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoWidth:I

    sget v7, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoHeight:I

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 114
    .local v4, "target":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 115
    .local v0, "canvas":Landroid/graphics/Canvas;
    sget v6, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoWidth:I

    int-to-float v6, v6

    div-float/2addr v6, v9

    sget v7, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoHeight:I

    int-to-float v7, v7

    div-float/2addr v7, v9

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 116
    int-to-float v6, p1

    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 117
    invoke-virtual {v0, v3, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 118
    new-instance v2, Landroid/graphics/Paint;

    const/4 v6, 0x6

    invoke-direct {v2, v6}, Landroid/graphics/Paint;-><init>(I)V

    .line 119
    .local v2, "paint":Landroid/graphics/Paint;
    neg-int v6, v5

    int-to-float v6, v6

    div-float/2addr v6, v9

    neg-int v7, v1

    int-to-float v7, v7

    div-float/2addr v7, v9

    invoke-virtual {v0, p0, v6, v7, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    return-object v4

    .line 108
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "paint":Landroid/graphics/Paint;
    .end local v3    # "scale":F
    .end local v4    # "target":Landroid/graphics/Bitmap;
    :cond_0
    sget v6, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoWidth:I

    int-to-float v6, v6

    int-to-float v7, v1

    div-float/2addr v6, v7

    sget v7, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoHeight:I

    int-to-float v7, v7

    int-to-float v8, v5

    div-float/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .restart local v3    # "scale":F
    goto :goto_0
.end method

.method public static createWidgetBitmap(Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "image"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 90
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 92
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 93
    const-string v1, "WidgetUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to get image of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const/4 v1, 0x0

    .line 96
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createWidgetBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public static getAppWidgetIds(Landroid/content/Context;)[I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 519
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 520
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/gallery3d/gadget/PhotoAppWidgetProvider;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    .local v2, "provider":Landroid/content/ComponentName;
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 524
    .local v0, "appWidgetIds":[I
    return-object v0
.end method

.method private static getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 483
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 484
    .local v0, "config":Landroid/graphics/Bitmap$Config;
    if-nez v0, :cond_0

    .line 485
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 487
    :cond_0
    return-object v0
.end method

.method private static getDisplayPixels(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 354
    const-string/jumbo v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 355
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 356
    .local v1, "outSize":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 357
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v3

    return v2
.end method

.method public static getIntervalLayoutResId(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 168
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 169
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v1, -0x1

    .line 170
    .local v1, "resId":I
    iget v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    packed-switch v2, :pswitch_data_0

    .line 181
    :goto_0
    return v1

    .line 172
    :pswitch_0
    const v1, 0x7f0300b7

    .line 173
    goto :goto_0

    .line 175
    :pswitch_1
    const v1, 0x7f0300b8

    .line 176
    goto :goto_0

    .line 178
    :pswitch_2
    const v1, 0x7f0300b9

    goto :goto_0

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getLayoutResId(I)I
    .locals 1
    .param p0, "frameType"    # I

    .prologue
    .line 148
    packed-switch p0, :pswitch_data_0

    .line 163
    const v0, 0x7f0300b5

    :goto_0
    return v0

    .line 150
    :pswitch_0
    const v0, 0x7f030083

    goto :goto_0

    .line 152
    :pswitch_1
    const v0, 0x7f030082

    goto :goto_0

    .line 154
    :pswitch_2
    const v0, 0x7f030081

    goto :goto_0

    .line 156
    :pswitch_3
    const v0, 0x7f0300b3

    goto :goto_0

    .line 158
    :pswitch_4
    const v0, 0x7f0300b4

    goto :goto_0

    .line 160
    :pswitch_5
    const v0, 0x7f0300b2

    goto :goto_0

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static getMagazineWidgetIds(Landroid/content/Context;)[I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 528
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 529
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    .local v2, "provider":Landroid/content/ComponentName;
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 533
    .local v0, "appWidgetIds":[I
    return-object v0
.end method

.method public static getPersonImagePath(Landroid/content/ContentResolver;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 492
    .local p1, "personIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    .line 494
    .local v7, "faceCursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 495
    .local v10, "path":Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 497
    .local v8, "filePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 498
    .local v11, "personId":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "person_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 500
    .local v3, "selectorPersonId":Ljava/lang/String;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 501
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 503
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/local/image/item/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 504
    invoke-static {v10}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 505
    invoke-static {v10}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 512
    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 509
    :catch_0
    move-exception v6

    .line 510
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 512
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 515
    .end local v3    # "selectorPersonId":Ljava/lang/String;
    .end local v11    # "personId":I
    :cond_3
    return-object v8
.end method

.method public static getSubframeCount(I)I
    .locals 1
    .param p0, "frameType"    # I

    .prologue
    .line 130
    packed-switch p0, :pswitch_data_0

    .line 143
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 134
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 139
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static getWidgetBitmap(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mFrameType"    # I
    .param p3, "index"    # I

    .prologue
    const/4 v5, 0x0

    .line 196
    const/4 v2, 0x0

    .line 197
    .local v2, "fd":Landroid/os/ParcelFileDescriptor;
    const/4 v0, 0x0

    .line 199
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "r"

    invoke-virtual {v6, p1, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 200
    if-nez v2, :cond_0

    .line 201
    const-string v6, "WidgetUtils"

    const-string v7, "Can\'t get file descriptor for "

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    .line 232
    :goto_0
    return-object v5

    .line 205
    :cond_0
    :try_start_1
    invoke-static {p0, p2, p3}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetSize(Landroid/content/Context;II)I

    move-result v4

    .line 207
    .local v4, "widgetSize":I
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 208
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 209
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 210
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6, v3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 211
    const/4 v5, 0x0

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 212
    const-string v5, "WidgetUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bitmap orig : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget v5, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v7, -0x1

    invoke-static {v5, v6, v7, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSize(IIII)I

    move-result v5

    iput v5, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 216
    const-string v5, "WidgetUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "     sample : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6, v3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    mul-int/2addr v5, v6

    if-le v5, v4, :cond_1

    .line 222
    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownToPixels(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_1

    .line 224
    const-string v5, "WidgetUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    resized : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    :cond_1
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    .end local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "widgetSize":I
    :goto_1
    move-object v5, v0

    .line 232
    goto/16 :goto_0

    .line 227
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    const-string v5, "WidgetUtils"

    const-string v6, "Uri not found : "

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 230
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v5

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    throw v5
.end method

.method public static getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "frameType"    # I
    .param p3, "index"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 236
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    .line 237
    :cond_0
    const-string v6, "WidgetUtils"

    const-string v7, "error while getWidgetBitmap because mediaItem or mediaItem.getFilePath is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    .line 269
    :goto_0
    return-object v0

    .line 241
    :cond_1
    const-string v6, "WidgetUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "index : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const/4 v0, 0x0

    .line 243
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePNGFullDecoding:Z

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    const-string v7, ".png"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez p2, :cond_3

    .line 245
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 246
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v5, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 247
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 248
    iput-boolean v9, v2, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 250
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 265
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_1
    const-string v5, "WidgetUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "original : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-static {p0, p2, p3}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->calcFrameSubSize(Landroid/content/Context;II)[I

    move-result-object v3

    .line 267
    .local v3, "size":[I
    const/4 v5, 0x0

    aget v5, v3, v5

    aget v6, v3, v9

    invoke-static {v0, v5, v6, v9}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->resizeDownAndCrop(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 268
    const-string v5, "WidgetUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resized  : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 252
    .end local v3    # "size":[I
    :cond_3
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_4

    .line 254
    const-string v6, "WidgetUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file not exist ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]. return null"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    .line 255
    goto/16 :goto_0

    .line 258
    :cond_4
    const/4 v4, 0x1

    .line 259
    .local v4, "type":I
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_5

    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 260
    const/4 v4, 0x5

    .line 262
    :cond_5
    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v5

    sget-object v6, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    invoke-interface {v5, v6}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto/16 :goto_1
.end method

.method public static getWidgetSize(Landroid/content/Context;II)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mFrameType"    # I
    .param p2, "index"    # I

    .prologue
    const/4 v2, -0x1

    .line 275
    sget v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameWidthMax:I

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameHeightMax:I

    if-ne v1, v2, :cond_1

    .line 276
    :cond_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->calcFrameMaxSize(Landroid/content/Context;)[I

    move-result-object v0

    .line 277
    .local v0, "widgetSize":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    sput v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameWidthMax:I

    .line 278
    const/4 v1, 0x1

    aget v1, v0, v1

    sput v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameHeightMax:I

    .line 281
    .end local v0    # "widgetSize":[I
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 292
    sget v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameWidthMax:I

    sget v2, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameHeightMax:I

    mul-int/2addr v1, v2

    :goto_0
    return v1

    .line 283
    :pswitch_0
    sget v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameWidthMax:I

    sget v2, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameHeightMax:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 286
    :pswitch_1
    if-nez p2, :cond_2

    .line 287
    sget v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameWidthMax:I

    sget v2, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameHeightMax:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 289
    :cond_2
    sget v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameWidthMax:I

    sget v2, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->mFrameHeightMax:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x4

    goto :goto_0

    .line 281
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 85
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoWidth:I

    .line 86
    const v1, 0x7f0d0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->sStackPhotoHeight:I

    .line 87
    return-void
.end method

.method public static isEmpty(Landroid/content/Context;I)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 537
    const/4 v1, 0x0

    .line 538
    .local v1, "result":Z
    const/4 v3, 0x1

    if-ne v3, p1, :cond_0

    .line 539
    new-instance v2, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;-><init>(Landroid/content/Context;)V

    .line 540
    .local v2, "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    check-cast v2, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    .end local v2    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->shuffleAllSize()I

    move-result v0

    .line 542
    .local v0, "count":I
    if-gtz v0, :cond_0

    .line 543
    const/4 v1, 0x1

    .line 545
    .end local v0    # "count":I
    :cond_0
    return v1
.end method

.method public static resizeDownAndCrop(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "srcBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "dstWidth"    # I
    .param p2, "dstHeight"    # I
    .param p3, "recycle"    # Z

    .prologue
    .line 438
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    .line 439
    .local v9, "srcWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 443
    .local v8, "srcHeight":I
    if-gt v9, p1, :cond_0

    if-gt v8, p2, :cond_0

    .line 478
    .end local p0    # "srcBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p0

    .line 446
    .restart local p0    # "srcBitmap":Landroid/graphics/Bitmap;
    :cond_0
    int-to-float v11, p1

    int-to-float v12, v9

    div-float v7, v11, v12

    .line 447
    .local v7, "scaleWidth":F
    int-to-float v11, p2

    int-to-float v12, v8

    div-float v6, v11, v12

    .line 449
    .local v6, "scaleHeight":F
    invoke-static {p0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;

    move-result-object v11

    invoke-static {p1, p2, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 451
    .local v10, "target":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 454
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-static {v7, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 455
    .local v2, "dstScale":F
    invoke-virtual {v1, v2, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 458
    int-to-float v11, v9

    mul-float v5, v11, v2

    .line 459
    .local v5, "resizedWidth":F
    int-to-float v11, v8

    mul-float v4, v11, v2

    .line 461
    .local v4, "resizedHeight":F
    int-to-float v11, p1

    sub-float v11, v5, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    int-to-float v12, p2

    sub-float v12, v4, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-lez v11, :cond_3

    .line 463
    int-to-float v11, p1

    sub-float/2addr v11, v5

    const/high16 v12, 0x40000000    # 2.0f

    div-float v0, v11, v12

    .line 466
    .local v0, "amount":F
    div-float v11, v0, v2

    const/4 v12, 0x0

    invoke-virtual {v1, v11, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 474
    :goto_1
    new-instance v3, Landroid/graphics/Paint;

    const/4 v11, 0x6

    invoke-direct {v3, v11}, Landroid/graphics/Paint;-><init>(I)V

    .line 475
    .local v3, "paint":Landroid/graphics/Paint;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v11

    if-nez v11, :cond_1

    .line 476
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v1, p0, v11, v12, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 477
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    move-object p0, v10

    .line 478
    goto :goto_0

    .line 469
    .end local v0    # "amount":F
    .end local v3    # "paint":Landroid/graphics/Paint;
    :cond_3
    int-to-float v11, p2

    sub-float/2addr v11, v4

    const/high16 v12, 0x40000000    # 2.0f

    div-float v0, v11, v12

    .line 471
    .restart local v0    # "amount":F
    const/4 v11, 0x0

    div-float v12, v0, v2

    invoke-virtual {v1, v11, v12}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_1
.end method
