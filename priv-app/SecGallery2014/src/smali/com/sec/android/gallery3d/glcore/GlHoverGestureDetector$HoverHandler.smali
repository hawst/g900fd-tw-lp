.class Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;
.super Landroid/os/Handler;
.source "GlHoverGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HoverHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .line 104
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 105
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;Landroid/os/Handler;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .line 108
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 109
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 113
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 150
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 151
    return-void

    .line 116
    :pswitch_0
    const/4 v2, 0x0

    .local v2, "dx":I
    const/4 v3, 0x0

    .line 117
    .local v3, "dy":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v0, v4

    .line 118
    .local v0, "currentTime":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mRecongnizedStartTime:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$000(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)I

    move-result v4

    sub-int v1, v0, v4

    .line 120
    .local v1, "duration":I
    int-to-long v4, v1

    const-wide/16 v6, 0x64

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 140
    :goto_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mIsListScroll:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$300(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$400(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x1

    const-wide/16 v6, 0x1e

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 124
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mScrollDirection:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$100(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 138
    :goto_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$HoverHandler;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # invokes: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->scrollBy(II)V
    invoke-static {v4, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$200(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;II)V

    goto :goto_1

    .line 126
    :pswitch_1
    const/16 v3, -0x14

    .line 127
    goto :goto_2

    .line 129
    :pswitch_2
    const/16 v3, 0x14

    .line 130
    goto :goto_2

    .line 132
    :pswitch_3
    const/16 v2, -0x14

    .line 133
    goto :goto_2

    .line 135
    :pswitch_4
    const/16 v2, 0x14

    goto :goto_2

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 124
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
