.class public final enum Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;
.super Ljava/lang/Enum;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GestureType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

.field public static final enum FINGER_ZOOM:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

.field public static final enum GESTURE_NONE:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

.field public static final enum MOTION_TILT:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 532
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    const-string v1, "GESTURE_NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->GESTURE_NONE:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    const-string v1, "FINGER_ZOOM"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->FINGER_ZOOM:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    const-string v1, "MOTION_TILT"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->MOTION_TILT:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    .line 531
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->GESTURE_NONE:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->FINGER_ZOOM:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->MOTION_TILT:Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->$VALUES:[Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 531
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 531
    const-class v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;
    .locals 1

    .prologue
    .line 531
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->$VALUES:[Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/ui/PhotoView$GestureType;

    return-object v0
.end method
