.class Lcom/sec/android/gallery3d/data/EventAlbumManager$6;
.super Ljava/lang/Object;
.source "EventAlbumManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/EventAlbumManager;->prepareCreateEventDialog(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$selectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/EventAlbumManager;Lcom/sec/android/gallery3d/ui/SelectionManager;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iput-object p2, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->val$selectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iput-object p3, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 237
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->val$selectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    # invokes: Lcom/sec/android/gallery3d/data/EventAlbumManager;->getSelectedItemList(Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;
    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->access$300(Lcom/sec/android/gallery3d/data/EventAlbumManager;Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/util/ArrayList;

    move-result-object v5

    .line 238
    .local v5, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/android/gallery3d/data/EventAlbumManager;->createNewEventAlbumName(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;
    invoke-static {v8, v9, v5}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->access$400(Lcom/sec/android/gallery3d/data/EventAlbumManager;Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    .line 239
    .local v6, "newAlbumName":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    invoke-virtual {v8, v5}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v7

    .line 245
    .local v7, "newAlbumTime":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/local/event/all/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 248
    .local v1, "bucketId":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getEventAlbumMap()Ljava/util/ArrayList;

    move-result-object v0

    .line 249
    .local v0, "albumMap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;>;"
    move-object v2, v6

    .line 250
    .local v2, "candidateName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 252
    .local v3, "guideIndex":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    # invokes: Lcom/sec/android/gallery3d/data/EventAlbumManager;->isDuplicated(Ljava/util/ArrayList;ILjava/lang/String;)I
    invoke-static {v8, v0, v1, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->access$500(Lcom/sec/android/gallery3d/data/EventAlbumManager;Ljava/util/ArrayList;ILjava/lang/String;)I

    move-result v4

    .line 253
    .local v4, "index":I
    if-lez v4, :cond_0

    .line 254
    add-int/2addr v3, v4

    .line 255
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 256
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/local/event/all/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 260
    goto :goto_0

    .line 261
    :cond_0
    if-lez v3, :cond_1

    .line 262
    move-object v6, v2

    .line 264
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$6;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/android/gallery3d/data/EventAlbumManager;->showCreateEventDialog(Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;)V
    invoke-static {v8, v9, v5, v1, v6}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->access$600(Lcom/sec/android/gallery3d/data/EventAlbumManager;Landroid/content/Context;Ljava/util/ArrayList;ILjava/lang/String;)V

    .line 265
    return-void
.end method
