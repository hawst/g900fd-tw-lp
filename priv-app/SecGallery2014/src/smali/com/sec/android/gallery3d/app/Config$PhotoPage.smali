.class public Lcom/sec/android/gallery3d/app/Config$PhotoPage;
.super Ljava/lang/Object;
.source "Config.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoPage"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/android/gallery3d/app/Config$PhotoPage;


# instance fields
.field public final filmstripBarSize:I

.field public final filmstripBottomMargin:I

.field public final filmstripContentSize:I

.field public final filmstripGripSize:I

.field public final filmstripGripWidth:I

.field public final filmstripMidMargin:I

.field public final filmstripThumbSize:I

.field public final filmstripTopMargin:I

.field public placeholderColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 56
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d0109

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripTopMargin:I

    .line 57
    const v1, 0x7f0d010a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripMidMargin:I

    .line 58
    const v1, 0x7f0d010b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripBottomMargin:I

    .line 59
    const v1, 0x7f0d010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripThumbSize:I

    .line 60
    const v1, 0x7f0d010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripContentSize:I

    .line 61
    const v1, 0x7f0d0110

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripGripSize:I

    .line 62
    const v1, 0x7f0d0111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripBarSize:I

    .line 63
    const v1, 0x7f0d0112

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripGripWidth:I

    .line 64
    const v1, 0x7f0b000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->placeholderColor:I

    .line 65
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const-class v1, Lcom/sec/android/gallery3d/app/Config$PhotoPage;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->sInstance:Lcom/sec/android/gallery3d/app/Config$PhotoPage;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/Config$PhotoPage;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->sInstance:Lcom/sec/android/gallery3d/app/Config$PhotoPage;

    .line 51
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->sInstance:Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
