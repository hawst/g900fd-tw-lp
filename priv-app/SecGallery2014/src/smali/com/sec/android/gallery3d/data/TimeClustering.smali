.class public Lcom/sec/android/gallery3d/data/TimeClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "TimeClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/TimeClustering$1;,
        Lcom/sec/android/gallery3d/data/TimeClustering$DateComparator;
    }
.end annotation


# static fields
.field private static CLUSTER_SPLIT_MULTIPLIER:I = 0x0

.field private static final GEOGRAPHIC_DISTANCE_CUTOFF_IN_MILES:I = 0x14

.field private static final MAX_CLUSTER_SPLIT_TIME_IN_MS:J = 0x6ddd00L

.field private static final MAX_MAX_CLUSTER_SIZE:I = 0x32

.field private static final MAX_MIN_CLUSTER_SIZE:I = 0xf

.field private static final MIN_CLUSTER_SPLIT_TIME_IN_MS:J = 0xea60L

.field private static final MIN_MAX_CLUSTER_SIZE:I = 0x14

.field private static final MIN_MIN_CLUSTER_SIZE:I = 0x8

.field private static final MIN_PARTITION_CHANGE_FACTOR:I = 0x2

.field private static final NUM_CLUSTERS_TARGETED:I = 0x9

.field private static final PARTITION_CLUSTER_SPLIT_TIME_FACTOR:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mClusterSplitTime:J

.field private mClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Cluster;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

.field private mDecending:Z

.field private mLargeClusterSplitTime:J

.field private mMaxClusterSize:I

.field private mMinClusterSize:I

.field private mNames:[Ljava/lang/String;

.field private final sDateComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/gallery3d/data/TimeClustering;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/TimeClustering;->TAG:Ljava/lang/String;

    .line 62
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/gallery3d/data/TimeClustering;->CLUSTER_SPLIT_MULTIPLIER:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 78
    const-wide/32 v0, 0x3763b0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusterSplitTime:J

    .line 80
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusterSplitTime:J

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mLargeClusterSplitTime:J

    .line 82
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    .line 83
    const/16 v0, 0x23

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMaxClusterSize:I

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mDecending:Z

    .line 90
    new-instance v0, Lcom/sec/android/gallery3d/data/TimeClustering$DateComparator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/data/TimeClustering$DateComparator;-><init>(Lcom/sec/android/gallery3d/data/TimeClustering;Lcom/sec/android/gallery3d/data/TimeClustering$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->sDateComparator:Ljava/util/Comparator;

    .line 108
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mContext:Landroid/content/Context;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Lcom/sec/android/gallery3d/data/Cluster;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/Cluster;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    .line 111
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/TimeClustering;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/TimeClustering;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private compute(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V
    .locals 10
    .param p1, "currentItem"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 220
    if-eqz p1, :cond_7

    .line 221
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 222
    .local v2, "numClusters":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->size()I

    move-result v3

    .line 223
    .local v3, "numCurrClusterItems":I
    const/4 v0, 0x0

    .line 224
    .local v0, "geographicallySeparateItem":Z
    const/4 v1, 0x0

    .line 228
    .local v1, "itemAddedToCurrentCluster":Z
    if-nez v3, :cond_1

    .line 229
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, p1}, Lcom/sec/android/gallery3d/data/Cluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 278
    .end local v0    # "geographicallySeparateItem":Z
    .end local v1    # "itemAddedToCurrentCluster":Z
    .end local v2    # "numClusters":I
    .end local v3    # "numCurrClusterItems":I
    :cond_0
    :goto_0
    return-void

    .line 231
    .restart local v0    # "geographicallySeparateItem":Z
    .restart local v1    # "itemAddedToCurrentCluster":Z
    .restart local v2    # "numClusters":I
    .restart local v3    # "numCurrClusterItems":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->getLastItem()Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    move-result-object v4

    .line 232
    .local v4, "prevItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    invoke-static {v4, p1}, Lcom/sec/android/gallery3d/data/TimeClustering;->isGeographicallySeparated(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 233
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    const/4 v0, 0x1

    .line 249
    :goto_1
    if-nez v1, :cond_0

    .line 250
    new-instance v5, Lcom/sec/android/gallery3d/data/Cluster;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/data/Cluster;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    .line 251
    if-eqz v0, :cond_2

    .line 252
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/android/gallery3d/data/Cluster;->mGeographicallySeparatedFromPrevCluster:Z

    .line 254
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, p1}, Lcom/sec/android/gallery3d/data/Cluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 235
    :cond_3
    iget v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMaxClusterSize:I

    if-le v3, v5, :cond_4

    .line 236
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/TimeClustering;->splitAndAddCurrentCluster()V

    goto :goto_1

    .line 237
    :cond_4
    if-eqz v4, :cond_5

    invoke-static {v4, p1}, Lcom/sec/android/gallery3d/data/TimeClustering;->timeDistance(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)J

    move-result-wide v6

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusterSplitTime:J

    cmp-long v5, v6, v8

    if-gez v5, :cond_5

    .line 239
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, p1}, Lcom/sec/android/gallery3d/data/Cluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 240
    const/4 v1, 0x1

    goto :goto_1

    .line 241
    :cond_5
    if-lez v2, :cond_6

    iget v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    if-ge v3, v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    iget-boolean v5, v5, Lcom/sec/android/gallery3d/data/Cluster;->mGeographicallySeparatedFromPrevCluster:Z

    if-nez v5, :cond_6

    .line 243
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/TimeClustering;->mergeAndAddCurrentCluster()V

    goto :goto_1

    .line 245
    :cond_6
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 258
    .end local v0    # "geographicallySeparateItem":Z
    .end local v1    # "itemAddedToCurrentCluster":Z
    .end local v2    # "numClusters":I
    .end local v3    # "numCurrClusterItems":I
    .end local v4    # "prevItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 263
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 264
    .restart local v2    # "numClusters":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->size()I

    move-result v3

    .line 267
    .restart local v3    # "numCurrClusterItems":I
    iget v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMaxClusterSize:I

    if-le v3, v5, :cond_8

    .line 268
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/TimeClustering;->splitAndAddCurrentCluster()V

    .line 275
    :goto_2
    new-instance v5, Lcom/sec/android/gallery3d/data/Cluster;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/data/Cluster;-><init>()V

    iput-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    goto :goto_0

    .line 269
    :cond_8
    if-lez v2, :cond_9

    iget v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    if-ge v3, v5, :cond_9

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    iget-boolean v5, v5, Lcom/sec/android/gallery3d/data/Cluster;->mGeographicallySeparatedFromPrevCluster:Z

    if-nez v5, :cond_9

    .line 271
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/TimeClustering;->mergeAndAddCurrentCluster()V

    goto :goto_2

    .line 273
    :cond_9
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private getPartitionIndexForCurrentCluster()I
    .locals 26

    .prologue
    .line 301
    const/4 v14, -0x1

    .line 302
    .local v14, "partitionIndex":I
    const/high16 v10, 0x40000000    # 2.0f

    .line 303
    .local v10, "largestChange":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/Cluster;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    .line 304
    .local v3, "currClusterItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/Cluster;->size()I

    move-result v13

    .line 305
    .local v13, "numCurrClusterItems":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    .line 308
    .local v11, "minClusterSize":I
    add-int/lit8 v22, v11, 0x1

    move/from16 v0, v22

    if-le v13, v0, :cond_3

    .line 309
    move v5, v11

    .local v5, "i":I
    :goto_0
    sub-int v22, v13, v11

    move/from16 v0, v22

    if-ge v5, v0, :cond_3

    .line 310
    add-int/lit8 v22, v5, -0x1

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .line 311
    .local v15, "prevItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .line 312
    .local v4, "currItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    add-int/lit8 v22, v5, 0x1

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .line 314
    .local v12, "nextItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    iget-wide v0, v12, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    move-wide/from16 v18, v0

    .line 315
    .local v18, "timeNext":J
    iget-wide v0, v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    move-wide/from16 v16, v0

    .line 316
    .local v16, "timeCurr":J
    iget-wide v0, v15, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    move-wide/from16 v20, v0

    .line 318
    .local v20, "timePrev":J
    const-wide/16 v22, 0x0

    cmp-long v22, v18, v22

    if-eqz v22, :cond_0

    const-wide/16 v22, 0x0

    cmp-long v22, v16, v22

    if-eqz v22, :cond_0

    const-wide/16 v22, 0x0

    cmp-long v22, v20, v22

    if-nez v22, :cond_1

    .line 309
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 320
    :cond_1
    sub-long v22, v18, v16

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    .line 321
    .local v6, "diff1":J
    sub-long v22, v16, v20

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    .line 323
    .local v8, "diff2":J
    long-to-float v0, v6

    move/from16 v22, v0

    long-to-float v0, v8

    move/from16 v23, v0

    const v24, 0x3c23d70a    # 0.01f

    add-float v23, v23, v24

    div-float v22, v22, v23

    long-to-float v0, v8

    move/from16 v23, v0

    long-to-float v0, v6

    move/from16 v24, v0

    const v25, 0x3c23d70a    # 0.01f

    add-float v24, v24, v25

    div-float v23, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 324
    .local v2, "change":F
    cmpl-float v22, v2, v10

    if-lez v22, :cond_0

    .line 325
    invoke-static {v4, v15}, Lcom/sec/android/gallery3d/data/TimeClustering;->timeDistance(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mLargeClusterSplitTime:J

    move-wide/from16 v24, v0

    cmp-long v22, v22, v24

    if-lez v22, :cond_2

    .line 326
    move v14, v5

    .line 327
    move v10, v2

    goto :goto_1

    .line 328
    :cond_2
    invoke-static {v12, v4}, Lcom/sec/android/gallery3d/data/TimeClustering;->timeDistance(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mLargeClusterSplitTime:J

    move-wide/from16 v24, v0

    cmp-long v22, v22, v24

    if-lez v22, :cond_0

    .line 329
    add-int/lit8 v14, v5, 0x1

    .line 330
    move v10, v2

    goto :goto_1

    .line 335
    .end local v2    # "change":F
    .end local v4    # "currItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .end local v5    # "i":I
    .end local v6    # "diff1":J
    .end local v8    # "diff2":J
    .end local v12    # "nextItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .end local v15    # "prevItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .end local v16    # "timeCurr":J
    .end local v18    # "timeNext":J
    .end local v20    # "timePrev":J
    :cond_3
    return v14
.end method

.method private static isGeographicallySeparated(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)Z
    .locals 11
    .param p0, "itemA"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .param p1, "itemB"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    const/4 v10, 0x0

    .line 356
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 370
    :cond_0
    :goto_0
    return v10

    .line 360
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    iget-wide v2, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    iget-wide v4, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iget-wide v6, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->fastDistanceMeters(DDDD)D

    move-result-wide v8

    .line 370
    .local v8, "distance":D
    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->toMile(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v10, v0

    goto :goto_0

    :cond_2
    move v0, v10

    goto :goto_1
.end method

.method private mergeAndAddCurrentCluster()V
    .locals 7

    .prologue
    .line 339
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 340
    .local v2, "numClusters":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Cluster;

    .line 341
    .local v4, "prevCluster":Lcom/sec/android/gallery3d/data/Cluster;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 342
    .local v0, "currClusterItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->size()I

    move-result v3

    .line 343
    .local v3, "numCurrClusterItems":I
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Cluster;->size()I

    move-result v5

    iget v6, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    if-ge v5, v6, :cond_1

    .line 344
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 345
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/Cluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 344
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 347
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v5, v6, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 351
    .end local v1    # "i":I
    :goto_1
    return-void

    .line 349
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private setTimeRange(JI)V
    .locals 7
    .param p1, "timeRange"    # J
    .param p3, "numItems"    # I

    .prologue
    .line 202
    if-eqz p3, :cond_0

    .line 203
    div-int/lit8 v6, p3, 0x9

    .line 206
    .local v6, "meanItemsPerCluster":I
    div-int/lit8 v0, v6, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    .line 207
    mul-int/lit8 v0, v6, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMaxClusterSize:I

    .line 208
    int-to-long v0, p3

    div-long v0, p1, v0

    sget v2, Lcom/sec/android/gallery3d/data/TimeClustering;->CLUSTER_SPLIT_MULTIPLIER:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusterSplitTime:J

    .line 210
    .end local v6    # "meanItemsPerCluster":I
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusterSplitTime:J

    const-wide/32 v2, 0xea60

    const-wide/32 v4, 0x6ddd00

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(JJJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusterSplitTime:J

    .line 211
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusterSplitTime:J

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mLargeClusterSplitTime:J

    .line 212
    iget v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    const/16 v1, 0x8

    const/16 v2, 0xf

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMinClusterSize:I

    .line 215
    iput p3, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mMaxClusterSize:I

    .line 217
    return-void
.end method

.method private splitAndAddCurrentCluster()V
    .locals 7

    .prologue
    .line 281
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 282
    .local v0, "currClusterItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Cluster;->size()I

    move-result v2

    .line 283
    .local v2, "numCurrClusterItems":I
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/TimeClustering;->getPartitionIndexForCurrentCluster()I

    move-result v4

    .line 284
    .local v4, "secondPartitionStartIndex":I
    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 285
    new-instance v3, Lcom/sec/android/gallery3d/data/Cluster;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/data/Cluster;-><init>()V

    .line 286
    .local v3, "partitionedCluster":Lcom/sec/android/gallery3d/data/Cluster;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 287
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/data/Cluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 286
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 289
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    new-instance v3, Lcom/sec/android/gallery3d/data/Cluster;

    .end local v3    # "partitionedCluster":Lcom/sec/android/gallery3d/data/Cluster;
    invoke-direct {v3}, Lcom/sec/android/gallery3d/data/Cluster;-><init>()V

    .line 291
    .restart local v3    # "partitionedCluster":Lcom/sec/android/gallery3d/data/Cluster;
    move v1, v4

    :goto_1
    if-ge v1, v2, :cond_1

    .line 292
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/data/Cluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 291
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 294
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    .end local v1    # "j":I
    .end local v3    # "partitionedCluster":Lcom/sec/android/gallery3d/data/Cluster;
    :goto_2
    return-void

    .line 296
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private static timeDistance(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)J
    .locals 4
    .param p0, "a"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .param p1, "b"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 375
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    iget-wide v2, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public getCluster(I)Ljava/util/ArrayList;
    .locals 5
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Cluster;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Cluster;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 189
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 190
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 191
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_0
    return-object v3
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 386
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 17
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 115
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v11

    .line 140
    .local v11, "total":I
    if-gtz v11, :cond_1

    .line 179
    :cond_0
    return-void

    .line 143
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 144
    .local v4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v11, v14}, Lcom/sec/android/gallery3d/data/TimeClustering;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V

    .line 147
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->sDateComparator:Ljava/util/Comparator;

    invoke-static {v4, v14}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 153
    .local v10, "n":I
    const-wide/16 v8, 0x0

    .line 154
    .local v8, "minTime":J
    const-wide/16 v6, 0x0

    .line 155
    .local v6, "maxTime":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v10, :cond_4

    .line 156
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-wide v12, v14, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 157
    .local v12, "t":J
    const-wide/16 v14, 0x0

    cmp-long v14, v12, v14

    if-nez v14, :cond_2

    .line 155
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 148
    .end local v2    # "i":I
    .end local v6    # "maxTime":J
    .end local v8    # "minTime":J
    .end local v10    # "n":I
    .end local v12    # "t":J
    :catch_0
    move-exception v3

    .line 149
    .local v3, "iae":Ljava/lang/IllegalArgumentException;
    sget-object v14, Lcom/sec/android/gallery3d/data/TimeClustering;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "conversationInfoCursor sorting error "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 158
    .end local v3    # "iae":Ljava/lang/IllegalArgumentException;
    .restart local v2    # "i":I
    .restart local v6    # "maxTime":J
    .restart local v8    # "minTime":J
    .restart local v10    # "n":I
    .restart local v12    # "t":J
    :cond_2
    const-wide/16 v14, 0x0

    cmp-long v14, v8, v14

    if-nez v14, :cond_3

    .line 159
    move-wide v6, v12

    move-wide v8, v12

    goto :goto_2

    .line 161
    :cond_3
    invoke-static {v8, v9, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 162
    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_2

    .line 166
    .end local v12    # "t":J
    :cond_4
    sub-long v14, v6, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v10}, Lcom/sec/android/gallery3d/data/TimeClustering;->setTimeRange(JI)V

    .line 168
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v10, :cond_5

    .line 169
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/gallery3d/data/TimeClustering;->compute(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 172
    :cond_5
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/gallery3d/data/TimeClustering;->compute(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 174
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 175
    .local v5, "m":I
    new-array v14, v5, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mNames:[Ljava/lang/String;

    .line 176
    const/4 v2, 0x0

    :goto_4
    if-ge v2, v5, :cond_0

    .line 177
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mNames:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/Cluster;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/TimeClustering;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/sec/android/gallery3d/data/Cluster;->generateCaption(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v15, v2

    .line 176
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method public setTimeOrder(Z)V
    .locals 0
    .param p1, "decending"    # Z

    .prologue
    .line 380
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/TimeClustering;->mDecending:Z

    .line 381
    return-void
.end method
