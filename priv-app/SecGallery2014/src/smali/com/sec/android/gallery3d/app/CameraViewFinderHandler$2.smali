.class Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;
.super Ljava/lang/Object;
.source "CameraViewFinderHandler.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mEVFStartTime:J

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V
    .locals 2

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->mEVFStartTime:J

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 3
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "i"    # I

    .prologue
    .line 267
    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAccuracyChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/16 v6, 0x64

    const/16 v10, 0x65

    .line 241
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # invokes: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->isHDMIConnected()Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$300(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v4}, [F->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [F

    .line 246
    .local v3, "values":[F
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v4}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_0

    .line 247
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$400(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 248
    const/4 v4, 0x0

    aget v2, v3, v4

    .line 249
    .local v2, "distance":F
    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-nez v4, :cond_2

    .line 250
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->mEVFStartTime:J

    .line 251
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$400(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 252
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$400(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 253
    :cond_2
    const/high16 v4, 0x41000000    # 8.0f

    cmpl-float v4, v2, v4

    if-nez v4, :cond_0

    .line 254
    const-wide/16 v4, 0x3e8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->mEVFStartTime:J

    sub-long/2addr v6, v8

    sub-long v0, v4, v6

    .line 255
    .local v0, "delaytime":J
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$400(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 256
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    .line 257
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$400(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 259
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->stopEVF()V

    goto :goto_0
.end method
