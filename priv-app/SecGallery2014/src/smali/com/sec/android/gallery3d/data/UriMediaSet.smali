.class public Lcom/sec/android/gallery3d/data/UriMediaSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "UriMediaSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field public static final SET_PATH:Ljava/lang/String; = "/uri/mediaset"

.field private static final TAG:Ljava/lang/String; = "UriMediaSet"


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mCount:I

.field mDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field mMid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "mid"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 23
    invoke-static {}, Lcom/sec/android/gallery3d/data/UriMediaSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 20
    iput v2, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mCount:I

    .line 24
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    .line 26
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mMid:Ljava/lang/String;

    .line 27
    iput v2, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mCount:I

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    .line 29
    return-void
.end method

.method private loadDataFromMMS()Ljava/util/ArrayList;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    const-string v1, "content://mms/part"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 34
    .local v2, "partsUri":Landroid/net/Uri;
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v1

    const/4 v1, 0x1

    const-string v5, "mid"

    aput-object v5, v3, v1

    const/4 v1, 0x2

    const-string v5, "ct"

    aput-object v5, v3, v1

    const/4 v1, 0x3

    const-string v5, "_data"

    aput-object v5, v3, v1

    .line 35
    .local v3, "projection":[Ljava/lang/String;
    const-string v4, "mid = ?"

    .line 36
    .local v4, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 37
    .local v8, "cursorFound":Landroid/database/Cursor;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .local v13, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v14

    .line 41
    .local v14, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mMid:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v5, v6

    const-string v6, "_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 47
    if-eqz v8, :cond_3

    .line 48
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 49
    const-string v1, "UriMediaSet"

    const-string v5, "mms image : cursorFound count is 0"

    invoke-static {v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mCount:I

    .line 51
    new-instance v13, Ljava/util/ArrayList;

    .end local v13    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 86
    :goto_0
    return-object v13

    .line 53
    .restart local v13    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 56
    :cond_1
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 57
    .local v10, "foundId":I
    const-string v1, "ct"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 58
    .local v11, "foundType":Ljava/lang/String;
    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 60
    .local v9, "foundData":Ljava/lang/String;
    const/4 v12, 0x0

    .line 61
    .local v12, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://mms/part/"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 62
    .local v16, "uriItem":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/gallery3d/data/UriImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v10}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    .line 64
    .local v7, "childPath":Lcom/sec/android/gallery3d/data/Path;
    const-string v1, "image"

    invoke-virtual {v11, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 65
    invoke-virtual {v14, v7}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v15

    .line 66
    .local v15, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v15, :cond_4

    .line 67
    new-instance v12, Lcom/sec/android/gallery3d/data/UriImage;

    .end local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v12, v1, v7, v5}, Lcom/sec/android/gallery3d/data/UriImage;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V

    .line 71
    .restart local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_1
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    .end local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 84
    .end local v7    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v9    # "foundData":Ljava/lang/String;
    .end local v10    # "foundId":I
    .end local v11    # "foundType":Ljava/lang/String;
    .end local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v16    # "uriItem":Ljava/lang/String;
    :cond_3
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 69
    .restart local v7    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v9    # "foundData":Ljava/lang/String;
    .restart local v10    # "foundId":I
    .restart local v11    # "foundType":Ljava/lang/String;
    .restart local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v16    # "uriItem":Ljava/lang/String;
    :cond_4
    :try_start_2
    move-object v0, v15

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v12, v0

    goto :goto_1

    .line 72
    .end local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_5
    const-string/jumbo v1, "video"

    invoke-virtual {v11, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    invoke-virtual {v14, v7}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v15

    .line 74
    .restart local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v15, :cond_6

    .line 75
    new-instance v12, Lcom/sec/android/gallery3d/data/UriVideo;

    .end local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v12, v1, v7, v5}, Lcom/sec/android/gallery3d/data/UriVideo;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V

    .line 79
    .restart local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_3
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 84
    .end local v7    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v9    # "foundData":Ljava/lang/String;
    .end local v10    # "foundId":I
    .end local v11    # "foundType":Ljava/lang/String;
    .end local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v13    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v16    # "uriItem":Ljava/lang/String;
    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1

    .line 77
    .restart local v7    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v9    # "foundData":Ljava/lang/String;
    .restart local v10    # "foundId":I
    .restart local v11    # "foundType":Ljava/lang/String;
    .restart local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v13    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v16    # "uriItem":Ljava/lang/String;
    :cond_6
    :try_start_3
    move-object v0, v15

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v12, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 3
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    const-string v0, "UriMediaSet"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMediaItem "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int v2, p1, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    :cond_0
    const/4 v0, 0x0

    .line 208
    :goto_0
    return-object v0

    .line 207
    :cond_1
    const-string v0, "UriMediaSet"

    const-string v1, "getMediaItem+"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPositon(Ljava/lang/String;)I
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 190
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 191
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 190
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriMediaSet;->notifyContentChanged()V

    .line 242
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/UriMediaSet;->loadDataFromMMS()Ljava/util/ArrayList;

    move-result-object v0

    .line 232
    .local v0, "newData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 233
    :cond_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    .line 234
    invoke-static {}, Lcom/sec/android/gallery3d/data/UriMediaSet;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataVersion:J

    .line 236
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataVersion:J

    return-wide v2
.end method

.method public reloadDataFromMMS()V
    .locals 26

    .prologue
    .line 91
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v24, "uriItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v22, "typeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v11, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v17, "idList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v2, "content://mms/part"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 97
    .local v3, "partsUri":Landroid/net/Uri;
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string v6, "mid"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string v6, "ct"

    aput-object v6, v4, v2

    const/4 v2, 0x3

    const-string v6, "_data"

    aput-object v6, v4, v2

    .line 98
    .local v4, "projection":[Ljava/lang/String;
    const-string v5, "mid = ?"

    .line 99
    .local v5, "selection":Ljava/lang/String;
    const/4 v9, 0x0

    .line 102
    .local v9, "cursorFound":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mMid:Ljava/lang/String;

    move-object/from16 v25, v0

    aput-object v25, v6, v7

    const-string v7, "_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 108
    if-eqz v9, :cond_5

    .line 109
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_1

    .line 110
    const-string v2, "UriMediaSet"

    const-string v6, "mms image reload: cursorFound count is 0"

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mCount:I

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 119
    :cond_2
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 120
    .local v13, "foundId":I
    const-string v2, "ct"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 121
    .local v14, "foundType":Ljava/lang/String;
    const-string v2, "_data"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 123
    .local v12, "foundData":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mms/part/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 125
    .local v23, "uriItem":Ljava/lang/String;
    const-string v2, "image"

    invoke-virtual {v14, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "video"

    invoke-virtual {v14, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 126
    :cond_3
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 134
    .end local v12    # "foundData":Ljava/lang/String;
    .end local v13    # "foundId":I
    .end local v14    # "foundType":Ljava/lang/String;
    .end local v23    # "uriItem":Ljava/lang/String;
    :cond_5
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 137
    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 139
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eq v2, v6, :cond_9

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 142
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v15, v2, :cond_8

    .line 143
    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 144
    .local v21, "reloadUri":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 145
    .local v16, "id":I
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 146
    .local v20, "reLoadType":Ljava/lang/String;
    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 148
    .local v19, "reLoadData":Ljava/lang/String;
    const/16 v18, 0x0

    .line 149
    .local v18, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    sget-object v2, Lcom/sec/android/gallery3d/data/UriImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 150
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    const-string v2, "image"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 151
    new-instance v18, Lcom/sec/android/gallery3d/data/UriImage;

    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static/range {v21 .. v21}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v8, v6}, Lcom/sec/android/gallery3d/data/UriImage;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V

    .line 152
    .restart local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_6
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 134
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v15    # "i":I
    .end local v16    # "id":I
    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v19    # "reLoadData":Ljava/lang/String;
    .end local v20    # "reLoadType":Ljava/lang/String;
    .end local v21    # "reloadUri":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 154
    .restart local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v15    # "i":I
    .restart local v16    # "id":I
    .restart local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v19    # "reLoadData":Ljava/lang/String;
    .restart local v20    # "reLoadType":Ljava/lang/String;
    .restart local v21    # "reloadUri":Ljava/lang/String;
    :cond_7
    const-string/jumbo v2, "video"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 155
    new-instance v18, Lcom/sec/android/gallery3d/data/UriVideo;

    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static/range {v21 .. v21}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v8, v6}, Lcom/sec/android/gallery3d/data/UriVideo;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V

    .line 156
    .restart local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 161
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v16    # "id":I
    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v19    # "reLoadData":Ljava/lang/String;
    .end local v20    # "reLoadType":Ljava/lang/String;
    .end local v21    # "reloadUri":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mCount:I

    goto/16 :goto_0

    .line 164
    .end local v15    # "i":I
    :cond_9
    const/4 v15, 0x0

    .restart local v15    # "i":I
    :goto_3
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v15, v2, :cond_0

    .line 165
    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 166
    .restart local v21    # "reloadUri":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 167
    .restart local v16    # "id":I
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 168
    .restart local v20    # "reLoadType":Ljava/lang/String;
    invoke-virtual {v11, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 169
    .restart local v19    # "reLoadData":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 171
    .local v10, "data":Ljava/lang/String;
    if-eqz v10, :cond_a

    if-eqz v19, :cond_a

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 172
    const/16 v18, 0x0

    .line 173
    .restart local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    sget-object v2, Lcom/sec/android/gallery3d/data/UriImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 174
    .restart local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    const-string v2, "image"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 175
    new-instance v18, Lcom/sec/android/gallery3d/data/UriImage;

    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static/range {v21 .. v21}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v8, v6}, Lcom/sec/android/gallery3d/data/UriImage;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V

    .line 176
    .restart local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v2, v15, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v2, v15, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 164
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_a
    :goto_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 178
    .restart local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_b
    const-string/jumbo v2, "video"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 179
    new-instance v18, Lcom/sec/android/gallery3d/data/UriVideo;

    .end local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static/range {v21 .. v21}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v8, v6}, Lcom/sec/android/gallery3d/data/UriVideo;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V

    .line 180
    .restart local v18    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v2, v15, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;->mDataList:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v2, v15, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_4
.end method
