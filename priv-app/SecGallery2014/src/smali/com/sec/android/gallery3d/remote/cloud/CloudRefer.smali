.class public Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;
.super Ljava/lang/Object;
.source "CloudRefer.java"


# static fields
.field public static final CLOUD_MEDIA_TYPE_IMAGE:I = 0x1

.field public static final CLOUD_MEDIA_TYPE_UNKNOWN:I = -0x1

.field public static final CLOUD_MEDIA_TYPE_VIDEO:I = 0x3

.field private static final CLOUD_MODE:Ljava/lang/String; = "facetag_mode"

.field public static final STATUS_CACHED:I = 0x1

.field public static final STATUS_NOT_CACHED:I = 0x0

.field public static final STATUS_NOT_THUMB:I = 0x0

.field public static final STATUS_THUMB:I = 0x1

.field protected static final TAG:Ljava/lang/String; = "CloudRefer"

.field public static final XCLOUD_ACTIONTYPE_DOWNLOAD:Ljava/lang/String; = "download"

.field public static final XCLOUD_ACTIONTYPE_UPLOAD:Ljava/lang/String; = "upload"

.field public static final XCLOUD_ACTION_DOWNLOAD:Ljava/lang/String; = "com.baidu.xcloud.netdisk.ACITON_DOWNLOAD_FILES"

.field public static final XCLOUD_ACTION_FAIL:I = 0x0

.field public static final XCLOUD_ACTION_FINISH:Ljava/lang/String; = "com.baidu.xcloud.netdisk.ACTION_FINISH_BRODCAST"

.field public static final XCLOUD_ACTION_SUCC:I = 0x1

.field public static final XCLOUD_ACTION_UPLOAD:Ljava/lang/String; = "com.baidu.xcloud.netdisk.ACTION_UPLOAD_FILES"

.field public static final XCLOUD_EXTRA_DOWNLOAD_DIR:Ljava/lang/String; = "downloaddir"

.field public static final XCLOUD_EXTRA_DOWNLOAD_FILES:Ljava/lang/String; = "downloadfiles"

.field public static final XCLOUD_EXTRA_UPLOAD_DIR:Ljava/lang/String; = "uploaddir"

.field public static final XCLOUD_EXTRA_UPLOAD_FILES:Ljava/lang/String; = "uploadfiles"

.field private static mDropBoxAuthDialog:Landroid/app/Dialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->mDropBoxAuthDialog:Landroid/app/Dialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->mDropBoxAuthDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public static cloudAlbumSync(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 159
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->cloudAlbumsSync(Landroid/content/Context;)V

    .line 160
    return-void
.end method

.method public static cloudAlbumsSync(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    new-instance v1, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer$1;-><init>(Landroid/content/Context;)V

    .line 152
    .local v1, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v2, 0x0

    :try_start_0
    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    const-string v2, "CloudRefer"

    const-string/jumbo v3, "skip cloud sync"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCloudMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 72
    const-string v2, "facetag_mode"

    invoke-virtual {p0, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 73
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-nez v1, :cond_0

    .line 76
    :goto_0
    return v0

    .line 75
    :cond_0
    const-string v2, "facetag_mode"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 76
    .local v0, "cloudMode":Z
    goto :goto_0
.end method

.method public static getDropBoxAuthDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Landroid/app/Dialog;
    .locals 5
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    .line 164
    .local v1, "context":Landroid/content/Context;
    sget-object v2, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->mDropBoxAuthDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    .line 165
    sget-object v2, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->mDropBoxAuthDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->cancel()V

    .line 167
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0e0158

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0159

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 170
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x104000a

    new-instance v3, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer$3;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer$3;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer$2;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer$2;-><init>()V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 194
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    sput-object v2, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->mDropBoxAuthDialog:Landroid/app/Dialog;

    .line 195
    sget-object v2, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->mDropBoxAuthDialog:Landroid/app/Dialog;

    return-object v2
.end method

.method public static isCloudAgentAndAppExist(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 111
    const/4 v3, 0x0

    .line 112
    .local v3, "result":Z
    const/4 v0, 0x0

    .line 113
    .local v0, "agent":Landroid/content/pm/PackageInfo;
    const/4 v1, 0x0

    .line 116
    .local v1, "app":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.android.cloudagent"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 117
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.dropbox.android"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 123
    :goto_0
    if-eqz v0, :cond_0

    .line 124
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-eqz v4, :cond_1

    .line 125
    const/4 v3, 0x1

    .line 132
    :cond_0
    :goto_1
    return v3

    .line 119
    :catch_0
    move-exception v2

    .line 120
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 127
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    if-eqz v1, :cond_0

    .line 128
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static isCloudAgentExist(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    const/4 v2, 0x0

    .line 97
    .local v2, "result":Z
    const/4 v0, 0x0

    .line 99
    .local v0, "agent":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.cloudagent"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 104
    :goto_0
    if-eqz v0, :cond_0

    .line 105
    const/4 v2, 0x1

    .line 107
    :cond_0
    return v2

    .line 100
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isCloudContentSyncOn(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/cloudagent/CloudStore$API;->isCloudAvailable(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 83
    :goto_0
    return v1

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCloudSupported(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudAgentAndAppExist(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDropBoxSignIn(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/cloudagent/CloudStore$API;->isCloudVendorAvailable(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 91
    :goto_0
    return v1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setCloudMode(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cloudMode"    # Z

    .prologue
    .line 65
    const-string v2, "facetag_mode"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 66
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 67
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "facetag_mode"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 68
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 69
    return-void
.end method
