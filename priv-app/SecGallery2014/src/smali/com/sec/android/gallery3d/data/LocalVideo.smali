.class public Lcom/sec/android/gallery3d/data/LocalVideo;
.super Lcom/sec/android/gallery3d/data/LocalMediaItem;
.source "LocalVideo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;
    }
.end annotation


# static fields
.field private static final CONTEXTUAL_URI:Landroid/net/Uri;

.field public static final INDEX_BUCKET_ID:I = 0xa

.field public static final INDEX_CAPTION:I = 0x1

.field public static final INDEX_DATA:I = 0x8

.field public static final INDEX_DATE_ADDED:I = 0x6

.field public static final INDEX_DATE_MODIFIED:I = 0x7

.field public static final INDEX_DATE_TAKEN:I = 0x5

.field public static final INDEX_DURATION:I = 0x9

.field public static final INDEX_ID:I = 0x0

.field public static final INDEX_LATITUDE:I = 0x3

.field public static final INDEX_LONGITUDE:I = 0x4

.field public static final INDEX_MIME_TYPE:I = 0x2

.field public static final INDEX_RESOLUTION:I = 0xc

.field public static final INDEX_SIZE:I = 0xb

.field static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field public static final PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "LocalVideo"


# instance fields
.field public duration:I

.field public durationInSec:I

.field private mIsDeleted:Z

.field public mIsGolf:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    const-string v0, "/local/video/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 52
    const-string v0, "content://media/external/contextural_tags"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->CONTEXTUAL_URI:Landroid/net/Uri;

    .line 72
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "date_added"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "resolution"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "id"    # I

    .prologue
    const/4 v3, 0x0

    .line 104
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalVideo;->nextVersionNumber()J

    move-result-wide v4

    invoke-direct {p0, p1, v4, v5, p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;JLcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 89
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsGolf:Z

    .line 95
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsDeleted:Z

    .line 105
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 106
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 107
    .local v2, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 109
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    invoke-static {v1, v2, v3, p3}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getItemCursor(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    .line 110
    if-nez v0, :cond_0

    .line 111
    const-string v3, "LocalVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot get cursor for: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 122
    :goto_0
    return-void

    .line 114
    :cond_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 115
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/LocalVideo;->loadFromCursor(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :goto_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 117
    :cond_1
    :try_start_2
    const-string v3, "LocalVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot find data for: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 120
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalVideo;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;JLcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 89
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsGolf:Z

    .line 95
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsDeleted:Z

    .line 100
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/data/LocalVideo;->loadFromCursor(Landroid/database/Cursor;)V

    .line 101
    return-void
.end method

.method private loadFromCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x1

    .line 125
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    .line 126
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->caption:Ljava/lang/String;

    .line 127
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mimeType:Ljava/lang/String;

    .line 128
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->latitude:D

    .line 129
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->longitude:D

    .line 130
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateTakenInMs:J

    .line 131
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateAddedInSec:J

    .line 132
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateModifiedInSec:J

    .line 133
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    .line 135
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->duration:I

    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->duration:I

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->durationInSec:I

    .line 138
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->bucketId:I

    .line 139
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->fileSize:J

    .line 142
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->resolution:Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDRMInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->drmInfo:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    .line 145
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->isdrm:Z

    .line 147
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->updateAttribute()V

    .line 148
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mRefreshable:Z

    .line 149
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->isSceretBoxItem:Z

    .line 153
    :cond_1
    return-void
.end method

.method private parseResolution(Ljava/lang/String;)V
    .locals 5
    .param p1, "resolution"    # Ljava/lang/String;

    .prologue
    .line 156
    if-nez p1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    const/16 v4, 0x78

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 158
    .local v1, "m":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 160
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 161
    .local v3, "w":I
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 162
    .local v0, "h":I
    iput v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->width:I

    .line 163
    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->height:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 164
    .end local v0    # "h":I
    .end local v3    # "w":I
    :catch_0
    move-exception v2

    .line 165
    .local v2, "t":Ljava/lang/Throwable;
    const-string v4, "LocalVideo"

    invoke-static {v4, v2}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private updateAttribute()V
    .locals 4

    .prologue
    .line 558
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->isGolf()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsGolf:Z

    .line 559
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMpo3DDisplay:Z

    if-eqz v2, :cond_2

    .line 560
    const/4 v0, 0x0

    .line 561
    .local v0, "supported":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 562
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->get3DVideoType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 563
    .local v1, "videoType":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v2, "SBS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "TB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 564
    :cond_0
    const/4 v0, 0x1

    .line 566
    .end local v1    # "videoType":Ljava/lang/String;
    :cond_1
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3, v0}, Lcom/sec/android/gallery3d/data/LocalVideo;->setAttribute(JZ)V

    .line 568
    .end local v0    # "supported":Z
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsSdCardFile:Z

    .line 569
    return-void
.end method


# virtual methods
.method protected addHiddenInfo(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 551
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    .line 552
    .local v0, "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    const-string/jumbo v1, "video_hidden_album"

    invoke-virtual {v0, v1, p1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addHiddenItem(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected copyTo(Ljava/lang/String;)V
    .locals 17
    .param p1, "dstFolderPath"    # Ljava/lang/String;

    .prologue
    .line 475
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 476
    .local v2, "resolver":Landroid/content/ContentResolver;
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 477
    .local v8, "bucketName":Ljava/lang/String;
    sget-object v14, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 478
    .local v14, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->getContentValues()Landroid/content/ContentValues;

    move-result-object v15

    .line 480
    .local v15, "values":Landroid/content/ContentValues;
    const-string v4, "_id"

    invoke-virtual {v15, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 481
    const-string v4, "bucket_id"

    invoke-static/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBucketId(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 482
    const-string v4, "bucket_display_name"

    invoke-virtual {v15, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v4, :cond_1

    .line 485
    const/4 v9, 0x0

    .line 486
    .local v9, "c":Landroid/database/Cursor;
    sget-object v4, Lcom/sec/android/gallery3d/data/LocalVideo;->CONTEXTUAL_URI:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->getItemId()I

    move-result v5

    int-to-long v6, v5

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 488
    .local v3, "contextualUri":Landroid/net/Uri;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_0
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 489
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 490
    const-string/jumbo v4, "weather_ID"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 491
    .local v16, "weather_tag":I
    const-string v4, "city_ID"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v10, v4

    .line 492
    .local v10, "city_tag":J
    const-string/jumbo v4, "weather_ID"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 493
    const-string v4, "city_ID"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    .end local v10    # "city_tag":J
    .end local v16    # "weather_tag":I
    :cond_0
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 502
    .end local v3    # "contextualUri":Landroid/net/Uri;
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    const-string v5, "\'"

    const-string v6, "\'\'"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 503
    .local v13, "path":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v14, v15, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-gtz v4, :cond_2

    .line 504
    invoke-virtual {v2, v14, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 506
    :cond_2
    return-void

    .line 495
    .end local v13    # "path":Ljava/lang/String;
    .restart local v3    # "contextualUri":Landroid/net/Uri;
    .restart local v9    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 496
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4
.end method

.method public delete()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->isHiddenItem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 387
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const-string/jumbo v3, "video_hidden_album"

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/gallery3d/data/LocalVideo;->deleteHiddenItem(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V

    .line 388
    monitor-exit v2

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 388
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 392
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 394
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsDeleted:Z

    if-nez v1, :cond_0

    .line 397
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsDeleted:Z

    .line 399
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 400
    .local v0, "baseUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "_id=?"

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->isHiddenItem()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 416
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 419
    :goto_0
    return-object v1

    .line 418
    :cond_0
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 419
    .local v0, "baseUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method protected getContentValues()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 510
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 512
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "_id"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 513
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->caption:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-string v1, "mime_type"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    const-string v1, "latitude"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 516
    const-string v1, "longitude"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 517
    const-string v1, "datetaken"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateTakenInMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 518
    const-string v1, "date_added"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateAddedInSec:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 519
    const-string v1, "date_modified"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateModifiedInSec:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 520
    const-string v1, "_data"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const-string v1, "duration"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->duration:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 522
    const-string v1, "bucket_id"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->bucketId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 523
    const-string v1, "_size"

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->fileSize:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 524
    const-string v1, "resolution"

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->resolution:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 435
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 437
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateTakenInMs:J

    invoke-static {v4, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 440
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->durationInSec:I

    .line 441
    .local v1, "s":I
    if-lez v1, :cond_0

    .line 442
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->durationInSec:I

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatDuration(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 446
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->isdrm:Z

    if-eqz v3, :cond_1

    .line 447
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDetails(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 451
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->resolution:Ljava/lang/String;

    const-string/jumbo v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 452
    .local v2, "values":[Ljava/lang/String;
    const/16 v3, 0xf

    const-string v4, "%dx%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aget-object v6, v2, v8

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    aget-object v6, v2, v9

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 454
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 464
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->height:I

    return v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 531
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 536
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 541
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->longitude:D

    return-wide v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x4

    return v0
.end method

.method public getPlayUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 344
    const-wide v0, 0x411013800485L

    .line 347
    .local v0, "operations":J
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->isHiddenItem()Z

    move-result v2

    if-nez v2, :cond_0

    .line 348
    const-wide v2, 0x4030000000200000L    # 16.00000000745058

    or-long/2addr v0, v2

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->isHideBlockedItem()Z

    move-result v2

    if-nez v2, :cond_0

    .line 351
    const-wide/high16 v2, 0x4000000000000L

    or-long/2addr v0, v2

    .line 355
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsGolf:Z

    if-eqz v2, :cond_1

    .line 356
    const-wide v2, -0x1000000001L

    and-long/2addr v0, v2

    .line 358
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v2, :cond_2

    .line 359
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->isSceretBoxItem:Z

    if-eqz v2, :cond_5

    .line 360
    const-wide/32 v2, 0x20000000

    or-long/2addr v0, v2

    .line 361
    const-wide/32 v2, -0x40000001

    and-long/2addr v0, v2

    .line 368
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    const-string v3, ".pyv"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 369
    const-wide v2, -0x10000000005L

    and-long/2addr v0, v2

    .line 373
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->isdrm:Z

    if-eqz v2, :cond_4

    .line 374
    const-wide/16 v0, 0x481

    .line 375
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmType(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 376
    const-wide v2, 0x10000000004L

    or-long/2addr v0, v2

    .line 379
    :cond_4
    return-wide v0

    .line 363
    :cond_5
    const-wide/32 v2, 0x40000000

    or-long/2addr v0, v2

    .line 364
    const-wide/32 v2, -0x20000001

    and-long/2addr v0, v2

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 459
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->width:I

    return v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 405
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsDeleted:Z

    return v0
.end method

.method public isGolf()Z
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mimeType:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mimeType:Ljava/lang/String;

    const-string v1, "image/golf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 10
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->getDateInMs()J

    move-result-wide v4

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mIsGolf:Z

    move v6, p1

    move-object v9, p0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/gallery3d/data/LocalVideo$LocalVideoRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;ZLcom/sec/android/gallery3d/data/MediaItem;)V

    return-object v1
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot regquest a large image to a local video!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public rotate(I)V
    .locals 0
    .param p1, "degrees"    # I

    .prologue
    .line 411
    return-void
.end method

.method protected updateFromCursor(Landroid/database/Cursor;)Z
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 171
    new-instance v0, Lcom/sec/android/gallery3d/util/UpdateHelper;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;-><init>()V

    .line 172
    .local v0, "uh":Lcom/sec/android/gallery3d/util/UpdateHelper;
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    .line 173
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->caption:Ljava/lang/String;

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->caption:Ljava/lang/String;

    .line 174
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mimeType:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->mimeType:Ljava/lang/String;

    .line 175
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->latitude:D

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(DD)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->latitude:D

    .line 176
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->longitude:D

    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(DD)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->longitude:D

    .line 177
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateTakenInMs:J

    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateTakenInMs:J

    .line 179
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateAddedInSec:J

    const/4 v1, 0x6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateAddedInSec:J

    .line 181
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateModifiedInSec:J

    const/4 v1, 0x7

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->dateModifiedInSec:J

    .line 183
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    .line 186
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->duration:I

    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->duration:I

    .line 187
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->duration:I

    div-int/lit16 v1, v1, 0x3e8

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->durationInSec:I

    .line 189
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->bucketId:I

    const/16 v3, 0xa

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->bucketId:I

    .line 190
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->fileSize:J

    const/16 v1, 0xb

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->fileSize:J

    .line 193
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->resolution:Ljava/lang/String;

    const/16 v3, 0xc

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalVideo;->resolution:Ljava/lang/String;

    .line 194
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalVideo;->updateAttribute()V

    .line 196
    :cond_0
    invoke-virtual {p0, v0, p1, v8}, Lcom/sec/android/gallery3d/data/LocalVideo;->isItemRenamed(Lcom/sec/android/gallery3d/util/UpdateHelper;Landroid/database/Cursor;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v1

    goto :goto_0
.end method
