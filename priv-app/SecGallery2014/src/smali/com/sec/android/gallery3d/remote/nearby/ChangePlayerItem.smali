.class public Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
.super Ljava/lang/Object;
.source "ChangePlayerItem.java"


# static fields
.field public static final TYPE_MY_DEVICE:I = 0x1

.field public static final TYPE_NEARBY_DEVICE:I = 0x2

.field public static final TYPE_WFD_DEVICE:I = 0x3

.field private static sMyDeviceItem:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;


# instance fields
.field private mDevice:Ljava/lang/Object;

.field private mDeviceID:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mDeviceType:I

.field private mIconUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "deviceType"    # I
    .param p4, "deviceID"    # Ljava/lang/String;
    .param p5, "device"    # Ljava/lang/Object;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mIconUri:Landroid/net/Uri;

    .line 55
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDeviceName:Ljava/lang/String;

    .line 56
    iput p3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDeviceType:I

    .line 57
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDeviceID:Ljava/lang/String;

    .line 58
    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDevice:Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public static final getMyDevice(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const v3, 0x7f0e012f

    .line 33
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->sMyDeviceItem:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 35
    .local v2, "myDevice":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    const/4 v3, 0x1

    move-object v4, v2

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;-><init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Object;)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->sMyDeviceItem:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    .line 40
    .end local v2    # "myDevice":Ljava/lang/String;
    :goto_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->sMyDeviceItem:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    return-object v0

    .line 37
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->sMyDeviceItem:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDeviceName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getDevice()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDevice:Ljava/lang/Object;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mDeviceType:I

    return v0
.end method

.method public getIconUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->mIconUri:Landroid/net/Uri;

    return-object v0
.end method
