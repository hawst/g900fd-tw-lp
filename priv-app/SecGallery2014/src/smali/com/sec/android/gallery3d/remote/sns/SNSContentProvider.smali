.class public final Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
.super Landroid/content/ContentProvider;
.source "SNSContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$1;,
        Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;,
        Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;,
        Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    }
.end annotation


# static fields
.field public static final ALBUMS_URI:Landroid/net/Uri;

.field private static final ALBUM_TABLE_NAME:Ljava/lang/String; = "albums"

.field private static final ALBUM_TYPE_WHERE:Ljava/lang/String; = "_id in (SELECT album_id FROM photos WHERE content_type LIKE ?)"

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.gallery3d.sns.contentprovider"

.field public static final BASE_URI:Landroid/net/Uri;

.field private static final CACHE_FLAG_PROJECTION:[Ljava/lang/String;

.field private static final ERROR_TYPE_FORCE:I = -0x1

.field private static final ERROR_TYPE_FROMNETWORK:I = 0x2

.field private static final ERROR_TYPE_NONE:I = 0x0

.field private static final ERROR_TYPE_NOSTORAGE:I = 0x1

.field private static final ID_CONTENT_URL_PROJECTION:[Ljava/lang/String;

.field private static final ID_EDITED_INDEX_PROJECTION:[Ljava/lang/String;

.field private static final ID_EDITED_PROJECTION:[Ljava/lang/String;

.field private static final ID_SCREENNAIL_URL_PROJECTION:[Ljava/lang/String;

.field private static final MINIMUN_STORAGE_TO_CACHE:J = 0x40000000L

.field public static final PHOTOS_URI:Landroid/net/Uri;

.field private static final PHOTO_TABLE_NAME:Ljava/lang/String; = "photos"

.field private static final SOURCE_ID_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SNSContentProvider"

.field private static final TIMEOUT_CONNECTION:I = 0x3a98

.field private static final TIMEOUT_READ:I = 0x3a98

.field private static final WHERE_ALBUM_ID:Ljava/lang/String; = "album_id=?"

.field private static final WHERE_ID_FOR_ALBUM:Ljava/lang/String; = "source_id=?"

.field private static final WHERE_ID_FOR_PHOTO:Ljava/lang/String; = "source_id=?"


# instance fields
.field private mCacheDir:Ljava/lang/String;

.field private mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

.field private final mMappings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mStopSync:Z

.field private mSyncContext:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    const-string v0, "content://com.sec.android.gallery3d.sns.contentprovider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->BASE_URI:Landroid/net/Uri;

    .line 52
    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->BASE_URI:Landroid/net/Uri;

    const-string v1, "photos"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    .line 53
    sget-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->BASE_URI:Landroid/net/Uri;

    const-string v1, "albums"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    .line 62
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "source_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->SOURCE_ID_PROJECTION:[Ljava/lang/String;

    .line 70
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "source_id"

    aput-object v1, v0, v3

    const-string v1, "date_edited"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_EDITED_PROJECTION:[Ljava/lang/String;

    .line 73
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "source_id"

    aput-object v1, v0, v3

    const-string v1, "date_edited"

    aput-object v1, v0, v4

    const-string v1, "display_index"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "cache_pathname"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cache_status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_EDITED_INDEX_PROJECTION:[Ljava/lang/String;

    .line 78
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "cache_flag"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->CACHE_FLAG_PROJECTION:[Ljava/lang/String;

    .line 81
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "source_id"

    aput-object v1, v0, v3

    const-string v1, "content_url"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_CONTENT_URL_PROJECTION:[Ljava/lang/String;

    .line 84
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "source_id"

    aput-object v1, v0, v3

    const-string v1, "screennail_url"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_SCREENNAIL_URL_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z

    .line 898
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 899
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mMappings:Ljava/util/ArrayList;

    .line 913
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;)Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    return-object v0
.end method

.method private deleteAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # Ljava/lang/String;

    .prologue
    .line 639
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->deletePhotosForAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 642
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    const-string v1, "albums"

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->deleteWithSourceId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    .line 643
    return-void
.end method

.method private deleteCache(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "photoId"    # Ljava/lang/String;

    .prologue
    .line 674
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getCacheFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 676
    .local v0, "pathname":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 680
    :goto_0
    return-void

    .line 677
    :catch_0
    move-exception v1

    .line 678
    .local v1, "t":Ljava/lang/Throwable;
    const-string v2, "SNSContentProvider"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private deleteCacheForAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 659
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v1

    .line 660
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 662
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "photos"

    sget-object v2, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->SOURCE_ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "album_id=?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 665
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->deleteCache(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 669
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_0
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 671
    return-void
.end method

.method private deletePhoto(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # Ljava/lang/String;
    .param p3, "photoId"    # Ljava/lang/String;

    .prologue
    .line 654
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    const-string v1, "photos"

    invoke-virtual {v0, p1, p3, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->deleteWithSourceId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    .line 655
    invoke-direct {p0, p2, p3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->deleteCache(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    return-void
.end method

.method private deletePhotosForAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # Ljava/lang/String;

    .prologue
    .line 646
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->deleteCacheForAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 649
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 650
    .local v0, "whereArgs":[Ljava/lang/String;
    const-string v1, "photos"

    const-string v2, "album_id=?"

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 651
    return-void
.end method

.method private downloadPhoto(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "photoId"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "pathname"    # Ljava/lang/String;

    .prologue
    .line 586
    const/4 v6, 0x0

    .line 587
    .local v6, "os":Ljava/io/OutputStream;
    const/4 v5, 0x0

    .line 588
    .local v5, "is":Ljava/io/InputStream;
    const-string v9, "SNSContentProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "downloadPhoto id["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 591
    .end local v6    # "os":Ljava/io/OutputStream;
    .local v7, "os":Ljava/io/OutputStream;
    :try_start_1
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 592
    .local v1, "connection":Ljava/net/URLConnection;
    const/16 v9, 0x3a98

    invoke-virtual {v1, v9}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 593
    const/16 v9, 0x3a98

    invoke-virtual {v1, v9}, Ljava/net/URLConnection;->setReadTimeout(I)V

    .line 594
    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 596
    const/16 v9, 0x1000

    new-array v0, v9, [B

    .line 597
    .local v0, "buffer":[B
    const/4 v9, 0x0

    array-length v10, v0

    invoke-virtual {v5, v0, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    .line 598
    .local v8, "rc":I
    const/4 v2, 0x1

    .line 599
    .local v2, "emptyData":Z
    :cond_0
    :goto_0
    if-lez v8, :cond_2

    .line 600
    iget-boolean v9, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v9, :cond_1

    .line 601
    const/4 v9, 0x0

    .line 619
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v6, v7

    .line 623
    .end local v0    # "buffer":[B
    .end local v1    # "connection":Ljava/net/URLConnection;
    .end local v2    # "emptyData":Z
    .end local v7    # "os":Ljava/io/OutputStream;
    .end local v8    # "rc":I
    .restart local v6    # "os":Ljava/io/OutputStream;
    :goto_1
    return v9

    .line 602
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "connection":Ljava/net/URLConnection;
    .restart local v2    # "emptyData":Z
    .restart local v7    # "os":Ljava/io/OutputStream;
    .restart local v8    # "rc":I
    :cond_1
    const/4 v9, 0x0

    :try_start_2
    invoke-virtual {v7, v0, v9, v8}, Ljava/io/OutputStream;->write([BII)V

    .line 603
    const/4 v9, 0x0

    array-length v10, v0

    invoke-virtual {v5, v0, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    .line 604
    if-eqz v2, :cond_0

    .line 605
    const/4 v2, 0x0

    goto :goto_0

    .line 608
    :cond_2
    if-eqz v2, :cond_3

    .line 609
    const-string v9, "SNSContentProvider"

    const-string v10, "input data empty - "

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 610
    const/4 v9, 0x0

    .line 619
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v6, v7

    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    goto :goto_1

    .line 619
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v7    # "os":Ljava/io/OutputStream;
    :cond_3
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 622
    const-string v9, "SNSContentProvider"

    const-string v10, "downloadPhoto end"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    const/4 v9, 0x1

    move-object v6, v7

    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    goto :goto_1

    .line 612
    .end local v0    # "buffer":[B
    .end local v1    # "connection":Ljava/net/URLConnection;
    .end local v2    # "emptyData":Z
    .end local v8    # "rc":I
    :catch_0
    move-exception v4

    .line 613
    .local v4, "exception":Ljava/lang/NullPointerException;
    :goto_2
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 614
    const/4 v9, 0x0

    .line 619
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 615
    .end local v4    # "exception":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v3

    .line 616
    .local v3, "ex":Ljava/io/IOException;
    :goto_3
    :try_start_4
    const-string v9, "SNSContentProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "error during download : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 617
    const/4 v9, 0x0

    .line 619
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 619
    .end local v3    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_4
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v9

    .line 619
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v7    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v9

    move-object v6, v7

    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    goto :goto_4

    .line 615
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v7    # "os":Ljava/io/OutputStream;
    :catch_2
    move-exception v3

    move-object v6, v7

    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    goto :goto_3

    .line 612
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v7    # "os":Ljava/io/OutputStream;
    :catch_3
    move-exception v4

    move-object v6, v7

    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    goto :goto_2
.end method

.method private static flagToTargetStatus(I)I
    .locals 1
    .param p0, "flag"    # I

    .prologue
    .line 429
    packed-switch p0, :pswitch_data_0

    .line 435
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 431
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 433
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getAvailableStorage()J
    .locals 6

    .prologue
    .line 629
    :try_start_0
    new-instance v0, Landroid/os/StatFs;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getCacheDirectory()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 630
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    .line 633
    .end local v0    # "stat":Landroid/os/StatFs;
    :goto_0
    return-wide v2

    .line 631
    :catch_0
    move-exception v1

    .line 632
    .local v1, "t":Ljava/lang/Throwable;
    const-string v2, "SNSContentProvider"

    const-string v3, "Fail to getAvailableStorage"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 633
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method private getCacheDirName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getCacheDirectory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sns-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCacheDirectory()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mCacheDir:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 134
    .local v0, "cacheDir":Ljava/io/File;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mCacheDir:Ljava/lang/String;

    .line 136
    .end local v0    # "cacheDir":Ljava/io/File;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mCacheDir:Ljava/lang/String;

    return-object v1

    .line 134
    .restart local v0    # "cacheDir":Ljava/io/File;
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getCacheFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "photoId"    # Ljava/lang/String;

    .prologue
    .line 582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getCacheDirectory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/sns-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private notifyAlbumChange()V
    .locals 4

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 546
    return-void
.end method

.method private final notifyChange(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 861
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 862
    return-void
.end method

.method private notifyPhotoChange()V
    .locals 4

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 550
    return-void
.end method

.method private syncAlbumAndPhotos(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/util/ArrayList;Landroid/net/Uri;)V
    .locals 23
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    .param p3, "photoUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/AlbumTable;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 204
    .local p2, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/AlbumTable;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 205
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_0

    .line 284
    :goto_0
    return-void

    .line 207
    :cond_0
    const/4 v3, 0x0

    new-array v0, v3, [Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    move-object/from16 v19, v0

    .line 208
    .local v19, "local":[Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    const/16 v20, 0x0

    .line 209
    .local v20, "localCount":I
    const/4 v13, 0x0

    .line 212
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "albums"

    sget-object v4, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_EDITED_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "date_edited"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 214
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v20

    .line 217
    move/from16 v0, v20

    new-array v0, v0, [Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    move-object/from16 v19, v0

    .line 218
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move/from16 v0, v20

    if-eq v15, v0, :cond_1

    .line 219
    invoke-interface {v13, v15}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 220
    new-instance v4, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;-><init>(Ljava/lang/String;JILjava/lang/String;I)V

    aput-object v4, v19, v15
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 225
    :cond_1
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 228
    .end local v15    # "i":I
    :goto_2
    invoke-static/range {v19 .. v19}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 232
    new-instance v18, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;-><init>()V

    .line 233
    .local v18, "key":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 235
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    .line 236
    .local v11, "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_3

    .line 260
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .line 222
    .end local v11    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v18    # "key":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    :catch_0
    move-exception v14

    .line 223
    .local v14, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 225
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 238
    .restart local v11    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v18    # "key":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    :cond_3
    :try_start_3
    iget-object v12, v11, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    .line 239
    .local v12, "albumId":Ljava/lang/String;
    move-object/from16 v0, v18

    iput-object v12, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->id:Ljava/lang/String;

    .line 240
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v17

    .line 241
    .local v17, "index":I
    if-ltz v17, :cond_6

    aget-object v22, v19, v17

    .line 242
    .local v22, "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    :goto_4
    if-eqz v22, :cond_4

    move-object/from16 v0, v22

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->dateEdited:J

    iget-wide v6, v11, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->date_edited:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_5

    .line 244
    :cond_4
    const/4 v3, 0x1

    iput-boolean v3, v11, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->photos_dirty:Z

    .line 245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    invoke-virtual {v3, v2, v11}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)J

    .line 246
    if-nez v22, :cond_5

    .line 247
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->albumsChanged:Z

    .line 254
    :cond_5
    if-eqz v22, :cond_2

    .line 255
    const/4 v3, 0x1

    move-object/from16 v0, v22

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->survived:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    .line 260
    .end local v11    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .end local v12    # "albumId":Ljava/lang/String;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "index":I
    .end local v22    # "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 241
    .restart local v11    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .restart local v12    # "albumId":Ljava/lang/String;
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v17    # "index":I
    :cond_6
    const/16 v22, 0x0

    goto :goto_4

    .line 258
    .end local v11    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .end local v12    # "albumId":Ljava/lang/String;
    .end local v17    # "index":I
    :cond_7
    :try_start_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 260
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 263
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    .line 264
    .restart local v11    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-static {v3, v0, v11}, Lcom/sec/android/gallery3d/remote/sns/SNSSyncAdapter;->syncPhotos(Landroid/content/Context;Landroid/net/Uri;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V

    .line 265
    iget-object v3, v11, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->syncCacheForAlbum(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/lang/String;)V

    goto :goto_5

    .line 269
    .end local v11    # "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    :cond_8
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 271
    const/4 v15, 0x0

    .restart local v15    # "i":I
    :goto_6
    move/from16 v0, v20

    if-eq v15, v0, :cond_a

    .line 272
    :try_start_5
    aget-object v21, v19, v15

    .line 273
    .local v21, "meta":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    move-object/from16 v0, v21

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->survived:Z

    if-nez v3, :cond_9

    .line 274
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->deleteAlbum(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 275
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->albumsChanged:Z

    .line 276
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->photosChanged:Z

    .line 271
    :cond_9
    add-int/lit8 v15, v15, 0x1

    goto :goto_6

    .line 279
    .end local v21    # "meta":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    :cond_a
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 281
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 283
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->finish()V

    goto/16 :goto_0

    .line 281
    :catchall_2
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private syncCacheForAlbum(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/lang/String;)V
    .locals 22
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    .param p2, "albumId"    # Ljava/lang/String;

    .prologue
    .line 441
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 442
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_1

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    const/16 v17, 0x0

    .line 446
    .local v17, "cursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 448
    .local v16, "cachingFlag":I
    :try_start_0
    const-string v3, "albums"

    sget-object v4, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->CACHE_FLAG_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v5, "source_id=?"

    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 453
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 454
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v16

    .line 457
    :cond_2
    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 460
    if-eqz v16, :cond_0

    .line 462
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->flagToTargetStatus(I)I

    move-result v15

    .line 466
    .local v15, "targetStatus":I
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v6, v3

    const/4 v3, 0x1

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v3

    .line 471
    .local v6, "whereArgs":[Ljava/lang/String;
    const/4 v3, 0x3

    if-ne v15, v3, :cond_3

    sget-object v4, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_CONTENT_URL_PROJECTION:[Ljava/lang/String;

    .line 477
    .local v4, "idUrlProjection":[Ljava/lang/String;
    :goto_1
    const-string v9, "display_index"

    .line 478
    .local v9, "order":Ljava/lang/String;
    const-string v3, "photos"

    const-string v5, "album_id=? AND cache_status < ?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 483
    if-eqz v17, :cond_0

    .line 485
    :try_start_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-nez v3, :cond_4

    .line 522
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 457
    .end local v4    # "idUrlProjection":[Ljava/lang/String;
    .end local v6    # "whereArgs":[Ljava/lang/String;
    .end local v9    # "order":Ljava/lang/String;
    .end local v15    # "targetStatus":I
    :catchall_0
    move-exception v3

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 471
    .restart local v6    # "whereArgs":[Ljava/lang/String;
    .restart local v15    # "targetStatus":I
    :cond_3
    sget-object v4, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_SCREENNAIL_URL_PROJECTION:[Ljava/lang/String;

    goto :goto_1

    .line 489
    .restart local v4    # "idUrlProjection":[Ljava/lang/String;
    .restart local v9    # "order":Ljava/lang/String;
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getCacheDirName(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v18

    .line 491
    .local v18, "dirName":Ljava/lang/String;
    :try_start_3
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 497
    const/4 v3, 0x1

    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 499
    const/16 v20, 0x0

    .line 501
    .local v20, "stopped":Z
    :cond_5
    :goto_2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 502
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v3, :cond_6

    .line 522
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 492
    .end local v20    # "stopped":Z
    :catch_0
    move-exception v21

    .line 493
    .local v21, "t":Ljava/lang/Throwable;
    :try_start_5
    const-string v3, "SNSContentProvider"

    const-string v5, "cannot create cache dir: "

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 522
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 504
    .end local v21    # "t":Ljava/lang/Throwable;
    .restart local v20    # "stopped":Z
    :cond_6
    const/4 v3, 0x0

    :try_start_6
    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 505
    .local v13, "photoId":Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 506
    .local v14, "url":Ljava/lang/String;
    if-eqz v14, :cond_5

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    .line 508
    invoke-direct/range {v10 .. v15}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->syncOnePhoto(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v19

    .line 509
    .local v19, "result":I
    if-eqz v19, :cond_9

    .line 510
    const/16 v20, 0x1

    .line 518
    .end local v13    # "photoId":Ljava/lang/String;
    .end local v14    # "url":Ljava/lang/String;
    .end local v19    # "result":I
    :cond_7
    if-nez v20, :cond_8

    .line 519
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1, v15}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 522
    :cond_8
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 513
    .restart local v13    # "photoId":Ljava/lang/String;
    .restart local v14    # "url":Ljava/lang/String;
    .restart local v19    # "result":I
    :cond_9
    const/4 v3, 0x1

    :try_start_7
    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->photosChanged:Z

    .line 514
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->albumsChanged:Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    .line 522
    .end local v13    # "photoId":Ljava/lang/String;
    .end local v14    # "url":Ljava/lang/String;
    .end local v18    # "dirName":Ljava/lang/String;
    .end local v19    # "result":I
    .end local v20    # "stopped":Z
    :catchall_1
    move-exception v3

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    throw v3
.end method

.method private syncOnePhoto(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 8
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    .param p2, "albumId"    # Ljava/lang/String;
    .param p3, "photoId"    # Ljava/lang/String;
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "targetStatus"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 554
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->allowDownload(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 555
    :cond_0
    const/4 v2, -0x1

    .line 574
    :cond_1
    :goto_0
    return v2

    .line 557
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getAvailableStorage()J

    move-result-wide v4

    const-wide/32 v6, 0x40000000

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    .line 561
    invoke-direct {p0, p2, p3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getCacheFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 562
    .local v0, "pathname":Ljava/lang/String;
    invoke-direct {p0, p3, p4, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->downloadPhoto(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 563
    const/4 v2, 0x2

    goto :goto_0

    .line 567
    :cond_3
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 568
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "cache_status"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 569
    const-string v4, "cache_pathname"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "photos"

    const-string/jumbo v6, "source_id=?"

    new-array v2, v2, [Ljava/lang/String;

    aput-object p3, v2, v3

    invoke-virtual {v4, v5, v1, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 572
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->notifyPhotoChange()V

    .line 573
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->notifyAlbumChange()V

    move v2, v3

    .line 574
    goto :goto_0
.end method

.method private syncPhotos(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/util/ArrayList;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V
    .locals 32
    .param p1, "context"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    .param p3, "album"    # Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/PhotoTable;",
            ">;",
            "Lcom/sec/android/gallery3d/remote/sns/AlbumTable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 288
    .local p2, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 289
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_0

    .line 426
    :goto_0
    return-void

    .line 291
    :cond_0
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    .line 292
    .local v15, "albumId":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v15, v6, v3

    .line 296
    .local v6, "albumIdArgs":[Ljava/lang/String;
    new-instance v24, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;-><init>()V

    .line 297
    .local v24, "key":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    const/4 v3, 0x0

    new-array v0, v3, [Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    move-object/from16 v25, v0

    .line 298
    .local v25, "local":[Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    const/16 v26, 0x0

    .line 300
    .local v26, "localCount":I
    const/16 v18, 0x0

    .line 302
    .local v18, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "photos"

    sget-object v4, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ID_EDITED_INDEX_PROJECTION:[Ljava/lang/String;

    const-string v5, "album_id=?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "date_edited"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 304
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v26

    .line 308
    move/from16 v0, v26

    new-array v0, v0, [Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    move-object/from16 v25, v0

    .line 310
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    move/from16 v0, v21

    move/from16 v1, v26

    if-eq v0, v1, :cond_1

    .line 311
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 312
    new-instance v8, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v3, 0x2

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/4 v3, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v3, 0x4

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-direct/range {v8 .. v14}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;-><init>(Ljava/lang/String;JILjava/lang/String;I)V

    aput-object v8, v25, v21
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 310
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 319
    :cond_1
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 321
    .end local v21    # "i":I
    :goto_2
    invoke-static/range {v25 .. v25}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 325
    const/4 v3, 0x1

    new-array v0, v3, [I

    move-object/from16 v19, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v19, v3

    .line 327
    .local v19, "displayIndex":[I
    const-string v3, "SNSContentProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "entries size ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 330
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_3
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .line 331
    .local v29, "photo":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->source_id:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 332
    .local v30, "photoId":Ljava/lang/String;
    const/4 v3, 0x0

    aget v28, v19, v3

    .line 333
    .local v28, "newDisplayIndex":I
    move-object/from16 v0, v30

    move-object/from16 v1, v24

    iput-object v0, v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->id:Ljava/lang/String;

    .line 334
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v23

    .line 335
    .local v23, "index":I
    if-ltz v23, :cond_9

    aget-object v27, v25, v23

    .line 336
    .local v27, "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    :goto_4
    if-eqz v27, :cond_3

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->cachePath:Ljava/lang/String;

    if-eqz v3, :cond_3

    move-object/from16 v0, v27

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->dateEdited:J

    move-object/from16 v0, v29

    iget-wide v8, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->date_edited:J

    cmp-long v3, v4, v8

    if-ltz v3, :cond_3

    move-object/from16 v0, v27

    iget v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->displayIndex:I

    move/from16 v0, v28

    if-eq v3, v0, :cond_7

    .line 340
    :cond_3
    move/from16 v0, v28

    move-object/from16 v1, v29

    iput v0, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->display_index:I

    .line 341
    if-eqz v27, :cond_4

    .line 342
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->cachePath:Ljava/lang/String;

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    .line 343
    move-object/from16 v0, v27

    iget v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->cacheStatus:I

    move-object/from16 v0, v29

    iput v3, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_status:I

    .line 345
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    move-object/from16 v0, v29

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)J

    .line 347
    move-object/from16 v0, v29

    iget v3, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->display_index:I

    if-nez v3, :cond_6

    .line 349
    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V

    .line 350
    .local v31, "thumbnail":Landroid/content/ContentValues;
    const-string/jumbo v3, "thumbnail_url"

    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->thumbnail_url:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "albums"

    const-string/jumbo v5, "source_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v15, v7, v8

    move-object/from16 v0, v31

    invoke-virtual {v3, v4, v0, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 355
    move-object/from16 v0, v29

    iget v3, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->display_index:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v3, :cond_6

    .line 356
    const/16 v17, 0x0

    .line 357
    .local v17, "cachingFlag":I
    const/16 v16, 0x0

    .line 359
    .local v16, "c":Landroid/database/Cursor;
    :try_start_2
    const-string v8, "albums"

    sget-object v9, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->CACHE_FLAG_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v10, "source_id=?"

    const/4 v3, 0x1

    new-array v11, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v15, v11, v3

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v7, v2

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 365
    if-eqz v16, :cond_5

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 366
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v17

    .line 371
    :cond_5
    :try_start_3
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 374
    :goto_5
    if-eqz v17, :cond_2

    .line 377
    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->flagToTargetStatus(I)I

    move-result v12

    .line 378
    .local v12, "targetStatus":I
    move-object/from16 v0, v29

    iget-object v11, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object v9, v15

    move-object/from16 v10, v30

    invoke-direct/range {v7 .. v12}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->syncOnePhoto(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_a

    .line 380
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->notifyAlbumChange()V

    .line 386
    .end local v12    # "targetStatus":I
    .end local v16    # "c":Landroid/database/Cursor;
    .end local v17    # "cachingFlag":I
    .end local v31    # "thumbnail":Landroid/content/ContentValues;
    :cond_6
    :goto_6
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->photosChanged:Z

    .line 392
    :cond_7
    if-eqz v27, :cond_8

    .line 393
    const/4 v3, 0x1

    move-object/from16 v0, v27

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->survived:Z

    .line 397
    :cond_8
    const/4 v3, 0x0

    add-int/lit8 v4, v28, 0x1

    aput v4, v19, v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 401
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v23    # "index":I
    .end local v27    # "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    .end local v28    # "newDisplayIndex":I
    .end local v29    # "photo":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .end local v30    # "photoId":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 316
    .end local v19    # "displayIndex":[I
    :catch_0
    move-exception v20

    .line 317
    .local v20, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 319
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_2

    .end local v20    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v3

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 335
    .restart local v19    # "displayIndex":[I
    .restart local v22    # "i$":Ljava/util/Iterator;
    .restart local v23    # "index":I
    .restart local v28    # "newDisplayIndex":I
    .restart local v29    # "photo":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .restart local v30    # "photoId":Ljava/lang/String;
    :cond_9
    const/16 v27, 0x0

    goto/16 :goto_4

    .line 368
    .restart local v16    # "c":Landroid/database/Cursor;
    .restart local v17    # "cachingFlag":I
    .restart local v27    # "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    .restart local v31    # "thumbnail":Landroid/content/ContentValues;
    :catch_1
    move-exception v20

    .line 369
    .restart local v20    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 371
    :try_start_6
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_5

    .end local v20    # "e":Ljava/lang/Exception;
    :catchall_2
    move-exception v3

    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 382
    .restart local v12    # "targetStatus":I
    :cond_a
    const-string v3, "SNSContentProvider"

    const-string v4, "fail to sync the cover image."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 399
    .end local v12    # "targetStatus":I
    .end local v16    # "c":Landroid/database/Cursor;
    .end local v17    # "cachingFlag":I
    .end local v23    # "index":I
    .end local v27    # "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    .end local v28    # "newDisplayIndex":I
    .end local v29    # "photo":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .end local v30    # "photoId":Ljava/lang/String;
    .end local v31    # "thumbnail":Landroid/content/ContentValues;
    :cond_b
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 401
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 405
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 407
    const/16 v21, 0x0

    .restart local v21    # "i":I
    :goto_7
    move/from16 v0, v21

    move/from16 v1, v26

    if-eq v0, v1, :cond_d

    .line 408
    :try_start_7
    aget-object v27, v25, v21

    .line 409
    .restart local v27    # "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->survived:Z

    if-nez v3, :cond_c

    .line 410
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v15, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->deletePhoto(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->albumsChanged:Z

    .line 412
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->photosChanged:Z

    .line 407
    :cond_c
    add-int/lit8 v21, v21, 0x1

    goto :goto_7

    .line 416
    .end local v27    # "metadata":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
    :cond_d
    const/4 v3, 0x0

    move-object/from16 v0, p3

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->photos_dirty:Z

    .line 417
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    move-object/from16 v0, p3

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)J

    .line 419
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 421
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    :catchall_3
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # Ljava/lang/String;
    .param p3, "status"    # I

    .prologue
    .line 537
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 538
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "cache_status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 539
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 540
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string v2, "albums"

    const-string/jumbo v3, "source_id=?"

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 541
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->notifyAlbumChange()V

    .line 542
    return-void
.end method

.method private updateAlbumCachingFlag(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "albumId"    # Ljava/lang/String;
    .param p3, "flag"    # I

    .prologue
    .line 527
    if-nez p1, :cond_0

    .line 534
    :goto_0
    return-void

    .line 529
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 530
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "cache_flag"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 531
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 532
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string v2, "albums"

    const-string/jumbo v3, "source_id=?"

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 533
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->notifyAlbumChange()V

    goto :goto_0
.end method

.method private final whereWithId(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    .line 865
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 866
    .local v0, "id":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "_id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 867
    .local v1, "where":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 868
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 869
    const-string v2, " AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 870
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 871
    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 873
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public addMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)V
    .locals 4
    .param p1, "authority"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "mimeSubtype"    # Ljava/lang/String;
    .param p4, "table"    # Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    .prologue
    .line 903
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mMappings:Ljava/util/ArrayList;

    .line 904
    .local v0, "mappings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 905
    .local v1, "matcher":Landroid/content/UriMatcher;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, p1, p2, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 906
    new-instance v2, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;

    const/4 v3, 0x0

    invoke-direct {v2, p4, p3, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;-><init>(Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 909
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 910
    new-instance v2, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;

    const/4 v3, 0x1

    invoke-direct {v2, p4, p3, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;-><init>(Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 911
    return-void
.end method

.method public allowDownload(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 112
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    :goto_0
    return v0

    .line 115
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->setStopCurrentSyncing(Z)V

    .line 116
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Landroid/content/pm/ProviderInfo;

    .prologue
    .line 142
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 143
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    invoke-direct {v1, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    .line 145
    const-string v1, "com.sec.android.gallery3d.sns.contentprovider"

    const-string v2, "photos"

    const-string/jumbo v3, "vnd.android.gallery3d.sns.photo"

    new-instance v4, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;-><init>()V

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->addMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)V

    .line 146
    const-string v1, "com.sec.android.gallery3d.sns.contentprovider"

    const-string v2, "albums"

    const-string/jumbo v3, "vnd.android.gallery3d.sns.album"

    new-instance v4, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;-><init>()V

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->addMapping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)V

    .line 150
    :try_start_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;-><init>(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$1;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    return-void

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SNSContentProvider"

    const-string v2, "cannot get sync context"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 160
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 161
    .local v3, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz p2, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v10

    .line 166
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;

    .line 169
    .local v1, "context":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 170
    .local v7, "type":Ljava/lang/String;
    const/4 v8, 0x1

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 171
    .local v4, "id":J
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 172
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "photos"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 174
    new-instance v6, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-direct {v6}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;-><init>()V

    .line 175
    .local v6, "photo":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    invoke-virtual {p0, v2, v4, v5, v6}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 185
    .end local v6    # "photo":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    :cond_2
    :goto_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->finish()V

    goto :goto_0

    .line 178
    :cond_3
    const-string v8, "albums"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 180
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;-><init>()V

    .line 181
    .local v0, "album":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    invoke-virtual {p0, v2, v4, v5, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_1
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 749
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->snsSupport(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 750
    .local v3, "support":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 765
    .end local v3    # "support":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 755
    .restart local v3    # "support":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 756
    .local v1, "match":I
    const/4 v4, -0x1

    if-ne v1, v4, :cond_1

    .line 757
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 761
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;

    .line 762
    .local v0, "mapping":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;
    iget-boolean v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->hasId:Z

    if-eqz v4, :cond_2

    const-string/jumbo v2, "vnd.android.cursor.item"

    .line 765
    .local v2, "prefix":Ljava/lang/String;
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->mimeSubtype:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 762
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_2
    const-string/jumbo v2, "vnd.android.cursor.dir"

    goto :goto_1
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 771
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 772
    .local v1, "match":I
    const/4 v5, -0x1

    if-eq v1, v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;

    move-object v0, v5

    .line 773
    .local v0, "mapping":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;
    :goto_0
    if-eqz v0, :cond_0

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->hasId:Z

    if-eqz v5, :cond_2

    .line 774
    :cond_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid URI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 772
    .end local v0    # "mapping":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 779
    .restart local v0    # "mapping":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;
    :cond_2
    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->getTableName()Ljava/lang/String;

    move-result-object v4

    .line 780
    .local v4, "tableName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "_id"

    invoke-virtual {v5, v4, v6, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 781
    .local v2, "rowId":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-lez v5, :cond_3

    .line 782
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->notifyChange(Landroid/net/Uri;)V

    .line 783
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    return-object v5

    .line 785
    :cond_3
    new-instance v5, Landroid/database/SQLException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert row at: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public isStopCurrentSyncing()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z

    return v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 792
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 798
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v15

    .line 799
    .local v15, "match":I
    const/4 v3, -0x1

    if-ne v15, v3, :cond_0

    .line 800
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 804
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;

    .line 805
    .local v14, "mapping":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;
    iget-boolean v3, v14, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->hasId:Z

    if-eqz v3, :cond_1

    .line 806
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->whereWithId(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 810
    :cond_1
    iget-object v3, v14, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->getTableName()Ljava/lang/String;

    move-result-object v4

    .line 811
    .local v4, "tableName":Ljava/lang/String;
    const-string v3, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 813
    .local v11, "limit":Ljava/lang/String;
    if-nez p3, :cond_2

    const-string v3, "albums"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 815
    const-string/jumbo v3, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 816
    .local v16, "type":Ljava/lang/String;
    const-string v3, "image"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 817
    const-string p3, "_id in (SELECT album_id FROM photos WHERE content_type LIKE ?)"

    .line 818
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string v5, "image/%"

    aput-object v5, p4, v3

    .line 825
    .end local v16    # "type":Ljava/lang/String;
    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    :cond_2
    :goto_0
    const/4 v12, 0x0

    .line 827
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v10, p5

    invoke-virtual/range {v3 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 830
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v12, v3, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 834
    :goto_1
    return-object v12

    .line 819
    .end local v12    # "cursor":Landroid/database/Cursor;
    .restart local v16    # "type":Ljava/lang/String;
    :cond_3
    const-string/jumbo v3, "video"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 820
    const-string p3, "_id in (SELECT album_id FROM photos WHERE content_type LIKE ?)"

    .line 821
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 p4, v0

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x0

    const-string/jumbo v5, "video/%"

    aput-object v5, p4, v3

    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 831
    .end local v16    # "type":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v13

    .line 832
    .local v13, "e":Ljava/lang/Exception;
    const-string v3, "SNSContentProvider"

    const-string v5, "Can not return query"

    invoke-static {v3, v5, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Z
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "id"    # J
    .param p4, "table"    # Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    .prologue
    .line 877
    invoke-virtual/range {p4 .. p4}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->getTableName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "albums"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->getProjection()[Ljava/lang/String;

    move-result-object v4

    .line 879
    .local v4, "columns":[Ljava/lang/String;
    :goto_0
    const/4 v11, 0x0

    .line 880
    .local v11, "success":Z
    const/4 v10, 0x0

    .line 881
    .local v10, "cursor":Landroid/database/Cursor;
    if-nez p1, :cond_1

    move v12, v11

    .line 895
    .end local v11    # "success":Z
    .local v12, "success":I
    :goto_1
    return v12

    .line 877
    .end local v4    # "columns":[Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v12    # "success":I
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->getProjection()[Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 884
    .restart local v4    # "columns":[Ljava/lang/String;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "success":Z
    :cond_1
    :try_start_0
    invoke-virtual/range {p4 .. p4}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->getTableName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 888
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 889
    move-object/from16 v0, p4

    move-object/from16 v1, p4

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 890
    const/4 v11, 0x1

    .line 893
    :cond_2
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v12, v11

    .line 895
    .restart local v12    # "success":I
    goto :goto_1

    .line 893
    .end local v12    # "success":I
    :catchall_0
    move-exception v2

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public setAlbumCachingFlag(Ljava/lang/String;I)V
    .locals 2
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "cachingFlag"    # I

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;

    .line 191
    .local v0, "context":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    packed-switch p2, :pswitch_data_0

    .line 200
    :goto_0
    return-void

    .line 199
    :pswitch_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;->getDb()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {p0, v1, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->updateAlbumCachingFlag(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setStopCurrentSyncing(Z)V
    .locals 0
    .param p1, "sync"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z

    .line 105
    return-void
.end method

.method public snsSupport(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 741
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "support_sns"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    const-string/jumbo v0, "true"

    .line 744
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 841
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 842
    .local v3, "match":I
    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    .line 843
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid URI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 847
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mMappings:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;

    .line 848
    .local v2, "mapping":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;
    iget-boolean v5, v2, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->hasId:Z

    if-eqz v5, :cond_1

    .line 849
    invoke-direct {p0, p1, p3}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->whereWithId(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 853
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mDatabase:Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/sns/SNSDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 854
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v5, v2, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$Mapping;->table:Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->getTableName()Ljava/lang/String;

    move-result-object v4

    .line 855
    .local v4, "tableName":Ljava/lang/String;
    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 856
    .local v0, "count":I
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->notifyChange(Landroid/net/Uri;)V

    .line 857
    return v0
.end method

.method public updateAlbums(Ljava/util/ArrayList;Landroid/net/Uri;)V
    .locals 2
    .param p2, "photoUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/AlbumTable;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/AlbumTable;>;"
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mStopSync:Z

    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;

    .line 123
    .local v0, "context":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->syncAlbumAndPhotos(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/util/ArrayList;Landroid/net/Uri;)V

    .line 124
    return-void
.end method

.method public updatePhotos(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V
    .locals 1
    .param p2, "album"    # Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/PhotoTable;",
            ">;",
            "Lcom/sec/android/gallery3d/remote/sns/AlbumTable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/PhotoTable;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->mSyncContext:Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;

    .line 128
    .local v0, "context":Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;
    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->syncPhotos(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$SyncContext;Ljava/util/ArrayList;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V

    .line 129
    return-void
.end method
