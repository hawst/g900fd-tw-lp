.class public final enum Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;
.super Ljava/lang/Enum;
.source "GLPaint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glrenderer/GLPaint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Style"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

.field public static final enum FILL:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

.field public static final enum FILL_AND_STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

.field public static final enum STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;


# instance fields
.field final nativeInt:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    const-string v1, "FILL"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->FILL:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    .line 84
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    const-string v1, "STROKE"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    .line 92
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    const-string v1, "FILL_AND_STROKE"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->FILL_AND_STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    sget-object v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->FILL:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->FILL_AND_STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->$VALUES:[Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nativeInt"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput p3, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->nativeInt:I

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const-class v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->$VALUES:[Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    return-object v0
.end method
