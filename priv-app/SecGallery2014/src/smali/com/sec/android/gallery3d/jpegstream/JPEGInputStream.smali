.class public Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;
.super Ljava/io/FilterInputStream;
.source "JPEGInputStream.java"


# instance fields
.field private JNIPointer:J

.field private mConfigChanged:Z

.field private mFormat:I

.field private mHeight:I

.field private mTmpBuffer:[B

.field private mValidConfig:Z

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 191
    const-string v0, "jni_jpegstream"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->JNIPointer:J

    .line 28
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mValidConfig:Z

    .line 29
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mConfigChanged:Z

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mFormat:I

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mTmpBuffer:[B

    .line 32
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mWidth:I

    .line 33
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mHeight:I

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "format"    # I

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 26
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->JNIPointer:J

    .line 28
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mValidConfig:Z

    .line 29
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mConfigChanged:Z

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mFormat:I

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mTmpBuffer:[B

    .line 32
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mWidth:I

    .line 33
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mHeight:I

    .line 41
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->setConfig(I)Z

    .line 42
    return-void
.end method

.method private applyConfigChange()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 164
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mConfigChanged:Z

    if-eqz v2, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->cleanup()V

    .line 166
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v4, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 167
    .local v0, "dimens":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->in:Ljava/io/InputStream;

    iget v3, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mFormat:I

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->setup(Landroid/graphics/Point;Ljava/io/InputStream;I)I

    move-result v1

    .line 168
    .local v1, "flag":I
    packed-switch v1, :pswitch_data_0

    .line 174
    :pswitch_0
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Error to reading jpeg headers."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 172
    :pswitch_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Bad arguments to read"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 176
    :pswitch_2
    iget v2, v0, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mWidth:I

    .line 177
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mHeight:I

    .line 178
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mConfigChanged:Z

    .line 180
    .end local v0    # "dimens":Landroid/graphics/Point;
    .end local v1    # "flag":I
    :cond_0
    return-void

    .line 168
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private native cleanup()V
.end method

.method private native readDecodedBytes([BII)I
.end method

.method private native setup(Landroid/graphics/Point;Ljava/io/InputStream;I)I
.end method

.method private native skipDecodedBytes(I)I
.end method


# virtual methods
.method public available()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->cleanup()V

    .line 77
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 78
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 157
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->cleanup()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 161
    return-void

    .line 159
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getDimensions()Landroid/graphics/Point;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mValidConfig:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->applyConfigChange()V

    .line 64
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mWidth:I

    iget v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mHeight:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized mark(I)V
    .locals 0
    .param p1, "readlimit"    # I

    .prologue
    .line 83
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mTmpBuffer:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->read([BII)I

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mTmpBuffer:[B

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 103
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v1, p2, p3

    array-length v2, p1

    if-le v1, v2, :cond_1

    .line 104
    :cond_0
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v2, " buffer length %d, offset %d, length %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 108
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mValidConfig:Z

    if-nez v1, :cond_3

    .line 128
    :cond_2
    :goto_0
    return v0

    .line 111
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->applyConfigChange()V

    .line 112
    const/4 v0, -0x1

    .line 114
    .local v0, "flag":I
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->readDecodedBytes([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 116
    if-gez v0, :cond_4

    .line 117
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->cleanup()V

    .line 120
    :cond_4
    if-gez v0, :cond_2

    .line 121
    packed-switch v0, :pswitch_data_0

    .line 125
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error reading jpeg stream"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 116
    :catchall_0
    move-exception v1

    if-gez v0, :cond_5

    .line 117
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->cleanup()V

    :cond_5
    throw v1

    .line 123
    :pswitch_0
    const/4 v0, -0x1

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Reset not supported."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setConfig(I)Z
    .locals 1
    .param p1, "format"    # I

    .prologue
    const/4 v0, 0x1

    .line 46
    sparse-switch p1, :sswitch_data_0

    .line 53
    const/4 v0, 0x0

    .line 58
    :goto_0
    return v0

    .line 55
    :sswitch_0
    iput p1, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mFormat:I

    .line 56
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mValidConfig:Z

    .line 57
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->mConfigChanged:Z

    goto :goto_0

    .line 46
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x104 -> :sswitch_0
    .end sparse-switch
.end method

.method public skip(J)J
    .locals 7
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 138
    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    .line 151
    :goto_0
    :pswitch_0
    return-wide v2

    .line 142
    :cond_0
    const-wide/32 v4, 0x7fffffff

    and-long/2addr v4, p1

    long-to-int v1, v4

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/jpegstream/JPEGInputStream;->skipDecodedBytes(I)I

    move-result v0

    .line 143
    .local v0, "flag":I
    if-gez v0, :cond_1

    .line 144
    packed-switch v0, :pswitch_data_0

    .line 148
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error skipping jpeg stream"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 151
    :cond_1
    int-to-long v2, v0

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_0
    .end packed-switch
.end method
