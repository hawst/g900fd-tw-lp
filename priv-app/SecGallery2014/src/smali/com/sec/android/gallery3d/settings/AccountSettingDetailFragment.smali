.class public Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;
.super Landroid/preference/PreferenceFragment;
.source "AccountSettingDetailFragment.java"

# interfaces
.implements Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountSetting"


# instance fields
.field backStackChangeListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

.field private mAaccountCategory:Landroid/preference/PreferenceGroup;

.field private mAccountPrefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mCloudSyncPrefCategory:Landroid/preference/Preference;

.field private mContext:Landroid/content/Context;

.field private mContextualTagChanged:Z

.field private mContextualTagPref:Landroid/preference/Preference;

.field private mDropBoxSyncPref:Landroid/preference/Preference;

.field private mFaceTagPermissionHandler:Landroid/os/Handler;

.field private final mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

.field private final mResourceId:I

.field private final mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 179
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 60
    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    .line 64
    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagChanged:Z

    .line 72
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mFaceTagPermissionHandler:Landroid/os/Handler;

    .line 85
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 246
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->backStackChangeListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    .line 180
    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .line 181
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    const v0, 0x7f070006

    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mResourceId:I

    .line 189
    :goto_0
    return-void

    .line 184
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v0, :cond_1

    .line 185
    const v0, 0x7f070004

    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mResourceId:I

    goto :goto_0

    .line 187
    :cond_1
    const v0, 0x7f070003

    iput v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mResourceId:I

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;I)V
    .locals 2
    .param p1, "manager"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager;
    .param p2, "resourceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 191
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 60
    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    .line 64
    iput-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagChanged:Z

    .line 72
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mFaceTagPermissionHandler:Landroid/os/Handler;

    .line 85
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    .line 246
    new-instance v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$3;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->backStackChangeListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    .line 192
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .line 193
    iput p2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mResourceId:I

    .line 194
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Lcom/sec/android/gallery3d/settings/AccountSettingManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateContextualTagPrefSwitchState()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagChanged:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagChanged:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startTagBuddyFragment()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mFaceTagPermissionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addAccountPrefs(Landroid/preference/PreferenceGroup;Ljava/util/ArrayList;)I
    .locals 7
    .param p1, "root"    # Landroid/preference/PreferenceGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/PreferenceGroup;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p2, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    const/4 v4, 0x0

    .line 197
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 198
    new-instance v3, Landroid/preference/Preference;

    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 199
    .local v3, "prefs":Landroid/preference/Preference;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0057

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 200
    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 201
    invoke-virtual {p1, v3}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 215
    .end local v3    # "prefs":Landroid/preference/Preference;
    :goto_0
    return v4

    .line 204
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;

    .line 205
    .local v0, "authAccount":Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;
    new-instance v3, Landroid/preference/Preference;

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 206
    .restart local v3    # "prefs":Landroid/preference/Preference;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getIconForAccount(Landroid/accounts/Account;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 207
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 208
    iget-object v4, v0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 209
    iget-object v4, v0, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 211
    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    invoke-virtual {p1, v3}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 214
    .end local v0    # "authAccount":Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;
    .end local v2    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v3    # "prefs":Landroid/preference/Preference;
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->onSyncStateUpdated(Ljava/util/ArrayList;)V

    .line 215
    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto :goto_0
.end method

.method private getIconForAccount(Landroid/accounts/Account;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v6, 0x0

    .line 220
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    move-result-object v2

    .line 221
    .local v2, "descriptions":[Landroid/accounts/AuthenticatorDescription;
    move-object v0, v2

    .local v0, "arr$":[Landroid/accounts/AuthenticatorDescription;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 222
    .local v1, "description":Landroid/accounts/AuthenticatorDescription;
    iget-object v7, v1, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    iget-object v8, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 223
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 224
    .local v5, "pm":Landroid/content/pm/PackageManager;
    iget-object v7, v1, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;

    iget v8, v1, Landroid/accounts/AuthenticatorDescription;->iconId:I

    invoke-virtual {v5, v7, v8, v6}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 227
    .end local v1    # "description":Landroid/accounts/AuthenticatorDescription;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    return-object v6

    .line 221
    .restart local v1    # "description":Landroid/accounts/AuthenticatorDescription;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private isSyncing(Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;)Z
    .locals 5
    .param p1, "authAccount"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;

    .prologue
    .line 231
    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v0

    .line 232
    .local v0, "currentSyncs":Ljava/util/List;, "Ljava/util/List<Landroid/content/SyncInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/SyncInfo;

    .line 233
    .local v2, "syncInfo":Landroid/content/SyncInfo;
    iget-object v3, v2, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    iget-object v4, p1, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    invoke-virtual {v3, v4}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/content/SyncInfo;->authority:Ljava/lang/String;

    iget-object v4, p1, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->authToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 234
    const/4 v3, 0x1

    .line 237
    .end local v2    # "syncInfo":Landroid/content/SyncInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private onSyncStateUpdated(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 512
    .local p1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/Preference;

    .line 513
    .local v1, "prefs":Landroid/preference/Preference;
    invoke-direct {p0, v1, p1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->setSyncStatus(Landroid/preference/Preference;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 515
    .end local v1    # "prefs":Landroid/preference/Preference;
    :cond_0
    return-void
.end method

.method private resetAccountPreferences(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 520
    .local p1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 521
    .local v2, "preferenceScreen":Landroid/preference/PreferenceScreen;
    const-string/jumbo v3, "setting_accounts"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-nez v3, :cond_0

    .line 522
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 525
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v3}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 526
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 535
    .end local v1    # "i":I
    .end local v2    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :catch_0
    move-exception v0

    .line 536
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 538
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-void

    .line 528
    .restart local v1    # "i":I
    .restart local v2    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :cond_2
    const/4 v1, 0x0

    :goto_2
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 529
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 531
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    invoke-direct {p0, v3, p1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->addAccountPrefs(Landroid/preference/PreferenceGroup;Ljava/util/ArrayList;)I

    move-result v3

    if-gtz v3, :cond_1

    .line 532
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private setContextualTagSwitchState()V
    .locals 3

    .prologue
    .line 541
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    if-eqz v1, :cond_2

    .line 542
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    .line 543
    .local v0, "bContextualTagStatus":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_1

    .line 544
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 545
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 547
    :cond_0
    const/4 v0, 0x0

    .line 549
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    if-eqz v0, :cond_3

    const v1, 0x7f0e00bc

    :goto_0
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 550
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 551
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateContextualTagPrefSwitchState()V

    .line 554
    .end local v0    # "bContextualTagStatus":Z
    :cond_2
    return-void

    .line 549
    .restart local v0    # "bContextualTagStatus":Z
    :cond_3
    const v1, 0x7f0e0187

    goto :goto_0
.end method

.method private setSyncStatus(Landroid/preference/Preference;Ljava/util/ArrayList;)V
    .locals 20
    .param p1, "prefs"    # Landroid/preference/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/Preference;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 557
    .local p2, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;

    .line 558
    .local v3, "authAccount":Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;
    iget-object v15, v3, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    iget-object v0, v3, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->authToken:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Landroid/content/ContentResolver;->getSyncStatus(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/SyncStatusInfo;

    move-result-object v10

    .line 560
    .local v10, "status":Landroid/content/SyncStatusInfo;
    const/4 v14, 0x0

    .line 561
    .local v14, "syncIsFailing":Z
    iget-object v15, v3, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    iget-object v0, v3, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->authToken:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v15

    if-eqz v15, :cond_2

    iget-object v15, v3, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    iget-object v0, v3, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->authToken:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v15

    if-lez v15, :cond_2

    const/4 v11, 0x1

    .line 564
    .local v11, "syncEnabled":Z
    :goto_0
    if-nez v10, :cond_3

    const/4 v4, 0x0

    .line 565
    .local v4, "authorityIsPending":Z
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->isSyncing(Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;)Z

    move-result v2

    .line 566
    .local v2, "activelySyncing":Z
    if-eqz v10, :cond_4

    iget-wide v0, v10, Landroid/content/SyncStatusInfo;->lastFailureTime:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-eqz v15, :cond_4

    const/4 v15, 0x0

    invoke-virtual {v10, v15}, Landroid/content/SyncStatusInfo;->getLastFailureMesgAsInt(I)I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_4

    const/4 v7, 0x1

    .line 569
    .local v7, "lastSyncFailed":Z
    :goto_2
    if-nez v11, :cond_0

    .line 570
    const/4 v7, 0x0

    .line 572
    :cond_0
    if-eqz v7, :cond_1

    if-nez v2, :cond_1

    if-nez v4, :cond_1

    .line 573
    const/4 v14, 0x1

    .line 576
    :cond_1
    if-nez v10, :cond_5

    const-wide/16 v12, 0x0

    .line 577
    .local v12, "successEndTime":J
    :goto_3
    if-nez v11, :cond_6

    .line 578
    const v15, 0x7f0e026c

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/preference/Preference;->setSummary(I)V

    .line 592
    :goto_4
    return-void

    .line 561
    .end local v2    # "activelySyncing":Z
    .end local v4    # "authorityIsPending":Z
    .end local v7    # "lastSyncFailed":Z
    .end local v11    # "syncEnabled":Z
    .end local v12    # "successEndTime":J
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 564
    .restart local v11    # "syncEnabled":Z
    :cond_3
    iget-boolean v4, v10, Landroid/content/SyncStatusInfo;->pending:Z

    goto :goto_1

    .line 566
    .restart local v2    # "activelySyncing":Z
    .restart local v4    # "authorityIsPending":Z
    :cond_4
    const/4 v7, 0x0

    goto :goto_2

    .line 576
    .restart local v7    # "lastSyncFailed":Z
    :cond_5
    iget-wide v12, v10, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    goto :goto_3

    .line 579
    .restart local v12    # "successEndTime":J
    :cond_6
    if-eqz v2, :cond_7

    .line 580
    const v15, 0x7f0e026f

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_4

    .line 581
    :cond_7
    const-wide/16 v16, 0x0

    cmp-long v15, v12, v16

    if-eqz v15, :cond_8

    .line 582
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 583
    .local v5, "date":Ljava/util/Date;
    iget-wide v8, v10, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    .line 584
    .local v8, "lastSuccessTime":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v15}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    .line 585
    .local v6, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v5, v8, v9}, Ljava/util/Date;->setTime(J)V

    .line 586
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e026e

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v6, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 587
    .end local v5    # "date":Ljava/util/Date;
    .end local v6    # "dateFormat":Ljava/text/DateFormat;
    .end local v8    # "lastSuccessTime":J
    :cond_8
    if-eqz v14, :cond_9

    .line 588
    const v15, 0x7f0e026d

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_4

    .line 590
    :cond_9
    const-string v15, ""

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method private startTagBuddyFragment()V
    .locals 5

    .prologue
    .line 258
    new-instance v0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;

    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;)V

    .line 259
    .local v0, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 260
    .local v2, "transaction":Landroid/app/FragmentTransaction;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->popBackStack()V

    .line 260
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 263
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v3, :cond_1

    .line 264
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->setContextualTagSettingsFagmentDisplayed(Z)V

    .line 266
    :cond_1
    const v3, 0x7f0f000a

    invoke-virtual {v2, v3, v0}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 267
    const-string/jumbo v3, "setting_contextual_tag_fragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 268
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 269
    const v3, 0x7f0e021f

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->setBreadCrumbTitle(I)Landroid/app/FragmentTransaction;

    .line 270
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 271
    return-void
.end method

.method private updateCloudPref()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 595
    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mCloudSyncPrefCategory:Landroid/preference/Preference;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    if-nez v5, :cond_1

    .line 626
    :cond_0
    :goto_0
    return-void

    .line 599
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->isDropboxSignIn()Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v3

    .line 600
    .local v2, "isDropboxSignIn":Z
    :goto_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->isCloudContentSyncOn()Z

    move-result v5

    if-eqz v5, :cond_4

    move v1, v3

    .line 601
    .local v1, "isCloudContentSyncOn":Z
    :goto_2
    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    .line 602
    const-string v5, "cloud_sync"

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 603
    .local v0, "cloudSyncPrefCategory":Landroid/preference/Preference;
    if-nez v0, :cond_2

    .line 604
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mCloudSyncPrefCategory:Landroid/preference/Preference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 605
    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    iget-object v6, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v5, v6}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 607
    :cond_2
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-eqz v5, :cond_5

    .line 608
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    const v4, 0x7f0e027b

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .end local v0    # "cloudSyncPrefCategory":Landroid/preference/Preference;
    .end local v1    # "isCloudContentSyncOn":Z
    .end local v2    # "isDropboxSignIn":Z
    :cond_3
    move v2, v4

    .line 599
    goto :goto_1

    .restart local v2    # "isDropboxSignIn":Z
    :cond_4
    move v1, v4

    .line 600
    goto :goto_2

    .line 610
    .restart local v0    # "cloudSyncPrefCategory":Landroid/preference/Preference;
    .restart local v1    # "isCloudContentSyncOn":Z
    :cond_5
    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    const v6, 0x7f0e0279

    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v7, "Dropbox"

    aput-object v7, v3, v4

    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 613
    .end local v0    # "cloudSyncPrefCategory":Landroid/preference/Preference;
    :cond_6
    if-eqz v2, :cond_8

    if-nez v1, :cond_8

    .line 614
    const-string v3, "cloud_sync"

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 615
    .restart local v0    # "cloudSyncPrefCategory":Landroid/preference/Preference;
    if-nez v0, :cond_7

    .line 616
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mCloudSyncPrefCategory:Landroid/preference/Preference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 617
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 619
    :cond_7
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e027d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 621
    .end local v0    # "cloudSyncPrefCategory":Landroid/preference/Preference;
    :cond_8
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mCloudSyncPrefCategory:Landroid/preference/Preference;

    if-eqz v3, :cond_0

    .line 622
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mCloudSyncPrefCategory:Landroid/preference/Preference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method

.method private updateContextualTagPrefSwitchState()V
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    instance-of v0, v0, Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 244
    :cond_0
    return-void
.end method

.method private updateHiddenItems()V
    .locals 4

    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 630
    .local v2, "preferenceScreen":Landroid/preference/PreferenceScreen;
    const-string/jumbo v3, "setting_hidden_items"

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceGroup;

    .line 631
    .local v1, "hiddenItems":Landroid/preference/PreferenceGroup;
    const-string v3, "hidden_items"

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 633
    .local v0, "hiddenItemPref":Landroid/preference/Preference;
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 637
    :cond_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v3, :cond_2

    .line 638
    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 640
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->hasHiddenItem()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 641
    iget-object v3, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0

    .line 643
    :cond_3
    if-eqz v0, :cond_0

    .line 644
    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method


# virtual methods
.method public loadPreferenceFragment(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    const/4 v11, 0x0

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 276
    .local v4, "preferenceScreen":Landroid/preference/PreferenceScreen;
    if-nez v4, :cond_0

    .line 392
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    if-nez v9, :cond_1

    .line 282
    const-string/jumbo v9, "setting_accounts"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/PreferenceGroup;

    iput-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    .line 283
    :cond_1
    const-string/jumbo v9, "setting_data_usage"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 284
    .local v0, "dataUsage":Landroid/preference/Preference;
    const-string/jumbo v9, "setting_tags"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceGroup;

    .line 286
    .local v5, "settingsTag":Landroid/preference/PreferenceGroup;
    const-string/jumbo v9, "setting_contextual_tag"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    .line 287
    const-string/jumbo v9, "setting_face_tag"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 288
    .local v1, "faceTag":Landroid/preference/CheckBoxPreference;
    const-string/jumbo v9, "sound_shot"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/PreferenceGroup;

    .line 289
    .local v6, "soundShot":Landroid/preference/PreferenceGroup;
    const-string/jumbo v9, "setting_play_sound_shot_tag"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    .line 291
    .local v7, "soundShotAutoPlay":Landroid/preference/CheckBoxPreference;
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    invoke-direct {p0, v9, p1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->addAccountPrefs(Landroid/preference/PreferenceGroup;Ljava/util/ArrayList;)I

    move-result v9

    if-gtz v9, :cond_2

    .line 292
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    if-eqz v9, :cond_2

    .line 293
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 297
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-nez v9, :cond_5

    .line 298
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsWifiOnlyModel:Z

    if-nez v9, :cond_3

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSnsSettings:Z

    if-eqz v9, :cond_3

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCmcc:Z

    if-nez v9, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 301
    :cond_3
    if-eqz v0, :cond_4

    .line 302
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 303
    const/4 v0, 0x0

    .line 306
    :cond_4
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-nez v9, :cond_5

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-nez v9, :cond_5

    .line 307
    if-eqz v5, :cond_5

    .line 308
    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 309
    const/4 v5, 0x0

    .line 310
    iput-object v11, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    .line 315
    :cond_5
    const-string/jumbo v9, "setting_wifi_only"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Landroid/preference/CheckBoxPreference;

    .line 316
    .local v8, "wifiOnlyPref":Landroid/preference/CheckBoxPreference;
    if-eqz v0, :cond_6

    if-eqz v8, :cond_6

    .line 317
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v10, "setting_wifi_only"

    invoke-static {v9, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 318
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v8, v9}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 322
    :cond_6
    const-string v9, "cloud_sync"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mCloudSyncPrefCategory:Landroid/preference/Preference;

    .line 323
    const-string/jumbo v9, "setting_dropbox_sync"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    .line 324
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    if-eqz v9, :cond_7

    .line 325
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mDropBoxSyncPref:Landroid/preference/Preference;

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v9, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 326
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateCloudPref()V

    .line 330
    :cond_7
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v9, :cond_c

    .line 331
    const-string/jumbo v9, "setting_filterby"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 332
    .local v3, "filterbyPref":Landroid/preference/Preference;
    if-eqz v3, :cond_8

    .line 333
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v3, v9}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 342
    .end local v3    # "filterbyPref":Landroid/preference/Preference;
    :cond_8
    :goto_1
    if-eqz v5, :cond_a

    .line 343
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-nez v9, :cond_d

    .line 344
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    if-eqz v9, :cond_9

    .line 345
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    invoke-virtual {v5, v9}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 346
    iput-object v11, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    .line 371
    :cond_9
    :goto_2
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-nez v9, :cond_e

    .line 372
    if-eqz v1, :cond_a

    .line 373
    invoke-virtual {v5, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 380
    :cond_a
    :goto_3
    if-eqz v6, :cond_b

    .line 381
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSoundScene:Z

    if-eqz v9, :cond_f

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSupportSoundAndShot:Z

    if-eqz v9, :cond_f

    .line 382
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlay(Landroid/content/Context;)Z

    move-result v9

    invoke-virtual {v7, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 383
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v7, v9}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 391
    :cond_b
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateHiddenItems()V

    goto/16 :goto_0

    .line 336
    :cond_c
    const-string v9, "filter"

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    .line 337
    .local v2, "filterbyCategory":Landroid/preference/PreferenceCategory;
    if-eqz v2, :cond_8

    .line 338
    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 349
    .end local v2    # "filterbyCategory":Landroid/preference/PreferenceCategory;
    :cond_d
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v9, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 350
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->setContextualTagSwitchState()V

    .line 351
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    instance-of v9, v9, Landroid/preference/SwitchPreference;

    if-eqz v9, :cond_9

    .line 352
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    check-cast v9, Landroid/preference/SwitchPreference;

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v10

    invoke-virtual {v9, v10}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 353
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    new-instance v10, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    invoke-virtual {v9, v10}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2

    .line 376
    :cond_e
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v9

    invoke-virtual {v1, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 377
    iget-object v9, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mOnPreferenceClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v1, v9}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_3

    .line 385
    :cond_f
    if-eqz v6, :cond_b

    .line 386
    invoke-virtual {v4, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_4
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 396
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->registerOnAccountUpdatedListener(Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;)V

    .line 400
    :cond_0
    return-void
.end method

.method public onCloudStatusUpdated(ZZ)V
    .locals 0
    .param p1, "isDropboxSignIn"    # Z
    .param p2, "isCloudContentSyncOn"    # Z

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateCloudPref()V

    .line 405
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 409
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    .line 412
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v0, :cond_0

    .line 413
    iget v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mResourceId:I

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->addPreferencesFromResource(I)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->getAuthAccounts()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->loadPreferenceFragment(Ljava/util/ArrayList;)V

    .line 416
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 420
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->backStackChangeListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 432
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 3

    .prologue
    .line 437
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v1, :cond_0

    .line 438
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->unregisterOnAccountUpdatedListener(Lcom/sec/android/gallery3d/settings/AccountSettingManager$OnAccountUpdatedListener;)V

    .line 441
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v1, :cond_2

    .line 442
    const/16 v0, 0x111

    .line 446
    .local v0, "dateTagDefaultCheck":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 447
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 448
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f0e00bc

    :goto_1
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 449
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 450
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateContextualTagPrefSwitchState()V

    .line 453
    :cond_1
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDetach()V

    .line 454
    return-void

    .line 444
    .end local v0    # "dateTagDefaultCheck":I
    :cond_2
    const/16 v0, 0x1111

    .restart local v0    # "dateTagDefaultCheck":I
    goto :goto_0

    .line 448
    :cond_3
    const v1, 0x7f0e0187

    goto :goto_1
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 458
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 460
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->setContextualTagSwitchState()V

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$5;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$5;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 490
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 491
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 492
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 493
    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 494
    .local v1, "preferencesList":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 495
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 499
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "preferencesList":Landroid/view/View;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateHiddenItems()V

    .line 500
    return-void
.end method

.method public onSyncAccontsUpdated(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 504
    .local p1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAaccountCategory:Landroid/preference/PreferenceGroup;

    if-nez v0, :cond_0

    .line 509
    :goto_0
    return-void

    .line 507
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->resetAccountPreferences(Ljava/util/ArrayList;)V

    .line 508
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->onSyncStateUpdated(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
