.class public Lcom/sec/android/gallery3d/data/FestivalClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "FestivalClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/FestivalClustering$1;,
        Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;
    }
.end annotation


# static fields
.field private static final MMDDYY_FORMAT:Ljava/lang/String; = "MMddyy"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/FestivalCluster;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrCluster:Lcom/sec/android/gallery3d/data/FestivalCluster;

.field private mNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/gallery3d/data/FestivalClustering;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mContext:Landroid/content/Context;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Lcom/sec/android/gallery3d/data/FestivalCluster;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/FestivalCluster;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/FestivalCluster;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/FestivalClustering;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/FestivalClustering;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public generateCaption(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "minTimestamp"    # J
    .param p4, "maxTimestamp"    # J

    .prologue
    .line 180
    const-wide/16 v8, 0x0

    cmp-long v5, p2, v8

    if-nez v5, :cond_1

    const-string v4, ""

    .line 224
    :cond_0
    :goto_0
    return-object v4

    .line 183
    :cond_1
    const-string v5, "MMddyy"

    move-wide/from16 v0, p2

    invoke-static {v5, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    .line 185
    .local v18, "minDay":Ljava/lang/String;
    const-string v5, "MMddyy"

    move-wide/from16 v0, p4

    invoke-static {v5, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v17

    .line 188
    .local v17, "maxDay":Ljava/lang/String;
    const/4 v5, 0x4

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 191
    const/high16 v10, 0x80000

    move-object/from16 v5, p1

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-static/range {v5 .. v10}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "caption":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 198
    const v14, 0x10010

    .line 201
    .local v14, "flags":I
    move-object/from16 v0, p1

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2, v14}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v15

    .line 203
    .local v15, "dateRangeWithOptionalYear":Ljava/lang/String;
    const v5, 0x10014

    move-object/from16 v0, p1

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v16

    .line 205
    .local v16, "dateRangeWithYear":Ljava/lang/String;
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 210
    add-long v8, p2, p4

    const-wide/16 v10, 0x2

    div-long v6, v8, v10

    .line 211
    .local v6, "midTimestamp":J
    const v10, 0x10011

    move-object/from16 v5, p1

    move-wide v8, v6

    invoke-static/range {v5 .. v10}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 218
    .end local v4    # "caption":Ljava/lang/String;
    .end local v6    # "midTimestamp":J
    .end local v14    # "flags":I
    .end local v15    # "dateRangeWithOptionalYear":Ljava/lang/String;
    .end local v16    # "dateRangeWithYear":Ljava/lang/String;
    :cond_2
    const v14, 0x10030

    .restart local v14    # "flags":I
    move-object/from16 v9, p1

    move-wide/from16 v10, p2

    move-wide/from16 v12, p4

    .line 220
    invoke-static/range {v9 .. v14}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "caption":Ljava/lang/String;
    goto :goto_0
.end method

.method public getCluster(I)Ljava/util/ArrayList;
    .locals 7
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt p1, v4, :cond_1

    .line 155
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCluster index("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") error. size:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/4 v3, 0x0

    .line 163
    :cond_0
    return-object v3

    .line 158
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/FestivalCluster;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/FestivalCluster;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 159
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 160
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 161
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mNames:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 169
    const-string v0, ""

    .line 170
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 34
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 67
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    const-string v6, "run start"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v32

    .line 69
    .local v32, "total":I
    if-gtz v32, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mContext:Landroid/content/Context;

    check-cast v14, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 74
    .local v14, "appImplContext":Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getFestivalStartTime()J

    move-result-wide v30

    .line 75
    .local v30, "startTime":J
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getFestivalEndTime()J

    move-result-wide v22

    .line 77
    .local v22, "endTime":J
    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 78
    .local v5, "uriImage":Landroid/net/Uri;
    sget-object v33, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 80
    .local v33, "uriVideo":Landroid/net/Uri;
    const/16 v17, 0x0

    .line 81
    .local v17, "cursorImage":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 82
    .local v18, "cursorVideo":Landroid/database/Cursor;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(datetaken >= "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " AND "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "datetaken"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " <= "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 84
    .local v7, "selection":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "selection is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/4 v15, 0x0

    .line 87
    .local v15, "countImage":I
    const/16 v16, 0x0

    .line 88
    .local v16, "countVideo":I
    new-instance v25, Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-direct {v0, v15}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    .local v25, "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v28, Ljava/util/ArrayList;

    move-object/from16 v0, v28

    move/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 91
    .local v28, "itemVideo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v27, Ljava/util/ArrayList;

    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 94
    .local v27, "itemTotal":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 95
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v9, v33

    move-object v11, v7

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 96
    if-eqz v17, :cond_3

    .line 97
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v15

    .line 98
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ImageCount = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-instance v26, Ljava/util/ArrayList;

    move-object/from16 v0, v26

    invoke-direct {v0, v15}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    .end local v25    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .local v26, "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const/4 v4, 0x1

    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v4}, Lcom/sec/android/gallery3d/data/FestivalClustering;->buildCursorToSmallItem(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V

    .line 101
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v21

    if-ge v0, v4, :cond_2

    .line 102
    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 101
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v25, v26

    .line 105
    .end local v21    # "i":I
    .end local v26    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .restart local v25    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    :cond_3
    if-eqz v18, :cond_4

    .line 106
    :try_start_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v16

    .line 107
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "VideoCount = "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2, v4}, Lcom/sec/android/gallery3d/data/FestivalClustering;->buildCursorToSmallItem(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V

    .line 109
    const/16 v21, 0x0

    .restart local v21    # "i":I
    :goto_2
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v21

    if-ge v0, v4, :cond_4

    .line 110
    move-object/from16 v0, v28

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 114
    .end local v21    # "i":I
    :cond_4
    new-instance v19, Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/data/FestivalClustering$DateComparator;-><init>(Lcom/sec/android/gallery3d/data/FestivalClustering;Lcom/sec/android/gallery3d/data/FestivalClustering$1;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 116
    .local v19, "dateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    :try_start_3
    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    :goto_3
    const/16 v21, 0x0

    .restart local v21    # "i":I
    :goto_4
    :try_start_4
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v21

    if-ge v0, v4, :cond_6

    .line 122
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/FestivalCluster;

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-virtual {v6, v4}, Lcom/sec/android/gallery3d/data/FestivalCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 121
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 117
    .end local v21    # "i":I
    :catch_0
    move-exception v24

    .line 118
    .local v24, "iae":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "conversationInfoCursor sorting error "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v24 .. v24}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 135
    .end local v19    # "dateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .end local v24    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v20

    .line 136
    .local v20, "e":Ljava/lang/NullPointerException;
    :goto_5
    :try_start_5
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    const-string v6, "NullPointerException at getGalleryCount"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 138
    if-eqz v17, :cond_5

    .line 139
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 141
    :cond_5
    if-eqz v18, :cond_0

    .line 142
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 124
    .end local v20    # "e":Ljava/lang/NullPointerException;
    .restart local v19    # "dateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .restart local v21    # "i":I
    :cond_6
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/FestivalCluster;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/FestivalCluster;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mContext:Landroid/content/Context;

    move-object/from16 v8, p0

    move-wide/from16 v10, v30

    move-wide/from16 v12, v22

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/gallery3d/data/FestivalClustering;->generateCaption(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/data/FestivalCluster;->setName(Ljava/lang/String;)V

    .line 128
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v29

    .line 129
    .local v29, "m":I
    move/from16 v0, v29

    new-array v4, v0, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mNames:[Ljava/lang/String;

    .line 130
    const/16 v21, 0x0

    :goto_6
    move/from16 v0, v21

    move/from16 v1, v29

    if-ge v0, v1, :cond_7

    .line 131
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mNames:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/FestivalClustering;->mClusters:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/FestivalCluster;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/FestivalCluster;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v21

    .line 130
    add-int/lit8 v21, v21, 0x1

    goto :goto_6

    .line 134
    :cond_7
    sget-object v4, Lcom/sec/android/gallery3d/data/FestivalClustering;->TAG:Ljava/lang/String;

    const-string v6, "run end"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 138
    if-eqz v17, :cond_8

    .line 139
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 141
    :cond_8
    if-eqz v18, :cond_0

    .line 142
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 138
    .end local v19    # "dateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .end local v21    # "i":I
    .end local v29    # "m":I
    :catchall_0
    move-exception v4

    :goto_7
    if-eqz v17, :cond_9

    .line 139
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 141
    :cond_9
    if-eqz v18, :cond_a

    .line 142
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v4

    .line 138
    .end local v25    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .restart local v26    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    :catchall_1
    move-exception v4

    move-object/from16 v25, v26

    .end local v26    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .restart local v25    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    goto :goto_7

    .line 135
    .end local v25    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .restart local v26    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    :catch_2
    move-exception v20

    move-object/from16 v25, v26

    .end local v26    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    .restart local v25    # "itemImage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    goto/16 :goto_5
.end method
