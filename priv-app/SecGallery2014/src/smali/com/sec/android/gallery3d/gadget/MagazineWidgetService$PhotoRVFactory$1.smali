.class Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;
.super Ljava/lang/Thread;
.source "MagazineWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->getSingleFrame(I)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 404
    const/16 v4, 0xa

    invoke-static {v4}, Landroid/os/Process;->setThreadPriority(I)V

    .line 406
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFuturePosition:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$200(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)I

    move-result v3

    .line 407
    .local v3, "localFuturePosition":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$300(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$400(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 408
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mIsInit == true"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$600(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Ljava/util/HashMap;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$500(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-static {v7}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$400(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v7

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsInit:Z
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$302(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # setter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;
    invoke-static {v4, v10}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$1002(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 433
    .end local v3    # "localFuturePosition":I
    :goto_0
    return-void

    .line 413
    .restart local v3    # "localFuturePosition":I
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->QUEUE_COUNT_FUTURE_BMP:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$700(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 414
    add-int v4, v3, v1

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSourceSize:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$800(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)I

    move-result v5

    rem-int v2, v4, v5

    .line 415
    .local v2, "index":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$600(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v11, :cond_3

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$600(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$600(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_3

    .line 417
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "index is continue : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 421
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mIsThreadStop:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$900(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Z

    move-result v4

    if-eq v4, v11, :cond_0

    .line 423
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$400(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 424
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureBmpMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$600(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mTempContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$500(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mSource:Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-static {v7}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$400(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;)Lcom/sec/android/gallery3d/gadget/WidgetSource;

    move-result-object v7

    invoke-interface {v7, v2}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v7

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getWidgetBitmap(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;II)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 428
    .end local v1    # "i":I
    .end local v2    # "index":I
    .end local v3    # "localFuturePosition":I
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getViewAt: exception occurred!! >>  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 431
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # setter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;
    invoke-static {v4, v10}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$1002(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;Ljava/lang/Thread;)Ljava/lang/Thread;

    goto/16 :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory$1;->this$0:Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;

    # setter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->mFutureThread:Ljava/lang/Thread;
    invoke-static {v5, v10}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;->access$1002(Lcom/sec/android/gallery3d/gadget/MagazineWidgetService$PhotoRVFactory;Ljava/lang/Thread;)Ljava/lang/Thread;

    throw v4
.end method
