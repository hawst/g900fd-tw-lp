.class Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;
.super Ljava/lang/Object;
.source "NearbyClient.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->startAllShareFrameworkService()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 4
    .param p1, "serviceProvider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "serviceState"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 296
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ServiceProvider Created : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    sget-object v1, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    if-ne p2, v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mUseAsf:Z
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$402(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 302
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-object v1, p1

    check-cast v1, Lcom/samsung/android/allshare/media/MediaServiceProvider;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$502(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 303
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/ServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/DeviceFinder;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$602(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 305
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setDeviceFinderEventListener. provider and imageviewer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$600(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 308
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$600(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 311
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshDeviceList()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$800(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    .line 313
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 314
    .local v0, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 315
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "update device change icon handler at first"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerIcon()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    .line 319
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playPendedPlayer()V

    .line 320
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty()V

    .line 321
    return-void
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 3
    .param p1, "serviceProvider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    const/4 v2, 0x0

    .line 325
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ServiceProvider Deleted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mUseAsf:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mUseAsf:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$402(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 328
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ServiceProvider is stopped by system. try restart"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$500(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->startAllShareFrameworkService()V

    .line 336
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$502(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 334
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$602(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    goto :goto_0
.end method
