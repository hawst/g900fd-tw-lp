.class Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;
.super Ljava/lang/Object;
.source "WidgetPreferenceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LayoutChanger"
.end annotation


# instance fields
.field private mCol:I

.field private final mDbFrameTypes:[[I

.field private final mFocusedDrawables:[[I

.field private final mIds:[[I

.field private mIsSelected:Z

.field private mListView:Landroid/widget/ListView;

.field private final mNormalDrawables:[[I

.field private mRow:I

.field private final mSelectedDrawables:[[I

.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 169
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIds:[[I

    .line 175
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mDbFrameTypes:[[I

    .line 180
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mNormalDrawables:[[I

    .line 185
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_6

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_7

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mFocusedDrawables:[[I

    .line 190
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_8

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_9

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mSelectedDrawables:[[I

    .line 196
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIsSelected:Z

    return-void

    .line 170
    nop

    :array_0
    .array-data 4
        0x7f0f01f0
        0x7f0f01f1
    .end array-data

    :array_1
    .array-data 4
        0x7f0f01f2
        0x7f0f01f3
    .end array-data

    .line 175
    :array_2
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_3
    .array-data 4
        0x2
        0x3
    .end array-data

    .line 180
    :array_4
    .array-data 4
        0x7f020581
        0x7f020578
    .end array-data

    :array_5
    .array-data 4
        0x7f02057b
        0x7f02057e
    .end array-data

    .line 185
    :array_6
    .array-data 4
        0x7f020582
        0x7f020579
    .end array-data

    :array_7
    .array-data 4
        0x7f02057c
        0x7f02057f
    .end array-data

    .line 190
    :array_8
    .array-data 4
        0x7f020583
        0x7f02057a
    .end array-data

    :array_9
    .array-data 4
        0x7f02057d
        0x7f020580
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V

    return-void
.end method

.method private changeImageResource([[I)V
    .locals 5
    .param p1, "drawables"    # [[I

    .prologue
    .line 275
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIds:[[I

    iget v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mRow:I

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mCol:I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 276
    .local v1, "imgView":Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mFocusedDrawables:[[I

    if-eq p1, v2, :cond_0

    .line 277
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 278
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mDbFrameTypes:[[I

    iget v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mRow:I

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mCol:I

    aget v3, v3, v4

    if-ne v2, v3, :cond_1

    .line 279
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mSelectedDrawables:[[I

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mRow:I

    aget-object v2, v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mCol:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 286
    .end local v0    # "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    :goto_0
    return-void

    .line 283
    :cond_0
    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 285
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mRow:I

    aget-object v2, p1, v2

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mCol:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private resetDrawables()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 289
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v5, v5, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 290
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_2

    .line 291
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-ge v3, v6, :cond_1

    .line 292
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIds:[[I

    aget-object v5, v5, v1

    aget v5, v5, v3

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 293
    .local v2, "imgView":Landroid/widget/ImageView;
    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mDbFrameTypes:[[I

    aget-object v5, v5, v1

    aget v5, v5, v3

    if-ne v4, v5, :cond_0

    .line 294
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mSelectedDrawables:[[I

    aget-object v4, v4, v1

    aget v4, v4, v3

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 291
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 296
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mNormalDrawables:[[I

    aget-object v4, v4, v1

    aget v4, v4, v3

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 290
    .end local v2    # "imgView":Landroid/widget/ImageView;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 300
    .end local v3    # "j":I
    :cond_2
    return-void
.end method

.method private setCurrentPosition(II)V
    .locals 0
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    .line 200
    iput p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mRow:I

    iput p2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mCol:I

    .line 201
    return-void
.end method


# virtual methods
.method protected dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    .line 222
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIsSelected:Z

    if-nez v5, :cond_1

    .line 271
    :cond_0
    :goto_0
    return v1

    .line 225
    :cond_1
    iget v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mRow:I

    .line 226
    .local v3, "newRow":I
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mCol:I

    .line 228
    .local v2, "newCol":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto :goto_0

    .line 230
    :sswitch_0
    add-int/lit8 v3, v3, -0x1

    .line 254
    :goto_1
    const/4 v1, 0x1

    .line 257
    .local v1, "isValidIndex":Z
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIds:[[I

    aget-object v5, v5, v3

    aget v5, v5, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :goto_2
    if-eqz v1, :cond_0

    .line 264
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mNormalDrawables:[[I

    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->changeImageResource([[I)V

    .line 266
    invoke-direct {p0, v3, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->setCurrentPosition(II)V

    .line 268
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mFocusedDrawables:[[I

    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->changeImageResource([[I)V

    goto :goto_0

    .line 233
    .end local v1    # "isValidIndex":Z
    :sswitch_1
    add-int/lit8 v2, v2, -0x1

    .line 234
    goto :goto_1

    .line 236
    :sswitch_2
    add-int/lit8 v2, v2, 0x1

    .line 237
    goto :goto_1

    .line 239
    :sswitch_3
    add-int/lit8 v3, v3, 0x1

    .line 240
    goto :goto_1

    .line 243
    :sswitch_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_0

    .line 246
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIds:[[I

    iget v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mRow:I

    aget-object v6, v6, v7

    iget v7, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mCol:I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 247
    .local v4, "view":Landroid/view/View;
    iget-object v5, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    # getter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutCategory:Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;
    invoke-static {v5}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$300(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 258
    .end local v4    # "view":Landroid/view/View;
    .restart local v1    # "isValidIndex":Z
    :catch_0
    move-exception v0

    .line 259
    .local v0, "ae":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v1, 0x0

    .line 260
    # getter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ignore : newRow : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", newCol : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 228
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_3
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_4
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method public setListView(Landroid/widget/AdapterView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 205
    check-cast p1, Landroid/widget/ListView;

    .end local p1    # "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mListView:Landroid/widget/ListView;

    .line 206
    :cond_0
    return-void
.end method

.method public setSelected(Z)V
    .locals 2
    .param p1, "isSelected"    # Z

    .prologue
    const/4 v1, 0x0

    .line 209
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIsSelected:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 210
    invoke-direct {p0, v1, v1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->setCurrentPosition(II)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mFocusedDrawables:[[I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->changeImageResource([[I)V

    .line 214
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIsSelected:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 215
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->resetDrawables()V

    .line 218
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->mIsSelected:Z

    .line 219
    return-void
.end method
