.class public Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
.super Lcom/sec/android/gallery3d/remote/picasa/Entry;
.source "AlbumEntry.java"


# annotations
.annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Table;
    value = "albums"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry$Columns;
    }
.end annotation


# static fields
.field public static final ALBUM_INVALID:I = 0x1

.field public static final ALBUM_NOT_VALIDATED:I = 0x0

.field public static final ALBUM_VALID:I = 0x2

.field public static final SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

.field public static final STATUS_CACHED_SCREENNAIL:I = 0x2

.field public static final STATUS_CACHING:I = 0x1

.field public static final STATUS_NOT_CACHED:I


# instance fields
.field public bytesUsed:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "bytes_used"
    .end annotation
.end field

.field public cacheStatus:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        doNotMerge = true
        extraSql = "DEFAULT 0"
        value = "cache_status"
    .end annotation
.end field

.field public dateEdited:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "date_edited"
    .end annotation
.end field

.field public datePublished:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "date_published"
    .end annotation
.end field

.field public dateUpdated:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "date_updated"
    .end annotation
.end field

.field public editUri:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "edit_uri"
    .end annotation
.end field

.field public htmlPageUrl:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "html_page_url"
    .end annotation
.end field

.field public locationString:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "location_string"
    .end annotation
.end field

.field public numPhotos:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "num_photos"
    .end annotation
.end field

.field public photosDirty:Z
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "photos_dirty"
    .end annotation
.end field

.field public photosEtag:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "photos_etag"
    .end annotation
.end field

.field public summary:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "summary"
    .end annotation
.end field

.field public syncAccount:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "sync_account"
    .end annotation
.end field

.field public thumbnailUrl:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "thumbnail_url"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "title"
    .end annotation
.end field

.field public user:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "user"
    .end annotation
.end field

.field public valid:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        doNotMerge = true
        extraSql = "DEFAULT 0"
        value = "valid"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    const-class v1, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/Entry;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosEtag:Ljava/lang/String;

    .line 146
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 162
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/picasa/Entry;->clear()V

    .line 163
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    .line 164
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->cacheStatus:I

    .line 165
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosDirty:Z

    .line 166
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->editUri:Ljava/lang/String;

    .line 167
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->user:Ljava/lang/String;

    .line 168
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->title:Ljava/lang/String;

    .line 169
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->summary:Ljava/lang/String;

    .line 170
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->datePublished:J

    .line 171
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateUpdated:J

    .line 172
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateEdited:J

    .line 173
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->numPhotos:I

    .line 174
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->bytesUsed:J

    .line 175
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->locationString:Ljava/lang/String;

    .line 176
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->thumbnailUrl:Ljava/lang/String;

    .line 177
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->htmlPageUrl:Ljava/lang/String;

    .line 178
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->valid:I

    .line 179
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 183
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    if-nez v2, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 185
    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    .line 186
    .local v0, "p":Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->cacheStatus:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->cacheStatus:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosDirty:Z

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosDirty:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->editUri:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->editUri:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->user:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->user:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->title:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->summary:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->summary:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->datePublished:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->datePublished:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateUpdated:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateUpdated:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateEdited:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateEdited:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->numPhotos:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->numPhotos:I

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->bytesUsed:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->bytesUsed:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->locationString:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->locationString:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->thumbnailUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->thumbnailUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->htmlPageUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->htmlPageUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->valid:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->valid:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public setPropertyFromXml(Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;
    .param p4, "content"    # Ljava/lang/String;

    .prologue
    .line 210
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 211
    .local v1, "localNameChar":C
    const-string v3, "http://schemas.google.com/photos/2007"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 212
    sparse-switch v1, :sswitch_data_0

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 214
    :sswitch_0
    const-string v3, "id"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 215
    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->id:J

    goto :goto_0

    .line 219
    :sswitch_1
    const-string/jumbo v3, "user"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->user:Ljava/lang/String;

    goto :goto_0

    .line 224
    :sswitch_2
    const-string v3, "numphotos"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 225
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->numPhotos:I

    goto :goto_0

    .line 229
    :sswitch_3
    const-string v3, "bytesUsed"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 230
    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->bytesUsed:J

    goto :goto_0

    .line 236
    :cond_1
    const-string v3, "http://www.w3.org/2005/Atom"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 237
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 259
    :pswitch_1
    const-string v3, "link"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 260
    const-string v3, ""

    const-string v4, "rel"

    invoke-interface {p3, v3, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 261
    .local v2, "rel":Ljava/lang/String;
    const-string v3, ""

    const-string v4, "href"

    invoke-interface {p3, v3, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "href":Ljava/lang/String;
    const-string v3, "alternate"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v3, "text/html"

    const-string v4, ""

    const-string/jumbo v5, "type"

    invoke-interface {p3, v4, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 264
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->htmlPageUrl:Ljava/lang/String;

    goto :goto_0

    .line 239
    .end local v0    # "href":Ljava/lang/String;
    .end local v2    # "rel":Ljava/lang/String;
    :pswitch_2
    const-string/jumbo v3, "title"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 240
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 244
    :pswitch_3
    const-string/jumbo v3, "summary"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 245
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->summary:Ljava/lang/String;

    goto/16 :goto_0

    .line 249
    :pswitch_4
    const-string v3, "published"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 250
    invoke-static {p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->parseAtomTimestamp(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->datePublished:J

    goto/16 :goto_0

    .line 254
    :pswitch_5
    const-string/jumbo v3, "updated"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 255
    invoke-static {p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->parseAtomTimestamp(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateUpdated:J

    goto/16 :goto_0

    .line 265
    .restart local v0    # "href":Ljava/lang/String;
    .restart local v2    # "rel":Ljava/lang/String;
    :cond_2
    const-string v3, "edit"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 266
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->editUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 273
    .end local v0    # "href":Ljava/lang/String;
    .end local v2    # "rel":Ljava/lang/String;
    :cond_3
    const-string v3, "http://www.w3.org/2007/app"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 274
    const-string v3, "edited"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 275
    invoke-static {p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->parseAtomTimestamp(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateEdited:J

    goto/16 :goto_0

    .line 277
    :cond_4
    const-string v3, "http://search.yahoo.com/mrss/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 279
    const-string/jumbo v3, "thumbnail"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 280
    const-string v3, ""

    const-string/jumbo v4, "url"

    invoke-interface {p3, v3, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->thumbnailUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 212
    :sswitch_data_0
    .sparse-switch
        0x62 -> :sswitch_3
        0x69 -> :sswitch_0
        0x6e -> :sswitch_2
        0x75 -> :sswitch_1
    .end sparse-switch

    .line 237
    :pswitch_data_0
    .packed-switch 0x6c
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method
