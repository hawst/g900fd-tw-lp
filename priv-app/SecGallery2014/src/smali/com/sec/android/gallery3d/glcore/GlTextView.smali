.class public Lcom/sec/android/gallery3d/glcore/GlTextView;
.super Lcom/sec/android/gallery3d/glcore/GlView;
.source "GlTextView.java"


# static fields
.field private static final mDefaultPaintForLabel:Landroid/text/TextPaint;


# instance fields
.field private mIsRTL:Z

.field private mMetrics:Landroid/graphics/Paint$FontMetricsInt;

.field private mPaint:Landroid/text/TextPaint;

.field private mScaleX:F

.field private mScaleY:F

.field private mText:Ljava/lang/String;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mDefaultPaintForLabel:Landroid/text/TextPaint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/text/TextPaint;Landroid/graphics/Paint$FontMetricsInt;II)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "paint"    # Landroid/text/TextPaint;
    .param p3, "metrics"    # Landroid/graphics/Paint$FontMetricsInt;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 28
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlView;-><init>()V

    .line 22
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mScaleX:F

    .line 23
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mScaleY:F

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mIsRTL:Z

    .line 29
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    .line 31
    iput-object p3, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    .line 32
    invoke-virtual {p0, p4, p5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 33
    return-void
.end method

.method public static ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "postfix"    # Ljava/lang/String;
    .param p2, "lengthLimit"    # F
    .param p3, "paint"    # Landroid/text/TextPaint;

    .prologue
    .line 86
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 87
    :cond_0
    const-string p0, ""

    .line 98
    .end local p0    # "text":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 88
    .restart local p0    # "text":Ljava/lang/String;
    :cond_1
    if-eqz p1, :cond_2

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p3, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float v1, p2, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p2

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p0, p3, p2, v1}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 94
    :cond_2
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p0, p3, p2, v0}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getDefaultPaint(FI)Landroid/text/TextPaint;
    .locals 4
    .param p0, "textSize"    # F
    .param p1, "color"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 48
    .local v0, "paint":Landroid/text/TextPaint;
    invoke-virtual {v0, p0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 49
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 50
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 51
    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 52
    if-ne p1, v3, :cond_0

    .line 53
    invoke-virtual {v0, v2, v2, v2, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 56
    :goto_0
    return-object v0

    .line 55
    :cond_0
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    goto :goto_0
.end method

.method public static getDefaultPaintForLabel(F)Landroid/text/TextPaint;
    .locals 1
    .param p0, "textSize"    # F

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mDefaultPaintForLabel:Landroid/text/TextPaint;

    invoke-virtual {v0, p0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 43
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mDefaultPaintForLabel:Landroid/text/TextPaint;

    return-object v0
.end method

.method public static newInstance(II)Lcom/sec/android/gallery3d/glcore/GlTextView;
    .locals 6
    .param p0, "width"    # I
    .param p1, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 204
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlTextView;

    const-string v1, ""

    move-object v3, v2

    move v4, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;-><init>(Ljava/lang/String;Landroid/text/TextPaint;Landroid/graphics/Paint$FontMetricsInt;II)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "textSize"    # F
    .param p2, "color"    # I

    .prologue
    .line 61
    invoke-static {p1, p2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v0

    return-object v0
.end method

.method private static newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glcore/GlTextView;
    .locals 6
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "paint"    # Landroid/text/TextPaint;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    .line 122
    .local v3, "metrics":Landroid/graphics/Paint$FontMetricsInt;
    invoke-virtual {p1, p0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v4, v0

    .line 123
    .local v4, "width":I
    iget v0, v3, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v1, v3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v5, v0, v1

    .line 125
    .local v5, "height":I
    if-gtz v4, :cond_0

    const/4 v4, 0x1

    .line 126
    :cond_0
    if-gtz v5, :cond_1

    const/4 v5, 0x1

    .line 127
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;-><init>(Ljava/lang/String;Landroid/text/TextPaint;Landroid/graphics/Paint$FontMetricsInt;II)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glcore/GlTextView;
    .locals 7
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "postfix"    # Ljava/lang/String;
    .param p2, "textSize"    # F
    .param p3, "color"    # I
    .param p4, "lengthLimit"    # F
    .param p5, "isBold"    # Z

    .prologue
    .line 83
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Ljava/lang/String;FIFZZ)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;FIFZZ)Lcom/sec/android/gallery3d/glcore/GlTextView;
    .locals 4
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "postfix"    # Ljava/lang/String;
    .param p2, "textSize"    # F
    .param p3, "color"    # I
    .param p4, "lengthLimit"    # F
    .param p5, "isBold"    # Z
    .param p6, "showShadow"    # Z

    .prologue
    const/4 v3, 0x0

    .line 67
    invoke-static {p2, p3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v0

    .line 68
    .local v0, "paint":Landroid/text/TextPaint;
    const/high16 v1, -0x1000000

    if-ne p3, v1, :cond_0

    if-eqz p6, :cond_0

    .line 69
    const/high16 v1, 0x40000000    # 2.0f

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 72
    :cond_0
    if-eqz p5, :cond_1

    .line 73
    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 76
    :cond_1
    invoke-static {p0, p1, p4, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object p0

    .line 77
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Landroid/text/TextPaint;)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getTextPaint()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTextWidth()I
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    if-nez v0, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 200
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 161
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mTextView:Landroid/widget/TextView;

    if-nez v1, :cond_3

    .line 162
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mTranslateGlTextCanvas:Z

    if-eqz v1, :cond_1

    .line 163
    const/high16 v0, 0x40200000    # 2.5f

    .line 164
    .local v0, "yDistance":F
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    neg-int v1, v1

    int-to-float v1, v1

    add-float/2addr v1, v0

    invoke-virtual {p1, v4, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 168
    .end local v0    # "yDistance":F
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mScaleX:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mScaleY:F

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 169
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mIsRTL:Z

    if-eqz v1, :cond_2

    .line 170
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v1, v2, v4, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 178
    :cond_0
    :goto_1
    return-void

    .line 166
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v4, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v1, v4, v4, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mTextView:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 153
    :cond_0
    return-void
.end method

.method public setIsRTL(Z)V
    .locals 0
    .param p1, "isRTL"    # Z

    .prologue
    .line 220
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mIsRTL:Z

    .line 221
    return-void
.end method

.method public setScale(FF)V
    .locals 0
    .param p1, "sx"    # F
    .param p2, "sy"    # F

    .prologue
    .line 156
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mScaleX:F

    .line 157
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mScaleY:F

    .line 158
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 132
    .local v1, "width":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v0, v2, v3

    .line 134
    .local v0, "height":I
    if-gtz v1, :cond_0

    const/4 v1, 0x1

    .line 135
    :cond_0
    if-gtz v0, :cond_1

    const/4 v0, 0x1

    .line 136
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    .line 137
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->invalidate()V

    .line 139
    return-void
.end method

.method public setText(Ljava/lang/String;I)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "width"    # I

    .prologue
    .line 142
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v0, v1, v2

    .line 143
    .local v0, "height":I
    if-gtz v0, :cond_0

    const/4 v0, 0x1

    .line 144
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    .line 145
    invoke-virtual {p0, p2, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->invalidate()V

    .line 147
    return-void
.end method

.method public setTextBold()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 183
    :cond_0
    return-void
.end method

.method public setTextSize(F)V
    .locals 6
    .param p1, "textSize"    # F

    .prologue
    .line 186
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 187
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 188
    .local v1, "metrics":Landroid/graphics/Paint$FontMetricsInt;
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mMetrics:Landroid/graphics/Paint$FontMetricsInt;

    .line 189
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 190
    .local v2, "width":I
    iget v3, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v4, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v0, v3, v4

    .line 191
    .local v0, "height":I
    if-gtz v2, :cond_0

    const/4 v2, 0x1

    .line 192
    :cond_0
    if-gtz v0, :cond_1

    const/4 v0, 0x1

    .line 193
    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 194
    return-void
.end method

.method public setTextView(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlTextView;->mTextView:Landroid/widget/TextView;

    .line 209
    return-void
.end method
