.class public Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr2;
.super Lcom/sec/android/gallery3d/glcore/GlInterpolator;
.source "GlInterpolatorSqr2.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;-><init>()V

    .line 7
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 16
    sub-float v0, v3, p1

    .line 17
    .local v0, "intVal":F
    mul-float v2, v0, v0

    mul-float/2addr v2, v0

    sub-float v1, v3, v2

    .line 18
    .local v1, "retVal":F
    return v1
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 10
    return-void
.end method
