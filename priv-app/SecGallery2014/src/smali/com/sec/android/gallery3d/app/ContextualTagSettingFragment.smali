.class public Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;
.super Landroid/app/Fragment;
.source "ContextualTagSettingFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final CONTEXTUAL_TAG_ENABLE:Ljava/lang/String; = "ContextualTagEnable"

.field public static final CONTEXTUAL_TAG_LIST:Ljava/lang/String; = "ContextualTagList"

.field private static isContextualTagEnabled:Z

.field private static isContextualTagEnabledInited:Z


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mActivity:Landroid/app/Activity;

.field private mCategoryIcon:Landroid/widget/ImageView;

.field private mCategoryTextView:Landroid/widget/TextView;

.field private mCheckedItem:I

.field private mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

.field mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

.field private mDateIcon:Landroid/widget/ImageView;

.field private mDateTextView:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field private mLocationIcon:Landroid/widget/ImageView;

.field private mLocationTextView:Landroid/widget/TextView;

.field private mScrollview:Landroid/view/View;

.field private mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

.field private mUserTagsIcon:Landroid/widget/ImageView;

.field private mUserTagsTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 561
    sput-boolean v0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabledInited:Z

    .line 562
    sput-boolean v0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 69
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    .line 71
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;)V
    .locals 1
    .param p1, "mSettingManager"    # Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 69
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    .line 71
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 78
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setVisiblePreviewText(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mScrollview:Landroid/view/View;

    return-object v0
.end method

.method public static getActualTagEnable(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 573
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 574
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "ContextualTagEnable"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getContextualTagEnable(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 564
    sget-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabledInited:Z

    if-nez v1, :cond_0

    .line 565
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 566
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "ContextualTagEnable"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabled:Z

    .line 567
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabledInited:Z

    .line 569
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabled:Z

    return v1
.end method

.method public static getContextualTagStatus(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 552
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 553
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "ContextualTagList"

    const/16 v2, 0x1111

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static setContextualTagEnable(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isChecked"    # Z

    .prologue
    .line 578
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 579
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ContextualTagEnable"

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 581
    .local v0, "isCommited":Z
    if-eqz v0, :cond_0

    .line 582
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabledInited:Z

    .line 583
    sput-boolean p1, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->isContextualTagEnabled:Z

    .line 584
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 586
    :cond_0
    return-void
.end method

.method public static setContextualTagStatus(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "checkedItem"    # I

    .prologue
    .line 557
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 558
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ContextualTagList"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 559
    return-void
.end method

.method private setEnabledItems(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 427
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 428
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "checkstatusenabled"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 429
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 430
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    iget v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagStatus(Landroid/content/Context;I)V

    .line 431
    return-void
.end method

.method private setVisiblePreviewText(I)V
    .locals 4
    .param p1, "checkedItem"    # I

    .prologue
    .line 434
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_VERTICAL_TOP_MARGIN:I

    .line 438
    .local v1, "positionY":I
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 439
    .local v0, "point":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 440
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    if-le v2, v3, :cond_0

    .line 441
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_HORIZONTAL_TOP_MARGIN:I

    .line 444
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 445
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v1

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v3

    div-int v1, v2, v3

    .line 452
    :cond_1
    and-int/lit16 v2, p1, 0x110

    if-nez v2, :cond_5

    .line 453
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 458
    :goto_0
    and-int/lit8 v2, p1, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    .line 462
    and-int/lit16 v2, p1, 0x100

    if-nez v2, :cond_6

    .line 463
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 468
    :cond_2
    :goto_1
    and-int/lit16 v2, p1, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_3

    .line 472
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 474
    :cond_3
    and-int/lit16 v2, p1, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_4

    .line 478
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 489
    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->updatePreviewTextVisible(IZ)V

    .line 493
    return-void

    .line 455
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_BIG:I

    sub-int/2addr v1, v2

    goto :goto_0

    .line 465
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_BIG:I

    sub-int/2addr v1, v2

    goto :goto_1
.end method

.method private updatePreviewTextVisible(IZ)V
    .locals 6
    .param p1, "checkedItem"    # I
    .param p2, "disableAll"    # Z

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x80

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 496
    if-eqz p2, :cond_3

    .line 497
    const/4 p1, 0x0

    .line 498
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 510
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-nez v0, :cond_1

    .line 511
    and-int/lit16 v0, p1, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_4

    .line 512
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    :cond_1
    :goto_1
    and-int/lit16 v0, p1, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_5

    .line 521
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 522
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 529
    :goto_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_2

    .line 530
    and-int/lit8 v0, p1, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 531
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 539
    :goto_3
    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 540
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 549
    :cond_2
    :goto_4
    return-void

    .line 503
    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 506
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 515
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_1

    .line 524
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 526
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_2

    .line 534
    :cond_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_3

    .line 543
    :cond_7
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_4
.end method

.method private updateTagEnalble()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 257
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getActualTagEnable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 258
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v2

    .line 259
    .local v2, "sb":Landroid/util/SparseBooleanArray;
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 260
    .local v3, "sp":Landroid/content/SharedPreferences;
    const/4 v0, 0x0

    .line 262
    .local v0, "checkedItem":I
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatusdate"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 263
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatuslocation"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 264
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatuscategory"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 265
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatususertags"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 267
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    if-eqz v4, :cond_4

    .line 268
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getCount()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_4

    .line 269
    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 270
    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    .line 271
    const/4 v0, 0x1

    .line 272
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatususertags"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 268
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 273
    :cond_1
    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 274
    add-int/lit8 v0, v0, 0x10

    .line 275
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatuscategory"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 276
    :cond_2
    if-ne v1, v6, :cond_3

    .line 277
    add-int/lit16 v0, v0, 0x100

    .line 278
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatuslocation"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 280
    :cond_3
    add-int/lit16 v0, v0, 0x1000

    .line 281
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "checkstatusdate"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 287
    .end local v1    # "i":I
    :cond_4
    if-nez v0, :cond_6

    .line 288
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v4, v7}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 293
    .end local v0    # "checkedItem":I
    .end local v2    # "sb":Landroid/util/SparseBooleanArray;
    .end local v3    # "sp":Landroid/content/SharedPreferences;
    :cond_5
    :goto_2
    return-void

    .line 290
    .restart local v0    # "checkedItem":I
    .restart local v2    # "sb":Landroid/util/SparseBooleanArray;
    .restart local v3    # "sp":Landroid/content/SharedPreferences;
    :cond_6
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v4, v6}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagEnable(Landroid/content/Context;Z)V

    goto :goto_2
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "ischecked"    # Z

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p2}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 343
    if-eqz p2, :cond_1

    .line 345
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setVisiblePreviewText(I)V

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->updatePreviewTextVisible(IZ)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    const v9, 0x7f030040

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 85
    .local v6, "rootView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    .line 86
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v9}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 87
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    const-string v10, "accessibility"

    invoke-virtual {v9, v10}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/accessibility/AccessibilityManager;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 89
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    .line 91
    const v9, 0x7f0f00b7

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ListView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    .line 92
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v1, "contextualData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/util/ContextualListClass;>;"
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e0103

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e0234

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v9, :cond_0

    .line 96
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e00a9

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e0235

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {v9, v10, v1}, Lcom/sec/samsung/gallery/util/ContextualListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    .line 100
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 103
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit16 v9, v9, 0x1000

    const/16 v10, 0x1000

    if-ne v9, v10, :cond_1

    .line 104
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v9, :cond_5

    .line 105
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 110
    :cond_1
    :goto_0
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit16 v9, v9, 0x100

    const/16 v10, 0x100

    if-ne v9, v10, :cond_2

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 111
    :cond_2
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit8 v9, v9, 0x10

    const/16 v10, 0x10

    if-ne v9, v10, :cond_3

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x2

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 112
    :cond_3
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit8 v9, v9, 0x1

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x3

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 113
    :cond_4
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 114
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 115
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_e

    .line 118
    const/4 v5, 0x0

    .line 119
    .local v5, "listviewTotalheight":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getCount()I

    move-result v9

    if-ge v4, v9, :cond_6

    .line 120
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, v4, v10, v11}, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 121
    .local v0, "childView":Landroid/view/View;
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Landroid/view/View;->measure(II)V

    .line 123
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v10

    add-int/2addr v9, v10

    add-int/2addr v5, v9

    .line 119
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 107
    .end local v0    # "childView":Landroid/view/View;
    .end local v4    # "i":I
    .end local v5    # "listviewTotalheight":I
    :cond_5
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0

    .line 125
    .restart local v4    # "i":I
    .restart local v5    # "listviewTotalheight":I
    :cond_6
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iput v5, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 139
    .end local v4    # "i":I
    .end local v5    # "listviewTotalheight":I
    :cond_7
    :goto_2
    const v9, 0x7f0f00a4

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationTextView:Landroid/widget/TextView;

    .line 140
    const v9, 0x7f0f00b2

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationIcon:Landroid/widget/ImageView;

    .line 142
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mLocationTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 143
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v9, :cond_8

    .line 144
    const v9, 0x7f0f00a6

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsTextView:Landroid/widget/TextView;

    .line 145
    const v9, 0x7f0f00b6

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsIcon:Landroid/widget/ImageView;

    .line 146
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mUserTagsTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 148
    :cond_8
    const v9, 0x7f0f00a3

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    .line 149
    const v9, 0x7f0f00b0

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateIcon:Landroid/widget/ImageView;

    .line 151
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    if-eqz v9, :cond_9

    .line 152
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    const v10, -0xf0f10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 156
    :cond_9
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mDateTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 158
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v9, :cond_a

    .line 159
    const v9, 0x7f0f00a5

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryTextView:Landroid/widget/TextView;

    .line 160
    const v9, 0x7f0f00b4

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryIcon:Landroid/widget/ImageView;

    .line 162
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCategoryTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 171
    :cond_a
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 172
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x400

    const/16 v11, 0x400

    invoke-virtual {v9, v10, v11}, Landroid/view/Window;->setFlags(II)V

    .line 175
    :cond_b
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 176
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setVisiblePreviewText(I)V

    .line 180
    :goto_3
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getActualTagEnable(Landroid/content/Context;)Z

    move-result v9

    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setEnabledItems(Z)V

    .line 182
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_c

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v9, :cond_c

    .line 183
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 184
    .local v2, "deco":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v8

    .line 185
    .local v8, "vto2":Landroid/view/ViewTreeObserver;
    new-instance v9, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment$1;

    invoke-direct {v9, p0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment$1;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;)V

    invoke-virtual {v8, v9}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 193
    .end local v2    # "deco":Landroid/view/View;
    .end local v8    # "vto2":Landroid/view/ViewTreeObserver;
    :cond_c
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-nez v9, :cond_d

    .line 194
    const v9, 0x7f0f00ae

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mScrollview:Landroid/view/View;

    .line 195
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mScrollview:Landroid/view/View;

    if-eqz v9, :cond_10

    .line 196
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mScrollview:Landroid/view/View;

    new-instance v10, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment$2;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment$2;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;)V

    invoke-virtual {v9, v10}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 206
    :cond_d
    :goto_4
    return-object v6

    .line 126
    :cond_e
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-nez v9, :cond_7

    .line 127
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 128
    .local v3, "display":Landroid/view/Display;
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 129
    .local v7, "size":Landroid/graphics/Point;
    invoke-virtual {v3, v7}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 130
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Point;->x:I

    div-int/lit8 v10, v10, 0x2

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto/16 :goto_2

    .line 178
    .end local v3    # "display":Landroid/view/Display;
    .end local v7    # "size":Landroid/graphics/Point;
    :cond_f
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setVisiblePreviewText(I)V

    goto :goto_3

    .line 202
    :cond_10
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->scrollTo(II)V

    goto :goto_4
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->setContextualTagSettingsFagmentDisplayed(Z)V

    .line 250
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->updateTagEnalble()V

    .line 253
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 254
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 365
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    .line 366
    .local v3, "sb":Landroid/util/SparseBooleanArray;
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 367
    .local v5, "sp":Landroid/content/SharedPreferences;
    move-object v4, p2

    .line 369
    .local v4, "selectView":Landroid/view/View;
    const/4 v0, 0x0

    .line 371
    .local v0, "checkedItem":I
    if-eqz v4, :cond_7

    .line 372
    const v6, 0x7f0f00a1

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 376
    .local v2, "itemCheckBox":Landroid/widget/CheckBox;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatusdate"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 377
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuslocation"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 378
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuscategory"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 379
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatususertags"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 381
    if-eqz v2, :cond_0

    .line 382
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 383
    invoke-virtual {v2, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 388
    :goto_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 389
    invoke-virtual {v2, v8}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 392
    :cond_0
    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->size()I

    move-result v6

    if-eqz v6, :cond_6

    .line 393
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getCount()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_6

    .line 394
    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 395
    const/4 v6, 0x3

    if-ne v1, v6, :cond_3

    .line 396
    const/4 v0, 0x1

    .line 397
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatususertags"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 393
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 385
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v2, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 398
    .restart local v1    # "i":I
    :cond_3
    const/4 v6, 0x2

    if-ne v1, v6, :cond_4

    .line 399
    add-int/lit8 v0, v0, 0x10

    .line 400
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuscategory"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_2

    .line 401
    :cond_4
    if-ne v1, v8, :cond_5

    .line 402
    add-int/lit16 v0, v0, 0x100

    .line 403
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuslocation"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_2

    .line 405
    :cond_5
    add-int/lit16 v0, v0, 0x1000

    .line 406
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatusdate"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_2

    .line 412
    .end local v1    # "i":I
    :cond_6
    iput v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    .line 413
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setVisiblePreviewText(I)V

    .line 414
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    iget v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagStatus(Landroid/content/Context;I)V

    .line 416
    if-nez v0, :cond_8

    .line 417
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 424
    .end local v2    # "itemCheckBox":Landroid/widget/CheckBox;
    :cond_7
    :goto_3
    return-void

    .line 422
    .restart local v2    # "itemCheckBox":Landroid/widget/CheckBox;
    :cond_8
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagEnable(Landroid/content/Context;Z)V

    goto :goto_3
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x42

    if-eq p2, v0, :cond_0

    const/16 v0, 0x17

    if-ne p2, v0, :cond_1

    .line 600
    :cond_0
    const/4 v0, 0x1

    .line 602
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 297
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 316
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 301
    :sswitch_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v2, :cond_1

    .line 302
    const/16 v0, 0x111

    .line 306
    .local v0, "dateTagDefaultCheck":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v2

    and-int/2addr v2, v0

    if-nez v2, :cond_0

    .line 307
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 309
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 304
    .end local v0    # "dateTagDefaultCheck":I
    :cond_1
    const/16 v0, 0x1111

    .restart local v0    # "dateTagDefaultCheck":I
    goto :goto_1

    .line 312
    .end local v0    # "dateTagDefaultCheck":I
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    iget v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setContextualTagStatus(Landroid/content/Context;I)V

    .line 313
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 297
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0259 -> :sswitch_0
        0x7f0f025a -> :sswitch_1
    .end sparse-switch
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 211
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 212
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_3

    .line 213
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    .line 214
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 216
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->setVisiblePreviewText(I)V

    .line 222
    :goto_0
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    .line 223
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v0, :cond_5

    .line 224
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 229
    :cond_0
    :goto_1
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 230
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 231
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mCheckedItem:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 233
    :cond_3
    return-void

    .line 219
    :cond_4
    invoke-direct {p0, v3, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->updatePreviewTextVisible(IZ)V

    goto :goto_0

    .line 226
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSettingFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 593
    const/4 v0, 0x0

    return v0
.end method
