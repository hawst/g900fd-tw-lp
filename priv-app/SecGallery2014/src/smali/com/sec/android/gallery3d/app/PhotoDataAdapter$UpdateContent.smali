.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateContent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mIndex:I

.field mUpdateInfo:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;I)V
    .locals 0
    .param p2, "updateInfo"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;
    .param p3, "index"    # I

    .prologue
    .line 1469
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1470
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mUpdateInfo:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

    .line 1471
    iput p3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mIndex:I

    .line 1472
    return-void
.end method

.method private updateCurrentItem()V
    .locals 2

    .prologue
    .line 1556
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1567
    :cond_0
    :goto_0
    return-void

    .line 1557
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1558
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1402(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1559
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 1560
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;

    if-nez v0, :cond_0

    .line 1561
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->startSlideInAnimation(I)V

    goto :goto_0

    .line 1563
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 1564
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/UriMediaSet;

    if-nez v0, :cond_0

    .line 1565
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->startSlideInAnimation(I)V

    goto :goto_0
.end method

.method private updateLastItem()V
    .locals 2

    .prologue
    .line 1570
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;

    if-eqz v0, :cond_0

    .line 1571
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1572
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1502(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    .line 1573
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1402(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1574
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 1575
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->startSlideInAnimation(I)V

    .line 1578
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1465
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1476
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mUpdateInfo:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

    .line 1477
    .local v4, "info":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-wide v8, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->version:J

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J
    invoke-static {v6, v8, v9}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2002(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;J)J

    .line 1479
    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->size:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    if-eq v6, v8, :cond_1

    .line 1480
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget v8, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->size:I

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1802(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1481
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v6

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    if-le v6, v8, :cond_0

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I
    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1202(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1482
    :cond_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v6

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    if-le v6, v8, :cond_1

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I
    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2102(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1486
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v6

    if-lez v6, :cond_2

    .line 1487
    iget v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mIndex:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    if-lt v6, v8, :cond_2

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mIndex:I

    .line 1490
    :cond_2
    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->size:I

    if-lez v6, :cond_3

    .line 1491
    iget v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mIndex:I

    iget v8, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->size:I

    if-lt v6, v8, :cond_3

    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->size:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mIndex:I

    .line 1494
    :cond_3
    iget v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->mIndex:I

    iput v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    .line 1497
    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    const/4 v8, -0x1

    if-ne v6, v8, :cond_8

    .line 1500
    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->curIndex:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    if-ne v6, v8, :cond_4

    .line 1501
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1502(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    .line 1502
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->updateCurrentItem()V

    .line 1511
    :cond_4
    :goto_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFirstLoading:Z
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1512
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget v8, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1402(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1513
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFirstLoading:Z
    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1702(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Z)Z

    .line 1516
    :cond_5
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 1517
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    move-result-object v6

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    invoke-interface {v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;->onFilmIndexChanged(I)V

    .line 1521
    :cond_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 1523
    iget-object v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    if-eqz v6, :cond_9

    .line 1524
    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1525
    .local v5, "start":I
    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    iget-object v8, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/2addr v6, v8

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1526
    .local v2, "end":I
    rem-int/lit8 v1, v5, 0x28

    .line 1527
    .local v1, "dataIndex":I
    move v3, v5

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_9

    .line 1528
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    iget-object v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    iget v9, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    sub-int v9, v3, v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v6, v8, v1

    .line 1529
    add-int/lit8 v1, v1, 0x1

    const/16 v6, 0x28

    if-ne v1, v6, :cond_7

    const/4 v1, 0x0

    .line 1527
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1505
    .end local v1    # "dataIndex":I
    .end local v2    # "end":I
    .end local v3    # "i":I
    .end local v5    # "start":I
    :cond_8
    iget v6, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->curIndex:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    if-ne v6, v8, :cond_4

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v6

    if-eqz v6, :cond_4

    .line 1506
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget v8, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1402(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1507
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->updateLastItem()V

    goto/16 :goto_0

    .line 1534
    :cond_9
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v8

    rem-int/lit8 v8, v8, 0x28

    aget-object v0, v6, v8

    .line 1535
    .local v0, "current":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-nez v0, :cond_a

    move-object v6, v7

    :goto_2
    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v8, v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1502(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    .line 1537
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 1538
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 1539
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    .line 1545
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 1548
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2600(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/samsung/gallery/decoder/CacheInterface;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->createAllOnCurrent()V

    .line 1551
    return-object v7

    .line 1535
    :cond_a
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    goto :goto_2
.end method
