.class public Lcom/sec/android/gallery3d/data/LocalAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "LocalAlbum.java"


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field public static final DCIM_PATH:Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field private static final LOCALIMAGE_PROJECTION:Ljava/lang/String;

.field public static final NCR_PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "LocalAlbum"

.field private static final mGroupProjection:[Ljava/lang/String;

.field private static final mGroupProjectionExP:Ljava/lang/String;

.field private static final mGroupProjectionExR:Ljava/lang/String;

.field private static final mGroupWhere:Ljava/lang/String; = "group_id = 0 and bucket_id = ?"

.field private static final mGroupWhereExP:Ljava/lang/String; = "(group_id = 0"

.field private static final mGroupWhereExR:Ljava/lang/String; = ") and bucket_id = ?"


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mBaseUri:Landroid/net/Uri;

.field private final mBucketId:I

.field private mCachedBurstImageCount:I

.field private mCachedCount:I

.field private mCameraAlbum:Z

.field private mEventAlbumTimeInfo:Ljava/lang/String;

.field private mGroupCachedCount:I

.field private mGroupCntMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsImage:Z

.field private final mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mName:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mOrderClause:Ljava/lang/String;

.field private mPathOnFileSystem:Ljava/lang/String;

.field private final mProjection:[Ljava/lang/String;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mSlideshowSupport:Z

.field private mSupportShare:Z

.field private final mWhereClause:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "count(*)"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    .line 55
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/DCIM/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->DCIM_PATH:Ljava/lang/String;

    .line 56
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/NCR/Image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->NCR_PATH:Ljava/lang/String;

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const-string v2, ", "

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbum;->mergeStrings([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->LOCALIMAGE_PROJECTION:Ljava/lang/String;

    .line 97
    new-array v0, v4, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbum;->LOCALIMAGE_PROJECTION:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "from (select * from images where group_id != 0 and bucket_id = ? order by datetaken asc, _id desc) group by group_id union select "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbum;->LOCALIMAGE_PROJECTION:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupProjection:[Ljava/lang/String;

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbum;->LOCALIMAGE_PROJECTION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "from (select * from images where group_id != 0 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupProjectionExP:Ljava/lang/String;

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " and bucket_id = ? order by datetaken asc, _id desc) group by group_id union select "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbum;->LOCALIMAGE_PROJECTION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupProjectionExR:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "bucketId"    # I
    .param p4, "isImage"    # Z

    .prologue
    .line 136
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/sec/android/gallery3d/data/BucketHelper;->getBucketName(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/data/LocalAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;)V
    .locals 5
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "bucketId"    # I
    .param p4, "isImage"    # Z
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 108
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 76
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedCount:I

    .line 78
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSupportShare:Z

    .line 80
    iput-object v4, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    .line 81
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedBurstImageCount:I

    .line 82
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSlideshowSupport:Z

    .line 83
    iput-object v4, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 84
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCameraAlbum:Z

    .line 700
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    .line 109
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 110
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    .line 111
    iput p3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    .line 112
    iput-object p5, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mName:Ljava/lang/String;

    .line 113
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    .line 115
    if-eqz p4, :cond_0

    .line 116
    const-string v0, "bucket_id = ?"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    .line 117
    const-string v0, "datetaken DESC, _id DESC"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 119
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    .line 120
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mProjection:[Ljava/lang/String;

    .line 121
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 131
    :goto_0
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 132
    return-void

    .line 123
    :cond_0
    const-string v0, "bucket_id = ?"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    .line 124
    const-string v0, "datetaken DESC, _id DESC"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 126
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    .line 127
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mProjection:[Ljava/lang/String;

    .line 128
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "bucketId"    # I
    .param p4, "isImage"    # Z
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "filePath"    # Ljava/lang/String;

    .prologue
    .line 147
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/gallery3d/data/LocalAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;)V

    .line 148
    iput-object p6, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public static getItemCursor(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 6
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "id"    # I

    .prologue
    .line 510
    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getLocalizedName(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "bucketId"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 656
    sget v0, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-ne p1, v0, :cond_1

    .line 657
    const v0, 0x7f0e025c

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 667
    .end local p2    # "name":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p2

    .line 658
    .restart local p2    # "name":Ljava/lang/String;
    :cond_1
    sget v0, Lcom/sec/android/gallery3d/util/MediaSetUtils;->DOWNLOAD_BUCKET_ID:I

    if-ne p1, v0, :cond_2

    .line 659
    const v0, 0x7f0e010d

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 660
    :cond_2
    sget v0, Lcom/sec/android/gallery3d/util/MediaSetUtils;->IMPORTED_BUCKET_ID:I

    if-ne p1, v0, :cond_3

    .line 661
    const v0, 0x7f0e009b

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 662
    :cond_3
    sget v0, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SNAPSHOT_BUCKET_ID:I

    if-ne p1, v0, :cond_4

    .line 663
    const v0, 0x7f0e0364

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 664
    :cond_4
    sget v0, Lcom/sec/android/gallery3d/util/MediaSetUtils;->EDITED_ONLINE_PHOTOS_BUCKET_ID:I

    if-ne p1, v0, :cond_0

    .line 665
    const v0, 0x7f0e0365

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public static getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;ZLjava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 21
    .param p0, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)[",
            "Lcom/sec/android/gallery3d/data/MediaItem;"
        }
    .end annotation

    .prologue
    .line 358
    .local p2, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v19, v0

    .line 359
    .local v19, "result":[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 454
    :goto_0
    return-object v19

    .line 360
    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 361
    .local v15, "idLow":I
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 367
    .local v14, "idHigh":I
    if-eqz p1, :cond_1

    .line 368
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 369
    .local v3, "baseUri":Landroid/net/Uri;
    sget-object v4, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    .line 370
    .local v4, "projection":[Ljava/lang/String;
    sget-object v17, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 377
    .local v17, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 378
    .local v2, "resolver":Landroid/content/ContentResolver;
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v10

    .line 415
    .local v10, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v9, 0x0

    .line 417
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v5, "_id BETWEEN ? AND ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v6, v7

    const/4 v7, 0x1

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v6, v7

    const-string v7, "_id"

    invoke-static/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 422
    if-nez v9, :cond_2

    .line 423
    const-string v5, "LocalAlbum"

    const-string v6, "query fail"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 372
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "baseUri":Landroid/net/Uri;
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v17    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 373
    .restart local v3    # "baseUri":Landroid/net/Uri;
    sget-object v4, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    .line 374
    .restart local v4    # "projection":[Ljava/lang/String;
    sget-object v17, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .restart local v17    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    goto :goto_1

    .line 426
    .restart local v2    # "resolver":Landroid/content/ContentResolver;
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_2
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 427
    .local v18, "n":I
    const/4 v12, 0x0

    .line 429
    .local v12, "i":I
    :cond_3
    :goto_2
    move/from16 v0, v18

    if-ge v12, v0, :cond_6

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 430
    const/4 v5, 0x0

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 433
    .local v13, "id":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-gt v5, v13, :cond_3

    .line 437
    :cond_4
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-ge v5, v13, :cond_5

    .line 438
    add-int/lit8 v12, v12, 0x1

    move/from16 v0, v18

    if-lt v12, v0, :cond_4

    .line 454
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 443
    :cond_5
    :try_start_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 444
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v8, v9, v10, v0, v1}, Lcom/sec/android/gallery3d/data/LocalAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v16

    .line 446
    .local v16, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    aput-object v16, v19, v12
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 447
    add-int/lit8 v12, v12, 0x1

    .line 448
    goto :goto_2

    .line 454
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v13    # "id":I
    .end local v16    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 450
    .end local v12    # "i":I
    .end local v18    # "n":I
    :catch_0
    move-exception v11

    .line 451
    .local v11, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 454
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v5
.end method

.method public static getRelativePath(I)Ljava/lang/String;
    .locals 6
    .param p0, "bucketId"    # I

    .prologue
    .line 673
    const-string v2, "/"

    .line 674
    .local v2, "relativePath":Ljava/lang/String;
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-ne p0, v3, :cond_0

    .line 675
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "DCIM/Camera"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 696
    :goto_0
    return-object v2

    .line 676
    :cond_0
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->DOWNLOAD_BUCKET_ID:I

    if-ne p0, v3, :cond_1

    .line 677
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "download"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 678
    :cond_1
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->IMPORTED_BUCKET_ID:I

    if-ne p0, v3, :cond_2

    .line 679
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Imported"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 680
    :cond_2
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SNAPSHOT_BUCKET_ID:I

    if-ne p0, v3, :cond_3

    .line 681
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Pictures/Screenshots"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 682
    :cond_3
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->EDITED_ONLINE_PHOTOS_BUCKET_ID:I

    if-ne p0, v3, :cond_4

    .line 683
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "EditedOnlinePhotos"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 687
    :cond_4
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 688
    .local v0, "extStorage":Ljava/io/File;
    invoke-static {v0, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->searchDirForPath(Ljava/io/File;I)Ljava/lang/String;

    move-result-object v1

    .line 689
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_5

    .line 690
    const-string v3, "LocalAlbum"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Relative path for bucket id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not found."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 693
    :cond_5
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 4
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "isImage"    # Z

    .prologue
    .line 332
    sget-object v3, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 333
    :try_start_0
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 334
    .local v1, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    if-nez v1, :cond_2

    .line 335
    if-eqz p4, :cond_1

    .line 336
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-direct {v1, p0, p3, p1}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 341
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_0
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->isCameraPath(Ljava/lang/String;)Z

    move-result v0

    .line 342
    .local v0, "isCamera":Z
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCIMFolderMerge:Z

    if-eqz v2, :cond_0

    .line 343
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->isDcimPath(Ljava/lang/String;)Z

    move-result v0

    .line 345
    :cond_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->setCameraItem(Z)V

    .line 350
    .end local v0    # "isCamera":Z
    :goto_1
    monitor-exit v3

    return-object v1

    .line 338
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalVideo;

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-direct {v1, p0, p3, p1}, Lcom/sec/android/gallery3d/data/LocalVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    goto :goto_0

    .line 348
    :cond_2
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateContent(Landroid/database/Cursor;)V

    goto :goto_1

    .line 351
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static mergeStrings([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "strings"    # [Ljava/lang/String;
    .param p1, "seperator"    # Ljava/lang/String;

    .prologue
    .line 1012
    if-nez p0, :cond_0

    .line 1013
    const-string v2, ""

    .line 1022
    :goto_0
    return-object v2

    .line 1015
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1016
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_3

    .line 1017
    if-lez v0, :cond_1

    .line 1018
    if-nez p1, :cond_2

    const-string v2, ""

    :goto_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1019
    :cond_1
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1016
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v2, p1

    .line 1018
    goto :goto_2

    .line 1022
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public delete()V
    .locals 7

    .prologue
    .line 634
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 638
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v6, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->delete(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 640
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 641
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 642
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_0

    .line 643
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 647
    :cond_0
    return-void
.end method

.method public delete(Z)V
    .locals 7
    .param p1, "includeDir"    # Z

    .prologue
    .line 792
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 793
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v6, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 795
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 796
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 797
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    .line 798
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 801
    :cond_0
    return-void
.end method

.method public fakeChange()V
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->fakeChange()V

    .line 1009
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 787
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    return v0
.end method

.method public getBurstShotItems(J)Ljava/util/ArrayList;
    .locals 13
    .param p1, "groupId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x1

    .line 845
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-nez v0, :cond_1

    .line 883
    :cond_0
    :goto_0
    return-object v11

    .line 847
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 851
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 852
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v7, 0x0

    .line 854
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 855
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v1, "DESC"

    const-string v2, "ASC"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 859
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mProjection:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and group_id=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v12, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 861
    if-nez v7, :cond_3

    .line 862
    const-string v0, "LocalAlbum"

    const-string v1, "getBurstShotItems - query fail"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 881
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 857
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v1, "ASC"

    const-string v2, "DESC"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 881
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 865
    :cond_3
    :goto_2
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 866
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 867
    .local v9, "id":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 868
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    invoke-static {v6, v7, v8, v0, v1}, Lcom/sec/android/gallery3d/data/LocalAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    .line 870
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v10, :cond_3

    .line 872
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x4

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 873
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSupportShare:Z

    .line 875
    :cond_4
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 876
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSlideshowSupport:Z

    .line 878
    :cond_5
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 881
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v9    # "id":I
    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "bucketId"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "bucketId"

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getEventAlbumTimeInfo()Ljava/lang/String;
    .locals 3

    .prologue
    .line 931
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 932
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCount()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 934
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 806
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 16
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v10

    .line 173
    .local v10, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "limit"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 175
    .local v3, "uri":Landroid/net/Uri;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 176
    .local v14, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 198
    const/4 v9, 0x0

    .line 200
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v4, "DESC"

    const-string v5, "ASC"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 206
    :goto_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseIdSortByASC:Z

    if-eqz v2, :cond_0

    .line 207
    const-string v2, "datetaken DESC, _id ASC"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 211
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 216
    if-nez v9, :cond_2

    .line 217
    const-string v2, "LocalAlbum"

    const-string v4, "query fail: "

    invoke-static {v2, v4}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 246
    :goto_1
    return-object v14

    .line 203
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v4, "ASC"

    const-string v5, "DESC"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v11

    .line 238
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 243
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 221
    .end local v11    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_2
    :goto_2
    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 222
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 223
    .local v12, "id":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v2, v12}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 224
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    invoke-static {v8, v9, v10, v2, v4}, Lcom/sec/android/gallery3d/data/LocalAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v13

    .line 226
    .local v13, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v13, :cond_2

    .line 228
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSupportShare:Z

    if-eqz v2, :cond_3

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_3

    .line 229
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSupportShare:Z

    .line 231
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSlideshowSupport:Z

    if-nez v2, :cond_4

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 232
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSlideshowSupport:Z

    .line 234
    :cond_4
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 239
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v12    # "id":I
    .end local v13    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_1
    move-exception v11

    .line 241
    .local v11, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_4
    invoke-virtual {v11}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 243
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v11    # "e":Landroid/database/CursorWindowAllocationException;
    :cond_5
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public getMediaItemByGroupId(IIJ)Ljava/util/ArrayList;
    .locals 15
    .param p1, "start"    # I
    .param p2, "count"    # I
    .param p3, "groupId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v9

    .line 461
    .local v9, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "limit"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 463
    .local v2, "uri":Landroid/net/Uri;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 464
    .local v13, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 466
    const/4 v8, 0x0

    .line 468
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 469
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v3, "DESC"

    const-string v5, "ASC"

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 473
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-eqz v1, :cond_0

    .line 474
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bucket_id = ? and group_id = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 475
    .local v4, "where":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mProjection:[Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget v14, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v6

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 482
    .end local v4    # "where":Ljava/lang/String;
    :cond_0
    if-nez v8, :cond_2

    .line 500
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 502
    :goto_1
    return-object v13

    .line 471
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v3, "ASC"

    const-string v5, "DESC"

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 495
    :catch_0
    move-exception v10

    .line 496
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 500
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 486
    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_2
    :goto_2
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 487
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 488
    .local v11, "id":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    .line 489
    .local v7, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    invoke-static {v7, v8, v9, v1, v3}, Lcom/sec/android/gallery3d/data/LocalAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v12

    .line 491
    .local v12, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v12, :cond_2

    .line 493
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 497
    .end local v7    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v11    # "id":I
    .end local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_1
    move-exception v10

    .line 498
    .local v10, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_4
    invoke-virtual {v10}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 500
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v10    # "e":Landroid/database/CursorWindowAllocationException;
    :cond_3
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method public getMediaItemCount()I
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 525
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 540
    const/4 v6, 0x0

    .line 542
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v8, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 546
    if-nez v6, :cond_0

    .line 547
    const-string v0, "LocalAlbum"

    const-string v1, "query fail"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 553
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v7

    .line 557
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 550
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 551
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 553
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 557
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedCount:I

    goto :goto_0

    .line 553
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getMediaItemCountForBurstshot(Ljava/util/ArrayList;Z)I
    .locals 1
    .param p2, "showAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 517
    .local p1, "showGroupIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->isCameraRoll()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemGroupCount(Ljava/util/ArrayList;Z)I

    move-result v0

    .line 520
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCount()I

    move-result v0

    goto :goto_0
.end method

.method public getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 24
    .param p1, "start"    # I
    .param p2, "count"    # I
    .param p4, "showAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    .local p3, "showGroupIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    if-nez p4, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->isCameraRoll()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p3, :cond_1

    .line 252
    :cond_0
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v21

    .line 326
    :goto_0
    return-object v21

    .line 255
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v14

    .line 256
    .local v14, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v6, "limit"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 258
    .local v3, "uri":Landroid/net/Uri;
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .local v21, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 261
    const/4 v13, 0x0

    .line 263
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v2

    const/4 v6, 0x1

    if-ne v2, v6, :cond_4

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v6, "DESC"

    const-string v7, "ASC"

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 269
    :goto_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseIdSortByASC:Z

    if-eqz v2, :cond_2

    .line 270
    const-string v2, "datetaken DESC, _id ASC"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    .line 274
    :cond_2
    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 275
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .line 276
    .local v20, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    const-string v22, ""

    .line 277
    .local v22, "ungroupIdAndSet":Ljava/lang/String;
    const-string v23, ""

    .line 279
    .local v23, "ungroupIdOrSet":Ljava/lang/String;
    :cond_3
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 280
    .local v16, "gid":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " and group_id != "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 281
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " or group_id = "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 282
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 283
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupProjectionExP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupProjectionExR:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    .line 286
    .local v4, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(group_id = 0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ") and bucket_id = ?"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 287
    .local v5, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 301
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    .end local v16    # "gid":J
    .end local v20    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .end local v22    # "ungroupIdAndSet":Ljava/lang/String;
    .end local v23    # "ungroupIdOrSet":Ljava/lang/String;
    :goto_2
    if-nez v13, :cond_6

    .line 302
    const-string v2, "LocalAlbum"

    const-string v6, "query fail: "

    invoke-static {v2, v6}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 266
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    const-string v6, "ASC"

    const-string v7, "DESC"

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 321
    :catch_0
    move-exception v15

    .line 322
    .local v15, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 324
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 294
    .end local v15    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupProjection:[Ljava/lang/String;

    const-string v9, "group_id = 0 and bucket_id = ?"

    const/4 v2, 0x2

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v10, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v10, v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mOrderClause:Ljava/lang/String;

    move-object v7, v3

    invoke-static/range {v6 .. v11}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    goto :goto_2

    .line 306
    :cond_6
    :goto_3
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 307
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 308
    .local v18, "id":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v12

    .line 309
    .local v12, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    invoke-static {v12, v13, v14, v2, v6}, Lcom/sec/android/gallery3d/data/LocalAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v19

    .line 311
    .local v19, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v19, :cond_6

    .line 313
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v6

    const-wide/16 v8, 0x4

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-nez v2, :cond_7

    .line 314
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSupportShare:Z

    .line 316
    :cond_7
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 317
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSlideshowSupport:Z

    .line 319
    :cond_8
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 324
    .end local v12    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v18    # "id":I
    .end local v19    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    :cond_9
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0
.end method

.method public getMediaItemGroupCount(Ljava/util/ArrayList;Z)I
    .locals 20
    .param p2, "showAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 703
    .local p1, "showGroupIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-nez v2, :cond_0

    .line 704
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCount()I

    move-result v2

    .line 777
    :goto_0
    return v2

    .line 706
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-nez v2, :cond_1

    .line 707
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCount()I

    move-result v2

    goto :goto_0

    .line 710
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    if-nez v2, :cond_2

    .line 711
    const-string v2, "LocalAlbum"

    const-string v3, "mGroupCntMap is null"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCount()I

    move-result v2

    goto :goto_0

    .line 715
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_8

    .line 716
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 717
    const/4 v9, 0x0

    .line 718
    .local v9, "cursor1":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 720
    .local v10, "cursor2":Landroid/database/Cursor;
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I

    .line 721
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and group_id=0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 725
    if-eqz v9, :cond_3

    .line 726
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I

    .line 729
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "group_id"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and group_id<>0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v6, v7

    const-string v7, "group_id DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 733
    const-wide/16 v16, 0x0

    .line 734
    .local v16, "id":J
    const/4 v8, 0x0

    .line 735
    .local v8, "count":I
    if-eqz v10, :cond_7

    .line 736
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 737
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v2, v16, v2

    if-eqz v2, :cond_4

    if-lez v8, :cond_4

    .line 738
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 739
    const/4 v8, 0x0

    .line 741
    :cond_4
    add-int/lit8 v8, v8, 0x1

    .line 742
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    goto :goto_1

    .line 744
    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-eqz v2, :cond_6

    .line 745
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 747
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 751
    :cond_7
    :try_start_2
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 752
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 755
    .end local v8    # "count":I
    .end local v9    # "cursor1":Landroid/database/Cursor;
    .end local v10    # "cursor2":Landroid/database/Cursor;
    .end local v16    # "id":J
    :cond_8
    const/4 v12, 0x0

    .line 756
    .local v12, "groupCnt":I
    if-eqz p2, :cond_9

    .line 758
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v18

    .line 759
    .local v18, "values":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 760
    .local v14, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 761
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    add-int/2addr v12, v2

    goto :goto_2

    .line 751
    .end local v12    # "groupCnt":I
    .end local v14    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v18    # "values":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .restart local v9    # "cursor1":Landroid/database/Cursor;
    .restart local v10    # "cursor2":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 752
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 772
    .end local v9    # "cursor1":Landroid/database/Cursor;
    .end local v10    # "cursor2":Landroid/database/Cursor;
    :catch_0
    move-exception v11

    .line 773
    .local v11, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v11}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 774
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCount()I

    move-result v2

    goto/16 :goto_0

    .line 764
    .end local v11    # "e":Ljava/lang/NullPointerException;
    .restart local v12    # "groupCnt":I
    :cond_9
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v13, v2, :cond_b

    .line 765
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    .line 766
    .local v15, "showGroupId":Ljava/lang/Long;
    if-eqz v15, :cond_a

    .line 767
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCntMap:Ljava/util/HashMap;

    invoke-virtual {v2, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    add-int/2addr v12, v2

    .line 764
    :cond_a
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 771
    .end local v13    # "i":I
    .end local v15    # "showGroupId":Ljava/lang/Long;
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    add-int/2addr v2, v12

    goto/16 :goto_0

    .line 775
    .end local v12    # "groupCnt":I
    :catch_1
    move-exception v11

    .line 776
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 777
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPathOnFileSystem()Ljava/lang/String;
    .locals 2

    .prologue
    .line 812
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/BucketHelper;->getPathOnFileSystem(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    .line 815
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 10

    .prologue
    const-wide v8, -0x4000000000001L

    const-wide v6, -0x1000000000000001L    # -3.1050361846014175E231

    const-wide v4, -0x4000000000000001L    # -1.9999999999999998

    .line 595
    const-wide v0, 0x5084400003a00405L    # 7.503327462639702E79

    .line 599
    .local v0, "supported":J
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSupportShare:Z

    if-nez v2, :cond_0

    .line 600
    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 601
    const-wide/32 v2, -0x800001

    and-long/2addr v0, v2

    .line 602
    and-long/2addr v0, v6

    .line 604
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->isHideOrRenameBlockedAlbum()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 605
    and-long/2addr v0, v4

    .line 606
    and-long/2addr v0, v8

    .line 612
    :cond_1
    :goto_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mSlideshowSupport:Z

    if-eqz v2, :cond_2

    .line 613
    const-wide v2, -0x400000000001L

    and-long/2addr v0, v2

    .line 616
    :cond_2
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v2, :cond_3

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 618
    const-wide/32 v2, 0x20000000

    or-long/2addr v0, v2

    .line 619
    const-wide/32 v2, -0x40000001

    and-long/2addr v0, v2

    .line 625
    :cond_3
    :goto_1
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-nez v2, :cond_4

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedCount:I

    if-lez v2, :cond_4

    .line 626
    and-long/2addr v0, v6

    .line 628
    :cond_4
    return-wide v0

    .line 607
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 608
    and-long/2addr v0, v8

    goto :goto_0

    .line 609
    :cond_6
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBox(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 610
    and-long/2addr v0, v4

    goto :goto_0

    .line 621
    :cond_7
    const-wide/32 v2, 0x40000000

    or-long/2addr v0, v2

    .line 622
    const-wide/32 v2, -0x20000001

    and-long/2addr v0, v2

    goto :goto_1
.end method

.method public getTotalMediaItems(I)Landroid/database/Cursor;
    .locals 1
    .param p1, "filterType"    # I

    .prologue
    .line 939
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getTotalMediaItems(IZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getTotalMediaItems(IZ)Landroid/database/Cursor;
    .locals 9
    .param p1, "filterType"    # I
    .param p2, "useOrder"    # Z

    .prologue
    const/4 v6, 0x0

    .line 944
    const/4 v5, 0x0

    .line 945
    .local v5, "orderClause":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 996
    :cond_0
    :goto_0
    :pswitch_0
    return-object v6

    .line 947
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-eqz v0, :cond_0

    .line 950
    if-eqz p2, :cond_1

    .line 951
    const-string v5, "datetaken DESC, _id DESC"

    .line 967
    :cond_1
    :goto_1
    const/4 v6, 0x0

    .line 969
    .local v6, "cursor":Landroid/database/Cursor;
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_3

    .line 970
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "latitude"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "datetaken"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "mime_type"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v4, 0x6

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    :goto_2
    aput-object v3, v2, v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data not like\'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CLOUD_CACHE_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' and bucket_id = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 956
    .end local v6    # "cursor":Landroid/database/Cursor;
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-nez v0, :cond_0

    .line 959
    if-eqz p2, :cond_1

    .line 960
    const-string v5, "datetaken DESC, _id DESC"

    goto :goto_1

    .line 970
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :try_start_1
    const-string v3, "sef_file_type"

    goto :goto_2

    .line 981
    :cond_3
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "latitude"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "datetaken"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "mime_type"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "_data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data not like\'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CLOUD_CACHE_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' and bucket_id = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    goto/16 :goto_0

    .line 993
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 945
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public haveBurstShotImages()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 819
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mIsImage:Z

    if-nez v0, :cond_1

    .line 840
    :cond_0
    :goto_0
    return v8

    .line 821
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v0, :cond_0

    .line 824
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedBurstImageCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 825
    const/4 v6, 0x0

    .line 827
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBaseUri:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mWhereClause:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and group_id>0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v9, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 829
    if-nez v6, :cond_2

    .line 830
    const-string v0, "LocalAlbum"

    const-string v1, "query fail"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedBurstImageCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 837
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 834
    :cond_2
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 835
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedBurstImageCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 837
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 840
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedBurstImageCount:I

    if-le v0, v7, :cond_4

    move v0, v7

    :goto_1
    move v8, v0

    goto :goto_0

    .line 837
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_4
    move v0, v8

    .line 840
    goto :goto_1
.end method

.method public hide(Ljava/lang/String;)Z
    .locals 10
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 900
    if-nez p1, :cond_1

    move v5, v6

    .line 926
    :cond_0
    :goto_0
    return v5

    .line 903
    :cond_1
    const/4 v5, 0x0

    .line 904
    .local v5, "result":Z
    const/4 v4, 0x0

    .line 905
    .local v4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v7, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v7

    .line 906
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCount()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 907
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 909
    const/4 v5, 0x1

    .line 910
    const/4 v1, 0x0

    .line 911
    .local v1, "hideCount":I
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 912
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaItem;->hide(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 913
    const/4 v5, 0x0

    .line 919
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    if-eqz v5, :cond_3

    .line 920
    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/data/LocalAlbum;->delete(Z)V

    .line 922
    :cond_3
    if-lez v1, :cond_0

    .line 923
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    .line 924
    .local v0, "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getBucketId()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addHiddenAlbumInfo(I)Z

    goto :goto_0

    .line 907
    .end local v0    # "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .end local v1    # "hideCount":I
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 916
    .restart local v1    # "hideCount":I
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 918
    goto :goto_1
.end method

.method public isCameraAlbum()Z
    .locals 1

    .prologue
    .line 1004
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCameraAlbum:Z

    return v0
.end method

.method public isCameraRoll()Z
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    sget v1, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    sget v1, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SDCARD_CAMERA_BUCKET_ID:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHideOrRenameBlockedAlbum()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 887
    sget-object v0, Lcom/sec/android/gallery3d/util/MediaSetUtils;->BLOCK_HIDE_BUCKET_LIST:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget v1, v0, v2

    .line 888
    .local v1, "blockId":I
    iget v5, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mBucketId:I

    if-ne v1, v5, :cond_1

    .line 895
    .end local v1    # "blockId":I
    :cond_0
    :goto_1
    return v4

    .line 887
    .restart local v1    # "blockId":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 891
    .end local v1    # "blockId":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 892
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/gallery3d/data/LocalAlbum;->DCIM_PATH:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mPathOnFileSystem:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/gallery3d/data/LocalAlbum;->NCR_PATH:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 895
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 651
    const/4 v0, 0x1

    return v0
.end method

.method public reload()J
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 580
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mDataVersion:J

    .line 582
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedCount:I

    .line 583
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mGroupCachedCount:I

    .line 584
    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCachedBurstImageCount:I

    .line 585
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 588
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mDataVersion:J

    return-wide v0
.end method

.method public setCameraAlbum(Z)V
    .locals 0
    .param p1, "isCamAlbum"    # Z

    .prologue
    .line 1000
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mCameraAlbum:Z

    .line 1001
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 783
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalAlbum;->mName:Ljava/lang/String;

    .line 784
    return-void
.end method
