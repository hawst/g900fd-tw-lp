.class Lcom/sec/android/gallery3d/remote/slink/SLinkManager$3;
.super Ljava/lang/Object;
.source "SLinkManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->releaseWakeLockIfNeeded(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$3;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$3;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->access$100(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 181
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "releaseWakeLockIfNeeded : mActivity.getApplicationContext() is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "release network wake lock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkManager$3;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->access$300(Lcom/sec/android/gallery3d/remote/slink/SLinkManager;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->release()V

    goto :goto_0
.end method
