.class public Lcom/sec/android/gallery3d/data/LocalFaceImage;
.super Lcom/sec/android/gallery3d/data/LocalMediaItem;
.source "LocalFaceImage.java"


# instance fields
.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mFaceData:I

.field private mFacePath:Ljava/lang/String;

.field private mFacePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mFaceRect:Landroid/graphics/Rect;

.field private mFileId:I

.field private mGroupId:I

.field private mKind:Ljava/lang/String;

.field private mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

.field private mLocalImagePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mPathHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mPersonId:I

.field private mRecommendedId:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/util/SparseArray;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/gallery3d/data/LocalImage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "localImages":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/gallery3d/data/LocalImage;>;"
    const/4 v2, 0x0

    .line 59
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;JLcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceRect:Landroid/graphics/Rect;

    .line 29
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    .line 30
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 31
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mKind:Ljava/lang/String;

    .line 33
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    .line 34
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    .line 173
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePaths:Ljava/util/ArrayList;

    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 61
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->initFromPath()V

    .line 62
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->loadImage()V

    .line 63
    return-void
.end method

.method private initFromPath()V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 69
    .local v0, "p":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 70
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mKind:Ljava/lang/String;

    .line 72
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mGroupId:I

    .line 74
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    .line 76
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mRecommendedId:I

    .line 78
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 80
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 82
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 84
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 86
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceData:I

    .line 88
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 90
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFileId:I

    .line 93
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceData:I

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 94
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFileId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePath:Ljava/lang/String;

    .line 97
    :goto_0
    return-void

    .line 96
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData$BaseColumns;->FaceDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceData:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePath:Ljava/lang/String;

    goto :goto_0
.end method

.method private loadImage()V
    .locals 6

    .prologue
    .line 100
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFileId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 101
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    .line 102
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_0

    .line 103
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceData:I

    if-nez v1, :cond_1

    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 104
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getDataVersion()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setFaceImagePath(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/Long;)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getMimeType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mimeType:Ljava/lang/String;

    .line 110
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setFaceImagePathForExpansion(Lcom/sec/android/gallery3d/data/Path;)V

    .line 112
    :cond_0
    return-void

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getDataVersion()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/LocalImage;->setFaceImagePath(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/Long;)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getMimeType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public delete()V
    .locals 3

    .prologue
    .line 154
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/Path;

    .line 156
    .local v0, "localImagePath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 164
    .end local v0    # "localImagePath":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePaths:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 166
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePaths:Ljava/util/ArrayList;

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_2

    .line 170
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->deleteFaceImage()V

    .line 171
    :cond_2
    return-void
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 234
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 235
    .local v0, "baseUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFileId:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public getFacePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getHeight()I

    move-result v0

    return v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFileId:I

    return v0
.end method

.method public getItemType()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 224
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    if-le v1, v0, :cond_0

    .line 229
    :goto_0
    return v0

    .line 226
    :cond_0
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mRecommendedId:I

    if-le v1, v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mRecommendedId:I

    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    if-eq v0, v1, :cond_1

    .line 227
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    return-wide v0
.end method

.method public getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    return-object v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    return-wide v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x2

    return v0
.end method

.method public getPersonId()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    return v0
.end method

.method public getRotation()I
    .locals 2

    .prologue
    .line 147
    const-string v0, "people"

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mKind:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "group"

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mKind:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getRotation()I

    move-result v0

    .line 149
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getRotation()I

    move-result v0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 195
    const-wide v0, -0x65ffbfffffdffbfbL    # -1.912167862592089E-183

    .line 197
    .local v0, "operation":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mKind:Ljava/lang/String;

    const-string v3, "group"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 198
    const-wide/16 v0, 0x5

    .line 208
    :cond_0
    :goto_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirViewActionBar:Z

    if-eqz v2, :cond_1

    .line 209
    const-wide/16 v2, 0x8

    or-long/2addr v0, v2

    .line 210
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlashAnnotate:Z

    if-eqz v2, :cond_1

    .line 211
    const-wide/32 v2, 0x80000

    or-long/2addr v0, v2

    .line 213
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->isDrm()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 214
    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 215
    const-wide v2, -0x1000000000000001L    # -3.1050361846014175E231

    and-long/2addr v0, v2

    .line 216
    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    .line 219
    :cond_2
    return-wide v0

    .line 199
    :cond_3
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mRecommendedId:I

    if-le v2, v4, :cond_4

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mRecommendedId:I

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    if-eq v2, v3, :cond_4

    .line 200
    const-wide v0, -0x61ffbfffffdffbfbL

    goto :goto_0

    .line 201
    :cond_4
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mRecommendedId:I

    if-le v2, v4, :cond_5

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mRecommendedId:I

    iget v3, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    if-ne v2, v3, :cond_5

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mGroupId:I

    if-nez v2, :cond_5

    .line 202
    const-wide v0, -0x65ffbfffffdffbfbL    # -1.912167862592089E-183

    goto :goto_0

    .line 203
    :cond_5
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceData:I

    if-nez v2, :cond_6

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_6

    .line 204
    const-wide v0, -0x6fffbfffffdffbfbL

    goto :goto_0

    .line 205
    :cond_6
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mGroupId:I

    if-nez v2, :cond_0

    .line 206
    const-wide v0, -0x67ffbfffffdffbfbL    # -4.452112742215602E-193

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getWidth()I

    move-result v0

    return v0
.end method

.method public isDrm()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->isDrm()Z

    move-result v0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 18
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFaceData:I

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPersonId:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_0

    .line 117
    new-instance v3, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getModifiedDateInSec()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v9, v2, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    move/from16 v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    .line 125
    :goto_0
    return-object v3

    .line 118
    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePath:Ljava/lang/String;

    .line 119
    .local v9, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mKind:Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mKind:Ljava/lang/String;

    const-string v3, "group"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v2, :cond_1

    .line 121
    new-instance v3, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getModifiedDateInSec()J

    move-result-wide v6

    move/from16 v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_1
    new-instance v11, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getModifiedDateInSec()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v0, v2, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move/from16 v16, p1

    invoke-direct/range {v11 .. v17}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    move-object v3, v11

    goto :goto_0

    .line 125
    :cond_2
    new-instance v3, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getModifiedDateInSec()J

    move-result-wide v6

    move/from16 v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    goto :goto_0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 132
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFacePaths(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mFacePaths:Ljava/util/ArrayList;

    .line 177
    return-void
.end method

.method public setPathMap(Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "localImagePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local p2, "pathHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/Path;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    .line 38
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    .line 40
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_1

    .line 41
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 42
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImagePaths:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mLocalImage:Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/Path;

    .line 47
    .local v0, "localImagePath":Lcom/sec/android/gallery3d/data/Path;
    if-nez v0, :cond_1

    .line 48
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPathHashMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    .end local v0    # "localImagePath":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    return-void
.end method

.method protected updateFromCursor(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method

.method public updateVersion()J
    .locals 2

    .prologue
    .line 272
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mDataVersion:J

    .line 273
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalFaceImage;->mDataVersion:J

    return-wide v0
.end method
