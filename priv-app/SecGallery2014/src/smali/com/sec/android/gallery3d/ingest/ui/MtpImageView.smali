.class public Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;
.super Landroid/widget/ImageView;
.source "MtpImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$1;,
        Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$ShowImageHandler;,
        Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;
    }
.end annotation


# static fields
.field private static final MAX_FULLSIZE_PREVIEW_SIZE:I = 0x800000

.field private static final OVERLAY_ICON_SIZE_DENOMINATOR:I = 0x4

.field private static final TAG:Ljava/lang/String;

.field private static final sFetchCompleteHandler:Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$ShowImageHandler;

.field private static final sFetchHandler:Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;


# instance fields
.field private mDrawMatrix:Landroid/graphics/Matrix;

.field private mFetchDevice:Landroid/mtp/MtpDevice;

.field private mFetchLock:Ljava/lang/Object;

.field private mFetchObjectInfo:Landroid/mtp/MtpObjectInfo;

.field private mFetchPending:Z

.field private mFetchResult:Ljava/lang/Object;

.field private mGeneration:I

.field private mLastBitmapHeight:F

.field private mLastBitmapWidth:F

.field private mLastRotationDegrees:I

.field private mObjectHandle:I

.field private mOverlayIcon:Landroid/graphics/drawable/Drawable;

.field private mShowOverlayIcon:Z

.field private mWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->TAG:Ljava/lang/String;

    .line 57
    invoke-static {}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;->createOnNewThread()Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->sFetchHandler:Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$ShowImageHandler;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$ShowImageHandler;-><init>(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$1;)V

    sput-object v0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->sFetchCompleteHandler:Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$ShowImageHandler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mWeakReference:Ljava/lang/ref/WeakReference;

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchLock:Ljava/lang/Object;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchPending:Z

    .line 124
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    .line 66
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->init()V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mWeakReference:Ljava/lang/ref/WeakReference;

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchLock:Ljava/lang/Object;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchPending:Z

    .line 124
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    .line 71
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->init()V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mWeakReference:Ljava/lang/ref/WeakReference;

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchLock:Ljava/lang/Object;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchPending:Z

    .line 124
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    .line 76
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->init()V

    .line 77
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchPending:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;)Landroid/mtp/MtpDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchDevice:Landroid/mtp/MtpDevice;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;Landroid/mtp/MtpDevice;)Landroid/mtp/MtpDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;
    .param p1, "x1"    # Landroid/mtp/MtpDevice;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchDevice:Landroid/mtp/MtpDevice;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;)Landroid/mtp/MtpObjectInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchObjectInfo:Landroid/mtp/MtpObjectInfo;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;Landroid/mtp/MtpObjectInfo;)Landroid/mtp/MtpObjectInfo;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;
    .param p1, "x1"    # Landroid/mtp/MtpObjectInfo;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchObjectInfo:Landroid/mtp/MtpObjectInfo;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchResult:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchResult:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mWeakReference:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$700()Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$ShowImageHandler;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->sFetchCompleteHandler:Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$ShowImageHandler;

    return-object v0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->showPlaceholder()V

    .line 62
    return-void
.end method

.method private showPlaceholder()V
    .locals 1

    .prologue
    .line 80
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->setImageResource(I)V

    .line 81
    return-void
.end method

.method private updateDrawMatrix()V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, 0x3f000000    # 0.5f

    .line 127
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6}, Landroid/graphics/Matrix;->reset()V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->getHeight()I

    move-result v6

    int-to-float v4, v6

    .line 131
    .local v4, "vheight":F
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->getWidth()I

    move-result v6

    int-to-float v5, v6

    .line 133
    .local v5, "vwidth":F
    iget v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastRotationDegrees:I

    rem-int/lit16 v6, v6, 0xb4

    if-eqz v6, :cond_2

    const/4 v2, 0x1

    .line 134
    .local v2, "rotated90":Z
    :goto_0
    if-eqz v2, :cond_3

    .line 135
    iget v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastBitmapHeight:F

    .line 136
    .local v1, "dwidth":F
    iget v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastBitmapWidth:F

    .line 141
    .local v0, "dheight":F
    :goto_1
    cmpg-float v6, v1, v5

    if-gtz v6, :cond_4

    cmpg-float v6, v0, v4

    if-gtz v6, :cond_4

    .line 142
    const/high16 v3, 0x3f800000    # 1.0f

    .line 146
    .local v3, "scale":F
    :goto_2
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 147
    if-eqz v2, :cond_0

    .line 148
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    neg-float v7, v0

    mul-float/2addr v7, v3

    mul-float/2addr v7, v9

    neg-float v8, v1

    mul-float/2addr v8, v3

    mul-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 150
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    iget v7, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastRotationDegrees:I

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 151
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    mul-float v7, v1, v3

    mul-float/2addr v7, v9

    mul-float v8, v0, v3

    mul-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 154
    :cond_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    mul-float v7, v1, v3

    sub-float v7, v5, v7

    mul-float/2addr v7, v9

    mul-float v8, v0, v3

    sub-float v8, v4, v8

    mul-float/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 156
    if-nez v2, :cond_1

    iget v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastRotationDegrees:I

    if-lez v6, :cond_1

    .line 158
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    iget v7, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastRotationDegrees:I

    int-to-float v7, v7

    div-float v8, v5, v10

    div-float v9, v4, v10

    invoke-virtual {v6, v7, v8, v9}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 160
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 161
    return-void

    .line 133
    .end local v0    # "dheight":F
    .end local v1    # "dwidth":F
    .end local v2    # "rotated90":Z
    .end local v3    # "scale":F
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 138
    .restart local v2    # "rotated90":Z
    :cond_3
    iget v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastBitmapWidth:F

    .line 139
    .restart local v1    # "dwidth":F
    iget v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastBitmapHeight:F

    .restart local v0    # "dheight":F
    goto :goto_1

    .line 144
    :cond_4
    div-float v6, v5, v1

    div-float v7, v4, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .restart local v3    # "scale":F
    goto :goto_2
.end method

.method private updateOverlayIconBounds()V
    .locals 13

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 166
    iget-object v7, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 167
    .local v0, "iheight":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 168
    .local v1, "iwidth":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->getHeight()I

    move-result v5

    .line 169
    .local v5, "vheight":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->getWidth()I

    move-result v6

    .line 170
    .local v6, "vwidth":I
    int-to-float v7, v5

    mul-int/lit8 v8, v0, 0x4

    int-to-float v8, v8

    div-float v3, v7, v8

    .line 171
    .local v3, "scale_height":F
    int-to-float v7, v6

    mul-int/lit8 v8, v1, 0x4

    int-to-float v8, v8

    div-float v4, v7, v8

    .line 172
    .local v4, "scale_width":F
    cmpl-float v7, v3, v9

    if-ltz v7, :cond_0

    cmpl-float v7, v4, v9

    if-ltz v7, :cond_0

    .line 173
    iget-object v7, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    sub-int v8, v6, v1

    div-int/lit8 v8, v8, 0x2

    sub-int v9, v5, v0

    div-int/lit8 v9, v9, 0x2

    add-int v10, v6, v1

    div-int/lit8 v10, v10, 0x2

    add-int v11, v5, v0

    div-int/lit8 v11, v11, 0x2

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 184
    :goto_0
    return-void

    .line 178
    :cond_0
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 179
    .local v2, "scale":F
    iget-object v7, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    int-to-float v8, v6

    int-to-float v9, v1

    mul-float/2addr v9, v2

    sub-float/2addr v8, v9

    float-to-int v8, v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v9, v5

    int-to-float v10, v0

    mul-float/2addr v10, v2

    sub-float/2addr v9, v10

    float-to-int v9, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v10, v6

    int-to-float v11, v1

    mul-float/2addr v11, v2

    add-float/2addr v10, v11

    float-to-int v10, v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v11, v5

    int-to-float v12, v0

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    float-to-int v11, v11

    div-int/lit8 v11, v11, 0x2

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method


# virtual methods
.method protected cancelLoadingAndClear()V
    .locals 2

    .prologue
    .line 221
    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchLock:Ljava/lang/Object;

    monitor-enter v1

    .line 222
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchDevice:Landroid/mtp/MtpDevice;

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchObjectInfo:Landroid/mtp/MtpObjectInfo;

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchResult:Ljava/lang/Object;

    .line 225
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 227
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->setImageResource(I)V

    .line 228
    return-void

    .line 225
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected fetchMtpImageDataFromDevice(Landroid/mtp/MtpDevice;Landroid/mtp/MtpObjectInfo;)Ljava/lang/Object;
    .locals 3
    .param p1, "device"    # Landroid/mtp/MtpDevice;
    .param p2, "info"    # Landroid/mtp/MtpObjectInfo;

    .prologue
    .line 113
    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getCompressedSize()I

    move-result v0

    const/high16 v1, 0x800000

    if-gt v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_IMAGE_FORMATS:Ljava/util/Set;

    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getFormat()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-static {p1, p2}, Lcom/sec/android/gallery3d/ingest/data/MtpBitmapFetch;->getFullsize(Landroid/mtp/MtpDevice;Landroid/mtp/MtpObjectInfo;)Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;

    invoke-static {p1, p2}, Lcom/sec/android/gallery3d/ingest/data/MtpBitmapFetch;->getThumbnail(Landroid/mtp/MtpDevice;Landroid/mtp/MtpObjectInfo;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;-><init>(Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->cancelLoadingAndClear()V

    .line 233
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 234
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 200
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mShowOverlayIcon:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 203
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 188
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 189
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v0, v1, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->updateDrawMatrix()V

    .line 192
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mShowOverlayIcon:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 193
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->updateOverlayIconBounds()V

    .line 195
    :cond_1
    return-void
.end method

.method protected onMtpImageDataFetchedFromDevice(Ljava/lang/Object;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 206
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;

    .line 207
    .local v0, "bitmapWithMetadata":Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v1

    sget-object v2, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v1, v2, :cond_0

    .line 208
    iget-object v1, v0, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastBitmapHeight:F

    .line 209
    iget-object v1, v0, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastBitmapWidth:F

    .line 210
    iget v1, v0, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;->rotationDegrees:I

    iput v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mLastRotationDegrees:I

    .line 211
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->updateDrawMatrix()V

    .line 215
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->setAlpha(F)V

    .line 216
    iget-object v1, v0, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 218
    return-void

    .line 213
    :cond_0
    iget v1, v0, Lcom/sec/android/gallery3d/ingest/data/BitmapWithMetadata;->rotationDegrees:I

    int-to-float v1, v1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->setRotation(F)V

    goto :goto_0
.end method

.method public setMtpDeviceAndObjectInfo(Landroid/mtp/MtpDevice;Landroid/mtp/MtpObjectInfo;I)V
    .locals 6
    .param p1, "device"    # Landroid/mtp/MtpDevice;
    .param p2, "object"    # Landroid/mtp/MtpObjectInfo;
    .param p3, "gen"    # I

    .prologue
    .line 85
    if-nez p2, :cond_1

    .line 86
    sget-object v1, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->TAG:Ljava/lang/String;

    const-string v2, "setMtpDeviceAndObjectInfo object is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v0

    .line 90
    .local v0, "handle":I
    iget v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mObjectHandle:I

    if-ne v0, v1, :cond_2

    iget v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mGeneration:I

    if-eq p3, v1, :cond_0

    .line 93
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->cancelLoadingAndClear()V

    .line 94
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->showPlaceholder()V

    .line 95
    iput p3, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mGeneration:I

    .line 96
    iput v0, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mObjectHandle:I

    .line 97
    sget-object v1, Lcom/sec/android/gallery3d/ingest/MtpDeviceIndex;->SUPPORTED_VIDEO_FORMATS:Ljava/util/Set;

    invoke-virtual {p2}, Landroid/mtp/MtpObjectInfo;->getFormat()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mShowOverlayIcon:Z

    .line 98
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mShowOverlayIcon:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020180

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mOverlayIcon:Landroid/graphics/drawable/Drawable;

    .line 100
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->updateOverlayIconBounds()V

    .line 102
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchLock:Ljava/lang/Object;

    monitor-enter v2

    .line 103
    :try_start_0
    iput-object p2, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchObjectInfo:Landroid/mtp/MtpObjectInfo;

    .line 104
    iput-object p1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchDevice:Landroid/mtp/MtpDevice;

    .line 105
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchPending:Z

    if-eqz v1, :cond_4

    monitor-exit v2

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 106
    :cond_4
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mFetchPending:Z

    .line 107
    sget-object v1, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->sFetchHandler:Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;

    sget-object v3, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->sFetchHandler:Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView;->mWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/ingest/ui/MtpImageView$FetchImageHandler;->sendMessage(Landroid/os/Message;)Z

    .line 109
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
