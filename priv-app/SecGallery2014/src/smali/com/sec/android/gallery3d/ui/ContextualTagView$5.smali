.class Lcom/sec/android/gallery3d/ui/ContextualTagView$5;
.super Ljava/lang/Object;
.source "ContextualTagView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContextualTagView;->showContextualTagHelpView(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

.field final synthetic val$builder:Landroid/app/AlertDialog$Builder;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;Landroid/app/AlertDialog$Builder;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;->val$builder:Landroid/app/AlertDialog$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;->val$builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    # setter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$302(Lcom/sec/android/gallery3d/ui/ContextualTagView;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 378
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # getter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$300(Lcom/sec/android/gallery3d/ui/ContextualTagView;)Landroid/app/Dialog;

    move-result-object v0

    new-instance v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$5$1;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView$5;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # getter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$300(Lcom/sec/android/gallery3d/ui/ContextualTagView;)Landroid/app/Dialog;

    move-result-object v0

    new-instance v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$5$2;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$5$2;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView$5;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 395
    return-void
.end method
