.class Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;
.super Ljava/lang/Object;
.source "AccountSettingDetailFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->loadPreferenceFragment(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 356
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 357
    .local v0, "enable":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 358
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$300(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/preference/Preference;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0e00bc

    :goto_0
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 359
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # invokes: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateContextualTagPrefSwitchState()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$400(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    .line 360
    const-string v1, "AccountSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mContextualTagPref is now set to : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    if-eqz v0, :cond_0

    .line 362
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # setter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagChanged:Z
    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$502(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;Z)Z

    .line 363
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$4;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # invokes: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startTagBuddyFragment()V
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$600(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    .line 365
    :cond_0
    return v4

    .line 358
    :cond_1
    const v1, 0x7f0e0187

    goto :goto_0
.end method
