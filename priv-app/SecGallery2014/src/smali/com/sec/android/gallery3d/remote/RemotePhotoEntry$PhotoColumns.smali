.class public interface abstract Lcom/sec/android/gallery3d/remote/RemotePhotoEntry$PhotoColumns;
.super Ljava/lang/Object;
.source "RemotePhotoEntry.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/RemoteBaseEntry$Column;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PhotoColumns"
.end annotation


# static fields
.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final CACHE_PATHNAME:Ljava/lang/String; = "cache_pathname"

.field public static final CACHE_STATUS:Ljava/lang/String; = "cache_status"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "content_type"

.field public static final CONTENT_URL:Ljava/lang/String; = "content_url"

.field public static final DATE_EDITED:Ljava/lang/String; = "date_edited"

.field public static final DATE_TAKEN:Ljava/lang/String; = "date_taken"

.field public static final DISPLAY_INDEX:Ljava/lang/String; = "display_index"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final ROTATION:Ljava/lang/String; = "rotation"

.field public static final SCREENNAIL_URL:Ljava/lang/String; = "screennail_url"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final SOURCE_ID:Ljava/lang/String; = "source_id"

.field public static final THUMBNAIL_URL:Ljava/lang/String; = "thumbnail_url"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final WIDTH:Ljava/lang/String; = "width"
