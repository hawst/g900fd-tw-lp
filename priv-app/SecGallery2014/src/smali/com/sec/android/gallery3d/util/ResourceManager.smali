.class public Lcom/sec/android/gallery3d/util/ResourceManager;
.super Ljava/lang/Object;
.source "ResourceManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ResourceManager"

.field private static volatile mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;


# instance fields
.field mBitmapHashMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private mBrokenMovieThumbnail:Landroid/graphics/Bitmap;

.field private mBrokenPictureThumbnail:Landroid/graphics/Bitmap;

.field mDrawableHashMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/util/ResourceManager;->mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBitmapHashMap:Landroid/util/SparseArray;

    .line 38
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mDrawableHashMap:Landroid/util/SparseArray;

    .line 42
    iput-object v1, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenMovieThumbnail:Landroid/graphics/Bitmap;

    .line 43
    iput-object v1, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenPictureThumbnail:Landroid/graphics/Bitmap;

    .line 46
    return-void
.end method

.method private getFromCache(Landroid/util/SparseArray;I)Ljava/lang/Object;
    .locals 2
    .param p2, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<TT;>;>;I)TT;"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "cache":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/ref/WeakReference<TT;>;>;"
    monitor-enter p1

    .line 135
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 136
    .local v0, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    monitor-exit p1

    .line 139
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    monitor-exit p1

    goto :goto_0

    .line 140
    .end local v0    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :catchall_0
    move-exception v1

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;
    .locals 2

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/gallery3d/util/ResourceManager;->mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;

    if-nez v0, :cond_1

    .line 50
    const-class v1, Lcom/sec/android/gallery3d/util/ResourceManager;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/util/ResourceManager;->mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/sec/android/gallery3d/util/ResourceManager;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/ResourceManager;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/util/ResourceManager;->mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;

    .line 53
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/util/ResourceManager;->mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private putToCache(Landroid/util/SparseArray;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 3
    .param p2, "key"    # I
    .param p4, "forceReplace"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<TT;>;>;ITT;Z)TT;"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "cache":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/ref/WeakReference<TT;>;>;"
    .local p3, "data":Ljava/lang/Object;, "TT;"
    monitor-enter p1

    .line 146
    if-nez p4, :cond_1

    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "existData":Ljava/lang/Object;, "TT;"
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 149
    .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    if-eqz v1, :cond_0

    .line 150
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 152
    .end local v0    # "existData":Ljava/lang/Object;, "TT;"
    :cond_0
    if-eqz v0, :cond_1

    .line 153
    monitor-exit p1

    .line 157
    .end local v1    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    :goto_0
    return-object v0

    .line 156
    :cond_1
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 157
    monitor-exit p1

    move-object v0, p3

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private release()V
    .locals 2

    .prologue
    .line 64
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBitmapHashMap:Landroid/util/SparseArray;

    monitor-enter v1

    .line 65
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBitmapHashMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 66
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mDrawableHashMap:Landroid/util/SparseArray;

    monitor-enter v1

    .line 68
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mDrawableHashMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 69
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 70
    return-void

    .line 66
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 69
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized releaseInstance()V
    .locals 2

    .prologue
    .line 58
    const-class v1, Lcom/sec/android/gallery3d/util/ResourceManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/util/ResourceManager;->mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lcom/sec/android/gallery3d/util/ResourceManager;->mResourceManager:Lcom/sec/android/gallery3d/util/ResourceManager;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :cond_0
    monitor-exit v1

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 79
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseResouceManager:Z

    if-eqz v3, :cond_4

    .line 80
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBitmapHashMap:Landroid/util/SparseArray;

    invoke-direct {p0, v3, p2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getFromCache(Landroid/util/SparseArray;I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 81
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 83
    :cond_0
    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 88
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBitmapHashMap:Landroid/util/SparseArray;

    monitor-enter v4

    .line 89
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBitmapHashMap:Landroid/util/SparseArray;

    const/4 v5, 0x0

    invoke-direct {p0, v3, p2, v0, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->putToCache(Landroid/util/SparseArray;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 90
    .local v1, "cachedBitmap":Landroid/graphics/Bitmap;
    if-eq v1, v0, :cond_1

    .line 91
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_3

    .line 92
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 93
    move-object v0, v1

    .line 98
    :cond_1
    :goto_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    .end local v1    # "cachedBitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_2
    return-object v0

    .line 84
    :catch_0
    move-exception v2

    .line 85
    .local v2, "ome":Ljava/lang/OutOfMemoryError;
    const-string v3, "ResourceManager"

    const-string v4, "decodeResource falied. OutOfMemoryError occured.."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0

    .line 95
    .end local v2    # "ome":Ljava/lang/OutOfMemoryError;
    .restart local v1    # "cachedBitmap":Landroid/graphics/Bitmap;
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBitmapHashMap:Landroid/util/SparseArray;

    const/4 v5, 0x1

    invoke-direct {p0, v3, p2, v0, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->putToCache(Landroid/util/SparseArray;ILjava/lang/Object;Z)Ljava/lang/Object;

    goto :goto_1

    .line 98
    .end local v1    # "cachedBitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 104
    :cond_4
    invoke-static {p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method

.method public getBrokenMovieThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenMovieThumbnail:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenMovieThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    :cond_0
    const v0, 0x7f020489

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenMovieThumbnail:Landroid/graphics/Bitmap;

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenMovieThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenPictureThumbnail:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenPictureThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    const v0, 0x7f020488

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenPictureThumbnail:Landroid/graphics/Bitmap;

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mBrokenPictureThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBrokenThumnailSize(Landroid/content/res/Resources;I)I
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "brokenType"    # I

    .prologue
    .line 176
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 177
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 179
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenMovieThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "resId"    # I

    .prologue
    .line 110
    const/4 v1, 0x0

    .line 111
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseResouceManager:Z

    if-eqz v3, :cond_1

    .line 112
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mDrawableHashMap:Landroid/util/SparseArray;

    invoke-direct {p0, v3, p2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getFromCache(Landroid/util/SparseArray;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    .line 113
    .local v0, "dc":Landroid/graphics/drawable/Drawable$ConstantState;
    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 116
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ResourceManager;->mDrawableHashMap:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v3, p2, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->putToCache(Landroid/util/SparseArray;ILjava/lang/Object;Z)Ljava/lang/Object;

    .line 130
    .end local v0    # "dc":Landroid/graphics/drawable/Drawable$ConstantState;
    :goto_0
    return-object v1

    .line 118
    .restart local v0    # "dc":Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_0
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 124
    .end local v0    # "dc":Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_1
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 125
    :catch_0
    move-exception v2

    .line 126
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 127
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method
