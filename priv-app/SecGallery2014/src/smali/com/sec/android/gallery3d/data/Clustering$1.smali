.class Lcom/sec/android/gallery3d/data/Clustering$1;
.super Ljava/lang/Object;
.source "Clustering.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/Clustering;->enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/Clustering;

.field final synthetic val$items:Ljava/util/ArrayList;

.field final synthetic val$total:I


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/Clustering;ILjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Clustering$1;->this$0:Lcom/sec/android/gallery3d/data/Clustering;

    iput p2, p0, Lcom/sec/android/gallery3d/data/Clustering$1;->val$total:I

    iput-object p3, p0, Lcom/sec/android/gallery3d/data/Clustering$1;->val$items:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 5
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v4, 0x1

    .line 240
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/data/Clustering$1;->val$total:I

    if-lt p1, v1, :cond_1

    .line 248
    :cond_0
    :goto_0
    return v4

    .line 242
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Clustering$1;->this$0:Lcom/sec/android/gallery3d/data/Clustering;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;-><init>(Lcom/sec/android/gallery3d/data/Clustering;)V

    .line 243
    .local v0, "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 244
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    .line 245
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    .line 246
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 247
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Clustering$1;->val$items:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
