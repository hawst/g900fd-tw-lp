.class Lcom/sec/android/gallery3d/app/BasicDream$1;
.super Landroid/content/BroadcastReceiver;
.source "BasicDream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/BasicDream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/BasicDream;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/BasicDream;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/BasicDream$1;->this$0:Lcom/sec/android/gallery3d/app/BasicDream;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x80

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    const-string v3, "plugged"

    invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-ne v1, v3, :cond_1

    .line 83
    .local v1, "plugged":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BasicDream$1;->this$0:Lcom/sec/android/gallery3d/app/BasicDream;

    # getter for: Lcom/sec/android/gallery3d/app/BasicDream;->mPlugged:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/BasicDream;->access$000(Lcom/sec/android/gallery3d/app/BasicDream;)Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 84
    const-string v3, "BasicDream"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "now "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v1, :cond_2

    const-string v2, "plugged in"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BasicDream$1;->this$0:Lcom/sec/android/gallery3d/app/BasicDream;

    # setter for: Lcom/sec/android/gallery3d/app/BasicDream;->mPlugged:Z
    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/app/BasicDream;->access$002(Lcom/sec/android/gallery3d/app/BasicDream;Z)Z

    .line 87
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BasicDream$1;->this$0:Lcom/sec/android/gallery3d/app/BasicDream;

    # getter for: Lcom/sec/android/gallery3d/app/BasicDream;->mPlugged:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/BasicDream;->access$000(Lcom/sec/android/gallery3d/app/BasicDream;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 88
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BasicDream$1;->this$0:Lcom/sec/android/gallery3d/app/BasicDream;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/BasicDream;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->addFlags(I)V

    .line 94
    .end local v1    # "plugged":Z
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v1, v2

    .line 81
    goto :goto_0

    .line 84
    .restart local v1    # "plugged":Z
    :cond_2
    const-string/jumbo v2, "unplugged"

    goto :goto_1

    .line 90
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BasicDream$1;->this$0:Lcom/sec/android/gallery3d/app/BasicDream;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/BasicDream;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_2
.end method
