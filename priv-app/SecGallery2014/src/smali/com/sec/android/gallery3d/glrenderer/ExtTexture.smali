.class public Lcom/sec/android/gallery3d/glrenderer/ExtTexture;
.super Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
.source "ExtTexture.java"


# instance fields
.field private mTarget:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;I)V
    .locals 2
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "target"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;-><init>()V

    .line 29
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getGLId()Lcom/sec/android/gallery3d/glrenderer/GLId;

    move-result-object v0

    .line 30
    .local v0, "glId":Lcom/sec/android/gallery3d/glrenderer/GLId;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/glrenderer/GLId;->generateTexture()I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/ExtTexture;->mId:I

    .line 31
    iput p2, p0, Lcom/sec/android/gallery3d/glrenderer/ExtTexture;->mTarget:I

    .line 32
    return-void
.end method

.method private uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 35
    invoke-interface {p1, p0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setTextureParameters(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/ExtTexture;->setAssociatedCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/ExtTexture;->mState:I

    .line 38
    return-void
.end method


# virtual methods
.method public getTarget()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/ExtTexture;->mTarget:I

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method protected onBind(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/ExtTexture;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/ExtTexture;->uploadToCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 46
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public yield()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method
