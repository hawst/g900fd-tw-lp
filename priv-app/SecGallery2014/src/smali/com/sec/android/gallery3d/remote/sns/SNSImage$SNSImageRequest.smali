.class Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;
.super Ljava/lang/Object;
.source "SNSImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/SNSImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SNSImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 0
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "type"    # I

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput p3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->mType:I

    .line 123
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 17
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 127
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->access$200(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v2

    .line 129
    .local v2, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v7

    .line 131
    .local v7, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->access$300(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getDateInMs()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->mType:I

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v15

    .line 132
    .local v15, "found":Z
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v14, 0x0

    .line 143
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 170
    :goto_0
    return-object v14

    .line 133
    :cond_0
    if-eqz v15, :cond_2

    .line 134
    :try_start_1
    new-instance v16, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 135
    .local v16, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v16

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 136
    iget-object v3, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v4, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    iget v5, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v3, v4, v5, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeUsingPool(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 137
    .local v14, "bitmap":Landroid/graphics/Bitmap;
    if-nez v14, :cond_1

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 138
    const-string v3, "SNSImage"

    const-string v4, "decode cached failed "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_0

    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    .end local v16    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 146
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 147
    const/4 v14, 0x0

    goto :goto_0

    .line 143
    .end local v15    # "found":Z
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v3

    .line 150
    .restart local v15    # "found":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->access$400(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 151
    const/4 v14, 0x0

    goto :goto_0

    .line 154
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->mType:I

    # invokes: Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getPhotoUrl(I)Ljava/net/URL;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->access$500(Lcom/sec/android/gallery3d/remote/sns/SNSImage;I)Ljava/net/URL;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/DownloadUtils;->requestDownload(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;)[B

    move-result-object v13

    .line 155
    .local v13, "array":[B
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v14, 0x0

    goto :goto_0

    .line 157
    :cond_5
    if-nez v13, :cond_6

    .line 158
    const-string v3, "SNSImage"

    const-string v4, "download failed "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 162
    :cond_6
    new-instance v16, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 163
    .restart local v16    # "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v16

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 164
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 166
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v14, :cond_7

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_7
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 169
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->access$600(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getDateInMs()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->mType:I

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V

    goto/16 :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
