.class Lcom/sec/android/gallery3d/ui/ContactPopup$4;
.super Ljava/lang/Object;
.source "ContactPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContactPopup;->reloadWidget()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 536
    const-string v3, "ContactPopup"

    const-string v4, "Message button clicked!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 538
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/samsung/gallery/access/contact/ContactData;->phoneNum:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 539
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$600(Lcom/sec/android/gallery3d/ui/ContactPopup;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    const/4 v4, 0x1

    # setter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$602(Lcom/sec/android/gallery3d/ui/ContactPopup;Z)Z

    .line 543
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$900(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 544
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$900(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getId()I

    move-result v4

    # invokes: Lcom/sec/android/gallery3d/ui/ContactPopup;->resetButtonsClickable(IZ)V
    invoke-static {v3, v4, v6}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$800(Lcom/sec/android/gallery3d/ui/ContactPopup;IZ)V

    .line 545
    :cond_2
    const-string/jumbo v3, "smsto"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/samsung/gallery/access/contact/ContactData;->phoneNum:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 546
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SENDTO"

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 548
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 549
    :catch_0
    move-exception v0

    .line 550
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0062

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 551
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$4;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # setter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z
    invoke-static {v3, v6}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$602(Lcom/sec/android/gallery3d/ui/ContactPopup;Z)Z

    .line 552
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
