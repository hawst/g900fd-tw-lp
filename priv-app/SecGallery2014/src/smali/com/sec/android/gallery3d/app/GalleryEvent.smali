.class public Lcom/sec/android/gallery3d/app/GalleryEvent;
.super Ljava/lang/Object;
.source "GalleryEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;,
        Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;,
        Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;,
        Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;,
        Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;
    }
.end annotation


# instance fields
.field private mBody:Ljava/lang/Object;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mName:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mBody:Ljava/lang/Object;

    .line 31
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mName:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mBody:Ljava/lang/Object;

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mName:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mBody:Ljava/lang/Object;

    .line 36
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mName:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mBody:Ljava/lang/Object;

    .line 38
    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mBody:Ljava/lang/Object;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public setBody(Ljava/lang/Object;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/Object;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryEvent;->mBody:Ljava/lang/Object;

    .line 50
    return-void
.end method
