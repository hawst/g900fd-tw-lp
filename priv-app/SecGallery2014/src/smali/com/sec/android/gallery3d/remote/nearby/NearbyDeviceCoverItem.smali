.class public Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaItem;
.source "NearbyDeviceCoverItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDevice:Lcom/samsung/android/allshare/Device;

.field private mHttpClient:Landroid/net/http/AndroidHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/samsung/android/allshare/Device;J)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "device"    # Lcom/samsung/android/allshare/Device;
    .param p4, "version"    # J

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 100
    const-string v0, "Nearby Cover Item"

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    .line 44
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;

    const-string v1, "new NearbyDeviceCoverItem"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/samsung/android/allshare/Device;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Landroid/net/http/AndroidHttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/net/http/AndroidHttpClient;)Landroid/net/http/AndroidHttpClient;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;
    .param p1, "x1"    # Landroid/net/http/AndroidHttpClient;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDataVersion:J

    return-wide v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->convertToThumbnail(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDataVersion:J

    return-wide v0
.end method

.method private convertToThumbnail(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "iconBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v9, 0xc8

    const/16 v8, 0x96

    .line 213
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 214
    .local v2, "iconWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 216
    .local v1, "iconHeight":I
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v8, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 220
    .local v6, "thumbBitmap":Landroid/graphics/Bitmap;
    if-ge v2, v9, :cond_0

    if-ge v1, v8, :cond_0

    .line 221
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 222
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 223
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 224
    const/high16 v7, -0x1000000

    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 225
    rsub-int v7, v2, 0xc8

    div-int/lit8 v3, v7, 0x2

    .line 226
    .local v3, "offsetX":I
    rsub-int v7, v1, 0x96

    div-int/lit8 v4, v7, 0x2

    .line 227
    .local v4, "offsetY":I
    int-to-float v7, v3

    int-to-float v8, v4

    invoke-virtual {v0, p1, v7, v8, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 230
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "offsetX":I
    .end local v4    # "offsetY":I
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "thumbBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v6

    .restart local v6    # "thumbBitmap":Landroid/graphics/Bitmap;
    :cond_0
    move-object v6, p1

    goto :goto_0
.end method


# virtual methods
.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    if-nez v0, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 238
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 97
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 3
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v0

    .line 52
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 53
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;)V

    .line 62
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem$HttpImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method
