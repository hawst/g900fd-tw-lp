.class public Lcom/sec/android/gallery3d/util/HdmiUtils;
.super Ljava/lang/Object;
.source "HdmiUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;,
        Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_HDMI_PLUGGED:Ljava/lang/String; = "android.intent.action.HDMI_PLUGGED"

.field private static final KEY_STATE:Ljava/lang/String; = "state"

.field private static final MSG_HDMI_CONNECTED:I = 0x6

.field private static final NAME_HDMI:Ljava/lang/String; = "HDMI"

.field public static final S3D_2DTV_CONNECTED:I = 0x2

.field public static final S3D_3DTV_CONNECTED:I = 0x3

.field public static final S3D_ERROR:I = -0x1

.field private static final S3D_NONE:I = 0x0

.field public static final S3D_NOT_CONNECTED:I = 0x0

.field private static final S3D_SBS_LR:I = 0x1

.field public static final S3D_UNKNOWN_CONNECTED:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static mCurrent3DMode:I

.field private static mHandler:Landroid/os/Handler;

.field private static mIsSupport3D:I

.field private static mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;",
            ">;"
        }
    .end annotation
.end field

.field public static sIsPlugged:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    const-class v0, Lcom/sec/android/gallery3d/util/HdmiUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->TAG:Ljava/lang/String;

    .line 42
    sput-boolean v1, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    .line 43
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mIsSupport3D:I

    .line 44
    sput v1, Lcom/sec/android/gallery3d/util/HdmiUtils;->mCurrent3DMode:I

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mListeners:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Lcom/sec/android/gallery3d/util/HdmiUtils$1;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/HdmiUtils$1;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    return-void
.end method

.method static synthetic access$000()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 23
    sput p0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mIsSupport3D:I

    return p0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static addHdmiConnectListener(Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    .prologue
    .line 173
    sget-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    sget-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_0
    return-void
.end method

.method public static getHdmiIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.HDMI_PLUGGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static initHdmiConnected(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTvOut:Z

    if-nez v3, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    const/4 v3, 0x0

    sput-boolean v3, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    .line 90
    const-string v3, "media_router"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaRouter;

    .line 92
    .local v1, "mediaRouter":Landroid/media/MediaRouter;
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v2

    .line 96
    .local v2, "routeInfo":Landroid/media/MediaRouter$RouteInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    move-result v3

    if-nez v3, :cond_0

    .line 100
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    .line 101
    .local v0, "display":Landroid/view/Display;
    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {v0}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "HDMI"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    sput-boolean v3, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    .line 109
    const/4 v3, -0x1

    sput v3, Lcom/sec/android/gallery3d/util/HdmiUtils;->mIsSupport3D:I

    .line 110
    sget-object v3, Lcom/sec/android/gallery3d/util/HdmiUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initHdmiConnected plugged = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static is3DTvConnected()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 157
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTvOut:Z

    if-nez v1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v0

    .line 167
    :cond_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/gallery3d/util/HdmiUtils;->mIsSupport3D:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 168
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final isPlugged()Z
    .locals 1

    .prologue
    .line 63
    sget-boolean v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    return v0
.end method

.method public static removeHdmiConnectListener(Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    .prologue
    .line 179
    sget-object v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 180
    return-void
.end method

.method public static setExternal3Dmode(Z)V
    .locals 3
    .param p0, "enable"    # Z

    .prologue
    .line 132
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTvOut:Z

    if-nez v1, :cond_0

    .line 154
    :goto_0
    return-void

    .line 135
    :cond_0
    sget v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->mCurrent3DMode:I

    .line 143
    .local v0, "new3Dmode":I
    sget v1, Lcom/sec/android/gallery3d/util/HdmiUtils;->mIsSupport3D:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 144
    if-eqz p0, :cond_1

    const/4 v0, 0x1

    :goto_1
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 146
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
