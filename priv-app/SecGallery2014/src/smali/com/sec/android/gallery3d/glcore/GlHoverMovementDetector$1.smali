.class Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;
.super Lcom/sec/android/gallery3d/glcore/GlHandler;
.source "GlHoverMovementDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glcore/GlHandler;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public onMessage(ILjava/lang/Object;III)V
    .locals 8
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const-wide/16 v6, 0xa

    const/4 v4, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 66
    if-ne p1, v4, :cond_1

    .line 67
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    # setter for: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z
    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$002(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Z)Z

    .line 68
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$100(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    if-ne p1, v2, :cond_0

    .line 70
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$000(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 71
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveEvent:Landroid/view/MotionEvent;
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$200(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Landroid/view/MotionEvent;

    move-result-object v4

    # invokes: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->updateMovement(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$300(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 72
    .local v0, "updatedEvent":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    # invokes: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->calcMovement(Landroid/view/MotionEvent;Z)Z
    invoke-static {v1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$400(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Landroid/view/MotionEvent;Z)Z

    .line 73
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$100(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    goto :goto_0

    .line 75
    .end local v0    # "updatedEvent":Landroid/view/MotionEvent;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->access$100(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    goto :goto_0
.end method
