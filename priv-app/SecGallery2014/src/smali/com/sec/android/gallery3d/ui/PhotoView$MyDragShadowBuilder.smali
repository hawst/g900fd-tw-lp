.class Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;
.super Landroid/view/View$DragShadowBuilder;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyDragShadowBuilder"
.end annotation


# static fields
.field private static shadow:Landroid/graphics/drawable/BitmapDrawable;

.field private static size:I


# instance fields
.field private bmpHeight:I

.field private bmpWidth:I

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4626
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->size:I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 4632
    invoke-direct {p0, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 4628
    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->bmpWidth:I

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->bmpHeight:I

    .line 4633
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->mContext:Landroid/content/Context;

    .line 4634
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mDragUri:Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10000()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 4635
    .local v3, "mFilePath":Ljava/lang/String;
    sget v4, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->size:I

    sget v5, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->size:I

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getSafeResizingBitmap(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 4636
    .local v0, "bmp":Landroid/graphics/Bitmap;
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mDragUri:Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10000()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "content://media/external/video/media"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4637
    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 4638
    .local v1, "bmp_background":Landroid/graphics/Bitmap;
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$10100()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020308

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 4639
    .local v2, "bmp_foreground":Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->combineBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 4642
    .end local v1    # "bmp_background":Landroid/graphics/Bitmap;
    .end local v2    # "bmp_foreground":Landroid/graphics/Bitmap;
    :cond_0
    if-eqz v0, :cond_1

    .line 4643
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->bmpWidth:I

    .line 4644
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->bmpHeight:I

    .line 4646
    :cond_1
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    sput-object v4, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->shadow:Landroid/graphics/drawable/BitmapDrawable;

    .line 4647
    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 4668
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->shadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 4669
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 8
    .param p1, "size"    # Landroid/graphics/Point;
    .param p2, "touch"    # Landroid/graphics/Point;

    .prologue
    const v7, 0x7f0d0229

    const v6, 0x7f0d0228

    const/4 v5, 0x0

    .line 4651
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 4652
    .local v1, "res":Landroid/content/res/Resources;
    const/4 v2, 0x0

    .line 4653
    .local v2, "width":I
    const/4 v0, 0x0

    .line 4654
    .local v0, "height":I
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->bmpWidth:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->bmpHeight:I

    if-lt v3, v4, :cond_0

    .line 4655
    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 4656
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 4661
    :goto_0
    sget-object v3, Lcom/sec/android/gallery3d/ui/PhotoView$MyDragShadowBuilder;->shadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3, v5, v5, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 4662
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Point;->set(II)V

    .line 4663
    div-int/lit8 v3, v2, 0x2

    div-int/lit8 v4, v0, 0x2

    invoke-virtual {p2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 4664
    return-void

    .line 4658
    :cond_0
    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 4659
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_0
.end method
