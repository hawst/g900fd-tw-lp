.class Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
.super Ljava/lang/Object;
.source "TCloudAlbumSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BucketEntry"
.end annotation


# instance fields
.field public bucketId:I

.field public bucketIdString:Ljava/lang/String;

.field public bucketName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "bIdString"    # Ljava/lang/String;

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput p1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketId:I

    .line 192
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->ensureNotNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketName:Ljava/lang/String;

    .line 193
    invoke-static {p3}, Lcom/sec/android/gallery3d/common/Utils;->ensureNotNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketIdString:Ljava/lang/String;

    .line 194
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 203
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;

    if-nez v2, :cond_1

    .line 207
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 206
    check-cast v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;

    .line 207
    .local v0, "entry":Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;
    iget v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketId:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketId:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet$BucketEntry;->bucketId:I

    return v0
.end method
