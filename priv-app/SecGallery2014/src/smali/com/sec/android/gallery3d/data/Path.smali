.class public Lcom/sec/android/gallery3d/data/Path;
.super Ljava/lang/Object;
.source "Path.java"

# interfaces
.implements Ljava/io/Externalizable;
.implements Ljava/io/Serializable;


# static fields
.field private static final TAG:Ljava/lang/String; = "Path"

.field private static sRoot:Lcom/sec/android/gallery3d/data/Path; = null

.field private static final serialVersionUID:J = -0x3ad6ca21cdf6e292L


# instance fields
.field private mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/IdentityCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mObject:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field

.field private mParent:Lcom/sec/android/gallery3d/data/Path;

.field private mSegment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/sec/android/gallery3d/data/Path;

    const/4 v1, 0x0

    const-string v2, "ROOT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/Path;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method private constructor <init>(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;)V
    .locals 0
    .param p1, "parent"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "segment"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    .line 48
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    .line 49
    return-void
.end method

.method static clearAll()V
    .locals 4

    .prologue
    .line 237
    const-class v1, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v1

    .line 238
    :try_start_0
    new-instance v0, Lcom/sec/android/gallery3d/data/Path;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-direct {v0, v2, v3}, Lcom/sec/android/gallery3d/data/Path;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    .line 239
    monitor-exit v1

    .line 240
    return-void

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static dumpAll()V
    .locals 3

    .prologue
    .line 243
    sget-object v0, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    const-string v1, ""

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/Path;->dumpAll(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return-void
.end method

.method static dumpAll(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p0, "p"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "prefix1"    # Ljava/lang/String;
    .param p2, "prefix2"    # Ljava/lang/String;

    .prologue
    .line 247
    const-class v8, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v8

    .line 248
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    .line 249
    .local v6, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    const-string v9, "Path"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ":"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-nez v6, :cond_0

    const-string v7, "null"

    :goto_0
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v7}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/Path;->mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;

    if-eqz v7, :cond_3

    .line 252
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/Path;->mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/util/IdentityCache;->keys()Ljava/util/ArrayList;

    move-result-object v1

    .line 253
    .local v1, "childrenKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 254
    .local v5, "n":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 255
    .local v4, "key":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/Path;->mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/util/IdentityCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/Path;

    .line 256
    .local v0, "child":Lcom/sec/android/gallery3d/data/Path;
    if-nez v0, :cond_1

    .line 257
    add-int/lit8 v2, v2, 0x1

    .line 258
    goto :goto_1

    .line 249
    .end local v0    # "child":Lcom/sec/android/gallery3d/data/Path;
    .end local v1    # "childrenKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "n":I
    :cond_0
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 260
    .restart local v0    # "child":Lcom/sec/android/gallery3d/data/Path;
    .restart local v1    # "childrenKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "i":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "key":Ljava/lang/String;
    .restart local v5    # "n":I
    :cond_1
    const-string v7, "Path"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "|"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v5, :cond_2

    .line 262
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "+-- "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "|   "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v7, v9}, Lcom/sec/android/gallery3d/data/Path;->dumpAll(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 268
    .end local v0    # "child":Lcom/sec/android/gallery3d/data/Path;
    .end local v1    # "childrenKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "n":I
    .end local v6    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 264
    .restart local v0    # "child":Lcom/sec/android/gallery3d/data/Path;
    .restart local v1    # "childrenKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "i":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "key":Ljava/lang/String;
    .restart local v5    # "n":I
    .restart local v6    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "+-- "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "    "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v7, v9}, Lcom/sec/android/gallery3d/data/Path;->dumpAll(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 268
    .end local v0    # "child":Lcom/sec/android/gallery3d/data/Path;
    .end local v1    # "childrenKeys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "n":I
    :cond_3
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 122
    const-class v4, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v4

    .line 123
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/data/Path;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "segments":[Ljava/lang/String;
    sget-object v0, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    .line 125
    .local v0, "current":Lcom/sec/android/gallery3d/data/Path;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 126
    aget-object v3, v2, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 127
    if-nez v0, :cond_0

    .line 128
    sget-object v3, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    monitor-exit v4

    .line 131
    :goto_1
    return-object v3

    .line 125
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 131
    :cond_1
    monitor-exit v4

    move-object v3, v0

    goto :goto_1

    .line 132
    .end local v0    # "current":Lcom/sec/android/gallery3d/data/Path;
    .end local v1    # "i":I
    .end local v2    # "segments":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static fromString([Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 6
    .param p0, "s"    # [Ljava/lang/String;
    .param p1, "separator"    # Ljava/lang/String;

    .prologue
    .line 273
    const-string v3, ""

    .line 275
    .local v3, "p":Ljava/lang/String;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 276
    .local v4, "str":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 275
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 279
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    invoke-static {v3}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    return-object v5
.end method

.method public static split(Ljava/lang/String;)[Ljava/lang/String;
    .locals 10
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x2f

    const/4 v7, 0x0

    .line 151
    if-nez p0, :cond_0

    .line 152
    new-array v5, v7, [Ljava/lang/String;

    .line 178
    :goto_0
    return-object v5

    .line 154
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 155
    .local v4, "n":I
    if-nez v4, :cond_1

    new-array v5, v7, [Ljava/lang/String;

    goto :goto_0

    .line 156
    :cond_1
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v8, :cond_2

    .line 157
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "malformed path:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 159
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v6, "segments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 161
    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_8

    .line 162
    const/4 v0, 0x0

    .line 164
    .local v0, "brace":I
    move v3, v2

    .local v3, "j":I
    :goto_2
    if-ge v3, v4, :cond_6

    .line 165
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 166
    .local v1, "c":C
    const/16 v7, 0x7b

    if-ne v1, v7, :cond_4

    add-int/lit8 v0, v0, 0x1

    .line 164
    :cond_3
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 167
    :cond_4
    const/16 v7, 0x7d

    if-ne v1, v7, :cond_5

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 168
    :cond_5
    if-nez v0, :cond_3

    if-ne v1, v8, :cond_3

    .line 170
    .end local v1    # "c":C
    :cond_6
    if-eqz v0, :cond_7

    .line 171
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "unbalanced brace in path:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 173
    :cond_7
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    add-int/lit8 v2, v3, 0x1

    .line 175
    goto :goto_1

    .line 176
    .end local v0    # "brace":I
    .end local v3    # "j":I
    :cond_8
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v5, v7, [Ljava/lang/String;

    .line 177
    .local v5, "result":[Ljava/lang/String;
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0
.end method

.method public static splitSequence(Ljava/lang/String;)[Ljava/lang/String;
    .locals 12
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x7b

    const/4 v10, 0x0

    const/16 v9, 0x7d

    .line 184
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 185
    .local v4, "n":I
    invoke-virtual {p0, v10}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v7, v11, :cond_0

    invoke-virtual {p0, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_1

    .line 186
    :cond_0
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bad sequence: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 188
    :cond_1
    add-int/lit8 v7, v4, -0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v9, :cond_2

    .line 189
    invoke-virtual {p0, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p0, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 190
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .local v6, "segments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 192
    .local v2, "i":I
    :goto_0
    add-int/lit8 v7, v4, -0x1

    if-ge v2, v7, :cond_8

    .line 193
    const/4 v0, 0x0

    .line 195
    .local v0, "brace":I
    move v3, v2

    .local v3, "j":I
    :goto_1
    add-int/lit8 v7, v4, -0x1

    if-ge v3, v7, :cond_6

    .line 196
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 197
    .local v1, "c":C
    if-ne v1, v11, :cond_4

    add-int/lit8 v0, v0, 0x1

    .line 195
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 198
    :cond_4
    if-ne v1, v9, :cond_5

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 199
    :cond_5
    if-nez v0, :cond_3

    const/16 v7, 0x2c

    if-ne v1, v7, :cond_3

    .line 201
    .end local v1    # "c":C
    :cond_6
    if-eqz v0, :cond_7

    .line 202
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "unbalanced brace in path:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 204
    :cond_7
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    add-int/lit8 v2, v3, 0x1

    .line 206
    goto :goto_0

    .line 207
    .end local v0    # "brace":I
    .end local v3    # "j":I
    :cond_8
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v5, v7, [Ljava/lang/String;

    .line 208
    .local v5, "result":[Ljava/lang/String;
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 209
    return-object v5
.end method


# virtual methods
.method public equalsIgnoreCase(Ljava/lang/String;)Z
    .locals 2
    .param p1, "p"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public getChild(I)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p1, "segment"    # I

    .prologue
    .line 78
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    return-object v0
.end method

.method public getChild(J)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p1, "segment"    # J

    .prologue
    .line 82
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    return-object v0
.end method

.method public getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 4
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v3, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v3

    .line 58
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/Path;->mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;

    if-nez v2, :cond_1

    .line 59
    new-instance v2, Lcom/sec/android/gallery3d/util/IdentityCache;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/util/IdentityCache;-><init>()V

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/Path;->mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;

    .line 65
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/data/Path;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/data/Path;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/lang/String;)V

    .line 66
    .local v0, "p":Lcom/sec/android/gallery3d/data/Path;
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/Path;->mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;

    invoke-virtual {v2, p1, v0}, Lcom/sec/android/gallery3d/util/IdentityCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    monitor-exit v3

    move-object v1, v0

    .end local v0    # "p":Lcom/sec/android/gallery3d/data/Path;
    .local v1, "p":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 61
    .end local v1    # "p":Ljava/lang/Object;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/Path;->mChildren:Lcom/sec/android/gallery3d/util/IdentityCache;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/util/IdentityCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/Path;

    .line 62
    .restart local v0    # "p":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v0, :cond_0

    monitor-exit v3

    move-object v1, v0

    .restart local v1    # "p":Ljava/lang/Object;
    goto :goto_0

    .line 68
    .end local v0    # "p":Lcom/sec/android/gallery3d/data/Path;
    .end local v1    # "p":Ljava/lang/Object;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getObject()Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 2

    .prologue
    .line 97
    const-class v1, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mObject:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mObject:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getParent()Lcom/sec/android/gallery3d/data/Path;
    .locals 2

    .prologue
    .line 72
    const-class v1, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    monitor-exit v1

    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    if-ne p0, v0, :cond_0

    const-string v0, ""

    .line 214
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->getPrefixPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPrefixPath()Lcom/sec/android/gallery3d/data/Path;
    .locals 4

    .prologue
    .line 218
    const-class v2, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v2

    .line 219
    move-object v0, p0

    .line 220
    .local v0, "current":Lcom/sec/android/gallery3d/data/Path;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    if-ne v0, v1, :cond_0

    .line 221
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 227
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 223
    :cond_0
    :goto_0
    :try_start_1
    iget-object v1, v0, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    sget-object v3, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    if-eq v1, v3, :cond_1

    .line 224
    iget-object v0, v0, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 226
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public getSuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    return-object v0
.end method

.method public getSuffix(I)Ljava/lang/String;
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 284
    move-object v1, p0

    .local v1, "p":Lcom/sec/android/gallery3d/data/Path;
    move v0, p1

    .line 285
    .end local p1    # "level":I
    .local v0, "level":I
    :goto_0
    add-int/lit8 p1, v0, -0x1

    .end local v0    # "level":I
    .restart local p1    # "level":I
    if-eqz v0, :cond_0

    .line 286
    iget-object v1, v1, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    move v0, p1

    .end local p1    # "level":I
    .restart local v0    # "level":I
    goto :goto_0

    .line 288
    .end local v0    # "level":I
    .restart local p1    # "level":I
    :cond_0
    iget-object v2, v1, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    return-object v2
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 3
    .param p1, "input"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 293
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "pathString":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 295
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v2, v0, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    .line 296
    iget-object v2, v0, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    .line 297
    return-void
.end method

.method public setObject(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 86
    const-class v1, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v1

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mObject:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mObject:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    monitor-exit v1

    .line 94
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mObject:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mObject:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/UriImage;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 92
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/Path;->mObject:Ljava/lang/ref/WeakReference;

    .line 93
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 90
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public split()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 136
    const-class v6, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v6

    .line 137
    const/4 v2, 0x0

    .line 138
    .local v2, "n":I
    move-object v3, p0

    .local v3, "p":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    :try_start_0
    sget-object v5, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    if-eq v3, v5, :cond_0

    .line 139
    add-int/lit8 v2, v2, 0x1

    .line 138
    iget-object v3, v3, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 141
    :cond_0
    new-array v4, v2, [Ljava/lang/String;

    .line 142
    .local v4, "segments":[Ljava/lang/String;
    add-int/lit8 v0, v2, -0x1

    .line 143
    .local v0, "i":I
    move-object v3, p0

    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_1
    sget-object v5, Lcom/sec/android/gallery3d/data/Path;->sRoot:Lcom/sec/android/gallery3d/data/Path;

    if-eq v3, v5, :cond_1

    .line 144
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    iget-object v5, v3, Lcom/sec/android/gallery3d/data/Path;->mSegment:Ljava/lang/String;

    aput-object v5, v4, v1

    .line 143
    iget-object v3, v3, Lcom/sec/android/gallery3d/data/Path;->mParent:Lcom/sec/android/gallery3d/data/Path;

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 146
    :cond_1
    monitor-exit v6

    return-object v4

    .line 147
    .end local v1    # "i":I
    .end local v4    # "segments":[Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 105
    const-class v4, Lcom/sec/android/gallery3d/data/Path;

    monitor-enter v4

    .line 106
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "segments":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 109
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    monitor-exit v4

    return-object v3

    .line 113
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "segments":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1
    .param p1, "output"    # Ljava/io/ObjectOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 302
    return-void
.end method
