.class public Lcom/sec/android/gallery3d/ui/ImageRotation;
.super Ljava/lang/Object;
.source "ImageRotation.java"


# static fields
.field private static imageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;


# instance fields
.field private final FINISH:I

.field private final INIT:I

.field private final PREINIT:I

.field private final PREPARED:I

.field private final STARTED:I

.field private degree:F

.field private duration:F

.field private endDegree:F

.field private isRotateClockWise:Z

.field private mCurrentScale:F

.field private mEndScale:F

.field private mStartScale:F

.field private rotationArray:[F

.field private rotationState:I

.field private sLastWidth:F

.field private startDegree:F

.field private startTime:F


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationArray:[F

    .line 13
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->PREINIT:I

    .line 14
    iput v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->INIT:I

    .line 15
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->PREPARED:I

    .line 16
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->STARTED:I

    .line 17
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->FINISH:I

    .line 20
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->isRotateClockWise:Z

    .line 21
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startDegree:F

    .line 22
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->endDegree:F

    .line 23
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startTime:F

    .line 24
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->duration:F

    .line 25
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->degree:F

    .line 26
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mStartScale:F

    .line 27
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mEndScale:F

    .line 28
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->sLastWidth:F

    .line 29
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mCurrentScale:F

    .line 38
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    .line 39
    return-void
.end method

.method private advanceAnimation()V
    .locals 10

    .prologue
    const v9, 0x40490fdc

    const/high16 v8, 0x40000000    # 2.0f

    .line 82
    iget v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 83
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 85
    .local v0, "now":J
    long-to-float v3, v0

    iget v4, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startTime:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->duration:F

    div-float v2, v3, v4

    .line 86
    .local v2, "progress":F
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    .line 87
    const/high16 v2, 0x3f800000    # 1.0f

    .line 88
    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    .line 90
    :cond_0
    iget v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startDegree:F

    iget v4, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->endDegree:F

    iget v5, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startDegree:F

    sub-float/2addr v4, v5

    mul-float v5, v9, v2

    div-float/2addr v5, v8

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->degree:F

    .line 91
    iget v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mStartScale:F

    iget v4, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mEndScale:F

    iget v5, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mStartScale:F

    sub-float/2addr v4, v5

    mul-float v5, v9, v2

    div-float/2addr v5, v8

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mCurrentScale:F

    .line 93
    .end local v0    # "now":J
    .end local v2    # "progress":F
    :cond_1
    return-void
.end method

.method public static getImageRotation()Lcom/sec/android/gallery3d/ui/ImageRotation;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/gallery3d/ui/ImageRotation;->imageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/sec/android/gallery3d/ui/ImageRotation;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/ImageRotation;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/ui/ImageRotation;->imageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    .line 34
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/ui/ImageRotation;->imageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    return-object v0
.end method

.method private getScale()F
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mCurrentScale:F

    return v0
.end method

.method private startAnimation(F)V
    .locals 2
    .param p1, "height"    # F

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 62
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    .line 64
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->isRotateClockWise:Z

    if-eqz v0, :cond_1

    .line 65
    const/high16 v0, -0x3d4c0000    # -90.0f

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startDegree:F

    .line 69
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->endDegree:F

    .line 70
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->sLastWidth:F

    div-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mStartScale:F

    .line 71
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->mEndScale:F

    .line 72
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    long-to-float v0, v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startTime:F

    .line 73
    const/high16 v0, 0x43c80000    # 400.0f

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->duration:F

    .line 75
    :cond_0
    return-void

    .line 67
    :cond_1
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->startDegree:F

    goto :goto_0
.end method


# virtual methods
.method public endRotation(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V
    .locals 0
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "isActive"    # Z

    .prologue
    .line 136
    if-eqz p3, :cond_0

    .line 137
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 138
    invoke-interface {p2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 140
    :cond_0
    return-void
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    return v0
.end method

.method public init(FZ)V
    .locals 1
    .param p1, "width"    # F
    .param p2, "clockwise"    # Z

    .prologue
    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    .line 48
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->isRotateClockWise:Z

    .line 49
    iput p1, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->sLastWidth:F

    .line 51
    return-void
.end method

.method public isAnimationStarted()Z
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 55
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepare()V
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    .line 44
    :cond_0
    return-void
.end method

.method public startRotation(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 13
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 98
    :cond_0
    const/4 v0, 0x0

    .line 128
    :goto_0
    return v0

    .line 100
    :cond_1
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 102
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v9

    .line 104
    .local v9, "pc":Lcom/sec/android/gallery3d/ui/PositionController;
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v7

    .line 105
    .local v7, "bounds":Landroid/graphics/Rect;
    iget v0, v7, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 106
    .local v8, "left":I
    iget v0, v7, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 107
    .local v10, "right":I
    iget v0, v7, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v12

    .line 108
    .local v12, "top":I
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 110
    .local v6, "bottom":I
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 111
    sub-int v0, v6, v12

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/ImageRotation;->startAnimation(F)V

    .line 112
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 115
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 116
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageRotation;->advanceAnimation()V

    .line 118
    add-int v0, v8, v10

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    add-int v1, v12, v6

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    .line 119
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p2, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyAlpha(F)V

    .line 120
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageRotation;->getScale()F

    move-result v11

    .line 121
    .local v11, "scale":F
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p2, v11, v11, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->scale(FFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationArray:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationArray:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->degree:F

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageRotation;->rotationArray:[F

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyMatrix([FI)V

    .line 126
    add-int v0, v8, v10

    neg-int v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    add-int v1, v12, v6

    neg-int v1, v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    .line 128
    .end local v11    # "scale":F
    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
