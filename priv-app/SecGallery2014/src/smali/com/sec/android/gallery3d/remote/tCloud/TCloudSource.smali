.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSource;
.source "TCloudSource.java"


# static fields
.field public static final ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field public static final MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final NO_MATCH:I = -0x1

.field protected static final TAG:Ljava/lang/String;

.field private static final TCLOUD_ALBUM:I = 0x1

.field private static final TCLOUD_ALBUMSET:I = 0x0

.field private static final TCLOUD_IMAGE_ALBUM:I = 0x2

.field private static final TCLOUD_IMAGE_ITEM:I = 0x4

.field private static final TCLOUD_MERGESET:I = 0x6

.field private static final TCLOUD_MERGE_ALL_ALBUM:I = 0x7

.field private static final TCLOUD_MERGE_IMAGE_ALBUM:I = 0x8

.field private static final TCLOUD_MERGE_VIDEO_ALBUM:I = 0x9

.field private static final TCLOUD_VIDEO_ALBUM:I = 0x3

.field private static final TCLOUD_VIDEO_ITEM:I = 0x5


# instance fields
.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->TAG:Ljava/lang/String;

    .line 58
    const-string v0, "/tCloud/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/tCloud/mergedall/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->TCLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 8
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x6

    const/4 v3, 0x0

    .line 64
    const-string/jumbo v0, "tCloud"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 45
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    .line 66
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 67
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/all"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/image"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/video"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/all/*"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/image/*"

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/video/*"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/mergedall"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/mergedimage"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/mergedvideo"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/mergedall/*"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/mergedimage/*"

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/mergedvideo/*"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/image/item/*"

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/video/item/*"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/all/*/*"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/image/*/*"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/tCloud/video/*/*"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->AUTHORITY:Ljava/lang/String;

    const-string v2, "images/#"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->AUTHORITY:Ljava/lang/String;

    const-string/jumbo v2, "videos/#"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    return-void
.end method

.method public static isTCloudImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p0, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 224
    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-eqz v0, :cond_0

    .line 225
    const/4 v0, 0x1

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V
    .locals 15
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    sget-object v13, Lcom/sec/android/gallery3d/data/LocalSource;->sIdComparator:Ljava/util/Comparator;

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 193
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 194
    .local v8, "n":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v8, :cond_1

    .line 195
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 197
    .local v9, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v4, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v13, v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 199
    .local v12, "startId":I
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v6, v3, 0x1

    .local v6, "j":I
    :goto_1
    if-ge v6, v8, :cond_0

    .line 202
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 203
    .local v10, "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v13, v10, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 204
    .local v2, "curId":I
    sub-int v13, v2, v12

    const/16 v14, 0x20

    if-lt v13, v14, :cond_2

    .line 210
    .end local v2    # "curId":I
    .end local v10    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_0
    if-eqz p3, :cond_3

    iget-object v13, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v13, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    .line 211
    .local v1, "baseUri":Landroid/net/Uri;
    :goto_2
    if-eqz p3, :cond_4

    iget-object v13, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getTCloudImageProjection()[Ljava/lang/String;

    move-result-object v11

    .line 212
    .local v11, "projection":[Ljava/lang/String;
    :goto_3
    iget-object v13, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move/from16 v0, p3

    invoke-static {v13, v0, v4, v1, v11}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;ZLjava/util/ArrayList;Landroid/net/Uri;[Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    .line 214
    .local v5, "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    move v7, v3

    .local v7, "k":I
    :goto_4
    if-ge v7, v6, :cond_6

    .line 215
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 216
    .restart local v10    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget v13, v10, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->id:I

    sub-int v14, v7, v3

    aget-object v14, v5, v14

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14}, Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;->consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 221
    .end local v1    # "baseUri":Landroid/net/Uri;
    .end local v4    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "j":I
    .end local v7    # "k":I
    .end local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .end local v10    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .end local v11    # "projection":[Ljava/lang/String;
    .end local v12    # "startId":I
    :cond_1
    return-void

    .line 207
    .restart local v2    # "curId":I
    .restart local v4    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v6    # "j":I
    .restart local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v10    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v12    # "startId":I
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 210
    .end local v2    # "curId":I
    .end local v10    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_3
    iget-object v13, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v13, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseVideoUri:Landroid/net/Uri;

    goto :goto_2

    .line 211
    .restart local v1    # "baseUri":Landroid/net/Uri;
    :cond_4
    iget-object v13, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getTCloudVideoProjection()[Ljava/lang/String;

    move-result-object v11

    goto :goto_3

    .line 214
    .restart local v5    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v7    # "k":I
    .restart local v10    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v11    # "projection":[Ljava/lang/String;
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 219
    .end local v10    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_6
    move v3, v6

    .line 220
    goto/16 :goto_0
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 12
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v11, 0x0

    .line 92
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 93
    .local v2, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 135
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bad path: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :pswitch_0
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 133
    :goto_0
    return-object v0

    .line 98
    :pswitch_1
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)V

    goto :goto_0

    .line 100
    :pswitch_2
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-object v1, p1

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)V

    goto :goto_0

    .line 102
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v6

    .line 103
    .local v6, "bucketId":I
    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 104
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 106
    .local v9, "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 108
    .local v10, "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v7, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 109
    .local v7, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    new-array v1, v1, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v9, v1, v11

    aput-object v10, v1, v4

    invoke-direct {v0, p1, v7, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 114
    .end local v6    # "bucketId":I
    .end local v7    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v9    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v10    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_4
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v3, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    invoke-direct {v0, p1, v1, v3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    goto :goto_0

    .line 116
    :pswitch_5
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v3, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    invoke-direct {v0, p1, v1, v3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    goto :goto_0

    .line 118
    :pswitch_6
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    goto :goto_0

    .line 120
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v6

    .line 121
    .restart local v6    # "bucketId":I
    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 122
    .restart local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 124
    .restart local v9    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 126
    .restart local v10    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v7, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 127
    .restart local v7    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    new-array v1, v1, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v9, v1, v11

    aput-object v10, v1, v4

    invoke-direct {v0, p1, v7, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto/16 :goto_0

    .line 131
    .end local v6    # "bucketId":I
    .end local v7    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v9    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v10    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_8
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)V

    goto/16 :goto_0

    .line 133
    :pswitch_9
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-object v1, p1

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)V

    goto/16 :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 152
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 165
    :cond_0
    :goto_0
    return-object v1

    .line 154
    :pswitch_0
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 155
    .local v2, "id":J
    cmp-long v4, v2, v6

    if-ltz v4, :cond_0

    sget-object v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 158
    .end local v2    # "id":J
    :pswitch_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 159
    .restart local v2    # "id":J
    cmp-long v4, v2, v6

    if-ltz v4, :cond_0

    sget-object v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 162
    .end local v2    # "id":J
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "uri: "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 141
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 142
    .local v1, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v2, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-eqz v2, :cond_1

    .line 143
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v2, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 144
    .local v0, "albumPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    check-cast v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;

    .end local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->getBucketId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 146
    .end local v0    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    return-object v2

    .line 143
    .restart local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 146
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V
    .locals 7
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v1, "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v5, "videoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 173
    .local v2, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 174
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 177
    .local v4, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v6, v4, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 178
    .local v3, "parent":Lcom/sec/android/gallery3d/data/Path;
    sget-object v6, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    if-ne v3, v6, :cond_1

    .line 179
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_1
    sget-object v6, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    if-ne v3, v6, :cond_0

    .line 181
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 184
    .end local v3    # "parent":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_2
    const/4 v6, 0x1

    invoke-direct {p0, v1, p2, v6}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V

    .line 185
    const/4 v6, 0x0

    invoke-direct {p0, v5, p2, v6}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudSource;->processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V

    .line 186
    return-void
.end method

.method public requestSyncAll()V
    .locals 0

    .prologue
    .line 233
    return-void
.end method
