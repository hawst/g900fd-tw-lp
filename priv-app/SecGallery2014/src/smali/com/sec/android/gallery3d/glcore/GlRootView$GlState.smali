.class public Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;
.super Ljava/lang/Object;
.source "GlRootView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlRootView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlState"
.end annotation


# instance fields
.field public mBlendType:I

.field public mBorderWidth:F

.field public mClipRect:[Landroid/graphics/Rect;

.field public mClipUsedCount:I

.field private mGL:Ljavax/microedition/khronos/opengles/GL11;

.field public mVertexRotation:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 1

    .prologue
    .line 2681
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2685
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mBorderWidth:F

    .line 2686
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipRect:[Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;
    .param p1, "x1"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 2681
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->reset(Ljavax/microedition/khronos/opengles/GL11;)V

    return-void
.end method

.method private reset(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 2
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const/4 v1, -0x1

    .line 2690
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 2691
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    .line 2692
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mBlendType:I

    .line 2693
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mVertexRotation:I

    .line 2694
    return-void
.end method


# virtual methods
.method public resetClipRect()V
    .locals 6

    .prologue
    .line 2712
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    .line 2713
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipRect:[Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 2714
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    if-nez v1, :cond_0

    .line 2715
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v2, 0xc11

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 2720
    :goto_0
    return-void

    .line 2717
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipRect:[Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v1, v2

    .line 2718
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glScissor(IIII)V

    goto :goto_0
.end method

.method public setBlendType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    const/16 v2, 0x303

    const/4 v1, 0x1

    .line 2723
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mBlendType:I

    .line 2724
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mBlendType:I

    if-ne v0, v1, :cond_0

    .line 2725
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x302

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBlendFunc(II)V

    .line 2729
    :goto_0
    return-void

    .line 2727
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBlendFunc(II)V

    goto :goto_0
.end method

.method public setBorderWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 2697
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mBorderWidth:F

    .line 2698
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, p1}, Ljavax/microedition/khronos/opengles/GL11;->glLineWidth(F)V

    .line 2699
    return-void
.end method

.method public setClipRect(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 2702
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    if-nez v0, :cond_0

    .line 2703
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0xc11

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 2705
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipRect:[Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mClipUsedCount:I

    aput-object p1, v0, v1

    .line 2706
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glScissor(IIII)V

    .line 2707
    return-void
.end method

.method public setVertexRotation(I)V
    .locals 5
    .param p1, "rotation"    # I

    .prologue
    const/16 v4, 0x1406

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 2732
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mVertexRotation:I

    .line 2733
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mVertexRotation:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_0

    .line 2734
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$1300(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer270:Ljava/nio/FloatBuffer;

    invoke-interface {v0, v3, v4, v2, v1}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 2742
    :goto_0
    return-void

    .line 2735
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mVertexRotation:I

    const/16 v1, 0xb4

    if-ne v0, v1, :cond_1

    .line 2736
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$1300(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer180:Ljava/nio/FloatBuffer;

    invoke-interface {v0, v3, v4, v2, v1}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIILjava/nio/Buffer;)V

    goto :goto_0

    .line 2737
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mVertexRotation:I

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_2

    .line 2738
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$1300(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer90:Ljava/nio/FloatBuffer;

    invoke-interface {v0, v3, v4, v2, v1}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIILjava/nio/Buffer;)V

    goto :goto_0

    .line 2740
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$1300(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {v0, v3, v4, v2, v1}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method
