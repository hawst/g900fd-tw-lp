.class Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;
.super Ljava/lang/Thread;
.source "PreDisplayScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/PreDisplayScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "prepareScreenNailTask"
.end annotation


# instance fields
.field mFilePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Ljava/lang/String;)V
    .locals 0
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    .line 102
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 103
    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 107
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    # getter for: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mIntent:Landroid/content/Intent;
    invoke-static {v5}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$000(Lcom/sec/android/gallery3d/util/PreDisplayScreen;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 108
    .local v3, "uri":Landroid/net/Uri;
    if-nez v3, :cond_0

    .line 109
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->setPrepareScreenNailResult(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$100(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Z)V

    .line 154
    :goto_0
    return-void

    .line 113
    :cond_0
    const/4 v8, 0x0

    .line 114
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 115
    .local v12, "fis":Ljava/io/FileInputStream;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    # getter for: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$200(Lcom/sec/android/gallery3d/util/PreDisplayScreen;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 116
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "orientation"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_data"

    aput-object v6, v4, v5

    .line 118
    .local v4, "projection":[Ljava/lang/String;
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_0
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 119
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 120
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    const/4 v6, 0x0

    invoke-interface {v8, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    # setter for: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mOrientation:I
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$302(Lcom/sec/android/gallery3d/util/PreDisplayScreen;I)I

    .line 121
    const/4 v5, 0x1

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->mFilePath:Ljava/lang/String;

    .line 123
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->mFilePath:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 124
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->mFilePath:Ljava/lang/String;

    invoke-direct {v11, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 125
    .local v11, "file":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v16, 0x3200000

    cmp-long v5, v6, v16

    if-lez v5, :cond_1

    .line 126
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->setPrepareScreenNailResult(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$100(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Z)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 149
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 131
    .end local v11    # "file":Ljava/io/File;
    :cond_1
    :try_start_1
    new-instance v13, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->mFilePath:Ljava/lang/String;

    invoke-direct {v13, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .local v13, "fis":Ljava/io/FileInputStream;
    :try_start_2
    invoke-virtual {v13}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    .line 133
    .local v10, "fd":Ljava/io/FileDescriptor;
    new-instance v14, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v14}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 134
    .local v14, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput-boolean v5, v14, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 135
    const/4 v5, 0x0

    invoke-static {v10, v5, v14}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 136
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    # getter for: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$200(Lcom/sec/android/gallery3d/util/PreDisplayScreen;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v15

    .line 137
    .local v15, "p":Landroid/graphics/Point;
    iget v5, v14, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v5, v5

    invoke-static {}, Lcom/sec/android/gallery3d/ui/PositionController;->getScaleLimit()F

    move-result v6

    mul-float/2addr v5, v6

    iget v6, v15, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    iget v5, v14, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v5, v5

    invoke-static {}, Lcom/sec/android/gallery3d/ui/PositionController;->getScaleLimit()F

    move-result v6

    mul-float/2addr v5, v6

    iget v6, v15, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 139
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->setPrepareScreenNailResult(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$100(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Z)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 148
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 149
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_2
    move-object v12, v13

    .line 148
    .end local v10    # "fd":Ljava/io/FileDescriptor;
    .end local v13    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v15    # "p":Landroid/graphics/Point;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    :cond_3
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 149
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 152
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->mFilePath:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v7

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v6, v7, v0}, Lcom/sec/android/gallery3d/data/DecodeUtils;->requestDecode(Ljava/lang/String;IZ)Landroid/graphics/Bitmap;

    move-result-object v6

    # setter for: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$402(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 153
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/util/PreDisplayScreen$prepareScreenNailTask;->this$0:Lcom/sec/android/gallery3d/util/PreDisplayScreen;

    # getter for: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$400(Lcom/sec/android/gallery3d/util/PreDisplayScreen;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    # invokes: Lcom/sec/android/gallery3d/util/PreDisplayScreen;->setPrepareScreenNailResult(Z)V
    invoke-static {v6, v5}, Lcom/sec/android/gallery3d/util/PreDisplayScreen;->access$100(Lcom/sec/android/gallery3d/util/PreDisplayScreen;Z)V

    goto/16 :goto_0

    .line 143
    :catch_0
    move-exception v9

    .line 144
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :goto_3
    :try_start_3
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 148
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 149
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 145
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v9

    .line 146
    .local v9, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 148
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 149
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 148
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    :goto_5
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 149
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5

    .line 153
    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    .line 148
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 145
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v9

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 143
    .end local v12    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v9

    move-object v12, v13

    .end local v13    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method
