.class public final Lcom/sec/android/gallery3d/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_dropdown_item_bg:I = 0x7f020000

.field public static final action_bar_icon_cancel:I = 0x7f020001

.field public static final action_bar_icon_cancel_disable:I = 0x7f020002

.field public static final action_bar_icon_cancel_press:I = 0x7f020003

.field public static final action_bar_icon_detail:I = 0x7f020004

.field public static final action_bar_icon_detail_disable:I = 0x7f020005

.field public static final action_bar_icon_detail_press:I = 0x7f020006

.field public static final action_bar_icon_home:I = 0x7f020007

.field public static final action_bar_icon_home_disable:I = 0x7f020008

.field public static final action_bar_icon_home_press:I = 0x7f020009

.field public static final action_bar_icon_play:I = 0x7f02000a

.field public static final action_bar_icon_save:I = 0x7f02000b

.field public static final action_bar_icon_show:I = 0x7f02000c

.field public static final action_bar_icon_turn:I = 0x7f02000d

.field public static final action_bar_icon_turn_disable:I = 0x7f02000e

.field public static final action_bar_icon_turn_press:I = 0x7f02000f

.field public static final action_bar_icon_up:I = 0x7f020010

.field public static final action_bar_icon_up_disable:I = 0x7f020011

.field public static final action_bar_icon_up_press:I = 0x7f020012

.field public static final action_bar_select_all_bg_selector:I = 0x7f020013

.field public static final action_bar_spinner_bg:I = 0x7f020014

.field public static final action_bar_spinner_bg_black:I = 0x7f020015

.field public static final action_bar_two_line_background:I = 0x7f020016

.field public static final actionbar_check_box_animator_list:I = 0x7f020017

.field public static final actionbar_ic_add_account:I = 0x7f020018

.field public static final actionbar_ic_add_folder:I = 0x7f020019

.field public static final actionbar_ic_allshare:I = 0x7f02001a

.field public static final actionbar_ic_allshare_disabled:I = 0x7f02001b

.field public static final actionbar_ic_allshare_on:I = 0x7f02001c

.field public static final actionbar_ic_camera:I = 0x7f02001d

.field public static final actionbar_ic_cancel:I = 0x7f02001e

.field public static final actionbar_ic_clipboard:I = 0x7f02001f

.field public static final actionbar_ic_copy:I = 0x7f020020

.field public static final actionbar_ic_create_motion_picture:I = 0x7f020021

.field public static final actionbar_ic_create_story_album:I = 0x7f020022

.field public static final actionbar_ic_delete:I = 0x7f020023

.field public static final actionbar_ic_details:I = 0x7f020024

.field public static final actionbar_ic_done:I = 0x7f020025

.field public static final actionbar_ic_edit:I = 0x7f020026

.field public static final actionbar_ic_favorite_off:I = 0x7f020027

.field public static final actionbar_ic_favorite_on:I = 0x7f020028

.field public static final actionbar_ic_favorite_refresh:I = 0x7f020029

.field public static final actionbar_ic_more:I = 0x7f02002a

.field public static final actionbar_ic_move:I = 0x7f02002b

.field public static final actionbar_ic_photo_note:I = 0x7f02002c

.field public static final actionbar_ic_rename:I = 0x7f02002d

.field public static final actionbar_ic_save:I = 0x7f02002e

.field public static final actionbar_ic_search:I = 0x7f02002f

.field public static final actionbar_ic_share:I = 0x7f020030

.field public static final actionbar_ic_slideshow:I = 0x7f020031

.field public static final actionbar_ic_smart_stay:I = 0x7f020032

.field public static final actionbar_ic_smart_stay_off:I = 0x7f020033

.field public static final actionbar_ic_sort_by:I = 0x7f020034

.field public static final actionbar_ic_view:I = 0x7f020035

.field public static final actionbar_ic_view_02:I = 0x7f020036

.field public static final actionbar_icon_list_close:I = 0x7f020037

.field public static final actionbar_icon_list_open:I = 0x7f020038

.field public static final actionbar_item_bg:I = 0x7f020039

.field public static final actionbar_item_ripple_bg:I = 0x7f02003a

.field public static final actionbar_translucent:I = 0x7f02003b

.field public static final actionbar_transparent_background:I = 0x7f02003c

.field public static final add_tag_change_selector:I = 0x7f02003d

.field public static final air_browse_try01:I = 0x7f02003e

.field public static final air_browse_try01_h:I = 0x7f02003f

.field public static final air_browse_try02:I = 0x7f020040

.field public static final air_browse_try02_h:I = 0x7f020041

.field public static final air_browse_try03:I = 0x7f020042

.field public static final air_browse_try03_h:I = 0x7f020043

.field public static final air_view_zoom_in_btn_icon_delete:I = 0x7f020044

.field public static final air_view_zoom_in_btn_icon_delete_focused:I = 0x7f020045

.field public static final air_view_zoom_in_btn_icon_delete_light:I = 0x7f020046

.field public static final air_view_zoom_in_btn_icon_delete_light_focused:I = 0x7f020047

.field public static final air_view_zoom_in_btn_icon_delete_light_pressed:I = 0x7f020048

.field public static final air_view_zoom_in_btn_icon_delete_pressed:I = 0x7f020049

.field public static final air_view_zoom_in_btn_icon_edit:I = 0x7f02004a

.field public static final air_view_zoom_in_btn_icon_edit_focused:I = 0x7f02004b

.field public static final air_view_zoom_in_btn_icon_edit_light:I = 0x7f02004c

.field public static final air_view_zoom_in_btn_icon_edit_light_focused:I = 0x7f02004d

.field public static final air_view_zoom_in_btn_icon_edit_light_pressed:I = 0x7f02004e

.field public static final air_view_zoom_in_btn_icon_edit_pressed:I = 0x7f02004f

.field public static final air_view_zoom_in_btn_icon_screenwrite:I = 0x7f020050

.field public static final air_view_zoom_in_btn_icon_screenwrite_focused:I = 0x7f020051

.field public static final air_view_zoom_in_btn_icon_screenwrite_light:I = 0x7f020052

.field public static final air_view_zoom_in_btn_icon_screenwrite_light_focused:I = 0x7f020053

.field public static final air_view_zoom_in_btn_icon_screenwrite_light_pressed:I = 0x7f020054

.field public static final air_view_zoom_in_btn_icon_screenwrite_pressed:I = 0x7f020055

.field public static final air_view_zoom_in_btn_icon_share:I = 0x7f020056

.field public static final air_view_zoom_in_btn_icon_share_focused:I = 0x7f020057

.field public static final air_view_zoom_in_btn_icon_share_light:I = 0x7f020058

.field public static final air_view_zoom_in_btn_icon_share_light_focused:I = 0x7f020059

.field public static final air_view_zoom_in_btn_icon_share_light_pressed:I = 0x7f02005a

.field public static final air_view_zoom_in_btn_icon_share_pressed:I = 0x7f02005b

.field public static final air_view_zoom_in_divider:I = 0x7f02005c

.field public static final airbutton_icon_crop:I = 0x7f02005d

.field public static final airbutton_icon_delete:I = 0x7f02005e

.field public static final airbutton_icon_edit:I = 0x7f02005f

.field public static final airbutton_icon_photoframe:I = 0x7f020060

.field public static final airbutton_icon_share:I = 0x7f020061

.field public static final airbutton_icon_trim:I = 0x7f020062

.field public static final all_focus:I = 0x7f020063

.field public static final all_focus_dim:I = 0x7f020064

.field public static final all_focus_focused:I = 0x7f020065

.field public static final all_focus_pressed:I = 0x7f020066

.field public static final all_focus_selected:I = 0x7f020067

.field public static final allshare_nearby_hhp_48:I = 0x7f020068

.field public static final appwidget_photo_border:I = 0x7f020069

.field public static final assistant_menu_icon_gallery_camera:I = 0x7f02006a

.field public static final assistant_menu_icon_gallery_slideshow:I = 0x7f02006b

.field public static final assistant_menu_icon_general_delete:I = 0x7f02006c

.field public static final assistant_menu_icon_general_drawer:I = 0x7f02006d

.field public static final bg_drawer_firsttime_helppopup:I = 0x7f02006e

.field public static final bg_drawer_firsttime_helppopup_right:I = 0x7f02006f

.field public static final bg_pressed:I = 0x7f020070

.field public static final bg_pressed_exit_fading:I = 0x7f020071

.field public static final bg_text_on_preview:I = 0x7f020072

.field public static final bg_vidcontrol:I = 0x7f020073

.field public static final block_selection:I = 0x7f020074

.field public static final bodyicon_bg_center:I = 0x7f020075

.field public static final bodyicon_bg_center_focus:I = 0x7f020076

.field public static final bodyicon_bg_center_press:I = 0x7f020077

.field public static final bodyicon_bg_focus:I = 0x7f020078

.field public static final bodyicon_bg_left:I = 0x7f020079

.field public static final bodyicon_bg_left_focus:I = 0x7f02007a

.field public static final bodyicon_bg_left_press:I = 0x7f02007b

.field public static final bodyicon_bg_press:I = 0x7f02007c

.field public static final bodyicon_bg_right:I = 0x7f02007d

.field public static final bodyicon_bg_right_focus:I = 0x7f02007e

.field public static final bodyicon_bg_right_press:I = 0x7f02007f

.field public static final border_photo_frame_widget:I = 0x7f020080

.field public static final border_photo_frame_widget_focused_holo:I = 0x7f020081

.field public static final border_photo_frame_widget_holo:I = 0x7f020082

.field public static final border_photo_frame_widget_pressed_holo:I = 0x7f020083

.field public static final btn_check_off_holo_dark:I = 0x7f020084

.field public static final btn_check_on_holo_dark:I = 0x7f020085

.field public static final btn_make_offline_disabled_on_holo_dark:I = 0x7f020086

.field public static final btn_make_offline_normal_off_holo_dark:I = 0x7f020087

.field public static final btn_make_offline_normal_on_holo_dark:I = 0x7f020088

.field public static final button_cancel:I = 0x7f020089

.field public static final camera_crop_edge_holo:I = 0x7f02008a

.field public static final camera_crop_holo:I = 0x7f02008b

.field public static final camera_guick_settings_line:I = 0x7f02008c

.field public static final camera_icon_for_no_item:I = 0x7f02008d

.field public static final checked_bitmap_color_list:I = 0x7f02008e

.field public static final clear_camera_close:I = 0x7f02008f

.field public static final clear_camera_close_focused:I = 0x7f020090

.field public static final clear_camera_close_pressed:I = 0x7f020091

.field public static final clear_camera_close_selected:I = 0x7f020092

.field public static final cloud_icon:I = 0x7f020093

.field public static final contact_btn_bg:I = 0x7f020094

.field public static final contact_btn_call:I = 0x7f020095

.field public static final contact_btn_center:I = 0x7f020096

.field public static final contact_btn_chat_chn:I = 0x7f020097

.field public static final contact_btn_email:I = 0x7f020098

.field public static final contact_btn_left:I = 0x7f020099

.field public static final contact_btn_msg:I = 0x7f02009a

.field public static final contact_btn_right:I = 0x7f02009b

.field public static final contact_btn_social:I = 0x7f02009c

.field public static final contact_btn_social_chn:I = 0x7f02009d

.field public static final contact_button_more:I = 0x7f02009e

.field public static final contact_button_more_bg:I = 0x7f02009f

.field public static final contacts_quick_bar:I = 0x7f0200a0

.field public static final contacts_quick_bar_line:I = 0x7f0200a1

.field public static final cover_close_btn:I = 0x7f0200a2

.field public static final cropimage_btn_save:I = 0x7f0200a3

.field public static final default_frame:I = 0x7f0200a4

.field public static final default_widget:I = 0x7f0200a5

.field public static final description_bubble_popup_04:I = 0x7f0200a6

.field public static final detail_image_error:I = 0x7f0200a7

.field public static final detail_video_error:I = 0x7f0200a8

.field public static final detail_view_handwriting_memo_icon_n:I = 0x7f0200a9

.field public static final detail_view_handwriting_memo_icon_p:I = 0x7f0200aa

.field public static final detailview_actionbar_shadow_bg:I = 0x7f0200ab

.field public static final drawer_3g4g:I = 0x7f0200ac

.field public static final drawer_3g4g_dim:I = 0x7f0200ad

.field public static final drawer_expandable_list_header_textcolor:I = 0x7f0200ae

.field public static final drawer_expandable_list_selector:I = 0x7f0200af

.field public static final drawer_expander_close_button:I = 0x7f0200b0

.field public static final drawer_expander_open_button:I = 0x7f0200b1

.field public static final drawer_list_filter_item_selector:I = 0x7f0200b2

.field public static final drawer_list_item_textcolor:I = 0x7f0200b3

.field public static final drawer_list_selector:I = 0x7f0200b4

.field public static final drawer_shadow:I = 0x7f0200b5

.field public static final drawer_wifi:I = 0x7f0200b6

.field public static final drawer_wifi_dim:I = 0x7f0200b7

.field public static final drawer_wlan:I = 0x7f0200b8

.field public static final drawer_wlan_dim:I = 0x7f0200b9

.field public static final dropdown_ic_arrow_normal_holo_dark:I = 0x7f0200ba

.field public static final dropdown_list_bg:I = 0x7f0200bb

.field public static final dropdown_text_color:I = 0x7f0200bc

.field public static final easymode_sw_btn_thumb_delete:I = 0x7f0200bd

.field public static final edit_textfield_selector:I = 0x7f0200be

.field public static final expand_button_close_selector:I = 0x7f0200bf

.field public static final expand_button_open_selector:I = 0x7f0200c0

.field public static final expander_close_selector:I = 0x7f0200c1

.field public static final expander_open_selector:I = 0x7f0200c2

.field public static final f_airview_infopreview_thumb_bg_light:I = 0x7f0200c3

.field public static final f_airview_popup_picker_bg_dark:I = 0x7f0200c4

.field public static final far_focus:I = 0x7f0200c5

.field public static final far_focus_dim:I = 0x7f0200c6

.field public static final far_focus_focused:I = 0x7f0200c7

.field public static final far_focus_pressed:I = 0x7f0200c8

.field public static final far_focus_selected:I = 0x7f0200c9

.field public static final filter_group_event_icon_selector:I = 0x7f0200ca

.field public static final filter_group_event_text_selector:I = 0x7f0200cb

.field public static final filter_group_location_icon_selector:I = 0x7f0200cc

.field public static final filter_group_location_text_selector:I = 0x7f0200cd

.field public static final filter_group_person_icon_selector:I = 0x7f0200ce

.field public static final filter_group_person_text_selector:I = 0x7f0200cf

.field public static final filter_group_time_icon_selector:I = 0x7f0200d0

.field public static final filter_group_time_text_selector:I = 0x7f0200d1

.field public static final filter_group_usertag_icon_selector:I = 0x7f0200d2

.field public static final filter_group_usertag_text_selector:I = 0x7f0200d3

.field public static final filter_list_item_bg:I = 0x7f0200d4

.field public static final filter_selected_item_bg_selector:I = 0x7f0200d5

.field public static final filter_selected_item_delete_btn_selector:I = 0x7f0200d6

.field public static final fingerprint_album_thumbnail_ic_fingerprint:I = 0x7f0200d7

.field public static final frame_overlay_gallery_baidu:I = 0x7f0200d8

.field public static final frame_overlay_gallery_cloud:I = 0x7f0200d9

.field public static final frame_overlay_gallery_facebook:I = 0x7f0200da

.field public static final frame_overlay_gallery_folder:I = 0x7f0200db

.field public static final frame_overlay_gallery_picasa:I = 0x7f0200dc

.field public static final frame_overlay_gallery_tcloud:I = 0x7f0200dd

.field public static final frame_overlay_gallery_unknow:I = 0x7f0200de

.field public static final gallery_2d_icon_normal:I = 0x7f0200df

.field public static final gallery_2d_icon_pressed:I = 0x7f0200e0

.field public static final gallery_3d_icon_normal:I = 0x7f0200e1

.field public static final gallery_3d_icon_pressed:I = 0x7f0200e2

.field public static final gallery_ab_textfield_focused:I = 0x7f0200e3

.field public static final gallery_ab_textfield_normal:I = 0x7f0200e4

.field public static final gallery_action_bar_ic_on_cancel:I = 0x7f0200e5

.field public static final gallery_actionbar_bg:I = 0x7f0200e6

.field public static final gallery_actionbar_ic_add:I = 0x7f0200e7

.field public static final gallery_actionbar_ic_allshare:I = 0x7f0200e8

.field public static final gallery_actionbar_ic_delete:I = 0x7f0200e9

.field public static final gallery_actionbar_ic_edit:I = 0x7f0200ea

.field public static final gallery_actionbar_ic_event:I = 0x7f0200eb

.field public static final gallery_actionbar_ic_marker:I = 0x7f0200ec

.field public static final gallery_actionbar_ic_memo:I = 0x7f0200ed

.field public static final gallery_actionbar_ic_more:I = 0x7f0200ee

.field public static final gallery_actionbar_ic_pen:I = 0x7f0200ef

.field public static final gallery_actionbar_ic_share:I = 0x7f0200f0

.field public static final gallery_actionbar_icon_list_close:I = 0x7f0200f1

.field public static final gallery_actionbar_icon_list_open:I = 0x7f0200f2

.field public static final gallery_actionbar_selected_bg:I = 0x7f0200f3

.field public static final gallery_actionbar_shadow_bg:I = 0x7f0200f4

.field public static final gallery_actionbar_slidshow_play:I = 0x7f0200f5

.field public static final gallery_actionbar_slidshow_play_press:I = 0x7f0200f6

.field public static final gallery_album_play:I = 0x7f0200f7

.field public static final gallery_album_play_dim:I = 0x7f0200f8

.field public static final gallery_album_reorder_stroke:I = 0x7f0200f9

.field public static final gallery_album_thumbnail_ic_3d:I = 0x7f0200fa

.field public static final gallery_album_thumbnail_ic_3d_panorama:I = 0x7f0200fb

.field public static final gallery_album_thumbnail_ic_3dtour:I = 0x7f0200fc

.field public static final gallery_album_thumbnail_ic_best_pic:I = 0x7f0200fd

.field public static final gallery_album_thumbnail_ic_bestface:I = 0x7f0200fe

.field public static final gallery_album_thumbnail_ic_bestphoto:I = 0x7f0200ff

.field public static final gallery_album_thumbnail_ic_burst_shot:I = 0x7f020100

.field public static final gallery_album_thumbnail_ic_burst_shot_folder:I = 0x7f020101

.field public static final gallery_album_thumbnail_ic_camera:I = 0x7f020102

.field public static final gallery_album_thumbnail_ic_camera_memory:I = 0x7f020103

.field public static final gallery_album_thumbnail_ic_camerasim:I = 0x7f020104

.field public static final gallery_album_thumbnail_ic_cinepic:I = 0x7f020105

.field public static final gallery_album_thumbnail_ic_cloud:I = 0x7f020106

.field public static final gallery_album_thumbnail_ic_drama:I = 0x7f020107

.field public static final gallery_album_thumbnail_ic_drawing:I = 0x7f020108

.field public static final gallery_album_thumbnail_ic_eraser:I = 0x7f020109

.field public static final gallery_album_thumbnail_ic_facebook:I = 0x7f02010a

.field public static final gallery_album_thumbnail_ic_focus_select:I = 0x7f02010b

.field public static final gallery_album_thumbnail_ic_folder:I = 0x7f02010c

.field public static final gallery_album_thumbnail_ic_folder_memory:I = 0x7f02010d

.field public static final gallery_album_thumbnail_ic_gif_ani:I = 0x7f02010e

.field public static final gallery_album_thumbnail_ic_google_drive:I = 0x7f02010f

.field public static final gallery_album_thumbnail_ic_new:I = 0x7f020110

.field public static final gallery_album_thumbnail_ic_panorama:I = 0x7f020111

.field public static final gallery_album_thumbnail_ic_pencil:I = 0x7f020112

.field public static final gallery_album_thumbnail_ic_personal:I = 0x7f020113

.field public static final gallery_album_thumbnail_ic_picasa:I = 0x7f020114

.field public static final gallery_album_thumbnail_ic_qq:I = 0x7f020115

.field public static final gallery_album_thumbnail_ic_renren:I = 0x7f020116

.field public static final gallery_album_thumbnail_ic_sequnce_shot:I = 0x7f020117

.field public static final gallery_album_thumbnail_ic_shot_more:I = 0x7f020118

.field public static final gallery_album_thumbnail_ic_sim:I = 0x7f020119

.field public static final gallery_album_thumbnail_ic_sina:I = 0x7f02011a

.field public static final gallery_album_thumbnail_ic_slideshow:I = 0x7f02011b

.field public static final gallery_album_thumbnail_ic_sound_scene:I = 0x7f02011c

.field public static final gallery_album_thumbnail_ic_tcloud:I = 0x7f02011d

.field public static final gallery_bg:I = 0x7f02011e

.field public static final gallery_btn_bg:I = 0x7f02011f

.field public static final gallery_btn_bg_pressed:I = 0x7f020120

.field public static final gallery_btn_check_off:I = 0x7f020121

.field public static final gallery_btn_check_on:I = 0x7f020122

.field public static final gallery_buddyphotoshare_add:I = 0x7f020123

.field public static final gallery_buddyphotoshare_add_press:I = 0x7f020124

.field public static final gallery_burst_play_menu_icon_selector:I = 0x7f020125

.field public static final gallery_button_bg_02:I = 0x7f020126

.field public static final gallery_button_bg_02_press:I = 0x7f020127

.field public static final gallery_category_delete:I = 0x7f020128

.field public static final gallery_category_delete_focus:I = 0x7f020129

.field public static final gallery_category_delete_press:I = 0x7f02012a

.field public static final gallery_communication_shortcut_btn_ic_setting:I = 0x7f02012b

.field public static final gallery_communication_shortcut_btn_ic_setting_press:I = 0x7f02012c

.field public static final gallery_communication_shortcut_button:I = 0x7f02012d

.field public static final gallery_communication_shortcut_button_press:I = 0x7f02012e

.field public static final gallery_communication_shortcut_picker_l:I = 0x7f02012f

.field public static final gallery_communication_shortcut_popup:I = 0x7f020130

.field public static final gallery_communication_shortcut_popup_02:I = 0x7f020131

.field public static final gallery_communication_shortcut_popup_4btn_bg:I = 0x7f020132

.field public static final gallery_communication_shortcut_popup_4btn_bg_pressed:I = 0x7f020133

.field public static final gallery_communication_shortcut_popup_4btn_c:I = 0x7f020134

.field public static final gallery_communication_shortcut_popup_4btn_c_press:I = 0x7f020135

.field public static final gallery_communication_shortcut_popup_4btn_l:I = 0x7f020136

.field public static final gallery_communication_shortcut_popup_4btn_l_press:I = 0x7f020137

.field public static final gallery_communication_shortcut_popup_4btn_r:I = 0x7f020138

.field public static final gallery_communication_shortcut_popup_4btn_r_press:I = 0x7f020139

.field public static final gallery_contextual_view_ic_category:I = 0x7f02013a

.field public static final gallery_contextual_view_ic_date:I = 0x7f02013b

.field public static final gallery_contextual_view_ic_location:I = 0x7f02013c

.field public static final gallery_contextual_view_ic_tags:I = 0x7f02013d

.field public static final gallery_copy_menu_icon_selector:I = 0x7f02013e

.field public static final gallery_detail_aperture:I = 0x7f02013f

.field public static final gallery_detail_exposure_time:I = 0x7f020140

.field public static final gallery_detail_flash:I = 0x7f020141

.field public static final gallery_detail_golf_play:I = 0x7f020142

.field public static final gallery_detail_ic_3d_panorama:I = 0x7f020143

.field public static final gallery_detail_ic_3d_panorama_press:I = 0x7f020144

.field public static final gallery_detail_ic_3dtour:I = 0x7f020145

.field public static final gallery_detail_ic_3dtour_press:I = 0x7f020146

.field public static final gallery_detail_ic_best_pic:I = 0x7f020147

.field public static final gallery_detail_ic_best_pic_press:I = 0x7f020148

.field public static final gallery_detail_ic_bestface:I = 0x7f020149

.field public static final gallery_detail_ic_bestface_press:I = 0x7f02014a

.field public static final gallery_detail_ic_bestphoto:I = 0x7f02014b

.field public static final gallery_detail_ic_bestphoto_press:I = 0x7f02014c

.field public static final gallery_detail_ic_camera:I = 0x7f02014d

.field public static final gallery_detail_ic_camera_press:I = 0x7f02014e

.field public static final gallery_detail_ic_cinepic:I = 0x7f02014f

.field public static final gallery_detail_ic_cinepic_press:I = 0x7f020150

.field public static final gallery_detail_ic_drama:I = 0x7f020151

.field public static final gallery_detail_ic_drama_press:I = 0x7f020152

.field public static final gallery_detail_ic_eraser:I = 0x7f020153

.field public static final gallery_detail_ic_eraser_press:I = 0x7f020154

.field public static final gallery_detail_ic_fingerprint:I = 0x7f020155

.field public static final gallery_detail_ic_focus_select:I = 0x7f020156

.field public static final gallery_detail_ic_focus_select_press:I = 0x7f020157

.field public static final gallery_detail_ic_sequnce_shot:I = 0x7f020158

.field public static final gallery_detail_ic_sequnce_shot_press:I = 0x7f020159

.field public static final gallery_detail_ic_shot_more:I = 0x7f02015a

.field public static final gallery_detail_ic_shot_more_press:I = 0x7f02015b

.field public static final gallery_detail_ic_slideshow:I = 0x7f02015c

.field public static final gallery_detail_ic_slideshow_press:I = 0x7f02015d

.field public static final gallery_detail_ic_slideshow_x02:I = 0x7f02015e

.field public static final gallery_detail_ic_slideshow_x02_press:I = 0x7f02015f

.field public static final gallery_detail_ic_slideshow_x05:I = 0x7f020160

.field public static final gallery_detail_ic_slideshow_x05_press:I = 0x7f020161

.field public static final gallery_detail_ic_slideshow_x10:I = 0x7f020162

.field public static final gallery_detail_ic_slideshow_x10_press:I = 0x7f020163

.field public static final gallery_detail_ic_slideshow_x20:I = 0x7f020164

.field public static final gallery_detail_ic_slideshow_x20_press:I = 0x7f020165

.field public static final gallery_detail_ic_slink_blueray_nor:I = 0x7f020166

.field public static final gallery_detail_ic_slink_blueray_press:I = 0x7f020167

.field public static final gallery_detail_ic_slink_camera_nor:I = 0x7f020168

.field public static final gallery_detail_ic_slink_camera_press:I = 0x7f020169

.field public static final gallery_detail_ic_slink_homesync_nor:I = 0x7f02016a

.field public static final gallery_detail_ic_slink_homesync_pivate_nor:I = 0x7f02016b

.field public static final gallery_detail_ic_slink_homesync_pivate_press:I = 0x7f02016c

.field public static final gallery_detail_ic_slink_homesync_press:I = 0x7f02016d

.field public static final gallery_detail_ic_slink_pc_nor:I = 0x7f02016e

.field public static final gallery_detail_ic_slink_pc_press:I = 0x7f02016f

.field public static final gallery_detail_ic_slink_phone_nor:I = 0x7f020170

.field public static final gallery_detail_ic_slink_phone_press:I = 0x7f020171

.field public static final gallery_detail_ic_slink_tab_nor:I = 0x7f020172

.field public static final gallery_detail_ic_slink_tab_press:I = 0x7f020173

.field public static final gallery_detail_ic_slink_tv_nor:I = 0x7f020174

.field public static final gallery_detail_ic_slink_tv_press:I = 0x7f020175

.field public static final gallery_detail_ic_slink_unknown_nor:I = 0x7f020176

.field public static final gallery_detail_ic_slink_unknown_press:I = 0x7f020177

.field public static final gallery_detail_ic_sound_scene:I = 0x7f020178

.field public static final gallery_detail_ic_sound_scene_press:I = 0x7f020179

.field public static final gallery_detail_iso:I = 0x7f02017a

.field public static final gallery_detail_slideshow_popup_ic_x02:I = 0x7f02017b

.field public static final gallery_detail_slideshow_popup_ic_x05:I = 0x7f02017c

.field public static final gallery_detail_slideshow_popup_ic_x10:I = 0x7f02017d

.field public static final gallery_detail_slideshow_popup_ic_x20:I = 0x7f02017e

.field public static final gallery_detail_view_bottom_camera:I = 0x7f02017f

.field public static final gallery_detail_view_ic_play:I = 0x7f020180

.field public static final gallery_detail_view_ic_play_press:I = 0x7f020181

.field public static final gallery_detail_view_scroll:I = 0x7f020182

.field public static final gallery_detail_view_thumbnail_focus:I = 0x7f020183

.field public static final gallery_detail_white_balance:I = 0x7f020184

.field public static final gallery_drawer_ic_album:I = 0x7f020185

.field public static final gallery_drawer_ic_document:I = 0x7f020186

.field public static final gallery_drawer_ic_documents:I = 0x7f020187

.field public static final gallery_drawer_ic_event:I = 0x7f020188

.field public static final gallery_drawer_ic_flowers:I = 0x7f020189

.field public static final gallery_drawer_ic_food:I = 0x7f02018a

.field public static final gallery_drawer_ic_people:I = 0x7f02018b

.field public static final gallery_drawer_ic_pets:I = 0x7f02018c

.field public static final gallery_drawer_ic_phone:I = 0x7f02018d

.field public static final gallery_drawer_ic_scenery:I = 0x7f02018e

.field public static final gallery_drawer_ic_selfie:I = 0x7f02018f

.field public static final gallery_drawer_ic_time:I = 0x7f020190

.field public static final gallery_drawer_ic_vehicles:I = 0x7f020191

.field public static final gallery_dropbox_img01:I = 0x7f020192

.field public static final gallery_dropbox_img02:I = 0x7f020193

.field public static final gallery_easy_album_play:I = 0x7f020194

.field public static final gallery_easy_album_play_dim:I = 0x7f020195

.field public static final gallery_easy_popup_handler:I = 0x7f020196

.field public static final gallery_easy_popup_handler_02:I = 0x7f020197

.field public static final gallery_easy_popup_handler_left:I = 0x7f020198

.field public static final gallery_easy_time_popup_01:I = 0x7f020199

.field public static final gallery_easy_time_popup_02:I = 0x7f02019a

.field public static final gallery_easy_time_scroll:I = 0x7f02019b

.field public static final gallery_easymode_album_thumbnail_ic_best_pic:I = 0x7f02019c

.field public static final gallery_easymode_album_thumbnail_ic_bestface:I = 0x7f02019d

.field public static final gallery_easymode_album_thumbnail_ic_bestphoto:I = 0x7f02019e

.field public static final gallery_easymode_album_thumbnail_ic_burst_shot:I = 0x7f02019f

.field public static final gallery_easymode_album_thumbnail_ic_camera:I = 0x7f0201a0

.field public static final gallery_easymode_album_thumbnail_ic_camerasim:I = 0x7f0201a1

.field public static final gallery_easymode_album_thumbnail_ic_cinepic:I = 0x7f0201a2

.field public static final gallery_easymode_album_thumbnail_ic_drama:I = 0x7f0201a3

.field public static final gallery_easymode_album_thumbnail_ic_eraser:I = 0x7f0201a4

.field public static final gallery_easymode_album_thumbnail_ic_focus_select:I = 0x7f0201a5

.field public static final gallery_easymode_album_thumbnail_ic_gif_ani:I = 0x7f0201a6

.field public static final gallery_easymode_album_thumbnail_ic_panorama:I = 0x7f0201a7

.field public static final gallery_easymode_album_thumbnail_ic_pencil:I = 0x7f0201a8

.field public static final gallery_easymode_album_thumbnail_ic_personal:I = 0x7f0201a9

.field public static final gallery_easymode_album_thumbnail_ic_sequnce_shot:I = 0x7f0201aa

.field public static final gallery_easymode_album_thumbnail_ic_shot_more:I = 0x7f0201ab

.field public static final gallery_easymode_album_thumbnail_ic_sim:I = 0x7f0201ac

.field public static final gallery_easymode_album_thumbnail_ic_sound_scene:I = 0x7f0201ad

.field public static final gallery_easymode_album_thumbnail_ic_virtual_tour:I = 0x7f0201ae

.field public static final gallery_easymode_cloud:I = 0x7f0201af

.field public static final gallery_easymode_facebook:I = 0x7f0201b0

.field public static final gallery_easymode_help_sharing_img_02:I = 0x7f0201b1

.field public static final gallery_easymode_help_viewing_img_01:I = 0x7f0201b2

.field public static final gallery_easymode_picasa:I = 0x7f0201b3

.field public static final gallery_easymode_tcloud:I = 0x7f0201b4

.field public static final gallery_edit_photoeditor:I = 0x7f0201b5

.field public static final gallery_event_play:I = 0x7f0201b6

.field public static final gallery_event_view_play_icon:I = 0x7f0201b7

.field public static final gallery_event_view_play_icon_press:I = 0x7f0201b8

.field public static final gallery_face_tagging_01:I = 0x7f0201b9

.field public static final gallery_face_tagging_02:I = 0x7f0201ba

.field public static final gallery_face_tagging_03:I = 0x7f0201bb

.field public static final gallery_face_tagging_04:I = 0x7f0201bc

.field public static final gallery_face_tagging_05:I = 0x7f0201bd

.field public static final gallery_face_tagging_06:I = 0x7f0201be

.field public static final gallery_face_tagging_07:I = 0x7f0201bf

.field public static final gallery_face_tagging_08:I = 0x7f0201c0

.field public static final gallery_face_tagging_list_press:I = 0x7f0201c1

.field public static final gallery_face_tagging_list_press_light:I = 0x7f0201c2

.field public static final gallery_face_tagging_popup:I = 0x7f0201c3

.field public static final gallery_face_tagging_shortcut_picker_l:I = 0x7f0201c4

.field public static final gallery_face_tagging_shortcut_picker_r:I = 0x7f0201c5

.field public static final gallery_filter_bg:I = 0x7f0201c6

.field public static final gallery_filter_grey:I = 0x7f0201c7

.field public static final gallery_filter_sepia:I = 0x7f0201c8

.field public static final gallery_filter_simple:I = 0x7f0201c9

.field public static final gallery_filter_vintage:I = 0x7f0201ca

.field public static final gallery_filtered_unknown:I = 0x7f0201cb

.field public static final gallery_frame_4_1_h:I = 0x7f0201cc

.field public static final gallery_frame_4_2_h:I = 0x7f0201cd

.field public static final gallery_frame_4_3_h:I = 0x7f0201ce

.field public static final gallery_help_ic_delete:I = 0x7f0201cf

.field public static final gallery_help_ic_drawer:I = 0x7f0201d0

.field public static final gallery_help_ic_more:I = 0x7f0201d1

.field public static final gallery_help_ic_share:I = 0x7f0201d2

.field public static final gallery_help_img_move_quickly:I = 0x7f0201d3

.field public static final gallery_help_img_move_quicklyi:I = 0x7f0201d4

.field public static final gallery_help_resizing_thumbnails_01:I = 0x7f0201d5

.field public static final gallery_help_sharing_img_01:I = 0x7f0201d6

.field public static final gallery_help_sharing_img_02:I = 0x7f0201d7

.field public static final gallery_help_try_image_01:I = 0x7f0201d8

.field public static final gallery_help_try_image_02:I = 0x7f0201d9

.field public static final gallery_help_try_image_03:I = 0x7f0201da

.field public static final gallery_help_viewing_img_01:I = 0x7f0201db

.field public static final gallery_ic_no_item:I = 0x7f0201dc

.field public static final gallery_ic_record:I = 0x7f0201dd

.field public static final gallery_ic_search:I = 0x7f0201de

.field public static final gallery_ic_search_01:I = 0x7f0201df

.field public static final gallery_ic_search_tablet:I = 0x7f0201e0

.field public static final gallery_icon_panorama:I = 0x7f0201e1

.field public static final gallery_icon_picasa:I = 0x7f0201e2

.field public static final gallery_info_button_01:I = 0x7f0201e3

.field public static final gallery_info_button_01_focused:I = 0x7f0201e4

.field public static final gallery_info_button_01_press:I = 0x7f0201e5

.field public static final gallery_info_button_02:I = 0x7f0201e6

.field public static final gallery_info_button_02_focused:I = 0x7f0201e7

.field public static final gallery_info_button_02_press:I = 0x7f0201e8

.field public static final gallery_info_delete_f:I = 0x7f0201e9

.field public static final gallery_info_delete_n:I = 0x7f0201ea

.field public static final gallery_info_delete_p:I = 0x7f0201eb

.field public static final gallery_info_ic_add_disabled:I = 0x7f0201ec

.field public static final gallery_info_ic_add_focus:I = 0x7f0201ed

.field public static final gallery_info_ic_add_nor:I = 0x7f0201ee

.field public static final gallery_info_ic_add_press:I = 0x7f0201ef

.field public static final gallery_info_ic_add_selected:I = 0x7f0201f0

.field public static final gallery_info_ic_addtag_disabled:I = 0x7f0201f1

.field public static final gallery_info_ic_addtag_focus:I = 0x7f0201f2

.field public static final gallery_info_ic_addtag_nor:I = 0x7f0201f3

.field public static final gallery_info_ic_addtag_press:I = 0x7f0201f4

.field public static final gallery_info_ic_addtag_selected:I = 0x7f0201f5

.field public static final gallery_info_ic_location_disabled:I = 0x7f0201f6

.field public static final gallery_info_ic_location_focus:I = 0x7f0201f7

.field public static final gallery_info_ic_location_nor:I = 0x7f0201f8

.field public static final gallery_info_ic_location_press:I = 0x7f0201f9

.field public static final gallery_info_ic_location_selected:I = 0x7f0201fa

.field public static final gallery_info_line_list:I = 0x7f0201fb

.field public static final gallery_info_line_title:I = 0x7f0201fc

.field public static final gallery_info_list_ic_category:I = 0x7f0201fd

.field public static final gallery_info_list_ic_date:I = 0x7f0201fe

.field public static final gallery_info_list_ic_details:I = 0x7f0201ff

.field public static final gallery_info_list_ic_location:I = 0x7f020200

.field public static final gallery_info_list_ic_marker:I = 0x7f020201

.field public static final gallery_info_list_ic_tags:I = 0x7f020202

.field public static final gallery_info_top_bg_press:I = 0x7f020203

.field public static final gallery_info_top_ic_location:I = 0x7f020204

.field public static final gallery_info_top_ic_search:I = 0x7f020205

.field public static final gallery_infor_icon:I = 0x7f020206

.field public static final gallery_information_frame:I = 0x7f020207

.field public static final gallery_left_split_album_count:I = 0x7f020208

.field public static final gallery_left_split_album_count_nine_patch:I = 0x7f020209

.field public static final gallery_left_split_selected_frame:I = 0x7f02020a

.field public static final gallery_left_split_white_theme_bg:I = 0x7f02020b

.field public static final gallery_locaiton_popup_bg:I = 0x7f02020c

.field public static final gallery_locaiton_popup_frame:I = 0x7f02020d

.field public static final gallery_location_mapview_ic_location:I = 0x7f02020e

.field public static final gallery_location_popup_picker:I = 0x7f02020f

.field public static final gallery_location_view_ic_location:I = 0x7f020210

.field public static final gallery_location_view_map_ic_01:I = 0x7f020211

.field public static final gallery_location_view_map_ic_02:I = 0x7f020212

.field public static final gallery_moreinfo_ic_album:I = 0x7f020213

.field public static final gallery_moreinfo_ic_autofilter:I = 0x7f020214

.field public static final gallery_moreinfo_ic_event:I = 0x7f020215

.field public static final gallery_moreinfo_ic_time:I = 0x7f020216

.field public static final gallery_move_bg_text_dark:I = 0x7f020217

.field public static final gallery_move_menu_icon_selector:I = 0x7f020218

.field public static final gallery_no_images:I = 0x7f020219

.field public static final gallery_one_move_bg_dark:I = 0x7f02021a

.field public static final gallery_popup_ic_event:I = 0x7f02021b

.field public static final gallery_popup_subtitle_bg:I = 0x7f02021c

.field public static final gallery_popup_tab_bg:I = 0x7f02021d

.field public static final gallery_recognition_box_01:I = 0x7f02021e

.field public static final gallery_recognition_box_02:I = 0x7f02021f

.field public static final gallery_rename_menu_icon_selector:I = 0x7f020220

.field public static final gallery_search_bar_focus:I = 0x7f020221

.field public static final gallery_search_bar_normal:I = 0x7f020222

.field public static final gallery_search_bar_press:I = 0x7f020223

.field public static final gallery_search_bar_select:I = 0x7f020224

.field public static final gallery_search_category_select_bg:I = 0x7f020225

.field public static final gallery_search_category_select_bg_focus:I = 0x7f020226

.field public static final gallery_search_category_select_bg_press:I = 0x7f020227

.field public static final gallery_search_divider_horizontal_dark:I = 0x7f020228

.field public static final gallery_search_fiter_bg_01:I = 0x7f020229

.field public static final gallery_search_icon_date:I = 0x7f02022a

.field public static final gallery_search_icon_date_color:I = 0x7f02022b

.field public static final gallery_search_icon_date_dim:I = 0x7f02022c

.field public static final gallery_search_icon_date_focus:I = 0x7f02022d

.field public static final gallery_search_icon_date_press:I = 0x7f02022e

.field public static final gallery_search_icon_event:I = 0x7f02022f

.field public static final gallery_search_icon_event_color:I = 0x7f020230

.field public static final gallery_search_icon_event_dim:I = 0x7f020231

.field public static final gallery_search_icon_event_focus:I = 0x7f020232

.field public static final gallery_search_icon_event_press:I = 0x7f020233

.field public static final gallery_search_icon_location:I = 0x7f020234

.field public static final gallery_search_icon_location_color:I = 0x7f020235

.field public static final gallery_search_icon_location_dim:I = 0x7f020236

.field public static final gallery_search_icon_location_focus:I = 0x7f020237

.field public static final gallery_search_icon_location_press:I = 0x7f020238

.field public static final gallery_search_icon_people:I = 0x7f020239

.field public static final gallery_search_icon_people_color:I = 0x7f02023a

.field public static final gallery_search_icon_people_dim:I = 0x7f02023b

.field public static final gallery_search_icon_people_focus:I = 0x7f02023c

.field public static final gallery_search_icon_people_press:I = 0x7f02023d

.field public static final gallery_search_icon_tag:I = 0x7f02023e

.field public static final gallery_search_icon_tag_color:I = 0x7f02023f

.field public static final gallery_search_icon_tag_dim:I = 0x7f020240

.field public static final gallery_search_icon_tag_focus:I = 0x7f020241

.field public static final gallery_search_icon_tag_press:I = 0x7f020242

.field public static final gallery_search_list_cancel:I = 0x7f020243

.field public static final gallery_search_list_cancel_press:I = 0x7f020244

.field public static final gallery_search_page_bg:I = 0x7f020245

.field public static final gallery_search_tag_list_icon:I = 0x7f020246

.field public static final gallery_search_updown:I = 0x7f020247

.field public static final gallery_search_voice:I = 0x7f020248

.field public static final gallery_search_white_bg:I = 0x7f020249

.field public static final gallery_selectall_actionbar_shadow_bg:I = 0x7f02024a

.field public static final gallery_selectall_actionbar_shadow_bg_selector:I = 0x7f02024b

.field public static final gallery_selected_check:I = 0x7f02024c

.field public static final gallery_selected_event_check:I = 0x7f02024d

.field public static final gallery_selected_zoom_out:I = 0x7f02024e

.field public static final gallery_sequence_ic_control_next:I = 0x7f02024f

.field public static final gallery_sequence_ic_control_next_focused:I = 0x7f020250

.field public static final gallery_sequence_ic_control_next_pressed:I = 0x7f020251

.field public static final gallery_sequence_ic_control_pause:I = 0x7f020252

.field public static final gallery_sequence_ic_control_pause_focused:I = 0x7f020253

.field public static final gallery_sequence_ic_control_pause_pressed:I = 0x7f020254

.field public static final gallery_sequence_ic_control_play:I = 0x7f020255

.field public static final gallery_sequence_ic_control_play_focused:I = 0x7f020256

.field public static final gallery_sequence_ic_control_play_pressed:I = 0x7f020257

.field public static final gallery_sequence_ic_control_prev:I = 0x7f020258

.field public static final gallery_sequence_ic_control_prev_focused:I = 0x7f020259

.field public static final gallery_sequence_ic_control_prev_pressed:I = 0x7f02025a

.field public static final gallery_sequence_ic_trim_pause:I = 0x7f02025b

.field public static final gallery_sequence_ic_trim_pause_focused:I = 0x7f02025c

.field public static final gallery_sequence_ic_trim_pause_pressed:I = 0x7f02025d

.field public static final gallery_sequence_trim_handler:I = 0x7f02025e

.field public static final gallery_sequence_trim_handler_focused:I = 0x7f02025f

.field public static final gallery_sequence_trim_handler_pressed:I = 0x7f020260

.field public static final gallery_sequence_trim_playing:I = 0x7f020261

.field public static final gallery_set_as_home_lockscreen:I = 0x7f020262

.field public static final gallery_set_as_homescreen:I = 0x7f020263

.field public static final gallery_set_as_lockscreen:I = 0x7f020264

.field public static final gallery_sideshow_bg:I = 0x7f020265

.field public static final gallery_slidesettings_ic_play:I = 0x7f020266

.field public static final gallery_slipon_bg_01:I = 0x7f020267

.field public static final gallery_slipon_ic_drag_in:I = 0x7f020268

.field public static final gallery_slipon_ic_drag_out:I = 0x7f020269

.field public static final gallery_split_arrow:I = 0x7f02026a

.field public static final gallery_split_arrow_white_theme:I = 0x7f02026b

.field public static final gallery_split_divider:I = 0x7f02026c

.field public static final gallery_split_drag_bg:I = 0x7f02026d

.field public static final gallery_split_select:I = 0x7f02026e

.field public static final gallery_ssearch_button:I = 0x7f02026f

.field public static final gallery_ssearch_button_focus:I = 0x7f020270

.field public static final gallery_ssearch_button_press:I = 0x7f020271

.field public static final gallery_ssearch_button_select:I = 0x7f020272

.field public static final gallery_ssearch_filed:I = 0x7f020273

.field public static final gallery_ssearch_filed_focus:I = 0x7f020274

.field public static final gallery_ssearch_list_ic_copy:I = 0x7f020275

.field public static final gallery_ssearch_list_ic_event:I = 0x7f020276

.field public static final gallery_ssearch_list_ic_location:I = 0x7f020277

.field public static final gallery_ssearch_list_ic_mode:I = 0x7f020278

.field public static final gallery_ssearch_list_ic_people:I = 0x7f020279

.field public static final gallery_ssearch_list_ic_tag:I = 0x7f02027a

.field public static final gallery_ssearch_list_ic_time:I = 0x7f02027b

.field public static final gallery_story_popup_01:I = 0x7f02027c

.field public static final gallery_sub_ic_marker:I = 0x7f02027d

.field public static final gallery_surround_shot_ic_contorl_pause:I = 0x7f02027e

.field public static final gallery_surround_shot_ic_contorl_play:I = 0x7f02027f

.field public static final gallery_tag_call_d:I = 0x7f020280

.field public static final gallery_tag_call_n:I = 0x7f020281

.field public static final gallery_tag_call_p:I = 0x7f020282

.field public static final gallery_tag_email_d:I = 0x7f020283

.field public static final gallery_tag_email_n:I = 0x7f020284

.field public static final gallery_tag_email_p:I = 0x7f020285

.field public static final gallery_tag_message_d:I = 0x7f020286

.field public static final gallery_tag_message_n:I = 0x7f020287

.field public static final gallery_tag_message_p:I = 0x7f020288

.field public static final gallery_tag_qzone_n:I = 0x7f020289

.field public static final gallery_tag_qzone_p:I = 0x7f02028a

.field public static final gallery_tag_social_d:I = 0x7f02028b

.field public static final gallery_tag_social_n:I = 0x7f02028c

.field public static final gallery_tag_social_p:I = 0x7f02028d

.field public static final gallery_tag_wechat_n:I = 0x7f02028e

.field public static final gallery_tag_wechat_p:I = 0x7f02028f

.field public static final gallery_thumb_golf_play:I = 0x7f020290

.field public static final gallery_thumbnail_focus:I = 0x7f020291

.field public static final gallery_time_popup_01:I = 0x7f020292

.field public static final gallery_time_popup_center:I = 0x7f020293

.field public static final gallery_time_scroll:I = 0x7f020294

.field public static final gallery_toolbar_brush:I = 0x7f020295

.field public static final gallery_toolbar_brush_dim:I = 0x7f020296

.field public static final gallery_toolbar_brush_press:I = 0x7f020297

.field public static final gallery_toolbar_btn_close:I = 0x7f020298

.field public static final gallery_toolbar_btn_close_press:I = 0x7f020299

.field public static final gallery_toolbar_btn_open:I = 0x7f02029a

.field public static final gallery_toolbar_btn_open_press:I = 0x7f02029b

.field public static final gallery_toolbar_cancel:I = 0x7f02029c

.field public static final gallery_toolbar_cancel_dim:I = 0x7f02029d

.field public static final gallery_toolbar_chinabrush:I = 0x7f02029e

.field public static final gallery_toolbar_chinabrush_dim:I = 0x7f02029f

.field public static final gallery_toolbar_chinabrush_press:I = 0x7f0202a0

.field public static final gallery_toolbar_divider:I = 0x7f0202a1

.field public static final gallery_toolbar_done:I = 0x7f0202a2

.field public static final gallery_toolbar_done_dim:I = 0x7f0202a3

.field public static final gallery_toolbar_eraser:I = 0x7f0202a4

.field public static final gallery_toolbar_eraser_dim:I = 0x7f0202a5

.field public static final gallery_toolbar_eraser_press:I = 0x7f0202a6

.field public static final gallery_toolbar_marker:I = 0x7f0202a7

.field public static final gallery_toolbar_marker_alpha:I = 0x7f0202a8

.field public static final gallery_toolbar_marker_alpha_dim:I = 0x7f0202a9

.field public static final gallery_toolbar_marker_alpha_press:I = 0x7f0202aa

.field public static final gallery_toolbar_marker_dim:I = 0x7f0202ab

.field public static final gallery_toolbar_marker_press:I = 0x7f0202ac

.field public static final gallery_toolbar_move:I = 0x7f0202ad

.field public static final gallery_toolbar_move_dim:I = 0x7f0202ae

.field public static final gallery_toolbar_move_press:I = 0x7f0202af

.field public static final gallery_toolbar_pen:I = 0x7f0202b0

.field public static final gallery_toolbar_pen_dim:I = 0x7f0202b1

.field public static final gallery_toolbar_pencil:I = 0x7f0202b2

.field public static final gallery_toolbar_pencil_dim:I = 0x7f0202b3

.field public static final gallery_toolbar_pencil_press:I = 0x7f0202b4

.field public static final gallery_toolbar_redo:I = 0x7f0202b5

.field public static final gallery_toolbar_redo_dim:I = 0x7f0202b6

.field public static final gallery_toolbar_redo_press:I = 0x7f0202b7

.field public static final gallery_toolbar_undo:I = 0x7f0202b8

.field public static final gallery_toolbar_undo_dim:I = 0x7f0202b9

.field public static final gallery_toolbar_undo_press:I = 0x7f0202ba

.field public static final gallery_try_popup1:I = 0x7f0202bb

.field public static final gallery_try_popup2:I = 0x7f0202bc

.field public static final gallery_view_by_menu_icon_selector:I = 0x7f0202bd

.field public static final google_tag_plus:I = 0x7f0202be

.field public static final grey_scale_l:I = 0x7f0202bf

.field public static final grid_pressed:I = 0x7f0202c0

.field public static final group_item_subtitle_btn_selector:I = 0x7f0202c1

.field public static final group_msg_setting_bg:I = 0x7f0202c2

.field public static final group_msg_setting_bg_02:I = 0x7f0202c3

.field public static final group_msg_setting_bg_02_h:I = 0x7f0202c4

.field public static final header_button_icon_knox_move_home:I = 0x7f0202c5

.field public static final header_button_icon_move_knox:I = 0x7f0202c6

.field public static final help_1:I = 0x7f0202c7

.field public static final help_2:I = 0x7f0202c8

.field public static final help_3:I = 0x7f0202c9

.field public static final help_air_button_hover:I = 0x7f0202ca

.field public static final help_album:I = 0x7f0202cb

.field public static final help_album_easy:I = 0x7f0202cc

.field public static final help_album_easy_land:I = 0x7f0202cd

.field public static final help_album_land:I = 0x7f0202ce

.field public static final help_cancel:I = 0x7f0202cf

.field public static final help_cue_swipe:I = 0x7f0202d0

.field public static final help_easymode_airbutton_04:I = 0x7f0202d1

.field public static final help_easymode_airbutton_04_popup:I = 0x7f0202d2

.field public static final help_easymode_airbutton_04_v:I = 0x7f0202d3

.field public static final help_face_detail_1:I = 0x7f0202d4

.field public static final help_face_detail_2:I = 0x7f0202d5

.field public static final help_face_detail_land_1:I = 0x7f0202d6

.field public static final help_face_detail_land_2:I = 0x7f0202d7

.field public static final help_face_photo:I = 0x7f0202d8

.field public static final help_face_photo_land:I = 0x7f0202d9

.field public static final help_hover:I = 0x7f0202da

.field public static final help_img_move_quickly:I = 0x7f0202db

.field public static final help_img_move_quickly_left:I = 0x7f0202dc

.field public static final help_infopreview:I = 0x7f0202dd

.field public static final help_infopreview_land:I = 0x7f0202de

.field public static final help_item:I = 0x7f0202df

.field public static final help_peek_detail:I = 0x7f0202e0

.field public static final help_peek_detail_land:I = 0x7f0202e1

.field public static final help_photo:I = 0x7f0202e2

.field public static final help_photo_land:I = 0x7f0202e3

.field public static final help_popup_picker_b_c:I = 0x7f0202e4

.field public static final help_popup_picker_bg_w_01:I = 0x7f0202e5

.field public static final help_popup_picker_t_c:I = 0x7f0202e6

.field public static final help_start_now_01:I = 0x7f0202e7

.field public static final help_start_now_02:I = 0x7f0202e8

.field public static final help_start_now_03:I = 0x7f0202e9

.field public static final help_start_now_04:I = 0x7f0202ea

.field public static final help_start_now_left_01:I = 0x7f0202eb

.field public static final help_start_now_left_02:I = 0x7f0202ec

.field public static final help_start_now_right_01:I = 0x7f0202ed

.field public static final help_start_now_right_02:I = 0x7f0202ee

.field public static final help_tap:I = 0x7f0202ef

.field public static final help_zoominout_cue:I = 0x7f0202f0

.field public static final help_zoomoutin_cue:I = 0x7f0202f1

.field public static final history_list_delete_all_selector:I = 0x7f0202f2

.field public static final home_lock:I = 0x7f0202f3

.field public static final home_wallpaper:I = 0x7f0202f4

.field public static final hover_album_re:I = 0x7f0202f5

.field public static final hover_frame:I = 0x7f0202f6

.field public static final ic_360pano_holo_light:I = 0x7f0202f7

.field public static final ic_cameraalbum_overlay:I = 0x7f0202f8

.field public static final ic_control_play:I = 0x7f0202f9

.field public static final ic_effects_holo_light:I = 0x7f0202fa

.field public static final ic_event:I = 0x7f0202fb

.field public static final ic_exposure_0:I = 0x7f0202fc

.field public static final ic_exposure_n1:I = 0x7f0202fd

.field public static final ic_exposure_n2:I = 0x7f0202fe

.field public static final ic_exposure_n3:I = 0x7f0202ff

.field public static final ic_exposure_p1:I = 0x7f020300

.field public static final ic_exposure_p2:I = 0x7f020301

.field public static final ic_exposure_p3:I = 0x7f020302

.field public static final ic_flash_auto_holo_light:I = 0x7f020303

.field public static final ic_flash_off_holo_light:I = 0x7f020304

.field public static final ic_flash_on_holo_light:I = 0x7f020305

.field public static final ic_gallery_play:I = 0x7f020306

.field public static final ic_gallery_play_big:I = 0x7f020307

.field public static final ic_gallery_thumb_play:I = 0x7f020308

.field public static final ic_hdr:I = 0x7f020309

.field public static final ic_hdr_off:I = 0x7f02030a

.field public static final ic_history_delete_selector:I = 0x7f02030b

.field public static final ic_indicator_wb_cloudy:I = 0x7f02030c

.field public static final ic_indicator_wb_daylight:I = 0x7f02030d

.field public static final ic_indicator_wb_fluorescent:I = 0x7f02030e

.field public static final ic_indicator_wb_off:I = 0x7f02030f

.field public static final ic_indicator_wb_tungsten:I = 0x7f020310

.field public static final ic_launcher_settings:I = 0x7f020311

.field public static final ic_location:I = 0x7f020312

.field public static final ic_location_off:I = 0x7f020313

.field public static final ic_logo_accuweather:I = 0x7f020314

.field public static final ic_logo_weathernews:I = 0x7f020315

.field public static final ic_menu_allshare:I = 0x7f020316

.field public static final ic_menu_allshare_refresh:I = 0x7f020317

.field public static final ic_menu_download:I = 0x7f020318

.field public static final ic_menu_edit_holo_dark:I = 0x7f020319

.field public static final ic_menu_export_light_tw:I = 0x7f02031a

.field public static final ic_menu_facetag_light_tw:I = 0x7f02031b

.field public static final ic_menu_forward_export_send:I = 0x7f02031c

.field public static final ic_menu_forward_export_send_dim:I = 0x7f02031d

.field public static final ic_menu_grid_light_tw:I = 0x7f02031e

.field public static final ic_menu_make_offline:I = 0x7f02031f

.field public static final ic_menu_moreoverflow_normal_holo_dark:I = 0x7f020320

.field public static final ic_menu_print_light_tw:I = 0x7f020321

.field public static final ic_menu_ptp_holo_light:I = 0x7f020322

.field public static final ic_menu_revert_holo_dark:I = 0x7f020323

.field public static final ic_menu_rotate_light_d:I = 0x7f020324

.field public static final ic_menu_rotate_light_n:I = 0x7f020325

.field public static final ic_menu_savephoto:I = 0x7f020326

.field public static final ic_menu_savephoto_disabled:I = 0x7f020327

.field public static final ic_menu_settings_light_tw:I = 0x7f020328

.field public static final ic_menu_share_light_tw:I = 0x7f020329

.field public static final ic_menu_show_light_tw:I = 0x7f02032a

.field public static final ic_menu_slideshow_light_tw:I = 0x7f02032b

.field public static final ic_menu_split:I = 0x7f02032c

.field public static final ic_menu_split_icon_cancle:I = 0x7f02032d

.field public static final ic_menu_tiny_planet:I = 0x7f02032e

.field public static final ic_menu_trash_light_tw:I = 0x7f02032f

.field public static final ic_menu_upload:I = 0x7f020330

.field public static final ic_next_depth:I = 0x7f020331

.field public static final ic_option_buddy_photo_share:I = 0x7f020332

.field public static final ic_option_crop:I = 0x7f020333

.field public static final ic_option_facetag:I = 0x7f020334

.field public static final ic_option_facetag_disabled:I = 0x7f020335

.field public static final ic_option_print:I = 0x7f020336

.field public static final ic_option_rotate_left:I = 0x7f020337

.field public static final ic_option_rotate_right:I = 0x7f020338

.field public static final ic_option_set_picture_as:I = 0x7f020339

.field public static final ic_option_settings:I = 0x7f02033a

.field public static final ic_option_settings_disabled:I = 0x7f02033b

.field public static final ic_option_video_album:I = 0x7f02033c

.field public static final ic_option_view_by:I = 0x7f02033d

.field public static final ic_option_view_by_disabled:I = 0x7f02033e

.field public static final ic_option_weathertag:I = 0x7f02033f

.field public static final ic_people:I = 0x7f020340

.field public static final ic_sce_action:I = 0x7f020341

.field public static final ic_sce_night:I = 0x7f020342

.field public static final ic_sce_off:I = 0x7f020343

.field public static final ic_sce_party:I = 0x7f020344

.field public static final ic_sce_sunset:I = 0x7f020345

.field public static final ic_switch_back:I = 0x7f020346

.field public static final ic_switch_front:I = 0x7f020347

.field public static final ic_tag:I = 0x7f020348

.field public static final ic_time:I = 0x7f020349

.field public static final ic_vidcontrol_pause:I = 0x7f02034a

.field public static final ic_vidcontrol_reload:I = 0x7f02034b

.field public static final ic_video_effects_background_fields_of_wheat_holo:I = 0x7f02034c

.field public static final ic_video_effects_background_intergalactic_holo:I = 0x7f02034d

.field public static final ic_video_effects_background_normal_holo_dark:I = 0x7f02034e

.field public static final ic_video_effects_faces_big_eyes_holo_dark:I = 0x7f02034f

.field public static final ic_video_effects_faces_big_mouth_holo_dark:I = 0x7f020350

.field public static final ic_video_effects_faces_big_nose_holo_dark:I = 0x7f020351

.field public static final ic_video_effects_faces_small_eyes_holo_dark:I = 0x7f020352

.field public static final ic_video_effects_faces_small_mouth_holo_dark:I = 0x7f020353

.field public static final ic_video_effects_faces_squeeze_holo_dark:I = 0x7f020354

.field public static final ic_video_thumb:I = 0x7f020355

.field public static final ic_view_photosphere:I = 0x7f020356

.field public static final ic_wb_auto:I = 0x7f020357

.field public static final ic_wb_cloudy:I = 0x7f020358

.field public static final ic_wb_fluorescent:I = 0x7f020359

.field public static final ic_wb_incandescent:I = 0x7f02035a

.field public static final ic_wb_sunlight:I = 0x7f02035b

.field public static final ic_wfd_audio:I = 0x7f02035c

.field public static final ic_wfd_camera:I = 0x7f02035d

.field public static final ic_wfd_computer:I = 0x7f02035e

.field public static final ic_wfd_displays:I = 0x7f02035f

.field public static final ic_wfd_game_deivces:I = 0x7f020360

.field public static final ic_wfd_input_device:I = 0x7f020361

.field public static final ic_wfd_multimedia:I = 0x7f020362

.field public static final ic_wfd_network_infra:I = 0x7f020363

.field public static final ic_wfd_printer:I = 0x7f020364

.field public static final ic_wfd_storage:I = 0x7f020365

.field public static final ic_wfd_telephone:I = 0x7f020366

.field public static final icn_media_play:I = 0x7f020367

.field public static final icn_media_play_focused_holo_dark:I = 0x7f020368

.field public static final icn_media_play_normal_holo_dark:I = 0x7f020369

.field public static final icn_media_play_pressed_holo_dark:I = 0x7f02036a

.field public static final icon_label_bg_1:I = 0x7f02036b

.field public static final icon_more_down:I = 0x7f02036c

.field public static final image_note_brush_btn:I = 0x7f02036d

.field public static final image_note_btn_bg:I = 0x7f02036e

.field public static final image_note_btn_bg_center:I = 0x7f02036f

.field public static final image_note_btn_bg_cue:I = 0x7f020370

.field public static final image_note_btn_bg_focus:I = 0x7f020371

.field public static final image_note_btn_bg_focus_center:I = 0x7f020372

.field public static final image_note_btn_bg_focus_cue:I = 0x7f020373

.field public static final image_note_btn_bg_focus_left:I = 0x7f020374

.field public static final image_note_btn_bg_focus_right:I = 0x7f020375

.field public static final image_note_btn_bg_left:I = 0x7f020376

.field public static final image_note_btn_bg_nocue:I = 0x7f020377

.field public static final image_note_btn_bg_press:I = 0x7f020378

.field public static final image_note_btn_bg_press_center:I = 0x7f020379

.field public static final image_note_btn_bg_press_cue:I = 0x7f02037a

.field public static final image_note_btn_bg_press_left:I = 0x7f02037b

.field public static final image_note_btn_bg_press_right:I = 0x7f02037c

.field public static final image_note_btn_bg_right:I = 0x7f02037d

.field public static final image_note_btn_devider:I = 0x7f02037e

.field public static final image_note_cancel_btn:I = 0x7f02037f

.field public static final image_note_chinabrush_btn:I = 0x7f020380

.field public static final image_note_close_btn:I = 0x7f020381

.field public static final image_note_close_btn_bg:I = 0x7f020382

.field public static final image_note_close_btn_bg_press:I = 0x7f020383

.field public static final image_note_close_tool_bar_bg:I = 0x7f020384

.field public static final image_note_done_btn:I = 0x7f020385

.field public static final image_note_done_btn_focus:I = 0x7f020386

.field public static final image_note_done_btn_press:I = 0x7f020387

.field public static final image_note_draw_btn:I = 0x7f020388

.field public static final image_note_eraser_btn:I = 0x7f020389

.field public static final image_note_ic_pen_type_cue:I = 0x7f02038a

.field public static final image_note_marker_alpha_btn:I = 0x7f02038b

.field public static final image_note_marker_btn:I = 0x7f02038c

.field public static final image_note_move_btn:I = 0x7f02038d

.field public static final image_note_open_btn:I = 0x7f02038e

.field public static final image_note_open_tool_bar_bg:I = 0x7f02038f

.field public static final image_note_pen_btn:I = 0x7f020390

.field public static final image_note_pencil_btn:I = 0x7f020391

.field public static final image_note_redo_btn:I = 0x7f020392

.field public static final image_note_toolbar_bg:I = 0x7f020393

.field public static final image_note_undo_btn:I = 0x7f020394

.field public static final improvements_list_line:I = 0x7f020395

.field public static final ingest_item_list_selector:I = 0x7f020396

.field public static final intro_image_02:I = 0x7f020397

.field public static final list_divider_holo_dark:I = 0x7f020398

.field public static final list_item_bg_selector:I = 0x7f020399

.field public static final list_pressed_holo_light:I = 0x7f02039a

.field public static final list_scroll_bg:I = 0x7f02039b

.field public static final list_scroll_bg_land:I = 0x7f02039c

.field public static final list_selector_background_selected:I = 0x7f02039d

.field public static final livepanel_gallery_play_focus:I = 0x7f02039e

.field public static final livepanel_gallery_play_normal:I = 0x7f02039f

.field public static final livepanel_gallery_play_press:I = 0x7f0203a0

.field public static final location_icon_pin:I = 0x7f0203a1

.field public static final location_touched_icon_pin:I = 0x7f0203a2

.field public static final lock_wallpaper:I = 0x7f0203a3

.field public static final magazine_gallery_default:I = 0x7f0203a4

.field public static final magazine_gallery_preview:I = 0x7f0203a5

.field public static final magic_photo_focus_01:I = 0x7f0203a6

.field public static final magic_photo_focus_02:I = 0x7f0203a7

.field public static final main_subtitle_bg:I = 0x7f0203a8

.field public static final mainmenu_icon_gallery:I = 0x7f0203a9

.field public static final mainmenu_icon_gallery_no_item:I = 0x7f0203aa

.field public static final mainpage_btn_focus:I = 0x7f0203ab

.field public static final mainpage_btn_normal:I = 0x7f0203ac

.field public static final mainpage_btn_press:I = 0x7f0203ad

.field public static final mainpage_btn_select:I = 0x7f0203ae

.field public static final memo:I = 0x7f0203af

.field public static final memo_multi:I = 0x7f0203b0

.field public static final menu_dropdown_panel_holo_dark:I = 0x7f0203b1

.field public static final menu_icon_allshare_selector:I = 0x7f0203b2

.field public static final menu_icon_back_selector:I = 0x7f0203b3

.field public static final menu_icon_camera_selector:I = 0x7f0203b4

.field public static final menu_icon_cancel_selector:I = 0x7f0203b5

.field public static final menu_icon_cancel_selector_best_pic:I = 0x7f0203b6

.field public static final menu_icon_cancel_selector_details:I = 0x7f0203b7

.field public static final menu_icon_details_selector:I = 0x7f0203b8

.field public static final menu_icon_done_selector:I = 0x7f0203b9

.field public static final menu_icon_done_selector_best_pic:I = 0x7f0203ba

.field public static final menu_icon_home_selector:I = 0x7f0203bb

.field public static final menu_icon_list_selector:I = 0x7f0203bc

.field public static final menu_icon_manual_rotate_selector:I = 0x7f0203bd

.field public static final menu_icon_new_album_selector:I = 0x7f0203be

.field public static final menu_icon_play_selector:I = 0x7f0203bf

.field public static final menu_icon_search_selector:I = 0x7f0203c0

.field public static final menu_icon_up_selector:I = 0x7f0203c1

.field public static final menu_popup_ic_switch_windows_holo_dark:I = 0x7f0203c2

.field public static final menu_save_photo:I = 0x7f0203c3

.field public static final message_bubble_bg_add:I = 0x7f0203c4

.field public static final message_center_bg:I = 0x7f0203c5

.field public static final message_divider:I = 0x7f0203c6

.field public static final mg_camera_w:I = 0x7f0203c7

.field public static final mg_camera_w_press:I = 0x7f0203c8

.field public static final mg_setting_w:I = 0x7f0203c9

.field public static final mg_setting_w_press:I = 0x7f0203ca

.field public static final moreinfo_delete_bg_selector:I = 0x7f0203cb

.field public static final moreinfo_delete_selector:I = 0x7f0203cc

.field public static final moreinfo_details_button_bg_selector:I = 0x7f0203cd

.field public static final moreinfo_details_histogram_border:I = 0x7f0203ce

.field public static final moreinfo_divider:I = 0x7f0203cf

.field public static final moreinfo_expander_header_selector:I = 0x7f0203d0

.field public static final moreinfo_gallery_info_button_02_selector:I = 0x7f0203d1

.field public static final moreinfo_location_editor_ic_selector:I = 0x7f0203d2

.field public static final moreinfo_location_editor_selector:I = 0x7f0203d3

.field public static final moreinfo_location_view_ic_selector:I = 0x7f0203d4

.field public static final moreinfo_tag_add_selector:I = 0x7f0203d5

.field public static final moreinfo_tag_change_selector:I = 0x7f0203d6

.field public static final moreinfo_tw_list_icon_create_holo_dark_selector:I = 0x7f0203d7

.field public static final moreinfo_usertag_edit_header_selector:I = 0x7f0203d8

.field public static final motion_browsing_panning_01:I = 0x7f0203d9

.field public static final motion_browsing_panning_02:I = 0x7f0203da

.field public static final motion_browsing_panning_03:I = 0x7f0203db

.field public static final motion_browsing_panning_05:I = 0x7f0203dc

.field public static final motion_browsing_panning_07:I = 0x7f0203dd

.field public static final motion_header_icon:I = 0x7f0203de

.field public static final motion_moving_zoom_01:I = 0x7f0203df

.field public static final motion_moving_zoom_02:I = 0x7f0203e0

.field public static final motion_moving_zoom_03:I = 0x7f0203e1

.field public static final motion_sensitivity_bg:I = 0x7f0203e2

.field public static final motion_view_album_list_01:I = 0x7f0203e3

.field public static final motion_view_album_list_02:I = 0x7f0203e4

.field public static final motion_view_album_list_03:I = 0x7f0203e5

.field public static final movie_drm:I = 0x7f0203e6

.field public static final multi_edit_textfield_selector:I = 0x7f0203e7

.field public static final mux_settings_ic_gallery:I = 0x7f0203e8

.field public static final mux_settings_personal_gallery:I = 0x7f0203e9

.field public static final mux_settings_personal_gallery_dim:I = 0x7f0203ea

.field public static final near_focus:I = 0x7f0203eb

.field public static final near_focus_dim:I = 0x7f0203ec

.field public static final near_focus_focused:I = 0x7f0203ed

.field public static final near_focus_pressed:I = 0x7f0203ee

.field public static final near_focus_selected:I = 0x7f0203ef

.field public static final nearby_unknown_device:I = 0x7f0203f0

.field public static final none_l:I = 0x7f0203f1

.field public static final option_popup_icon_help:I = 0x7f0203f2

.field public static final overlay_help_button:I = 0x7f0203f3

.field public static final overlay_help_button_focused:I = 0x7f0203f4

.field public static final overlay_help_button_normal:I = 0x7f0203f5

.field public static final overlay_help_button_pressed:I = 0x7f0203f6

.field public static final overlay_help_point_up:I = 0x7f0203f7

.field public static final overlay_selective_focus_help_popup:I = 0x7f0203f8

.field public static final overscroll_edge:I = 0x7f0203f9

.field public static final overscroll_glow:I = 0x7f0203fa

.field public static final panel_undo_holo:I = 0x7f0203fb

.field public static final photo_bg_l:I = 0x7f0203fc

.field public static final photo_bg_v:I = 0x7f0203fd

.field public static final photo_crop_handler:I = 0x7f0203fe

.field public static final photo_crop_handler_press:I = 0x7f0203ff

.field public static final photo_reader_actionbar_detect_text:I = 0x7f020400

.field public static final photopage_bottom_button_background:I = 0x7f020401

.field public static final pic_frame_bg:I = 0x7f020402

.field public static final pic_magazine_camera_icon_selector:I = 0x7f020403

.field public static final pic_magazine_setting_icon_selector:I = 0x7f020404

.field public static final pic_setting_icon_nor:I = 0x7f020405

.field public static final pic_setting_icon_press:I = 0x7f020406

.field public static final pic_setting_icon_selector:I = 0x7f020407

.field public static final pictureframe_blank_bg:I = 0x7f020408

.field public static final pictureframe_blank_btn_add:I = 0x7f020409

.field public static final pictureframe_blank_btn_add_normal:I = 0x7f02040a

.field public static final pictureframe_blank_btn_add_press:I = 0x7f02040b

.field public static final pictureframe_easy_icon:I = 0x7f02040c

.field public static final pictureframe_h_divider:I = 0x7f02040d

.field public static final pictureframe_v_divider:I = 0x7f02040e

.field public static final placeholder_camera:I = 0x7f02040f

.field public static final placeholder_empty:I = 0x7f020410

.field public static final placeholder_locked:I = 0x7f020411

.field public static final point_bg_popup_bottm:I = 0x7f020412

.field public static final point_bg_popup_title:I = 0x7f020413

.field public static final point_ic_addtocontact:I = 0x7f020414

.field public static final point_ic_bookmark:I = 0x7f020415

.field public static final point_ic_call:I = 0x7f020416

.field public static final point_ic_copy:I = 0x7f020417

.field public static final point_ic_email:I = 0x7f020418

.field public static final point_ic_message:I = 0x7f020419

.field public static final point_ic_share:I = 0x7f02041a

.field public static final point_ic_url:I = 0x7f02041b

.field public static final point_om_ic_search_by_text:I = 0x7f02041c

.field public static final point_popup_ic_cancel:I = 0x7f02041d

.field public static final point_popup_ic_cancel_disable:I = 0x7f02041e

.field public static final point_popup_ic_cancel_press:I = 0x7f02041f

.field public static final point_popup_ic_cancel_selector:I = 0x7f020420

.field public static final popup_add_tag:I = 0x7f020421

.field public static final popup_add_tag_disabled:I = 0x7f020422

.field public static final popup_add_tag_disalbed:I = 0x7f020423

.field public static final popup_add_tag_focus:I = 0x7f020424

.field public static final popup_add_tag_nor:I = 0x7f020425

.field public static final popup_add_tag_press:I = 0x7f020426

.field public static final popup_detail_ic_bg:I = 0x7f020427

.field public static final popup_detail_ic_diapharagm:I = 0x7f020428

.field public static final popup_detail_ic_flash:I = 0x7f020429

.field public static final popup_detail_ic_iso:I = 0x7f02042a

.field public static final popup_detail_ic_shutter:I = 0x7f02042b

.field public static final popup_detail_ic_wb:I = 0x7f02042c

.field public static final popup_folder_icon:I = 0x7f02042d

.field public static final popup_folder_sd:I = 0x7f02042e

.field public static final popup_sd_icon:I = 0x7f02042f

.field public static final popup_tag_change_selector:I = 0x7f020430

.field public static final prefocus:I = 0x7f020431

.field public static final preview_easy_widget:I = 0x7f020432

.field public static final previewmode_img:I = 0x7f020433

.field public static final scanvas_bg:I = 0x7f020434

.field public static final scrubber_knob:I = 0x7f020435

.field public static final search_ic_event:I = 0x7f020436

.field public static final search_ic_location:I = 0x7f020437

.field public static final search_ic_people:I = 0x7f020438

.field public static final search_ic_tag:I = 0x7f020439

.field public static final search_ic_time:I = 0x7f02043a

.field public static final search_textfield_bg_selector:I = 0x7f02043b

.field public static final security_home_icon_security:I = 0x7f02043c

.field public static final security_home_icon_security_02:I = 0x7f02043d

.field public static final selective_focus_help_popup:I = 0x7f02043e

.field public static final sepia_l:I = 0x7f02043f

.field public static final setting_picker:I = 0x7f020440

.field public static final sfinder_search_ic_cancel:I = 0x7f020441

.field public static final sfinder_search_ic_mic:I = 0x7f020442

.field public static final sfinder_textfield_search_default:I = 0x7f020443

.field public static final sfinder_textfield_search_focused:I = 0x7f020444

.field public static final sim_1_l_v2:I = 0x7f020445

.field public static final sim_2_l_v2:I = 0x7f020446

.field public static final sim_call_l_v2:I = 0x7f020447

.field public static final sim_heart_l_v2:I = 0x7f020448

.field public static final sim_home_l_v2:I = 0x7f020449

.field public static final sim_internet_l_v2:I = 0x7f02044a

.field public static final sim_mms_l_v2:I = 0x7f02044b

.field public static final sim_office_l_v2:I = 0x7f02044c

.field public static final sim_sms_l_v2:I = 0x7f02044d

.field public static final simgle_picture_frame:I = 0x7f02044e

.field public static final simple_slide_item:I = 0x7f02044f

.field public static final slideshow_button_selector:I = 0x7f020450

.field public static final slideshow_resume_button_selector:I = 0x7f020451

.field public static final slidshow_actionbar_bg:I = 0x7f020452

.field public static final sns_button_light_bg:I = 0x7f020453

.field public static final sns_button_selector_default:I = 0x7f020454

.field public static final sns_comment_activity_title_btn_bg:I = 0x7f020455

.field public static final sns_comment_tab_bg:I = 0x7f020456

.field public static final sns_icon_qq:I = 0x7f020457

.field public static final sns_icon_renren:I = 0x7f020458

.field public static final sns_icon_sina:I = 0x7f020459

.field public static final sns_popup_edittext_bg:I = 0x7f02045a

.field public static final sns_popup_send_bg:I = 0x7f02045b

.field public static final sns_thumnail_bg:I = 0x7f02045c

.field public static final spinner_76_inner_holo:I = 0x7f02045d

.field public static final spinner_76_outer_holo:I = 0x7f02045e

.field public static final split_ic_event:I = 0x7f02045f

.field public static final split_ic_event_select:I = 0x7f020460

.field public static final split_ic_event_selector:I = 0x7f020461

.field public static final split_ic_location:I = 0x7f020462

.field public static final split_ic_location_select:I = 0x7f020463

.field public static final split_ic_location_selector:I = 0x7f020464

.field public static final split_ic_people:I = 0x7f020465

.field public static final split_ic_people_select:I = 0x7f020466

.field public static final split_ic_people_selector:I = 0x7f020467

.field public static final split_ic_tag:I = 0x7f020468

.field public static final split_ic_tag_select:I = 0x7f020469

.field public static final split_ic_tag_selector:I = 0x7f02046a

.field public static final split_ic_time:I = 0x7f02046b

.field public static final split_ic_time_select:I = 0x7f02046c

.field public static final split_ic_time_selector:I = 0x7f02046d

.field public static final ssearch_field:I = 0x7f02046e

.field public static final ssearch_tag_btn:I = 0x7f02046f

.field public static final ssearch_tag_btn_tablet:I = 0x7f020470

.field public static final ssearch_tag_btn_tablet_list:I = 0x7f020471

.field public static final start_now_left_far_ani:I = 0x7f020472

.field public static final start_now_left_near_ani:I = 0x7f020473

.field public static final start_now_right_far_ani:I = 0x7f020474

.field public static final start_now_right_near_ani:I = 0x7f020475

.field public static final stat_sys_hdmi:I = 0x7f020476

.field public static final sw_btn_add:I = 0x7f020477

.field public static final sw_btn_thumb_delete:I = 0x7f020478

.field public static final tab_selected_focused_holo:I = 0x7f020479

.field public static final tab_selected_holo:I = 0x7f02047a

.field public static final tab_selected_pressed_holo:I = 0x7f02047b

.field public static final tab_unselected_pressed_holo:I = 0x7f02047c

.field public static final tab_widget_btn:I = 0x7f02047d

.field public static final tab_widget_btn_white:I = 0x7f02047e

.field public static final tag_buddy_textcolor:I = 0x7f02047f

.field public static final tag_focus:I = 0x7f020480

.field public static final tag_list_divider:I = 0x7f020481

.field public static final tag_press:I = 0x7f020482

.field public static final tag_scrollview_horizontal_style:I = 0x7f020483

.field public static final temp_checkbox_selector:I = 0x7f020484

.field public static final text_cursor_mtrl_alpha:I = 0x7f020485

.field public static final text_select_handle_left:I = 0x7f020486

.field public static final text_select_handle_right:I = 0x7f020487

.field public static final thumbnail_image_error:I = 0x7f020488

.field public static final thumbnail_video_error:I = 0x7f020489

.field public static final transparent_button_background:I = 0x7f02048a

.field public static final transparent_loading:I = 0x7f02048b

.field public static final tw_ab_spinner_list_focused_holo_dark:I = 0x7f02048c

.field public static final tw_ab_spinner_list_focused_holo_light:I = 0x7f02048d

.field public static final tw_ab_spinner_list_pressed_holo_light:I = 0x7f02048e

.field public static final tw_ab_transparent_holo_dark:I = 0x7f02048f

.field public static final tw_action_bar_icon_assign_holo_dark:I = 0x7f020490

.field public static final tw_action_bar_icon_busrtshot_disabled_holo_dark:I = 0x7f020491

.field public static final tw_action_bar_icon_busrtshot_holo_dark:I = 0x7f020492

.field public static final tw_action_bar_icon_cancel_02_disabled_holo_dark:I = 0x7f020493

.field public static final tw_action_bar_icon_cancel_02_holo_dark:I = 0x7f020494

.field public static final tw_action_bar_icon_fingerprint_move_holo_light:I = 0x7f020495

.field public static final tw_action_bar_icon_fingerprint_remove_holo_light:I = 0x7f020496

.field public static final tw_action_bar_icon_home_holo_dark:I = 0x7f020497

.field public static final tw_action_bar_icon_removetag_holo_dark:I = 0x7f020498

.field public static final tw_action_bar_icon_showonmap_holo_dark:I = 0x7f020499

.field public static final tw_action_bar_icon_signature_holo_dark:I = 0x7f02049a

.field public static final tw_action_bar_icon_trim_holo_light:I = 0x7f02049b

.field public static final tw_action_bar_sub_tab_bg_holo_dark:I = 0x7f02049c

.field public static final tw_action_bar_sub_tab_bg_holo_light:I = 0x7f02049d

.field public static final tw_action_bar_tab_selected_bg_h_holo_dark:I = 0x7f02049e

.field public static final tw_action_item_background_focused_holo_dark:I = 0x7f02049f

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f0204a0

.field public static final tw_background_dark:I = 0x7f0204a1

.field public static final tw_btn_check_off_disabled_focused_holo_dark:I = 0x7f0204a2

.field public static final tw_btn_check_off_disabled_holo_dark:I = 0x7f0204a3

.field public static final tw_btn_check_off_filmstrip_holo_dark:I = 0x7f0204a4

.field public static final tw_btn_check_off_focused_holo_dark:I = 0x7f0204a5

.field public static final tw_btn_check_off_holo_dark:I = 0x7f0204a6

.field public static final tw_btn_check_off_holo_light:I = 0x7f0204a7

.field public static final tw_btn_check_off_pressed_holo_dark:I = 0x7f0204a8

.field public static final tw_btn_check_on_disabled_focused_holo_dark:I = 0x7f0204a9

.field public static final tw_btn_check_on_disabled_holo_dark:I = 0x7f0204aa

.field public static final tw_btn_check_on_filmstrip_holo_dark:I = 0x7f0204ab

.field public static final tw_btn_check_on_focused_holo_dark:I = 0x7f0204ac

.field public static final tw_btn_check_on_holo_dark:I = 0x7f0204ad

.field public static final tw_btn_check_on_holo_light:I = 0x7f0204ae

.field public static final tw_btn_check_on_pressed_holo_dark:I = 0x7f0204af

.field public static final tw_btn_check_to_on_mtrl_000:I = 0x7f0204b0

.field public static final tw_btn_check_to_on_mtrl_001:I = 0x7f0204b1

.field public static final tw_btn_check_to_on_mtrl_002:I = 0x7f0204b2

.field public static final tw_btn_check_to_on_mtrl_003:I = 0x7f0204b3

.field public static final tw_btn_check_to_on_mtrl_004:I = 0x7f0204b4

.field public static final tw_btn_check_to_on_mtrl_005:I = 0x7f0204b5

.field public static final tw_btn_check_to_on_mtrl_006:I = 0x7f0204b6

.field public static final tw_btn_check_to_on_mtrl_007:I = 0x7f0204b7

.field public static final tw_btn_check_to_on_mtrl_008:I = 0x7f0204b8

.field public static final tw_btn_check_to_on_mtrl_009:I = 0x7f0204b9

.field public static final tw_btn_check_to_on_mtrl_010:I = 0x7f0204ba

.field public static final tw_btn_check_to_on_mtrl_011:I = 0x7f0204bb

.field public static final tw_btn_check_to_on_mtrl_012:I = 0x7f0204bc

.field public static final tw_btn_check_to_on_mtrl_013:I = 0x7f0204bd

.field public static final tw_btn_check_to_on_mtrl_014:I = 0x7f0204be

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f0204bf

.field public static final tw_btn_default_disabled_holo_dark:I = 0x7f0204c0

.field public static final tw_btn_default_normal_holo_dark:I = 0x7f0204c1

.field public static final tw_btn_default_pressed_holo_dark:I = 0x7f0204c2

.field public static final tw_btn_next_depth_focused_holo_dark:I = 0x7f0204c3

.field public static final tw_btn_next_depth_holo_dark:I = 0x7f0204c4

.field public static final tw_btn_next_depth_pressed_holo_dark:I = 0x7f0204c5

.field public static final tw_btn_reorder_holo_dark:I = 0x7f0204c6

.field public static final tw_buttonbarbutton_selector_default_holo_dark:I = 0x7f0204c7

.field public static final tw_buttonbarbutton_selector_default_holo_light:I = 0x7f0204c8

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_dark:I = 0x7f0204c9

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_light:I = 0x7f0204ca

.field public static final tw_buttonbarbutton_selector_disabled_holo_dark:I = 0x7f0204cb

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f0204cc

.field public static final tw_buttonbarbutton_selector_focused_holo_dark:I = 0x7f0204cd

.field public static final tw_buttonbarbutton_selector_focused_holo_light:I = 0x7f0204ce

.field public static final tw_buttonbarbutton_selector_pressed_holo_dark:I = 0x7f0204cf

.field public static final tw_buttonbarbutton_selector_pressed_holo_light:I = 0x7f0204d0

.field public static final tw_buttonbarbutton_selector_selected_holo_dark:I = 0x7f0204d1

.field public static final tw_buttonbarbutton_selector_selected_holo_light:I = 0x7f0204d2

.field public static final tw_device_options_divider:I = 0x7f0204d3

.field public static final tw_dialog_list_section_divider_holo_dark:I = 0x7f0204d4

.field public static final tw_dialog_list_section_divider_holo_light:I = 0x7f0204d5

.field public static final tw_divider_ab_holo_dark:I = 0x7f0204d6

.field public static final tw_divider_ab_holo_light:I = 0x7f0204d7

.field public static final tw_divider_ab_holo_light_custom:I = 0x7f0204d8

.field public static final tw_divider_vertical_holo_dark:I = 0x7f0204d9

.field public static final tw_divider_vertical_holo_light:I = 0x7f0204da

.field public static final tw_drawer_bg_holo_dark:I = 0x7f0204db

.field public static final tw_drawer_bg_holo_light:I = 0x7f0204dc

.field public static final tw_drawer_list_line_holo_dark:I = 0x7f0204dd

.field public static final tw_drawer_list_line_holo_light:I = 0x7f0204de

.field public static final tw_drawer_section_divider_holo_dark:I = 0x7f0204df

.field public static final tw_expandable_list_left_focused_holo_dark:I = 0x7f0204e0

.field public static final tw_expandable_list_left_holo_dark:I = 0x7f0204e1

.field public static final tw_expandable_list_left_pressed_holo_dark:I = 0x7f0204e2

.field public static final tw_expander_close_01_holo_dark:I = 0x7f0204e3

.field public static final tw_expander_close_01_holo_light:I = 0x7f0204e4

.field public static final tw_expander_close_default_holo_dark:I = 0x7f0204e5

.field public static final tw_expander_close_disabled_focused_holo_dark:I = 0x7f0204e6

.field public static final tw_expander_close_disabled_holo_dark:I = 0x7f0204e7

.field public static final tw_expander_close_focused_holo_dark:I = 0x7f0204e8

.field public static final tw_expander_close_holo_dark:I = 0x7f0204e9

.field public static final tw_expander_close_mtrl_alpha:I = 0x7f0204ea

.field public static final tw_expander_close_pressed_holo_dark:I = 0x7f0204eb

.field public static final tw_expander_list_bg_holo_dark:I = 0x7f0204ec

.field public static final tw_expander_list_bg_holo_light:I = 0x7f0204ed

.field public static final tw_expander_list_line_holo_dark:I = 0x7f0204ee

.field public static final tw_expander_open_01_holo_dark:I = 0x7f0204ef

.field public static final tw_expander_open_01_holo_light:I = 0x7f0204f0

.field public static final tw_expander_open_default_holo_dark:I = 0x7f0204f1

.field public static final tw_expander_open_disabled_focused_holo_dark:I = 0x7f0204f2

.field public static final tw_expander_open_disabled_holo_dark:I = 0x7f0204f3

.field public static final tw_expander_open_focused_holo_dark:I = 0x7f0204f4

.field public static final tw_expander_open_holo_dark:I = 0x7f0204f5

.field public static final tw_expander_open_mtrl_alpha:I = 0x7f0204f6

.field public static final tw_expander_open_pressed_holo_dark:I = 0x7f0204f7

.field public static final tw_fastscroll_thumb_default_holo:I = 0x7f0204f8

.field public static final tw_fastscroll_thumb_pressed_holo:I = 0x7f0204f9

.field public static final tw_fullscreen_background_dark:I = 0x7f0204fa

.field public static final tw_group_list_icon_down_holo_dark:I = 0x7f0204fb

.field public static final tw_group_list_icon_down_holo_pressed_dark:I = 0x7f0204fc

.field public static final tw_group_list_icon_up_holo_dark:I = 0x7f0204fd

.field public static final tw_group_list_icon_up_holo_pressed_dark:I = 0x7f0204fe

.field public static final tw_ic_ab_drawer_holo_dark:I = 0x7f0204ff

.field public static final tw_ic_ab_drawer_holo_light:I = 0x7f020500

.field public static final tw_ic_cab_save_dark:I = 0x7f020501

.field public static final tw_ic_cab_save_dark_dim:I = 0x7f020502

.field public static final tw_ic_cab_save_dark_press:I = 0x7f020503

.field public static final tw_ic_clear_search_api_holo_dark:I = 0x7f020504

.field public static final tw_ic_mic:I = 0x7f020505

.field public static final tw_list_divider_holo_dark:I = 0x7f020506

.field public static final tw_list_focused_holo_dark:I = 0x7f020507

.field public static final tw_list_focused_holo_dark_opaque:I = 0x7f020508

.field public static final tw_list_focused_holo_light:I = 0x7f020509

.field public static final tw_list_icon_create_focused_holo_dark:I = 0x7f02050a

.field public static final tw_list_icon_create_holo_dark:I = 0x7f02050b

.field public static final tw_list_icon_create_pressed_holo_dark:I = 0x7f02050c

.field public static final tw_list_pressed_holo_dark:I = 0x7f02050d

.field public static final tw_list_pressed_holo_dark_opaque:I = 0x7f02050e

.field public static final tw_list_pressed_holo_light:I = 0x7f02050f

.field public static final tw_list_section_divider_dark:I = 0x7f020510

.field public static final tw_list_section_divider_focused_holo_dark:I = 0x7f020511

.field public static final tw_list_section_divider_focused_holo_light:I = 0x7f020512

.field public static final tw_list_section_divider_holo_dark:I = 0x7f020513

.field public static final tw_list_section_divider_holo_light:I = 0x7f020514

.field public static final tw_list_section_divider_index_holo_dark:I = 0x7f020515

.field public static final tw_list_section_divider_index_holo_light:I = 0x7f020516

.field public static final tw_list_section_divider_light:I = 0x7f020517

.field public static final tw_list_section_divider_pressed_holo_dark:I = 0x7f020518

.field public static final tw_list_section_divider_pressed_holo_light:I = 0x7f020519

.field public static final tw_list_section_divider_selected_holo_dark:I = 0x7f02051a

.field public static final tw_list_section_divider_selected_holo_light:I = 0x7f02051b

.field public static final tw_list_selected_holo_dark:I = 0x7f02051c

.field public static final tw_list_selected_holo_light:I = 0x7f02051d

.field public static final tw_menu_ab_dropdown_panel_holo_light:I = 0x7f02051e

.field public static final tw_menu_dropdown_panel_holo_light:I = 0x7f02051f

.field public static final tw_no_item_bg_h_holo_dark_gallery_land:I = 0x7f020520

.field public static final tw_no_item_bg_holo_dark:I = 0x7f020521

.field public static final tw_no_item_popup_bg_holo_dark:I = 0x7f020522

.field public static final tw_no_item_popup_bg_holo_dark_middle:I = 0x7f020523

.field public static final tw_no_item_popup_bg_holo_light:I = 0x7f020524

.field public static final tw_popup_horizontal_divider_holo_dark:I = 0x7f020525

.field public static final tw_preference_contents_bg_holo_light:I = 0x7f020526

.field public static final tw_preference_contents_list_arrow_holo_light:I = 0x7f020527

.field public static final tw_preference_contents_list_divider_holo_light:I = 0x7f020528

.field public static final tw_preference_contents_list_left_split_default_holo_dark:I = 0x7f020529

.field public static final tw_preference_contents_list_no_arrow_left_holo_light:I = 0x7f02052a

.field public static final tw_preference_contents_list_scrollbar_handle_holo_light:I = 0x7f02052b

.field public static final tw_preference_header_item_background_holo_light:I = 0x7f02052c

.field public static final tw_preference_header_list_arrow_focused_holo_light:I = 0x7f02052d

.field public static final tw_preference_header_list_arrow_pressed_holo_light:I = 0x7f02052e

.field public static final tw_preference_popup_list_section_divider_holo_dark:I = 0x7f02052f

.field public static final tw_scrollbar_handle_holo_dark:I = 0x7f020530

.field public static final tw_searchfiled_background_focused_holo_dark:I = 0x7f020531

.field public static final tw_searchfiled_background_holo_dark:I = 0x7f020532

.field public static final tw_select_all_bg_holo_dark:I = 0x7f020533

.field public static final tw_select_all_bg_holo_light:I = 0x7f020534

.field public static final tw_spinner_ab_default_holo_dark_am:I = 0x7f020535

.field public static final tw_spinner_ab_default_holo_light:I = 0x7f020536

.field public static final tw_spinner_ab_default_holo_light_white:I = 0x7f020537

.field public static final tw_spinner_ab_disabled_holo_dark_am:I = 0x7f020538

.field public static final tw_spinner_ab_disabled_holo_light:I = 0x7f020539

.field public static final tw_spinner_ab_focused_holo_dark_am:I = 0x7f02053a

.field public static final tw_spinner_ab_focused_holo_light:I = 0x7f02053b

.field public static final tw_spinner_ab_focused_holo_light_white:I = 0x7f02053c

.field public static final tw_spinner_ab_pressed_holo_dark_am:I = 0x7f02053d

.field public static final tw_spinner_ab_pressed_holo_light:I = 0x7f02053e

.field public static final tw_spinner_ab_selected_holo_light:I = 0x7f02053f

.field public static final tw_spinner_divider_horizontal_light:I = 0x7f020540

.field public static final tw_tab_divider_holo_dark:I = 0x7f020541

.field public static final tw_tab_divider_holo_light:I = 0x7f020542

.field public static final tw_tab_selected_bg_holo_light:I = 0x7f020543

.field public static final tw_tab_selected_focused_holo_dark:I = 0x7f020544

.field public static final tw_tab_selected_focused_holo_light:I = 0x7f020545

.field public static final tw_tab_selected_focused_pressed_holo_dark:I = 0x7f020546

.field public static final tw_tab_selected_focused_pressed_holo_light:I = 0x7f020547

.field public static final tw_tab_selected_pressed_holo_dark:I = 0x7f020548

.field public static final tw_tab_selected_pressed_holo_light:I = 0x7f020549

.field public static final tw_textfield_activated_holo_dark:I = 0x7f02054a

.field public static final tw_textfield_default_holo_dark:I = 0x7f02054b

.field public static final tw_textfield_disabled_focused_holo_dark:I = 0x7f02054c

.field public static final tw_textfield_disabled_holo_dark:I = 0x7f02054d

.field public static final tw_textfield_focused_holo_dark:I = 0x7f02054e

.field public static final tw_textfield_multiline_activated_holo_dark:I = 0x7f02054f

.field public static final tw_textfield_multiline_default_holo_dark:I = 0x7f020550

.field public static final tw_textfield_multiline_disabled_focused_holo_dark:I = 0x7f020551

.field public static final tw_textfield_multiline_disabled_holo_dark:I = 0x7f020552

.field public static final tw_textfield_multiline_focused_holo_dark:I = 0x7f020553

.field public static final tw_textfield_multiline_pressed_holo_dark:I = 0x7f020554

.field public static final tw_textfield_multiline_selected_holo_dark:I = 0x7f020555

.field public static final tw_textfield_pressed_holo_dark:I = 0x7f020556

.field public static final tw_textfield_search_default_bg_holo_dark:I = 0x7f020557

.field public static final tw_textfield_search_default_holo_dark:I = 0x7f020558

.field public static final tw_textfield_search_focused_01_holo_dark:I = 0x7f020559

.field public static final tw_textfield_search_select_holo_dark:I = 0x7f02055a

.field public static final tw_textfield_search_selected_holo_dark:I = 0x7f02055b

.field public static final tw_textfield_selected_holo_dark:I = 0x7f02055c

.field public static final tw_thumbnail_multiple_selected_holo_dark:I = 0x7f02055d

.field public static final tw_thumbnail_multiple_selection_holo_dark:I = 0x7f02055e

.field public static final tw_toast_frame_holo:I = 0x7f02055f

.field public static final unchecked_bitmap_color_list:I = 0x7f020560

.field public static final videoclip_edit_bg:I = 0x7f020561

.field public static final videoclip_edit_focus:I = 0x7f020562

.field public static final videoclip_edit_press:I = 0x7f020563

.field public static final videoclip_edit_selected:I = 0x7f020564

.field public static final videoclip_edit_selected_check:I = 0x7f020565

.field public static final videoclip_effect_pic_select:I = 0x7f020566

.field public static final videoclip_effect_scroll:I = 0x7f020567

.field public static final vintage_l:I = 0x7f020568

.field public static final virtual_tour_detail_dot:I = 0x7f020569

.field public static final virtual_tour_detail_dot_end:I = 0x7f02056a

.field public static final virtual_tour_detail_dot_start:I = 0x7f02056b

.field public static final virtual_tour_detail_light:I = 0x7f02056c

.field public static final virtual_tour_full_dot_end:I = 0x7f02056d

.field public static final virtual_tour_full_dot_start:I = 0x7f02056e

.field public static final virtual_tour_full_light:I = 0x7f02056f

.field public static final virtual_tour_fulll_dot:I = 0x7f020570

.field public static final virtual_tour_minimap:I = 0x7f020571

.field public static final wallpaper_edit_cross:I = 0x7f020572

.field public static final wallpaper_picker_preview:I = 0x7f020573

.field public static final white_text_bg_gradient:I = 0x7f020574

.field public static final widget_innerline:I = 0x7f020575

.field public static final widget_masking_bg:I = 0x7f020576

.field public static final widget_play_button:I = 0x7f020577

.field public static final widget_preference_frametype_multi_left_rightupdown_off:I = 0x7f020578

.field public static final widget_preference_frametype_multi_left_rightupdown_off_focused:I = 0x7f020579

.field public static final widget_preference_frametype_multi_left_rightupdown_on:I = 0x7f02057a

.field public static final widget_preference_frametype_multi_up_down_off:I = 0x7f02057b

.field public static final widget_preference_frametype_multi_up_down_off_focused:I = 0x7f02057c

.field public static final widget_preference_frametype_multi_up_down_on:I = 0x7f02057d

.field public static final widget_preference_frametype_multi_up_downleftright_off:I = 0x7f02057e

.field public static final widget_preference_frametype_multi_up_downleftright_off_focused:I = 0x7f02057f

.field public static final widget_preference_frametype_multi_up_downleftright_on:I = 0x7f020580

.field public static final widget_preference_frametype_single_off:I = 0x7f020581

.field public static final widget_preference_frametype_single_off_focused:I = 0x7f020582

.field public static final widget_preference_frametype_single_on:I = 0x7f020583

.field public static final widget_shadow:I = 0x7f020584


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
