.class public Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;
.super Ljava/lang/Object;
.source "GlHoverGestureDetector.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SimpleOnGestureListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMove(III)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "direction"    # I

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public onNext()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public onPress(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public onPrevious()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public onRelease(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method
