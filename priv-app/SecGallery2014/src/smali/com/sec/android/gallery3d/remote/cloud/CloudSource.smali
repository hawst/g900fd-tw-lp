.class public Lcom/sec/android/gallery3d/remote/cloud/CloudSource;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSource;
.source "CloudSource.java"


# static fields
.field public static final ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final CLOUD_ALBUM:I = 0x1

.field private static final CLOUD_ALBUMSET:I = 0x0

.field private static final CLOUD_IMAGE_ALBUM:I = 0x2

.field private static final CLOUD_IMAGE_ITEM:I = 0x4

.field private static final CLOUD_MERGESET:I = 0x6

.field private static final CLOUD_MERGE_ALL_ALBUM:I = 0x7

.field private static final CLOUD_MERGE_IMAGE_ALBUM:I = 0x8

.field private static final CLOUD_MERGE_VIDEO_ALBUM:I = 0x9

.field private static final CLOUD_VIDEO_ALBUM:I = 0x3

.field private static final CLOUD_VIDEO_ITEM:I = 0x5

.field public static final MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final NO_MATCH:I = -0x1

.field private static final TAG:Ljava/lang/String; = "CloudSource"


# instance fields
.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    const-string v0, "/cloud/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/cloud/mergedall/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->CLOUD_MERGESET_BUCKETID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 8
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 63
    const-string v0, "cloud"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 45
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    .line 65
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/all"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/image"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/video"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/all/*"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/image/*"

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/video/*"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/mergedall"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/mergedimage"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/mergedvideo"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/mergedall/*"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/mergedall/*/*"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/mergedimage/*"

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/mergedvideo/*"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/image/item/*"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/video/item/*"

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/all/*/*"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/image/*/*"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cloud/video/*/*"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "data/images/media/#"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "data/videos/media/#"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.gallery3d.provider"

    const-string v2, "cloud/image/item/#"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.gallery3d.provider"

    const-string v2, "cloud/video/item/#"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 89
    return-void
.end method

.method public static isCloudImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p0, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 220
    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V
    .locals 12
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    sget-object v10, Lcom/sec/android/gallery3d/data/LocalSource;->sIdComparator:Ljava/util/Comparator;

    invoke-static {p1, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 193
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 194
    .local v6, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_1

    .line 195
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 197
    .local v7, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v2, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v10, v7, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 199
    .local v9, "startId":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v4, v1, 0x1

    .local v4, "j":I
    :goto_1
    if-ge v4, v6, :cond_0

    .line 202
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 203
    .local v8, "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v10, v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 204
    .local v0, "curId":I
    sub-int v10, v0, v9

    const/16 v11, 0x20

    if-lt v10, v11, :cond_2

    .line 209
    .end local v0    # "curId":I
    .end local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v10, p3, v2}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;->getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;ZLjava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 210
    .local v3, "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    move v5, v1

    .local v5, "k":I
    :goto_2
    if-ge v5, v4, :cond_4

    .line 211
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 212
    .restart local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget v10, v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->id:I

    sub-int v11, v5, v1

    aget-object v11, v3, v11

    invoke-interface {p2, v10, v11}, Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;->consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 217
    .end local v2    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v3    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "j":I
    .end local v5    # "k":I
    .end local v7    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .end local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .end local v9    # "startId":I
    :cond_1
    return-void

    .line 207
    .restart local v0    # "curId":I
    .restart local v2    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v4    # "j":I
    .restart local v7    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v9    # "startId":I
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 210
    .end local v0    # "curId":I
    .restart local v3    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v5    # "k":I
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 215
    .end local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_4
    move v1, v4

    .line 216
    goto :goto_0
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 10
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 94
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v6, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 136
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bad path: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 97
    :pswitch_0
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v6, p1, v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 134
    :goto_0
    return-object v6

    .line 99
    :pswitch_1
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v7

    invoke-direct {v6, p1, v0, v7, v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 101
    :pswitch_2
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v7

    invoke-direct {v6, p1, v0, v7, v9}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 103
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v1

    .line 104
    .local v1, "bucketId":I
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 105
    .local v3, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 107
    .local v4, "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 109
    .local v5, "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 110
    .local v2, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    new-array v7, v7, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v4, v7, v9

    aput-object v5, v7, v8

    invoke-direct {v6, p1, v2, v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 115
    .end local v1    # "bucketId":I
    .end local v2    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v3    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_4
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v8

    invoke-direct {v6, p1, v7, v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    goto :goto_0

    .line 117
    :pswitch_5
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v8

    invoke-direct {v6, p1, v7, v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    goto :goto_0

    .line 119
    :pswitch_6
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v6, p1, v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    goto :goto_0

    .line 121
    :pswitch_7
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v1

    .line 122
    .restart local v1    # "bucketId":I
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 123
    .restart local v3    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 125
    .restart local v4    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 127
    .restart local v5    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 128
    .restart local v2    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    new-array v7, v7, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v4, v7, v9

    aput-object v5, v7, v8

    invoke-direct {v6, p1, v2, v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto/16 :goto_0

    .line 132
    .end local v1    # "bucketId":I
    .end local v2    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v3    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_8
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v7

    invoke-direct {v6, p1, v0, v7, v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto/16 :goto_0

    .line 134
    :pswitch_9
    new-instance v6, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v7

    invoke-direct {v6, p1, v0, v7, v9}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto/16 :goto_0

    .line 94
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 153
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 166
    :cond_0
    :goto_0
    return-object v1

    .line 155
    :pswitch_0
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 156
    .local v2, "id":J
    cmp-long v4, v2, v6

    if-ltz v4, :cond_0

    sget-object v4, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 159
    .end local v2    # "id":J
    :pswitch_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 160
    .restart local v2    # "id":J
    cmp-long v4, v2, v6

    if-ltz v4, :cond_0

    sget-object v4, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 163
    .end local v2    # "id":J
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v4, "CloudSource"

    const-string/jumbo v5, "uri: "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 142
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 143
    .local v1, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v2, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v2, :cond_1

    .line 144
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v2, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->MERGE_ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 145
    .local v0, "albumPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    check-cast v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    .end local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->getAlbumId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 147
    .end local v0    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    return-object v2

    .line 144
    .restart local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 147
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V
    .locals 7
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v1, "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v5, "videoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 174
    .local v2, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 175
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 178
    .local v4, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v6, v4, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 179
    .local v3, "parent":Lcom/sec/android/gallery3d/data/Path;
    sget-object v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    if-ne v3, v6, :cond_1

    .line 180
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    sget-object v6, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    if-ne v3, v6, :cond_0

    .line 182
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 185
    .end local v3    # "parent":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_2
    const/4 v6, 0x1

    invoke-direct {p0, v1, p2, v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V

    .line 186
    const/4 v6, 0x0

    invoke-direct {p0, v5, p2, v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V

    .line 187
    return-void
.end method

.method public requestSyncAll()V
    .locals 0

    .prologue
    .line 229
    return-void
.end method
