.class Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;
.super Ljava/lang/Object;
.source "GlHoverGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Area"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method contains(II)Z
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/high16 v9, 0x42700000    # 60.0f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 282
    const/4 v4, 0x0

    .line 284
    .local v4, "result":Z
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$500(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v3

    .line 285
    .local v3, "point":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$500(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    mul-float v1, v7, v8

    .line 286
    .local v1, "height":F
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$500(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    mul-float v6, v7, v8

    .line 287
    .local v6, "width":F
    iget v7, v3, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    div-float/2addr v7, v6

    mul-float/2addr v7, v9

    float-to-int v2, v7

    .line 288
    .local v2, "horizontalArea":I
    iget v7, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    div-float/2addr v7, v1

    mul-float/2addr v7, v9

    float-to-int v5, v7

    .line 290
    .local v5, "verticalArea":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$Area;->this$0:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->access$500(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 291
    .local v0, "actionbarHeight":I
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v7, :cond_1

    .line 292
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 293
    :cond_0
    const/4 v7, 0x0

    .line 302
    :goto_0
    return v7

    .line 296
    :cond_1
    if-le p2, v0, :cond_2

    add-int v7, v0, v5

    if-lt p2, v7, :cond_3

    :cond_2
    iget v7, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v5

    if-gt p2, v7, :cond_3

    if-lt p1, v2, :cond_3

    iget v7, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v2

    if-le p1, v7, :cond_4

    .line 300
    :cond_3
    const/4 v4, 0x1

    :cond_4
    move v7, v4

    .line 302
    goto :goto_0
.end method
