.class final Lcom/sec/android/gallery3d/data/GallerySearchAlbum$4;
.super Ljava/lang/Object;
.source "GallerySearchAlbum.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/GallerySearchAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 4
    .param p1, "object1"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "object2"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 558
    const/4 v2, -0x1

    .line 559
    .local v2, "result":I
    # getter for: Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mSortByOldest:Z
    invoke-static {}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->access$100()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 560
    const/4 v2, 0x1

    .line 563
    :cond_0
    if-nez p1, :cond_2

    .line 564
    neg-int v2, v2

    .line 569
    .end local v2    # "result":I
    :cond_1
    :goto_0
    return v2

    .line 565
    .restart local v2    # "result":I
    :cond_2
    if-eqz p2, :cond_1

    .line 567
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v0

    .line 568
    .local v0, "id1":I
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v1

    .line 569
    .local v1, "id2":I
    if-le v0, v1, :cond_3

    neg-int v2, v2

    goto :goto_0

    :cond_3
    if-ne v0, v1, :cond_1

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 554
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$4;->compare(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    return v0
.end method
