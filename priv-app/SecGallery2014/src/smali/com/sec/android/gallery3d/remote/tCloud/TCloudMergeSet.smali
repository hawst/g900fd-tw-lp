.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "TCloudMergeSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field private static final FAKE_LOADING_COUNT:I = 0x64

.field public static TCLOUD_MERGESET_BUCKETID:I

.field private static final mWatchTalbum:Landroid/net/Uri;

.field private static sbFakeLoading:Z

.field public static sbNeedFullLoading:Z


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mRootSet:Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

.field private final mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "content://talbum_full_load"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mWatchTalbum:Landroid/net/Uri;

    .line 44
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbFakeLoading:Z

    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbNeedFullLoading:Z

    .line 47
    const-string v0, "/tCloud/mergedall"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->TCLOUD_MERGESET_BUCKETID:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 52
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 53
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->getBucketId()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 55
    const-string v0, "T cloud"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mName:Ljava/lang/String;

    .line 56
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mWatchTalbum:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 57
    return-void
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    return-object v0
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    .line 108
    const/4 v0, -0x1

    .line 109
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 70
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getSubMediaSetCount(Z)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getSupportedOperations()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalMediaItems(IZ)[Landroid/database/Cursor;
    .locals 10
    .param p1, "filterType"    # I
    .param p2, "useFakeLoading"    # Z

    .prologue
    const/4 v0, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 118
    new-array v8, v0, [Landroid/database/Cursor;

    .line 119
    .local v8, "cursors":[Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v1, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    .line 121
    .local v1, "imageUri":Landroid/net/Uri;
    if-eqz p2, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbFakeLoading:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "0,100"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 123
    sput-boolean v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbFakeLoading:Z

    .line 124
    sput-boolean v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbNeedFullLoading:Z

    .line 128
    :cond_0
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    .line 129
    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v4, v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_ID:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v4, v4, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DATE_MODIFIED:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v7, v7, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DATE_MODIFIED:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " DESC"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v8, v6

    .line 136
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    .line 137
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseVideoUri:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_ID:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v6, v6, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DATE_MODIFIED:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v9, v9, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DATE_MODIFIED:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " DESC"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v8, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :cond_2
    :goto_0
    return-object v8

    .line 143
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->notifyContentChanged()V

    .line 115
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->reload()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mDataVersion:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mDataVersion:J

    .line 88
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->mDataVersion:J

    return-wide v0
.end method
