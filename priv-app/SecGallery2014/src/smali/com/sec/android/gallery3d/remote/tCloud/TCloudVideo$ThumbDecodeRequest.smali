.class Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;
.super Ljava/lang/Object;
.source "TCloudVideo.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbDecodeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;I)V
    .locals 0
    .param p2, "type"    # I

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput p2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;->mType:I

    .line 142
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v3

    .line 162
    :goto_0
    return-object v2

    .line 149
    :cond_0
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v2, v3

    .line 150
    goto :goto_0

    .line 152
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->access$100(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    iget-wide v6, v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    invoke-virtual {v4, v2, v6, v7}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getThumbnail(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 156
    :goto_1
    invoke-interface {p1, v8}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v2

    if-nez v2, :cond_2

    move-object v2, v3

    .line 157
    goto :goto_0

    .line 153
    :catch_0
    move-exception v1

    .line 154
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 159
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_2
    if-eqz v0, :cond_3

    .line 160
    iget v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;->mType:I

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v2

    invoke-static {v0, v2, v8}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_3
    move-object v2, v0

    .line 162
    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
