.class public Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "PicasaMergeAlbum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;
    }
.end annotation


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field private static final SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

.field private static final TAG:Ljava/lang/String;

.field private static final TYPE_WHERE_CLAUSE:Ljava/lang/String; = "content_type LIKE ?"

.field private static final sIdIndex:I


# instance fields
.field private mCachedCount:I

.field private mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->TAG:Ljava/lang/String;

    .line 40
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    .line 41
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->sIdIndex:I

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mCachedCount:I

    .line 50
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mType:I

    .line 56
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mResolver:Landroid/content/ContentResolver;

    .line 57
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mType:I

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Z)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "isImage"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 65
    if-eqz p3, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mType:I

    .line 66
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    return-object v0
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "isImage"    # Z

    .prologue
    .line 89
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Z)V

    return-object v0
.end method

.method public static getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 23
    .param p0, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)[",
            "Lcom/sec/android/gallery3d/data/MediaItem;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v21, v0

    .line 139
    .local v21, "result":[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 185
    :goto_0
    return-object v21

    .line 141
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 142
    .local v18, "idLow":J
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 144
    .local v16, "idHigh":J
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 145
    .local v2, "resolver":Landroid/content/ContentResolver;
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v10

    .line 146
    .local v10, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    .line 147
    .local v3, "uri":Landroid/net/Uri;
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "_id BETWEEN ? AND ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v6, v7

    const/4 v7, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v6, v7

    const-string v7, "_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 153
    .local v9, "cursor":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 154
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->TAG:Ljava/lang/String;

    const-string v5, "query fail"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 159
    :cond_1
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 160
    .local v20, "n":I
    const/4 v12, 0x0

    .line 162
    .local v12, "i":I
    :cond_2
    :goto_1
    move/from16 v0, v20

    if-ge v12, v0, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 163
    sget v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->sIdIndex:I

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 166
    .local v14, "id":J
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v14

    if-gtz v4, :cond_2

    .line 170
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    cmp-long v4, v4, v14

    if-gez v4, :cond_4

    .line 171
    add-int/lit8 v12, v12, 0x1

    move/from16 v0, v20

    if-lt v12, v0, :cond_3

    .line 185
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 176
    :cond_4
    :try_start_1
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    new-instance v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    invoke-virtual {v4, v9, v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 177
    .local v11, "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v14, v15}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 178
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    invoke-static {v8, v11, v10, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v13

    .line 180
    .local v13, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    aput-object v13, v21, v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    add-int/lit8 v12, v12, 0x1

    .line 182
    goto :goto_1

    .line 185
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v11    # "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .end local v13    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v14    # "id":J
    :cond_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .end local v12    # "i":I
    .end local v20    # "n":I
    :catchall_0
    move-exception v4

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method public static getPhotoEntry(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .locals 9
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J

    .prologue
    const/4 v5, 0x0

    .line 190
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 196
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :goto_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    return-object v0

    :cond_0
    move-object v0, v5

    .line 196
    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 5
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "name":[Ljava/lang/String;
    array-length v2, v0

    if-ge v2, v1, :cond_0

    .line 71
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 73
    :cond_0
    const-string v2, "all"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "mergedall"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 74
    :cond_1
    const/4 v1, 0x6

    .line 78
    :cond_2
    :goto_0
    return v1

    .line 75
    :cond_3
    const-string v2, "image"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mergedimage"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 77
    const-string/jumbo v1, "video"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "mergedvideo"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 78
    :cond_4
    const/4 v1, 0x4

    goto :goto_0

    .line 79
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 125
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .line 126
    .local v0, "item":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    if-nez v0, :cond_0

    .line 127
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;)V

    .line 131
    .restart local v0    # "item":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    :goto_0
    return-object v0

    .line 129
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->updateContent(Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;)V

    goto :goto_0
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    .line 237
    const/4 v0, -0x1

    .line 239
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getCacheFlag()I
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x1

    return v0
.end method

.method public getCacheSize()J
    .locals 2

    .prologue
    .line 278
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getCacheStatus()I
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 97
    .local v1, "uri":Landroid/net/Uri;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-string v5, "album_id DESC, display_index ASC"

    .line 99
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 100
    const-string v5, "album_id DESC, display_index DESC"

    .line 102
    :cond_0
    new-instance v12, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;

    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mType:I

    invoke-direct {v12, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;-><init>(I)V

    .line 103
    .local v12, "whereEntry":Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, v12, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->selection:Ljava/lang/String;

    iget-object v4, v12, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->args:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 107
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 108
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :goto_0
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    new-instance v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    invoke-virtual {v0, v7, v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 110
    .local v9, "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iget-wide v2, v9, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->id:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 111
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v6, v9, v8, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    .line 113
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 116
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v9    # "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_1

    .line 117
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 116
    .restart local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_2
    if-eqz v7, :cond_3

    .line 117
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 120
    :cond_3
    return-object v11
.end method

.method public getMediaItemCount()I
    .locals 8

    .prologue
    .line 209
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 210
    new-instance v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;

    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mType:I

    invoke-direct {v7, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;-><init>(I)V

    .line 211
    .local v7, "entry":Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    iget-object v3, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->selection:Ljava/lang/String;

    iget-object v4, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->args:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 215
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 216
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 217
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mCachedCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    :cond_0
    if-eqz v6, :cond_1

    .line 222
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 226
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "entry":Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mCachedCount:I

    return v0

    .line 221
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "entry":Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 222
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    const-string v0, "Picasa"

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 12

    .prologue
    const/4 v3, 0x4

    .line 244
    const/4 v8, 0x0

    .line 245
    .local v8, "hasVideo":Z
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mType:I

    if-ne v0, v3, :cond_2

    .line 246
    const/4 v8, 0x1

    .line 260
    :cond_0
    :goto_0
    const-wide v10, 0x400000000500L

    .line 261
    .local v10, "supportedOperations":J
    if-nez v8, :cond_1

    .line 262
    const-wide/16 v2, 0x4

    or-long/2addr v10, v2

    .line 263
    :cond_1
    return-wide v10

    .line 247
    .end local v10    # "supportedOperations":J
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mType:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    .line 248
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 249
    .local v1, "uri":Landroid/net/Uri;
    new-instance v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;

    invoke-direct {v7, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;-><init>(I)V

    .line 250
    .local v7, "entry":Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->selection:Ljava/lang/String;

    iget-object v4, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->args:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 253
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    const/4 v8, 0x1

    .line 256
    :cond_3
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x0

    return v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    invoke-static {}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mDataVersion:J

    .line 285
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mCachedCount:I

    .line 287
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->mDataVersion:J

    return-wide v0
.end method

.method protected updateContent(Lcom/sec/android/gallery3d/remote/picasa/AlbumData;)V
    .locals 0
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    .prologue
    .line 205
    return-void
.end method
