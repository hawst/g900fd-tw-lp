.class public Lcom/sec/android/gallery3d/app/Wallpaper;
.super Landroid/app/Activity;
.source "Wallpaper.java"


# static fields
.field private static final IMAGE_TYPE:Ljava/lang/String; = "image/*"

.field public static final KEY_CROP_MODE:Ljava/lang/String; = "mode-crop"

.field private static final KEY_PICKED_ITEM:Ljava/lang/String; = "picked-item"

.field private static final KEY_STATE:Ljava/lang/String; = "activity-state"

.field private static final STATE_INIT:I = 0x0

.field private static final STATE_PHOTO_PICKED:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

.field private mPickedItem:Landroid/net/Uri;

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/android/gallery3d/app/Wallpaper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/Wallpaper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mState:I

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    return-void
.end method

.method private getDefaultDisplaySize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 3
    .param p1, "size"    # Landroid/graphics/Point;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 80
    .local v0, "d":Landroid/view/Display;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-lt v1, v2, :cond_0

    .line 81
    invoke-virtual {v0, p1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 85
    :goto_0
    return-object p1

    .line 83
    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 175
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-nez v0, :cond_1

    .line 177
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 178
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/app/Wallpaper;->setResult(I)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->finish()V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mState:I

    .line 186
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-eqz p3, :cond_0

    .line 188
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    if-eqz p1, :cond_0

    .line 63
    const-string v0, "activity-state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mState:I

    .line 64
    const-string v0, "picked-item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    .line 66
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 199
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->dismissDialog()V

    .line 202
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/16 v7, 0x400

    const/4 v6, 0x1

    .line 91
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v4, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 94
    new-instance v5, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-direct {v5, v4, p0, v6}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .line 95
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->showDialog()V

    .line 170
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 100
    .local v1, "intent":Landroid/content/Intent;
    iget v4, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mState:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    .line 103
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    if-nez v4, :cond_1

    .line 108
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 109
    .local v3, "request":Landroid/content/Intent;
    const-string v4, "android.intent.action.GET_CONTENT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v4, "image/*"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v4, "mode-crop"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    const-string v4, "set-as-wallpaper"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 114
    const-string/jumbo v4, "wallpaper_type"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 116
    invoke-virtual {p0, v3, v6}, Lcom/sec/android/gallery3d/app/Wallpaper;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 119
    .end local v3    # "request":Landroid/content/Intent;
    :cond_1
    iput v6, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mState:I

    .line 159
    :pswitch_1
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    if-eqz v4, :cond_4

    .line 160
    :cond_2
    const/4 v2, 0x0

    .line 161
    .local v2, "isFromRcs":Z
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 162
    .local v0, "extra":Landroid/os/Bundle;
    if-eqz v0, :cond_3

    .line 163
    const-string v4, "rcs"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 164
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    const-string v5, "image/*"

    invoke-static {p0, v4, v5, v7, v2}, Lcom/sec/samsung/gallery/controller/CropImageCmd;->startCropImageActivity(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;IZ)V

    .line 166
    .end local v0    # "extra":Landroid/os/Bundle;
    .end local v2    # "isFromRcs":Z
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/Wallpaper;->finish()V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "saveState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const-string v0, "activity-state"

    iget v1, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "picked-item"

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/Wallpaper;->mPickedItem:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 74
    :cond_0
    return-void
.end method
