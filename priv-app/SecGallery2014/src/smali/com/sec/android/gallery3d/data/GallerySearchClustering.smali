.class public Lcom/sec/android/gallery3d/data/GallerySearchClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "GallerySearchClustering.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mItemsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation
.end field

.field private mNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mNames:Ljava/util/List;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mItemsList:Ljava/util/ArrayList;

    .line 39
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method


# virtual methods
.method public getCluster(I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mItemsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mItemsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 58
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mItemsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-object v3, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return-object v2
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 3
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v0

    .line 45
    .local v0, "total":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mItemsList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v0, v2}, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V

    .line 46
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchClustering;->mNames:Ljava/util/List;

    const-string v2, "Photos"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method
