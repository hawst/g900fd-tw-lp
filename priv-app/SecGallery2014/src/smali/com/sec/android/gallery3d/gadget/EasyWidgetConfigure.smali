.class public Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;
.super Landroid/app/Activity;
.source "EasyWidgetConfigure.java"


# static fields
.field private static final INVALID:I = -0x1

.field private static final REQUEST_GET_PHOTOS:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAppWidgetId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    return-void
.end method

.method private setChoosenPhotos(Landroid/content/Intent;)V
    .locals 11
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 93
    sget-object v7, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->TAG:Ljava/lang/String;

    const-string v8, "set choosen photos"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    if-eqz p1, :cond_1

    .line 96
    const-string v7, "selectedItems"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 97
    .local v6, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v4, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 100
    .local v3, "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 101
    .local v5, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    invoke-virtual {v3, v5, v7}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {p0, v7, v8}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 107
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v6    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 108
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 109
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v7, 0x3

    iput v7, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 111
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 112
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 117
    const/4 v7, -0x1

    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const-string v9, "appWidgetId"

    iget v10, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->finish()V

    .line 119
    return-void

    .line 114
    :catchall_0
    move-exception v7

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v7
.end method

.method private updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    .locals 4
    .param p1, "entry"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    invoke-static {v0, v1, p1}, Lcom/sec/android/gallery3d/gadget/EasyPhotoAppWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    .line 68
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "appWidgetId"

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->finish()V

    .line 70
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivity Result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 76
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->finish()V

    .line 86
    :goto_0
    return-void

    .line 81
    :cond_0
    if-nez p1, :cond_1

    .line 82
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->setChoosenPhotos(Landroid/content/Intent;)V

    goto :goto_0

    .line 84
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    sget-object v1, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->TAG:Ljava/lang/String;

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    .line 49
    iget v1, p0, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->mAppWidgetId:I

    if-ne v1, v3, :cond_0

    .line 50
    sget-object v1, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->TAG:Ljava/lang/String;

    const-string v2, "AppWidgetId is invalid. cancelled"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->setResult(I)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->finish()V

    .line 64
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .local v0, "request":Landroid/content/Intent;
    const-string v1, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v1, "photo-pick"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    const-string v1, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 62
    invoke-virtual {p0, v0, v4}, Lcom/sec/android/gallery3d/gadget/EasyWidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
