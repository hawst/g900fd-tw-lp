.class Lcom/sec/android/gallery3d/data/SearchTimeClustering$DateComparator;
.super Ljava/lang/Object;
.source "SearchTimeClustering.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/SearchTimeClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DateComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/data/SearchTimeClustering$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/data/SearchTimeClustering$1;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/SearchTimeClustering$DateComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)I
    .locals 4
    .param p1, "item1"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .param p2, "item2"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 44
    iget-wide v0, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    iget-wide v2, p2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->compare(JJ)I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/SearchTimeClustering$DateComparator;->compare(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)I

    move-result v0

    return v0
.end method
