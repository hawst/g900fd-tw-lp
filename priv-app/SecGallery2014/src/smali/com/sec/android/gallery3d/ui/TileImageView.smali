.class public Lcom/sec/android/gallery3d/ui/TileImageView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "TileImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/TileImageView$1;,
        Lcom/sec/android/gallery3d/ui/TileImageView$TileDecoder;,
        Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;,
        Lcom/sec/android/gallery3d/ui/TileImageView$Tile;,
        Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;,
        Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;
    }
.end annotation


# static fields
.field public static final SIZE_UNKNOWN:I = -0x1

.field private static final STATE_ACTIVATED:I = 0x1

.field private static final STATE_DECODED:I = 0x8

.field private static final STATE_DECODE_FAIL:I = 0x10

.field private static final STATE_DECODING:I = 0x4

.field private static final STATE_IN_QUEUE:I = 0x2

.field private static final STATE_RECYCLED:I = 0x40

.field private static final STATE_RECYCLING:I = 0x20

.field private static final TAG:Ljava/lang/String; = "TileImageView"

.field public static final TILE_BORDER:I = 0x1

.field private static final UPLOAD_LIMIT:I = 0x1

.field private static sDisplayHeightPixels:I

.field private static sDisplayWidthPixels:I

.field private static sTileSize:I


# instance fields
.field private final mActiveRange:[Landroid/graphics/Rect;

.field private final mActiveTiles:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/TileImageView$Tile;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundTileUploaded:Z

.field protected mCenterX:F

.field protected mCenterY:F

.field private final mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

.field private mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field protected mImageHeight:I

.field protected mImageWidth:I

.field private mIsTextureFreed:Z

.field private mLevel:I

.field protected mLevelCount:I

.field private mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

.field private mOffsetX:I

.field private mOffsetY:I

.field mRealOffsetX:I

.field mRealOffsetY:I

.field private final mRecycledQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

.field private mRenderComplete:Z

.field protected mRotation:I

.field protected mScale:F

.field protected mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

.field private final mSourceRect:Landroid/graphics/RectF;

.field private final mTargetRect:Landroid/graphics/RectF;

.field private final mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mTileDecoder:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mTileRange:Landroid/graphics/Rect;

.field private final mTileUploader:Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;

.field private final mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

.field private mUploadQuota:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    const/16 v0, 0x438

    sput v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayWidthPixels:I

    .line 135
    const/16 v0, 0x780

    sput v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayHeightPixels:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryContext;)V
    .locals 4
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryContext;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 190
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 94
    iput v2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    .line 104
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mSourceRect:Landroid/graphics/RectF;

    .line 105
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTargetRect:Landroid/graphics/RectF;

    .line 107
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    .line 110
    new-instance v0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRecycledQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    .line 111
    new-instance v0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    .line 112
    new-instance v0, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    .line 115
    iput v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    .line 116
    iput v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileRange:Landroid/graphics/Rect;

    .line 125
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveRange:[Landroid/graphics/Rect;

    .line 127
    new-instance v0, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/ui/TileImageView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileUploader:Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;

    .line 191
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryContext;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 192
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    new-instance v1, Lcom/sec/android/gallery3d/ui/TileImageView$TileDecoder;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileDecoder;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/ui/TileImageView$1;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileDecoder:Lcom/sec/android/gallery3d/util/Future;

    .line 195
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryContext;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 198
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    invoke-static {v0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->setTileDecSize(I)V

    .line 200
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/TileImageView;III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTile(III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/TileImageView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    return v0
.end method

.method private activateTile(III)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I

    .prologue
    .line 666
    invoke-static {p1, p2, p3}, Lcom/sec/android/gallery3d/ui/TileImageView;->makeTileKey(III)J

    move-result-wide v0

    .line 667
    .local v0, "key":J
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 668
    .local v2, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    if-eqz v2, :cond_1

    .line 669
    iget v3, v2, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 670
    const/4 v3, 0x1

    iput v3, v2, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 674
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/TileImageView;->obtainTile(III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v2

    .line 675
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0, v1, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0
.end method

.method private getRange(Landroid/graphics/Rect;FFIFI)V
    .locals 26
    .param p1, "out"    # Landroid/graphics/Rect;
    .param p2, "cX"    # F
    .param p3, "cY"    # F
    .param p4, "level"    # I
    .param p5, "scale"    # F
    .param p6, "rotation"    # I

    .prologue
    .line 365
    move/from16 v0, p6

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    .line 366
    .local v10, "radians":D
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getWidth()I

    move-result v20

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v18, v0

    .line 367
    .local v18, "w":D
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-double v6, v0

    .line 369
    .local v6, "h":D
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 370
    .local v4, "cos":D
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 371
    .local v14, "sin":D
    mul-double v20, v4, v18

    mul-double v22, v14, v6

    sub-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    mul-double v22, v4, v18

    mul-double v24, v14, v6

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->max(DD)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v17, v0

    .line 373
    .local v17, "width":I
    mul-double v20, v14, v18

    mul-double v22, v4, v6

    add-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    mul-double v22, v14, v18

    mul-double v24, v4, v6

    sub-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->max(DD)D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v8, v0

    .line 376
    .local v8, "height":I
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    mul-float v21, v21, p5

    div-float v20, v20, v21

    sub-float v20, p2, v20

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    :try_start_0
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v9, v0

    .line 377
    .local v9, "left":I
    int-to-float v0, v8

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    mul-float v21, v21, p5

    div-float v20, v20, v21

    sub-float v20, p3, v20

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v16, v0

    .line 378
    .local v16, "top":I
    int-to-float v0, v9

    move/from16 v20, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v21, v21, p5

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v12, v0

    .line 379
    .local v12, "right":I
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v8

    move/from16 v21, v0

    div-float v21, v21, p5

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v2, v0

    .line 382
    .local v2, "bottom":I
    sget v20, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    shl-int v13, v20, p4

    .line 383
    .local v13, "size":I
    const/16 v20, 0x0

    div-int v21, v9, v13

    mul-int v21, v21, v13

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 384
    const/16 v20, 0x0

    div-int v21, v16, v13

    mul-int v21, v21, v13

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 385
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-static {v0, v12}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 386
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 388
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v9, v1, v12, v2}, Landroid/graphics/Rect;->set(IIII)V
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    .end local v2    # "bottom":I
    .end local v9    # "left":I
    .end local v12    # "right":I
    .end local v13    # "size":I
    .end local v16    # "top":I
    :goto_0
    return-void

    .line 389
    :catch_0
    move-exception v3

    .line 390
    .local v3, "e":Ljava/lang/ArithmeticException;
    invoke-virtual {v3}, Ljava/lang/ArithmeticException;->printStackTrace()V

    goto :goto_0
.end method

.method private getRange(Landroid/graphics/Rect;FFII)V
    .locals 7
    .param p1, "out"    # Landroid/graphics/Rect;
    .param p2, "cX"    # F
    .param p3, "cY"    # F
    .param p4, "level"    # I
    .param p5, "rotation"    # I

    .prologue
    .line 353
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    add-int/lit8 v2, p4, 0x1

    shl-int/2addr v1, v2

    int-to-float v1, v1

    div-float v5, v0, v1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/TileImageView;->getRange(Landroid/graphics/Rect;FFIFI)V

    .line 354
    return-void
.end method

.method private getTile(III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I

    .prologue
    .line 679
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-static {p1, p2, p3}, Lcom/sec/android/gallery3d/ui/TileImageView;->makeTileKey(III)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    return-object v0
.end method

.method public static getTileNumPerScreen()I
    .locals 3

    .prologue
    .line 1004
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayWidthPixels:I

    int-to-float v0, v0

    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTileSize()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sget v1, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayHeightPixels:I

    int-to-float v1, v1

    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTileSize()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public static getTileSize()I
    .locals 1

    .prologue
    .line 1000
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    return v0
.end method

.method public static isHighResolution()Z
    .locals 2

    .prologue
    const/16 v1, 0x500

    .line 158
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayHeightPixels:I

    if-ge v0, v1, :cond_0

    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayWidthPixels:I

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScreenNailAnimating()Z
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v0, v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v0, :cond_0

    .line 573
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->isAnimating()Z

    move-result v0

    .line 576
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v0, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private layoutTiles(FFFI)V
    .locals 25
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "scale"    # F
    .param p4, "rotation"    # I

    .prologue
    .line 249
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getWidth()I

    move-result v22

    .line 250
    .local v22, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getHeight()I

    move-result v14

    .line 258
    .local v14, "height":I
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v3, v3, p3

    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->floorLog2(F)I

    move-result v3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    invoke-static {v3, v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    .line 263
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    if-eq v3, v5, :cond_3

    .line 264
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileRange:Landroid/graphics/Rect;

    .line 265
    .local v4, "range":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/ui/TileImageView;->getRange(Landroid/graphics/Rect;FFIFI)V

    .line 266
    move/from16 v0, v22

    int-to-float v3, v0

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    iget v5, v4, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    sub-float v5, v5, p1

    mul-float v5, v5, p3

    add-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetX:I

    .line 267
    int-to-float v3, v14

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    iget v5, v4, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    sub-float v5, v5, p2

    mul-float v5, v5, p3

    add-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetY:I

    .line 268
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    shl-int/2addr v3, v5

    int-to-float v3, v3

    mul-float v3, v3, p3

    const/high16 v5, 0x3f400000    # 0.75f

    cmpl-float v3, v3, v5

    if-lez v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    add-int/lit8 v13, v3, -0x1

    .line 277
    .end local v4    # "range":Landroid/graphics/Rect;
    .local v13, "fromLevel":I
    :goto_0
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v3

    if-nez v3, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v3, :cond_5

    .line 278
    :cond_0
    move/from16 v0, v22

    int-to-float v3, v0

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    mul-float v5, p1, p3

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetX:I

    .line 279
    int-to-float v3, v14

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    mul-float v5, p2, p3

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetY:I

    .line 281
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->useCenterLargeThumbnail()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 282
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_5

    .line 283
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v7

    invoke-virtual {v3, v5, v6, v7}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->isMinLevel(FII)Z

    move-result v15

    .line 284
    .local v15, "isMinLevel":Z
    if-eqz v15, :cond_5

    .line 337
    .end local v15    # "isMinLevel":Z
    :cond_1
    :goto_1
    return-void

    .line 268
    .end local v13    # "fromLevel":I
    .restart local v4    # "range":Landroid/graphics/Rect;
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    goto :goto_0

    .line 271
    .end local v4    # "range":Landroid/graphics/Rect;
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    add-int/lit8 v13, v3, -0x2

    .line 272
    .restart local v13    # "fromLevel":I
    move/from16 v0, v22

    int-to-float v3, v0

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    mul-float v5, p1, p3

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetX:I

    .line 273
    int-to-float v3, v14

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    mul-float v5, p2, p3

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetY:I

    goto :goto_0

    .line 286
    :cond_4
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v3, :cond_5

    .line 287
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    move-object/from16 v5, p0

    move/from16 v6, v22

    move v7, v14

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/gallery3d/ui/TileImageView;->calculateLTNLevel(IIIII)I

    move-result v16

    .line 288
    .local v16, "level":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    move/from16 v0, v16

    if-gt v0, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    instance-of v3, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-nez v3, :cond_1

    .line 295
    .end local v16    # "level":I
    :cond_5
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    add-int/lit8 v5, v5, -0x2

    invoke-static {v13, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 296
    add-int/lit8 v3, v13, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 298
    .local v12, "endLevel":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveRange:[Landroid/graphics/Rect;

    .line 299
    .local v4, "range":[Landroid/graphics/Rect;
    move v9, v13

    .local v9, "i":I
    :goto_2
    if-ge v9, v12, :cond_6

    .line 300
    sub-int v3, v9, v13

    aget-object v6, v4, v3

    move-object/from16 v5, p0

    move/from16 v7, p1

    move/from16 v8, p2

    move/from16 v10, p4

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/gallery3d/ui/TileImageView;->getRange(Landroid/graphics/Rect;FFII)V

    .line 299
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 304
    :cond_6
    rem-int/lit8 v3, p4, 0x5a

    if-nez v3, :cond_1

    .line 306
    monitor-enter p0

    .line 307
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->clean()V

    .line 308
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->clean()V

    .line 309
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mBackgroundTileUploaded:Z

    .line 313
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v17

    .line 314
    .local v17, "n":I
    const/4 v9, 0x0

    :goto_3
    move/from16 v0, v17

    if-ge v9, v0, :cond_9

    .line 315
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v9}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 316
    .local v21, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileLevel:I

    move/from16 v16, v0

    .line 317
    .restart local v16    # "level":I
    move/from16 v0, v16

    if-lt v0, v13, :cond_7

    move/from16 v0, v16

    if-ge v0, v12, :cond_7

    sub-int v3, v16, v13

    aget-object v3, v4, v3

    move-object/from16 v0, v21

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    move-object/from16 v0, v21

    iget v6, v0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_8

    .line 319
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v9}, Landroid/util/LongSparseArray;->removeAt(I)V

    .line 320
    add-int/lit8 v9, v9, -0x1

    .line 321
    add-int/lit8 v17, v17, -0x1

    .line 322
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->recycleTile(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)V

    .line 314
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 325
    .end local v16    # "level":I
    .end local v21    # "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :cond_9
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    move v9, v13

    :goto_4
    if-ge v9, v12, :cond_c

    .line 328
    sget v3, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    shl-int v20, v3, v9

    .line 329
    .local v20, "size":I
    sub-int v3, v9, v13

    aget-object v18, v4, v3

    .line 330
    .local v18, "r":Landroid/graphics/Rect;
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    .local v24, "y":I
    move-object/from16 v0, v18

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    .local v11, "bottom":I
    :goto_5
    move/from16 v0, v24

    if-ge v0, v11, :cond_b

    .line 331
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    .local v23, "x":I
    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    .local v19, "right":I
    :goto_6
    move/from16 v0, v23

    move/from16 v1, v19

    if-ge v0, v1, :cond_a

    .line 332
    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v2, v9}, Lcom/sec/android/gallery3d/ui/TileImageView;->activateTile(III)V

    .line 331
    add-int v23, v23, v20

    goto :goto_6

    .line 325
    .end local v11    # "bottom":I
    .end local v17    # "n":I
    .end local v18    # "r":Landroid/graphics/Rect;
    .end local v19    # "right":I
    .end local v20    # "size":I
    .end local v23    # "x":I
    .end local v24    # "y":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 330
    .restart local v11    # "bottom":I
    .restart local v17    # "n":I
    .restart local v18    # "r":Landroid/graphics/Rect;
    .restart local v19    # "right":I
    .restart local v20    # "size":I
    .restart local v23    # "x":I
    .restart local v24    # "y":I
    :cond_a
    add-int v24, v24, v20

    goto :goto_5

    .line 327
    .end local v19    # "right":I
    .end local v23    # "x":I
    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 336
    .end local v11    # "bottom":I
    .end local v18    # "r":Landroid/graphics/Rect;
    .end local v20    # "size":I
    .end local v24    # "y":I
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V

    goto/16 :goto_1
.end method

.method private static makeTileKey(III)J
    .locals 7
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "level"    # I

    .prologue
    const/16 v6, 0x10

    .line 683
    int-to-long v0, p0

    .line 684
    .local v0, "result":J
    shl-long v2, v0, v6

    int-to-long v4, p1

    or-long v0, v2, v4

    .line 685
    shl-long v2, v0, v6

    int-to-long v4, p2

    or-long v0, v2, v4

    .line 686
    return-wide v0
.end method

.method private declared-synchronized obtainTile(III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I

    .prologue
    .line 640
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRecycledQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->pop()Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v0

    .line 641
    .local v0, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    if-eqz v0, :cond_0

    .line 642
    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 643
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->update(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 646
    .end local v0    # "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :goto_0
    monitor-exit p0

    return-object v0

    .restart local v0    # "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :cond_0
    :try_start_1
    new-instance v0, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .end local v0    # "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView;III)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 640
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static setDisplaySize(II)V
    .locals 0
    .param p0, "widthPixels"    # I
    .param p1, "heightPixels"    # I

    .prologue
    .line 162
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    sput p0, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayWidthPixels:I

    .line 166
    sput p1, Lcom/sec/android/gallery3d/ui/TileImageView;->sDisplayHeightPixels:I

    .line 167
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->setTileSize()V

    goto :goto_0
.end method

.method private static setTileSize()V
    .locals 1

    .prologue
    .line 171
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    if-nez v0, :cond_1

    .line 172
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_3

    .line 174
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->isHighResolution()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 175
    const/16 v0, 0x1fe

    sput v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    .line 188
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    const/16 v0, 0xfe

    sput v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    goto :goto_0

    .line 181
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->isHighResolution()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 182
    const/16 v0, 0x200

    sput v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    goto :goto_0

    .line 184
    :cond_4
    const/16 v0, 0x100

    sput v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    goto :goto_0
.end method

.method private uploadBackgroundTiles(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 4
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 581
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mBackgroundTileUploaded:Z

    .line 582
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    .line 583
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 584
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 585
    .local v2, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->isContentValid()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/TileImageView;->queueForDecode(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)V

    .line 583
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 587
    .end local v2    # "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :cond_1
    return-void
.end method


# virtual methods
.method public calculateLTNLevel(IIIII)I
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "imageW"    # I
    .param p4, "imageH"    # I
    .param p5, "levelCount"    # I

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 994
    int-to-float v2, p1

    int-to-float v3, p3

    div-float/2addr v2, v3

    int-to-float v3, p2

    int-to-float v4, p4

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 995
    .local v0, "scale1":F
    int-to-float v2, p1

    int-to-float v3, p4

    div-float/2addr v2, v3

    int-to-float v3, p2

    int-to-float v4, p3

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 996
    .local v1, "scale2":F
    div-float v2, v5, v0

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->floorLog2(F)I

    move-result v2

    invoke-static {v2, v6, p5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    div-float v3, v5, v1

    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->floorLog2(F)I

    move-result v3

    invoke-static {v3, v6, p5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    return v2
.end method

.method decodeTile(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)Z
    .locals 4
    .param p1, "tile"    # Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .prologue
    const/4 v1, 0x0

    .line 616
    monitor-enter p0

    .line 617
    :try_start_0
    iget v2, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    monitor-exit p0

    move v0, v1

    .line 635
    :goto_0
    return v0

    .line 618
    :cond_0
    const/4 v2, 0x4

    iput v2, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 619
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->decode()Z

    move-result v0

    .line 621
    .local v0, "decodeComplete":Z
    monitor-enter p0

    .line 622
    :try_start_1
    iget v2, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    .line 623
    const/16 v2, 0x40

    iput v2, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 624
    iget-object v2, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 627
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget-object v3, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BitmapReuseFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 629
    const/4 v2, 0x0

    iput-object v2, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 631
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRecycledQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->push(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)Z

    .line 632
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v0, v1

    goto :goto_0

    .line 619
    .end local v0    # "decodeComplete":Z
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 634
    .restart local v0    # "decodeComplete":Z
    :cond_2
    if-eqz v0, :cond_3

    const/16 v1, 0x8

    :goto_1
    :try_start_3
    iput v1, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 635
    monitor-exit p0

    goto :goto_0

    .line 636
    :catchall_1
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 634
    :cond_3
    const/16 v1, 0x10

    goto :goto_1
.end method

.method public drawTile(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIFFF)V
    .locals 13
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "tx"    # I
    .param p3, "ty"    # I
    .param p4, "level"    # I
    .param p5, "x"    # F
    .param p6, "y"    # F
    .param p7, "length"    # F

    .prologue
    .line 725
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mSourceRect:Landroid/graphics/RectF;

    .line 726
    .local v6, "source":Landroid/graphics/RectF;
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTargetRect:Landroid/graphics/RectF;

    .line 727
    .local v7, "target":Landroid/graphics/RectF;
    add-float v9, p5, p7

    add-float v10, p6, p7

    move/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v7, v0, v1, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 728
    const/4 v9, 0x0

    const/4 v10, 0x0

    sget v11, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    int-to-float v11, v11

    sget v12, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    int-to-float v12, v12

    invoke-virtual {v6, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 730
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTile(III)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v8

    .line 731
    .local v8, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    if-eqz v8, :cond_4

    .line 732
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->isContentValid()Z

    move-result v9

    if-nez v9, :cond_0

    .line 733
    iget v9, v8, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/16 v10, 0x8

    if-ne v9, v10, :cond_3

    .line 735
    iget v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQuota:I

    if-lez v9, :cond_2

    .line 737
    iget v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQuota:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQuota:I

    .line 738
    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->updateContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 747
    :cond_0
    :goto_0
    invoke-virtual {p0, v8, p1, v6, v7}, Lcom/sec/android/gallery3d/ui/TileImageView;->drawTile(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 766
    :cond_1
    :goto_1
    return-void

    .line 740
    :cond_2
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    goto :goto_0

    .line 742
    :cond_3
    iget v9, v8, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/16 v10, 0x10

    if-eq v9, v10, :cond_0

    .line 743
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    .line 744
    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/ui/TileImageView;->queueForDecode(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)V

    goto :goto_0

    .line 750
    :cond_4
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    instance-of v9, v9, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-nez v9, :cond_1

    .line 752
    :cond_5
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->isFilmMode()Z

    move-result v9

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget v10, v10, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->LARGETHUMBNAIL_CENTER:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->readyToDraw(I)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v2, 0x1

    .line 756
    .local v2, "readyToDrawLTN":Z
    :goto_2
    if-nez v2, :cond_1

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v9, :cond_1

    .line 757
    sget v9, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    shl-int v5, v9, p4

    .line 758
    .local v5, "size":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    int-to-float v10, v10

    div-float v3, v9, v10

    .line 759
    .local v3, "scaleX":F
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    int-to-float v10, v10

    div-float v4, v9, v10

    .line 760
    .local v4, "scaleY":F
    int-to-float v9, p2

    mul-float/2addr v9, v3

    move/from16 v0, p3

    int-to-float v10, v0

    mul-float/2addr v10, v4

    add-int v11, p2, v5

    int-to-float v11, v11

    mul-float/2addr v11, v3

    add-int v12, p3, v5

    int-to-float v12, v12

    mul-float/2addr v12, v4

    invoke-virtual {v6, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 762
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9, p1, v6, v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 752
    .end local v2    # "readyToDrawLTN":Z
    .end local v3    # "scaleX":F
    .end local v4    # "scaleY":F
    .end local v5    # "size":I
    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method

.method drawTile(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 8
    .param p1, "tile"    # Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "source"    # Landroid/graphics/RectF;
    .param p4, "target"    # Landroid/graphics/RectF;

    .prologue
    const/4 v7, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x40000000    # 2.0f

    .line 773
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->isContentValid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 774
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_1

    .line 775
    :cond_0
    invoke-virtual {p3, v3, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 778
    :cond_1
    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->needToAnimate()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 780
    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    iget-object v1, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->getProgress()F

    move-result v5

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V

    .line 783
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    .line 788
    :goto_1
    const/4 v0, 0x1

    .line 793
    :goto_2
    return v0

    .line 785
    :cond_2
    invoke-interface {p2, p1, p3, p4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_1

    .line 792
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->getParentTile()Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v6

    .line 793
    .local v6, "parent":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    if-nez v6, :cond_4

    move v0, v7

    goto :goto_2

    .line 794
    :cond_4
    iget v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    iget v1, v6, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mX:I

    if-ne v0, v1, :cond_5

    .line 795
    iget v0, p3, Landroid/graphics/RectF;->left:F

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->left:F

    .line 796
    iget v0, p3, Landroid/graphics/RectF;->right:F

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->right:F

    .line 801
    :goto_3
    iget v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    iget v1, v6, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mY:I

    if-ne v0, v1, :cond_6

    .line 802
    iget v0, p3, Landroid/graphics/RectF;->top:F

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->top:F

    .line 803
    iget v0, p3, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->bottom:F

    .line 808
    :goto_4
    move-object p1, v6

    .line 809
    goto :goto_0

    .line 798
    :cond_5
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    int-to-float v0, v0

    iget v1, p3, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->left:F

    .line 799
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    int-to-float v0, v0

    iget v1, p3, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->right:F

    goto :goto_3

    .line 805
    :cond_6
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    int-to-float v0, v0

    iget v1, p3, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->top:F

    .line 806
    sget v0, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    int-to-float v0, v0

    iget v1, p3, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p3, Landroid/graphics/RectF;->bottom:F

    goto :goto_4
.end method

.method public freeTextures()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 431
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mIsTextureFreed:Z

    .line 433
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileDecoder:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v4, :cond_0

    .line 434
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileDecoder:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 435
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileDecoder:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    .line 436
    iput-object v6, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileDecoder:Lcom/sec/android/gallery3d/util/Future;

    .line 439
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    .line 440
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 441
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v4, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 442
    .local v2, "texture":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->recycle()V

    .line 440
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 444
    .end local v2    # "texture":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/util/LongSparseArray;->clear()V

    .line 445
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileRange:Landroid/graphics/Rect;

    invoke-virtual {v4, v5, v5, v5, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 447
    monitor-enter p0

    .line 448
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->clean()V

    .line 449
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->clean()V

    .line 450
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRecycledQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->pop()Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v3

    .line 451
    .local v3, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :goto_1
    if-eqz v3, :cond_2

    .line 452
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->recycle()V

    .line 453
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRecycledQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->pop()Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v3

    goto :goto_1

    .line 455
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/ui/TileImageView;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 457
    return-void

    .line 455
    .end local v3    # "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public getImageCenter(Landroid/graphics/Point;)V
    .locals 7
    .param p1, "center"    # Landroid/graphics/Point;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getWidth()I

    move-result v3

    .line 398
    .local v3, "viewW":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getHeight()I

    move-result v2

    .line 404
    .local v2, "viewH":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    rem-int/lit16 v4, v4, 0xb4

    if-nez v4, :cond_0

    .line 405
    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    sub-float v1, v4, v5

    .line 406
    .local v1, "distW":F
    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    sub-float v0, v4, v5

    .line 414
    .local v0, "distH":F
    :goto_0
    int-to-float v4, v3

    div-float/2addr v4, v6

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iput v4, p1, Landroid/graphics/Point;->x:I

    .line 415
    int-to-float v4, v2

    div-float/2addr v4, v6

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iput v4, p1, Landroid/graphics/Point;->y:I

    .line 416
    return-void

    .line 408
    .end local v0    # "distH":F
    .end local v1    # "distW":F
    :cond_0
    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    sub-float v1, v4, v5

    .line 409
    .restart local v1    # "distW":F
    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    iget v5, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    sub-float v0, v4, v5

    .restart local v0    # "distH":F
    goto :goto_0
.end method

.method public invalidateScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V
    .locals 0
    .param p1, "snail"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    .line 989
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/TileImageView;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 990
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V

    .line 991
    return-void
.end method

.method protected declared-synchronized invalidateTiles()V
    .locals 4

    .prologue
    .line 340
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->clean()V

    .line 341
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->clean()V

    .line 344
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    .line 345
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 346
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .line 347
    .local v2, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/TileImageView;->recycleTile(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)V

    .line 345
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 349
    .end local v2    # "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mActiveTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    monitor-exit p0

    return-void

    .line 340
    .end local v0    # "i":I
    .end local v1    # "n":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public notifyModelInvalidated()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidateTiles()V

    .line 213
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    if-nez v0, :cond_0

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 215
    iput v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    .line 216
    iput v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    .line 217
    iput v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    .line 224
    :goto_0
    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    iget v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    iget v2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/TileImageView;->layoutTiles(FFFI)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V

    .line 226
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/TileImageView;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    .line 221
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    .line 222
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getLevelCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 231
    invoke-super/range {p0 .. p5}, Lcom/sec/android/gallery3d/ui/GLView;->onLayout(ZIIII)V

    .line 232
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    iget v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    iget v2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/TileImageView;->layoutTiles(FFFI)V

    .line 233
    :cond_0
    return-void
.end method

.method public prepareTextures()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 460
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileDecoder:Lcom/sec/android/gallery3d/util/Future;

    if-nez v1, :cond_0

    .line 461
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    new-instance v2, Lcom/sec/android/gallery3d/ui/TileImageView$TileDecoder;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileDecoder;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/ui/TileImageView$1;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileDecoder:Lcom/sec/android/gallery3d/util/Future;

    .line 463
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mIsTextureFreed:Z

    if-eqz v1, :cond_1

    .line 464
    iget v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    iget v2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    iget v4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/TileImageView;->layoutTiles(FFFI)V

    .line 465
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mIsTextureFreed:Z

    .line 466
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    if-nez v1, :cond_2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/TileImageView;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 468
    :cond_1
    return-void

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    goto :goto_0
.end method

.method declared-synchronized queueForDecode(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)V
    .locals 4
    .param p1, "tile"    # Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .prologue
    const/4 v2, 0x2

    .line 604
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 605
    const/4 v0, 0x2

    iput v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 606
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->push(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 613
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 609
    :cond_1
    :try_start_1
    iget v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecodeQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->mHead:Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->access$300(Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;)Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v0

    if-nez v0, :cond_0

    .line 610
    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    iget v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    iget v2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/TileImageView;->layoutTiles(FFFI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method queueForUpload(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)V
    .locals 3
    .param p1, "tile"    # Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .prologue
    .line 591
    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mAlphablendingAnimation:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimation;->reset()V

    .line 595
    :cond_0
    monitor-enter p0

    .line 596
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->push(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)Z

    .line 597
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 598
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileUploader:Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;->mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 599
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileUploader:Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->addOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    .line 601
    :cond_1
    return-void

    .line 597
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method declared-synchronized recycleTile(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)V
    .locals 2
    .param p1, "tile"    # Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    .prologue
    .line 650
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 651
    const/16 v0, 0x20

    iput v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 663
    :goto_0
    monitor-exit p0

    return-void

    .line 654
    :cond_0
    const/16 v0, 0x40

    :try_start_1
    iput v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    .line 655
    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 658
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget-object v1, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BitmapReuseFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 660
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 662
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRecycledQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->push(Lcom/sec/android/gallery3d/ui/TileImageView$Tile;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 650
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 34
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 474
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQuota:I

    .line 475
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    .line 477
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevel:I

    .line 478
    .local v11, "level":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    move/from16 v33, v0

    .line 479
    .local v33, "rotation":I
    const/16 v28, 0x0

    .line 480
    .local v28, "flags":I
    if-eqz v33, :cond_0

    or-int/lit8 v28, v28, 0x2

    .line 482
    :cond_0
    if-eqz v28, :cond_1

    .line 483
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 484
    if-eqz v33, :cond_1

    .line 485
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getWidth()I

    move-result v2

    div-int/lit8 v26, v2, 0x2

    .local v26, "centerX":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->getHeight()I

    move-result v2

    div-int/lit8 v27, v2, 0x2

    .line 486
    .local v27, "centerY":I
    move/from16 v0, v26

    int-to-float v2, v0

    move/from16 v0, v27

    int-to-float v3, v0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 487
    move/from16 v0, v33

    int-to-float v2, v0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v6, v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->rotate(FFFF)V

    .line 488
    move/from16 v0, v26

    neg-int v2, v0

    int-to-float v2, v2

    move/from16 v0, v27

    neg-int v3, v0

    int-to-float v3, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 492
    .end local v26    # "centerX":I
    .end local v27    # "centerY":I
    :cond_1
    :try_start_0
    sget v2, Lcom/sec/android/gallery3d/ui/TileImageView;->sTileSize:I

    shl-int v12, v2, v11

    .line 495
    .local v12, "size":I
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 497
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetX:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetY:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    move/from16 v0, v33

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v11}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFFLcom/sec/android/gallery3d/ui/ScreenNail;I)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    .line 502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-nez v2, :cond_3

    .line 503
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 560
    if-eqz v28, :cond_2

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 568
    :cond_2
    :goto_0
    return-void

    .line 505
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->useCenterLargeThumbnail()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 506
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    if-nez v2, :cond_4

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V

    .line 510
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v9

    invoke-virtual {v2, v3, v6, v9}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->isMinLevel(FII)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v30

    .line 512
    .local v30, "isMinLevel":Z
    if-eqz v30, :cond_5

    .line 560
    if-eqz v28, :cond_2

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    goto :goto_0

    .line 517
    .end local v30    # "isMinLevel":Z
    :cond_5
    :try_start_2
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-nez v2, :cond_7

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v2, v11}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->isLevelChanged(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 518
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    instance-of v2, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v2, :cond_7

    .line 519
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetX:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetY:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    move/from16 v0, v33

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v11}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFFLcom/sec/android/gallery3d/ui/ScreenNail;I)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    .line 524
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    if-nez v2, :cond_6

    .line 525
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 560
    :cond_6
    if-eqz v28, :cond_2

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    goto/16 :goto_0

    .line 531
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mLevelCount:I

    if-eq v11, v2, :cond_f

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->isScreenNailAnimating()Z

    move-result v2

    if-nez v2, :cond_f

    .line 532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_8

    .line 533
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/ScreenNail;->noDraw()V

    .line 536
    :cond_8
    int-to-float v2, v12

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    mul-float v13, v2, v3

    .line 537
    .local v13, "length":F
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v2, :cond_b

    .line 538
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileRange:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetX:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRealOffsetY:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetX:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetY:I

    move/from16 v17, v0

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    invoke-virtual/range {v7 .. v17}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->drawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIIII)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 560
    .end local v13    # "length":F
    :cond_9
    :goto_1
    if-eqz v28, :cond_a

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 563
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRenderComplete:Z

    if-eqz v2, :cond_10

    .line 564
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mBackgroundTileUploaded:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-nez v2, :cond_2

    invoke-direct/range {p0 .. p1}, Lcom/sec/android/gallery3d/ui/TileImageView;->uploadBackgroundTiles(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    goto/16 :goto_0

    .line 539
    .restart local v13    # "length":F
    :cond_b
    :try_start_4
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v2, :cond_d

    .line 540
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileRange:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetX:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetY:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    move/from16 v25, v0

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move/from16 v18, v11

    move/from16 v19, v12

    invoke-virtual/range {v14 .. v25}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->centerDrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIILcom/sec/android/gallery3d/ui/ScreenNail;II)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 560
    .end local v12    # "size":I
    .end local v13    # "length":F
    :catchall_0
    move-exception v2

    if-eqz v28, :cond_c

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    :cond_c
    throw v2

    .line 542
    .restart local v12    # "size":I
    .restart local v13    # "length":F
    :cond_d
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mTileRange:Landroid/graphics/Rect;

    move-object/from16 v32, v0

    .line 543
    .local v32, "r":Landroid/graphics/Rect;
    move-object/from16 v0, v32

    iget v5, v0, Landroid/graphics/Rect;->top:I

    .local v5, "ty":I
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_2
    move-object/from16 v0, v32

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    if-ge v5, v2, :cond_9

    .line 544
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetY:I

    int-to-float v2, v2

    move/from16 v0, v29

    int-to-float v3, v0

    mul-float/2addr v3, v13

    add-float v8, v2, v3

    .line 545
    .local v8, "y":F
    move-object/from16 v0, v32

    iget v4, v0, Landroid/graphics/Rect;->left:I

    .local v4, "tx":I
    const/16 v31, 0x0

    .local v31, "j":I
    :goto_3
    move-object/from16 v0, v32

    iget v2, v0, Landroid/graphics/Rect;->right:I

    if-ge v4, v2, :cond_e

    .line 546
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetX:I

    int-to-float v2, v2

    move/from16 v0, v31

    int-to-float v3, v0

    mul-float/2addr v3, v13

    add-float v7, v2, v3

    .local v7, "x":F
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move v6, v11

    move v9, v13

    .line 547
    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/gallery3d/ui/TileImageView;->drawTile(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIFFF)V

    .line 545
    add-int/2addr v4, v12

    add-int/lit8 v31, v31, 0x1

    goto :goto_3

    .line 543
    .end local v7    # "x":F
    :cond_e
    add-int/2addr v5, v12

    add-int/lit8 v29, v29, 0x1

    goto :goto_2

    .line 551
    .end local v4    # "tx":I
    .end local v5    # "ty":I
    .end local v8    # "y":F
    .end local v13    # "length":F
    .end local v29    # "i":I
    .end local v31    # "j":I
    .end local v32    # "r":Landroid/graphics/Rect;
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_9

    .line 552
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetX:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mOffsetY:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageWidth:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v18

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mImageHeight:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v15, p1

    invoke-interface/range {v14 .. v19}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 555
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->isScreenNailAnimating()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 556
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 566
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V

    goto/16 :goto_0
.end method

.method public setModel(Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;)V
    .locals 0
    .param p1, "model"    # Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mModel:Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;

    .line 204
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->notifyModelInvalidated()V

    .line 205
    :cond_0
    return-void
.end method

.method public setPosition(FFFI)Z
    .locals 1
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "scale"    # F
    .param p4, "rotation"    # I

    .prologue
    .line 419
    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    if-ne v0, p4, :cond_0

    .line 420
    const/4 v0, 0x0

    .line 427
    :goto_0
    return v0

    .line 421
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterX:F

    .line 422
    iput p2, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mCenterY:F

    .line 423
    iput p3, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScale:F

    .line 424
    iput p4, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mRotation:I

    .line 425
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/ui/TileImageView;->layoutTiles(FFFI)V

    .line 426
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/TileImageView;->invalidate()V

    .line 427
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V
    .locals 0
    .param p1, "s"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/TileImageView;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 209
    return-void
.end method
