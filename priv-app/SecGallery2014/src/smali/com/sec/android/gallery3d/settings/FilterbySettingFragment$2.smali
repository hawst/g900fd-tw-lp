.class Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;
.super Ljava/lang/Object;
.source "FilterbySettingFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$100(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 102
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 106
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$100(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 107
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$100(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 106
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$100(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    move-object v1, v2

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 111
    .local v1, "preference":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 114
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 116
    .end local v1    # "preference":Landroid/preference/CheckBoxPreference;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$100(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 117
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .param p6, "arg4"    # Z
    .param p7, "arg5"    # Z
    .param p8, "arg6"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 96
    .local v0, "item":Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->access$100(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method
