.class public Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
.super Ljava/lang/Object;
.source "RecogFaceSnsInfo.java"


# instance fields
.field public mAccountId:Ljava/lang/String;

.field public mAccountKey:Ljava/lang/String;

.field public mAccountName:Ljava/lang/String;

.field public mAccountType:Ljava/lang/String;

.field public mDescription:Ljava/lang/String;

.field public mMessageId:Ljava/lang/String;

.field public mResPackage:Ljava/lang/String;

.field public mStory:Ljava/lang/String;

.field public mText:Ljava/lang/String;

.field public mTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mMessageId:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountName:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountId:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountKey:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountType:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mResPackage:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mDescription:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mStory:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mTime:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "messageId"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "accountId"    # Ljava/lang/String;
    .param p4, "accountKey"    # Ljava/lang/String;
    .param p5, "accountType"    # Ljava/lang/String;
    .param p6, "resPackage"    # Ljava/lang/String;
    .param p7, "text"    # Ljava/lang/String;
    .param p8, "description"    # Ljava/lang/String;
    .param p9, "story"    # Ljava/lang/String;
    .param p10, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mMessageId:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountName:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountId:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountKey:Ljava/lang/String;

    .line 37
    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mAccountType:Ljava/lang/String;

    .line 38
    iput-object p6, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mResPackage:Ljava/lang/String;

    .line 39
    iput-object p7, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mText:Ljava/lang/String;

    .line 40
    iput-object p8, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mDescription:Ljava/lang/String;

    .line 41
    iput-object p9, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mStory:Ljava/lang/String;

    .line 42
    iput-object p10, p0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;->mTime:Ljava/lang/String;

    .line 43
    return-void
.end method
