.class Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts$Stub;
.source "FacebookService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/sns/FacebookService;->getLatestData(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

.field final synthetic val$lookupKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/sns/FacebookService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->val$lookupKey:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;)V
    .locals 11
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "posts"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 212
    if-nez p6, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mMessage:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mDescription:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mStory:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v9, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v9, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCreatedTime:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->val$lookupKey:Ljava/lang/String;

    invoke-direct/range {v1 .. v10}, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;-><init>(Lcom/sec/android/gallery3d/remote/sns/FacebookService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .local v1, "userData":Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mCache:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->val$lookupKey:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSNSRefreshListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->access$000(Lcom/sec/android/gallery3d/remote/sns/FacebookService;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 224
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSNSRefreshListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->access$000(Lcom/sec/android/gallery3d/remote/sns/FacebookService;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;->onSnsInfoUpdated()V

    goto :goto_0
.end method
