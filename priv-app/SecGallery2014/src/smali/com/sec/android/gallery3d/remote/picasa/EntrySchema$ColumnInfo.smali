.class public final Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
.super Ljava/lang/Object;
.source "EntrySchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ColumnInfo"
.end annotation


# instance fields
.field public final extraSql:Ljava/lang/String;

.field public final field:Ljava/lang/reflect/Field;

.field public final fullText:Z

.field public final indexed:Z

.field public final name:Ljava/lang/String;

.field public final projectionIndex:I

.field public final type:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IZZLjava/lang/reflect/Field;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "indexed"    # Z
    .param p4, "fullText"    # Z
    .param p5, "field"    # Ljava/lang/reflect/Field;
    .param p6, "projectionIndex"    # I

    .prologue
    .line 462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->extraSql:Ljava/lang/String;

    .line 463
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    .line 464
    iput p2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->type:I

    .line 465
    iput-boolean p3, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->indexed:Z

    .line 466
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->fullText:Z

    .line 467
    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->field:Ljava/lang/reflect/Field;

    .line 468
    iput p6, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->projectionIndex:I

    .line 469
    return-void
.end method


# virtual methods
.method public isId()Z
    .locals 2

    .prologue
    .line 472
    const-string v0, "_id"

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
