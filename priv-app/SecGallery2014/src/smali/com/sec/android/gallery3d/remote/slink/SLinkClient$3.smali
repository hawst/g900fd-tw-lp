.class Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;
.super Ljava/lang/Object;
.source "SLinkClient.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->setLoaderManager(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 9
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateLoader : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v7, 0x0

    .line 110
    .local v7, "loader":Landroid/content/CursorLoader;
    :try_start_0
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string/jumbo v4, "transport_type == ? AND (transport_type != ? AND network_mode != ? AND physical_type != ?)"

    sget-object v5, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->sDeviceWhereArgs:[Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v7    # "loader":Landroid/content/CursorLoader;
    .local v0, "loader":Landroid/content/CursorLoader;
    const-wide/16 v2, 0x7d0

    :try_start_1
    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 117
    :goto_0
    return-object v0

    .line 114
    .end local v0    # "loader":Landroid/content/CursorLoader;
    .restart local v7    # "loader":Landroid/content/CursorLoader;
    :catch_0
    move-exception v8

    move-object v0, v7

    .line 115
    .end local v7    # "loader":Landroid/content/CursorLoader;
    .restart local v0    # "loader":Landroid/content/CursorLoader;
    .local v8, "t":Ljava/lang/Throwable;
    :goto_1
    invoke-virtual {v8}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 114
    .end local v8    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v8

    goto :goto_1
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onLoadFinished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentObserver:Landroid/database/ContentObserver;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$300(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/database/ContentObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentObserver:Landroid/database/ContentObserver;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$300(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/database/ContentObserver;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 131
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 104
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onLoaderReset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    return-void
.end method
