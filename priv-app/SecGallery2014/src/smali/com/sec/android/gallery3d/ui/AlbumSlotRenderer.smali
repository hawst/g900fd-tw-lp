.class public Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;
.super Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;
.source "AlbumSlotRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$1;,
        Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$MyDataModelListener;,
        Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;
    }
.end annotation


# static fields
.field private static final CACHE_SIZE:I = 0x60

.field private static final TAG:Ljava/lang/String; = "AlbumView"


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAnimatePressedUp:Z

.field private mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

.field private mFocusedIndex:I

.field private mGenericFocusIndex:I

.field private mHighlightItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mInSelectionMode:Z

.field private final mPlaceholderColor:I

.field private mPressedIndex:I

.field private final mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSelectionPathSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mSlotFilter:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;

.field private final mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

.field private final mWaitLoadingTexture:Lcom/sec/android/gallery3d/glrenderer/ColorTexture;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SlotView;Lcom/sec/android/gallery3d/ui/SelectionManager;I)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "slotView"    # Lcom/sec/android/gallery3d/ui/SlotView;
    .param p3, "selectionManager"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p4, "placeholderColor"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;-><init>(Landroid/content/Context;)V

    .line 53
    iput v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPressedIndex:I

    .line 56
    iput v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mFocusedIndex:I

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mHighlightItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    .line 62
    iput v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mGenericFocusIndex:I

    .line 67
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 68
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    .line 69
    iput-object p3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 70
    iput p4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPlaceholderColor:I

    .line 72
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ColorTexture;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPlaceholderColor:I

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/ColorTexture;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mWaitLoadingTexture:Lcom/sec/android/gallery3d/glrenderer/ColorTexture;

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mWaitLoadingTexture:Lcom/sec/android/gallery3d/glrenderer/ColorTexture;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/gallery3d/glrenderer/ColorTexture;->setSize(II)V

    .line 74
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;)Lcom/sec/android/gallery3d/ui/SlotView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    return-object v0
.end method

.method private static checkTexture(Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;
    .locals 1
    .param p0, "texture"    # Lcom/sec/android/gallery3d/glrenderer/Texture;

    .prologue
    .line 193
    instance-of v0, p0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->isReady()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    .end local p0    # "texture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :cond_0
    return-object p0
.end method

.method private renderOverlay(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ILcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;II)I
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "index"    # I
    .param p3, "entry"    # Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 351
    const/4 v0, 0x0

    .line 352
    .local v0, "renderRequestFlags":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPressedIndex:I

    if-ne v1, p2, :cond_3

    .line 353
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mAnimatePressedUp:Z

    if-eqz v1, :cond_2

    .line 354
    invoke-virtual {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawPressedUpFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 355
    or-int/lit8 v0, v0, 0x2

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->isPressedUpFrameFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 357
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mAnimatePressedUp:Z

    .line 358
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPressedIndex:I

    .line 370
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mFocusedIndex:I

    if-ne v1, p2, :cond_5

    .line 371
    invoke-virtual {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawFocusFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 376
    :cond_1
    :goto_1
    return v0

    .line 361
    :cond_2
    invoke-virtual {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawPressedFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto :goto_0

    .line 363
    :cond_3
    iget-object v1, p3, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->path:Lcom/sec/android/gallery3d/data/Path;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mHighlightItemPath:Lcom/sec/android/gallery3d/data/Path;

    iget-object v2, p3, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->path:Lcom/sec/android/gallery3d/data/Path;

    if-ne v1, v2, :cond_4

    .line 364
    invoke-virtual {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawSelectedFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto :goto_0

    .line 365
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mInSelectionMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v2, p3, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isItemSelected(Lcom/sec/android/gallery3d/data/Path;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366
    invoke-virtual {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawSelectedFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto :goto_0

    .line 372
    :cond_5
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mGenericFocusIndex:I

    if-ne v1, p2, :cond_1

    .line 373
    invoke-virtual {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawGenericFocusFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto :goto_1
.end method


# virtual methods
.method public addSelectionIndex(I)V
    .locals 5
    .param p1, "slotIndex"    # I

    .prologue
    .line 95
    if-gez p1, :cond_0

    .line 108
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mInSelectionMode:Z

    .line 100
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    monitor-enter v3

    .line 101
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->get(I)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    move-result-object v0

    .line 102
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-eqz v0, :cond_1

    .line 103
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 104
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_1

    .line 105
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 107
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public checkIfFilmStripInitiated()Z
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->checkIfFimStripInitiated()Z

    move-result v0

    return v0
.end method

.method public clearAllSelectionIndex()V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mInSelectionMode:Z

    .line 159
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 161
    monitor-exit v1

    .line 162
    return-void

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->destroy()V

    .line 401
    return-void
.end method

.method public getFocusIndex()I
    .locals 1

    .prologue
    .line 437
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mFocusedIndex:I

    return v0
.end method

.method public hasAddedSelectionIndex(I)Z
    .locals 5
    .param p1, "slotIndex"    # I

    .prologue
    .line 145
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    monitor-enter v3

    .line 146
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->get(I)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    move-result-object v0

    .line 147
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-eqz v0, :cond_0

    .line 148
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 149
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 150
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    monitor-exit v3

    .line 153
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 154
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public onSlotSizeChanged(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 417
    return-void
.end method

.method public onVisibleRangeChanged(II)V
    .locals 1
    .param p1, "visibleStart"    # I
    .param p2, "visibleEnd"    # I

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->setActiveWindow(II)V

    .line 412
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->pause()V

    .line 397
    return-void
.end method

.method public prepareDrawing()V
    .locals 0

    .prologue
    .line 405
    return-void
.end method

.method public removeSelectionIndex(I)V
    .locals 5
    .param p1, "slotIndex"    # I

    .prologue
    .line 134
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    monitor-enter v3

    .line 135
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->get(I)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    move-result-object v0

    .line 136
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-eqz v0, :cond_0

    .line 137
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 138
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 139
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 141
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    monitor-exit v3

    .line 142
    return-void

    .line 141
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public renderSlot(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)I
    .locals 13
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "index"    # I
    .param p3, "pass"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 201
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotFilter:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotFilter:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;

    invoke-interface {v2, p2}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;->acceptSlot(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v12, 0x0

    .line 346
    :goto_0
    return v12

    .line 203
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v2, p2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->get(I)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    move-result-object v8

    .line 204
    .local v8, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    if-nez v8, :cond_1

    .line 205
    const/4 v12, 0x0

    goto :goto_0

    .line 208
    :cond_1
    const/4 v12, 0x0

    .line 210
    .local v12, "renderRequestFlags":I
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->content:Lcom/sec/android/gallery3d/glrenderer/Texture;

    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->checkTexture(Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    move-result-object v4

    .line 211
    .local v4, "content":Lcom/sec/android/gallery3d/glrenderer/Texture;
    if-nez v4, :cond_19

    .line 212
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mWaitLoadingTexture:Lcom/sec/android/gallery3d/glrenderer/ColorTexture;

    .line 213
    const/4 v2, 0x1

    iput-boolean v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->isWaitDisplayed:Z

    .line 219
    :cond_2
    :goto_1
    iget v7, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->rotation:I

    move-object v2, p0

    move-object v3, p1

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/Texture;III)V

    .line 220
    instance-of v2, v4, Lcom/sec/android/gallery3d/glrenderer/FadeInTexture;

    if-eqz v2, :cond_3

    check-cast v4, Lcom/sec/android/gallery3d/glrenderer/FadeInTexture;

    .end local v4    # "content":Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/FadeInTexture;->isAnimating()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 222
    or-int/lit8 v12, v12, 0x2

    .line 225
    :cond_3
    iget v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->mediaType:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v2, :cond_4

    .line 226
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawVideoOverlay(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 229
    :cond_4
    iget-boolean v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->isPanorama:Z

    if-eqz v2, :cond_5

    .line 230
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawPanoramaIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 234
    :cond_5
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_18

    .line 235
    const/4 v11, 0x0

    .line 237
    .local v11, "iconCount":I
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v2, :cond_6

    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 238
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawPrivateIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 239
    add-int/lit8 v11, v11, 0x1

    .line 243
    :cond_6
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x2

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 244
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->draw3DMpoIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 245
    add-int/lit8 v11, v11, 0x1

    .line 248
    :cond_7
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x4

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 249
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x8

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v2

    if-nez v2, :cond_8

    .line 250
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawSPanoramaIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 251
    add-int/lit8 v11, v11, 0x1

    .line 256
    :cond_8
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x8

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 257
    :cond_9
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->draw3DPanoramaIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 258
    add-int/lit8 v11, v11, 0x1

    .line 261
    :cond_a
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x10

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 262
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawSoundSceneIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 263
    add-int/lit8 v11, v11, 0x1

    .line 266
    :cond_b
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x20

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v6, 0x40000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_c

    .line 268
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawCinePicIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 269
    add-int/lit8 v11, v11, 0x1

    .line 273
    :cond_c
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x800

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 274
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawMagicShotIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 275
    add-int/lit8 v11, v11, 0x1

    .line 278
    :cond_d
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x1000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 279
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawBestPhotoIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 280
    add-int/lit8 v11, v11, 0x1

    .line 283
    :cond_e
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x2000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 284
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawBestFaceIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 285
    add-int/lit8 v11, v11, 0x1

    .line 288
    :cond_f
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x4000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 289
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawEraserIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 290
    add-int/lit8 v11, v11, 0x1

    .line 293
    :cond_10
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v6, 0x8000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 294
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawDramaShotIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 295
    add-int/lit8 v11, v11, 0x1

    .line 298
    :cond_11
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v6, 0x10000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 299
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawPicMotionIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 300
    add-int/lit8 v11, v11, 0x1

    .line 303
    :cond_12
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v6, 0x400

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 304
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->draw3DTourIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 305
    add-int/lit8 v11, v11, 0x1

    .line 308
    :cond_13
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v6, 0x20000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 309
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawOutOfFocusIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 310
    add-int/lit8 v11, v11, 0x1

    .line 313
    :cond_14
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v6, 0x80000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 314
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawPhotoNoteIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 315
    add-int/lit8 v11, v11, 0x1

    .line 318
    :cond_15
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v6, 0x40000

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_16

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    if-eqz v2, :cond_16

    .line 319
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v11}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawSequenceIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 320
    add-int/lit8 v11, v11, 0x1

    .line 333
    :cond_16
    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/CameraShortcutImage;

    if-eqz v2, :cond_17

    .line 334
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawCameraShortcut(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 338
    :cond_17
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mInSelectionMode:Z

    if-eqz v2, :cond_18

    iget-object v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/CameraShortcutImage;

    if-nez v2, :cond_18

    .line 339
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->hasAddedSelectionIndex(I)Z

    move-result v2

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->drawCheckBoxIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIZ)V

    .end local v11    # "iconCount":I
    :cond_18
    move-object v5, p0

    move-object v6, p1

    move v7, p2

    move/from16 v9, p4

    move/from16 v10, p5

    .line 344
    invoke-direct/range {v5 .. v10}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->renderOverlay(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ILcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;II)I

    move-result v2

    or-int/2addr v12, v2

    .line 346
    goto/16 :goto_0

    .line 214
    .restart local v4    # "content":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :cond_19
    iget-boolean v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->isWaitDisplayed:Z

    if-eqz v2, :cond_2

    .line 215
    const/4 v2, 0x0

    iput-boolean v2, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->isWaitDisplayed:Z

    .line 216
    new-instance v4, Lcom/sec/android/gallery3d/glrenderer/FadeInTexture;

    .end local v4    # "content":Lcom/sec/android/gallery3d/glrenderer/Texture;
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPlaceholderColor:I

    iget-object v3, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-direct {v4, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/FadeInTexture;-><init>(ILcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    .line 217
    .restart local v4    # "content":Lcom/sec/android/gallery3d/glrenderer/Texture;
    iput-object v4, v8, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->content:Lcom/sec/android/gallery3d/glrenderer/Texture;

    goto/16 :goto_1
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->resume()V

    .line 393
    return-void
.end method

.method public selectAll(I)V
    .locals 5
    .param p1, "maxSlotIndex"    # I

    .prologue
    .line 111
    if-gez p1, :cond_0

    .line 125
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    monitor-enter v3

    .line 116
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 117
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-gt v0, p1, :cond_3

    .line 118
    const/4 v1, 0x0

    .line 119
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_1

    .line 120
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 121
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    if-eqz v1, :cond_2

    .line 122
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 124
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    monitor-exit v3

    goto :goto_0

    .end local v0    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setFocusIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mFocusedIndex:I

    if-ne v0, p1, :cond_0

    .line 428
    :goto_0
    return-void

    .line 426
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mFocusedIndex:I

    .line 427
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    goto :goto_0
.end method

.method public setGenericFocusIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 431
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mGenericFocusIndex:I

    if-ne v0, p1, :cond_0

    .line 434
    :goto_0
    return-void

    .line 432
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mGenericFocusIndex:I

    .line 433
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    goto :goto_0
.end method

.method public setHighlightItemPath(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mHighlightItemPath:Lcom/sec/android/gallery3d/data/Path;

    if-ne v0, p1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mHighlightItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    goto :goto_0
.end method

.method public setModel(Lcom/sec/android/gallery3d/app/AlbumDataLoader;)V
    .locals 4
    .param p1, "model"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    .prologue
    const/4 v3, 0x0

    .line 180
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->setListener(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->setSlotCount(I)Z

    .line 183
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .line 185
    :cond_0
    if-eqz p1, :cond_1

    .line 186
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v2, 0x60

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/AlbumDataLoader;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .line 187
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    new-instance v1, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$MyDataModelListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$MyDataModelListener;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$1;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->setListener(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->setSlotCount(I)Z

    .line 190
    :cond_1
    return-void
.end method

.method public setModel(Lcom/sec/android/gallery3d/app/AlbumDataLoader;Z)V
    .locals 4
    .param p1, "model"    # Lcom/sec/android/gallery3d/app/AlbumDataLoader;
    .param p2, "miniThumbMode"    # Z

    .prologue
    const/4 v3, 0x0

    .line 166
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->setListener(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->setSlotCount(I)Z

    .line 169
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .line 171
    :cond_0
    if-eqz p1, :cond_1

    .line 172
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v2, 0x60

    invoke-direct {v0, v1, p1, v2, p2}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/AlbumDataLoader;IZ)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    .line 173
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    new-instance v1, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$MyDataModelListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$MyDataModelListener;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$1;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->setListener(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->setSlotCount(I)Z

    .line 176
    :cond_1
    return-void
.end method

.method public setPressedIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPressedIndex:I

    if-ne v0, p1, :cond_0

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPressedIndex:I

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    goto :goto_0
.end method

.method public setPressedUp()V
    .locals 2

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mPressedIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mAnimatePressedUp:Z

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    goto :goto_0
.end method

.method public setSlotFilter(Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;)V
    .locals 0
    .param p1, "slotFilter"    # Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotFilter:Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer$SlotFilter;

    .line 421
    return-void
.end method

.method public setSlotPosition(I)V
    .locals 2
    .param p1, "startIndex"    # I

    .prologue
    .line 445
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView;->setStartIndex(I)V

    .line 446
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mDataWindow:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->getSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SlotView;->setSlotCount(I)Z

    .line 447
    return-void
.end method

.method public unselectAll()V
    .locals 2

    .prologue
    .line 128
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    monitor-enter v1

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlotRenderer;->mSelectionPathSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 130
    monitor-exit v1

    .line 131
    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
