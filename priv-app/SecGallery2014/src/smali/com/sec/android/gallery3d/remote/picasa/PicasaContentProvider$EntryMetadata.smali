.class final Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
.super Ljava/lang/Object;
.source "PicasaContentProvider.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EntryMetadata"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field public dateEdited:J

.field public displayIndex:I

.field public id:J

.field public survived:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 956
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->survived:Z

    .line 960
    return-void
.end method

.method public constructor <init>(JJI)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "dateEdited"    # J
    .param p5, "displayIndex"    # I

    .prologue
    .line 962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 956
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->survived:Z

    .line 963
    iput-wide p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->id:J

    .line 964
    iput-wide p3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->dateEdited:J

    .line 965
    iput p5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->displayIndex:I

    .line 966
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;)I
    .locals 4
    .param p1, "other"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    .prologue
    .line 969
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->id:J

    iget-wide v2, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->id:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 949
    check-cast p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->compareTo(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;)I

    move-result v0

    return v0
.end method
