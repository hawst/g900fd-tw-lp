.class public Lcom/sec/android/gallery3d/glcore/GlHandler;
.super Ljava/lang/Object;
.source "GlHandler.java"


# instance fields
.field public mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 1
    .param p1, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 10
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 11
    return-void
.end method


# virtual methods
.method public hasMessage(I)Z
    .locals 1
    .param p1, "command"    # I

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasCommand(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public onMessage(ILjava/lang/Object;III)V
    .locals 0
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 70
    return-void
.end method

.method public removeAllMessage()V
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Ljava/lang/Object;)V

    .line 15
    return-void
.end method

.method public removeMessage(I)V
    .locals 2
    .param p1, "command"    # I

    .prologue
    const/4 v1, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, v1, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 21
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 22
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 23
    return-void
.end method

.method public sendMessage(IIII)V
    .locals 2
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 33
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_MESSSAGE:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 34
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 35
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 36
    return-void
.end method

.method public sendMessage(IIIILjava/lang/Object;)V
    .locals 6
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;)V

    .line 42
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_MESSSAGE:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 43
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 44
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 45
    return-void
.end method

.method public sendMessageDelayed(IIIIJ)V
    .locals 9
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "msecDelay"    # J

    .prologue
    .line 51
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p5

    .line 52
    .local v6, "expTime":J
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIIJ)V

    .line 53
    .local v1, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_MESSSAGE:I

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 54
    iput-object p0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 56
    return-void
.end method

.method public sendMessageDelayed(IIIILjava/lang/Object;J)V
    .locals 8
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;
    .param p6, "msecDelay"    # J

    .prologue
    .line 62
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p6

    .line 63
    .local v6, "expTime":J
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;J)V

    .line 64
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_MESSSAGE:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 65
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 66
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 67
    return-void
.end method
