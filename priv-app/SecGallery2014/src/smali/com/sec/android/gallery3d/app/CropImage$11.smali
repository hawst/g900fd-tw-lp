.class Lcom/sec/android/gallery3d/app/CropImage$11;
.super Ljava/lang/Object;
.source "CropImage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/app/CropImage;->displaySetWallpaperDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CropImage;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/CropImage;)V
    .locals 0

    .prologue
    .line 2647
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CropImage$11;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v2, 0x1

    .line 2650
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage$11;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "set-as-wallpaper"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2651
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage$11;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "set-as-lockscreen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2653
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage$11;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # getter for: Lcom/sec/android/gallery3d/app/CropImage;->mKnox:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CropImage;->access$1700(Lcom/sec/android/gallery3d/app/CropImage;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v0, :cond_1

    .line 2654
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage$11;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->checkSim()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CropImage;->access$5200(Lcom/sec/android/gallery3d/app/CropImage;)V

    .line 2659
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage$11;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->setLockScreenSettingAndBroadCast()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CropImage;->access$1600(Lcom/sec/android/gallery3d/app/CropImage;)V

    .line 2660
    return-void

    .line 2657
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CropImage$11;->this$0:Lcom/sec/android/gallery3d/app/CropImage;

    # invokes: Lcom/sec/android/gallery3d/app/CropImage;->saveCropImageFile()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CropImage;->access$4700(Lcom/sec/android/gallery3d/app/CropImage;)V

    goto :goto_0
.end method
