.class public Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;
.super Landroid/preference/PreferenceCategory;
.source "FramePreferenceCategory.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final BLACK_COLOR:I = -0x1000000

.field private static final WHITE_COLOR:I = -0x1


# instance fields
.field private mFrameType:I

.field private mPrefChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private mPrefIds:[I

.field private mPrefLayoutRessPortraitOff:[I

.field private mPrefLayoutRessPortraitOn:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x4

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefIds:[I

    .line 36
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefLayoutRessPortraitOff:[I

    .line 42
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefLayoutRessPortraitOn:[I

    .line 48
    return-void

    .line 30
    :array_0
    .array-data 4
        0x7f0f01f0
        0x7f0f01f2
        0x7f0f01f1
        0x7f0f01f3
    .end array-data

    .line 36
    :array_1
    .array-data 4
        0x7f020581
        0x7f02057b
        0x7f020578
        0x7f02057e
    .end array-data

    .line 42
    :array_2
    .array-data 4
        0x7f020583
        0x7f02057d
        0x7f02057a
        0x7f020580
    .end array-data
.end method


# virtual methods
.method public getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 59
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f0300ba

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 63
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v7, 0x7f0f01ef

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 66
    .local v6, "textView":Landroid/widget/TextView;
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v7, :cond_1

    .line 67
    const/high16 v7, -0x1000000

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 72
    :goto_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefIds:[I

    array-length v2, v7

    .line 73
    .local v2, "idLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_3

    .line 74
    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefIds:[I

    aget v7, v7, v1

    const v8, 0x7f0f01f0

    sub-int v5, v7, v8

    .line 75
    .local v5, "layoutOffset":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefIds:[I

    aget v7, v7, v1

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 76
    .local v3, "imgView":Landroid/widget/ImageView;
    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mFrameType:I

    if-ne v7, v5, :cond_2

    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefLayoutRessPortraitOn:[I

    aget v0, v7, v1

    .line 78
    .local v0, "adjustedResourceId":I
    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 69
    .end local v0    # "adjustedResourceId":I
    .end local v1    # "i":I
    .end local v2    # "idLength":I
    .end local v3    # "imgView":Landroid/widget/ImageView;
    .end local v5    # "layoutOffset":I
    :cond_1
    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 77
    .restart local v1    # "i":I
    .restart local v2    # "idLength":I
    .restart local v3    # "imgView":Landroid/widget/ImageView;
    .restart local v5    # "layoutOffset":I
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefLayoutRessPortraitOff:[I

    aget v0, v7, v1

    goto :goto_2

    .line 81
    .end local v3    # "imgView":Landroid/widget/ImageView;
    .end local v5    # "layoutOffset":I
    :cond_3
    return-object p1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 91
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    if-nez v2, :cond_0

    .line 117
    :goto_0
    return-void

    .line 94
    :cond_0
    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 95
    .local v1, "pref":Landroid/preference/Preference;
    const-string v2, "preference_list_frame_type"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 96
    const/4 v0, 0x0

    .line 97
    .local v0, "frameType":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 114
    :goto_1
    iput v0, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mFrameType:I

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->notifyChanged()V

    .line 116
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    goto :goto_0

    .line 99
    :pswitch_0
    const/4 v0, 0x0

    .line 100
    goto :goto_1

    .line 102
    :pswitch_1
    const/4 v0, 0x1

    .line 103
    goto :goto_1

    .line 105
    :pswitch_2
    const/4 v0, 0x2

    .line 106
    goto :goto_1

    .line 108
    :pswitch_3
    const/4 v0, 0x3

    .line 109
    goto :goto_1

    .line 97
    :pswitch_data_0
    .packed-switch 0x7f0f01f0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setFrameType(I)V
    .locals 0
    .param p1, "frameType"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mFrameType:I

    .line 52
    return-void
.end method

.method public setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 0
    .param p1, "onPreferenceChangeListener"    # Landroid/preference/Preference$OnPreferenceChangeListener;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->mPrefChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 87
    return-void
.end method
