.class Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;
.super Ljava/lang/Object;
.source "TCloudVideo.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->getVideoURL()Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 6

    .prologue
    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->access$100(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->access$200(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    invoke-virtual {v2, v1, v4, v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getVideoUri(Landroid/content/Context;J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 217
    :goto_0
    return-object v1

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 217
    const/4 v1, 0x0

    goto :goto_0
.end method
