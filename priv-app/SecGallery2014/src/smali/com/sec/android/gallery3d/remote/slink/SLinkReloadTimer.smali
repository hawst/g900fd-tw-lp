.class public Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;
.super Ljava/lang/Object;
.source "SLinkReloadTimer.java"


# static fields
.field private static final DEFAULT_TIMEOUT:J = 0xbb8L

.field private static final MSG_SET_EXPIRED:I = 0x2

.field private static final MSG_WAIT:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIsExpired:Z

.field private mSLinkStorage:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;)V
    .locals 1
    .param p1, "sLinkStorage"    # Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mIsExpired:Z

    .line 25
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer$1;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mHandler:Landroid/os/Handler;

    .line 47
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mSLinkStorage:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    .line 48
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mIsExpired:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;)Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mSLinkStorage:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    return-object v0
.end method


# virtual methods
.method public getToken()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 66
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mIsExpired:Z

    if-eqz v1, :cond_0

    .line 67
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    const-string v2, "getToken : true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mIsExpired:Z

    .line 69
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    .line 72
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    const-string v2, "getToken : false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public hasToken()Z
    .locals 3

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mIsExpired:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mIsExpired:Z

    return v0
.end method

.method public setTimeout()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 51
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mIsExpired:Z

    if-eqz v0, :cond_0

    .line 52
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    const-string v1, "setTimeOut : Expired. ignore timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    const-string v1, "setTimeOut : already waiting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 61
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->TAG:Ljava/lang/String;

    const-string v1, "setTimeOut : send delayed message"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
