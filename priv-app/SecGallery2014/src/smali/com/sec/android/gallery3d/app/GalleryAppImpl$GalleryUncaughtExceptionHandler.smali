.class Lcom/sec/android/gallery3d/app/GalleryAppImpl$GalleryUncaughtExceptionHandler;
.super Ljava/lang/Object;
.source "GalleryAppImpl.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/GalleryAppImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GalleryUncaughtExceptionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)V
    .locals 0

    .prologue
    .line 668
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$GalleryUncaughtExceptionHandler;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;

    .prologue
    .line 668
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl$GalleryUncaughtExceptionHandler;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 671
    instance-of v2, p2, Ljava/lang/OutOfMemoryError;

    if-eqz v2, :cond_0

    .line 672
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 674
    .local v1, "pid":I
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/log/OOM_dump_gallery_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 679
    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 684
    .end local v1    # "pid":I
    :goto_0
    return-void

    .line 675
    .restart local v1    # "pid":I
    :catch_0
    move-exception v0

    .line 676
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 678
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 679
    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    .line 678
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 679
    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    throw v2

    .line 682
    .end local v1    # "pid":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$GalleryUncaughtExceptionHandler;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$900(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
