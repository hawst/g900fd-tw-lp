.class public Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;
.super Ljava/lang/Object;
.source "ShuffleAllSource.java"

# interfaces
.implements Lcom/sec/android/gallery3d/gadget/WidgetSource;


# static fields
.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final DATE_TAKEN:Ljava/lang/String; = "datetaken"

.field private static final LOCAL_IMAGE_ROOT:Lcom/sec/android/gallery3d/data/Path;

.field private static final MAX_PHOTO_COUNT:I = 0x80

.field private static final ORDER:Ljava/lang/String;

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SELECTION:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "LocalPhotoSource"


# instance fields
.field private mContentDirty:Z

.field private mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mPhotos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->CONTENT_URI:Landroid/net/Uri;

    .line 54
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->PROJECTION:[Ljava/lang/String;

    .line 55
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "count(*)"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->COUNT_PROJECTION:[Ljava/lang/String;

    .line 57
    const-string v0, "%s != %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "bucket_id"

    aput-object v2, v1, v3

    invoke-static {}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->getDownloadBucketId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->SELECTION:Ljava/lang/String;

    .line 59
    const-string v0, "%s DESC"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "datetaken"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->ORDER:Ljava/lang/String;

    .line 67
    const-string v0, "/local/image/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->LOCAL_IMAGE_ROOT:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    .line 65
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentDirty:Z

    .line 70
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 72
    new-instance v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource$1;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource$1;-><init>(Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentObserver:Landroid/database/ContentObserver;

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 81
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentDirty:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;)Lcom/sec/android/gallery3d/data/ContentListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;

    return-object v0
.end method

.method private static getDownloadBucketId()I
    .locals 2

    .prologue
    .line 251
    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "downloadsPath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBucketId(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method private getExponentialIndice(II)[I
    .locals 12
    .param p1, "total"    # I
    .param p2, "count"    # I

    .prologue
    .line 139
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 140
    .local v3, "random":Ljava/util/Random;
    if-le p2, p1, :cond_0

    move p2, p1

    .line 141
    :cond_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5, p2}, Ljava/util/HashSet;-><init>(I)V

    .line 142
    .local v5, "selected":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_1
    :goto_0
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v8

    if-ge v8, p2, :cond_2

    .line 143
    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    neg-double v8, v8

    int-to-double v10, p1

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v8, v10

    double-to-int v4, v8

    .line 144
    .local v4, "row":I
    if-ge v4, p1, :cond_1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    .end local v4    # "row":I
    :cond_2
    new-array v7, p2, [I

    .line 147
    .local v7, "values":[I
    const/4 v1, 0x0

    .line 148
    .local v1, "index":I
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 149
    .local v6, "value":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aput v6, v7, v1

    move v1, v2

    .line 150
    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_1

    .line 151
    .end local v6    # "value":I
    :cond_3
    return-object v7
.end method

.method private getPhotoCount(Landroid/content/ContentResolver;)I
    .locals 8
    .param p1, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v7, 0x0

    .line 155
    const/4 v6, 0x0

    .line 157
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->COUNT_PROJECTION:[Ljava/lang/String;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->SELECTION:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 159
    if-nez v6, :cond_0

    .line 164
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v7

    :goto_0
    return v0

    .line 161
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 162
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 164
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private isContentSound(I)Z
    .locals 13
    .param p1, "totalCount"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x80

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 188
    :goto_0
    return v11

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move v11, v10

    goto :goto_0

    .line 172
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .local v6, "builder":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    .line 174
    .local v9, "imageId":Ljava/lang/Long;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, ","

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :cond_2
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 177
    .end local v9    # "imageId":Ljava/lang/Long;
    :cond_3
    const/4 v7, 0x0

    .line 179
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->COUNT_PROJECTION:[Ljava/lang/String;

    const-string v3, "%s in (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v12, "_id"

    aput-object v12, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 183
    if-nez v7, :cond_4

    .line 188
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 185
    :cond_4
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 186
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-ne v0, v1, :cond_5

    move v0, v10

    .line 188
    :goto_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v11, v0

    goto :goto_0

    :cond_5
    move v0, v11

    .line 186
    goto :goto_2

    .line 188
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized close()V
    .locals 2

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentObserver:Landroid/database/ContentObserver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_0
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getContentUri(I)Landroid/net/Uri;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 94
    sget-object v0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImage(I)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 124
    .local v0, "image":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 128
    :goto_0
    return-object v1

    .line 125
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x20

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 126
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a7

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    .line 128
    :cond_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createWidgetBitmap(Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 133
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt p1, v2, :cond_0

    const/4 v2, 0x0

    .line 135
    :goto_0
    return-object v2

    .line 134
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 135
    .local v0, "id":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->LOCAL_IMAGE_ROOT:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0
.end method

.method public isDrm(I)Z
    .locals 9
    .param p1, "index"    # I

    .prologue
    const/4 v8, 0x0

    .line 103
    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 104
    .local v4, "id":J
    sget-object v7, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->LOCAL_IMAGE_ROOT:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v7, v4, v5}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 105
    .local v6, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v6, :cond_0

    move v7, v8

    .line 118
    :goto_0
    return v7

    .line 107
    :cond_0
    const/4 v3, 0x0

    .line 109
    .local v3, "image":Lcom/sec/android/gallery3d/data/MediaItem;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v7, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_1
    if-nez v3, :cond_1

    move v7, v8

    .line 114
    goto :goto_0

    .line 110
    :catch_0
    move-exception v2

    .line 111
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 115
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge p1, v7, :cond_2

    .line 116
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v7

    goto :goto_0

    :cond_2
    move v7, v8

    .line 118
    goto :goto_0
.end method

.method public reload()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 193
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentDirty:Z

    if-nez v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentDirty:Z

    .line 196
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    .line 197
    .local v13, "resolver":Landroid/content/ContentResolver;
    invoke-direct {p0, v13}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->getPhotoCount(Landroid/content/ContentResolver;)I

    move-result v12

    .line 198
    .local v12, "photoCount":I
    invoke-direct {p0, v12}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->isContentSound(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    const/16 v0, 0x80

    invoke-direct {p0, v12, v0}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->getExponentialIndice(II)[I

    move-result-object v7

    .line 201
    .local v7, "choosedIds":[I
    invoke-static {v7}, Ljava/util/Arrays;->sort([I)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 204
    const/4 v8, 0x0

    .line 206
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->PROJECTION:[Ljava/lang/String;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->SELECTION:Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->ORDER:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 208
    if-nez v8, :cond_2

    .line 217
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 211
    :cond_2
    move-object v6, v7

    .local v6, "arr$":[I
    :try_start_1
    array-length v11, v6

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v11, :cond_4

    aget v10, v6, v9

    .line 212
    .local v10, "index":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 213
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 217
    .end local v10    # "index":I
    :cond_4
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v6    # "arr$":[I
    .end local v9    # "i$":I
    .end local v11    # "len$":I
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public setContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;

    .line 260
    return-void
.end method

.method public shuffleAllSize()I
    .locals 8

    .prologue
    .line 233
    const/4 v7, 0x0

    .line 234
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 236
    .local v6, "count":I
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->PROJECTION:[Ljava/lang/String;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->SELECTION:Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->ORDER:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 238
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v6, 0x0

    .line 240
    :goto_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 243
    return v6

    .line 238
    :cond_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    goto :goto_0

    .line 240
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->reload()V

    .line 224
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->mPhotos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
