.class Lcom/sec/android/gallery3d/gadget/EasyWidgetService$EmptySource;
.super Ljava/lang/Object;
.source "EasyWidgetService.java"

# interfaces
.implements Lcom/sec/android/gallery3d/gadget/WidgetSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/EasyWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EmptySource"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public getContentUri(I)Landroid/net/Uri;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getImage(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isDrm(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public reload()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public setContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 75
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method
