.class public Lcom/sec/android/gallery3d/data/EventAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "EventAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

.field private static final TAG:Ljava/lang/String;

.field public static final TOP_PATH:Ljava/lang/String; = "/local/event/all"

.field private static final mWatchUris:[Landroid/net/Uri;


# instance fields
.field private final data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

.field private mAlbums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private final mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mLoadTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->TAG:Ljava/lang/String;

    .line 39
    const-string v0, "/local/event/image"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    .line 40
    const-string v0, "/local/event/video"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    .line 41
    const-string v0, "/local/event/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mWatchUris:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    .line 58
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 60
    invoke-static {p2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 61
    invoke-static {p2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .line 62
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mWatchUris:[Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 63
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/EventAlbumSet;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumSet;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/EventAlbumSet;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumSet;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->loadSubMediaSets(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private getEventAlbum(Lcom/sec/android/gallery3d/data/Path;II)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 10
    .param p1, "parent"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "type"    # I
    .param p3, "bucketId"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 145
    invoke-virtual {p1, p3}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 146
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 147
    .local v0, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v5

    if-ne v5, p3, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 160
    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return-object v0

    .line 151
    .restart local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 162
    :pswitch_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 153
    :pswitch_1
    new-instance v0, Lcom/sec/android/gallery3d/data/EventAlbum;

    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v5, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v5, v6, p3, v8}, Lcom/sec/android/gallery3d/data/EventAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 155
    .restart local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_2
    new-instance v0, Lcom/sec/android/gallery3d/data/EventAlbum;

    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v5, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v5, v6, p3, v7}, Lcom/sec/android/gallery3d/data/EventAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 157
    .restart local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_3
    sget-object v5, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-direct {p0, v5, v9, p3}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->getEventAlbum(Lcom/sec/android/gallery3d/data/Path;II)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 158
    .local v2, "palb":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v5, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    const/4 v6, 0x4

    invoke-direct {p0, v5, v6, p3}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->getEventAlbum(Lcom/sec/android/gallery3d/data/Path;II)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 159
    .local v4, "valb":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 160
    .local v1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    new-array v6, v9, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v2, v6, v7

    aput-object v4, v6, v8

    invoke-direct {v0, v3, v1, v5, v6}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private loadSubMediaSets(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "albumMap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v1, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 134
    sget-object v4, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

    const/4 v5, 0x6

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;->bucketId:I

    invoke-direct {p0, v4, v5, v3}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->getEventAlbum(Lcom/sec/android/gallery3d/data/Path;II)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 135
    .local v0, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 136
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 138
    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 139
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 138
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 141
    :cond_2
    return-object v1
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const-string v0, "event"

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 74
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSubMediaSetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    return-object v0
.end method

.method public declared-synchronized onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p1, :cond_0

    .line 121
    :goto_0
    monitor-exit p0

    return-void

    .line 111
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 112
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    .line 113
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mAlbums:Ljava/util/List;

    .line 114
    :cond_1
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/sec/android/gallery3d/data/EventAlbumSet$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/data/EventAlbumSet$1;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumSet;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public reload()J
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->removeExpiredEvent()V

    .line 97
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string/jumbo v1, "suggest_event_info"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->updateDataBase(Ljava/lang/String;)Z

    .line 98
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    const/4 v1, 0x1

    const-string v2, "images_event_album"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->data:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    const/4 v1, 0x0

    const-string/jumbo v2, "video_event_album"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    .line 100
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    new-instance v1, Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumSet;Lcom/sec/android/gallery3d/data/EventAlbumSet$1;)V

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    .line 103
    invoke-static {}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mDataVersion:J

    .line 105
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet;->mDataVersion:J

    return-wide v0
.end method
