.class public Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;
.super Ljava/io/FilterOutputStream;
.source "JPEGOutputStream.java"


# instance fields
.field private JNIPointer:J

.field private mConfigChanged:Z

.field private mFormat:I

.field private mHeight:I

.field private mQuality:I

.field private mTmpBuffer:[B

.field private mValidConfig:Z

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const-string v0, "jni_jpegstream"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->JNIPointer:J

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mTmpBuffer:[B

    .line 26
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mWidth:I

    .line 27
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mHeight:I

    .line 28
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mQuality:I

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mFormat:I

    .line 30
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mValidConfig:Z

    .line 31
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mConfigChanged:Z

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;IIII)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "quality"    # I
    .param p5, "format"    # I

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->JNIPointer:J

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mTmpBuffer:[B

    .line 26
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mWidth:I

    .line 27
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mHeight:I

    .line 28
    iput v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mQuality:I

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mFormat:I

    .line 30
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mValidConfig:Z

    .line 31
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mConfigChanged:Z

    .line 40
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->setConfig(IIII)Z

    .line 41
    return-void
.end method

.method private native cleanup()V
.end method

.method private native setup(Ljava/io/OutputStream;IIII)I
.end method

.method private native writeInputBytes([BII)I
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->cleanup()V

    .line 76
    invoke-super {p0}, Ljava/io/FilterOutputStream;->close()V

    .line 77
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 129
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->cleanup()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 133
    return-void

    .line 131
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public setConfig(IIII)Z
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "quality"    # I
    .param p4, "format"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 45
    const/16 v1, 0x64

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result p3

    .line 48
    sparse-switch p4, :sswitch_data_0

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 59
    :sswitch_0
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 60
    iput p1, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mWidth:I

    .line 61
    iput p2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mHeight:I

    .line 62
    iput p4, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mFormat:I

    .line 63
    iput p3, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mQuality:I

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mValidConfig:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mConfigChanged:Z

    .line 70
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mValidConfig:Z

    goto :goto_0

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x104 -> :sswitch_0
    .end sparse-switch
.end method

.method public write(I)V
    .locals 3
    .param p1, "oneByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mTmpBuffer:[B

    const/4 v1, 0x0

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mTmpBuffer:[B

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->write([B)V

    .line 124
    return-void
.end method

.method public write([B)V
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->write([BII)V

    .line 118
    return-void
.end method

.method public write([BII)V
    .locals 9
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 81
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_1

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, " buffer length %d, offset %d, length %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mValidConfig:Z

    if-nez v0, :cond_3

    .line 113
    :cond_2
    return-void

    .line 89
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mConfigChanged:Z

    if-eqz v0, :cond_4

    .line 90
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->cleanup()V

    .line 91
    iget-object v1, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->out:Ljava/io/OutputStream;

    iget v2, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mWidth:I

    iget v3, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mHeight:I

    iget v4, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mFormat:I

    iget v5, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mQuality:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->setup(Ljava/io/OutputStream;IIII)I

    move-result v6

    .line 92
    .local v6, "flag":I
    packed-switch v6, :pswitch_data_0

    .line 98
    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error to writing jpeg headers."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad arguments to write"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_2
    iput-boolean v8, p0, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->mConfigChanged:Z

    .line 102
    .end local v6    # "flag":I
    :cond_4
    const/4 v7, -0x1

    .line 104
    .local v7, "returnCode":I
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->writeInputBytes([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 106
    if-gez v7, :cond_5

    .line 107
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->cleanup()V

    .line 110
    :cond_5
    if-gez v7, :cond_2

    .line 111
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error writing jpeg stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :catchall_0
    move-exception v0

    if-gez v7, :cond_6

    .line 107
    invoke-direct {p0}, Lcom/sec/android/gallery3d/jpegstream/JPEGOutputStream;->cleanup()V

    :cond_6
    throw v0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
