.class public Lcom/sec/android/gallery3d/homesync/HomeSyncSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "HomeSyncSource.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.spc.playerdb"

.field public static final BASE_URI:Landroid/net/Uri;

.field private static final HOMESYNC_ITEM:I = 0x2

.field private static final HOMESYNC_MEDIASET:I = 0x1

.field private static final NO_MATCH:I = -0x1

.field public static final PREFIX:Ljava/lang/String; = "homesync"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->TAG:Ljava/lang/String;

    .line 28
    const-string v0, "content://com.sec.android.spc/player/gallery"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 5
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 32
    const-string v0, "homesync"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 26
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mUriMatcher:Landroid/content/UriMatcher;

    .line 33
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->TAG:Ljava/lang/String;

    const-string v1, "init AspSource"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 35
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/homesync/mediaset/*"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/homesync/item/*/*"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.spc.playerdb"

    const-string v2, "player/gallery"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.spc.playerdb"

    const-string v2, "player/gallery/#"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 40
    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "create media object : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 47
    :pswitch_1
    new-instance v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 58
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 67
    :cond_0
    :goto_0
    return-object v2

    .line 60
    :pswitch_0
    const-string v2, "/homesync/mediaset"

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_0

    .line 62
    :pswitch_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 63
    .local v0, "id":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-ltz v3, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->TAG:Ljava/lang/String;

    const-string v1, "pause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSource;->pause()V

    .line 74
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 78
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->TAG:Ljava/lang/String;

    const-string v1, "resume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSource;->resume()V

    .line 80
    return-void
.end method
