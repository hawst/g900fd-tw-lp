.class public Lcom/sec/android/gallery3d/ds/SimIconListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SimIconListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;",
        ">;"
    }
.end annotation


# instance fields
.field protected mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;>;"
    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;->getLayout()I

    move-result v0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 38
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v0, :cond_1

    .line 39
    :cond_0
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 41
    :cond_1
    return-void
.end method

.method private static getLayout()I
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v0, :cond_1

    .line 45
    :cond_0
    const v0, 0x7f0300d4

    .line 47
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 53
    const/4 v3, 0x0

    .line 54
    .local v3, "view":Landroid/view/View;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDualSIM:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiSIM:Z

    if-eqz v4, :cond_2

    .line 55
    :cond_0
    const/4 v1, 0x0

    .line 56
    .local v1, "isNoIcon":Z
    const/4 v2, 0x0

    .line 57
    .local v2, "text":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 59
    .local v0, "image":Landroid/widget/ImageView;
    if-nez p2, :cond_3

    .line 60
    iget-object v4, p0, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;->getLayout()I

    move-result v5

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 65
    :goto_0
    if-eqz v3, :cond_2

    .line 67
    const v4, 0x7f0f0213

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "image":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .line 68
    .restart local v0    # "image":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;->getResource()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    .line 70
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 71
    const/4 v1, 0x1

    .line 78
    :cond_1
    :goto_1
    const v4, 0x7f0f00ad

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "text":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 79
    .restart local v2    # "text":Landroid/widget/TextView;
    if-eqz v2, :cond_2

    .line 80
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    if-eqz v1, :cond_2

    .line 82
    const/16 v4, 0x9

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v4

    invoke-virtual {v2, v4, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 88
    .end local v0    # "image":Landroid/widget/ImageView;
    .end local v1    # "isNoIcon":Z
    .end local v2    # "text":Landroid/widget/TextView;
    :cond_2
    return-object v3

    .line 62
    .restart local v0    # "image":Landroid/widget/ImageView;
    .restart local v1    # "isNoIcon":Z
    .restart local v2    # "text":Landroid/widget/TextView;
    :cond_3
    move-object v3, p2

    goto :goto_0

    .line 73
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ds/SimIconListAdapter$IconListItem;->getResource()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
