.class public Lcom/sec/android/gallery3d/util/CacheManager;
.super Ljava/lang/Object;
.source "CacheManager.java"


# static fields
.field private static final KEY_CACHE_UP_TO_DATE:Ljava/lang/String; = "cache-up-to-date"

.field private static final KEY_SDK_VERSION:Ljava/lang/String; = "android-sdk-version"

.field private static final TAG:Ljava/lang/String; = "CacheManager"

.field private static sCacheMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/common/BlobCache;",
            ">;"
        }
    .end annotation
.end field

.field private static sOldCheckDone:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/util/CacheManager;->sCacheMap:Ljava/util/HashMap;

    .line 36
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/CacheManager;->sOldCheckDone:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCache(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "maxEntries"    # I
    .param p3, "maxBytes"    # I
    .param p4, "version"    # I

    .prologue
    .line 46
    sget-object v9, Lcom/sec/android/gallery3d/util/CacheManager;->sCacheMap:Ljava/util/HashMap;

    monitor-enter v9

    .line 47
    :try_start_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/CacheManager;->sOldCheckDone:Z

    if-nez v2, :cond_0

    .line 48
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/CacheManager;->removeOldFilesIfNecessary(Landroid/content/Context;)V

    .line 49
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/util/CacheManager;->sOldCheckDone:Z

    .line 51
    :cond_0
    sget-object v2, Lcom/sec/android/gallery3d/util/CacheManager;->sCacheMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/common/BlobCache;

    .line 52
    .local v6, "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    if-eqz v6, :cond_1

    .line 53
    monitor-exit v9

    move-object v0, v6

    .line 68
    .end local v6    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .local v0, "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    :goto_0
    return-object v6

    .line 56
    .end local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .restart local v6    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v7

    .line 57
    .local v7, "cacheDir":Ljava/io/File;
    if-nez v7, :cond_2

    .line 58
    const/4 v0, 0x0

    monitor-exit v9

    move-object v10, v6

    .end local v6    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .local v10, "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    move-object v6, v0

    move-object v0, v10

    .end local v10    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .restart local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    goto :goto_0

    .line 61
    .end local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .restart local v6    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 63
    .local v1, "path":Ljava/lang/String;
    :try_start_1
    new-instance v0, Lcom/sec/android/gallery3d/common/BlobCache;

    const/4 v4, 0x0

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/common/BlobCache;-><init>(Ljava/lang/String;IIZI)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    .end local v6    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .restart local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    :try_start_2
    sget-object v2, Lcom/sec/android/gallery3d/util/CacheManager;->sCacheMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 68
    :goto_1
    :try_start_3
    monitor-exit v9

    move-object v6, v0

    goto :goto_0

    .line 65
    .end local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .restart local v6    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    :catch_0
    move-exception v8

    move-object v0, v6

    .line 66
    .end local v6    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .restart local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .local v8, "e":Ljava/io/IOException;
    :goto_2
    const-string v2, "CacheManager"

    const-string v3, "Cannot instantiate cache!"

    invoke-static {v2, v3, v8}, Lcom/sec/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 69
    .end local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .end local v1    # "path":Ljava/lang/String;
    .end local v7    # "cacheDir":Ljava/io/File;
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 65
    .restart local v0    # "cache":Lcom/sec/android/gallery3d/common/BlobCache;
    .restart local v1    # "path":Ljava/lang/String;
    .restart local v7    # "cacheDir":Ljava/io/File;
    :catch_1
    move-exception v8

    goto :goto_2
.end method

.method public static removeInCacheMap(Ljava/lang/String;)V
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 132
    sget-object v1, Lcom/sec/android/gallery3d/util/CacheManager;->sCacheMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 133
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/util/CacheManager;->sCacheMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    monitor-exit v1

    .line 135
    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static removeOldFilesIfNecessary(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/util/CacheManager;->removeOldFilesIfNecessary(Landroid/content/Context;Z)V

    .line 92
    return-void
.end method

.method public static removeOldFilesIfNecessary(Landroid/content/Context;Z)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bForce"    # Z

    .prologue
    .line 97
    if-nez p1, :cond_2

    .line 98
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 99
    .local v3, "pref":Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    .line 102
    .local v2, "n":I
    const/4 v5, 0x0

    .line 104
    .local v5, "version":I
    :try_start_0
    const-string v6, "cache-up-to-date"

    const/4 v7, 0x0

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 105
    const-string v6, "android-sdk-version"

    const/4 v7, 0x0

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 109
    :goto_0
    if-eqz v2, :cond_1

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v5, v6, :cond_1

    .line 129
    .end local v2    # "n":I
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    .end local v5    # "version":I
    :cond_0
    :goto_1
    return-void

    .line 110
    .restart local v2    # "n":I
    .restart local v3    # "pref":Landroid/content/SharedPreferences;
    .restart local v5    # "version":I
    :cond_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "cache-up-to-date"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 111
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "android-sdk-version"

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 116
    .end local v2    # "n":I
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    .end local v5    # "version":I
    :cond_2
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExternalCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 118
    .local v0, "cacheDir":Ljava/io/File;
    if-nez v0, :cond_3

    .line 119
    const-string v6, "CacheManager"

    const-string v7, "cacheDir is null"

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 123
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 125
    .local v4, "prefix":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/gallery3d/util/CacheManager;->sCacheMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 126
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 127
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/BlobCache;->deleteFiles(Ljava/lang/String;)V

    goto :goto_2

    .line 106
    .end local v0    # "cacheDir":Ljava/io/File;
    .end local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v4    # "prefix":Ljava/lang/String;
    .restart local v2    # "n":I
    .restart local v3    # "pref":Landroid/content/SharedPreferences;
    .restart local v5    # "version":I
    :catch_0
    move-exception v6

    goto :goto_0
.end method
