.class public abstract Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;
.super Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;
.source "RemoteAlbumEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry$AlbumColumns;,
        Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry$Table;
    }
.end annotation


# static fields
.field private static mFields:[Ljava/lang/reflect/Field;


# instance fields
.field public cache_flag:I

.field public cache_status:I

.field public date_edited:J

.field public num_photos:I

.field public photos_dirty:Z

.field public source_id:Ljava/lang/String;

.field public thumbnail_url:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->mFields:[Ljava/lang/reflect/Field;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;-><init>()V

    .line 43
    return-void
.end method

.method public static getProjection()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->mFields:[Ljava/lang/reflect/Field;

    .line 109
    .local v0, "fields":[Ljava/lang/reflect/Field;
    array-length v3, v0

    new-array v2, v3, [Ljava/lang/String;

    .line 110
    .local v2, "projection":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-eq v1, v3, :cond_0

    .line 111
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    :cond_0
    return-object v2
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 56
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->clear()V

    .line 57
    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->source_id:Ljava/lang/String;

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->cache_flag:I

    .line 59
    iput v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->cache_status:I

    .line 60
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->photos_dirty:Z

    .line 61
    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->title:Ljava/lang/String;

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->date_edited:J

    .line 63
    iput v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->num_photos:I

    .line 64
    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->thumbnail_url:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public doNotMerge(Ljava/lang/String;)Z
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 94
    const-string v1, "cache_status"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    const-string v1, "cache_flag"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 99
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 69
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;

    if-nez v2, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 71
    check-cast v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;

    .line 72
    .local v0, "p":Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->source_id:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->source_id:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->cache_flag:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->cache_flag:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->cache_status:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->cache_status:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->photos_dirty:Z

    iget-boolean v3, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->photos_dirty:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->title:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->date_edited:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->date_edited:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->num_photos:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->num_photos:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->thumbnail_url:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->thumbnail_url:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getExtraSql(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 84
    const-string v0, "cache_status"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "DEFAULT 0"

    .line 89
    :goto_0
    return-object v0

    .line 86
    :cond_0
    const-string v0, "cache_flag"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    const-string v0, "DEFAULT 1"

    goto :goto_0

    .line 89
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFields()[Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;->mFields:[Ljava/lang/reflect/Field;

    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    const-string v0, "albums"

    return-object v0
.end method
