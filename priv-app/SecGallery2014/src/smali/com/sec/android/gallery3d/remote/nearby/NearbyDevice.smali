.class public Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "NearbyDevice.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCoverItem:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

.field private final mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDevice:Lcom/samsung/android/allshare/Device;

.field private mFlatBrowseCount:I

.field private mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

.field private mFlatProviderConnection:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

.field private mIsFlatBrowseFinished:Z

.field private mIsFlatBrowseStarted:Z

.field private final mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mJpegChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/samsung/android/allshare/Device;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z

    .line 37
    iput v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatBrowseCount:I

    .line 88
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatProviderConnection:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .line 42
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    .line 43
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 44
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nearby://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/nearby/item/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 46
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatBrowseCount:I

    return p1
.end method

.method static synthetic access$104(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatBrowseCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatBrowseCount:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDataVersion:J

    return-wide p1
.end method

.method private loadItems()V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 50
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore flat browse : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  isStarted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isFinished"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :goto_0
    return-void

    .line 55
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadItems for : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    instance-of v0, v0, Lcom/samsung/android/allshare/media/Provider;

    if-eqz v0, :cond_2

    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->notifyContentChanged()V

    .line 60
    new-instance v1, Lcom/samsung/android/allshare/extension/FlatProvider;

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    check-cast v0, Lcom/samsung/android/allshare/media/Provider;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/extension/FlatProvider;-><init>(Lcom/samsung/android/allshare/media/Provider;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatProviderConnection:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/extension/FlatProvider;->startFlatBrowse(Lcom/samsung/android/allshare/Item$MediaType;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    goto :goto_0

    .line 63
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    const-string v1, "mDevice instanceof Provider = [ false ]"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public clearItems()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->notifyContentChanged()V

    .line 70
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    .line 71
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z

    .line 72
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 6

    .prologue
    .line 81
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mCoverItem:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->nextVersionNumber()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/samsung/android/allshare/Device;J)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mCoverItem:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mCoverItem:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceCoverItem;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    if-nez v0, :cond_0

    .line 232
    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceNic()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    if-nez v0, :cond_0

    .line 240
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 14
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v9, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v11, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    monitor-enter v11

    .line 154
    move v0, p1

    .line 155
    .local v0, "begin":I
    add-int v10, p1, p2

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->getMediaItemCount()I

    move-result v12

    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 158
    .local v3, "end":I
    move v4, v0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_0

    .line 159
    :try_start_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/Item;

    .line 160
    .local v6, "imageItem":Lcom/samsung/android/allshare/Item;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v7

    .line 162
    .local v7, "imageUri":Landroid/net/Uri;
    if-nez v7, :cond_1

    .line 163
    sget-object v10, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    const-string v12, "image uri is null! : "

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    new-instance v10, Ljava/lang/NullPointerException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "image uri null! : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", location : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getLocation()Landroid/location/Location;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180
    .end local v6    # "imageItem":Lcom/samsung/android/allshare/Item;
    .end local v7    # "imageUri":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 181
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 183
    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_0
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 184
    return-object v9

    .line 167
    .restart local v6    # "imageItem":Lcom/samsung/android/allshare/Item;
    .restart local v7    # "imageUri":Landroid/net/Uri;
    :cond_1
    :try_start_3
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 169
    .local v8, "imageUriString":Ljava/lang/String;
    const/16 v10, 0x2f

    const/16 v12, 0x5f

    invoke-virtual {v8, v10, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v8

    .line 170
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v10, v8}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 171
    .local v1, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .line 172
    .local v5, "image":Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    if-nez v5, :cond_2

    .line 173
    new-instance v5, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .end local v5    # "image":Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-direct {v5, v1, v10, v12, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/Item;)V

    .line 178
    .restart local v5    # "image":Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    :goto_1
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->updateContent(Lcom/samsung/android/allshare/Item;)V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 183
    .end local v1    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v3    # "end":I
    .end local v4    # "i":I
    .end local v5    # "image":Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    .end local v6    # "imageItem":Lcom/samsung/android/allshare/Item;
    .end local v7    # "imageUri":Landroid/net/Uri;
    .end local v8    # "imageUriString":Ljava/lang/String;
    :catchall_0
    move-exception v10

    :try_start_4
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v10
.end method

.method public getMediaItemCount()I
    .locals 3

    .prologue
    .line 189
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMediaItemCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    if-nez v0, :cond_0

    .line 196
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    const-string v1, "getName : mDevice is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const-string v0, ""

    .line 199
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 260
    const-wide v0, 0x800000000000L

    .line 261
    .local v0, "supported":J
    const-wide v2, -0x400000000001L

    and-long/2addr v0, v2

    .line 262
    return-wide v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public initFlatBrowseFlag()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 145
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    .line 146
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z

    .line 147
    return-void
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDirty()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->notifyDirty()V

    .line 256
    :cond_0
    return-void
.end method

.method public refreshItem()V
    .locals 3

    .prologue
    .line 218
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refreshItem, mIsFlatBrowseStarted = [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z

    if-nez v0, :cond_0

    .line 220
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDataVersion:J

    .line 221
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->loadItems()V

    .line 223
    :cond_0
    return-void
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDataVersion:J

    .line 206
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->loadItems()V

    .line 209
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDataVersion:J

    return-wide v0
.end method

.method public update(Lcom/samsung/android/allshare/Device;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 226
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update device : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDevice:Lcom/samsung/android/allshare/Device;

    .line 228
    return-void
.end method
