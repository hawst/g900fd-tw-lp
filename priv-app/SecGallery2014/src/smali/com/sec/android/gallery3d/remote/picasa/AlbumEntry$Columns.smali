.class public interface abstract Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry$Columns;
.super Ljava/lang/Object;
.source "AlbumEntry.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$Columns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Columns"
.end annotation


# static fields
.field public static final ALBUM_VALID:Ljava/lang/String; = "valid"

.field public static final BYTES_USED:Ljava/lang/String; = "bytes_used"

.field public static final CACHE_STATUS:Ljava/lang/String; = "cache_status"

.field public static final LOCATION_STRING:Ljava/lang/String; = "location_string"

.field public static final NUM_PHOTOS:Ljava/lang/String; = "num_photos"

.field public static final PHOTOS_DIRTY:Ljava/lang/String; = "photos_dirty"

.field public static final PHOTOS_ETAG:Ljava/lang/String; = "photos_etag"

.field public static final USER:Ljava/lang/String; = "user"
