.class Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/CropView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FaceHighlightView"
.end annotation


# static fields
.field private static final INDEX_NONE:I = -0x1


# instance fields
.field private mFaces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private mPressedFaceIndex:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/CropView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/CropView;)V
    .locals 1

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 234
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mFaces:Ljava/util/ArrayList;

    .line 235
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mPressedFaceIndex:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/CropView;Lcom/sec/android/gallery3d/ui/CropView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/CropView$1;

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;-><init>(Lcom/sec/android/gallery3d/ui/CropView;)V

    return-void
.end method

.method private getFaceIndexByPosition(FF)I
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mFaces:Ljava/util/ArrayList;

    .line 265
    .local v0, "faces":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/RectF;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 266
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v3

    .line 267
    .local v3, "r":Landroid/graphics/RectF;
    invoke-virtual {v3, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 270
    .end local v1    # "i":I
    .end local v3    # "r":Landroid/graphics/RectF;
    :goto_1
    return v1

    .line 265
    .restart local v1    # "i":I
    .restart local v3    # "r":Landroid/graphics/RectF;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    .end local v3    # "r":Landroid/graphics/RectF;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private renderFace(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Z)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "face"    # Landroid/graphics/RectF;
    .param p3, "pressed"    # Z

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v6

    .line 244
    .local v6, "r":Landroid/graphics/RectF;
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 245
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v4

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFacePaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/CropView;->access$400(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 246
    return-void
.end method

.method private setPressedFace(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 257
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mPressedFaceIndex:I

    if-ne v0, p1, :cond_0

    .line 261
    :goto_0
    return-void

    .line 259
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mPressedFaceIndex:I

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public addFace(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "faceRect"    # Landroid/graphics/RectF;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mFaces:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->invalidate()V

    .line 240
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, -0x1

    const/4 v5, 0x1

    .line 275
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->isScaleMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->invalidate()V

    .line 301
    :cond_0
    :goto_0
    return v5

    .line 279
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 280
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 281
    .local v2, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 284
    :pswitch_0
    invoke-direct {p0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->getFaceIndexByPosition(FF)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->setPressedFace(I)V

    goto :goto_0

    .line 289
    :pswitch_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mPressedFaceIndex:I

    .line 290
    .local v0, "index":I
    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->setPressedFace(I)V

    .line 291
    if-eq v0, v4, :cond_0

    .line 292
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCropMenuHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$500(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 293
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCropMenuHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$500(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x6b

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 295
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v4

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mFaces:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setFaceRectangle(Landroid/graphics/RectF;)V

    .line 296
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setVisibility(I)V

    .line 297
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->setVisibility(I)V

    goto :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 5
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mFaces:Ljava/util/ArrayList;

    .line 251
    .local v0, "faces":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/RectF;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 252
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->mPressedFaceIndex:I

    if-ne v1, v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    invoke-direct {p0, p1, v3, v4}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->renderFace(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Z)V

    .line 251
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 252
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 254
    :cond_1
    return-void
.end method
