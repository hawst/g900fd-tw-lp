.class public Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.super Ljava/lang/Object;
.source "GlAnimationBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;
    }
.end annotation


# static fields
.field public static final GL_ANIM_IDLE:I = 0x0

.field public static final GL_ANIM_READY:I = 0x1

.field public static final GL_ANIM_RUNNING:I = 0x2


# instance fields
.field public chn_time:J

.field public mAnimDuration:J

.field private mAnimRatioReset:Z

.field public mAnimStartTime:J

.field public mAnimState:I

.field private mAnimTimeOffset:J

.field protected mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

.field public mLastRatio:F

.field public mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field protected mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field public mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field public mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field public mStartRatio:F

.field public mView:Lcom/sec/android/gallery3d/glcore/GlView;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 19
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 20
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 21
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 24
    const-wide/16 v0, -0x1b

    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->chn_time:J

    .line 27
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 28
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 29
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mLastRatio:F

    .line 30
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mStartRatio:F

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimRatioReset:Z

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    .line 33
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 100
    return-void
.end method

.method public getLastRatio()F
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mLastRatio:F

    return v0
.end method

.method public isIdle()Z
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x1

    .line 38
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 42
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    if-ne v1, v0, :cond_0

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRunning()Z
    .locals 2

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 49
    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public process(J)V
    .locals 11
    .param p1, "curTime"    # J

    .prologue
    const/4 v8, 0x1

    .line 132
    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimStartTime:J

    cmp-long v3, p1, v6

    if-gez v3, :cond_1

    .line 133
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    if-ne v3, v8, :cond_4

    .line 136
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    if-eqz v3, :cond_2

    .line 137
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;->reset()V

    .line 139
    :cond_2
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mStartRatio:F

    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimDuration:J

    long-to-float v6, v6

    mul-float/2addr v3, v6

    float-to-long v6, v3

    iput-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimTimeOffset:J

    .line 140
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimRatioReset:Z

    if-eqz v3, :cond_3

    .line 141
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mStartRatio:F

    .line 142
    :cond_3
    const/4 v3, 0x2

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->onStart()V

    .line 144
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    if-eqz v3, :cond_4

    .line 145
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-interface {v3, p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;->onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 148
    :cond_4
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v3, :cond_5

    .line 149
    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->chn_time:J

    const-wide/16 v8, 0x1e

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->chn_time:J

    .line 150
    iget-wide v4, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->chn_time:J

    .line 156
    .local v4, "timePassed":J
    :goto_1
    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimDuration:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_6

    .line 157
    long-to-float v3, v4

    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimDuration:J

    long-to-float v6, v6

    div-float v2, v3, v6

    .line 158
    .local v2, "ratio":F
    const/4 v0, 0x0

    .line 165
    .local v0, "animDone":Z
    :goto_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    if-eqz v3, :cond_7

    .line 166
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;->getInterpolation(F)F

    move-result v1

    .line 170
    .local v1, "intRatio":F
    :goto_3
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->applyTransform(F)V

    .line 171
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mLastRatio:F

    .line 172
    if-eqz v0, :cond_8

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    goto :goto_0

    .line 153
    .end local v0    # "animDone":Z
    .end local v1    # "intRatio":F
    .end local v2    # "ratio":F
    .end local v4    # "timePassed":J
    :cond_5
    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimTimeOffset:J

    add-long/2addr v6, p1

    iget-wide v8, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimStartTime:J

    sub-long v4, v6, v8

    .restart local v4    # "timePassed":J
    goto :goto_1

    .line 160
    :cond_6
    const/high16 v2, 0x3f800000    # 1.0f

    .line 161
    .restart local v2    # "ratio":F
    const/4 v0, 0x1

    .line 163
    .restart local v0    # "animDone":Z
    const-wide/16 v6, -0x1b

    iput-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->chn_time:J

    goto :goto_2

    .line 168
    :cond_7
    move v1, v2

    .restart local v1    # "intRatio":F
    goto :goto_3

    .line 174
    :cond_8
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    if-eqz v3, :cond_0

    .line 175
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-interface {v3, p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;->onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 83
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimDuration:J

    .line 75
    return-void
.end method

.method public setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V
    .locals 0
    .param p1, "interpolator"    # Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 79
    return-void
.end method

.method public setStartRatio(FZ)V
    .locals 0
    .param p1, "ratio"    # F
    .param p2, "reset"    # Z

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mStartRatio:F

    .line 92
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimRatioReset:Z

    .line 93
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 55
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimStartTime:J

    .line 56
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mLastRatio:F

    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 61
    :cond_0
    return-void
.end method

.method public startAfter(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 65
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimStartTime:J

    .line 66
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mLastRatio:F

    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 71
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 113
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 116
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->onCancel()V

    goto :goto_0

    .line 120
    :cond_2
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->onStop()V

    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-interface {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;->onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method
