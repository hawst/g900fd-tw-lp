.class Lcom/sec/android/gallery3d/gadget/WidgetConfigure$5;
.super Ljava/lang/Object;
.source "WidgetConfigure.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->showWidgetTypeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/WidgetConfigure;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$5;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetConfigure;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 321
    const-string v0, "WidgetConfigure"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selected widget type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 323
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetConfigure$5;->this$0:Lcom/sec/android/gallery3d/gadget/WidgetConfigure;

    # invokes: Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->setWidgetType(I)V
    invoke-static {v0, p2}, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;->access$100(Lcom/sec/android/gallery3d/gadget/WidgetConfigure;I)V

    .line 324
    return-void
.end method
