.class public Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;
.super Ljava/lang/Object;
.source "HomeSyncIntent.java"


# static fields
.field public static final ACTION_NATIVE_IMAGEVIEWER:Ljava/lang/String; = "com.android.gallery.NATIVE_VIEWER"

.field private static final ACTON_HOMESYNC_PLAYER_STATE:Ljava/lang/String; = "com.android.spc.image.STATE_CHANGED"

.field public static final BASE_URI:Ljava/lang/String; = "content://com.sec.android.spc/player/gallery"

.field private static final KEY_CURRENT_TITLE:Ljava/lang/String; = "title"

.field private static final KEY_PLAYER_STATE:Ljava/lang/String; = "state"

.field private static final PLAYER_STATE_EXIT:I = 0x3

.field private static final PLAYER_STATE_IMAGE_CHANGED:I = 0x2

.field private static final PLAYER_STATE_SLIDESHOW_PLAY:I = 0x0

.field private static final PLAYER_STATE_SLIDESHOW_STOP:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static mIsStartSlideshowByController:Z

.field private static mLastTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->TAG:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mIsStartSlideshowByController:Z

    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static SendExitState(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 153
    .local v0, "statusIntent":Landroid/content/Intent;
    const-string v1, "com.android.spc.image.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string/jumbo v1, "state"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 157
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 159
    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->TAG:Ljava/lang/String;

    const-string v2, "SendExitState "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public static SendSlideShowPlayState(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 117
    .local v0, "statusIntent":Landroid/content/Intent;
    const-string v1, "com.android.spc.image.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const-string/jumbo v1, "state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 121
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 123
    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->TAG:Ljava/lang/String;

    const-string v2, "SendSlideShowPlayState"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public static SendSlideShowStopState(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 135
    .local v0, "statusIntent":Landroid/content/Intent;
    const-string v1, "com.android.spc.image.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const-string/jumbo v1, "state"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 139
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 141
    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->TAG:Ljava/lang/String;

    const-string v2, "SendTitleInformation"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public static SendTitleInformation(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 169
    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "skip send title"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :goto_0
    return-void

    .line 179
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 181
    .local v0, "statusIntent":Landroid/content/Intent;
    const-string v1, "com.android.spc.image.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    const-string/jumbo v1, "state"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 185
    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 191
    sput-object p1, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    .line 193
    const-string v1, "CHS"

    const-string v2, "SendTitleInformation"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isStartSlideshowByHomeSyncController()Z
    .locals 1

    .prologue
    .line 209
    sget-boolean v0, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mIsStartSlideshowByController:Z

    return v0
.end method

.method public static setStartSlideShow(Z)V
    .locals 0
    .param p0, "start"    # Z

    .prologue
    .line 201
    sput-boolean p0, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mIsStartSlideshowByController:Z

    .line 203
    return-void
.end method

.method private static startDetailViewState(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V
    .locals 2
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 109
    return-void
.end method

.method public static startHomeSyncNativeViewerMode(Landroid/content/Intent;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 9
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v8, 0x0

    .line 73
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 75
    .local v1, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const-string v3, "content://com.sec.android.spc/player/gallery/0"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3, v8}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 77
    .local v2, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    if-nez v2, :cond_0

    .line 79
    sget-object v3, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->TAG:Ljava/lang/String;

    const-string v4, "Can\'t find item path via URI : "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :goto_0
    return-void

    .line 87
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    .local v0, "data":Landroid/os/Bundle;
    const-string v3, "KEY_MEDIA_SET_PATH"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/homesync/mediaset/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->nextVersionNumber()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v3, "KEY_SPC_NATIVE_PLAYER_MODE"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 97
    sput-object v8, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->mLastTitle:Ljava/lang/String;

    .line 101
    invoke-static {p1, v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->startDetailViewState(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/os/Bundle;)V

    goto :goto_0
.end method
