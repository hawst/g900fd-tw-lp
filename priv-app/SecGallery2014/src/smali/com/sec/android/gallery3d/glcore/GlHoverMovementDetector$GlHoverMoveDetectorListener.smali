.class public interface abstract Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;
.super Ljava/lang/Object;
.source "GlHoverMovementDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GlHoverMoveDetectorListener"
.end annotation


# virtual methods
.method public abstract getListScrollMode()I
.end method

.method public abstract onMove(III)Z
.end method

.method public abstract onPress(II)Z
.end method

.method public abstract onRelease(IIII)Z
.end method
