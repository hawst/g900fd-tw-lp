.class Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;
.super Ljava/lang/Object;
.source "PicasaImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectDecodeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$1;

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 138
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 139
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 140
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    invoke-static {p1, v2, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->requestDecode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 141
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    if-eqz v2, :cond_0

    .line 142
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    neg-int v2, v2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 143
    :cond_0
    return-object v0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$DirectDecodeRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
