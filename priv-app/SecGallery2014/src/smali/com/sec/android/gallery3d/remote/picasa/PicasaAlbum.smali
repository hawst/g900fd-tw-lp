.class public Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "PicasaAlbum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;
    }
.end annotation


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field private static final SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

.field private static final TAG:Ljava/lang/String;

.field private static final TYPE_WHERE_CLAUSE:Ljava/lang/String; = "album_id = ? AND content_type LIKE ?"

.field private static final WHERE_CLAUSE:Ljava/lang/String; = "album_id = ?"

.field private static final sIdIndex:I


# instance fields
.field private mCachedCount:I

.field private mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

.field private mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->TAG:Ljava/lang/String;

    .line 40
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    .line 41
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->sIdIndex:I

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/picasa/AlbumData;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    .param p4, "type"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mCachedCount:I

    .line 59
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mResolver:Landroid/content/ContentResolver;

    .line 60
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    .line 61
    iput p4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mType:I

    .line 62
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 64
    return-void
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;
    .locals 2
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "id"    # J
    .param p4, "type"    # I

    .prologue
    .line 68
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p2, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->getAlbumData(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    move-result-object v0

    .line 70
    .local v0, "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    if-nez v0, :cond_0

    .line 71
    const/4 v1, 0x0

    .line 72
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    invoke-direct {v1, p0, p1, v0, p4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/picasa/AlbumData;I)V

    goto :goto_0
.end method

.method public static getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 23
    .param p0, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)[",
            "Lcom/sec/android/gallery3d/data/MediaItem;"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v21, v0

    .line 154
    .local v21, "result":[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 200
    :goto_0
    return-object v21

    .line 156
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 157
    .local v18, "idLow":J
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 159
    .local v16, "idHigh":J
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 160
    .local v2, "resolver":Landroid/content/ContentResolver;
    invoke-interface/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v10

    .line 161
    .local v10, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    .line 162
    .local v3, "uri":Landroid/net/Uri;
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "_id BETWEEN ? AND ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v6, v7

    const/4 v7, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v6, v7

    const-string v7, "_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 168
    .local v9, "cursor":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 169
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->TAG:Ljava/lang/String;

    const-string v5, "query fail"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 174
    :cond_1
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 175
    .local v20, "n":I
    const/4 v12, 0x0

    .line 177
    .local v12, "i":I
    :cond_2
    :goto_1
    move/from16 v0, v20

    if-ge v12, v0, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 178
    sget v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->sIdIndex:I

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 181
    .local v14, "id":J
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v14

    if-gtz v4, :cond_2

    .line 185
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    cmp-long v4, v4, v14

    if-gez v4, :cond_4

    .line 186
    add-int/lit8 v12, v12, 0x1

    move/from16 v0, v20

    if-lt v12, v0, :cond_3

    .line 200
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 191
    :cond_4
    :try_start_1
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    new-instance v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    invoke-virtual {v4, v9, v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 192
    .local v11, "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    sget-object v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v14, v15}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    .line 193
    .local v8, "childPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    invoke-static {v8, v11, v10, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v13

    .line 195
    .local v13, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    aput-object v13, v21, v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    add-int/lit8 v12, v12, 0x1

    .line 197
    goto :goto_1

    .line 200
    .end local v8    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v11    # "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .end local v13    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v14    # "id":J
    :cond_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .end local v12    # "i":I
    .end local v20    # "n":I
    :catchall_0
    move-exception v4

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method public static getPhotoEntry(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .locals 9
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J

    .prologue
    const/4 v5, 0x0

    .line 205
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 211
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    :goto_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    return-object v0

    :cond_0
    move-object v0, v5

    .line 211
    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 140
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .line 141
    .local v0, "item":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    if-nez v0, :cond_0

    .line 142
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;)V

    .line 146
    .restart local v0    # "item":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    :goto_0
    return-object v0

    .line 144
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->updateContent(Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;)V

    goto :goto_0
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    .line 255
    const/4 v0, -0x1

    .line 257
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getCacheFlag()I
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x1

    return v0
.end method

.method public getCacheSize()J
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->cacheSize:J

    return-wide v0
.end method

.method public getCacheStatus()I
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x1

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 112
    .local v1, "uri":Landroid/net/Uri;
    new-instance v12, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    iget-wide v2, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->id:J

    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mType:I

    invoke-direct {v12, v2, v3, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;-><init>(JI)V

    .line 113
    .local v12, "where":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-string v5, "display_index ASC"

    .line 115
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 116
    const-string v5, "display_index DESC"

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, v12, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;->selection:Ljava/lang/String;

    iget-object v4, v12, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;->args:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 122
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 123
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :goto_0
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    new-instance v2, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;-><init>()V

    invoke-virtual {v0, v7, v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 125
    .local v9, "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iget-wide v2, v9, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->id:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 126
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v6, v9, v8, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    .line 128
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 131
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v9    # "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_1

    .line 132
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 131
    .restart local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_2
    if-eqz v7, :cond_3

    .line 132
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_3
    return-object v11
.end method

.method public getMediaItemCount()I
    .locals 8

    .prologue
    .line 228
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 229
    new-instance v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->id:J

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mType:I

    invoke-direct {v7, v0, v1, v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;-><init>(JI)V

    .line 230
    .local v7, "where":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    iget-object v3, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;->selection:Ljava/lang/String;

    iget-object v4, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;->args:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 234
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 235
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 236
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mCachedCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :cond_0
    if-eqz v6, :cond_1

    .line 240
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 244
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "where":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mCachedCount:I

    return v0

    .line 239
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "where":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 240
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 262
    const/4 v7, 0x0

    .line 263
    .local v7, "isVideoIncluded":Z
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 264
    .local v1, "uri":Landroid/net/Uri;
    new-instance v10, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->id:J

    const/4 v0, 0x4

    invoke-direct {v10, v4, v5, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;-><init>(JI)V

    .line 265
    .local v10, "where":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v3, v10, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;->selection:Ljava/lang/String;

    iget-object v4, v10, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum$WhereEntry;->args:[Ljava/lang/String;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 267
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    const/4 v7, 0x1

    .line 270
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 272
    const-wide v8, 0x400000000500L

    .line 273
    .local v8, "supportedOperations":J
    if-nez v7, :cond_1

    .line 274
    const-wide/16 v2, 0x4

    or-long/2addr v8, v2

    .line 275
    :cond_1
    return-wide v8

    .line 270
    .end local v8    # "supportedOperations":J
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x1

    return v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    invoke-static {}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mDataVersion:J

    .line 295
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mCachedCount:I

    .line 297
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mDataVersion:J

    return-wide v0
.end method

.method protected updateContent(Lcom/sec/android/gallery3d/remote/picasa/AlbumData;)V
    .locals 2
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mData:Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    .line 222
    invoke-static {}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->mDataVersion:J

    .line 224
    :cond_0
    return-void
.end method
