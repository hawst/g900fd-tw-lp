.class Lcom/sec/android/gallery3d/homesync/DMRViewController$1;
.super Ljava/lang/Object;
.source "DMRViewController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/homesync/DMRViewController;->startViewControllerServer(Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/homesync/DMRViewController;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 93
    const/4 v3, 0x0

    .line 96
    .local v3, "socket":Ljava/net/Socket;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    new-instance v5, Ljava/net/ServerSocket;

    const/16 v6, 0x1e62

    invoke-direct {v5, v6}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v5, v4, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    :goto_0
    const-string v4, "DMRViewController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "startViewControllerServer() serverSocket = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v6, v6, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v4, v4, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    if-eqz v4, :cond_0

    .line 107
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v5, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v5, v5, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v5}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v5

    # setter for: Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverPort:I
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->access$002(Lcom/sec/android/gallery3d/homesync/DMRViewController;I)I

    .line 110
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    # getter for: Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverPort:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->access$000(Lcom/sec/android/gallery3d/homesync/DMRViewController;)I

    move-result v4

    if-eq v7, v4, :cond_1

    .line 111
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v5, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    # getter for: Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverPort:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->access$000(Lcom/sec/android/gallery3d/homesync/DMRViewController;)I

    move-result v5

    # invokes: Lcom/sec/android/gallery3d/homesync/DMRViewController;->sendServerPort(I)I
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->access$100(Lcom/sec/android/gallery3d/homesync/DMRViewController;I)I

    move-result v4

    if-ne v7, v4, :cond_4

    .line 112
    const-string v4, "DMRViewController"

    const-string/jumbo v5, "startViewControllerServer() SendServerPort failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_1
    :goto_1
    const-string v4, "DMRViewController"

    const-string/jumbo v5, "startViewControllerServer() before while= "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v4, v4, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    if-eqz v4, :cond_6

    .line 122
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v4, v4, Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    .line 123
    new-instance v2, Lcom/sec/android/gallery3d/homesync/DMRReceiver;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;-><init>()V

    .line 124
    .local v2, "receiver":Lcom/sec/android/gallery3d/homesync/DMRReceiver;
    if-eqz v2, :cond_3

    .line 125
    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->SetReceiverSocket(Ljava/net/Socket;)V

    .line 126
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v4, v4, Lcom/sec/android/gallery3d/homesync/DMRViewController;->mViewControllerHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->registerControllerActionHandler(Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;)V

    .line 127
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->start()V

    .line 128
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    # getter for: Lcom/sec/android/gallery3d/homesync/DMRViewController;->ReceiverInstanceList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->access$200(Lcom/sec/android/gallery3d/homesync/DMRViewController;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const-string v4, "DMRViewController"

    const-string/jumbo v5, "startViewControllerServer() receiver.start() "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :cond_3
    if-eqz v3, :cond_2

    .line 137
    :try_start_2
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 138
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 97
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "receiver":Lcom/sec/android/gallery3d/homesync/DMRReceiver;
    :catch_1
    move-exception v1

    .line 99
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 115
    .end local v1    # "e1":Ljava/io/IOException;
    :cond_4
    const-string v4, "DMRViewController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "startViewControllerServer() serverPort = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/homesync/DMRViewController$1;->this$0:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    # getter for: Lcom/sec/android/gallery3d/homesync/DMRViewController;->serverPort:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->access$000(Lcom/sec/android/gallery3d/homesync/DMRViewController;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 132
    :catch_2
    move-exception v0

    .line 133
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    if-eqz v3, :cond_2

    .line 137
    :try_start_4
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    .line 138
    :catch_3
    move-exception v0

    .line 140
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 135
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 136
    if-eqz v3, :cond_5

    .line 137
    :try_start_5
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 141
    :cond_5
    :goto_3
    throw v4

    .line 138
    :catch_4
    move-exception v0

    .line 140
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 144
    .end local v0    # "e":Ljava/io/IOException;
    :cond_6
    return-void
.end method
