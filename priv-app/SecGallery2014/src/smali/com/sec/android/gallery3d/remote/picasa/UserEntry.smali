.class public final Lcom/sec/android/gallery3d/remote/picasa/UserEntry;
.super Lcom/sec/android/gallery3d/remote/picasa/Entry;
.source "UserEntry.java"


# annotations
.annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Table;
    value = "users"
.end annotation


# static fields
.field public static final SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;


# instance fields
.field public account:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "account"
    .end annotation
.end field

.field public albumsEtag:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "albums_etag"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    const-class v1, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/Entry;-><init>()V

    return-void
.end method


# virtual methods
.method public setPropertyFromXml(Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;
    .param p4, "content"    # Ljava/lang/String;

    .prologue
    .line 33
    return-void
.end method
