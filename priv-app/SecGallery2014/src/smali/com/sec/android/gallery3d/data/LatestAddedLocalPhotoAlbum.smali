.class public Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "LatestAddedLocalPhotoAlbum.java"


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mBaseUri:Landroid/net/Uri;

.field private mCachedCount:I

.field private final mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private final mMaxCount:I

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mOrderClause:Ljava/lang/String;

.field private final mProjection:[Ljava/lang/String;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mSupportShare:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->TAG:Ljava/lang/String;

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "maxCount"    # I

    .prologue
    .line 52
    invoke-static {}, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mCachedCount:I

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mSupportShare:Z

    .line 53
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 54
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mResolver:Landroid/content/ContentResolver;

    .line 55
    iput p3, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mMaxCount:I

    .line 56
    const-string v0, "_id DESC"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mOrderClause:Ljava/lang/String;

    .line 57
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mBaseUri:Landroid/net/Uri;

    .line 58
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mProjection:[Ljava/lang/String;

    .line 59
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 61
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 62
    return-void
.end method

.method private static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "isImage"    # Z

    .prologue
    .line 102
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 103
    .local v0, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 108
    .restart local v0    # "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_0
    return-object v0

    .line 106
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateContent(Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 67
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 69
    .local v1, "uri":Landroid/net/Uri;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v12, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 72
    const/4 v7, 0x0

    .line 74
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mOrderClause:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 75
    if-nez v7, :cond_0

    .line 76
    sget-object v0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->TAG:Ljava/lang/String;

    const-string v2, "query fail: "

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 97
    :goto_0
    return-object v12

    .line 80
    :cond_0
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 82
    .local v10, "id":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 83
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v2, 0x1

    invoke-static {v6, v7, v8, v0, v2}, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    .line 85
    .local v11, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v11, :cond_0

    .line 87
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x4

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mSupportShare:Z

    .line 90
    :cond_1
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 92
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "id":I
    .end local v11    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v9

    .line 93
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 95
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getMediaItemCount()I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 113
    iget v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 114
    const/4 v6, 0x0

    .line 116
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mBaseUri:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->COUNT_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mOrderClause:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 117
    if-nez v6, :cond_0

    .line 118
    sget-object v0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->TAG:Ljava/lang/String;

    const-string v1, "query fail"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v7

    .line 130
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 121
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 122
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mCachedCount:I

    .line 123
    iget v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mCachedCount:I

    iget v1, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mMaxCount:I

    if-le v0, v1, :cond_1

    .line 124
    iget v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mMaxCount:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mCachedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 130
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mCachedCount:I

    goto :goto_0

    .line 127
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 149
    const-wide v0, 0x400000000405L

    .line 150
    .local v0, "supported":J
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mSupportShare:Z

    if-nez v2, :cond_0

    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 151
    :cond_0
    return-wide v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-static {}, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mDataVersion:J

    .line 142
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mCachedCount:I

    .line 144
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LatestAddedLocalPhotoAlbum;->mDataVersion:J

    return-wide v0
.end method
