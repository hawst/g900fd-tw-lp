.class Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;
.super Ljava/lang/Object;
.source "LocalAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/LocalAlbumSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumsLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/LocalAlbumSet;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/data/LocalAlbumSet;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;->this$0:Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/data/LocalAlbumSet;Lcom/sec/android/gallery3d/data/LocalAlbumSet$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/data/LocalAlbumSet;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/data/LocalAlbumSet$1;

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;-><init>(Lcom/sec/android/gallery3d/data/LocalAlbumSet;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 184
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;->this$0:Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->loadSubMediaSets()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
