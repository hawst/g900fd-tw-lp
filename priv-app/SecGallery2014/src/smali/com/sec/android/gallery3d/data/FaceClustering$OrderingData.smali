.class Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;
.super Ljava/lang/Object;
.source "FaceClustering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/FaceClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OrderingData"
.end annotation


# instance fields
.field public date:J

.field public personId:I

.field public recommendedId:I


# direct methods
.method public constructor <init>(JII)V
    .locals 1
    .param p1, "date"    # J
    .param p3, "recommendedId"    # I
    .param p4, "personId"    # I

    .prologue
    .line 632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633
    iput-wide p1, p0, Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;->date:J

    .line 634
    iput p3, p0, Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;->recommendedId:I

    .line 635
    iput p4, p0, Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;->personId:I

    .line 636
    return-void
.end method
