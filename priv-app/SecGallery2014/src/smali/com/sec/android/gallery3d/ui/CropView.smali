.class public Lcom/sec/android/gallery3d/ui/CropView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;,
        Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;,
        Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;,
        Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;
    }
.end annotation


# static fields
.field private static final COLOR_DIM:I = 0x59000000

.field private static final COLOR_FACE_OUTLINE:I = -0xff7501

.field private static final COLOR_OUTLINE:I = -0x50506

.field private static final EDGE_ADJUSTMENT:I = 0x1

.field private static final FACE_EYE_RATIO:F = 2.0f

.field private static final FACE_PIXEL_COUNT:I = 0x1d4c0

.field private static final MAX_FACE_COUNT:I = 0x3

.field private static final MIN_RANGE_RATIO:F = 0.0125f

.field private static final MIN_SELECTION_LENGTH:F = 16.0f

.field private static final MOVE_BLOCK:I = 0x10

.field private static final MOVE_BOTTOM:I = 0x8

.field private static final MOVE_LEFT:I = 0x1

.field private static final MOVE_RIGHT:I = 0x4

.field private static final MOVE_TOP:I = 0x2

.field private static final MSG_UPDATE_FACES:I = 0x1

.field private static final OUTLINE_WIDTH:F = 3.0f

.field private static SELECTION_RATIO:F = 0.0f

.field private static final SIZE_UNKNOWN:I = -0x1

.field private static final TAG:Ljava/lang/String; = "CropView"

.field private static final TOUCH_DEADZONE_ADJUST_a_WIDE:F = 1.0245f

.field private static final TOUCH_TOLERANCE:I = 0x32

.field public static final UNSPECIFIED:F = -1.0f


# instance fields
.field borderDisplayRect:Landroid/graphics/RectF;

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAspectRatio:F

.field private mCenterX:F

.field private mCenterY:F

.field private mContext:Landroid/content/Context;

.field private mCropMenuHandler:Landroid/os/Handler;

.field private mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

.field private mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

.field private mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mFacePaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

.field private mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

.field private mImageHeight:I

.field private mImageRotation:I

.field private mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

.field private mImageWidth:I

.field private mIsCalledLayout:Z

.field private volatile mIsCallerId:Z

.field private mIsManualFD:Z

.field private mIsPhotoFrame:Z

.field private mIsSNote:Z

.field private mIsSView:Z

.field private mIsSetAsContactPhoto:Z

.field private mIsSingleFrame:Z

.field private mIsWallpaper:Z

.field private mMainHandler:Landroid/os/Handler;

.field private mMove:Z

.field private mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

.field private mPaintDim:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

.field private final mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private final mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

.field private mSpotlightRatioX:F

.field private mSpotlightRatioY:F

.field private mTouch:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const v0, 0x3f19999a    # 0.6f

    sput v0, Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 7
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const v6, -0x50506

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x40400000    # 3.0f

    const/4 v4, 0x0

    .line 126
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 80
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F

    .line 81
    iput v5, p0, Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioX:F

    .line 82
    iput v5, p0, Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioY:F

    .line 90
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    .line 91
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    .line 95
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .line 96
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFacePaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .line 102
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsManualFD:Z

    .line 105
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsWallpaper:Z

    .line 106
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSNote:Z

    .line 107
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsPhotoFrame:Z

    .line 108
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z

    .line 109
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSView:Z

    .line 111
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mPaintDim:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .line 112
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .line 114
    new-instance v1, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;-><init>(Lcom/sec/android/gallery3d/ui/CropView;Lcom/sec/android/gallery3d/ui/CropView$1;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    .line 116
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 117
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsCalledLayout:Z

    .line 121
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z

    .line 123
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mTouch:Z

    .line 124
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSetAsContactPhoto:Z

    .line 999
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsCallerId:Z

    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 128
    new-instance v1, Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-direct {v1, p1}, Lcom/sec/android/gallery3d/ui/TileImageView;-><init>(Lcom/sec/android/gallery3d/app/GalleryContext;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

    .line 129
    new-instance v1, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;-><init>(Lcom/sec/android/gallery3d/ui/CropView;Lcom/sec/android/gallery3d/ui/CropView$1;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    .line 130
    new-instance v1, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;-><init>(Lcom/sec/android/gallery3d/ui/CropView;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    .line 131
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mContext:Landroid/content/Context;

    .line 133
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setVisibility(I)V

    .line 139
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 142
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFacePaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    const v2, -0xff7501

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 143
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFacePaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 145
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mPaintDim:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    const v2, -0x7f000001

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mPaintDim:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 148
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 151
    new-instance v1, Lcom/sec/android/gallery3d/ui/CropView$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/ui/CropView$1;-><init>(Lcom/sec/android/gallery3d/ui/CropView;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mMainHandler:Landroid/os/Handler;

    .line 158
    new-instance v1, Landroid/view/ScaleGestureDetector;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    invoke-direct {v1, v2, v3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 159
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 160
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 161
    const-string v1, "set-as-contactphoto"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSetAsContactPhoto:Z

    .line 163
    :cond_0
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsManualFD:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/ui/CropView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/CropView;->check()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/gallery3d/ui/CropView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioX:F

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioY:F

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsPhotoFrame:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSetAsContactPhoto:Z

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    return-object v0
.end method

.method static synthetic access$2400()F
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsWallpaper:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSNote:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSView:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsCallerId:Z

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/android/gallery3d/ui/CropView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsCallerId:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/android/gallery3d/ui/CropView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;
    .param p1, "x1"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F

    return p1
.end method

.method static synthetic access$3400(Lcom/sec/android/gallery3d/ui/CropView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F

    return v0
.end method

.method static synthetic access$3402(Lcom/sec/android/gallery3d/ui/CropView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;
    .param p1, "x1"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFacePaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCropMenuHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/CropView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mTouch:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/gallery3d/ui/CropView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mTouch:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/CropView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    return v0
.end method

.method private check()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1243
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v3, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1244
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v3, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1245
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v3, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1246
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v3, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1247
    return-void
.end method

.method private setImageViewPosition(IIF)Z
    .locals 7
    .param p1, "centerX"    # I
    .param p2, "centerY"    # I
    .param p3, "scale"    # F

    .prologue
    const/4 v6, 0x0

    .line 191
    iget v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    sub-int v0, v4, p1

    .line 192
    .local v0, "inverseX":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    sub-int v1, v4, p2

    .line 193
    .local v1, "inverseY":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

    .line 194
    .local v3, "t":Lcom/sec/android/gallery3d/ui/TileImageView;
    iget v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageRotation:I

    .line 195
    .local v2, "rotation":I
    sparse-switch v2, :sswitch_data_0

    .line 200
    int-to-float v4, p1

    int-to-float v5, p2

    invoke-virtual {v3, v4, v5, p3, v6}, Lcom/sec/android/gallery3d/ui/TileImageView;->setPosition(FFFI)Z

    move-result v4

    :goto_0
    return v4

    .line 196
    :sswitch_0
    int-to-float v4, p1

    int-to-float v5, p2

    invoke-virtual {v3, v4, v5, p3, v6}, Lcom/sec/android/gallery3d/ui/TileImageView;->setPosition(FFFI)Z

    move-result v4

    goto :goto_0

    .line 197
    :sswitch_1
    int-to-float v4, p2

    int-to-float v5, v0

    const/16 v6, 0x5a

    invoke-virtual {v3, v4, v5, p3, v6}, Lcom/sec/android/gallery3d/ui/TileImageView;->setPosition(FFFI)Z

    move-result v4

    goto :goto_0

    .line 198
    :sswitch_2
    int-to-float v4, v0

    int-to-float v5, v1

    const/16 v6, 0xb4

    invoke-virtual {v3, v4, v5, p3, v6}, Lcom/sec/android/gallery3d/ui/TileImageView;->setPosition(FFFI)Z

    move-result v4

    goto :goto_0

    .line 199
    :sswitch_3
    int-to-float v4, v1

    int-to-float v5, p1

    const/16 v6, 0x10e

    invoke-virtual {v3, v4, v5, p3, v6}, Lcom/sec/android/gallery3d/ui/TileImageView;->setPosition(FFFI)Z

    move-result v4

    goto :goto_0

    .line 195
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public addItem(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1060
    return-void
.end method

.method public detectFaces(Landroid/graphics/Bitmap;)V
    .locals 14
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    .line 896
    iget v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageRotation:I

    .line 897
    .local v4, "rotation":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 898
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 899
    .local v3, "height":I
    const v8, 0x47ea6000    # 120000.0f

    mul-int v9, v7, v3

    int-to-float v9, v9

    div-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v5, v8

    .line 903
    .local v5, "scale":F
    div-int/lit8 v8, v4, 0x5a

    and-int/lit8 v8, v8, 0x1

    if-nez v8, :cond_0

    .line 904
    int-to-float v8, v7

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    and-int/lit8 v6, v8, -0x2

    .line 905
    .local v6, "w":I
    int-to-float v8, v3

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 906
    .local v2, "h":I
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 907
    .local v1, "faceBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 908
    .local v0, "canvas":Landroid/graphics/Canvas;
    int-to-float v8, v4

    int-to-float v9, v6

    div-float/2addr v9, v11

    int-to-float v10, v2

    div-float/2addr v10, v11

    invoke-virtual {v0, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 909
    int-to-float v8, v6

    int-to-float v9, v7

    div-float/2addr v8, v9

    int-to-float v9, v2

    int-to-float v10, v3

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->scale(FF)V

    .line 910
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8, v13}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v12, v12, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 922
    :goto_0
    new-instance v8, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;

    invoke-direct {v8, p0, v1}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;-><init>(Lcom/sec/android/gallery3d/ui/CropView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->start()V

    .line 923
    return-void

    .line 912
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "faceBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "h":I
    .end local v6    # "w":I
    :cond_0
    int-to-float v8, v3

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    and-int/lit8 v6, v8, -0x2

    .line 913
    .restart local v6    # "w":I
    int-to-float v8, v7

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 914
    .restart local v2    # "h":I
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 915
    .restart local v1    # "faceBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 916
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    int-to-float v8, v6

    div-float/2addr v8, v11

    int-to-float v9, v2

    div-float/2addr v9, v11

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 917
    int-to-float v8, v4

    invoke-virtual {v0, v8}, Landroid/graphics/Canvas;->rotate(F)V

    .line 918
    neg-int v8, v2

    int-to-float v8, v8

    div-float/2addr v8, v11

    neg-int v9, v6

    int-to-float v9, v9

    div-float/2addr v9, v11

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 919
    int-to-float v8, v6

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v2

    int-to-float v10, v7

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->scale(FF)V

    .line 920
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8, v13}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v12, v12, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public detectFaces(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)V
    .locals 16
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 1002
    if-nez p1, :cond_0

    .line 1035
    :goto_0
    return-void

    .line 1005
    :cond_0
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/CropView;->mImageRotation:I

    .line 1006
    .local v6, "rotation":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 1007
    .local v10, "width":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 1008
    .local v5, "height":I
    const-wide v12, 0x40fd4c0000000000L    # 120000.0

    mul-int v11, v10, v5

    int-to-double v14, v11

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v7, v12

    .line 1012
    .local v7, "scale":F
    div-int/lit8 v11, v6, 0x5a

    and-int/lit8 v11, v11, 0x1

    if-nez v11, :cond_1

    .line 1013
    int-to-float v11, v10

    mul-float/2addr v11, v7

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    and-int/lit8 v9, v11, -0x2

    .line 1014
    .local v9, "w":I
    int-to-float v11, v5

    mul-float/2addr v11, v7

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 1015
    .local v4, "h":I
    sget-object v11, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v4, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1016
    .local v3, "faceBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1017
    .local v2, "canvas":Landroid/graphics/Canvas;
    int-to-float v11, v6

    int-to-float v12, v9

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    int-to-float v13, v4

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    invoke-virtual {v2, v11, v12, v13}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1018
    int-to-float v11, v9

    int-to-float v12, v10

    div-float/2addr v11, v12

    int-to-float v12, v4

    int-to-float v13, v5

    div-float/2addr v12, v13

    invoke-virtual {v2, v11, v12}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1019
    const/4 v11, 0x0

    const/4 v12, 0x0

    new-instance v13, Landroid/graphics/Paint;

    const/4 v14, 0x2

    invoke-direct {v13, v14}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1032
    :goto_1
    new-instance v8, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v3}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;-><init>(Lcom/sec/android/gallery3d/ui/CropView;Landroid/graphics/Bitmap;)V

    .line 1033
    .local v8, "task":Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;
    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->setCallerRect(Landroid/graphics/RectF;)V

    .line 1034
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->start()V

    goto :goto_0

    .line 1021
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "faceBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "h":I
    .end local v8    # "task":Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;
    .end local v9    # "w":I
    :cond_1
    int-to-float v11, v5

    mul-float/2addr v11, v7

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    and-int/lit8 v9, v11, -0x2

    .line 1022
    .restart local v9    # "w":I
    int-to-float v11, v10

    mul-float/2addr v11, v7

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 1023
    .restart local v4    # "h":I
    sget-object v11, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v4, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1024
    .restart local v3    # "faceBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1025
    .restart local v2    # "canvas":Landroid/graphics/Canvas;
    int-to-float v11, v9

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    int-to-float v12, v4

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    invoke-virtual {v2, v11, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1026
    int-to-float v11, v6

    invoke-virtual {v2, v11}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1027
    neg-int v11, v4

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    neg-int v12, v9

    int-to-float v12, v12

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    invoke-virtual {v2, v11, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1028
    int-to-float v11, v9

    int-to-float v12, v5

    div-float/2addr v11, v12

    int-to-float v12, v4

    int-to-float v13, v10

    div-float/2addr v12, v13

    invoke-virtual {v2, v11, v12}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1029
    const/4 v11, 0x0

    const/4 v12, 0x0

    new-instance v13, Landroid/graphics/Paint;

    const/4 v14, 0x2

    invoke-direct {v13, v14}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public detectFaces(Landroid/graphics/Bitmap;Landroid/os/Handler;)V
    .locals 12
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "handle"    # Landroid/os/Handler;

    .prologue
    .line 966
    if-nez p1, :cond_0

    .line 997
    :goto_0
    return-void

    .line 969
    :cond_0
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCropMenuHandler:Landroid/os/Handler;

    .line 970
    iget v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageRotation:I

    .line 971
    .local v4, "rotation":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 972
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 973
    .local v3, "height":I
    const-wide v8, 0x40fd4c0000000000L    # 120000.0

    mul-int v10, v7, v3

    int-to-double v10, v10

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v5, v8

    .line 977
    .local v5, "scale":F
    div-int/lit8 v8, v4, 0x5a

    and-int/lit8 v8, v8, 0x1

    if-nez v8, :cond_1

    .line 978
    int-to-float v8, v7

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    and-int/lit8 v6, v8, -0x2

    .line 979
    .local v6, "w":I
    int-to-float v8, v3

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 980
    .local v2, "h":I
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 981
    .local v1, "faceBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 982
    .local v0, "canvas":Landroid/graphics/Canvas;
    int-to-float v8, v4

    int-to-float v9, v6

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    int-to-float v10, v2

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v0, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 983
    int-to-float v8, v6

    int-to-float v9, v7

    div-float/2addr v8, v9

    int-to-float v9, v2

    int-to-float v10, v3

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->scale(FF)V

    .line 984
    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Landroid/graphics/Paint;

    const/4 v11, 0x2

    invoke-direct {v10, v11}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 996
    :goto_1
    new-instance v8, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;

    invoke-direct {v8, p0, v1}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;-><init>(Lcom/sec/android/gallery3d/ui/CropView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->start()V

    goto :goto_0

    .line 986
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "faceBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "h":I
    .end local v6    # "w":I
    :cond_1
    int-to-float v8, v3

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    and-int/lit8 v6, v8, -0x2

    .line 987
    .restart local v6    # "w":I
    int-to-float v8, v7

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 988
    .restart local v2    # "h":I
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 989
    .restart local v1    # "faceBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 990
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    int-to-float v8, v6

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    int-to-float v9, v2

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 991
    int-to-float v8, v4

    invoke-virtual {v0, v8}, Landroid/graphics/Canvas;->rotate(F)V

    .line 992
    neg-int v8, v2

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    neg-int v9, v6

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 993
    int-to-float v8, v6

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v2

    int-to-float v10, v7

    div-float/2addr v9, v10

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->scale(FF)V

    .line 994
    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Landroid/graphics/Paint;

    const/4 v11, 0x2

    invoke-direct {v10, v11}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1065
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public getCropRectangle()Landroid/graphics/RectF;
    .locals 7

    .prologue
    .line 216
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->getVisibility()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 217
    const/4 v1, 0x0

    .line 221
    :goto_0
    return-object v1

    .line 218
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;

    move-result-object v0

    .line 219
    .local v0, "rect":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 221
    .local v1, "result":Landroid/graphics/RectF;
    goto :goto_0
.end method

.method public getCropRectangleEx(II)Landroid/graphics/RectF;
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 1051
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->getVisibility()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1052
    const/4 v1, 0x0

    .line 1055
    :goto_0
    return-object v1

    .line 1053
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;

    move-result-object v0

    .line 1054
    .local v0, "rect":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    int-to-float v3, p1

    mul-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    int-to-float v4, p2

    mul-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->right:F

    int-to-float v5, p1

    mul-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v6, p2

    mul-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1055
    .local v1, "result":Landroid/graphics/RectF;
    goto :goto_0
.end method

.method public getHighlightRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getImageHeight()I
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    return v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    return v0
.end method

.method public getMaxIdentityRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1239
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->identityRect(Landroid/graphics/RectF;Z)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public initializeHighlightRectangle(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 1038
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/ui/CropView;->initializeHighlightRectangle(Landroid/graphics/RectF;Z)V

    .line 1039
    return-void
.end method

.method public initializeHighlightRectangle(Landroid/graphics/RectF;Z)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "setVisible"    # Z

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    # invokes: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setInitRectangle(Landroid/graphics/RectF;)V
    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$3200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;Landroid/graphics/RectF;)V

    .line 1043
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setVisibility(I)V

    .line 1044
    return-void

    .line 1043
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 176
    const-string v2, "CropView"

    const-string v3, "onLayout"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    sub-int v1, p4, p2

    .line 178
    .local v1, "width":I
    sub-int v0, p5, p3

    .line 180
    .local v0, "height":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    invoke-virtual {v2, v4, v4, v1, v0}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->layout(IIII)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    invoke-virtual {v2, v4, v4, v1, v0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->layout(IIII)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v2, v4, v4, v1, v0}, Lcom/sec/android/gallery3d/ui/TileImageView;->layout(IIII)V

    .line 183
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsCalledLayout:Z

    .line 184
    iget v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 185
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->initialize()V

    .line 187
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    .line 188
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 934
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TileImageView;->freeTextures()V

    .line 935
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 206
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F

    float-to-int v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getScale()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView;->setImageViewPosition(IIF)Z

    .line 207
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 208
    return-void
.end method

.method public renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 0
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 212
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->clearBuffer()V

    .line 213
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 926
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TileImageView;->prepareTextures()V

    .line 927
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsManualFD:Z

    if-eqz v0, :cond_0

    .line 928
    const v0, 0x3ecccccd    # 0.4f

    sput v0, Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F

    .line 931
    :goto_0
    return-void

    .line 930
    :cond_0
    const v0, 0x3f19999a    # 0.6f

    sput v0, Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F

    goto :goto_0
.end method

.method public setAspectRatio(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 166
    iput p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F

    .line 167
    return-void
.end method

.method public setDataModel(Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;I)V
    .locals 2
    .param p1, "dataModel"    # Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;
    .param p2, "rotation"    # I

    .prologue
    .line 879
    div-int/lit8 v0, p2, 0x5a

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 880
    invoke-interface {p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    .line 881
    invoke-interface {p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    .line 887
    :goto_0
    iput p2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageRotation:I

    .line 889
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageView:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/TileImageView;->setModel(Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;)V

    .line 890
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->initialize()V

    .line 891
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsCalledLayout:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 892
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView;->requestLayout()V

    .line 893
    :cond_0
    return-void

    .line 883
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I

    .line 884
    invoke-interface {p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileSource;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I

    goto :goto_0
.end method

.method public setIsPhotoFrame(Z)V
    .locals 0
    .param p1, "isPhotoFrame"    # Z

    .prologue
    .line 946
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsPhotoFrame:Z

    .line 947
    return-void
.end method

.method public setIsSNote(Z)V
    .locals 0
    .param p1, "isSNote"    # Z

    .prologue
    .line 942
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSNote:Z

    .line 943
    return-void
.end method

.method public setIsSView(Z)V
    .locals 0
    .param p1, "isSView"    # Z

    .prologue
    .line 958
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSView:Z

    .line 959
    return-void
.end method

.method public setIsWallpaper(Z)V
    .locals 0
    .param p1, "isWallpaper"    # Z

    .prologue
    .line 938
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsWallpaper:Z

    .line 939
    return-void
.end method

.method public setManualFD(Z)V
    .locals 0
    .param p1, "isManualFD"    # Z

    .prologue
    .line 1047
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsManualFD:Z

    .line 1048
    return-void
.end method

.method public setMultiFrame()V
    .locals 1

    .prologue
    .line 954
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z

    .line 955
    return-void
.end method

.method public setSingleFrame()V
    .locals 1

    .prologue
    .line 950
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z

    .line 951
    return-void
.end method

.method public setSpotlightRatio(FF)V
    .locals 0
    .param p1, "ratioX"    # F
    .param p2, "ratioY"    # F

    .prologue
    .line 170
    iput p1, p0, Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioX:F

    .line 171
    iput p2, p0, Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioY:F

    .line 172
    return-void
.end method
