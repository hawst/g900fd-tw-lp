.class Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;
.super Ljava/lang/Thread;
.source "FaceIndicatorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->loadCandidates([I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

.field final synthetic val$faceIds:[I


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;[I)V
    .locals 0

    .prologue
    .line 962
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;->val$faceIds:[I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 965
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/lang/Object;

    move-result-object v11

    monitor-enter v11

    .line 966
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 967
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 968
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;->val$faceIds:[I

    .local v6, "arr$":[I
    array-length v10, v6

    .local v10, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v10, :cond_0

    aget v9, v6, v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 969
    .local v9, "id":I
    const/4 v7, 0x0

    .line 971
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$7;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2200(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    .line 972
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->GET_SIMILAR_PERSONS_URI:Landroid/net/Uri;

    int-to-long v4, v9

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 973
    .local v1, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 977
    :try_start_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 968
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 974
    :catch_0
    move-exception v2

    .line 977
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 981
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "arr$":[I
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "i$":I
    .end local v9    # "id":I
    .end local v10    # "len$":I
    :catchall_0
    move-exception v2

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 977
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "arr$":[I
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "i$":I
    .restart local v9    # "id":I
    .restart local v10    # "len$":I
    :catchall_1
    move-exception v2

    :try_start_3
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 981
    .end local v6    # "arr$":[I
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "i$":I
    .end local v9    # "id":I
    .end local v10    # "len$":I
    :cond_0
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 982
    return-void
.end method
