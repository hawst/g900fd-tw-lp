.class public Lcom/sec/android/gallery3d/util/FeatureManager;
.super Ljava/lang/Object;
.source "FeatureManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;,
        Lcom/sec/android/gallery3d/util/FeatureManager$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPreTask:Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;

.field private mState:Lcom/sec/android/gallery3d/util/FeatureManager$State;

.field private mWaitToComplete:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/gallery3d/util/FeatureManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/FeatureManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mContext:Landroid/content/Context;

    .line 28
    sget-object v1, Lcom/sec/android/gallery3d/util/FeatureManager$State;->ACTIVE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mState:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    .line 29
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mWaitToComplete:Z

    .line 30
    const-class v1, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "threadName":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;-><init>(Lcom/sec/android/gallery3d/util/FeatureManager;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mPreTask:Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;

    .line 32
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mPreTask:Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;->start()V

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/util/FeatureManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/FeatureManager;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/util/FeatureManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/FeatureManager;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/util/FeatureManager;->setPrepareScreenNailResult(Z)V

    return-void
.end method

.method private setPrepareScreenNailResult(Z)V
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 52
    monitor-enter p0

    .line 53
    if-eqz p1, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->DONE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    :goto_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mState:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    .line 54
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mWaitToComplete:Z

    if-nez v0, :cond_1

    .line 55
    monitor-exit p0

    .line 58
    :goto_1
    return-void

    .line 53
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->FAILED:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 57
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public waitToBeCompleted()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 36
    monitor-enter p0

    .line 37
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mState:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    sget-object v3, Lcom/sec/android/gallery3d/util/FeatureManager$State;->IDLE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mState:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    sget-object v3, Lcom/sec/android/gallery3d/util/FeatureManager$State;->FAILED:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    if-ne v2, v3, :cond_2

    .line 38
    :cond_0
    monitor-exit p0

    move v0, v1

    .line 48
    :cond_1
    :goto_0
    return v0

    .line 39
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mState:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    sget-object v3, Lcom/sec/android/gallery3d/util/FeatureManager$State;->DONE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    if-ne v2, v3, :cond_3

    .line 40
    monitor-exit p0

    goto :goto_0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 42
    :cond_3
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mWaitToComplete:Z

    .line 43
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 44
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/FeatureManager;->mState:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    sget-object v3, Lcom/sec/android/gallery3d/util/FeatureManager$State;->DONE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    if-eq v2, v3, :cond_1

    move v0, v1

    .line 48
    goto :goto_0
.end method
