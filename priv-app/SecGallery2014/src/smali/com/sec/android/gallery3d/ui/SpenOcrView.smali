.class public Lcom/sec/android/gallery3d/ui/SpenOcrView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "SpenOcrView.java"

# interfaces
.implements Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;


# static fields
.field private static final COLOR_GUIDE_OUTLINE:I = 0xff0000

.field private static final MSG_RECOGNIZE_COMPLETE:I = 0x2

.field private static final MSG_RECOGNIZE_TEXT:I = 0x1

.field private static final MSG_SHOW_OCR:I = 0x3

.field private static final SPEN_OCR_NONE:I = 0x0

.field private static final SPEN_OCR_RECOGNIZING:I = 0x2

.field private static final SPEN_OCR_START:I = 0x1

.field private static final STATE:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SpenOcrView"

.field private static final WIDTH_GUIDEO_UTLIN:F = 4.0f


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mContext:Landroid/content/Context;

.field private mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private final mHandler:Landroid/os/Handler;

.field private mImageBounds:Landroid/graphics/Rect;

.field private mMediaWidth:I

.field private mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

.field mOCRRect:Landroid/graphics/Rect;

.field private mOcrBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mOcrMovementDetector:Lcom/dmc/ocr/OcrMovementDetector;

.field private mPoint:Landroid/graphics/Point;

.field private mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

.field mRecognizer:Lcom/dmc/ocr/OcrRecognizer;

.field mScreenRect:Landroid/graphics/Rect;

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SPEN_OCR_NONE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SPEN_OCR_RECT"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SPEN_OCR_RECOGNIZING"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->STATE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 125
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 41
    iput v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mMediaWidth:I

    .line 43
    iput v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mState:I

    .line 47
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mScreenRect:Landroid/graphics/Rect;

    .line 54
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    .line 55
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPoint:Landroid/graphics/Point;

    .line 71
    new-instance v0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;-><init>(Lcom/sec/android/gallery3d/ui/SpenOcrView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mHandler:Landroid/os/Handler;

    .line 126
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;

    .line 129
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;

    const v2, 0x7f02021f

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOcrBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 132
    new-instance v0, Lcom/dmc/ocr/OcrMovementDetector;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dmc/ocr/OcrMovementDetector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOcrMovementDetector:Lcom/dmc/ocr/OcrMovementDetector;

    .line 133
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOcrMovementDetector:Lcom/dmc/ocr/OcrMovementDetector;

    new-instance v1, Lcom/sec/android/gallery3d/ui/SpenOcrView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView$2;-><init>(Lcom/sec/android/gallery3d/ui/SpenOcrView;)V

    invoke-virtual {v0, v1}, Lcom/dmc/ocr/OcrMovementDetector;->setMoveDetectorListener(Lcom/dmc/ocr/OcrMovementDetector$OcrMovementDetectorListener;)V

    .line 141
    new-instance v0, Lcom/dmc/ocr/OcrRecognizer;

    invoke-direct {v0, p0}, Lcom/dmc/ocr/OcrRecognizer;-><init>(Lcom/dmc/ocr/OcrRecognizer$OcrRecongizerLisenter;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mRecognizer:Lcom/dmc/ocr/OcrRecognizer;

    .line 142
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/SpenOcrView;ILjava/lang/String;Landroid/graphics/Point;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/graphics/Point;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->prepareShowOCR(ILjava/lang/String;Landroid/graphics/Point;)V

    return-void
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/ui/SpenOcrView;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;
    .param p1, "x1"    # Landroid/graphics/Point;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPoint:Landroid/graphics/Point;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/SpenOcrView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->setState(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Lcom/sec/android/gallery3d/ui/OCRPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/SpenOcrView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mMediaWidth:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/SpenOcrView;Landroid/graphics/Rect;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SpenOcrView;
    .param p1, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->onRecognized(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method private getOCRRectOnPhoto(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "imageBounds"    # Landroid/graphics/Rect;

    .prologue
    const/4 v5, 0x0

    .line 200
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 202
    .local v0, "ocrRect":Landroid/graphics/Rect;
    iget v2, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mMediaWidth:I

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v3, v4

    div-float v1, v2, v3

    .line 204
    .local v1, "ratio":F
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 205
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v1

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 206
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 207
    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 209
    iget v2, v0, Landroid/graphics/Rect;->top:I

    if-gez v2, :cond_0

    .line 210
    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 211
    :cond_0
    iget v2, v0, Landroid/graphics/Rect;->right:I

    if-gez v2, :cond_1

    .line 212
    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 214
    :cond_1
    return-object v0
.end method

.method private getState()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mState:I

    return v0
.end method

.method private onRecognized(Landroid/graphics/Rect;)Z
    .locals 4
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 169
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v2, :cond_0

    .line 181
    :goto_0
    return v0

    .line 172
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->setImageBounds(Landroid/graphics/Rect;)V

    .line 174
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mScreenRect:Landroid/graphics/Rect;

    .line 176
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->setState(I)V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->invalidate()V

    .line 179
    const-string v0, "SpenOcrView"

    sget-object v2, Lcom/sec/android/gallery3d/ui/SpenOcrView;->STATE:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->getState()I

    move-result v3

    aget-object v2, v2, v3

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 181
    goto :goto_0
.end method

.method private prepareShowOCR(ILjava/lang/String;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "Type"    # I
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "pos"    # Landroid/graphics/Point;

    .prologue
    .line 58
    const-string v1, "SpenOcrView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Text : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    if-nez v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0f0175

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    .line 62
    .local v0, "container":Landroid/view/ViewGroup;
    new-instance v1, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    .line 65
    .end local v0    # "container":Landroid/view/ViewGroup;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->prepareWindow(ILjava/lang/String;)V

    .line 66
    return-void
.end method

.method private setImageBounds(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "imageBounds"    # Landroid/graphics/Rect;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;

    .line 160
    return-void
.end method

.method private setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 189
    iput p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mState:I

    .line 190
    return-void
.end method


# virtual methods
.method public deletePopup()V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRPopup:Lcom/sec/android/gallery3d/ui/OCRPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/OCRPopupWindow;->dismiss()V

    .line 278
    :cond_0
    return-void
.end method

.method drawOcrAreaRect(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 218
    new-instance v6, Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    invoke-direct {v6}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;-><init>()V

    .line 219
    .local v6, "glPaint":Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    const/high16 v0, 0xff0000

    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setColor(I)V

    .line 220
    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setLineWidth(F)V

    .line 222
    const-string v0, "SpenOcrView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rect"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOcrBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget v2, p2, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 226
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 262
    invoke-super/range {p0 .. p5}, Lcom/sec/android/gallery3d/ui/GLView;->onLayout(ZIIII)V

    .line 263
    return-void
.end method

.method public onRecognizeComplet(Landroid/content/Intent;)V
    .locals 4
    .param p1, "requestIntent"    # Landroid/content/Intent;

    .prologue
    .line 267
    if-eqz p1, :cond_0

    .line 268
    const-string v0, "pos"

    new-instance v1, Landroid/graphics/Point;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 272
    return-void
.end method

.method public onStylusButtonEvent(Landroid/view/MotionEvent;I)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "clipboardId"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOcrMovementDetector:Lcom/dmc/ocr/OcrMovementDetector;

    invoke-virtual {v0, p1, p2}, Lcom/dmc/ocr/OcrMovementDetector;->onStylusButtonEvent(Landroid/view/MotionEvent;I)V

    .line 195
    const-string v0, "ocr"

    const-string v1, "SpenOcrView>> onStylusButtonEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-void
.end method

.method public declared-synchronized render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 231
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->getState()I

    move-result v5

    if-ne v5, v6, :cond_2

    .line 234
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mScreenRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;

    invoke-direct {p0, v5, v6}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->getOCRRectOnPhoto(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    .line 236
    const/4 v5, 0x2

    iput v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mState:I

    .line 238
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 244
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPoint:Landroid/graphics/Point;

    if-eqz v5, :cond_1

    .line 245
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v3, v5

    .line 246
    .local v3, "x":F
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v4, v5

    .line 248
    .local v4, "y":F
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mImageBounds:Landroid/graphics/Rect;

    .line 249
    .local v0, "imageBounds":Landroid/graphics/Rect;
    iget v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mMediaWidth:I

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v6, v7

    div-float v2, v5, v6

    .line 250
    .local v2, "ratio":F
    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    div-float v6, v3, v2

    add-float v3, v5, v6

    .line 251
    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    div-float v6, v4, v2

    add-float v4, v5, v6

    .line 253
    new-instance v1, Landroid/graphics/Point;

    float-to-int v5, v3

    float-to-int v6, v4

    invoke-direct {v1, v5, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 254
    .local v1, "point":Landroid/graphics/Point;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 256
    .end local v0    # "imageBounds":Landroid/graphics/Rect;
    .end local v1    # "point":Landroid/graphics/Point;
    .end local v2    # "ratio":F
    .end local v3    # "x":F
    .end local v4    # "y":F
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    monitor-exit p0

    return-void

    .line 240
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->getState()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-ne v5, v7, :cond_0

    goto :goto_0

    .line 231
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public setPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->deletePopup()V

    .line 146
    if-nez p1, :cond_0

    .line 155
    :goto_0
    return-void

    .line 148
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 149
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v0, :cond_1

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mMediaWidth:I

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->invalidate()V

    goto :goto_0
.end method

.method public setPositionController(Lcom/sec/android/gallery3d/ui/PositionController;)V
    .locals 1
    .param p1, "positionController"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 164
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->setImageBounds(Landroid/graphics/Rect;)V

    .line 165
    return-void
.end method
