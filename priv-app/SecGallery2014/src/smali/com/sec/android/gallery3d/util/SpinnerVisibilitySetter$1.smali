.class Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;
.super Landroid/os/Handler;
.source "SpinnerVisibilitySetter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x7d0

    const-wide/16 v4, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 56
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 58
    :pswitch_0
    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->removeMessages(I)V

    .line 59
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    # getter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$000(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 60
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    # setter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$002(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;J)J

    .line 61
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    # getter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$100(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_0

    .line 64
    :pswitch_1
    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->removeMessages(I)V

    .line 65
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    # getter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$000(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 66
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    # getter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J
    invoke-static {v4}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$000(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 67
    .local v0, "t":J
    cmp-long v2, v0, v8

    if-ltz v2, :cond_1

    .line 68
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    const-wide/16 v4, -0x1

    # setter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mSpinnerVisibilityStartTime:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$002(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;J)J

    .line 69
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    # getter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$100(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    goto :goto_0

    .line 71
    :cond_1
    sub-long v2, v8, v0

    invoke-virtual {p0, v7, v2, v3}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 75
    .end local v0    # "t":J
    :pswitch_2
    sget-object v3, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->sInstanceMap:Ljava/util/WeakHashMap;

    monitor-enter v3

    .line 76
    :try_start_0
    sget-object v2, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->sInstanceMap:Ljava/util/WeakHashMap;

    iget-object v4, p0, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter$1;->this$0:Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    # getter for: Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->access$100(Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 56
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
