.class Lcom/sec/android/gallery3d/glcore/GlObject$1;
.super Ljava/lang/Object;
.source "GlObject.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field handled:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlObject;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0

    .prologue
    .line 1786
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()Z
    .locals 3

    .prologue
    .line 1817
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1818
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    if-eqz v0, :cond_0

    .line 1819
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;->onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1821
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onClicked()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1822
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    return v0
.end method

.method public onGenericMotionTouch(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 1835
    if-ne p1, v1, :cond_0

    if-ne p2, v1, :cond_0

    .line 1836
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    if-eqz v1, :cond_0

    .line 1837
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    .line 1847
    :goto_0
    return v0

    .line 1842
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    if-eqz v1, :cond_1

    .line 1843
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;->onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    goto :goto_0

    .line 1847
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLongClick(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1825
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1826
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mLongClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    if-eqz v0, :cond_0

    .line 1827
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mLongClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;->onLongClick(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1829
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onLongClicked()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1830
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    return v0
.end method

.method public onMove(II)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 1798
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1799
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    if-eqz v0, :cond_0

    .line 1800
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;->onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1802
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->onMoved(II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1803
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    return v0
.end method

.method public onPress(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1790
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1791
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    if-eqz v0, :cond_0

    .line 1792
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;->onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1794
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->onPressed(II)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1795
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    return v0
.end method

.method public onRelease(IIII)Z
    .locals 7
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 1809
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1810
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    if-eqz v0, :cond_0

    .line 1811
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;->onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z

    move-result v0

    or-int/2addr v0, v6

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1813
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlObject;->onReleased(IIII)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    .line 1814
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->handled:Z

    return v0
.end method

.method public onScroll(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 1806
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->onMoved(II)Z

    move-result v0

    return v0
.end method
