.class public Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;
.super Ljava/lang/Object;
.source "FaceClustering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/FaceClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageFaceValue"
.end annotation


# instance fields
.field public mFaceId:I

.field public mMaxSimilarity:I

.field public mPath:Lcom/sec/android/gallery3d/data/Path;

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/FaceClustering;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p2, "faceId"    # I
    .param p3, "maxSimilarity"    # I
    .param p4, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 660
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->this$0:Lcom/sec/android/gallery3d/data/FaceClustering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662
    iput p2, p0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mFaceId:I

    .line 663
    iput p3, p0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mMaxSimilarity:I

    .line 664
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 665
    return-void
.end method
