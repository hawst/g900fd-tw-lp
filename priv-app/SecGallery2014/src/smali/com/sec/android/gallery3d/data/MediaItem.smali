.class public abstract Lcom/sec/android/gallery3d/data/MediaItem;
.super Lcom/sec/android/gallery3d/data/MediaObject;
.source "MediaItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/MediaItem$TagType;
    }
.end annotation


# static fields
.field private static final ATTRIBUTE_BYTESBUFFER_SIZE:I = 0x4

.field public static final ATTR_3DMPO:J = 0x2L

.field public static final ATTR_3DPANORAMA:J = 0x8L

.field public static final ATTR_3DTOUR:J = 0x400L

.field public static final ATTR_AGIF:J = 0x20L

.field public static final ATTR_BESTFACE:J = 0x2000L

.field public static final ATTR_BESTPHOTO:J = 0x1000L

.field public static final ATTR_BESTSHOT:J = 0x1L

.field public static final ATTR_BURSTSHOT:J = 0x200L

.field public static final ATTR_DRAMASHOT:J = 0x8000L

.field public static final ATTR_ERASER:J = 0x4000L

.field public static final ATTR_GOLFSHOT:J = 0x80L

.field public static final ATTR_MAGICSHOT:J = 0x800L

.field public static final ATTR_OUTOFFOCUS:J = 0x20000L

.field public static final ATTR_PHOTONOTE:J = 0x80000L

.field public static final ATTR_PICMOTION:J = 0x10000L

.field public static final ATTR_SEQUENCE:J = 0x40000L

.field public static final ATTR_SOUND_SCENE:J = 0x10L

.field public static final ATTR_SPANORAMA:J = 0x4L

.field public static final ATTR_SPEN:J = 0x100L

.field private static final BYTESBUFFER_SIZE:I = 0x32000

.field private static final BYTESBUFFE_POOL_SIZE:I = 0x4

.field public static final CACHED_IMAGE_QUALITY:I = 0x5f

.field public static final CASE_DELETE:I = 0x1

.field public static final CASE_NONE:I = 0x0

.field public static final CASE_REMOVE_TAG:I = 0x3

.field public static final CASE_ROTATE:I = 0x2

.field public static final IMAGE_ERROR:I = -0x1

.field public static final IMAGE_READY:I = 0x0

.field public static final IMAGE_WAIT:I = 0x1

.field public static final INVALID_LATLNG:D = 0.0

.field public static final ITEM_TYPE_CONFIRMED:I = 0x1

.field public static final ITEM_TYPE_NULL:I = -0x1

.field public static final ITEM_TYPE_UNCONFIRMED:I = 0x0

.field public static final MIME_TYPE_JPEG:Ljava/lang/String; = "image/jpeg"

.field public static final NEED_LOAD_FACE_POSITION:I = -0x1

.field public static final NO_FACE_POSITION:I = -0x2

.field public static final TYPE_ATTRIBUTE:I = 0x6

.field public static final TYPE_CROPTHUMBNAIL:I = 0x4

.field public static final TYPE_LARGE_WIDGET_THUMBNAIL:I = 0x5

.field public static final TYPE_MICROTHUMBNAIL:I = 0x2

.field public static final TYPE_MINI_MICROTHUMBNAIL:I = 0x3

.field public static final TYPE_THUMBNAIL:I = 0x1

.field private static final sAttributeBufferPool:Lcom/sec/android/gallery3d/data/BytesBufferPool;

.field private static final sMicroThumbBufferPool:Lcom/sec/android/gallery3d/data/BytesBufferPool;

.field private static sMicrothumbnailTargetSize:I

.field private static sMiniMicroThumbnailTargetSize:I

.field private static sThumbnailTargetSize:I


# instance fields
.field protected drmInfo:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

.field public hasCache:Z

.field protected is3dpanorama:Z

.field private isBroken:Z

.field protected isSceretBoxItem:Z

.field protected isdrm:Z

.field protected mAttribute:J

.field protected mAttributePending:J

.field private mExpansion:Z

.field private mIsAGIF:Z

.field private mIsSamsungMakerChecked:Z

.field private mIsSamsungMakerFilter:Z

.field protected mParentSetPath:Lcom/sec/android/gallery3d/data/Path;

.field public mRefreshable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 49
    const/16 v0, 0xc8

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicrothumbnailTargetSize:I

    .line 50
    new-instance v0, Lcom/sec/android/gallery3d/data/BytesBufferPool;

    const v1, 0x32000

    invoke-direct {v0, v2, v1}, Lcom/sec/android/gallery3d/data/BytesBufferPool;-><init>(II)V

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicroThumbBufferPool:Lcom/sec/android/gallery3d/data/BytesBufferPool;

    .line 52
    new-instance v0, Lcom/sec/android/gallery3d/data/BytesBufferPool;

    invoke-direct {v0, v2, v2}, Lcom/sec/android/gallery3d/data/BytesBufferPool;-><init>(II)V

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaItem;->sAttributeBufferPool:Lcom/sec/android/gallery3d/data/BytesBufferPool;

    .line 55
    const/16 v0, 0x280

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sThumbnailTargetSize:I

    .line 90
    sget v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicrothumbnailTargetSize:I

    div-int/lit8 v0, v0, 0x2

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMiniMicroThumbnailTargetSize:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;J)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "version"    # J

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 131
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/data/MediaObject;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 87
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsAGIF:Z

    .line 107
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mExpansion:Z

    .line 108
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mRefreshable:Z

    .line 109
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->isdrm:Z

    .line 111
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->is3dpanorama:Z

    .line 112
    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttribute:J

    .line 113
    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttributePending:J

    .line 114
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->isSceretBoxItem:Z

    .line 127
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->hasCache:Z

    .line 353
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsSamsungMakerChecked:Z

    .line 354
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsSamsungMakerFilter:Z

    .line 132
    return-void
.end method

.method public static getAttributeBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/sec/android/gallery3d/data/MediaItem;->sAttributeBufferPool:Lcom/sec/android/gallery3d/data/BytesBufferPool;

    return-object v0
.end method

.method public static getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicroThumbBufferPool:Lcom/sec/android/gallery3d/data/BytesBufferPool;

    return-object v0
.end method

.method public static getTargetSize(I)I
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 197
    packed-switch p0, :pswitch_data_0

    .line 207
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "should only request thumb/microthumb from cache"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :pswitch_0
    sget v0, Lcom/sec/android/gallery3d/data/MediaItem;->sThumbnailTargetSize:I

    .line 205
    :goto_0
    return v0

    .line 203
    :pswitch_1
    sget v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicrothumbnailTargetSize:I

    goto :goto_0

    .line 205
    :pswitch_2
    sget v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMiniMicroThumbnailTargetSize:I

    goto :goto_0

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static setThumbnailSizes(IF)V
    .locals 1
    .param p0, "size"    # I
    .param p1, "density"    # F

    .prologue
    .line 221
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isOverWQHD(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/high16 v0, 0x40800000    # 4.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    .line 222
    :cond_0
    div-int/lit8 v0, p0, 0x4

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sThumbnailTargetSize:I

    .line 223
    div-int/lit8 v0, p0, 0x8

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicrothumbnailTargetSize:I

    .line 224
    div-int/lit8 v0, p0, 0x14

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMiniMicroThumbnailTargetSize:I

    .line 234
    :goto_0
    return-void

    .line 225
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChangeThumbnail:Z

    if-nez v0, :cond_2

    const/high16 v0, 0x40000000    # 2.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    .line 226
    :cond_2
    div-int/lit8 v0, p0, 0x2

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sThumbnailTargetSize:I

    .line 227
    div-int/lit8 v0, p0, 0x5

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicrothumbnailTargetSize:I

    .line 228
    div-int/lit8 v0, p0, 0xa

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMiniMicroThumbnailTargetSize:I

    goto :goto_0

    .line 230
    :cond_3
    div-int/lit8 v0, p0, 0x3

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sThumbnailTargetSize:I

    .line 231
    div-int/lit8 v0, p0, 0x6

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMicrothumbnailTargetSize:I

    .line 232
    div-int/lit8 v0, p0, 0xf

    sput v0, Lcom/sec/android/gallery3d/data/MediaItem;->sMiniMicroThumbnailTargetSize:I

    goto :goto_0
.end method


# virtual methods
.method public download(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 351
    return-void
.end method

.method public getAgifMode()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsAGIF:Z

    return v0
.end method

.method public getDRMInfo()Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->drmInfo:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    return-object v0
.end method

.method public getDateInMs()J
    .locals 2

    .prologue
    .line 135
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getExpansion()Z
    .locals 1

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mExpansion:Z

    return v0
.end method

.method public getFacePath(I)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 398
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFaces()[Lcom/sec/android/gallery3d/data/Face;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFaces(Lcom/sec/android/gallery3d/data/MediaItem$TagType;)[Lcom/sec/android/gallery3d/data/Face;
    .locals 1
    .param p1, "type"    # Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    .prologue
    .line 250
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const-string v0, ""

    return-object v0
.end method

.method public getFullImageRotation()I
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    return v0
.end method

.method public abstract getGroupId()J
.end method

.method public abstract getHeight()I
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    return v0
.end method

.method public getItemType()I
    .locals 1

    .prologue
    .line 255
    const/4 v0, -0x1

    return v0
.end method

.method public getLatLong([D)V
    .locals 4
    .param p1, "latLong"    # [D

    .prologue
    const-wide/16 v2, 0x0

    .line 143
    const/4 v0, 0x0

    aput-wide v2, p1, v0

    .line 144
    const/4 v0, 0x1

    aput-wide v2, p1, v0

    .line 145
    return-void
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 329
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 332
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public abstract getMimeType()Ljava/lang/String;
.end method

.method public getModifiedDateInSec()J
    .locals 2

    .prologue
    .line 242
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentSetPath()Lcom/sec/android/gallery3d/data/Path;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mParentSetPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method public getParentSetPathInMultiPick(Lcom/sec/android/gallery3d/data/MediaObject;)Lcom/sec/android/gallery3d/data/Path;
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v0, 0x0

    .line 385
    if-nez p1, :cond_1

    .line 394
    .end local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    :goto_0
    return-object v0

    .line 387
    .restart local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 388
    const-string v0, "/local/image"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    check-cast p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    goto :goto_0

    .line 390
    .restart local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 391
    const-string v0, "/local/video"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    check-cast p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    goto :goto_0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 169
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getTags()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTags(Lcom/sec/android/gallery3d/data/MediaItem$TagType;)[Ljava/lang/String;
    .locals 1
    .param p1, "type"    # Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    .prologue
    .line 246
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getWidth()I
.end method

.method public hasAttribute(J)Z
    .locals 3
    .param p1, "option"    # J

    .prologue
    .line 295
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttribute:J

    and-long/2addr v0, p1

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPendingAttribute(J)Z
    .locals 3
    .param p1, "option"    # J

    .prologue
    .line 310
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttributePending:J

    and-long/2addr v0, p1

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public is3DPanorama()Z
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->is3dpanorama:Z

    return v0
.end method

.method public isBroken()Z
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken:Z

    return v0
.end method

.method public isDrm()Z
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->isdrm:Z

    return v0
.end method

.method public isGolf()Z
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    return v0
.end method

.method public isHiddenItem()Z
    .locals 1

    .prologue
    .line 325
    const/4 v0, 0x0

    return v0
.end method

.method public isSamsungMakerFilter()Z
    .locals 5

    .prologue
    .line 356
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsSamsungMakerChecked:Z

    if-nez v4, :cond_2

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 358
    .local v2, "filePath":Ljava/lang/String;
    if-nez v2, :cond_0

    const/4 v4, 0x0

    .line 371
    .end local v2    # "filePath":Ljava/lang/String;
    :goto_0
    return v4

    .line 360
    .restart local v2    # "filePath":Ljava/lang/String;
    :cond_0
    :try_start_0
    new-instance v1, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 361
    .local v1, "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V

    .line 362
    sget v4, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_MAKE:I

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTagStringValue(I)Ljava/lang/String;

    move-result-object v3

    .line 363
    .local v3, "maker":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 364
    const-string v4, "SAMSUNG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsSamsungMakerFilter:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    .end local v1    # "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v3    # "maker":Ljava/lang/String;
    :cond_1
    :goto_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsSamsungMakerChecked:Z

    .line 371
    .end local v2    # "filePath":Ljava/lang/String;
    :cond_2
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsSamsungMakerFilter:Z

    goto :goto_0

    .line 366
    .restart local v2    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public isSdCardFile()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public refreshAvailable()Z
    .locals 1

    .prologue
    .line 347
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mRefreshable:Z

    return v0
.end method

.method public abstract requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end method

.method public abstract requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation
.end method

.method public requestRefresh(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    const/4 v0, 0x0

    return-object v0
.end method

.method public resetAgifMode(Z)V
    .locals 0
    .param p1, "isAGIF"    # Z

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mIsAGIF:Z

    .line 263
    return-void
.end method

.method public resetPendingAttribute()V
    .locals 2

    .prologue
    .line 306
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttributePending:J

    .line 307
    return-void
.end method

.method public setAttribute(JZ)V
    .locals 5
    .param p1, "option"    # J
    .param p3, "set"    # Z

    .prologue
    .line 299
    if-eqz p3, :cond_0

    .line 300
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttribute:J

    or-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttribute:J

    .line 303
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttribute:J

    const-wide/16 v2, -0x1

    xor-long/2addr v2, p1

    and-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttribute:J

    goto :goto_0
.end method

.method public setBroken(Z)V
    .locals 0
    .param p1, "broken"    # Z

    .prologue
    .line 291
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken:Z

    .line 292
    return-void
.end method

.method public setExpansion(Z)V
    .locals 0
    .param p1, "e"    # Z

    .prologue
    .line 339
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mExpansion:Z

    .line 340
    return-void
.end method

.method public setLatLong([D)V
    .locals 0
    .param p1, "latLong"    # [D

    .prologue
    .line 148
    return-void
.end method

.method public setParentSetPath(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mParentSetPath:Lcom/sec/android/gallery3d/data/Path;

    .line 382
    return-void
.end method

.method public setPendingAttribute(JZ)V
    .locals 5
    .param p1, "option"    # J
    .param p3, "set"    # Z

    .prologue
    .line 314
    if-eqz p3, :cond_0

    .line 315
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttributePending:J

    or-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttributePending:J

    .line 318
    :goto_0
    return-void

    .line 317
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttributePending:J

    const-wide/16 v2, -0x1

    xor-long/2addr v2, p1

    and-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaItem;->mAttributePending:J

    goto :goto_0
.end method

.method public setSceretBoxItem(Z)V
    .locals 0
    .param p1, "isSceret"    # Z

    .prologue
    .line 402
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/MediaItem;->isSceretBoxItem:Z

    .line 403
    return-void
.end method

.method public updateFaces(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "oldName"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    .line 259
    return-void
.end method
