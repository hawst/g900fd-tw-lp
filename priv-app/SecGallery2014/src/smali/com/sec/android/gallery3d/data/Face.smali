.class public Lcom/sec/android/gallery3d/data/Face;
.super Ljava/lang/Object;
.source "Face.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/gallery3d/data/Face;",
        ">;"
    }
.end annotation


# instance fields
.field private mAlbumKey:Ljava/lang/String;

.field private mAutoGroup:I

.field private mFaceData:I

.field private mFaceId:I

.field private mFacePath:Ljava/lang/String;

.field private mFaceRect:Landroid/graphics/Rect;

.field private mGroupId:I

.field private mName:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;

.field private mPosition:Landroid/graphics/Rect;

.field private mRecommendedId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;IIILandroid/graphics/Rect;I)V
    .locals 2
    .param p1, "faceId"    # I
    .param p2, "personId"    # Ljava/lang/String;
    .param p3, "recommendedId"    # I
    .param p4, "groupId"    # I
    .param p5, "faceData"    # I
    .param p6, "rect"    # Landroid/graphics/Rect;
    .param p7, "autoGroup"    # I

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput p1, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceId:I

    .line 117
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    .line 118
    iput p3, p0, Lcom/sec/android/gallery3d/data/Face;->mRecommendedId:I

    .line 120
    iput p4, p0, Lcom/sec/android/gallery3d/data/Face;->mGroupId:I

    .line 121
    iput p5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceData:I

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData$BaseColumns;->FaceDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceData:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mFacePath:Ljava/lang/String;

    .line 123
    iput-object p6, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    .line 125
    iput p7, p0, Lcom/sec/android/gallery3d/data/Face;->mAutoGroup:I

    .line 126
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;IIILandroid/graphics/Rect;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "faceId"    # I
    .param p3, "personId"    # Ljava/lang/String;
    .param p4, "recommendedId"    # I
    .param p5, "groupId"    # I
    .param p6, "faceData"    # I
    .param p7, "rect"    # Landroid/graphics/Rect;
    .param p8, "autoGroup"    # I

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    .line 102
    iput p2, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceId:I

    .line 103
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    .line 104
    iput p4, p0, Lcom/sec/android/gallery3d/data/Face;->mRecommendedId:I

    .line 106
    iput p5, p0, Lcom/sec/android/gallery3d/data/Face;->mGroupId:I

    .line 107
    iput p6, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceData:I

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData$BaseColumns;->FaceDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceData:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mFacePath:Ljava/lang/String;

    .line 109
    iput-object p7, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    .line 110
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    .line 111
    iput p8, p0, Lcom/sec/android/gallery3d/data/Face;->mAutoGroup:I

    .line 112
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "personId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    .line 84
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "personId"    # Ljava/lang/String;
    .param p3, "rect"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    .line 46
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 47
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, "tokenizer":Ljava/util/StringTokenizer;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    .line 49
    :goto_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 51
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 52
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 53
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 46
    .end local v0    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 55
    .restart local v0    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_1
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/gallery3d/data/Face;)I
    .locals 2
    .param p1, "another"    # Lcom/sec/android/gallery3d/data/Face;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lcom/sec/android/gallery3d/data/Face;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/Face;->compareTo(Lcom/sec/android/gallery3d/data/Face;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 67
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/Face;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 68
    check-cast v0, Lcom/sec/android/gallery3d/data/Face;

    .line 69
    .local v0, "face":Lcom/sec/android/gallery3d/data/Face;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 71
    .end local v0    # "face":Lcom/sec/android/gallery3d/data/Face;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAlbumKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mAlbumKey:Ljava/lang/String;

    return-object v0
.end method

.method public getAutoGroup()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/sec/android/gallery3d/data/Face;->mAutoGroup:I

    return v0
.end method

.method public getFaceData()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceData:I

    return v0
.end method

.method public getFaceId()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceId:I

    return v0
.end method

.method public getFacePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mFacePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFaceRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/sec/android/gallery3d/data/Face;->mGroupId:I

    return v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getRecommendedId()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/gallery3d/data/Face;->mRecommendedId:I

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/Face;->mPosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public rotate(III)Landroid/graphics/Rect;
    .locals 6
    .param p1, "degrees"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 180
    if-gez p1, :cond_0

    .line 181
    add-int/lit16 p1, p1, 0x168

    .line 183
    :cond_0
    const/4 v3, 0x0

    .line 184
    .local v3, "rt":Landroid/graphics/Rect;
    const/16 v5, 0x5a

    if-ne p1, v5, :cond_2

    .line 185
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v1, p3, v5

    .line 186
    .local v1, "left":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v4, v5, Landroid/graphics/Rect;->left:I

    .line 187
    .local v4, "top":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int v2, p3, v5

    .line 188
    .local v2, "right":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v0, v5, Landroid/graphics/Rect;->right:I

    .line 190
    .local v0, "bottom":I
    new-instance v3, Landroid/graphics/Rect;

    .end local v3    # "rt":Landroid/graphics/Rect;
    invoke-direct {v3, v1, v4, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 201
    .end local v0    # "bottom":I
    .end local v1    # "left":I
    .end local v2    # "right":I
    .end local v4    # "top":I
    .restart local v3    # "rt":Landroid/graphics/Rect;
    :cond_1
    :goto_0
    return-object v3

    .line 192
    :cond_2
    const/16 v5, 0x10e

    if-ne p1, v5, :cond_1

    .line 193
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v1, v5, Landroid/graphics/Rect;->top:I

    .line 194
    .restart local v1    # "left":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int v4, p2, v5

    .line 195
    .restart local v4    # "top":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v2, v5, Landroid/graphics/Rect;->bottom:I

    .line 196
    .restart local v2    # "right":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/Face;->mFaceRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int v0, p2, v5

    .line 198
    .restart local v0    # "bottom":I
    new-instance v3, Landroid/graphics/Rect;

    .end local v3    # "rt":Landroid/graphics/Rect;
    invoke-direct {v3, v1, v4, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v3    # "rt":Landroid/graphics/Rect;
    goto :goto_0
.end method

.method public setAlbumKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumKey"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Face;->mAlbumKey:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Face;->mName:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setPersonId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/Face;->mPersonId:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public setRecommendedId(I)V
    .locals 0
    .param p1, "recommendedId"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/android/gallery3d/data/Face;->mRecommendedId:I

    .line 138
    return-void
.end method
