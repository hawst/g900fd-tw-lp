.class public final enum Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;
.super Ljava/lang/Enum;
.source "MotionDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/MotionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MotionStateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

.field public static final enum STATE_MOVE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

.field public static final enum STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

.field public static final enum STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    const-string v1, "STATE_NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    new-instance v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    const-string v1, "STATE_MOVE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_MOVE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    new-instance v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    const-string v1, "STATE_TILT"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_MOVE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->$VALUES:[Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->$VALUES:[Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    return-object v0
.end method
