.class public final Lcom/sec/android/gallery3d/R$menu;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "menu"
.end annotation


# static fields
.field public static final account_setting_menu:I = 0x7f120000

.field public static final album:I = 0x7f120001

.field public static final albumset:I = 0x7f120002

.field public static final contextual_tag_setting_menu:I = 0x7f120003

.field public static final crop:I = 0x7f120004

.field public static final gallery:I = 0x7f120005

.field public static final groupby:I = 0x7f120006

.field public static final imagenote_menu:I = 0x7f120007

.field public static final ingest_menu_item_list_selection:I = 0x7f120008

.field public static final menu_album_edit_view_camera:I = 0x7f120009

.field public static final menu_album_edit_view_phone:I = 0x7f12000a

.field public static final menu_album_edit_view_tablet:I = 0x7f12000b

.field public static final menu_album_pick_view:I = 0x7f12000c

.field public static final menu_album_view_camera:I = 0x7f12000d

.field public static final menu_album_view_easymode_phone:I = 0x7f12000e

.field public static final menu_album_view_phone:I = 0x7f12000f

.field public static final menu_album_view_tablet:I = 0x7f120010

.field public static final menu_category_view:I = 0x7f120011

.field public static final menu_category_view_edit:I = 0x7f120012

.field public static final menu_detail_single_view:I = 0x7f120013

.field public static final menu_detail_view_camera:I = 0x7f120014

.field public static final menu_detail_view_camera_quickview_on_lockscreen:I = 0x7f120015

.field public static final menu_detail_view_easymode_phone:I = 0x7f120016

.field public static final menu_detail_view_expansion:I = 0x7f120017

.field public static final menu_detail_view_phone:I = 0x7f120018

.field public static final menu_detail_view_phone_wqhd:I = 0x7f120019

.field public static final menu_detail_view_preview:I = 0x7f12001a

.field public static final menu_detail_view_tablet:I = 0x7f12001b

.field public static final menu_done_and_cancel:I = 0x7f12001c

.field public static final menu_done_and_cancel_search_actionbar:I = 0x7f12001d

.field public static final menu_done_search_actionbar:I = 0x7f12001e

.field public static final menu_download_cloud_view:I = 0x7f12001f

.field public static final menu_emergency_view:I = 0x7f120020

.field public static final menu_event_edit_view:I = 0x7f120021

.field public static final menu_event_view:I = 0x7f120022

.field public static final menu_festival_view:I = 0x7f120023

.field public static final menu_help_view:I = 0x7f120024

.field public static final menu_hidden_album_edit_view:I = 0x7f120025

.field public static final menu_hidden_album_normal_view:I = 0x7f120026

.field public static final menu_items_edit_view:I = 0x7f120027

.field public static final menu_map_view:I = 0x7f120028

.field public static final menu_no_items_view:I = 0x7f120029

.field public static final menu_no_items_view_single_album:I = 0x7f12002a

.field public static final menu_no_items_view_tablet:I = 0x7f12002b

.field public static final menu_people_edit_view:I = 0x7f12002c

.field public static final menu_photo_edit_view_camera:I = 0x7f12002d

.field public static final menu_photo_edit_view_phone:I = 0x7f12002e

.field public static final menu_photo_edit_view_tablet:I = 0x7f12002f

.field public static final menu_photo_multipick_view:I = 0x7f120030

.field public static final menu_photo_pick_view:I = 0x7f120031

.field public static final menu_photo_view_camera:I = 0x7f120032

.field public static final menu_photo_view_easymode_phone:I = 0x7f120033

.field public static final menu_photo_view_phone:I = 0x7f120034

.field public static final menu_photo_view_tablet:I = 0x7f120035

.field public static final menu_search_edit_view_phone:I = 0x7f120036

.field public static final menu_search_view:I = 0x7f120037

.field public static final menu_slideshow_start_action:I = 0x7f120038

.field public static final menu_slink_album_view_phone:I = 0x7f120039

.field public static final menu_slink_items_edit_view:I = 0x7f12003a

.field public static final menu_time_edit_view_phone:I = 0x7f12003b

.field public static final menu_time_edit_view_tablet:I = 0x7f12003c

.field public static final menu_timeall_edit_view:I = 0x7f12003d

.field public static final menu_timeall_normal_view:I = 0x7f12003e

.field public static final moreinfo_menu_edit:I = 0x7f12003f

.field public static final moreinfo_menu_normal:I = 0x7f120040

.field public static final moreinfo_menu_usertag_edit:I = 0x7f120041

.field public static final motion_preview:I = 0x7f120042

.field public static final motion_tutorial:I = 0x7f120043

.field public static final movie:I = 0x7f120044

.field public static final new_album_done:I = 0x7f120045

.field public static final operation:I = 0x7f120046

.field public static final photo:I = 0x7f120047

.field public static final pickup:I = 0x7f120048

.field public static final selection:I = 0x7f120049

.field public static final sendoption:I = 0x7f12004a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
