.class public Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
.super Ljava/lang/Object;
.source "CameraViewFinderHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;,
        Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;
    }
.end annotation


# static fields
.field private static final MODULE_OFF_MASTER:Ljava/lang/String; = "module_off_master"

.field private static final MODULE_ON_MASTER:Ljava/lang/String; = "module_on_master"

.field private static final MSG_HIDE_EVF:I = 0x65

.field private static final MSG_SET_EVF_STATUS:I = 0x67

.field private static final MSG_START_EVF:I = 0x64

.field private static final MSG_UPDATE_PRESENTATION:I = 0x66

.field private static final TAG:Ljava/lang/String;

.field private static final TSP_CMD_PATH:Ljava/lang/String; = "/sys/class/sec/tsp/cmd"

.field private static final TSP_RESULT_PATH:Ljava/lang/String; = "/sys/class/sec/tsp/cmd_result"

.field private static final TSP_STATUS_PATH:Ljava/lang/String; = "/sys/class/sec/tsp/cmd_status"

.field private static final TVOUT:Ljava/lang/String; = "tvoutservice"

.field private static sActiveInstance:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mIsEVFOn:Z

.field private final mMainHandler:Landroid/os/Handler;

.field private final mMediaRouter:Landroid/media/MediaRouter;

.field private final mMediaRouterCallback:Landroid/media/MediaRouter$SimpleCallback;

.field private final mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

.field private final mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private final mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

.field private mProximityListener:Landroid/hardware/SensorEventListener;

.field private final mProximitySensor:Landroid/hardware/Sensor;

.field private final mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private final mWindowManager:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p3, "model"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mIsEVFOn:Z

    .line 235
    new-instance v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$2;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mProximityListener:Landroid/hardware/SensorEventListener;

    .line 312
    new-instance v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$3;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMediaRouterCallback:Landroid/media/MediaRouter$SimpleCallback;

    .line 370
    new-instance v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$5;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$5;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 88
    sput-object p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->sActiveInstance:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .line 89
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mContext:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 91
    iput-object p3, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-object v0, p1

    .line 92
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 94
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mWindowManager:Landroid/view/IWindowManager;

    .line 95
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mSensorManager:Landroid/hardware/SensorManager;

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mProximitySensor:Landroid/hardware/Sensor;

    .line 97
    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMediaRouter:Landroid/media/MediaRouter;

    .line 99
    new-instance v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->updatePresentation()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->stopEVFInternal()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->setTvoutEVFStatus(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->isHDMIConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;)Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method public static getActiveInstance()Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->sActiveInstance:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    return-object v0
.end method

.method private isHDMIConnected()Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method private isSystemKeyEventRequested(I)Z
    .locals 3
    .param p1, "keyCode"    # I

    .prologue
    .line 217
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mWindowManager:Landroid/view/IWindowManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-interface {v2, p1, v1}, Landroid/view/IWindowManager;->isSystemKeyEventRequested(ILandroid/content/ComponentName;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 222
    :goto_0
    return v1

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 222
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static read(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 272
    const/4 v5, 0x0

    .line 273
    .local v5, "value":Ljava/lang/String;
    const/4 v3, 0x0

    .line 274
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 276
    .local v1, "freader":Ljava/io/FileReader;
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    .end local v1    # "freader":Ljava/io/FileReader;
    .local v2, "freader":Ljava/io/FileReader;
    if-eqz v2, :cond_0

    .line 278
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 280
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_0
    if-eqz v3, :cond_1

    .line 281
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 282
    if-eqz v5, :cond_1

    .line 283
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 290
    :cond_1
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 292
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    :goto_0
    return-object v5

    .line 286
    :catch_0
    move-exception v0

    .line 287
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v6, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to read: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 290
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_2
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_2

    .line 286
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_1
.end method

.method private requestSystemKeyEvent(IZ)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "request"    # Z

    .prologue
    .line 227
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mWindowManager:Landroid/view/IWindowManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-interface {v2, p1, v1, p2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 232
    :goto_0
    return v1

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 232
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setTouchDisable(Z)V
    .locals 2
    .param p1, "disable"    # Z

    .prologue
    .line 197
    if-eqz p1, :cond_0

    .line 198
    const-string v0, "/sys/class/sec/tsp/cmd"

    const-string v1, "module_off_master"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 202
    :goto_0
    const-string v0, "/sys/class/sec/tsp/cmd_status"

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->read(Ljava/lang/String;)Ljava/lang/String;

    .line 203
    const-string v0, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->read(Ljava/lang/String;)Ljava/lang/String;

    .line 204
    return-void

    .line 200
    :cond_0
    const-string v0, "/sys/class/sec/tsp/cmd"

    const-string v1, "module_on_master"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private setTvoutEVFStatus(Z)V
    .locals 0
    .param p1, "bStatus"    # Z

    .prologue
    .line 194
    return-void
.end method

.method private stopEVFInternal()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x1a

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 160
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mIsEVFOn:Z

    if-nez v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 163
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mIsEVFOn:Z

    .line 164
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->dismiss()V

    .line 168
    iput-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    .line 171
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->setTvoutEVFStatus(Z)V

    .line 172
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->registerEVF(Z)V

    .line 173
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->isSystemKeyEventRequested(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    invoke-direct {p0, v3, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->requestSystemKeyEvent(IZ)Z

    .line 175
    :cond_2
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->isSystemKeyEventRequested(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 176
    invoke-direct {p0, v2, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->requestSystemKeyEvent(IZ)Z

    .line 177
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->setTouchDisable(Z)V

    goto :goto_0
.end method

.method private updatePresentation()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 333
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMediaRouter:Landroid/media/MediaRouter;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v3

    .line 334
    .local v3, "route":Landroid/media/MediaRouter$RouteInfo;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v2

    .line 337
    .local v2, "presentationDisplay":Landroid/view/Display;
    :goto_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->getDisplay()Landroid/view/Display;

    move-result-object v1

    .line 338
    .local v1, "oldDisPlay":Landroid/view/Display;
    :goto_1
    if-eqz v1, :cond_3

    if-eq v1, v2, :cond_3

    .line 339
    sget-object v4, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "old display: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " current display: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " display is changed."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;

    const/16 v5, 0x65

    const-wide/16 v6, 0x32

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 365
    :cond_0
    :goto_2
    return-void

    .end local v1    # "oldDisPlay":Landroid/view/Display;
    .end local v2    # "presentationDisplay":Landroid/view/Display;
    :cond_1
    move-object v2, v4

    .line 334
    goto :goto_0

    .restart local v2    # "presentationDisplay":Landroid/view/Display;
    :cond_2
    move-object v1, v4

    .line 337
    goto :goto_1

    .line 346
    .restart local v1    # "oldDisPlay":Landroid/view/Display;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    if-nez v5, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/Display;->isValid()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 347
    sget-object v5, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Showing presentation on display: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    new-instance v5, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mContext:Landroid/content/Context;

    invoke-direct {v5, p0, v6, v2}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Landroid/content/Context;Landroid/view/Display;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    .line 349
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 350
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    new-instance v6, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$4;

    invoke-direct {v6, p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$4;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 359
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$InvalidDisplayException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "ex":Landroid/view/WindowManager$InvalidDisplayException;
    sget-object v5, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;

    const-string v6, "Couldn\'t show presentation!  Display was removed in the meantime."

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    iput-object v4, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    goto :goto_2
.end method

.method private static write(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 296
    const/4 v1, 0x1

    .line 297
    .local v1, "res":Z
    const/4 v2, 0x0

    .line 299
    .local v2, "writer":Ljava/io/FileWriter;
    :try_start_0
    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    .end local v2    # "writer":Ljava/io/FileWriter;
    .local v3, "writer":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 301
    invoke-virtual {v3}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 307
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v2, v3

    .line 309
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    :goto_0
    return v1

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v4, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to write: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 305
    const/4 v1, 0x0

    .line 307
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :goto_2
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v4

    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_2

    .line 302
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_1
.end method


# virtual methods
.method public isEVFOn()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mIsEVFOn:Z

    return v0
.end method

.method public onDestory()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->sActiveInstance:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .line 139
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mProximityListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMediaRouter:Landroid/media/MediaRouter;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMediaRouterCallback:Landroid/media/MediaRouter$SimpleCallback;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->stopEVF()V

    .line 135
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mProximityListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mProximitySensor:Landroid/hardware/Sensor;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 128
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMediaRouter:Landroid/media/MediaRouter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMediaRouterCallback:Landroid/media/MediaRouter$SimpleCallback;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    .line 129
    return-void
.end method

.method public registerEVF(Z)V
    .locals 0
    .param p1, "register"    # Z

    .prologue
    .line 186
    return-void
.end method

.method public startEVF()V
    .locals 4

    .prologue
    const/16 v3, 0x1a

    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 142
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mIsEVFOn:Z

    if-eqz v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 145
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mIsEVFOn:Z

    .line 146
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->isSystemKeyEventRequested(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    invoke-direct {p0, v3, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->requestSystemKeyEvent(IZ)Z

    .line 148
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->isSystemKeyEventRequested(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 149
    invoke-direct {p0, v2, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->requestSystemKeyEvent(IZ)Z

    .line 150
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->setTouchDisable(Z)V

    .line 151
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->registerEVF(Z)V

    goto :goto_0
.end method

.method public stopEVF()V
    .locals 2

    .prologue
    const/16 v1, 0x65

    .line 155
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 157
    return-void
.end method
