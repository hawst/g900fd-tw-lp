.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
.super Ljava/lang/Object;
.source "TCloudRefer.java"


# static fields
.field public static final STATUS_CACHED:I = 0x1

.field public static final STATUS_NOT_CACHED:I = 0x0

.field public static final STATUS_NOT_THUMB:I = 0x0

.field public static final STATUS_THUMB:I = 0x1

.field protected static final TAG:Ljava/lang/String;

.field public static final TCLOUD_ACCOUNT_NAME:Ljava/lang/String; = "com.skp.tcloud"

.field public static final TCLOUD_MEDIA_TYPE_IMAGE:I = 0x1

.field public static final TCLOUD_MEDIA_TYPE_UNKNOWN:I = -0x1

.field public static final TCLOUD_MEDIA_TYPE_VIDEO:I = 0x3


# instance fields
.field public AUTHORITY:Ljava/lang/String;

.field public Files_BUCKET_DISPLAY_NAME:Ljava/lang/String;

.field public Files_BUCKET_ID:Ljava/lang/String;

.field public Files_MEDIA_TYPE:Ljava/lang/String;

.field public Images_BUCKET_DISPLAY_NAME:Ljava/lang/String;

.field public Images_BUCKET_ID:Ljava/lang/String;

.field public Images_CLOUD_CACHED_PATH:Ljava/lang/String;

.field public Images_CLOUD_IS_AVAILABLE_THUMB:Ljava/lang/String;

.field public Images_CLOUD_IS_CACHED:Ljava/lang/String;

.field public Images_CLOUD_THUMB_PATH:Ljava/lang/String;

.field public Images_DATA:Ljava/lang/String;

.field public Images_DATE_MODIFIED:Ljava/lang/String;

.field public Images_DISPLAY_NAME:Ljava/lang/String;

.field public Images_ID:Ljava/lang/String;

.field public Images_MIME_TYPE:Ljava/lang/String;

.field public Images_ORIENTATION:Ljava/lang/String;

.field public Images_SIZE:Ljava/lang/String;

.field public PROJECTION_BUCKET:[Ljava/lang/String;

.field public TCLOUD_BUCKETS_URI:Landroid/net/Uri;

.field public Videos_BUCKET_DISPLAY_NAME:Ljava/lang/String;

.field public Videos_BUCKET_ID:Ljava/lang/String;

.field public Videos_CLOUD_CACHED_PATH:Ljava/lang/String;

.field public Videos_CLOUD_IS_AVAILABLE_THUMB:Ljava/lang/String;

.field public Videos_CLOUD_IS_CACHED:Ljava/lang/String;

.field public Videos_CLOUD_THUMB_PATH:Ljava/lang/String;

.field public Videos_DATA:Ljava/lang/String;

.field public Videos_DATE_MODIFIED:Ljava/lang/String;

.field public Videos_DISPLAY_NAME:Ljava/lang/String;

.field public Videos_ID:Ljava/lang/String;

.field public Videos_MIME_TYPE:Ljava/lang/String;

.field public Videos_SIZE:Ljava/lang/String;

.field public mBaseFileUri:Landroid/net/Uri;

.field public mBaseImageUri:Landroid/net/Uri;

.field public mBaseVideoUri:Landroid/net/Uri;

.field public mInit:Z

.field private mTcloudStore:Ljava/lang/Object;

.field private mTcloudStoreAPI:Ljava/lang/Object;

.field private mTcloudStoreFiles:Ljava/lang/Object;

.field private mTcloudStoreImages:Ljava/lang/Object;

.field private mTcloudStoreVideos:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStore:Ljava/lang/Object;

    .line 46
    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    .line 47
    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    .line 48
    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    .line 49
    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    .line 95
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z

    .line 99
    :try_start_0
    const-string v6, "com.skp.tcloud.service.lib.TcloudStore"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 100
    .local v1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStore:Ljava/lang/Object;

    .line 101
    const-string v6, "com.skp.tcloud.service.lib.TcloudStore$API"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 102
    .local v4, "klassInner":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    .line 103
    const-string v6, "com.skp.tcloud.service.lib.TcloudStore$Images"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 104
    .local v3, "klassImages":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    .line 105
    const-string v6, "com.skp.tcloud.service.lib.TcloudStore$Videos"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 106
    .local v5, "klassVideos":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    .line 107
    const-string v6, "com.skp.tcloud.service.lib.TcloudStore$Files"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 108
    .local v2, "klassFiles":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    .line 109
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->init()V

    .line 110
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    .line 124
    .end local v1    # "klass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "klassFiles":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "klassImages":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "klassInner":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v5    # "klassVideos":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 113
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z

    goto :goto_0

    .line 114
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    .line 116
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z

    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 119
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z

    goto :goto_0

    .line 120
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 122
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mInit:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    return-object v0
.end method

.method public static existsAccount(Landroid/content/Context;)Z
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 510
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 511
    .local v1, "am":Landroid/accounts/AccountManager;
    const-string v2, "com.skp.tcloud"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 512
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 513
    const/4 v2, 0x1

    .line 515
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 10

    .prologue
    .line 128
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStore:Ljava/lang/Object;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    if-eqz v8, :cond_0

    .line 130
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    .line 131
    .local v5, "object":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStore:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "AUTHORITY"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 132
    .local v1, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->AUTHORITY:Ljava/lang/String;

    .line 133
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStore:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "PROJECTION_BUCKET"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 134
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    check-cast v8, [Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->PROJECTION_BUCKET:[Ljava/lang/String;

    .line 135
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStore:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "TCLOUD_BUCKETS_URI"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 136
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->TCLOUD_BUCKETS_URI:Landroid/net/Uri;

    .line 138
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "BUCKET_ID"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 139
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_ID:Ljava/lang/String;

    .line 140
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "BUCKET_DISPLAY_NAME"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 141
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_BUCKET_DISPLAY_NAME:Ljava/lang/String;

    .line 142
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "MIME_TYPE"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 143
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Files_MEDIA_TYPE:Ljava/lang/String;

    .line 145
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "_ID"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 146
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_ID:Ljava/lang/String;

    .line 147
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "_DATA"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 148
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DATA:Ljava/lang/String;

    .line 149
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "SIZE"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 150
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_SIZE:Ljava/lang/String;

    .line 151
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "DISPLAY_NAME"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 152
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DISPLAY_NAME:Ljava/lang/String;

    .line 153
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "MIME_TYPE"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 154
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_MIME_TYPE:Ljava/lang/String;

    .line 155
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "DATE_MODIFIED"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 156
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DATE_MODIFIED:Ljava/lang/String;

    .line 157
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "BUCKET_ID"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 158
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_BUCKET_ID:Ljava/lang/String;

    .line 159
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "BUCKET_DISPLAY_NAME"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 160
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_BUCKET_DISPLAY_NAME:Ljava/lang/String;

    .line 161
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_CACHED_PATH"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 162
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_CACHED_PATH:Ljava/lang/String;

    .line 163
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_THUMB_PATH"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 164
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_THUMB_PATH:Ljava/lang/String;

    .line 165
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "ORIENTATION"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 166
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_ORIENTATION:Ljava/lang/String;

    .line 167
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_IS_CACHED"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 168
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_IS_CACHED:Ljava/lang/String;

    .line 169
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_IS_AVAILABLE_THUMB"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 170
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_IS_AVAILABLE_THUMB:Ljava/lang/String;

    .line 172
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "_ID"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 173
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_ID:Ljava/lang/String;

    .line 174
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "_DATA"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 175
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DATA:Ljava/lang/String;

    .line 176
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "SIZE"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 177
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_SIZE:Ljava/lang/String;

    .line 178
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "DISPLAY_NAME"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 179
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DISPLAY_NAME:Ljava/lang/String;

    .line 180
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "MIME_TYPE"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 181
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_MIME_TYPE:Ljava/lang/String;

    .line 182
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "DATE_MODIFIED"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 183
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DATE_MODIFIED:Ljava/lang/String;

    .line 184
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "BUCKET_ID"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 185
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_BUCKET_ID:Ljava/lang/String;

    .line 186
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "BUCKET_DISPLAY_NAME"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 187
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_BUCKET_DISPLAY_NAME:Ljava/lang/String;

    .line 188
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_CACHED_PATH"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 189
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_CACHED_PATH:Ljava/lang/String;

    .line 190
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_IS_CACHED"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 191
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_IS_CACHED:Ljava/lang/String;

    .line 192
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_THUMB_PATH"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 193
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_THUMB_PATH:Ljava/lang/String;

    .line 194
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "CLOUD_IS_AVAILABLE_THUMB"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 195
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_IS_AVAILABLE_THUMB:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7

    .line 198
    const/4 v2, 0x0

    .line 199
    .local v2, "getContentUriFiles":Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    .line 200
    .local v3, "getContentUriImages":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    .line 202
    .local v4, "getContentUriVideos":Ljava/lang/reflect/Method;
    const/4 v8, 0x0

    :try_start_1
    new-array v7, v8, [Ljava/lang/Class;

    .line 203
    .local v7, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getContentUri"

    invoke-virtual {v8, v9, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 204
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getContentUri"

    invoke-virtual {v8, v9, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 205
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getContentUri"

    invoke-virtual {v8, v9, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7

    move-result-object v4

    .line 210
    .end local v7    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v8, 0x0

    :try_start_2
    new-array v6, v8, [Ljava/lang/Object;

    .line 211
    .local v6, "paramTypes":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreFiles:Ljava/lang/Object;

    invoke-virtual {v2, v8, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseFileUri:Landroid/net/Uri;

    .line 213
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v3, v8, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    .line 215
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v4, v8, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseVideoUri:Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7

    .line 234
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .end local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .end local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .end local v5    # "object":Ljava/lang/String;
    .end local v6    # "paramTypes":[Ljava/lang/Object;
    :cond_0
    :goto_1
    return-void

    .line 206
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .restart local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .restart local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .restart local v5    # "object":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7

    goto :goto_0

    .line 225
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .end local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .end local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .end local v5    # "object":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_1

    .line 217
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .restart local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .restart local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .restart local v5    # "object":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7

    goto :goto_1

    .line 227
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .end local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .end local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .end local v5    # "object":Ljava/lang/String;
    :catch_3
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 219
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .restart local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .restart local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .restart local v5    # "object":Ljava/lang/String;
    :catch_4
    move-exception v0

    .line 220
    .restart local v0    # "e":Ljava/lang/IllegalAccessException;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    goto :goto_1

    .line 229
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .end local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .end local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .end local v5    # "object":Ljava/lang/String;
    :catch_5
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 221
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .restart local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .restart local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .restart local v5    # "object":Ljava/lang/String;
    :catch_6
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7

    goto :goto_1

    .line 231
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "getContentUriFiles":Ljava/lang/reflect/Method;
    .end local v3    # "getContentUriImages":Ljava/lang/reflect/Method;
    .end local v4    # "getContentUriVideos":Ljava/lang/reflect/Method;
    .end local v5    # "object":Ljava/lang/String;
    :catch_7
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static isTCloudAgentExist(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 329
    const/4 v2, 0x0

    .line 330
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 332
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.skp.tcloud.agent"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 336
    :goto_0
    if-eqz v1, :cond_0

    .line 337
    const/4 v2, 0x1

    .line 338
    :cond_0
    return v2

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public download(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 483
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v4, :cond_0

    .line 484
    const/4 v0, 0x0

    .line 486
    .local v0, "download":Ljava/lang/reflect/Method;
    const/4 v4, 0x2

    :try_start_1
    new-array v3, v4, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/net/Uri;

    aput-object v5, v3, v4

    .line 487
    .local v3, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "getStreamingURL"

    invoke-virtual {v4, v5, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 493
    .end local v3    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v4, 0x2

    :try_start_2
    new-array v2, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    const/4 v4, 0x1

    aput-object p2, v2, v4

    .line 494
    .local v2, "paramTypes":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    .end local v2    # "paramTypes":[Ljava/lang/Object;
    invoke-virtual {v0, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 507
    .end local v0    # "download":Ljava/lang/reflect/Method;
    :cond_0
    :goto_1
    return-void

    .line 488
    .restart local v0    # "download":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 489
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 504
    .end local v0    # "download":Ljava/lang/reflect/Method;
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 505
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 496
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "download":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v1

    .line 497
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 498
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 499
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 500
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v1

    .line 501
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1
.end method

.method public getImage(Landroid/content/Context;J)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 413
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v8, :cond_0

    .line 414
    const/4 v2, 0x0

    .line 415
    .local v2, "getThumbnailUri":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    .line 417
    .local v1, "getImage":Ljava/lang/reflect/Method;
    const/4 v8, 0x1

    :try_start_1
    new-array v4, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v9, v4, v8

    .line 418
    .local v4, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v8, 0x2

    new-array v6, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-class v9, Landroid/net/Uri;

    aput-object v9, v6, v8

    .line 419
    .local v6, "paramTypes2":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getThumbnailUri"

    invoke-virtual {v8, v9, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 420
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getImage"

    invoke-virtual {v8, v9, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 426
    .end local v4    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v6    # "paramTypes2":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v8, 0x1

    :try_start_2
    new-array v3, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v3, v8

    .line 427
    .local v3, "paramTypes":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    .end local v3    # "paramTypes":[Ljava/lang/Object;
    invoke-virtual {v2, v8, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 429
    .local v7, "uri":Landroid/net/Uri;
    const/4 v8, 0x2

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v5, v8

    const/4 v8, 0x1

    aput-object v7, v5, v8

    .line 430
    .local v5, "paramTypes2":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    .end local v5    # "paramTypes2":[Ljava/lang/Object;
    invoke-virtual {v1, v8, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 443
    .end local v1    # "getImage":Ljava/lang/reflect/Method;
    .end local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    .end local v7    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v8

    .line 421
    .restart local v1    # "getImage":Ljava/lang/reflect/Method;
    .restart local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 440
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v1    # "getImage":Ljava/lang/reflect/Method;
    .end local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 441
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 443
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_2
    const/4 v8, 0x0

    goto :goto_1

    .line 432
    .restart local v1    # "getImage":Ljava/lang/reflect/Method;
    .restart local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v0

    .line 433
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 434
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 435
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 436
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 437
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2
.end method

.method public getTCloudImageProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 265
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DATA:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_SIZE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DISPLAY_NAME:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_MIME_TYPE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_DATE_MODIFIED:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_BUCKET_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_BUCKET_DISPLAY_NAME:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_CACHED_PATH:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_THUMB_PATH:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_ORIENTATION:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_IS_CACHED:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Images_CLOUD_IS_AVAILABLE_THUMB:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getTCloudVideoProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 283
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DATA:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_SIZE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DISPLAY_NAME:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_MIME_TYPE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_DATE_MODIFIED:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_BUCKET_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_BUCKET_DISPLAY_NAME:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_CACHED_PATH:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_IS_CACHED:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_THUMB_PATH:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_CLOUD_IS_AVAILABLE_THUMB:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getThumbnail(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 378
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v8, :cond_0

    .line 379
    const/4 v2, 0x0

    .line 380
    .local v2, "getThumbnailUri":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    .line 382
    .local v1, "getThumbnail":Ljava/lang/reflect/Method;
    const/4 v8, 0x1

    :try_start_1
    new-array v4, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v9, v4, v8

    .line 383
    .local v4, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v8, 0x2

    new-array v6, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-class v9, Landroid/net/Uri;

    aput-object v9, v6, v8

    .line 384
    .local v6, "paramTypes2":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getThumbnailUri"

    invoke-virtual {v8, v9, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 385
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getThumbnail"

    invoke-virtual {v8, v9, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 391
    .end local v4    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v6    # "paramTypes2":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v8, 0x1

    :try_start_2
    new-array v3, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v3, v8

    .line 392
    .local v3, "paramTypes":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreImages:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    .end local v3    # "paramTypes":[Ljava/lang/Object;
    invoke-virtual {v2, v8, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 394
    .local v7, "uri":Landroid/net/Uri;
    const/4 v8, 0x2

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v5, v8

    const/4 v8, 0x1

    aput-object v7, v5, v8

    .line 395
    .local v5, "paramTypes2":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    .end local v5    # "paramTypes2":[Ljava/lang/Object;
    invoke-virtual {v1, v8, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 408
    .end local v1    # "getThumbnail":Ljava/lang/reflect/Method;
    .end local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    .end local v7    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v8

    .line 386
    .restart local v1    # "getThumbnail":Ljava/lang/reflect/Method;
    .restart local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 405
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v1    # "getThumbnail":Ljava/lang/reflect/Method;
    .end local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 408
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_2
    const/4 v8, 0x0

    goto :goto_1

    .line 397
    .restart local v1    # "getThumbnail":Ljava/lang/reflect/Method;
    .restart local v2    # "getThumbnailUri":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v0

    .line 398
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 399
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 400
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 401
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 402
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2
.end method

.method public getVideoUri(Landroid/content/Context;J)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # J

    .prologue
    .line 448
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v8, :cond_0

    .line 449
    const/4 v2, 0x0

    .line 450
    .local v2, "getVideoUri":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    .line 452
    .local v1, "getStreamingURL":Ljava/lang/reflect/Method;
    const/4 v8, 0x1

    :try_start_1
    new-array v4, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v9, v4, v8

    .line 453
    .local v4, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v8, 0x2

    new-array v6, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-class v9, Landroid/net/Uri;

    aput-object v9, v6, v8

    .line 454
    .local v6, "paramTypes2":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getVideoUri"

    invoke-virtual {v8, v9, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 455
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "getStreamingURL"

    invoke-virtual {v8, v9, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 461
    .end local v4    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v6    # "paramTypes2":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v8, 0x1

    :try_start_2
    new-array v3, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v3, v8

    .line 462
    .local v3, "paramTypes":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreVideos:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    .end local v3    # "paramTypes":[Ljava/lang/Object;
    invoke-virtual {v2, v8, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 464
    .local v7, "uri":Landroid/net/Uri;
    const/4 v8, 0x2

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v5, v8

    const/4 v8, 0x1

    aput-object v7, v5, v8

    .line 465
    .local v5, "paramTypes2":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/Object;

    .end local v5    # "paramTypes2":[Ljava/lang/Object;
    invoke-virtual {v1, v8, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 478
    .end local v1    # "getStreamingURL":Ljava/lang/reflect/Method;
    .end local v2    # "getVideoUri":Ljava/lang/reflect/Method;
    .end local v7    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v8

    .line 456
    .restart local v1    # "getStreamingURL":Ljava/lang/reflect/Method;
    .restart local v2    # "getVideoUri":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 475
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v1    # "getStreamingURL":Ljava/lang/reflect/Method;
    .end local v2    # "getVideoUri":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 476
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 478
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_2
    const/4 v8, 0x0

    goto :goto_1

    .line 467
    .restart local v1    # "getStreamingURL":Ljava/lang/reflect/Method;
    .restart local v2    # "getVideoUri":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v0

    .line 468
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 469
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 470
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 471
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 472
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2
.end method

.method public initTCloud(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 238
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v4, :cond_0

    .line 239
    const/4 v1, 0x0

    .line 241
    .local v1, "init":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    :try_start_1
    new-array v3, v4, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    .line 242
    .local v3, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "init"

    invoke-virtual {v4, v5, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 248
    .end local v3    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v4, 0x1

    :try_start_2
    new-array v2, v4, [Landroid/content/Context;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    .line 249
    .local v2, "paramTypes":[Landroid/content/Context;
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    .end local v2    # "paramTypes":[Landroid/content/Context;
    invoke-virtual {v1, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 262
    .end local v1    # "init":Ljava/lang/reflect/Method;
    :cond_0
    :goto_1
    return-void

    .line 243
    .restart local v1    # "init":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 259
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v1    # "init":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 260
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 251
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "init":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v0

    .line 252
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 253
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 255
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1
.end method

.method public isTCloudAccoutActivated(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 301
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v4, :cond_0

    .line 302
    const/4 v1, 0x0

    .line 304
    .local v1, "isTcloudAvailable":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    :try_start_1
    new-array v3, v4, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v3, v4

    .line 305
    .local v3, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v6, "isCloudAvailable"

    invoke-virtual {v4, v6, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 311
    .end local v3    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v4, 0x1

    :try_start_2
    new-array v2, v4, [Landroid/content/Context;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    .line 312
    .local v2, "paramTypes":[Landroid/content/Context;
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    .end local v2    # "paramTypes":[Landroid/content/Context;
    invoke-virtual {v1, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v4

    .line 325
    .end local v1    # "isTcloudAvailable":Ljava/lang/reflect/Method;
    :goto_1
    return v4

    .line 306
    .restart local v1    # "isTcloudAvailable":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 322
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v1    # "isTcloudAvailable":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_2
    move v4, v5

    .line 325
    goto :goto_1

    .line 314
    .restart local v1    # "isTcloudAvailable":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v0

    .line 315
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 316
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 317
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 318
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 319
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2
.end method

.method public tCloudAlbumsSync(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 342
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;-><init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;Landroid/content/Context;)V

    .line 373
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 374
    return-void
.end method
