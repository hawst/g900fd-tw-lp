.class Lcom/sec/android/gallery3d/ui/PhotoView$4;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/FaceIndicatorView$UpdateFaceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0

    .prologue
    .line 640
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 644
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getShowBarState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_0

    .line 646
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentItemPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->showFaceIndicatorView()V

    .line 650
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v0, :cond_1

    .line 651
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhotoForCA(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 653
    :cond_1
    return-void
.end method

.method public preUpdate()V
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$4;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideFaceIndicatorView()V

    .line 658
    return-void
.end method
