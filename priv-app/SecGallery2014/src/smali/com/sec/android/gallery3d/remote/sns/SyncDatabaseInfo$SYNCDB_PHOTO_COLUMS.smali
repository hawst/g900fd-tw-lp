.class public interface abstract Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo$SYNCDB_PHOTO_COLUMS;
.super Ljava/lang/Object;
.source "SyncDatabaseInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SYNCDB_PHOTO_COLUMS"
.end annotation


# static fields
.field public static final photo_create_time:Ljava/lang/String; = "created_time"

.field public static final photo_from_id:Ljava/lang/String; = "from_id"

.field public static final photo_from_name:Ljava/lang/String; = "from_name"

.field public static final photo_height:Ljava/lang/String; = "height"

.field public static final photo_icon:Ljava/lang/String; = "icon"

.field public static final photo_id:Ljava/lang/String; = "id"

.field public static final photo_images_height:Ljava/lang/String; = "image_height"

.field public static final photo_images_url:Ljava/lang/String; = "url"

.field public static final photo_images_width:Ljava/lang/String; = "image_width"

.field public static final photo_link:Ljava/lang/String; = "link"

.field public static final photo_name:Ljava/lang/String; = "name"

.field public static final photo_parent_id:Ljava/lang/String; = "target_id"

.field public static final photo_position:Ljava/lang/String; = "position"

.field public static final photo_screennail_url:Ljava/lang/String; = "source"

.field public static final photo_thumbnail_url:Ljava/lang/String; = "picture"

.field public static final photo_updated_time:Ljava/lang/String; = "updated_time"

.field public static final photo_width:Ljava/lang/String; = "width"
