.class public final enum Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
.super Ljava/lang/Enum;
.source "GalleryMotion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/GalleryMotion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MotionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum AIR_MOTION_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum AIR_MOTION_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

.field public static final enum MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 107
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 108
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_TILT_TUTORIAL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 109
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_TILT_PREVIEW"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 110
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_PANNING_TUTORIAL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 111
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_PANNING_PREVIEW"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 112
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "AIR_MOTION_TUTORIAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 113
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_TILT_HELP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 114
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_PANNING_HELP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 115
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "AIR_MOTION_HELP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 116
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_PANNING_PEEK_TUTORIAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 117
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_PANNING_PEEK_PREVIEW"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 118
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    const-string v1, "MOTION_PANNING_PEEK_HELP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 106
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->$VALUES:[Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 106
    const-class v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->$VALUES:[Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    return-object v0
.end method
