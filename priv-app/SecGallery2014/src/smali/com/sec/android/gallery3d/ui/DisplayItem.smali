.class public abstract Lcom/sec/android/gallery3d/ui/DisplayItem;
.super Ljava/lang/Object;
.source "DisplayItem.java"


# static fields
.field public static final RENDER_MORE_FRAME:I = 0x2

.field public static final RENDER_MORE_PASS:I = 0x1


# instance fields
.field protected mBoxHeight:I

.field protected mBoxWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getIdentity()J
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public abstract render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;I)I
.end method

.method public setBox(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/gallery3d/ui/DisplayItem;->mBoxWidth:I

    .line 31
    iput p2, p0, Lcom/sec/android/gallery3d/ui/DisplayItem;->mBoxHeight:I

    .line 32
    return-void
.end method
