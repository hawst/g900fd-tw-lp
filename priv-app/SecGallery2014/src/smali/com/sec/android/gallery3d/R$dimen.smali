.class public final Lcom/sec/android/gallery3d/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final ablum_view_ic_group_height:I = 0x7f0d022c

.field public static final ablum_view_ic_group_width:I = 0x7f0d022b

.field public static final action_bar_arrow_margin_left:I = 0x7f0d03e2

.field public static final action_bar_arrow_margin_right:I = 0x7f0d03e3

.field public static final action_bar_divider_padding:I = 0x7f0d012c

.field public static final action_bar_icon_padding_left:I = 0x7f0d03e4

.field public static final action_bar_icon_padding_right:I = 0x7f0d03e5

.field public static final action_bar_select_all_checkbox_margin:I = 0x7f0d025d

.field public static final action_bar_select_all_list_padding_left:I = 0x7f0d0262

.field public static final action_bar_selected_spinner_button_width:I = 0x7f0d0258

.field public static final action_bar_selected_spinner_popup_menu_item_padding:I = 0x7f0d025c

.field public static final action_bar_selected_spinner_width:I = 0x7f0d0006

.field public static final action_bar_selected_spinner_width_max:I = 0x7f0d0257

.field public static final action_bar_spinner_height:I = 0x7f0d0251

.field public static final action_bar_spinner_popup_Vertical_offset:I = 0x7f0d025a

.field public static final action_bar_spinner_popup_horizontal_offset:I = 0x7f0d0259

.field public static final action_bar_spinner_popup_item_height:I = 0x7f0d0260

.field public static final action_bar_spinner_popup_item_text_size:I = 0x7f0d0261

.field public static final action_bar_spinner_popup_vertical_offset_for_delete_history:I = 0x7f0d025b

.field public static final action_bar_spinner_text_padding_end:I = 0x7f0d025f

.field public static final action_bar_spinner_text_padding_start:I = 0x7f0d025e

.field public static final action_bar_spinner_text_size:I = 0x7f0d0252

.field public static final action_bar_switch_padding:I = 0x7f0d0128

.field public static final action_button_padding_horizontal:I = 0x7f0d03e6

.field public static final actionbar_done_and_cancel_divider_padding:I = 0x7f0d0331

.field public static final actionbar_height_l:I = 0x7f0d0375

.field public static final actionbar_height_p:I = 0x7f0d0374

.field public static final add_tag_edit_addbtn_margin_left:I = 0x7f0d00be

.field public static final album_new_label_font_size:I = 0x7f0d037c

.field public static final album_new_label_height:I = 0x7f0d0377

.field public static final album_new_label_padding:I = 0x7f0d0379

.field public static final album_new_label_right_margin:I = 0x7f0d037b

.field public static final album_new_label_top_margin:I = 0x7f0d037a

.field public static final album_new_label_width:I = 0x7f0d0378

.field public static final album_set_item_image_height:I = 0x7f0d0073

.field public static final album_set_item_width:I = 0x7f0d0074

.field public static final album_slot_gap:I = 0x7f0d006f

.field public static final album_view_album_label_padding_left:I = 0x7f0d0376

.field public static final album_view_border_width:I = 0x7f0d02d6

.field public static final album_view_easy_mode_title_count_font_size:I = 0x7f0d031b

.field public static final album_view_easy_mode_title_font_size:I = 0x7f0d031a

.field public static final album_view_easy_mode_title_height:I = 0x7f0d0317

.field public static final album_view_easy_mode_title_margin_bottom:I = 0x7f0d0318

.field public static final album_view_easy_mode_title_margin_top:I = 0x7f0d0316

.field public static final album_view_easy_mode_title_width:I = 0x7f0d0319

.field public static final album_view_mediatype_icon_padding_bottom:I = 0x7f0d0384

.field public static final album_view_mediatype_icon_padding_left:I = 0x7f0d0383

.field public static final album_view_noitems_bg_height_landscape:I = 0x7f0d037e

.field public static final album_view_noitems_bg_height_portrait:I = 0x7f0d037f

.field public static final album_view_noitems_popup_padding_right:I = 0x7f0d0381

.field public static final album_view_noitems_popup_padding_right_nomenu:I = 0x7f0d0382

.field public static final album_view_noitems_popup_padding_right_picker:I = 0x7f0d036b

.field public static final album_view_noitems_popup_padding_top:I = 0x7f0d0380

.field public static final album_view_scrollbar_width:I = 0x7f0d037d

.field public static final album_view_title_count_font_size:I = 0x7f0d0314

.field public static final album_view_title_font_size:I = 0x7f0d0313

.field public static final album_view_title_h_margin:I = 0x7f0d0312

.field public static final album_view_title_height:I = 0x7f0d030f

.field public static final album_view_title_left_margin:I = 0x7f0d0315

.field public static final album_view_title_margin_bottom:I = 0x7f0d0310

.field public static final album_view_title_margin_top:I = 0x7f0d030e

.field public static final album_view_title_width:I = 0x7f0d0311

.field public static final album_view_top_margin:I = 0x7f0d030d

.field public static final albumset_count_font_size:I = 0x7f0d006b

.field public static final albumset_count_offset:I = 0x7f0d0069

.field public static final albumset_drawframe_bottom_padding:I = 0x7f0d00d8

.field public static final albumset_drawframe_left_padding:I = 0x7f0d00d5

.field public static final albumset_drawframe_right_padding:I = 0x7f0d00d7

.field public static final albumset_drawframe_top_padding:I = 0x7f0d00d6

.field public static final albumset_drawframes_2_bottom_padding:I = 0x7f0d00dc

.field public static final albumset_drawframes_2_left_padding:I = 0x7f0d00d9

.field public static final albumset_drawframes_2_right_padding:I = 0x7f0d00db

.field public static final albumset_drawframes_2_top_padding:I = 0x7f0d00da

.field public static final albumset_drawframes_3_bottom_padding:I = 0x7f0d00e0

.field public static final albumset_drawframes_3_left_padding:I = 0x7f0d00dd

.field public static final albumset_drawframes_3_right_padding:I = 0x7f0d00df

.field public static final albumset_drawframes_3_top_padding:I = 0x7f0d00de

.field public static final albumset_drawlabel_bottom_margin:I = 0x7f0d00e3

.field public static final albumset_drawlabel_count_font:I = 0x7f0d00e2

.field public static final albumset_drawlabel_left_margin:I = 0x7f0d00e4

.field public static final albumset_drawlabel_top_margin:I = 0x7f0d00e5

.field public static final albumset_icon_size:I = 0x7f0d006e

.field public static final albumset_imageview_2_bottom_margin:I = 0x7f0d00d0

.field public static final albumset_imageview_2_left_margin:I = 0x7f0d00cd

.field public static final albumset_imageview_2_right_margin:I = 0x7f0d00cf

.field public static final albumset_imageview_2_top_margin:I = 0x7f0d00ce

.field public static final albumset_imageview_3_bottom_margin:I = 0x7f0d00d4

.field public static final albumset_imageview_3_left_margin:I = 0x7f0d00d1

.field public static final albumset_imageview_3_right_margin:I = 0x7f0d00d3

.field public static final albumset_imageview_3_top_margin:I = 0x7f0d00d2

.field public static final albumset_imageview_bottom_margin:I = 0x7f0d00cc

.field public static final albumset_imageview_left_margin:I = 0x7f0d00c9

.field public static final albumset_imageview_right_margin:I = 0x7f0d00cb

.field public static final albumset_imageview_top_margin:I = 0x7f0d00ca

.field public static final albumset_label_background_height:I = 0x7f0d0067

.field public static final albumset_left_margin:I = 0x7f0d006c

.field public static final albumset_padding_bottom:I = 0x7f0d0065

.field public static final albumset_padding_top:I = 0x7f0d0064

.field public static final albumset_slot_gap:I = 0x7f0d0066

.field public static final albumset_title_font_size:I = 0x7f0d006a

.field public static final albumset_title_offset:I = 0x7f0d0068

.field public static final albumset_title_right_margin:I = 0x7f0d006d

.field public static final albumset_typeicon_bottom_margin:I = 0x7f0d00e1

.field public static final apdater_top_margin:I = 0x7f0d0246

.field public static final app_defaultsize_h:I = 0x7f0d0134

.field public static final app_defaultsize_w:I = 0x7f0d0133

.field public static final appwidget_bottom_padding:I = 0x7f0d00c8

.field public static final appwidget_height:I = 0x7f0d0061

.field public static final appwidget_width:I = 0x7f0d0060

.field public static final badge_fontsize_default:I = 0x7f0d022e

.field public static final badge_fontsize_small:I = 0x7f0d022f

.field public static final big_setting_popup_window_width:I = 0x7f0d0016

.field public static final big_setting_popup_window_width_xlarge:I = 0x7f0d0038

.field public static final border_width:I = 0x7f0d02d7

.field public static final ca_bg_height:I = 0x7f0d02d0

.field public static final ca_icon_left_margin:I = 0x7f0d02c9

.field public static final ca_item_height:I = 0x7f0d02c8

.field public static final ca_text_bg_vertical_margin:I = 0x7f0d02cb

.field public static final ca_text_date_top_margin:I = 0x7f0d02cc

.field public static final ca_text_horizontal_left_margin:I = 0x7f0d02ca

.field public static final ca_text_size:I = 0x7f0d02cd

.field public static final ca_text_size_for_date:I = 0x7f0d02ce

.field public static final ca_text_vertical_bottom_margin:I = 0x7f0d02cf

.field public static final cache_pin_margin:I = 0x7f0d0071

.field public static final cache_pin_size:I = 0x7f0d0070

.field public static final camera_button_help_popup_width:I = 0x7f0d01dd

.field public static final camera_controls_size:I = 0x7f0d005e

.field public static final camera_film_strip_gap:I = 0x7f0d005f

.field public static final capture_border:I = 0x7f0d005b

.field public static final capture_margin_right:I = 0x7f0d005c

.field public static final capture_margin_top:I = 0x7f0d005d

.field public static final capture_size:I = 0x7f0d005a

.field public static final card_default_divider_page_margin_left:I = 0x7f0d0367

.field public static final card_default_divider_page_margin_right:I = 0x7f0d0368

.field public static final change_player_dialog_header_text_size:I = 0x7f0d0219

.field public static final change_player_dialog_item_height:I = 0x7f0d021a

.field public static final change_player_dialog_progressBar_height:I = 0x7f0d0217

.field public static final change_player_dialog_progressBar_width:I = 0x7f0d0218

.field public static final change_player_dialog_title_height:I = 0x7f0d0216

.field public static final check_box_height:I = 0x7f0d02e4

.field public static final check_box_width:I = 0x7f0d02e3

.field public static final checkbox_right_margin:I = 0x7f0d0154

.field public static final checkbox_top_margin:I = 0x7f0d0153

.field public static final colorpicker_offset_min_height:I = 0x7f0d0148

.field public static final colorpicker_offset_min_width:I = 0x7f0d0149

.field public static final comment_reply_view_bottom_margin:I = 0x7f0d023b

.field public static final comment_reply_view_height:I = 0x7f0d0239

.field public static final comment_reply_view_to_filmstrip_min_offset:I = 0x7f0d023c

.field public static final comment_reply_view_top_margin:I = 0x7f0d023a

.field public static final common_thumbnail_list_item_height:I = 0x7f0d02b8

.field public static final common_thumbnail_list_item_padding_between:I = 0x7f0d02b9

.field public static final common_thumbnail_list_item_padding_right:I = 0x7f0d02ba

.field public static final contact_4btn_layout_margin_top:I = 0x7f0d0301

.field public static final contact_btn_layout_margin_left:I = 0x7f0d02ff

.field public static final contact_btn_layout_margin_right:I = 0x7f0d0300

.field public static final contact_btn_layout_margin_top:I = 0x7f0d02fe

.field public static final contact_dialog_head_height:I = 0x7f0d03db

.field public static final contact_sns_btn_layout_margin_top:I = 0x7f0d0302

.field public static final contextual_tag_help_txt_size:I = 0x7f0d0129

.field public static final contextual_tag_screen_overlay_positon_y:I = 0x7f0d014b

.field public static final cover_btn_margin_right:I = 0x7f0d024a

.field public static final cover_btn_margin_top:I = 0x7f0d0249

.field public static final cover_screen_height:I = 0x7f0d0248

.field public static final cover_screen_width:I = 0x7f0d0247

.field public static final crop_indicator_size:I = 0x7f0d03ee

.field public static final crop_min_side:I = 0x7f0d0002

.field public static final crop_touch_tolerance:I = 0x7f0d0003

.field public static final custom_actionbar_btn_margin:I = 0x7f0d0255

.field public static final custom_actionbar_btn_text_size:I = 0x7f0d0256

.field public static final custom_actionbar_divider_height:I = 0x7f0d0254

.field public static final custom_actionbar_margin_top:I = 0x7f0d0253

.field public static final custom_dialog_bottom_margin:I = 0x7f0d0152

.field public static final custom_dialog_left_margin:I = 0x7f0d014f

.field public static final custom_dialog_right_margin:I = 0x7f0d0151

.field public static final custom_dialog_top_margin:I = 0x7f0d0150

.field public static final custom_search_view_hint_text_height:I = 0x7f0d0372

.field public static final custom_search_view_hint_text_size:I = 0x7f0d0373

.field public static final custom_search_view_icon_margin_right:I = 0x7f0d0297

.field public static final custom_search_view_icon_search_height:I = 0x7f0d036f

.field public static final custom_search_view_icon_search_margin_left:I = 0x7f0d0370

.field public static final custom_search_view_icon_search_margin_right:I = 0x7f0d0371

.field public static final custom_search_view_icon_search_width:I = 0x7f0d036e

.field public static final custom_search_view_search_progress_height:I = 0x7f0d0358

.field public static final custom_search_view_search_progress_width:I = 0x7f0d0357

.field public static final custom_tab_dialog_height:I = 0x7f0d01ef

.field public static final default_hint_margin_top:I = 0x7f0d035a

.field public static final default_hint_size:I = 0x7f0d0359

.field public static final detail_content_height:I = 0x7f0d0222

.field public static final detail_list_padding_top:I = 0x7f0d021e

.field public static final detail_popup_left_margin:I = 0x7f0d023e

.field public static final detail_popup_model_text_size:I = 0x7f0d0243

.field public static final detail_popup_side_padding:I = 0x7f0d023f

.field public static final detail_popup_top_margin:I = 0x7f0d023d

.field public static final detail_title_botton_margin:I = 0x7f0d0221

.field public static final detail_view_icon_gap:I = 0x7f0d02de

.field public static final details_list_text_size:I = 0x7f0d0330

.field public static final detial_list_padding_left:I = 0x7f0d021f

.field public static final detial_list_padding_right:I = 0x7f0d0220

.field public static final dialog_common_list_padding_left:I = 0x7f0d02b1

.field public static final dialog_common_list_padding_right:I = 0x7f0d02b2

.field public static final dialog_common_list_padding_top:I = 0x7f0d02b3

.field public static final dialog_common_view_padding_bottom:I = 0x7f0d02b7

.field public static final dialog_common_view_padding_left:I = 0x7f0d02b4

.field public static final dialog_common_view_padding_right:I = 0x7f0d02b5

.field public static final dialog_common_view_padding_top:I = 0x7f0d02b6

.field public static final dialog_height_portrait:I = 0x7f0d012b

.field public static final dialog_text_bottom_margin:I = 0x7f0d014e

.field public static final dialog_text_left_margin:I = 0x7f0d014c

.field public static final dialog_text_right_margin:I = 0x7f0d036c

.field public static final dialog_text_top_margin:I = 0x7f0d014d

.field public static final drag_and_drop_text_size:I = 0x7f0d022a

.field public static final drag_and_drop_thumb_height:I = 0x7f0d0228

.field public static final drag_and_drop_thumb_width:I = 0x7f0d0229

.field public static final drawer_header_item_title_text_size:I = 0x7f0d02be

.field public static final drawer_header_title_text_size:I = 0x7f0d02bd

.field public static final drawer_help_margin_top:I = 0x7f0d02c7

.field public static final drawer_left_touch_area:I = 0x7f0d0303

.field public static final drawer_list_emptyitem_height:I = 0x7f0d02c3

.field public static final drawer_list_header_title_container_height:I = 0x7f0d02c0

.field public static final drawer_list_header_title_height:I = 0x7f0d02bf

.field public static final drawer_list_item_height:I = 0x7f0d02c2

.field public static final drawer_list_item_text_size:I = 0x7f0d02c5

.field public static final drawer_list_items_padding_left_right:I = 0x7f0d02c1

.field public static final drawer_list_left_right_margin:I = 0x7f0d02c4

.field public static final drawer_list_top_margin:I = 0x7f0d02c6

.field public static final drawer_listitem_icon_height:I = 0x7f0d02bc

.field public static final drawer_listitem_icon_width:I = 0x7f0d02bb

.field public static final drop_down_list_item_height:I = 0x7f0d02df

.field public static final effect_icon_size:I = 0x7f0d03df

.field public static final effect_label_margin_top:I = 0x7f0d03de

.field public static final effect_label_text_size:I = 0x7f0d03dc

.field public static final effect_label_width:I = 0x7f0d03dd

.field public static final effect_padding_horizontal:I = 0x7f0d03e0

.field public static final effect_setting_clear_text_min_height:I = 0x7f0d001d

.field public static final effect_setting_clear_text_min_height_xlarge:I = 0x7f0d003f

.field public static final effect_setting_clear_text_size:I = 0x7f0d001c

.field public static final effect_setting_clear_text_size_xlarge:I = 0x7f0d003e

.field public static final effect_setting_item_icon_width:I = 0x7f0d0018

.field public static final effect_setting_item_icon_width_xlarge:I = 0x7f0d003a

.field public static final effect_setting_item_text_size:I = 0x7f0d0019

.field public static final effect_setting_item_text_size_xlarge:I = 0x7f0d003b

.field public static final effect_setting_type_text_left_padding:I = 0x7f0d001e

.field public static final effect_setting_type_text_left_padding_xlarge:I = 0x7f0d0040

.field public static final effect_setting_type_text_min_height:I = 0x7f0d001b

.field public static final effect_setting_type_text_min_height_xlarge:I = 0x7f0d003d

.field public static final effect_setting_type_text_size:I = 0x7f0d001a

.field public static final effect_setting_type_text_size_xlarge:I = 0x7f0d003c

.field public static final effect_space_height:I = 0x7f0d01f1

.field public static final effect_tool_panel_padding_bottom:I = 0x7f0d03e9

.field public static final effect_tool_panel_padding_top:I = 0x7f0d03e8

.field public static final effects_container_padding:I = 0x7f0d03e1

.field public static final effects_menu_container_width:I = 0x7f0d03e7

.field public static final event_view_items_gap:I = 0x7f0d02b0

.field public static final event_view_sub_title_font_size:I = 0x7f0d02a3

.field public static final event_view_sub_title_height:I = 0x7f0d02a7

.field public static final event_view_suggestion_font_size:I = 0x7f0d02a4

.field public static final event_view_suggestion_height:I = 0x7f0d02aa

.field public static final event_view_suggestion_left_padding:I = 0x7f0d02a5

.field public static final event_view_suggestion_right_margin:I = 0x7f0d02ac

.field public static final event_view_suggestion_top_margin:I = 0x7f0d02ab

.field public static final event_view_thumb_max_height:I = 0x7f0d02ae

.field public static final event_view_title_font_size:I = 0x7f0d02a2

.field public static final event_view_title_height:I = 0x7f0d02a6

.field public static final event_view_title_left_margin:I = 0x7f0d02a8

.field public static final event_view_title_padding_for_ssfont:I = 0x7f0d02af

.field public static final event_view_title_top_margin:I = 0x7f0d02a9

.field public static final event_view_video_icon_size:I = 0x7f0d02ad

.field public static final expanded_mode_margin_bottom:I = 0x7f0d02fd

.field public static final expanded_mode_margin_top:I = 0x7f0d02fc

.field public static final expansion_height:I = 0x7f0d02e6

.field public static final expansion_width:I = 0x7f0d02e5

.field public static final face_circle_stroke:I = 0x7f0d0053

.field public static final filmstrip_bar_size:I = 0x7f0d0111

.field public static final filmstrip_border_margin:I = 0x7f0d0116

.field public static final filmstrip_bottom_margin:I = 0x7f0d010b

.field public static final filmstrip_content_size:I = 0x7f0d010f

.field public static final filmstrip_grip_size:I = 0x7f0d0110

.field public static final filmstrip_grip_width:I = 0x7f0d0112

.field public static final filmstrip_land_bottom_margin:I = 0x7f0d010c

.field public static final filmstrip_mid_margin:I = 0x7f0d010a

.field public static final filmstrip_port_bottom_margin:I = 0x7f0d010d

.field public static final filmstrip_scroll_bottom_padding:I = 0x7f0d0113

.field public static final filmstrip_scrollbar_bottom_padding:I = 0x7f0d0115

.field public static final filmstrip_scrollbar_height:I = 0x7f0d0114

.field public static final filmstrip_thumb_size:I = 0x7f0d010e

.field public static final filmstrip_top_margin:I = 0x7f0d0109

.field public static final filter_item_title_text_size:I = 0x7f0d0202

.field public static final filter_panel_button_group_height:I = 0x7f0d0344

.field public static final filter_panel_button_group_icon_height:I = 0x7f0d0343

.field public static final filter_panel_button_group_icon_width:I = 0x7f0d0342

.field public static final filter_panel_button_group_padding_bottom:I = 0x7f0d0345

.field public static final filter_panel_detail_bg_height:I = 0x7f0d0346

.field public static final filter_panel_detail_horizontal_spacing:I = 0x7f0d0354

.field public static final filter_panel_detail_item_height:I = 0x7f0d0348

.field public static final filter_panel_detail_item_text_size:I = 0x7f0d0349

.field public static final filter_panel_detail_min_height:I = 0x7f0d0356

.field public static final filter_panel_detail_padding_bottom:I = 0x7f0d0353

.field public static final filter_panel_detail_padding_left:I = 0x7f0d0350

.field public static final filter_panel_detail_padding_right:I = 0x7f0d0352

.field public static final filter_panel_detail_padding_top:I = 0x7f0d0351

.field public static final filter_panel_detail_rectangle_height:I = 0x7f0d0347

.field public static final filter_panel_detail_vertical_spacing:I = 0x7f0d0355

.field public static final filter_panel_horizontal_scroll_height:I = 0x7f0d0341

.field public static final filter_panel_horizontal_scroll_padding_bottom:I = 0x7f0d0340

.field public static final filter_panel_main_padding_bottom:I = 0x7f0d033f

.field public static final filter_panel_main_padding_left:I = 0x7f0d033c

.field public static final filter_panel_main_padding_right:I = 0x7f0d033d

.field public static final filter_panel_main_padding_top:I = 0x7f0d033e

.field public static final filter_panel_selected_item_height:I = 0x7f0d034a

.field public static final filter_panel_selected_item_horizontal_spacing:I = 0x7f0d034f

.field public static final filter_panel_selected_item_padding_left:I = 0x7f0d034c

.field public static final filter_panel_selected_item_padding_right:I = 0x7f0d034d

.field public static final filter_panel_selected_item_remove_button_margin_right:I = 0x7f0d034e

.field public static final filter_panel_selected_item_text_size:I = 0x7f0d034b

.field public static final focus_inner_offset:I = 0x7f0d004e

.field public static final focus_inner_stroke:I = 0x7f0d0050

.field public static final focus_outer_stroke:I = 0x7f0d004f

.field public static final focus_radius_offset:I = 0x7f0d004d

.field public static final focus_shot_help_text_left_margin:I = 0x7f0d0332

.field public static final folder_icon_left_padding:I = 0x7f0d0225

.field public static final folder_icon_size:I = 0x7f0d0224

.field public static final folder_text_left_padding:I = 0x7f0d0226

.field public static final gallery_search_count_height:I = 0x7f0d0264

.field public static final gallery_search_field_text_size:I = 0x7f0d026e

.field public static final gallery_search_filtertag_height:I = 0x7f0d026a

.field public static final gallery_search_icons_size:I = 0x7f0d026f

.field public static final gallery_search_input_field_padding_right:I = 0x7f0d0369

.field public static final gallery_search_list_group_button_margin_right:I = 0x7f0d027a

.field public static final gallery_search_list_group_height:I = 0x7f0d0275

.field public static final gallery_search_list_group_icon_height:I = 0x7f0d0277

.field public static final gallery_search_list_group_icon_margin_right:I = 0x7f0d0278

.field public static final gallery_search_list_group_icon_width:I = 0x7f0d0276

.field public static final gallery_search_list_group_margin_top:I = 0x7f0d036d

.field public static final gallery_search_list_group_title_size:I = 0x7f0d0279

.field public static final gallery_search_list_padding_left:I = 0x7f0d0272

.field public static final gallery_search_list_padding_right:I = 0x7f0d0271

.field public static final gallery_search_list_padding_top:I = 0x7f0d0270

.field public static final gallery_search_list_row_height:I = 0x7f0d0274

.field public static final gallery_search_result_height:I = 0x7f0d0269

.field public static final gallery_search_result_margin_left:I = 0x7f0d026b

.field public static final gallery_search_result_margin_right:I = 0x7f0d026c

.field public static final gallery_search_result_thumbnail_gap:I = 0x7f0d026d

.field public static final gallery_search_result_title_height:I = 0x7f0d0268

.field public static final gallery_search_search_bar_margin_left:I = 0x7f0d027b

.field public static final gallery_search_subtitle_text_size:I = 0x7f0d027e

.field public static final gallery_search_subtitle_text_size_easy_mode:I = 0x7f0d027f

.field public static final gallery_search_suggestion_list_padding_end:I = 0x7f0d03f2

.field public static final gallery_search_suggestion_list_padding_start:I = 0x7f0d03f1

.field public static final gallery_search_text_field_margin_end:I = 0x7f0d03f0

.field public static final gallery_search_text_field_padding_start:I = 0x7f0d03ef

.field public static final gallery_search_text_obj_height:I = 0x7f0d0265

.field public static final gallery_search_text_obj_width:I = 0x7f0d0266

.field public static final gallery_search_title_height:I = 0x7f0d0263

.field public static final gallery_search_ui_left_pane_width:I = 0x7f0d0273

.field public static final gallery_search_underline_obj_height:I = 0x7f0d0267

.field public static final gallery_search_view_bound_between_hint_and_icon:I = 0x7f0d0280

.field public static final gallery_search_view_close_tag_list_button_height:I = 0x7f0d0282

.field public static final gallery_search_view_edit_padding_left:I = 0x7f0d02a0

.field public static final gallery_search_view_edit_padding_right:I = 0x7f0d02a1

.field public static final gallery_search_view_height:I = 0x7f0d0281

.field public static final gallery_search_view_icon_divider_height:I = 0x7f0d0299

.field public static final gallery_search_view_icon_divider_margin_right:I = 0x7f0d029a

.field public static final gallery_search_view_icon_divider_width:I = 0x7f0d0298

.field public static final gallery_search_view_multiwindow_offset:I = 0x7f0d027d

.field public static final gallery_search_view_result_thumb_height:I = 0x7f0d029f

.field public static final gallery_search_view_result_thumb_width:I = 0x7f0d029e

.field public static final gallery_search_view_results_left_margin:I = 0x7f0d029c

.field public static final gallery_search_view_results_right_margin:I = 0x7f0d029d

.field public static final gallery_search_view_results_title_margin:I = 0x7f0d027c

.field public static final gallery_search_view_results_width_land:I = 0x7f0d029b

.field public static final group_title_height:I = 0x7f0d031c

.field public static final group_title_height_easy_mode:I = 0x7f0d031d

.field public static final group_v_gap:I = 0x7f0d031e

.field public static final header_bottom_margin:I = 0x7f0d0240

.field public static final help_air_command_hover_ring_x:I = 0x7f0d01c6

.field public static final help_air_command_hover_ring_x_land:I = 0x7f0d01d0

.field public static final help_air_command_hover_ring_y:I = 0x7f0d01c7

.field public static final help_air_command_hover_ring_y_land:I = 0x7f0d01d1

.field public static final help_air_command_hover_view_h:I = 0x7f0d01cf

.field public static final help_air_command_hover_view_w:I = 0x7f0d01ce

.field public static final help_air_command_hover_view_x:I = 0x7f0d01cc

.field public static final help_air_command_hover_view_x_land:I = 0x7f0d01d6

.field public static final help_air_command_hover_view_y:I = 0x7f0d01cd

.field public static final help_air_command_hover_view_y_land:I = 0x7f0d01d7

.field public static final help_air_command_hover_y_land:I = 0x7f0d01d8

.field public static final help_air_command_hover_y_port:I = 0x7f0d01d9

.field public static final help_air_command_tip_bubble_h:I = 0x7f0d01cb

.field public static final help_air_command_tip_bubble_h_land:I = 0x7f0d01d5

.field public static final help_air_command_tip_bubble_w:I = 0x7f0d01ca

.field public static final help_air_command_tip_bubble_w_land:I = 0x7f0d01d4

.field public static final help_air_command_tip_bubble_x:I = 0x7f0d01c8

.field public static final help_air_command_tip_bubble_x_land:I = 0x7f0d01d2

.field public static final help_air_command_tip_bubble_y:I = 0x7f0d01c9

.field public static final help_air_command_tip_bubble_y_land:I = 0x7f0d01d3

.field public static final help_album_popup_body_portrait_offsetX:I = 0x7f0d01ac

.field public static final help_album_popup_body_x:I = 0x7f0d01a8

.field public static final help_album_popup_body_x_easy:I = 0x7f0d01a9

.field public static final help_album_popup_body_x_easy_land:I = 0x7f0d01b9

.field public static final help_album_popup_body_x_land:I = 0x7f0d01af

.field public static final help_album_popup_body_y:I = 0x7f0d01a7

.field public static final help_album_popup_body_y_easy:I = 0x7f0d01c4

.field public static final help_album_popup_body_y_easy_land:I = 0x7f0d01ba

.field public static final help_album_popup_body_y_land:I = 0x7f0d01b0

.field public static final help_album_popup_body_y_mea_portrait:I = 0x7f0d01aa

.field public static final help_album_popup_head_x:I = 0x7f0d00c7

.field public static final help_album_popup_head_x_easy:I = 0x7f0d0174

.field public static final help_album_popup_head_x_easy_land:I = 0x7f0d01b2

.field public static final help_album_popup_head_x_land:I = 0x7f0d01b1

.field public static final help_album_popup_head_x_portrait_extra:I = 0x7f0d01ab

.field public static final help_album_tap_x:I = 0x7f0d01a5

.field public static final help_album_tap_x_easy:I = 0x7f0d0173

.field public static final help_album_tap_x_easy_land:I = 0x7f0d01bb

.field public static final help_album_tap_x_land:I = 0x7f0d01ad

.field public static final help_album_tap_y:I = 0x7f0d01a6

.field public static final help_album_tap_y_easy:I = 0x7f0d01c5

.field public static final help_album_tap_y_easy_land:I = 0x7f0d01bc

.field public static final help_album_tap_y_land:I = 0x7f0d01ae

.field public static final help_clickable_image_height_appendix:I = 0x7f0d01c0

.field public static final help_clickable_image_land_height_appendix:I = 0x7f0d01c1

.field public static final help_cue_margin_top:I = 0x7f0d016c

.field public static final help_edit_picture_detail_popup_x:I = 0x7f0d0162

.field public static final help_edit_picture_timeview_item_min_size:I = 0x7f0d0161

.field public static final help_edit_picture_timeview_item_x:I = 0x7f0d015f

.field public static final help_edit_picture_timeview_item_y:I = 0x7f0d0160

.field public static final help_edit_picture_timeview_popup_x:I = 0x7f0d015d

.field public static final help_edit_picture_timeview_popup_y:I = 0x7f0d015e

.field public static final help_face_detail_rect_head_land_margin_left:I = 0x7f0d0192

.field public static final help_face_detail_rect_head_margin_left:I = 0x7f0d0191

.field public static final help_face_detail_rect_popup_land_margin_left:I = 0x7f0d0190

.field public static final help_face_detail_rect_popup_land_margin_top:I = 0x7f0d018f

.field public static final help_face_detail_rect_popup_margin_left:I = 0x7f0d018e

.field public static final help_face_detail_rect_popup_margin_top:I = 0x7f0d018d

.field public static final help_face_detail_rect_tap_land_margin_left:I = 0x7f0d018c

.field public static final help_face_detail_rect_tap_land_margin_top:I = 0x7f0d018b

.field public static final help_face_detail_rect_tap_margin_left:I = 0x7f0d018a

.field public static final help_face_detail_rect_tap_margin_top:I = 0x7f0d0189

.field public static final help_face_photo_head_land_margin_left:I = 0x7f0d0180

.field public static final help_face_photo_head_land_margin_top:I = 0x7f0d017f

.field public static final help_face_photo_head_margin_left:I = 0x7f0d017e

.field public static final help_face_photo_head_margin_top:I = 0x7f0d017d

.field public static final help_face_photo_item_height:I = 0x7f0d0186

.field public static final help_face_photo_item_land_height:I = 0x7f0d0188

.field public static final help_face_photo_item_land_margin_left:I = 0x7f0d0184

.field public static final help_face_photo_item_land_margin_top:I = 0x7f0d0183

.field public static final help_face_photo_item_land_width:I = 0x7f0d0187

.field public static final help_face_photo_item_margin_left:I = 0x7f0d0182

.field public static final help_face_photo_item_margin_top:I = 0x7f0d0181

.field public static final help_face_photo_item_width:I = 0x7f0d0185

.field public static final help_face_photo_popup_land_margin_left:I = 0x7f0d017c

.field public static final help_face_photo_popup_land_margin_top:I = 0x7f0d017b

.field public static final help_face_photo_popup_margin_left:I = 0x7f0d017a

.field public static final help_face_photo_popup_margin_top:I = 0x7f0d0179

.field public static final help_face_photo_tap_land_margin_left:I = 0x7f0d0178

.field public static final help_face_photo_tap_land_margin_top:I = 0x7f0d0177

.field public static final help_face_photo_tap_margin_left:I = 0x7f0d0176

.field public static final help_face_photo_tap_margin_top:I = 0x7f0d0175

.field public static final help_infomation_preview_hover_tap_offset:I = 0x7f0d01dc

.field public static final help_infomation_preview_hover_y_land:I = 0x7f0d01da

.field public static final help_infomation_preview_hover_y_port:I = 0x7f0d01db

.field public static final help_information_preview_y_bottom_margin_land:I = 0x7f0d0166

.field public static final help_information_preview_y_margin:I = 0x7f0d0164

.field public static final help_information_preview_y_margin_land:I = 0x7f0d0165

.field public static final help_margin:I = 0x7f0d0167

.field public static final help_margin_bottom:I = 0x7f0d0168

.field public static final help_margin_left:I = 0x7f0d0169

.field public static final help_margin_left_additional:I = 0x7f0d016a

.field public static final help_margin_top:I = 0x7f0d016b

.field public static final help_photo_popup_body_x_land:I = 0x7f0d01b5

.field public static final help_photo_popup_body_y_land:I = 0x7f0d01b6

.field public static final help_photo_popup_head_x:I = 0x7f0d01b7

.field public static final help_photo_popup_head_x_land:I = 0x7f0d01b8

.field public static final help_photo_tap_x_land:I = 0x7f0d01b3

.field public static final help_photo_tap_y_land:I = 0x7f0d01b4

.field public static final help_photonote_head_margin:I = 0x7f0d01bf

.field public static final help_popup_tap_padding_right:I = 0x7f0d01c3

.field public static final help_tap_position_x:I = 0x7f0d01bd

.field public static final help_tap_position_y:I = 0x7f0d01be

.field public static final help_tilt_text_margin_top:I = 0x7f0d019d

.field public static final help_tilt_to_zoom_tap_margin:I = 0x7f0d01c2

.field public static final help_time_item_size:I = 0x7f0d016f

.field public static final help_time_item_size_land:I = 0x7f0d0172

.field public static final help_time_item_x:I = 0x7f0d016d

.field public static final help_time_item_x_land:I = 0x7f0d0170

.field public static final help_time_item_y:I = 0x7f0d016e

.field public static final help_time_item_y_land:I = 0x7f0d0171

.field public static final help_tipbubble_face_text_head_margin_top:I = 0x7f0d019b

.field public static final help_tipbubble_face_text_margin_top:I = 0x7f0d019a

.field public static final help_tipbubble_list_scroll_top_margin:I = 0x7f0d01a4

.field public static final help_tipbubble_photo_picker_head_margin_right:I = 0x7f0d0194

.field public static final help_tipbubble_photo_picker_head_margin_top:I = 0x7f0d0195

.field public static final help_tipbubble_photo_picker_text_margin_top:I = 0x7f0d0193

.field public static final help_tipbubble_short_text_max_width:I = 0x7f0d0199

.field public static final help_tipbubble_text_head_margin_bottom:I = 0x7f0d0196

.field public static final help_tipbubble_text_head_padding_bottom:I = 0x7f0d019e

.field public static final help_tipbubble_text_land_max_width:I = 0x7f0d0198

.field public static final help_tipbubble_text_margin_top:I = 0x7f0d019c

.field public static final help_tipbubble_text_max_width:I = 0x7f0d0197

.field public static final help_tipbubble_text_padding_bottom:I = 0x7f0d019f

.field public static final help_tipbubble_text_padding_left:I = 0x7f0d01a0

.field public static final help_tipbubble_text_padding_right:I = 0x7f0d01a1

.field public static final help_tipbubble_text_padding_top:I = 0x7f0d01a2

.field public static final help_tipbubble_text_size:I = 0x7f0d01a3

.field public static final help_viewing_pictures_popup_x:I = 0x7f0d00c6

.field public static final help_viewing_pictures_popup_x_potrait_photo:I = 0x7f0d0163

.field public static final hint_y_offset:I = 0x7f0d0007

.field public static final history_divider_height:I = 0x7f0d036a

.field public static final history_item_list_fullscreen_date_text_margin_right:I = 0x7f0d035d

.field public static final history_item_list_fullscreen_date_text_size:I = 0x7f0d035e

.field public static final history_item_list_fullscreen_delete_all_height:I = 0x7f0d0363

.field public static final history_item_list_fullscreen_delete_all_text_size:I = 0x7f0d0362

.field public static final history_item_list_fullscreen_delete_icon_margin_left:I = 0x7f0d0366

.field public static final history_item_list_fullscreen_delete_icon_size:I = 0x7f0d0365

.field public static final history_item_list_fullscreen_divider_inset:I = 0x7f0d0364

.field public static final history_item_list_fullscreen_keyword_text_margin_left:I = 0x7f0d035f

.field public static final history_item_list_fullscreen_keyword_text_padding_right:I = 0x7f0d0360

.field public static final history_item_list_fullscreen_keyword_text_size:I = 0x7f0d0361

.field public static final history_item_list_fullscreen_list_margin:I = 0x7f0d035b

.field public static final history_item_list_fullscreen_text_layout_height:I = 0x7f0d035c

.field public static final hovering_actionbar_button_height:I = 0x7f0d03ac

.field public static final hovering_actionbar_button_label_height:I = 0x7f0d03b4

.field public static final hovering_actionbar_button_label_width:I = 0x7f0d03b5

.field public static final hovering_actionbar_button_margin_left:I = 0x7f0d03ad

.field public static final hovering_actionbar_button_margin_right:I = 0x7f0d03ae

.field public static final hovering_actionbar_button_width:I = 0x7f0d03ab

.field public static final hovering_actionbar_divider_height:I = 0x7f0d03a9

.field public static final hovering_actionbar_divider_width:I = 0x7f0d03aa

.field public static final hovering_actionbar_height:I = 0x7f0d03af

.field public static final hovering_actionbar_menu_font_size:I = 0x7f0d03b3

.field public static final hovering_actionbar_min_height:I = 0x7f0d03b1

.field public static final hovering_actionbar_min_width:I = 0x7f0d03b2

.field public static final hovering_actionbar_shadow_size:I = 0x7f0d03b0

.field public static final hovering_background_res_padding_bottom:I = 0x7f0d03a7

.field public static final hovering_background_res_padding_inside:I = 0x7f0d03a8

.field public static final hovering_background_res_padding_left:I = 0x7f0d03a5

.field public static final hovering_background_res_padding_right:I = 0x7f0d03a6

.field public static final hovering_background_res_padding_top:I = 0x7f0d03a4

.field public static final hovering_image_gap:I = 0x7f0d03a3

.field public static final hovering_image_height:I = 0x7f0d039e

.field public static final hovering_image_offset_from_original_thumbnail:I = 0x7f0d039f

.field public static final hovering_image_set_height:I = 0x7f0d03a1

.field public static final hovering_image_set_offset_from_original_thumbnail:I = 0x7f0d03a2

.field public static final hovering_image_set_width:I = 0x7f0d03a0

.field public static final hovering_image_width:I = 0x7f0d039d

.field public static final icon_bottom_margin:I = 0x7f0d0103

.field public static final icon_left_margin:I = 0x7f0d0102

.field public static final indicator_bar_width:I = 0x7f0d0013

.field public static final indicator_bar_width_large:I = 0x7f0d0029

.field public static final indicator_bar_width_xlarge:I = 0x7f0d0035

.field public static final item_h_gap:I = 0x7f0d00f6

.field public static final item_v_gap:I = 0x7f0d00f5

.field public static final list_item_header_height:I = 0x7f0d01fd

.field public static final list_item_header_margin_left:I = 0x7f0d01ff

.field public static final list_item_header_text_size:I = 0x7f0d01fe

.field public static final list_item_secondary_text_size:I = 0x7f0d0201

.field public static final list_item_title_text_size:I = 0x7f0d0200

.field public static final list_row_height:I = 0x7f0d0223

.field public static final location_hover_offset:I = 0x7f0d012a

.field public static final magazine_widget_camera_button_right_margin:I = 0x7f0d0326

.field public static final magazine_widget_camera_button_top_margin:I = 0x7f0d0325

.field public static final magazine_widget_factory_line_text_size:I = 0x7f0d032c

.field public static final magazine_widget_factory_title_text_size:I = 0x7f0d032b

.field public static final magazine_widget_setting_button_right_margin:I = 0x7f0d0327

.field public static final magazine_widget_title_left_margin:I = 0x7f0d0329

.field public static final magazine_widget_title_text_size:I = 0x7f0d032a

.field public static final magazine_widget_title_top_margin:I = 0x7f0d0328

.field public static final manage_cache_bottom_height:I = 0x7f0d0072

.field public static final mapview_marker_bitmap_height:I = 0x7f0d02f5

.field public static final mapview_marker_bitmap_width:I = 0x7f0d02f4

.field public static final mapview_marker_img_height:I = 0x7f0d02ea

.field public static final mapview_marker_img_width:I = 0x7f0d02e9

.field public static final mapview_marker_item_count_height:I = 0x7f0d02ef

.field public static final mapview_marker_item_count_margin_left:I = 0x7f0d02f1

.field public static final mapview_marker_item_count_margin_right:I = 0x7f0d02f0

.field public static final mapview_marker_item_count_margin_top:I = 0x7f0d02f2

.field public static final mapview_marker_item_count_text_size:I = 0x7f0d02f3

.field public static final mapview_marker_representative_img_height:I = 0x7f0d02ec

.field public static final mapview_marker_representative_img_margin_left:I = 0x7f0d02ed

.field public static final mapview_marker_representative_img_margin_top:I = 0x7f0d02ee

.field public static final mapview_marker_representative_img_width:I = 0x7f0d02eb

.field public static final moreInfo_item_content_min_height:I = 0x7f0d008c

.field public static final moreInfo_item_icon_margin_Right:I = 0x7f0d0087

.field public static final moreInfo_item_icon_margin_left:I = 0x7f0d0086

.field public static final moreInfo_item_icon_margin_top:I = 0x7f0d0085

.field public static final moreInfo_item_icon_width:I = 0x7f0d0084

.field public static final moreInfo_item_title_height:I = 0x7f0d0088

.field public static final moreInfo_item_title_margin_top:I = 0x7f0d0089

.field public static final moreInfo_item_title_padding_bottom:I = 0x7f0d008a

.field public static final moreInfo_main_padding_top:I = 0x7f0d007d

.field public static final moreInfo_usertag_editview_item_content_margin_bottom:I = 0x7f0d00a4

.field public static final moreInfo_usertag_editview_item_content_margin_top:I = 0x7f0d00a3

.field public static final more_space_height:I = 0x7f0d01f3

.field public static final moreinfo_body_divider_padding:I = 0x7f0d0082

.field public static final moreinfo_body_padding_left:I = 0x7f0d007f

.field public static final moreinfo_body_padding_left_portrait:I = 0x7f0d0081

.field public static final moreinfo_body_padding_right:I = 0x7f0d007e

.field public static final moreinfo_body_padding_right_portrait:I = 0x7f0d0080

.field public static final moreinfo_category_list_item_text_size:I = 0x7f0d0123

.field public static final moreinfo_details_button_height:I = 0x7f0d0094

.field public static final moreinfo_details_button_margin_bottom:I = 0x7f0d0099

.field public static final moreinfo_details_button_margin_left:I = 0x7f0d0096

.field public static final moreinfo_details_button_margin_right:I = 0x7f0d0097

.field public static final moreinfo_details_button_margin_top:I = 0x7f0d0098

.field public static final moreinfo_details_button_padding_buttom:I = 0x7f0d0095

.field public static final moreinfo_details_button_width:I = 0x7f0d0093

.field public static final moreinfo_details_header_padding_bottom:I = 0x7f0d00b9

.field public static final moreinfo_details_header_padding_right:I = 0x7f0d00b8

.field public static final moreinfo_details_header_padding_top:I = 0x7f0d00ba

.field public static final moreinfo_details_histogram_height:I = 0x7f0d00bc

.field public static final moreinfo_details_histogram_width:I = 0x7f0d00bb

.field public static final moreinfo_details_sperator_margin_top:I = 0x7f0d00bd

.field public static final moreinfo_edit_icon_margin_right:I = 0x7f0d009d

.field public static final moreinfo_edit_location_actionbar_search_width:I = 0x7f0d02dc

.field public static final moreinfo_edit_location_edittextview_width:I = 0x7f0d02d8

.field public static final moreinfo_edit_location_edittextview_width_land:I = 0x7f0d02da

.field public static final moreinfo_edit_location_gps_popup_txt_size:I = 0x7f0d02dd

.field public static final moreinfo_edit_location_searchview_width:I = 0x7f0d02d9

.field public static final moreinfo_edit_location_searchview_width_land:I = 0x7f0d02db

.field public static final moreinfo_expandable_item_button_height:I = 0x7f0d009b

.field public static final moreinfo_expandable_item_button_margin_top:I = 0x7f0d009c

.field public static final moreinfo_expandable_subitem_height:I = 0x7f0d009a

.field public static final moreinfo_expandablelayout_horizontal_gap:I = 0x7f0d00b4

.field public static final moreinfo_expandablelayout_vertical_gap:I = 0x7f0d00b5

.field public static final moreinfo_item_content_margin_bottom:I = 0x7f0d008e

.field public static final moreinfo_item_content_padding_bottom:I = 0x7f0d008d

.field public static final moreinfo_item_content_text_size:I = 0x7f0d0122

.field public static final moreinfo_item_googlemap_height:I = 0x7f0d008f

.field public static final moreinfo_item_googlemap_margin_bottom:I = 0x7f0d0091

.field public static final moreinfo_item_googlemap_margin_top:I = 0x7f0d0090

.field public static final moreinfo_item_googlemap_padding_right:I = 0x7f0d0092

.field public static final moreinfo_item_title_addcontent_margin_top:I = 0x7f0d008b

.field public static final moreinfo_item_title_text_size:I = 0x7f0d0121

.field public static final moreinfo_set_padding_right:I = 0x7f0d0083

.field public static final moreinfo_usertag_addbtn_padding_left:I = 0x7f0d00a6

.field public static final moreinfo_usertag_addbtn_padding_right:I = 0x7f0d00a5

.field public static final moreinfo_usertag_edit_Text_margin_left:I = 0x7f0d00af

.field public static final moreinfo_usertag_edit_Text_margin_right:I = 0x7f0d00b0

.field public static final moreinfo_usertag_edit_addbtn_height:I = 0x7f0d00ac

.field public static final moreinfo_usertag_edit_addbtn_margin_left:I = 0x7f0d00ae

.field public static final moreinfo_usertag_edit_addbtn_margin_right:I = 0x7f0d00ad

.field public static final moreinfo_usertag_edit_addbtn_width:I = 0x7f0d00ab

.field public static final moreinfo_usertag_edit_edittextbox_height:I = 0x7f0d00a9

.field public static final moreinfo_usertag_edit_edittextbox_margin:I = 0x7f0d00aa

.field public static final moreinfo_usertag_edit_edittextbox_popup_height:I = 0x7f0d00a8

.field public static final moreinfo_usertag_edit_edittextset_padding_bottom:I = 0x7f0d00b2

.field public static final moreinfo_usertag_edit_edittextset_padding_left:I = 0x7f0d00b1

.field public static final moreinfo_usertag_edit_edittextset_padding_top:I = 0x7f0d00c5

.field public static final moreinfo_usertag_edit_listitem_height:I = 0x7f0d00b3

.field public static final moreinfo_usertag_editview_header_height:I = 0x7f0d00a2

.field public static final moreinfo_usertag_editview_header_padding_left:I = 0x7f0d00a0

.field public static final moreinfo_usertag_editview_header_padding_right:I = 0x7f0d00a1

.field public static final moreinfo_usertag_editview_padding_top:I = 0x7f0d009f

.field public static final moreinfo_usertag_item_edit_expandableset_margin_top:I = 0x7f0d00b6

.field public static final moreinfo_usertag_item_edit_textset_margin_bottom:I = 0x7f0d00b7

.field public static final moreinfo_usertag_list_header_margin_top:I = 0x7f0d00a7

.field public static final moreinfo_usertag_list_item_text_size:I = 0x7f0d0124

.field public static final moreinfo_usetag_edit_textset_margin_bootm:I = 0x7f0d009e

.field public static final motion_sensitivity_height:I = 0x7f0d01e5

.field public static final motion_sensitivity_horizontal_padding:I = 0x7f0d01e8

.field public static final motion_sensitivity_margin_left_land:I = 0x7f0d01e6

.field public static final motion_sensitivity_margin_left_port:I = 0x7f0d01e7

.field public static final motion_sensitivity_margin_top:I = 0x7f0d01e9

.field public static final motion_sensitivity_progress_margin:I = 0x7f0d01ee

.field public static final motion_sensitivity_sensitivity_text_font_size:I = 0x7f0d01eb

.field public static final motion_sensitivity_sensitivity_text_margin:I = 0x7f0d01ec

.field public static final motion_sensitivity_sensitivity_text_width:I = 0x7f0d01ea

.field public static final motion_sensitivity_speed_text_font_size:I = 0x7f0d01ed

.field public static final music_space_height:I = 0x7f0d01f2

.field public static final mw_bottom_margin_landscape:I = 0x7f0d012e

.field public static final mw_bottom_margin_landscape_max:I = 0x7f0d0130

.field public static final mw_bottom_margin_landscape_max_needed:I = 0x7f0d0132

.field public static final mw_bottom_margin_portrait:I = 0x7f0d012d

.field public static final mw_bottom_margin_portrait_max:I = 0x7f0d012f

.field public static final mw_bottom_margin_portrait_max_needed:I = 0x7f0d0131

.field public static final name_tag_text_size:I = 0x7f0d03da

.field public static final navigation_bar_height:I = 0x7f0d0058

.field public static final navigation_bar_width:I = 0x7f0d0059

.field public static final newmark_height:I = 0x7f0d015a

.field public static final newmark_right_margin:I = 0x7f0d0159

.field public static final newmark_text_top_margin:I = 0x7f0d015c

.field public static final newmark_top_margin:I = 0x7f0d0158

.field public static final newmark_width:I = 0x7f0d015b

.field public static final onscreen_exposure_indicator_text_size:I = 0x7f0d0020

.field public static final onscreen_exposure_indicator_text_size_xlarge:I = 0x7f0d0042

.field public static final onscreen_indicators_height:I = 0x7f0d001f

.field public static final onscreen_indicators_height_large:I = 0x7f0d002b

.field public static final onscreen_indicators_height_xlarge:I = 0x7f0d0041

.field public static final option_divider_padding:I = 0x7f0d0238

.field public static final pano_mosaic_surface_height:I = 0x7f0d0008

.field public static final pano_mosaic_surface_height_xlarge:I = 0x7f0d002c

.field public static final pano_review_button_height:I = 0x7f0d000a

.field public static final pano_review_button_height_xlarge:I = 0x7f0d002e

.field public static final pano_review_button_width:I = 0x7f0d0009

.field public static final pano_review_button_width_xlarge:I = 0x7f0d002d

.field public static final parallel_hwidth_margin:I = 0x7f0d00f1

.field public static final photo_imageview_bottom_margin:I = 0x7f0d00fc

.field public static final photo_imageview_icon_bottom_margin:I = 0x7f0d0104

.field public static final photo_imageview_icon_gap:I = 0x7f0d0105

.field public static final photo_imageview_icon_hight:I = 0x7f0d0107

.field public static final photo_imageview_icon_left_margin:I = 0x7f0d0101

.field public static final photo_imageview_icon_width:I = 0x7f0d0106

.field public static final photo_imageview_left_margin:I = 0x7f0d00f9

.field public static final photo_imageview_multishot_bottom_margin:I = 0x7f0d0100

.field public static final photo_imageview_multishot_left_margin:I = 0x7f0d00fd

.field public static final photo_imageview_multishot_right_margin:I = 0x7f0d00ff

.field public static final photo_imageview_multishot_top_margin:I = 0x7f0d00fe

.field public static final photo_imageview_right_margin:I = 0x7f0d00fb

.field public static final photo_imageview_top_margin:I = 0x7f0d00fa

.field public static final photo_imageview_unknowicon_bottom_margin:I = 0x7f0d0108

.field public static final photo_note_acton_button_padding:I = 0x7f0d0143

.field public static final photo_note_button_text_margin:I = 0x7f0d0144

.field public static final photo_note_top_margin:I = 0x7f0d0135

.field public static final photo_time_Bottom_margin:I = 0x7f0d0308

.field public static final photo_time_Left_margin:I = 0x7f0d0309

.field public static final photo_time_Right_margin:I = 0x7f0d030a

.field public static final photo_time_Top_margin:I = 0x7f0d0307

.field public static final photo_time_checkbox_left_margin:I = 0x7f0d0304

.field public static final photo_time_checkbox_top_margin:I = 0x7f0d0305

.field public static final photo_time_expand_button_h_margin:I = 0x7f0d030b

.field public static final photo_time_expand_button_v_margin:I = 0x7f0d030c

.field public static final photo_time_group_checkbox_top_margin:I = 0x7f0d0306

.field public static final photoeditor_original_text_margin:I = 0x7f0d0078

.field public static final photoeditor_original_text_size:I = 0x7f0d0077

.field public static final photoeditor_text_padding:I = 0x7f0d0076

.field public static final photoeditor_text_size:I = 0x7f0d0075

.field public static final photoset_image_bottom_margin:I = 0x7f0d00f7

.field public static final photoset_typeicon_bottom_margin:I = 0x7f0d00f8

.field public static final photoview_default_text_size:I = 0x7f0d011d

.field public static final photoview_taken_time_text_offsetY:I = 0x7f0d011f

.field public static final photoview_taken_time_text_size:I = 0x7f0d011e

.field public static final photoview_title_text_size:I = 0x7f0d0120

.field public static final pie_anglezone_width:I = 0x7f0d004c

.field public static final pie_arc_offset:I = 0x7f0d0048

.field public static final pie_arc_radius:I = 0x7f0d004a

.field public static final pie_deadzone_width:I = 0x7f0d004b

.field public static final pie_item_radius:I = 0x7f0d0049

.field public static final pie_radius_increment:I = 0x7f0d0044

.field public static final pie_radius_start:I = 0x7f0d0043

.field public static final pie_touch_offset:I = 0x7f0d0046

.field public static final pie_touch_slop:I = 0x7f0d0045

.field public static final pie_view_size:I = 0x7f0d0047

.field public static final play_speed_dialog_title_left_padding:I = 0x7f0d0227

.field public static final polaroid_bottom_margin:I = 0x7f0d0137

.field public static final polaroid_frame_bottom_margin:I = 0x7f0d0140

.field public static final polaroid_frame_frame_crop_margin:I = 0x7f0d0142

.field public static final polaroid_frame_hint_popup_top_margin:I = 0x7f0d0141

.field public static final polaroid_frame_left_margin:I = 0x7f0d013d

.field public static final polaroid_frame_right_margin:I = 0x7f0d013e

.field public static final polaroid_frame_top_margin:I = 0x7f0d013f

.field public static final polaroid_height_landscape:I = 0x7f0d013a

.field public static final polaroid_height_portrait:I = 0x7f0d0139

.field public static final polaroid_image_height:I = 0x7f0d013c

.field public static final polaroid_image_width:I = 0x7f0d013b

.field public static final polaroid_top_margin:I = 0x7f0d0136

.field public static final polaroid_width:I = 0x7f0d0138

.field public static final popup_height:I = 0x7f0d01fc

.field public static final popup_item_text_size:I = 0x7f0d03d9

.field public static final popup_menu_divide_line_left_margin:I = 0x7f0d03ca

.field public static final popup_menu_divide_line_right_margin:I = 0x7f0d03cb

.field public static final popup_menu_divide_line_thickness:I = 0x7f0d03c9

.field public static final popup_menu_item_height:I = 0x7f0d03c1

.field public static final popup_menu_item_left_margin:I = 0x7f0d03c3

.field public static final popup_menu_item_pressed_focus_off_y:I = 0x7f0d03c5

.field public static final popup_menu_item_right_margin:I = 0x7f0d03c4

.field public static final popup_menu_item_text_left_margin:I = 0x7f0d03c2

.field public static final popup_menu_space_height:I = 0x7f0d03c7

.field public static final popup_menu_str_off_y:I = 0x7f0d03cc

.field public static final popup_menu_top_margin:I = 0x7f0d03c8

.field public static final popup_menu_width:I = 0x7f0d03c6

.field public static final popup_title_frame_min_height:I = 0x7f0d0015

.field public static final popup_title_frame_min_height_xlarge:I = 0x7f0d0037

.field public static final popup_title_text_size:I = 0x7f0d0014

.field public static final popup_title_text_size_xlarge:I = 0x7f0d0036

.field public static final popup_width:I = 0x7f0d014a

.field public static final preview_image_center_margin:I = 0x7f0d0241

.field public static final preview_image_height:I = 0x7f0d0242

.field public static final preview_margin:I = 0x7f0d0000

.field public static final quick_center_popup_margin_top:I = 0x7f0d03bc

.field public static final quick_center_popup_text_bottom_padding:I = 0x7f0d03ba

.field public static final quick_center_popup_text_top_margin:I = 0x7f0d03bb

.field public static final quick_center_popup_width:I = 0x7f0d02e8

.field public static final quick_effect_option_height:I = 0x7f0d0126

.field public static final quick_effect_option_padding:I = 0x7f0d0127

.field public static final quick_effect_option_width:I = 0x7f0d0125

.field public static final quick_scroll_center_font_size:I = 0x7f0d03be

.field public static final quick_scroll_cetner_popup_height:I = 0x7f0d03bf

.field public static final quick_scroll_cetner_popup_width:I = 0x7f0d03c0

.field public static final quick_scroll_font_size:I = 0x7f0d03bd

.field public static final quick_scroll_height:I = 0x7f0d03b7

.field public static final quick_scroll_help_body_top_margin:I = 0x7f0d0339

.field public static final quick_scroll_help_close_bottom_margin:I = 0x7f0d033a

.field public static final quick_scroll_help_close_bottom_margin_multiwindow:I = 0x7f0d033b

.field public static final quick_scroll_help_text_left_margin:I = 0x7f0d0335

.field public static final quick_scroll_help_text_left_margin_multiwindow:I = 0x7f0d0336

.field public static final quick_scroll_help_text_top_margin:I = 0x7f0d0333

.field public static final quick_scroll_help_text_top_margin_multiwindow:I = 0x7f0d0334

.field public static final quick_scroll_help_text_width:I = 0x7f0d0337

.field public static final quick_scroll_help_text_width_multiwindow:I = 0x7f0d0338

.field public static final quick_scroll_popup_padding:I = 0x7f0d03b8

.field public static final quick_scroll_popup_text_bottom_padding:I = 0x7f0d03b9

.field public static final quick_scroll_width:I = 0x7f0d03b6

.field public static final quick_time_popup_top_margin:I = 0x7f0d02e7

.field public static final resume_button_margin_top:I = 0x7f0d0215

.field public static final screen_mirroring_dialog_checkbox_bottom_margin:I = 0x7f0d00c3

.field public static final screen_mirroring_dialog_checkbox_left_margin:I = 0x7f0d00c2

.field public static final screen_mirroring_dialog_checkbox_text_padding:I = 0x7f0d00c4

.field public static final screen_mirroring_dialog_text_bottom_margin:I = 0x7f0d00c1

.field public static final screen_mirroring_dialog_text_left_margin:I = 0x7f0d00bf

.field public static final screen_mirroring_dialog_text_top_margin:I = 0x7f0d00c0

.field public static final seekbar_height:I = 0x7f0d03eb

.field public static final seekbar_padding_horizontal:I = 0x7f0d03ec

.field public static final seekbar_padding_vertical:I = 0x7f0d03ed

.field public static final seekbar_width:I = 0x7f0d03ea

.field public static final select_count_view_height:I = 0x7f0d02d5

.field public static final select_count_view_width_n_five:I = 0x7f0d02d4

.field public static final select_count_view_width_n_four:I = 0x7f0d02d3

.field public static final select_count_view_width_n_less_two:I = 0x7f0d02d1

.field public static final select_count_view_width_n_three:I = 0x7f0d02d2

.field public static final select_mode_checkbox_left_margin:I = 0x7f0d0156

.field public static final select_mode_checkbox_top_margin:I = 0x7f0d0155

.field public static final selection_mode_checkbox_margin:I = 0x7f0d02e1

.field public static final selection_mode_layout_height:I = 0x7f0d02e0

.field public static final selection_mode_title_size:I = 0x7f0d02e2

.field public static final setting_item_icon_width:I = 0x7f0d0017

.field public static final setting_item_icon_width_large:I = 0x7f0d002a

.field public static final setting_item_icon_width_xlarge:I = 0x7f0d0039

.field public static final setting_item_list_margin:I = 0x7f0d0012

.field public static final setting_item_list_margin_xlarge:I = 0x7f0d0034

.field public static final setting_item_text_size:I = 0x7f0d000d

.field public static final setting_item_text_size_xlarge:I = 0x7f0d0030

.field public static final setting_item_text_width:I = 0x7f0d0010

.field public static final setting_item_text_width_xlarge:I = 0x7f0d0032

.field public static final setting_knob_text_size:I = 0x7f0d000f

.field public static final setting_knob_width:I = 0x7f0d000e

.field public static final setting_knob_width_xlarge:I = 0x7f0d0031

.field public static final setting_list_item_height:I = 0x7f0d01e2

.field public static final setting_main_divider_width:I = 0x7f0d0250

.field public static final setting_main_frag_list_item_left_padding:I = 0x7f0d024c

.field public static final setting_main_frag_list_item_margin:I = 0x7f0d024b

.field public static final setting_main_frag_list_item_right_padding:I = 0x7f0d024d

.field public static final setting_main_frag_list_item_text_size:I = 0x7f0d024e

.field public static final setting_main_fragment_layout_padding:I = 0x7f0d024f

.field public static final setting_main_screen_grid_item_padding_bottom:I = 0x7f0d01e0

.field public static final setting_main_screen_grid_item_padding_top:I = 0x7f0d01e1

.field public static final setting_main_screen_section_height:I = 0x7f0d01de

.field public static final setting_main_window_item_text_size:I = 0x7f0d01df

.field public static final setting_motion_action_bar_height:I = 0x7f0d01e4

.field public static final setting_page_fragment_side_padding:I = 0x7f0d01e3

.field public static final setting_popup_right_margin:I = 0x7f0d000b

.field public static final setting_popup_right_margin_large:I = 0x7f0d0026

.field public static final setting_popup_window_width:I = 0x7f0d0011

.field public static final setting_popup_window_width_large:I = 0x7f0d0028

.field public static final setting_popup_window_width_xlarge:I = 0x7f0d0033

.field public static final setting_row_height:I = 0x7f0d000c

.field public static final setting_row_height_large:I = 0x7f0d0027

.field public static final setting_row_height_xlarge:I = 0x7f0d002f

.field public static final setting_view_indicator_PenPosition:I = 0x7f0d0145

.field public static final setting_view_indicator_RemoverPosition:I = 0x7f0d0146

.field public static final setting_view_margin_Top:I = 0x7f0d0147

.field public static final shadow_margin:I = 0x7f0d0001

.field public static final share_via_list_item_height:I = 0x7f0d02fa

.field public static final share_via_list_item_textsize:I = 0x7f0d02fb

.field public static final share_via_list_max_height:I = 0x7f0d02f7

.field public static final share_via_list_max_height_for_tablet:I = 0x7f0d02f8

.field public static final share_via_list_width:I = 0x7f0d02f9

.field public static final share_via_location_y:I = 0x7f0d02f6

.field public static final shutter_offset:I = 0x7f0d0055

.field public static final sidemirror_bg_width:I = 0x7f0d011b

.field public static final sidemirror_bg_width_land:I = 0x7f0d011c

.field public static final sidemirror_bottom_margin:I = 0x7f0d0118

.field public static final sidemirror_thumb_height:I = 0x7f0d011a

.field public static final sidemirror_thumb_width:I = 0x7f0d0119

.field public static final sidemirror_top_margin:I = 0x7f0d0117

.field public static final single_item_divider_height:I = 0x7f0d01f9

.field public static final single_item_height:I = 0x7f0d01f4

.field public static final single_item_padding_left:I = 0x7f0d01f7

.field public static final single_item_padding_right:I = 0x7f0d01f8

.field public static final single_item_padding_top_bottom:I = 0x7f0d01f6

.field public static final single_item_text_margin:I = 0x7f0d01fa

.field public static final single_item_text_size:I = 0x7f0d01fb

.field public static final size_preview:I = 0x7f0d0057

.field public static final size_thumbnail:I = 0x7f0d0056

.field public static final slideshow_music_item_padding_left_right:I = 0x7f0d01f5

.field public static final slideshow_settings_effect_title_height:I = 0x7f0d0207

.field public static final slideshow_settings_effect_title_margin_left_right:I = 0x7f0d0208

.field public static final slideshow_settings_filter_image_height:I = 0x7f0d020b

.field public static final slideshow_settings_filter_image_width:I = 0x7f0d0209

.field public static final slideshow_settings_filter_image_width_land:I = 0x7f0d020a

.field public static final slideshow_settings_filter_layout_margin_bottom:I = 0x7f0d0210

.field public static final slideshow_settings_filter_layout_margin_left:I = 0x7f0d0213

.field public static final slideshow_settings_filter_layout_margin_left_first_item:I = 0x7f0d0212

.field public static final slideshow_settings_filter_layout_margin_right:I = 0x7f0d0214

.field public static final slideshow_settings_filter_layout_margin_top:I = 0x7f0d0211

.field public static final slideshow_settings_filter_scroller_padding_left:I = 0x7f0d020c

.field public static final slideshow_settings_filter_scroller_padding_top:I = 0x7f0d020d

.field public static final slideshow_settings_layout_effect_margin_top:I = 0x7f0d0206

.field public static final slideshow_settings_list_divider_margin_left_right:I = 0x7f0d020f

.field public static final slideshow_settings_music_titleview_height:I = 0x7f0d020e

.field public static final slideshow_settings_tab_min_height_size:I = 0x7f0d0203

.field public static final slideshow_speed_settings_list_height:I = 0x7f0d0204

.field public static final slideshow_transitioneffect_settings_list_height:I = 0x7f0d0205

.field public static final sns_comment_dialog_land_height:I = 0x7f0d0233

.field public static final sns_comment_dialog_land_listview_height:I = 0x7f0d0235

.field public static final sns_comment_dialog_land_width:I = 0x7f0d0232

.field public static final sns_comment_dialog_port_height:I = 0x7f0d0231

.field public static final sns_comment_dialog_port_listview_height:I = 0x7f0d0234

.field public static final sns_comment_dialog_port_width:I = 0x7f0d0230

.field public static final sns_comment_dialog_tab_height:I = 0x7f0d0237

.field public static final sns_comment_dialog_tab_width:I = 0x7f0d0236

.field public static final sns_head_width:I = 0x7f0d03d8

.field public static final sns_height:I = 0x7f0d03d6

.field public static final sns_icon_padding:I = 0x7f0d022d

.field public static final sns_width:I = 0x7f0d03d4

.field public static final sns_width_chn:I = 0x7f0d03d5

.field public static final sns_with_syn_info_height:I = 0x7f0d03d7

.field public static final splitview_album_left_margin:I = 0x7f0d00e7

.field public static final splitview_album_time_left_margin:I = 0x7f0d00e8

.field public static final splitview_album_top_margin:I = 0x7f0d00e6

.field public static final splitview_item_gap_height:I = 0x7f0d00ee

.field public static final splitview_item_size_scale:I = 0x7f0d00f0

.field public static final splitview_photo_title_bottom_margin:I = 0x7f0d00ed

.field public static final splitview_photo_title_height:I = 0x7f0d00eb

.field public static final splitview_photo_title_text_size:I = 0x7f0d00ef

.field public static final splitview_photo_title_top_margin:I = 0x7f0d00ea

.field public static final splitview_photo_title_width:I = 0x7f0d00ec

.field public static final splitview_photo_top_margin:I = 0x7f0d00e9

.field public static final stack_photo_height:I = 0x7f0d0063

.field public static final stack_photo_width:I = 0x7f0d0062

.field public static final switch_min_width:I = 0x7f0d0022

.field public static final switch_padding:I = 0x7f0d0021

.field public static final switch_text_max_width:I = 0x7f0d0023

.field public static final switcher_size:I = 0x7f0d0052

.field public static final tab_widget_height:I = 0x7f0d01f0

.field public static final table_row_top_bottom_padding:I = 0x7f0d0245

.field public static final table_row_top_margin:I = 0x7f0d0244

.field public static final tag_bps_height:I = 0x7f0d03d3

.field public static final tag_head_height:I = 0x7f0d03d2

.field public static final tag_head_left_margin:I = 0x7f0d03d0

.field public static final tag_scrollview_horizontal_content_margin_bottom:I = 0x7f0d0296

.field public static final tag_scrollview_horizontal_content_margin_top:I = 0x7f0d0295

.field public static final tag_scrollview_horizontal_icon_margin_bottom:I = 0x7f0d0292

.field public static final tag_scrollview_horizontal_icon_margin_right:I = 0x7f0d0293

.field public static final tag_scrollview_horizontal_icon_margin_top:I = 0x7f0d0294

.field public static final tag_scrollview_horizontal_margin_left:I = 0x7f0d0290

.field public static final tag_scrollview_horizontal_margin_right:I = 0x7f0d0291

.field public static final tag_scrollview_vertical_content_margin_bottom:I = 0x7f0d028f

.field public static final tag_scrollview_vertical_content_margin_left:I = 0x7f0d028d

.field public static final tag_scrollview_vertical_content_margin_right:I = 0x7f0d028e

.field public static final tag_scrollview_vertical_title_height:I = 0x7f0d0285

.field public static final tag_scrollview_vertical_title_icon_margin_left:I = 0x7f0d0289

.field public static final tag_scrollview_vertical_title_icon_margin_right:I = 0x7f0d028a

.field public static final tag_scrollview_vertical_title_margin_bottom:I = 0x7f0d0288

.field public static final tag_scrollview_vertical_title_margin_left:I = 0x7f0d0286

.field public static final tag_scrollview_vertical_title_margin_right:I = 0x7f0d0287

.field public static final tag_scrollview_vertical_title_text_margin_left:I = 0x7f0d028b

.field public static final tag_scrollview_vertical_title_text_size:I = 0x7f0d028c

.field public static final tag_space_width:I = 0x7f0d03ce

.field public static final tag_text_off_y:I = 0x7f0d03cf

.field public static final tag_text_top_margin:I = 0x7f0d03cd

.field public static final tag_up_position_off_y:I = 0x7f0d03d1

.field public static final tagcloud_item_height:I = 0x7f0d007b

.field public static final tagcloud_item_margin:I = 0x7f0d007a

.field public static final tagcloud_item_padding:I = 0x7f0d0079

.field public static final tagcloud_item_vertical_padding:I = 0x7f0d007c

.field public static final thumb_text_padding:I = 0x7f0d0024

.field public static final thumb_text_size:I = 0x7f0d0025

.field public static final thumbnail_view_camera_icon_margin_left:I = 0x7f0d0157

.field public static final thumbnail_view_icon_left_padding:I = 0x7f0d038e

.field public static final thumbnail_view_new_album_font_size:I = 0x7f0d0396

.field public static final thumbnail_view_new_album_font_size_ru:I = 0x7f0d0395

.field public static final thumbnail_view_new_album_hand_padding:I = 0x7f0d0392

.field public static final thumbnail_view_new_album_hand_size:I = 0x7f0d0391

.field public static final thumbnail_view_new_album_header_padding_left:I = 0x7f0d0394

.field public static final thumbnail_view_new_album_header_padding_top:I = 0x7f0d0393

.field public static final thumbnail_view_new_album_select_count_font_size:I = 0x7f0d039a

.field public static final thumbnail_view_new_album_select_count_hpadding_right:I = 0x7f0d039c

.field public static final thumbnail_view_new_album_select_count_vpadding_top:I = 0x7f0d039b

.field public static final thumbnail_view_new_album_text_view_bottom_padding:I = 0x7f0d0398

.field public static final thumbnail_view_new_album_text_view_padding:I = 0x7f0d0397

.field public static final thumbnail_view_new_album_text_view_top_margin:I = 0x7f0d0399

.field public static final thumbnail_view_split_album_count_font_size:I = 0x7f0d0388

.field public static final thumbnail_view_split_album_font_size:I = 0x7f0d0387

.field public static final thumbnail_view_split_album_new_font_size:I = 0x7f0d0385

.field public static final thumbnail_view_split_album_new_padding:I = 0x7f0d0386

.field public static final thumbnail_view_split_album_title_mw_padding_left:I = 0x7f0d038a

.field public static final thumbnail_view_split_album_title_padding_left:I = 0x7f0d0389

.field public static final thumbnail_view_split_album_title_rtl_padding_right:I = 0x7f0d038b

.field public static final thumbnail_view_split_focused_arrow_height:I = 0x7f0d038d

.field public static final thumbnail_view_split_focused_arrow_width:I = 0x7f0d038c

.field public static final time_folder_count_font_size:I = 0x7f0d0390

.field public static final time_folder_font_size:I = 0x7f0d038f

.field public static final time_view_easy_scroll_popup_txt_size:I = 0x7f0d0324

.field public static final timeline_tag_font_size:I = 0x7f0d00f4

.field public static final timeline_tag_height:I = 0x7f0d00f3

.field public static final timeline_tag_width:I = 0x7f0d00f2

.field public static final timeview_title_margin_bottom:I = 0x7f0d0320

.field public static final timeview_title_margin_left:I = 0x7f0d0321

.field public static final timeview_title_margin_top:I = 0x7f0d031f

.field public static final timeview_title_text_size:I = 0x7f0d0322

.field public static final timeview_title_text_size_easy_mode:I = 0x7f0d0323

.field public static final tw_alert_dialog_text_margin_left:I = 0x7f0d032d

.field public static final tw_alert_dialog_text_margin_right:I = 0x7f0d032e

.field public static final tw_checkbox_text_gap:I = 0x7f0d032f

.field public static final user_tag_cloud_view_margin_top:I = 0x7f0d0283

.field public static final user_tag_cloud_view_margin_top_land:I = 0x7f0d0284

.field public static final winset_dialog_item_height:I = 0x7f0d021b

.field public static final winset_dialog_title_height:I = 0x7f0d021c

.field public static final winset_dialog_title_text_size:I = 0x7f0d021d

.field public static final wp_selector_dash_length:I = 0x7f0d0004

.field public static final wp_selector_off_length:I = 0x7f0d0005

.field public static final zoom_font_size:I = 0x7f0d0054

.field public static final zoom_ring_min:I = 0x7f0d0051


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
