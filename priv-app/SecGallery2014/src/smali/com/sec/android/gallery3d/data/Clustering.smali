.class public abstract Lcom/sec/android/gallery3d/data/Clustering;
.super Ljava/lang/Object;
.source "Clustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Clustering"


# instance fields
.field protected mSortByType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method private enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V
    .locals 8
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "total"    # I
    .param p4, "useFakeloading"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    if-nez p1, :cond_0

    .line 161
    const-string v6, "Clustering"

    const-string v7, "mediaSet is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :goto_0
    return-void

    .line 165
    :cond_0
    instance-of v6, p1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-eqz v6, :cond_3

    .line 166
    const/4 v4, 0x0

    .line 167
    .local v4, "imageCursor":Landroid/database/Cursor;
    const/4 v5, 0x0

    .line 169
    .local v5, "videoCursor":Landroid/database/Cursor;
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    move-object v1, v0

    .line 170
    .local v1, "albumset":Lcom/sec/android/gallery3d/data/LocalAlbumSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getAlbumSetType()I

    move-result v6

    iget v7, p0, Lcom/sec/android/gallery3d/data/Clustering;->mSortByType:I

    invoke-virtual {v1, v6, v7, p4}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTotalMediaItems(IIZ)[Landroid/database/Cursor;

    move-result-object v3

    .line 171
    .local v3, "cursors":[Landroid/database/Cursor;
    if-eqz v3, :cond_2

    .line 172
    const/4 v6, 0x0

    aget-object v4, v3, v6

    .line 173
    const/4 v6, 0x1

    aget-object v5, v3, v6

    .line 174
    if-eqz v4, :cond_1

    .line 175
    const/4 v6, 0x1

    invoke-virtual {p0, v4, p2, v6}, Lcom/sec/android/gallery3d/data/Clustering;->buildCursorToSmallItem(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V

    .line 177
    :cond_1
    if-eqz v5, :cond_2

    .line 178
    const/4 v6, 0x0

    invoke-virtual {p0, v5, p2, v6}, Lcom/sec/android/gallery3d/data/Clustering;->buildCursorToSmallItem(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    :cond_2
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 183
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 182
    .end local v1    # "albumset":Lcom/sec/android/gallery3d/data/LocalAlbumSet;
    .end local v3    # "cursors":[Landroid/database/Cursor;
    :catchall_0
    move-exception v6

    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 183
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v6

    .line 185
    .end local v4    # "imageCursor":Landroid/database/Cursor;
    .end local v5    # "videoCursor":Landroid/database/Cursor;
    :cond_3
    instance-of v6, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;

    if-eqz v6, :cond_5

    .line 186
    const/4 v2, 0x0

    .line 188
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_1
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;

    move-object v1, v0

    .line 189
    .local v1, "albumset":Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->getTotalMediaItems(I)Landroid/database/Cursor;

    move-result-object v2

    .line 190
    if-eqz v2, :cond_4

    .line 191
    const/4 v6, 0x1

    invoke-virtual {p0, v2, p2, v6}, Lcom/sec/android/gallery3d/data/Clustering;->buildCursorToSmallPicasaItem(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 194
    :cond_4
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v1    # "albumset":Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;
    :catchall_1
    move-exception v6

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v6

    .line 196
    .end local v2    # "cursor":Landroid/database/Cursor;
    :cond_5
    instance-of v6, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;

    if-eqz v6, :cond_8

    .line 197
    const/4 v4, 0x0

    .line 198
    .restart local v4    # "imageCursor":Landroid/database/Cursor;
    const/4 v5, 0x0

    .line 200
    .restart local v5    # "videoCursor":Landroid/database/Cursor;
    :try_start_2
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;

    move-object v1, v0

    .line 201
    .local v1, "albumset":Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->getTotalMediaItems(I)[Landroid/database/Cursor;

    move-result-object v3

    .line 202
    .restart local v3    # "cursors":[Landroid/database/Cursor;
    if-eqz v3, :cond_7

    .line 203
    const/4 v6, 0x0

    aget-object v4, v3, v6

    .line 204
    const/4 v6, 0x1

    aget-object v5, v3, v6

    .line 205
    if-eqz v4, :cond_6

    .line 206
    const/4 v6, 0x1

    invoke-virtual {p0, v4, p2, v6}, Lcom/sec/android/gallery3d/data/Clustering;->buildCursorToSmallItemForCloud(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V

    .line 208
    :cond_6
    if-eqz v5, :cond_7

    .line 209
    const/4 v6, 0x0

    invoke-virtual {p0, v5, p2, v6}, Lcom/sec/android/gallery3d/data/Clustering;->buildCursorToSmallItemForCloud(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 213
    :cond_7
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 214
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 213
    .end local v1    # "albumset":Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;
    .end local v3    # "cursors":[Landroid/database/Cursor;
    :catchall_2
    move-exception v6

    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 214
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v6

    .line 216
    .end local v4    # "imageCursor":Landroid/database/Cursor;
    .end local v5    # "videoCursor":Landroid/database/Cursor;
    :cond_8
    instance-of v6, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;

    if-eqz v6, :cond_b

    .line 217
    const/4 v4, 0x0

    .line 218
    .restart local v4    # "imageCursor":Landroid/database/Cursor;
    const/4 v5, 0x0

    .line 220
    .restart local v5    # "videoCursor":Landroid/database/Cursor;
    :try_start_3
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;

    move-object v1, v0

    .line 221
    .local v1, "albumset":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbumSet;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v6

    invoke-virtual {v1, v6, p4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->getTotalMediaItems(IZ)[Landroid/database/Cursor;

    move-result-object v3

    .line 222
    .restart local v3    # "cursors":[Landroid/database/Cursor;
    if-eqz v3, :cond_a

    .line 223
    const/4 v6, 0x0

    aget-object v4, v3, v6

    .line 224
    const/4 v6, 0x1

    aget-object v5, v3, v6

    .line 225
    if-eqz v4, :cond_9

    .line 226
    const/4 v6, 0x1

    invoke-virtual {p0, v4, p2, v6}, Lcom/sec/android/gallery3d/data/Clustering;->buildCursorToSmallItemForTCloud(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V

    .line 228
    :cond_9
    if-eqz v5, :cond_a

    .line 229
    const/4 v6, 0x0

    invoke-virtual {p0, v5, p2, v6}, Lcom/sec/android/gallery3d/data/Clustering;->buildCursorToSmallItemForTCloud(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 233
    :cond_a
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 234
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 233
    .end local v1    # "albumset":Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;
    .end local v3    # "cursors":[Landroid/database/Cursor;
    :catchall_3
    move-exception v6

    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 234
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v6

    .line 237
    .end local v4    # "imageCursor":Landroid/database/Cursor;
    .end local v5    # "videoCursor":Landroid/database/Cursor;
    :cond_b
    new-instance v6, Lcom/sec/android/gallery3d/data/Clustering$1;

    invoke-direct {v6, p0, p3, p2}, Lcom/sec/android/gallery3d/data/Clustering$1;-><init>(Lcom/sec/android/gallery3d/data/Clustering;ILjava/util/ArrayList;)V

    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 133
    if-eqz p1, :cond_2

    .line 134
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 135
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 136
    .local v0, "id":I
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 137
    .local v2, "mediaType":I
    if-ne v2, v4, :cond_0

    move v1, v4

    .line 138
    .local v1, "isImage":Z
    :goto_1
    if-eqz v1, :cond_1

    sget-object v3, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 139
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_2
    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v1    # "isImage":Z
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    move v1, v5

    .line 137
    goto :goto_1

    .line 138
    .restart local v1    # "isImage":Z
    :cond_1
    sget-object v3, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_2

    .line 142
    .end local v0    # "id":I
    .end local v1    # "isImage":Z
    .end local v2    # "mediaType":I
    :cond_2
    return-void
.end method

.method public buildCursorToSmallItem(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p2, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    if-eqz p1, :cond_1

    .line 72
    if-eqz p3, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 73
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 74
    new-instance v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;-><init>(Lcom/sec/android/gallery3d/data/Clustering;)V

    .line 75
    .local v2, "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 76
    .local v0, "id":I
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 77
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    .line 78
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    .line 79
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 80
    iget-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->latRad:D

    .line 81
    iget-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lngRad:D

    .line 82
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    .end local v0    # "id":I
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v2    # "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 85
    :cond_1
    return-void
.end method

.method public buildCursorToSmallItemForCloud(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p2, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    if-eqz p1, :cond_1

    .line 104
    if-eqz p3, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 105
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 106
    new-instance v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;-><init>(Lcom/sec/android/gallery3d/data/Clustering;)V

    .line 107
    .local v2, "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 108
    .local v0, "id":I
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 109
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    .line 110
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    .line 111
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 112
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    .end local v0    # "id":I
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v2    # "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 115
    :cond_1
    return-void
.end method

.method public buildCursorToSmallItemForTCloud(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p2, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const-wide/16 v6, 0x0

    .line 118
    if-eqz p1, :cond_1

    .line 119
    if-eqz p3, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 120
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 121
    new-instance v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;-><init>(Lcom/sec/android/gallery3d/data/Clustering;)V

    .line 122
    .local v2, "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 123
    .local v0, "id":I
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 124
    iput-wide v6, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    .line 125
    iput-wide v6, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    .line 126
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 127
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    .end local v0    # "id":I
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v2    # "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 130
    :cond_1
    return-void
.end method

.method public buildCursorToSmallPicasaItem(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 89
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 90
    new-instance v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;-><init>(Lcom/sec/android/gallery3d/data/Clustering;)V

    .line 91
    .local v3, "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 92
    .local v0, "id":J
    invoke-virtual {v2, v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 93
    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    iput-wide v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    .line 94
    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    iput-wide v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    .line 95
    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 96
    iget-wide v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lat:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iput-wide v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->latRad:D

    .line 97
    iget-wide v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lng:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iput-wide v4, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->lngRad:D

    .line 98
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    .end local v0    # "id":J
    .end local v3    # "s":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    :cond_0
    return-void
.end method

.method public enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V
    .locals 4
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "total"    # I
    .param p4, "useFakeloading"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    instance-of v3, p1, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    if-nez v3, :cond_1

    .line 147
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/Clustering;->enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V

    .line 156
    .end local p1    # "baseSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void

    .line 151
    .restart local p1    # "baseSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    check-cast p1, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    .end local p1    # "baseSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->getMediaSets()[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 152
    .local v2, "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 153
    aget-object v1, v2, v0

    .line 154
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-direct {p0, v1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/Clustering;->enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public abstract getCluster(I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end method

.method public getClusterCover(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 48
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getClusterKey(I)Ljava/lang/String;
.end method

.method public abstract getClusterName(I)Ljava/lang/String;
.end method

.method public getClusterNameID(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getNumberOfClusters()I
.end method

.method public onCancel()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public abstract run(Lcom/sec/android/gallery3d/data/MediaSet;)V
.end method
