.class Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$2;
.super Ljava/lang/Object;
.source "AbstractGalleryActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$2;->this$0:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 404
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/sec/android/gallery3d/app/BatchService$LocalBinder;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$2;->this$0:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast p2, Lcom/sec/android/gallery3d/app/BatchService$LocalBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/app/BatchService$LocalBinder;->getService()Lcom/sec/android/gallery3d/app/BatchService;

    move-result-object v1

    # setter for: Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchService:Lcom/sec/android/gallery3d/app/BatchService;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->access$002(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/BatchService;)Lcom/sec/android/gallery3d/app/BatchService;

    .line 406
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$2;->this$0:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchService:Lcom/sec/android/gallery3d/app/BatchService;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->access$002(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/BatchService;)Lcom/sec/android/gallery3d/app/BatchService;

    .line 411
    return-void
.end method
