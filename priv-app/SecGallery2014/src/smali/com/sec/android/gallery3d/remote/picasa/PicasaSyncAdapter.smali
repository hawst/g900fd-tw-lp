.class public Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "PicasaSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter$AccountChangeReceiver;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "PicasaSyncAdapter"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 41
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "providerClient"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 48
    const-string v9, "initialize"

    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 50
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v9

    const-string v10, "com.google"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "service_lh2"

    aput-object v13, v11, v12

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v9

    invoke-interface {v9}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/accounts/Account;

    .line 55
    .local v8, "picasaAccounts":[Landroid/accounts/Account;
    const/4 v5, 0x0

    .line 56
    .local v5, "isPicasaAccount":Z
    move-object v2, v8

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v7, v2, v4

    .line 57
    .local v7, "picasaAccount":Landroid/accounts/Account;
    invoke-virtual {p1, v7}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 58
    const/4 v5, 0x1

    .line 62
    .end local v7    # "picasaAccount":Landroid/accounts/Account;
    :cond_0
    if-eqz v5, :cond_1

    .line 63
    const/4 v9, 0x1

    move-object/from16 v0, p3

    invoke-static {p1, v0, v9}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 64
    const/4 v9, 0x1

    move-object/from16 v0, p3

    invoke-static {p1, v0, v9}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 89
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "isPicasaAccount":Z
    .end local v6    # "len$":I
    .end local v8    # "picasaAccounts":[Landroid/accounts/Account;
    :cond_1
    :goto_1
    return-void

    .line 56
    .restart local v2    # "arr$":[Landroid/accounts/Account;
    .restart local v4    # "i$":I
    .restart local v5    # "isPicasaAccount":Z
    .restart local v6    # "len$":I
    .restart local v7    # "picasaAccount":Landroid/accounts/Account;
    .restart local v8    # "picasaAccounts":[Landroid/accounts/Account;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 78
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "isPicasaAccount":Z
    .end local v6    # "len$":I
    .end local v7    # "picasaAccount":Landroid/accounts/Account;
    .end local v8    # "picasaAccounts":[Landroid/accounts/Account;
    :cond_3
    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 79
    const-string v9, "PicasaSyncAdapter"

    const-string v10, "picasa sync is not available by settings"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 84
    :cond_4
    :try_start_1
    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {v9, p1, v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->performSync(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 85
    :catch_0
    move-exception v3

    .line 87
    .local v3, "e":Ljava/lang/Exception;
    move-object/from16 v0, p5

    iget-object v9, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v9, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v9, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_1

    .line 72
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v9

    goto :goto_1

    .line 70
    :catch_2
    move-exception v9

    goto :goto_1

    .line 68
    :catch_3
    move-exception v9

    goto :goto_1

    .line 66
    :catch_4
    move-exception v9

    goto :goto_1
.end method
