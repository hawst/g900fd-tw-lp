.class public interface abstract Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry$AlbumColumns;
.super Ljava/lang/Object;
.source "RemoteAlbumEntry.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/RemoteBaseEntry$Column;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/RemoteAlbumEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AlbumColumns"
.end annotation


# static fields
.field public static final CACHE_FLAG:Ljava/lang/String; = "cache_flag"

.field public static final CACHE_STATUS:Ljava/lang/String; = "cache_status"

.field public static final DATE_EDITED:Ljava/lang/String; = "date_edited"

.field public static final NUM_PHOTOS:Ljava/lang/String; = "num_photos"

.field public static final PHOTOS_DIRTY:Ljava/lang/String; = "photos_dirty"

.field public static final SOURCE_ID:Ljava/lang/String; = "source_id"

.field public static final THUMBNAIL_URL:Ljava/lang/String; = "thumbnail_url"

.field public static final TITLE:Ljava/lang/String; = "title"
