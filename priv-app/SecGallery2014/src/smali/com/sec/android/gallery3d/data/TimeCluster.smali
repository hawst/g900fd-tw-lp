.class Lcom/sec/android/gallery3d/data/TimeCluster;
.super Ljava/lang/Object;
.source "TimeAlbumClustering.java"


# static fields
.field private static final MMDDYY_FORMAT_DAY:Ljava/lang/String; = "dd"

.field private static final MMDDYY_FORMAT_DAY_MONTH:Ljava/lang/String; = "dd MMM"

.field private static final MMDDYY_FORMAT_MONTH:Ljava/lang/String; = "MMM"

.field private static final MMDDYY_FORMAT_MONTH_YEAR:Ljava/lang/String; = "MMM yyyy"

.field private static final MMDDYY_FORMAT_YEAR:Ljava/lang/String; = "yyyy"

.field private static final TAG:Ljava/lang/String; = "Cluster"


# instance fields
.field private mInsertToFirst:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 697
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    .line 700
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mInsertToFirst:Z

    .line 701
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "insertToFirst"    # Z

    .prologue
    .line 703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 697
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    .line 704
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mInsertToFirst:Z

    .line 705
    return-void
.end method


# virtual methods
.method public addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 720
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mInsertToFirst:Z

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 724
    :goto_0
    return-void

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public generateCaption(Landroid/content/Context;I)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .prologue
    .line 745
    const-string v9, ""

    .line 747
    .local v9, "caption":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 748
    .local v15, "n":I
    const-wide/16 v4, 0x0

    .line 749
    .local v4, "minTimestamp":J
    const-wide/16 v6, 0x0

    .line 751
    .local v6, "maxTimestamp":J
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    if-ge v11, v15, :cond_2

    .line 752
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-wide v0, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    move-wide/from16 v16, v0

    .line 753
    .local v16, "t":J
    const-wide/16 v20, 0x0

    cmp-long v3, v16, v20

    if-nez v3, :cond_0

    .line 751
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 754
    :cond_0
    const-wide/16 v20, 0x0

    cmp-long v3, v4, v20

    if-nez v3, :cond_1

    .line 755
    move-wide/from16 v6, v16

    move-wide/from16 v4, v16

    goto :goto_1

    .line 757
    :cond_1
    move-wide/from16 v0, v16

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 758
    move-wide/from16 v0, v16

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_1

    .line 761
    .end local v16    # "t":J
    :cond_2
    packed-switch p2, :pswitch_data_0

    .line 812
    :goto_2
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/sec/android/gallery3d/data/TimeCluster;->mName:Ljava/lang/String;

    .line 814
    return-void

    .line 764
    :pswitch_0
    const v3, 0x10010

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v9

    .line 766
    goto :goto_2

    .line 770
    :pswitch_1
    const-string v3, "dd MMM"

    invoke-static {v3, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    .line 771
    .local v13, "minDay":Ljava/lang/String;
    const-string v3, "dd MMM"

    invoke-static {v3, v6, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    .line 773
    .local v12, "maxDay":Ljava/lang/String;
    const/4 v3, 0x4

    invoke-virtual {v13, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const/4 v8, 0x4

    invoke-virtual {v12, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 774
    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 775
    const v3, 0x10010

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 777
    :cond_3
    const v8, 0x10010

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 780
    :cond_4
    const v8, 0x10010

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 787
    .end local v12    # "maxDay":Ljava/lang/String;
    .end local v13    # "minDay":Ljava/lang/String;
    :pswitch_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 788
    .local v2, "c":Ljava/util/Calendar;
    const-string v3, "MMM yyyy"

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-static {v3, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    .line 789
    .local v19, "todaymonth":Ljava/lang/String;
    const-string v3, "MMM yyyy"

    invoke-static {v3, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    .line 791
    .local v18, "thisMonth":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x4

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 792
    const v3, 0x10028

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 795
    :cond_5
    const v3, 0x10024

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 803
    .end local v2    # "c":Ljava/util/Calendar;
    .end local v18    # "thisMonth":Ljava/lang/String;
    .end local v19    # "todaymonth":Ljava/lang/String;
    :pswitch_3
    const v3, 0x10024

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v10

    .line 805
    .local v10, "fullDate":Ljava/lang/String;
    const v3, 0x10028

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v14

    .line 807
    .local v14, "monthDate":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v10, v14, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 808
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 761
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 740
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLastItem()Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .locals 3

    .prologue
    .line 735
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 736
    .local v0, "n":I
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    goto :goto_0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 712
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public setAddToFirst(Z)V
    .locals 0
    .param p1, "insertToFirst"    # Z

    .prologue
    .line 708
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mInsertToFirst:Z

    .line 709
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 716
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mName:Ljava/lang/String;

    .line 717
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
