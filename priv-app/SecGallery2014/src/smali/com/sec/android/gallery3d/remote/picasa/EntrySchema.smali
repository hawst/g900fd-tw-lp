.class public final Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;
.super Ljava/lang/Object;
.source "EntrySchema.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    }
.end annotation


# static fields
.field private static final FULL_TEXT_INDEX_SUFFIX:Ljava/lang/String; = "_fulltext"

.field public static final SQLITE_TYPES:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SchemaInfo"

.field public static final TYPE_BLOB:I = 0x7

.field public static final TYPE_BOOLEAN:I = 0x1

.field public static final TYPE_DOUBLE:I = 0x6

.field public static final TYPE_FLOAT:I = 0x5

.field public static final TYPE_INT:I = 0x3

.field public static final TYPE_LONG:I = 0x4

.field public static final TYPE_SHORT:I = 0x2

.field public static final TYPE_STRING:I


# instance fields
.field private final mColumnInfo:[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

.field private final mHasFullTextIndex:Z

.field private final mProjection:[Ljava/lang/String;

.field private final mTableName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TEXT"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "INTEGER"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "INTEGER"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "INTEGER"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "INTEGER"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "REAL"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "REAL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NONE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->SQLITE_TYPES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/remote/picasa/Entry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/remote/picasa/Entry;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->parseColumnInfo(Ljava/lang/Class;)[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    move-result-object v1

    .line 50
    .local v1, "columns":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->parseTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mColumnInfo:[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    .line 54
    const/4 v5, 0x0

    new-array v4, v5, [Ljava/lang/String;

    .line 55
    .local v4, "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 56
    .local v2, "hasFullTextIndex":Z
    if-eqz v1, :cond_1

    .line 57
    array-length v5, v1

    new-array v4, v5, [Ljava/lang/String;

    .line 58
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v1

    if-eq v3, v5, :cond_1

    .line 59
    aget-object v0, v1, v3

    .line 60
    .local v0, "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    aput-object v5, v4, v3

    .line 61
    iget-boolean v5, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->fullText:Z

    if-eqz v5, :cond_0

    .line 62
    const/4 v2, 0x1

    .line 58
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 66
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v3    # "i":I
    :cond_1
    iput-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mProjection:[Ljava/lang/String;

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mHasFullTextIndex:Z

    .line 68
    return-void
.end method

.method private logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "sql"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method private parseColumnInfo(Ljava/lang/Class;)[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Object;",
            ">;)[",
            "Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;"
        }
    .end annotation

    .prologue
    .line 409
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 410
    .local v8, "columns":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v10

    .line 411
    .local v10, "fields":[Ljava/lang/reflect/Field;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    array-length v0, v10

    if-eq v11, v0, :cond_9

    .line 413
    aget-object v5, v10, v11

    .line 414
    .local v5, "field":Ljava/lang/reflect/Field;
    const-class v0, Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;

    invoke-interface {v5, v0}, Ljava/lang/reflect/AnnotatedElement;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;

    .line 415
    .local v12, "info":Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
    if-nez v12, :cond_0

    .line 411
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 421
    :cond_0
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v9

    .line 422
    .local v9, "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v0, Ljava/lang/String;

    if-ne v9, v0, :cond_1

    .line 423
    const/4 v2, 0x0

    .line 443
    .local v2, "type":I
    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 444
    .local v6, "index":I
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    invoke-interface {v12}, Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;->value()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v12}, Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;->indexed()Z

    move-result v3

    invoke-interface {v12}, Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;->fullText()Z

    move-result v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;-><init>(Ljava/lang/String;IZZLjava/lang/reflect/Field;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 424
    .end local v2    # "type":I
    .end local v6    # "index":I
    :cond_1
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v9, v0, :cond_2

    .line 425
    const/4 v2, 0x1

    .restart local v2    # "type":I
    goto :goto_2

    .line 426
    .end local v2    # "type":I
    :cond_2
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-ne v9, v0, :cond_3

    .line 427
    const/4 v2, 0x2

    .restart local v2    # "type":I
    goto :goto_2

    .line 428
    .end local v2    # "type":I
    :cond_3
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v9, v0, :cond_4

    .line 429
    const/4 v2, 0x3

    .restart local v2    # "type":I
    goto :goto_2

    .line 430
    .end local v2    # "type":I
    :cond_4
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v9, v0, :cond_5

    .line 431
    const/4 v2, 0x4

    .restart local v2    # "type":I
    goto :goto_2

    .line 432
    .end local v2    # "type":I
    :cond_5
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v9, v0, :cond_6

    .line 433
    const/4 v2, 0x5

    .restart local v2    # "type":I
    goto :goto_2

    .line 434
    .end local v2    # "type":I
    :cond_6
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v9, v0, :cond_7

    .line 435
    const/4 v2, 0x6

    .restart local v2    # "type":I
    goto :goto_2

    .line 436
    .end local v2    # "type":I
    :cond_7
    const-class v0, [B

    if-ne v9, v0, :cond_8

    .line 437
    const/4 v2, 0x7

    .restart local v2    # "type":I
    goto :goto_2

    .line 439
    .end local v2    # "type":I
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported field type for column: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    .end local v5    # "field":Ljava/lang/reflect/Field;
    .end local v9    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v12    # "info":Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
    :cond_9
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v7, v0, [Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    .line 449
    .local v7, "columnList":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 450
    return-object v7
.end method

.method private parseTableName(Ljava/lang/Class;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 398
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-class v1, Lcom/sec/android/gallery3d/remote/picasa/Entry$Table;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/Entry$Table;

    .line 399
    .local v0, "table":Lcom/sec/android/gallery3d/remote/picasa/Entry$Table;
    if-nez v0, :cond_0

    .line 400
    const/4 v1, 0x0

    .line 404
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lcom/sec/android/gallery3d/remote/picasa/Entry$Table;->value()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 231
    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    .line 232
    .local v9, "tableName":Ljava/lang/String;
    if-nez v9, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "CREATE TABLE "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 238
    .local v8, "sql":Ljava/lang/StringBuilder;
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    const-string v10, " (_id INTEGER PRIMARY KEY"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mColumnInfo:[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    .line 241
    .local v2, "columns":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    array-length v7, v2

    .line 242
    .local v7, "numColumns":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-eq v4, v7, :cond_3

    .line 243
    aget-object v0, v2, v4

    .line 244
    .local v0, "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->isId()Z

    move-result v10

    if-nez v10, :cond_2

    .line 245
    const/16 v10, 0x2c

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 246
    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const/16 v10, 0x20

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 248
    sget-object v10, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->SQLITE_TYPES:[Ljava/lang/String;

    iget v11, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->type:I

    aget-object v10, v10, v11

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v10, ""

    if-eqz v10, :cond_2

    .line 250
    const/16 v10, 0x20

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 251
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v10, ""

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 255
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :cond_3
    const-string v10, ");"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 257
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 260
    const/4 v4, 0x0

    :goto_2
    if-eq v4, v7, :cond_5

    .line 262
    aget-object v0, v2, v4

    .line 263
    .restart local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget-boolean v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->indexed:Z

    if-eqz v10, :cond_4

    .line 264
    const-string v10, "CREATE INDEX "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const-string v10, "_index_"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string v10, " ON "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v10, " ("

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string v10, ");"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 274
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 260
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 278
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :cond_5
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mHasFullTextIndex:Z

    if-eqz v10, :cond_0

    .line 280
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_fulltext"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 281
    .local v3, "ftsTableName":Ljava/lang/String;
    const-string v10, "CREATE VIRTUAL TABLE "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    const-string v10, " USING FTS3 (_id INTEGER PRIMARY KEY"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const/4 v4, 0x0

    :goto_3
    if-eq v4, v7, :cond_7

    .line 285
    aget-object v0, v2, v4

    .line 286
    .restart local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget-boolean v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->fullText:Z

    if-eqz v10, :cond_6

    .line 288
    iget-object v1, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    .line 289
    .local v1, "columnName":Ljava/lang/String;
    const/16 v10, 0x2c

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 290
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const-string v10, " TEXT"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    .end local v1    # "columnName":Ljava/lang/String;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 294
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :cond_7
    const-string v10, ");"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 296
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 300
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v10, "INSERT OR REPLACE INTO "

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 301
    .local v5, "insertSql":Ljava/lang/StringBuilder;
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    const-string v10, " (_id"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const/4 v4, 0x0

    :goto_4
    if-eq v4, v7, :cond_9

    .line 304
    aget-object v0, v2, v4

    .line 305
    .restart local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget-boolean v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->fullText:Z

    if-eqz v10, :cond_8

    .line 306
    const/16 v10, 0x2c

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 307
    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 310
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :cond_9
    const-string v10, ") VALUES (new._id"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const/4 v4, 0x0

    :goto_5
    if-eq v4, v7, :cond_b

    .line 312
    aget-object v0, v2, v4

    .line 313
    .restart local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget-boolean v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->fullText:Z

    if-eqz v10, :cond_a

    .line 314
    const-string v10, ",new."

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    iget-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 318
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :cond_b
    const-string v10, ");"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 322
    .local v6, "insertSqlString":Ljava/lang/String;
    const-string v10, "CREATE TRIGGER "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    const-string v10, "_insert_trigger AFTER INSERT ON "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    const-string v10, " FOR EACH ROW BEGIN "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    const-string v10, "END;"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 330
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 333
    const-string v10, "CREATE TRIGGER "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    const-string v10, "_update_trigger AFTER UPDATE ON "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    const-string v10, " FOR EACH ROW BEGIN "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    const-string v10, "END;"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 341
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 344
    const-string v10, "CREATE TRIGGER "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    const-string v10, "_delete_trigger AFTER DELETE ON "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    const-string v10, " FOR EACH ROW BEGIN DELETE FROM "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    const-string v10, " WHERE _id = old._id; END;"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 352
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    goto/16 :goto_0
.end method

.method public cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/sec/android/gallery3d/remote/picasa/Entry;",
            ">(",
            "Landroid/database/Cursor;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .local p2, "object":Lcom/sec/android/gallery3d/remote/picasa/Entry;, "TT;"
    const/4 v8, 0x1

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mColumnInfo:[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v1, v0, v5

    .line 99
    .local v1, "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget v2, v1, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->projectionIndex:I

    .line 100
    .local v2, "columnIndex":I
    iget-object v4, v1, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->field:Ljava/lang/reflect/Field;

    .line 101
    .local v4, "field":Ljava/lang/reflect/Field;
    iget v7, v1, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->type:I

    packed-switch v7, :pswitch_data_0

    .line 98
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 103
    :pswitch_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, p2, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 130
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v1    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v2    # "columnIndex":I
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :catch_0
    move-exception v3

    .line 131
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    const-string v7, "SchemaInfo"

    const-string v8, "SchemaInfo.setFromCursor: object not of the right type"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    :goto_2
    return-object p2

    .line 106
    .restart local v0    # "arr$":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v1    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v2    # "columnIndex":I
    .restart local v4    # "field":Ljava/lang/reflect/Field;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    :pswitch_1
    :try_start_1
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v7

    if-ne v7, v8, :cond_1

    move v7, v8

    :goto_3
    invoke-virtual {v4, p2, v7}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 132
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v1    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v2    # "columnIndex":I
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :catch_1
    move-exception v3

    .line 133
    .local v3, "e":Ljava/lang/IllegalAccessException;
    const-string v7, "SchemaInfo"

    const-string v8, "SchemaInfo.setFromCursor: field not accessible"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 106
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    .restart local v0    # "arr$":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v1    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v2    # "columnIndex":I
    .restart local v4    # "field":Ljava/lang/reflect/Field;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    :cond_1
    const/4 v7, 0x0

    goto :goto_3

    .line 109
    :pswitch_2
    :try_start_2
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v7

    invoke-virtual {v4, p2, v7}, Ljava/lang/reflect/Field;->setShort(Ljava/lang/Object;S)V

    goto :goto_1

    .line 112
    :pswitch_3
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v4, p2, v7}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    goto :goto_1

    .line 115
    :pswitch_4
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v4, p2, v10, v11}, Ljava/lang/reflect/Field;->setLong(Ljava/lang/Object;J)V

    goto :goto_1

    .line 118
    :pswitch_5
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v7

    invoke-virtual {v4, p2, v7}, Ljava/lang/reflect/Field;->setFloat(Ljava/lang/Object;F)V

    goto :goto_1

    .line 121
    :pswitch_6
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    invoke-virtual {v4, p2, v10, v11}, Ljava/lang/reflect/Field;->setDouble(Ljava/lang/Object;D)V

    goto :goto_1

    .line 124
    :pswitch_7
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    invoke-virtual {v4, p2, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 101
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public deleteAll(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DELETE FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 391
    .local v0, "sql":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 394
    return-void
.end method

.method public deleteWithId(Landroid/database/sqlite/SQLiteDatabase;J)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "id"    # J

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 226
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    const-string v3, "_id=?"

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/16 v3, 0x3b

    .line 357
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    .line 358
    .local v1, "tableName":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "DROP TABLE IF EXISTS "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 359
    .local v0, "sql":Ljava/lang/StringBuilder;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 361
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 362
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 364
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mHasFullTextIndex:Z

    if-eqz v2, :cond_0

    .line 365
    const-string v2, "DROP TABLE IF EXISTS "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    const-string v2, "_fulltext"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 369
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 371
    :cond_0
    return-void
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 5
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mColumnInfo:[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 84
    .local v1, "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget-object v4, v1, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    iget v4, v1, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->projectionIndex:I

    .line 88
    .end local v1    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :goto_1
    return v4

    .line 83
    .restart local v1    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 88
    .end local v1    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public getColumnInfo()[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mColumnInfo:[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    return-object v0
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mProjection:[Ljava/lang/String;

    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    return-object v0
.end method

.method public insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/Entry;)J
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .prologue
    .line 209
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 210
    .local v1, "values":Landroid/content/ContentValues;
    invoke-virtual {p0, p2, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->objectToValues(Lcom/sec/android/gallery3d/remote/picasa/Entry;Landroid/content/ContentValues;)V

    .line 211
    iget-wide v4, p2, Lcom/sec/android/gallery3d/remote/picasa/Entry;->id:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 212
    const-string v4, "SchemaInfo"

    const-string v5, "removing id before insert"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const-string v4, "_id"

    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 215
    :cond_0
    const-wide/16 v2, 0x0

    .line 217
    .local v2, "id":J
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    const-string v5, "_id"

    invoke-virtual {p1, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_0
    iput-wide v2, p2, Lcom/sec/android/gallery3d/remote/picasa/Entry;->id:J

    .line 222
    return-wide v2

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SchemaInfo"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertOrReplace:: Exception... values : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public objectToValues(Lcom/sec/android/gallery3d/remote/picasa/Entry;Landroid/content/ContentValues;)V
    .locals 10
    .param p1, "object"    # Lcom/sec/android/gallery3d/remote/picasa/Entry;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 140
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mColumnInfo:[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;

    .line 141
    .local v2, "columns":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    const/4 v5, 0x0

    .local v5, "i":I
    array-length v6, v2

    .local v6, "size":I
    :goto_0
    if-eq v5, v6, :cond_0

    .line 142
    aget-object v0, v2, v5

    .line 143
    .local v0, "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    iget-object v1, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->name:Ljava/lang/String;

    .line 144
    .local v1, "columnName":Ljava/lang/String;
    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->field:Ljava/lang/reflect/Field;

    .line 145
    .local v4, "field":Ljava/lang/reflect/Field;
    iget v7, v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;->type:I

    packed-switch v7, :pswitch_data_0

    .line 141
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 147
    :pswitch_0
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 175
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v1    # "columnName":Ljava/lang/String;
    .end local v2    # "columns":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "i":I
    .end local v6    # "size":I
    :catch_0
    move-exception v3

    .line 176
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    const-string v7, "SchemaInfo"

    const-string v8, "SchemaInfo.setFromCursor: object not of the right type"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    :goto_2
    return-void

    .line 150
    .restart local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v1    # "columnName":Ljava/lang/String;
    .restart local v2    # "columns":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v4    # "field":Ljava/lang/reflect/Field;
    .restart local v5    # "i":I
    .restart local v6    # "size":I
    :pswitch_1
    :try_start_1
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 177
    .end local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v1    # "columnName":Ljava/lang/String;
    .end local v2    # "columns":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "i":I
    .end local v6    # "size":I
    :catch_1
    move-exception v3

    .line 178
    .local v3, "e":Ljava/lang/IllegalAccessException;
    const-string v7, "SchemaInfo"

    const-string v8, "SchemaInfo.setFromCursor: field not accessible"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 153
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    .restart local v0    # "column":Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v1    # "columnName":Ljava/lang/String;
    .restart local v2    # "columns":[Lcom/sec/android/gallery3d/remote/picasa/EntrySchema$ColumnInfo;
    .restart local v4    # "field":Ljava/lang/reflect/Field;
    .restart local v5    # "i":I
    .restart local v6    # "size":I
    :pswitch_2
    :try_start_2
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getShort(Ljava/lang/Object;)S

    move-result v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    goto :goto_1

    .line 156
    :pswitch_3
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 159
    :pswitch_4
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getLong(Ljava/lang/Object;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 162
    :pswitch_5
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getFloat(Ljava/lang/Object;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    goto :goto_1

    .line 165
    :pswitch_6
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getDouble(Ljava/lang/Object;)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    goto :goto_1

    .line 168
    :pswitch_7
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    check-cast v7, [B

    invoke-virtual {p2, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 145
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public queryAll(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 183
    const/4 v8, 0x0

    .line 186
    .local v8, "cur":Landroid/database/Cursor;
    if-eqz p1, :cond_0

    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 194
    :goto_0
    return-object v8

    .line 189
    :cond_0
    const-string v0, "SchemaInfo"

    const-string v1, "queryAll:: db is null....."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 191
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/sec/android/gallery3d/remote/picasa/Entry;)Z
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "id"    # J
    .param p4, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .prologue
    const/4 v5, 0x0

    .line 198
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mProjection:[Ljava/lang/String;

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 199
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 200
    .local v9, "success":Z
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0, v8, p4}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .line 202
    const/4 v9, 0x1

    .line 204
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 205
    return v9
.end method

.method public upgradeTable(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 374
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->mTableName:Ljava/lang/String;

    .line 375
    .local v1, "tableName":Ljava/lang/String;
    const-string v2, "albums"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 376
    const-string v2, "SchemaInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "upgrade : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 378
    .local v0, "sql":Ljava/lang/StringBuilder;
    const/16 v2, 0x5f

    if-ge p2, v2, :cond_0

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0    # "sql":Ljava/lang/StringBuilder;
    const-string v2, "ALTER TABLE "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 380
    .restart local v0    # "sql":Ljava/lang/StringBuilder;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    const-string v2, " ADD COLUMN "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    const-string/jumbo v2, "valid"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    const-string v2, " INTEGER DEFAULT 0;"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->logExecSql(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 387
    .end local v0    # "sql":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method
