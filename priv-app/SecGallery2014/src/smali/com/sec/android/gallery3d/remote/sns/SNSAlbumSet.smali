.class public Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "SNSAlbumSet.java"


# static fields
.field private static CACHED_PATH:[Ljava/lang/String; = null

.field private static final PHOTO_CACHED_FULL_WHERE_CLAUSE:Ljava/lang/String; = "cache_status=3"

.field private static final SUM_SIZE_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SNSAlbumSet"

.field private static WHERE:Ljava/lang/String;


# instance fields
.field private mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "sum(size)"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->SUM_SIZE_PROJECTION:[Ljava/lang/String;

    .line 45
    const-string v0, "album_id = ? AND cache_status >= ?"

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->WHERE:Ljava/lang/String;

    .line 47
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "cache_pathname"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->CACHED_PATH:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 65
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mType:I

    .line 66
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "Facebook"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mName:Ljava/lang/String;

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    const-string/jumbo v0, "sns Albumset"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mName:Ljava/lang/String;

    goto :goto_0
.end method

.method private static WHERE_CACHE_STATUS(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p0, "albumId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x2

    .line 49
    new-array v0, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static getAlbumEntry(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    .locals 9
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J

    .prologue
    const/4 v7, 0x0

    .line 172
    const/4 v6, 0x0

    .line 174
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 180
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    new-instance v7, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    invoke-direct {v7}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;-><init>()V

    .line 182
    .local v7, "table":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    invoke-virtual {v7, v6, v7}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .end local v7    # "table":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    :goto_0
    return-object v7

    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getChildCount(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 8
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 214
    const/4 v7, 0x0

    .line 216
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->CACHED_PATH:[Ljava/lang/String;

    sget-object v3, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->WHERE:Ljava/lang/String;

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->WHERE_CACHE_STATUS(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 219
    const/4 v6, 0x0

    .line 220
    .local v6, "count":I
    if-eqz v7, :cond_0

    .line 221
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 225
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    return v6

    .end local v6    # "count":I
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getTotalTargetCacheSize(Landroid/content/ContentResolver;)J
    .locals 2
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 210
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static getTotalUsedCacheSize(Landroid/content/ContentResolver;)J
    .locals 7
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 193
    const/4 v6, 0x0

    .line 195
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->SUM_SIZE_PROJECTION:[Ljava/lang/String;

    const-string v3, "cache_status=3"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 199
    if-eqz v6, :cond_0

    .line 200
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 201
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 205
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return-wide v0

    .line 203
    :cond_0
    const-wide/16 v0, 0x0

    .line 205
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 5
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "name":[Ljava/lang/String;
    array-length v2, v0

    if-ge v2, v1, :cond_0

    .line 76
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :cond_0
    const-string v2, "all"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "mergedall"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v1, 0x6

    .line 80
    :cond_2
    :goto_0
    return v1

    .line 79
    :cond_3
    const-string v2, "image"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mergedimage"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 80
    const-string/jumbo v1, "video"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "mergedvideo"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const/4 v1, 0x4

    goto :goto_0

    .line 81
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private loadSubMediaSets()Ljava/util/ArrayList;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    sget-object v4, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string/jumbo v5, "type"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mType:I

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getTypeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 125
    .local v3, "uri":Landroid/net/Uri;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v15, "loadBuffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/AlbumTable;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 127
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v11, 0x0

    .line 129
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->getProjection()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 131
    if-nez v11, :cond_1

    .line 132
    const-string v4, "SNSAlbumSet"

    const-string v5, "cannot open database"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 168
    :cond_0
    return-object v17

    .line 135
    :cond_1
    :goto_0
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 136
    new-instance v18, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;-><init>()V

    .line 137
    .local v18, "table":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    move-object/from16 v0, v18

    move-object/from16 v1, v18

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;

    .line 138
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->source_id:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getChildCount(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v9

    .line 140
    .local v9, "child":I
    if-lez v9, :cond_1

    .line 141
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    .end local v9    # "child":I
    .end local v18    # "table":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    :catchall_0
    move-exception v4

    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4

    :cond_2
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 148
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v17, "newAlbums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v12

    .line 151
    .local v12, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v14, 0x0

    .local v14, "i":I
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v16

    .local v16, "n":I
    :goto_1
    move/from16 v0, v16

    if-ge v14, v0, :cond_4

    .line 152
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;

    .line 153
    .local v13, "entry":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-wide v6, v13, Lcom/sec/android/gallery3d/remote/sns/AlbumTable;->_id:J

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    .line 154
    .local v10, "childPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v12, v10}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    .line 156
    .local v8, "album":Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;
    if-nez v8, :cond_3

    .line 157
    new-instance v8, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    .end local v8    # "album":Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mType:I

    invoke-direct {v8, v10, v4, v13, v5}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/sns/AlbumTable;I)V

    .line 161
    .restart local v8    # "album":Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;
    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 159
    :cond_3
    invoke-virtual {v8, v13}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->updateContent(Lcom/sec/android/gallery3d/remote/sns/AlbumTable;)V

    goto :goto_2

    .line 163
    .end local v8    # "album":Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;
    .end local v10    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v13    # "entry":Lcom/sec/android/gallery3d/remote/sns/AlbumTable;
    :cond_4
    sget-object v4, Lcom/sec/android/gallery3d/util/MediaSetUtils;->NAME_COMPARATOR:Ljava/util/Comparator;

    move-object/from16 v0, v17

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 165
    const/4 v14, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v16

    :goto_3
    move/from16 v0, v16

    if-ge v14, v0, :cond_0

    .line 166
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->reload()J

    .line 165
    add-int/lit8 v14, v14, 0x1

    goto :goto_3
.end method


# virtual methods
.method public getBucketId()I
    .locals 2

    .prologue
    .line 299
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 300
    sget v0, Lcom/sec/android/gallery3d/remote/sns/SNSMergeSet;->SNS_MERGESET_BUCKETID:I

    .line 304
    :goto_0
    return v0

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getBucketId()I

    move-result v0

    goto :goto_0

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->getBucketId()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 5
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 269
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v4, :cond_3

    .line 270
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 271
    .local v1, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .line 272
    .local v3, "totalItemCount":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 273
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    .line 274
    if-lt p1, v3, :cond_0

    .line 275
    sub-int/2addr p1, v3

    .line 276
    goto :goto_0

    .line 278
    :cond_0
    sub-int v4, v3, p1

    if-ge v4, p2, :cond_1

    .line 279
    sub-int v4, v3, p1

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 281
    sub-int v4, v3, p1

    sub-int/2addr p2, v4

    .line 282
    const/4 p1, 0x0

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 290
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "totalItemCount":I
    :cond_2
    :goto_1
    return-object v1

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1
.end method

.method public getMediaItemCount()I
    .locals 4

    .prologue
    .line 247
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v3, :cond_0

    .line 248
    const/4 v2, 0x0

    .line 249
    .local v2, "totalItemCount":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 250
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 251
    goto :goto_0

    .line 254
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "totalItemCount":I
    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v2

    :cond_1
    return v2
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 91
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->getSubMediaSetCount(Z)I

    move-result v0

    .line 94
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getSubMediaSetCount(Z)I
    .locals 1
    .param p1, "forMerge"    # Z

    .prologue
    .line 98
    if-eqz p1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 101
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 6

    .prologue
    .line 112
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/16 v2, 0x0

    .line 113
    .local v2, "supported":J
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 114
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v2, v4

    .line 115
    goto :goto_1

    .line 112
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "supported":J
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 116
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "supported":J
    :cond_1
    return-wide v2
.end method

.method public getTotalMediaItemCount()I
    .locals 4

    .prologue
    .line 260
    const/4 v0, 0x0

    .line 261
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 262
    .local v2, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 263
    goto :goto_0

    .line 264
    .end local v2    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return v0
.end method

.method public isAlbumSetEmpty()Z
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 311
    const/4 v0, 0x0

    .line 312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->loadSubMediaSets()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 233
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mDataVersion:J

    .line 235
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;->mDataVersion:J

    return-wide v0
.end method
