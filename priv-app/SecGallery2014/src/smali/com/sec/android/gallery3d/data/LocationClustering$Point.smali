.class Lcom/sec/android/gallery3d/data/LocationClustering$Point;
.super Ljava/lang/Object;
.source "LocationClustering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/LocationClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Point"
.end annotation


# instance fields
.field public latRad:D

.field public latitude:D

.field public lngRad:D

.field public lngitude:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 7
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {p1, p2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->latRad:D

    .line 67
    invoke-static {p3, p4}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocationClustering$Point;->lngRad:D

    .line 68
    move-wide v0, p1

    .line 69
    .local v0, "latitude":D
    move-wide v2, p3

    .line 70
    .local v2, "lngitude":D
    return-void
.end method
