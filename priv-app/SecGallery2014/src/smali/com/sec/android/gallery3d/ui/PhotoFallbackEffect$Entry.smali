.class public Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect$Entry;
.super Ljava/lang/Object;
.source "PhotoFallbackEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public dest:Landroid/graphics/Rect;

.field public index:I

.field public path:Lcom/sec/android/gallery3d/data/Path;

.field public source:Landroid/graphics/Rect;

.field public texture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "source"    # Landroid/graphics/Rect;
    .param p3, "texture"    # Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect$Entry;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 46
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect$Entry;->source:Landroid/graphics/Rect;

    .line 47
    iput-object p3, p0, Lcom/sec/android/gallery3d/ui/PhotoFallbackEffect$Entry;->texture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    .line 48
    return-void
.end method
