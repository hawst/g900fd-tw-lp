.class Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;
.super Ljava/lang/Object;
.source "NearbyClient.java"

# interfaces
.implements Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->changePlayer(ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetStateResponseReceived(Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;Lcom/samsung/android/allshare/ERROR;)V
    .locals 2
    .param p1, "arg0"    # Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    .param p2, "arg1"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 626
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onGetStateResponseReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    return-void
.end method

.method public onShowResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V
    .locals 5
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;
    .param p2, "contentInfo"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 585
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mIsViewerShowRequested:Z
    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1202(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 587
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShowResponseReceived : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$7;->$SwitchMap$com$samsung$android$allshare$ERROR:[I

    invoke-virtual {p3}, Lcom/samsung/android/allshare/ERROR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 617
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z
    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1102(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 618
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1800(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e012a

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 622
    :goto_0
    return-void

    .line 591
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z
    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1102(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 593
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerIcon()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    .line 595
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    move-result-object v1

    monitor-enter v1

    .line 596
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 598
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1500(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1600(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/ImageViewer;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1600(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1500(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->prepareItem(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/samsung/android/allshare/Item;
    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1700(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/samsung/android/allshare/Item;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/media/ImageViewer;->prepare(Lcom/samsung/android/allshare/Item;)V

    .line 603
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1502(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/sec/android/gallery3d/data/MediaSet;

    .line 604
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1602(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/data/MediaItem;

    .line 607
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 612
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z
    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1102(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 613
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1800(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0107

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 589
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onStopResponseReceived(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 577
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStopResponseReceived : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1102(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 580
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerIcon()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    .line 581
    return-void
.end method
