.class public Lcom/sec/android/gallery3d/data/LocalSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "LocalSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/LocalSource$1;,
        Lcom/sec/android/gallery3d/data/LocalSource$IdComparator;
    }
.end annotation


# static fields
.field public static final KEY_BUCKET_ID:Ljava/lang/String; = "bucketId"

.field private static final LOCAL_ALL_ALBUM:I = 0x7

.field private static final LOCAL_ALL_ALBUMSET:I = 0x6

.field private static final LOCAL_CATEGORY_ALBUM:I = 0xf

.field private static final LOCAL_CATEGORY_ALBUMSET:I = 0xe

.field private static final LOCAL_EVENT_ALBUMSET:I = 0x9

.field private static final LOCAL_EVENT_ALL_ALBUM:I = 0xc

.field private static final LOCAL_EVENT_IMAGE_ALBUM:I = 0xa

.field private static final LOCAL_EVENT_VIDEO_ALBUM:I = 0xb

.field private static final LOCAL_IMAGE_ALBUM:I = 0x2

.field private static final LOCAL_IMAGE_ALBUMSET:I = 0x0

.field private static final LOCAL_IMAGE_ITEM:I = 0x4

.field private static final LOCAL_LIST_ALBUM:I = 0xd

.field private static final LOCAL_SINGLE_ALBUMSET:I = 0x8

.field private static final LOCAL_VIDEO_ALBUM:I = 0x3

.field private static final LOCAL_VIDEO_ALBUMSET:I = 0x1

.field private static final LOCAL_VIDEO_ITEM:I = 0x5

.field private static final MEDIA_TYPE_ALL:I = 0x0

.field private static final MEDIA_TYPE_IMAGE:I = 0x1

.field private static final MEDIA_TYPE_VIDEO:I = 0x4

.field private static final NO_MATCH:I = -0x1

.field private static final TAG:Ljava/lang/String; = "LocalSource"

.field public static final sIdComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mClient:Landroid/content/ContentProviderClient;

.field private mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalSource$IdComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/LocalSource$IdComparator;-><init>(Lcom/sec/android/gallery3d/data/LocalSource$1;)V

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalSource;->sIdComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 8
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 73
    const-string v0, "local"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 42
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mUriMatcher:Landroid/content/UriMatcher;

    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 75
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/image"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/video"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/all"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/single/*/*"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/event/all"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/event/all/*"

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/event/image/*"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/event/video/*"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/categoryalbumset/*"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/categoryalbum/*"

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/image/*"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/video/*"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/all/*"

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/ListAlbum/*"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/image/item/*"

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/local/video/item/*"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "external/images/media/#"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "external/video/media/#"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "external/images/media"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "external/video/media"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "media"

    const-string v2, "external/file"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 107
    return-void
.end method

.method private getAlbumPath(Landroid/net/Uri;I)Lcom/sec/android/gallery3d/data/Path;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "defaultType"    # I

    .prologue
    .line 193
    const-string v4, "mediaTypes"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p2}, Lcom/sec/android/gallery3d/data/LocalSource;->getMediaType(Ljava/lang/String;I)I

    move-result v3

    .line 196
    .local v3, "mediaType":I
    const-string v4, "bucketId"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "bucketId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 199
    .local v2, "id":I
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 204
    packed-switch v3, :pswitch_data_0

    .line 210
    :pswitch_0
    const-string v4, "/local/all"

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    :goto_0
    return-object v4

    .line 200
    :catch_0
    move-exception v1

    .line 201
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v4, "LocalSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid bucket id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 202
    const/4 v4, 0x0

    goto :goto_0

    .line 206
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :pswitch_1
    const-string v4, "/local/image"

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    goto :goto_0

    .line 208
    :pswitch_2
    const-string v4, "/local/video"

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static getMediaType(Ljava/lang/String;I)I
    .locals 5
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "defaultType"    # I

    .prologue
    .line 176
    if-nez p0, :cond_1

    .line 184
    .end local p1    # "defaultType":I
    :cond_0
    :goto_0
    return p1

    .line 179
    .restart local p1    # "defaultType":I
    :cond_1
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 180
    .local v1, "value":I
    and-int/lit8 v2, v1, 0x5

    if-eqz v2, :cond_0

    move p1, v1

    goto :goto_0

    .line 181
    .end local v1    # "value":I
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "LocalSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V
    .locals 12
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    sget-object v10, Lcom/sec/android/gallery3d/data/LocalSource;->sIdComparator:Ljava/util/Comparator;

    invoke-static {p1, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 291
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 292
    .local v6, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_1

    .line 293
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 296
    .local v7, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .local v2, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v10, v7, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 298
    .local v9, "startId":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    add-int/lit8 v4, v1, 0x1

    .local v4, "j":I
    :goto_1
    if-ge v4, v6, :cond_0

    .line 302
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 303
    .local v8, "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v10, v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 304
    .local v0, "curId":I
    sub-int v10, v0, v9

    const/16 v11, 0x20

    if-lt v10, v11, :cond_2

    .line 310
    .end local v0    # "curId":I
    .end local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v10, p3, v2}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;ZLjava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 312
    .local v3, "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    move v5, v1

    .local v5, "k":I
    :goto_2
    if-ge v5, v4, :cond_4

    .line 313
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 314
    .restart local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget v10, v8, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->id:I

    sub-int v11, v5, v1

    aget-object v11, v3, v11

    invoke-interface {p2, v10, v11}, Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;->consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 320
    .end local v2    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v3    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "j":I
    .end local v5    # "k":I
    .end local v7    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .end local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .end local v9    # "startId":I
    :cond_1
    return-void

    .line 307
    .restart local v0    # "curId":I
    .restart local v2    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v4    # "j":I
    .restart local v7    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v9    # "startId":I
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 312
    .end local v0    # "curId":I
    .restart local v3    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v5    # "k":I
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 318
    .end local v8    # "pid2":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_4
    move v1, v4

    .line 319
    goto :goto_0
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 12
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v9, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 111
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 113
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 116
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v7, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v5

    .line 117
    .local v5, "matchId":I
    packed-switch v5, :pswitch_data_0

    .line 171
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bad path: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; calculated match id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 121
    :pswitch_0
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v7, p1, v8, v9}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 169
    :goto_0
    return-object v7

    .line 124
    :pswitch_1
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v10

    invoke-direct {v7, p1, v8, v9, v10}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    goto :goto_0

    .line 126
    :pswitch_2
    new-instance v7, Lcom/sec/android/gallery3d/data/EventAlbumSet;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v7, p1, v8}, Lcom/sec/android/gallery3d/data/EventAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    goto :goto_0

    .line 128
    :pswitch_3
    new-instance v7, Lcom/sec/android/gallery3d/data/EventAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v9

    invoke-direct {v7, p1, v8, v9, v11}, Lcom/sec/android/gallery3d/data/EventAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 130
    :pswitch_4
    new-instance v7, Lcom/sec/android/gallery3d/data/EventAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v9

    invoke-direct {v7, p1, v8, v9, v10}, Lcom/sec/android/gallery3d/data/EventAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 132
    :pswitch_5
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v1

    .line 133
    .local v1, "bucketId":I
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 134
    .local v3, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v7, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v7, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 136
    .local v4, "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v7, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v7, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 138
    .local v6, "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 139
    .local v2, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    new-array v9, v9, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v4, v9, v10

    aput-object v6, v9, v11

    invoke-direct {v7, p1, v2, v8, v9}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 144
    .end local v1    # "bucketId":I
    .end local v2    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v3    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_6
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v8, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v8

    invoke-direct {v7, p1, v0, v8, v11}, Lcom/sec/android/gallery3d/data/LocalAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 146
    :pswitch_7
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v8, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v8

    invoke-direct {v7, p1, v0, v8, v10}, Lcom/sec/android/gallery3d/data/LocalAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    goto :goto_0

    .line 148
    :pswitch_8
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v1

    .line 149
    .restart local v1    # "bucketId":I
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 150
    .restart local v3    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v7, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v7, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 152
    .restart local v4    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v7, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v7, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 154
    .restart local v6    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 157
    .restart local v2    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    new-array v9, v9, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v4, v9, v10

    aput-object v6, v9, v11

    invoke-direct {v7, p1, v2, v8, v9}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto/16 :goto_0

    .line 161
    .end local v1    # "bucketId":I
    .end local v2    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v3    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_9
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalListAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v8, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, p1, v0, v8}, Lcom/sec/android/gallery3d/data/LocalListAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 163
    :pswitch_a
    new-instance v7, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v8, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, p1, v0, v8}, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 165
    :pswitch_b
    new-instance v7, Lcom/sec/android/gallery3d/data/CategoryAlbum;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v8, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, p1, v0, v8}, Lcom/sec/android/gallery3d/data/CategoryAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 167
    :pswitch_c
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalImage;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v9

    invoke-direct {v7, p1, v8, v9}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    goto/16 :goto_0

    .line 169
    :pswitch_d
    new-instance v7, Lcom/sec/android/gallery3d/data/LocalVideo;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v9

    invoke-direct {v7, p1, v8, v9}, Lcom/sec/android/gallery3d/data/LocalVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    goto/16 :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 217
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 239
    :cond_0
    :goto_0
    :pswitch_0
    return-object v1

    .line 219
    :pswitch_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 220
    .local v2, "id":J
    cmp-long v4, v2, v6

    if-ltz v4, :cond_0

    sget-object v4, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 223
    .end local v2    # "id":J
    :pswitch_2
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 224
    .restart local v2    # "id":J
    cmp-long v4, v2, v6

    if-ltz v4, :cond_0

    sget-object v4, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 227
    .end local v2    # "id":J
    :pswitch_3
    const/4 v4, 0x1

    invoke-direct {p0, p1, v4}, Lcom/sec/android/gallery3d/data/LocalSource;->getAlbumPath(Landroid/net/Uri;I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 230
    :pswitch_4
    const/4 v4, 0x4

    invoke-direct {p0, p1, v4}, Lcom/sec/android/gallery3d/data/LocalSource;->getAlbumPath(Landroid/net/Uri;I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 233
    :pswitch_5
    const/4 v4, 0x0

    invoke-direct {p0, p1, v4}, Lcom/sec/android/gallery3d/data/LocalSource;->getAlbumPath(Landroid/net/Uri;I)Lcom/sec/android/gallery3d/data/Path;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v4, "LocalSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "uri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 258
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 259
    .local v0, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v1, :cond_0

    .line 260
    const-string v1, "/local/all"

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 263
    :goto_0
    return-object v1

    .restart local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getParentSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x0

    .line 243
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 244
    .local v0, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_1

    .line 253
    .end local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    :goto_0
    return-object v1

    .line 246
    .restart local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v2, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 247
    const-string v1, "/local/image"

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0

    .line 249
    .restart local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    instance-of v2, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 250
    const-string v1, "/local/video"

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local v0    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0
.end method

.method public mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V
    .locals 7
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 269
    .local v1, "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 270
    .local v5, "videoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 271
    .local v2, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 272
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 275
    .local v4, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v6, v4, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 276
    .local v3, "parent":Lcom/sec/android/gallery3d/data/Path;
    sget-object v6, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    if-ne v3, v6, :cond_1

    .line 277
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    :cond_1
    sget-object v6, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    if-ne v3, v6, :cond_0

    .line 279
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 283
    .end local v3    # "parent":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_2
    const/4 v6, 0x1

    invoke-direct {p0, v1, p2, v6}, Lcom/sec/android/gallery3d/data/LocalSource;->processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V

    .line 284
    const/4 v6, 0x0

    invoke-direct {p0, v5, p2, v6}, Lcom/sec/android/gallery3d/data/LocalSource;->processMapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;Z)V

    .line 285
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mClient:Landroid/content/ContentProviderClient;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mClient:Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mClient:Landroid/content/ContentProviderClient;

    .line 352
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "media"

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalSource;->mClient:Landroid/content/ContentProviderClient;

    .line 344
    return-void
.end method
