.class public abstract Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaItem;
.source "TCloudMediaItem.java"


# instance fields
.field public availablethumb:I

.field public bucketId:I

.field public bucketName:Ljava/lang/String;

.field public cachedPath:Ljava/lang/String;

.field public caption:Ljava/lang/String;

.field public data:Ljava/lang/String;

.field public dateModifiedInSec:J

.field public fileSize:J

.field public id:J

.field public isCached:I

.field public final mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

.field public mimeType:Ljava/lang/String;

.field public orientation:I

.field public thumbPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 47
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 48
    return-void
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->bucketId:I

    return v0
.end method

.method public getCachedPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->cachedPath:Ljava/lang/String;

    return-object v0
.end method

.method public getDateInMs()J
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->dateModifiedInSec:J

    return-wide v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 6

    .prologue
    .line 74
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 75
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/16 v1, 0xc8

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->data:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 76
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->caption:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 77
    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->fileSize:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 78
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->fileSize:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 79
    :cond_0
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 108
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getModifiedDateInSec()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->dateModifiedInSec:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->caption:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->fileSize:J

    return-wide v0
.end method

.method protected updateContent(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->updateFromCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-static {}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->mDataVersion:J

    .line 70
    :cond_0
    return-void
.end method

.method protected abstract updateFromCursor(Landroid/database/Cursor;)Z
.end method
