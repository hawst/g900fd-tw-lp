.class public Lcom/sec/android/gallery3d/gadget/WidgetService;
.super Landroid/widget/RemoteViewsService;
.source "WidgetService.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;,
        Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;
    }
.end annotation


# static fields
.field public static final EXTRA_ALBUM_PATH:Ljava/lang/String; = "album-path"

.field public static final EXTRA_FRAME_TYPE:Ljava/lang/String; = "frame-type"

.field public static final EXTRA_WIDGET_TYPE:Ljava/lang/String; = "widget-type"

.field private static final TAG:Ljava/lang/String; = "GalleryAppWidgetService"


# instance fields
.field private mAppWidgetIds:[I

.field public mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mFilePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mHelper:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    .line 633
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetService$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetService$1;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mContentObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/gadget/WidgetService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetService;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetService;->updatePersonImage()V

    return-void
.end method

.method private updatePersonImage()V
    .locals 11

    .prologue
    .line 646
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 647
    .local v6, "personIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 648
    .local v1, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mFilePaths:Ljava/util/ArrayList;

    .line 650
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mAppWidgetIds:[I

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_3

    aget v3, v0, v2

    .line 651
    .local v3, "id":I
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mFilePaths:Ljava/util/ArrayList;

    .line 652
    iget-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mHelper:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-virtual {v8, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v1

    .line 653
    if-eqz v1, :cond_2

    .line 654
    iget v8, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_1

    .line 655
    iget-object v8, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 656
    .local v7, "temp":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_1
    array-length v8, v7

    if-ge v4, v8, :cond_0

    .line 657
    aget-object v8, v7, v4

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 659
    :cond_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v8, v6}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getPersonImagePath(Landroid/content/ContentResolver;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mFilePaths:Ljava/util/ArrayList;

    .line 661
    iget-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mFilePaths:Ljava/util/ArrayList;

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-static {v8, v3, v9}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 664
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mHelper:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-virtual {v8, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 665
    iget-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mContext:Landroid/content/Context;

    invoke-static {v8, v3, v1}, Lcom/sec/android/gallery3d/gadget/PhotoAppWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    iget-object v8, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mHelper:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 669
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 650
    .end local v4    # "index":I
    .end local v7    # "temp":[Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 667
    .restart local v4    # "index":I
    .restart local v7    # "temp":[Ljava/lang/String;
    :catchall_0
    move-exception v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mHelper:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v8

    .line 672
    .end local v4    # "index":I
    .end local v7    # "temp":[Ljava/lang/String;
    :cond_2
    const-string v8, "GalleryAppWidgetService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cannot load widget: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 675
    .end local v3    # "id":I
    :cond_3
    return-void
.end method


# virtual methods
.method public initialize(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 626
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mHelper:Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    .line 627
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mResolver:Landroid/content/ContentResolver;

    move-object v0, p1

    .line 628
    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 629
    invoke-static {p1}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getAppWidgetIds(Landroid/content/Context;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mAppWidgetIds:[I

    .line 630
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetService;->mContext:Landroid/content/Context;

    .line 631
    return-void
.end method

.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 78
    const-string v0, "appWidgetId"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 79
    .local v2, "id":I
    const-string/jumbo v0, "widget-type"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 80
    .local v3, "type":I
    const-string v0, "album-path"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 82
    .local v4, "albumPath":Ljava/lang/String;
    const-string v0, "frame-type"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 88
    .local v5, "frameType":I
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/gadget/WidgetService$PhotoRVFactory;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;IILjava/lang/String;I)V

    return-object v0
.end method
