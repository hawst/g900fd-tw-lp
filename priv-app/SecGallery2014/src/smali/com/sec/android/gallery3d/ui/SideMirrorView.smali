.class public Lcom/sec/android/gallery3d/ui/SideMirrorView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "SideMirrorView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;
    }
.end annotation


# static fields
.field private static final DATA_CACHE_SIZE:I = 0x28

.field private static final HIDE_ANIMATION_DURATION:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "SideMirrorView"

.field public static mUse3DFilmStrip:Z


# instance fields
.field private filmstripThumbSize:I

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

.field private mAlbumSetView:Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;

.field private mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

.field private mBarSize:I

.field private mBg:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mBottomMargin:I

.field private mContentSize:I

.field private mHelpMode:I

.field private mIsSelectioMode:Z

.field private mIsShowing:Z

.field private mListener:Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;

.field private mMidMargin:I

.field private mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

.field protected mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

.field private mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

.field private mTopMargin:I

.field private mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

.field private final mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUse3DFilmStrip:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;IIIIIIIIILcom/sec/android/gallery3d/util/KeyBoardManager;I)V
    .locals 10
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "topMargin"    # I
    .param p4, "midMargin"    # I
    .param p5, "bottomMargin"    # I
    .param p6, "contentSize"    # I
    .param p7, "thumbWidth"    # I
    .param p8, "thumbHeight"    # I
    .param p9, "barSize"    # I
    .param p10, "gripSize"    # I
    .param p11, "gripWidth"    # I
    .param p12, "keyboardManager"    # Lcom/sec/android/gallery3d/util/KeyBoardManager;
    .param p13, "helpMode"    # I

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 63
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mIsSelectioMode:Z

    .line 65
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mHelpMode:I

    .line 67
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mIsShowing:Z

    .line 390
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->filmstripThumbSize:I

    .line 416
    new-instance v1, Lcom/sec/android/gallery3d/ui/SideMirrorView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView$2;-><init>(Lcom/sec/android/gallery3d/ui/SideMirrorView;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 82
    iput p3, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mTopMargin:I

    .line 83
    iput p4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mMidMargin:I

    .line 84
    iput p5, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBottomMargin:I

    .line 85
    move/from16 v0, p6

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mContentSize:I

    .line 86
    move/from16 v0, p9

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBarSize:I

    .line 87
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mHelpMode:I

    .line 88
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 89
    new-instance v9, Lcom/sec/android/gallery3d/ui/SlotView$Spec;

    invoke-direct {v9}, Lcom/sec/android/gallery3d/ui/SlotView$Spec;-><init>()V

    .line 90
    .local v9, "spec":Lcom/sec/android/gallery3d/ui/SlotView$Spec;
    move/from16 v0, p7

    iput v0, v9, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotWidth:I

    .line 91
    move/from16 v0, p8

    iput v0, v9, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotHeight:I

    .line 92
    const/4 v1, 0x0

    iput v1, v9, Lcom/sec/android/gallery3d/ui/SlotView$Spec;->slotGap:I

    .line 95
    const/4 v8, 0x0

    .line 97
    .local v8, "isMiniThumbMode":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 98
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectionListener(Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;)V

    .line 99
    new-instance v1, Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, v9}, Lcom/sec/android/gallery3d/ui/SlotView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SlotView$Spec;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    .line 100
    new-instance v5, Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;

    invoke-direct {v5}, Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;-><init>()V

    .line 101
    .local v5, "labelSpec":Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;
    new-instance v1, Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    const/4 v6, 0x0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/ui/SlotView;Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetView:Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;

    .line 102
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView;->setWide(Z)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView;->setOverscrollEffect(I)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    new-instance v2, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView$1;-><init>(Lcom/sec/android/gallery3d/ui/SideMirrorView;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView;->setListener(Lcom/sec/android/gallery3d/ui/SlotView$Listener;)V

    .line 129
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SlotView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 130
    new-instance v1, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    const/16 v2, 0x28

    invoke-direct {v1, p1, p2, v2}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .line 131
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetView:Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;->setModel(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)V

    .line 136
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    const v7, 0x7f02020b

    .line 138
    .local v7, "bgid":I
    :goto_0
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v7}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBg:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 139
    return-void

    .line 136
    .end local v7    # "bgid":I
    :cond_0
    const v7, 0x7f020267

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/SideMirrorView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SideMirrorView;

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mHelpMode:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/SideMirrorView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SideMirrorView;
    .param p1, "x1"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->onDown(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/SideMirrorView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SideMirrorView;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->onUp(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/SideMirrorView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SideMirrorView;
    .param p1, "x1"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->onSingleTapUp(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/SideMirrorView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/SideMirrorView;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    return-object v0
.end method

.method private getGenericFocusIndex()I
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    if-eqz v0, :cond_0

    .line 302
    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method private onDown(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 323
    return-void
.end method

.method private onSingleTapUp(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    const/4 v3, -0x1

    .line 331
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mIsSelectioMode:Z

    if-eqz v2, :cond_1

    .line 332
    if-eq p1, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-eqz v2, :cond_0

    .line 333
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mListener:Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;

    if-eqz v2, :cond_0

    .line 338
    if-eq p1, v3, :cond_2

    .line 339
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 342
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mHelpMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 343
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0f01c4

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 345
    .local v0, "currTextView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e02c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 347
    .local v1, "pinchInToZoomOutText":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 348
    const v2, 0x7f0e02c7

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private onUp(Z)V
    .locals 1
    .param p1, "followedByLongPress"    # Z

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 328
    return-void
.end method

.method private updateBottomMargin()V
    .locals 3

    .prologue
    .line 289
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 290
    .local v0, "r":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 291
    const v1, 0x7f0d010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBottomMargin:I

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_0
    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBottomMargin:I

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectionListener(Lcom/sec/android/gallery3d/ui/SelectionManager$SelectionListener;)V

    .line 387
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 272
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2

    .line 252
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteractionBegin()V

    goto :goto_0

    .line 256
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteractionEnd()V

    goto :goto_0

    .line 260
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    if-eqz v2, :cond_0

    .line 261
    const/4 v1, -0x1

    .line 262
    .local v1, "previousIndex":I
    const/16 v2, 0x3ea

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 263
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->getGenericFocusIndex()I

    move-result v1

    .line 265
    :cond_1
    const/4 v0, 0x0

    .line 266
    .local v0, "index":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    if-eq v1, v0, :cond_0

    .line 267
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    const/4 v3, 0x0

    invoke-interface {v2, v3, v0}, Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;->onGenericMotionEnter(Lcom/sec/android/gallery3d/ui/GLView;I)V

    goto :goto_0

    .line 249
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_2
    .end sparse-switch
.end method

.method public forceHide()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;->forceStop()V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetView:Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;

    if-eqz v0, :cond_1

    .line 199
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setVisibility(I)V

    .line 200
    return-void
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->getVisibility()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    .line 177
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;->setDuration(I)V

    .line 178
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAnimation:Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getAirButton(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 180
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "SideMirrorView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalStateException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isSelectionMode()Z
    .locals 1

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mIsSelectioMode:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mIsShowing:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 233
    if-nez p1, :cond_0

    .line 237
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mTopMargin:I

    sub-int v3, p4, p2

    sub-int v4, p5, p3

    iget v5, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mTopMargin:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/SlotView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->updateBottomMargin()V

    .line 225
    iget v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mTopMargin:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mContentSize:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mMidMargin:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBarSize:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBottomMargin:I

    add-int v0, v1, v2

    .line 226
    .local v0, "height":I
    invoke-static {p0}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->getInstance(Lcom/sec/android/gallery3d/ui/GLView;)Lcom/sec/android/gallery3d/ui/MeasureHelper;

    move-result-object v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->setPreferredContentSize(II)Lcom/sec/android/gallery3d/ui/MeasureHelper;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->measure(II)V

    .line 229
    return-void
.end method

.method public onSelectionChange(Lcom/sec/android/gallery3d/data/Path;Z)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "selected"    # Z

    .prologue
    .line 464
    return-void
.end method

.method public onSelectionModeChange(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 458
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 244
    const/4 v0, 0x1

    return v0
.end method

.method protected onVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 216
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onVisibilityChanged(I)V

    .line 217
    if-nez p1, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteraction()V

    .line 220
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetView:Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;->pause()V

    .line 369
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->pause()V

    .line 371
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->onUp(Z)V

    .line 372
    invoke-static {}, Lcom/sec/android/gallery3d/ui/MeasureHelper;->releaseInstance()V

    .line 373
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v2, 0x0

    .line 312
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-interface {p1, v2, v2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->clipRect(IIII)Z

    .line 314
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 315
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 316
    return-void
.end method

.method public renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v2, 0x0

    .line 307
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBg:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 308
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetView:Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer;->resume()V

    .line 377
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->resume()V

    .line 379
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->updateItemSize()V

    .line 382
    :cond_0
    return-void
.end method

.method public setFocusIndex(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView;->makeSlotVisible(I)V

    .line 361
    return-void
.end method

.method public setGenericFocusIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    if-eqz v0, :cond_0

    .line 282
    if-ltz p1, :cond_0

    .line 283
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->isSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    :goto_0
    invoke-virtual {v1, v2, p1, v0}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 286
    :cond_0
    return-void

    .line 283
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGroupIndex(J)V
    .locals 0
    .param p1, "groupIndex"    # J

    .prologue
    .line 446
    return-void
.end method

.method public setIsShowing(Z)V
    .locals 0
    .param p1, "isShowing"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mIsShowing:Z

    .line 143
    return-void
.end method

.method public setListener(Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mListener:Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;

    .line 151
    return-void
.end method

.method public setOnGenericMotionListener(Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    .line 159
    return-void
.end method

.method public setStartIndex(I)V
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView;->setStartIndex(I)V

    .line 365
    return-void
.end method

.method public setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 155
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateAllItem()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    if-eqz v0, :cond_0

    .line 212
    :cond_0
    return-void
.end method

.method public updateItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mAlbumSetDataAdapter:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    if-eqz v0, :cond_0

    .line 206
    :cond_0
    return-void
.end method

.method public updateItemSize()V
    .locals 10

    .prologue
    const-wide v8, 0x3fe6666666666666L    # 0.7

    .line 393
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v3

    .line 394
    .local v3, "size":Landroid/graphics/Point;
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->filmstripThumbSize:I

    if-nez v4, :cond_0

    .line 395
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d010f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->filmstripThumbSize:I

    .line 398
    :cond_0
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->filmstripThumbSize:I

    iget v5, v3, Landroid/graphics/Point;->x:I

    mul-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v5

    div-int v1, v4, v5

    .line 400
    .local v1, "newWidth":I
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->filmstripThumbSize:I

    iget v5, v3, Landroid/graphics/Point;->y:I

    mul-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v5

    div-int v0, v4, v5

    .line 402
    .local v0, "newHeight":I
    if-le v1, v0, :cond_2

    move v2, v0

    .line 403
    .local v2, "shortLength":I
    :goto_0
    int-to-double v4, v2

    iget v6, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->filmstripThumbSize:I

    int-to-double v6, v6

    mul-double/2addr v6, v8

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 404
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->filmstripThumbSize:I

    int-to-double v4, v4

    mul-double/2addr v4, v8

    double-to-int v2, v4

    .line 406
    :cond_1
    iget v4, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mContentSize:I

    if-ne v4, v2, :cond_3

    .line 412
    :goto_1
    return-void

    .end local v2    # "shortLength":I
    :cond_2
    move v2, v1

    .line 402
    goto :goto_0

    .line 409
    .restart local v2    # "shortLength":I
    :cond_3
    iput v2, p0, Lcom/sec/android/gallery3d/ui/SideMirrorView;->mContentSize:I

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->requestLayout()V

    goto :goto_1
.end method
