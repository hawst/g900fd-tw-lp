.class public Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaItem;
.source "NearbyImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;,
        Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIME_OUT:I = 0xea60

.field private static final NEARBY_SCREENNAIL_SIZE:I = 0x4b000

.field private static final NEARBY_THUMBNAIL_SIZE:I = 0x6400

.field private static final NIC_WLAN:Ljava/lang/String; = "wlan0"

.field private static final NOT_INITED:I = -0x1

.field private static final TAG:Ljava/lang/String;

.field private static mDisplayPixels:I

.field private static mIsOverFHD:Z


# instance fields
.field private mDecodedHeight:I

.field private mDecodedWidth:I

.field private mDevice:Lcom/samsung/android/allshare/Device;

.field private mNearbyItem:Lcom/samsung/android/allshare/Item;

.field private mNearbyLargeThumbnail:Ljava/lang/String;

.field private mRotation:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    .line 62
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDisplayPixels:I

    .line 63
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mIsOverFHD:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/Item;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "device"    # Lcom/samsung/android/allshare/Device;
    .param p4, "imageItem"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 79
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDevice:Lcom/samsung/android/allshare/Device;

    .line 80
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    .line 83
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->initLargeThumbnail()V

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->generateKey()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/samsung/android/allshare/Item;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedWidth:I

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedHeight:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDataVersion:J

    return-wide v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;)Lcom/samsung/android/allshare/Device;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDevice:Lcom/samsung/android/allshare/Device;

    return-object v0
.end method

.method private generateKey()J
    .locals 4

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getDateInMs()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private initLargeThumbnail()V
    .locals 10

    .prologue
    .line 526
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->isOverFullHD()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 527
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/Item;->getResourceList()Ljava/util/ArrayList;

    move-result-object v5

    .line 529
    .local v5, "resources":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    :try_start_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Item$Resource;

    .line 530
    .local v3, "resource":Lcom/samsung/android/allshare/Item$Resource;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item$Resource;->getResolution()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 531
    .local v6, "size":[Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v8, v6, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 532
    .local v7, "width":I
    const/4 v8, 0x1

    aget-object v8, v6, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 533
    .local v1, "height":I
    mul-int v4, v7, v1

    .line 535
    .local v4, "resourceSize":I
    const/16 v8, 0x6400

    if-ge v8, v4, :cond_0

    const v8, 0x4b000

    if-ge v4, v8, :cond_0

    .line 537
    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item$Resource;->getURI()Landroid/net/Uri;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 538
    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item$Resource;->getURI()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyLargeThumbnail:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 544
    .end local v1    # "height":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "resource":Lcom/samsung/android/allshare/Item$Resource;
    .end local v4    # "resourceSize":I
    .end local v6    # "size":[Ljava/lang/String;
    .end local v7    # "width":I
    :catch_0
    move-exception v0

    .line 545
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 548
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v5    # "resources":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    :cond_1
    return-void

    .line 540
    .restart local v1    # "height":I
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "resource":Lcom/samsung/android/allshare/Item$Resource;
    .restart local v4    # "resourceSize":I
    .restart local v5    # "resources":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    .restart local v6    # "size":[Ljava/lang/String;
    .restart local v7    # "width":I
    :cond_2
    :try_start_1
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    const-string v9, "resource.getURI() : null!!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private isOverFullHD()Z
    .locals 2

    .prologue
    .line 551
    sget v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDisplayPixels:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 552
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDisplayPixels:I

    .line 553
    sget v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDisplayPixels:I

    const v1, 0x1fa400

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mIsOverFHD:Z

    .line 556
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mIsOverFHD:Z

    return v0

    .line 553
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDateInMs()J
    .locals 4

    .prologue
    .line 297
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v0

    .line 298
    .local v0, "date":Ljava/util/Date;
    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 12

    .prologue
    .line 204
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 205
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    if-nez v0, :cond_1

    .line 206
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    const-string v7, "getDetails : details is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const/4 v0, 0x0

    .line 238
    .end local v0    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    :cond_0
    :goto_0
    return-object v0

    .line 209
    .restart local v0    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v6

    if-nez v6, :cond_2

    .line 210
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDetails : Title = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mNearbyItem.getDate() is null"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :cond_2
    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 213
    const/4 v8, 0x4

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v9

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v6

    if-nez v6, :cond_6

    const-wide/16 v6, 0x0

    :goto_1
    invoke-static {v9, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v8, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v5

    .line 216
    .local v5, "width":I
    if-eqz v5, :cond_3

    .line 217
    const/16 v6, 0xc

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 218
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v1

    .line 219
    .local v1, "height":I
    if-eqz v1, :cond_4

    .line 220
    const/16 v6, 0xd

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 222
    :cond_4
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v2

    .line 223
    .local v2, "fileSize":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-ltz v6, :cond_5

    .line 224
    const/16 v6, 0xe

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 226
    :cond_5
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getLocation()Landroid/location/Location;

    move-result-object v4

    .line 227
    .local v4, "location":Landroid/location/Location;
    if-eqz v4, :cond_0

    .line 228
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 229
    const/16 v6, 0x9

    const/4 v7, 0x2

    new-array v7, v7, [D

    const/4 v8, 0x0

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    aput-wide v10, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    aput-wide v10, v7, v8

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 230
    const/16 v6, 0xa

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 231
    const/16 v6, 0xb

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 213
    .end local v1    # "height":I
    .end local v2    # "fileSize":J
    .end local v4    # "location":Landroid/location/Location;
    .end local v5    # "width":I
    :cond_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item;->getDate()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    goto/16 :goto_1

    .line 233
    .restart local v1    # "height":I
    .restart local v2    # "fileSize":J
    .restart local v4    # "location":Landroid/location/Location;
    .restart local v5    # "width":I
    :cond_7
    const/16 v6, 0x9

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 234
    const/16 v6, 0xa

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 235
    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFullImageRotation()I
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, -0x1

    .line 261
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v3

    .line 262
    .local v3, "uri":Landroid/net/Uri;
    if-nez v3, :cond_0

    .line 287
    :goto_0
    return v4

    .line 265
    :cond_0
    sget-object v5, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getFullImageRotation : mRotation is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    if-eq v5, v8, :cond_1

    .line 268
    iget v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    goto :goto_0

    .line 271
    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->getOrientationValue(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    .line 273
    iget v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    if-ne v5, v8, :cond_3

    .line 275
    iput v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    .line 277
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v4

    new-instance v5, Ljava/net/URL;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->generateKey()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/data/DownloadCache;->lookup(Ljava/net/URL;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v1

    .line 278
    .local v1, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-nez v1, :cond_2

    iget v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    goto :goto_0

    .line 279
    :cond_2
    new-instance v2, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 280
    .local v2, "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    iget-object v4, v1, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V

    .line 281
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/Exif;->getOrientation(Lcom/sec/android/gallery3d/exif/ExifInterface;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    .end local v1    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .end local v2    # "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    :cond_3
    :goto_1
    iget v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mRotation:I

    goto :goto_0

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 513
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getHeight()I
    .locals 7

    .prologue
    .line 173
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Item;->getResolution()Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "resolution":Ljava/lang/String;
    const/4 v3, 0x0

    .line 176
    .local v3, "result":I
    const/16 v4, 0x78

    :try_start_0
    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "height":Ljava/lang/String;
    if-eqz v1, :cond_0

    const/16 v4, 0x78

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 178
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :cond_0
    move v4, v3

    .line 183
    .end local v1    # "height":Ljava/lang/String;
    :goto_0
    return v4

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resolution is null, decode size instead : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedHeight:I

    goto :goto_0
.end method

.method public getItemToShow()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method public getLatitude()D
    .locals 4

    .prologue
    .line 501
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 502
    .local v0, "location":Landroid/location/Location;
    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    goto :goto_0
.end method

.method public getLongitude()D
    .locals 4

    .prologue
    .line 507
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 508
    .local v0, "location":Landroid/location/Location;
    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    goto :goto_0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x2

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNearbyThumbUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    sget-boolean v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mIsOverFHD:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyLargeThumbnail:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyLargeThumbnail:Ljava/lang/String;

    .line 135
    :goto_0
    return-object v1

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v0

    .line 129
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_1

    .line 130
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    .line 132
    :cond_1
    if-eqz v0, :cond_2

    .line 133
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 135
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNearbyUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    .line 114
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 115
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v0

    .line 117
    :cond_0
    if-eqz v0, :cond_1

    .line 118
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 577
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 582
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    .line 583
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 584
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 587
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 198
    const-wide v0, 0x800080000440L

    return-wide v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 602
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThumbUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 592
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v0

    .line 593
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 594
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 597
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWidth()I
    .locals 7

    .prologue
    .line 158
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Item;->getResolution()Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "resolution":Ljava/lang/String;
    const/4 v2, 0x0

    .line 161
    .local v2, "result":I
    const/4 v4, 0x0

    const/16 v5, 0x78

    :try_start_0
    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 162
    .local v3, "width":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 163
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    move v4, v2

    .line 168
    .end local v3    # "width":Ljava/lang/String;
    :goto_0
    return v4

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resolution is null, decode size instead : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDecodedWidth:I

    goto :goto_0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 4
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 93
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    if-nez v2, :cond_0

    .line 94
    const/4 v2, 0x0

    .line 108
    :goto_0
    return-object v2

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getWidth()I

    move-result v1

    .line 97
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getHeight()I

    move-result v0

    .line 100
    .local v0, "height":I
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v2

    if-le v1, v2, :cond_1

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v2

    if-gt v0, v2, :cond_2

    .line 102
    :cond_1
    new-instance v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getNearbyUri()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;Ljava/lang/String;I)V

    goto :goto_0

    .line 105
    :cond_2
    if-ne p1, v3, :cond_3

    .line 106
    new-instance v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getNearbyUri()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;Ljava/lang/String;I)V

    goto :goto_0

    .line 108
    :cond_3
    new-instance v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getNearbyThumbUri()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$HttpImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 143
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 144
    new-instance v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;

    new-instance v3, Ljava/net/URL;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, p0, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage$NearbyLargeImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;Ljava/net/URL;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v2

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 148
    .end local v0    # "e":Ljava/net/MalformedURLException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public updateContent(Lcom/samsung/android/allshare/Item;)V
    .locals 4
    .param p1, "imageItem"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 251
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 252
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    .line 253
    .local v0, "newUri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 254
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mNearbyItem:Lcom/samsung/android/allshare/Item;

    .line 255
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->mDataVersion:J

    .line 257
    :cond_0
    return-void
.end method
