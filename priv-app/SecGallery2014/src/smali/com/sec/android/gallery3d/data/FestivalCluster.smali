.class Lcom/sec/android/gallery3d/data/FestivalCluster;
.super Ljava/lang/Object;
.source "FestivalClustering.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Cluster"


# instance fields
.field private mInsertToFirst:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mInsertToFirst:Z

    .line 239
    return-void
.end method


# virtual methods
.method public addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mInsertToFirst:Z

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 258
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLastItem()Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .locals 3

    .prologue
    .line 269
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 270
    .local v0, "n":I
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    goto :goto_0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public setAddToFirst(Z)V
    .locals 0
    .param p1, "insertToFirst"    # Z

    .prologue
    .line 242
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mInsertToFirst:Z

    .line 243
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mName:Ljava/lang/String;

    .line 251
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FestivalCluster;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
