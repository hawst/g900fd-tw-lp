.class public final Lcom/sec/android/gallery3d/remote/picasa/GDataClient;
.super Ljava/lang/Object;
.source "GDataClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT:I = 0x4e20

.field private static final HTTPS_SCHEME:Ljava/lang/String; = "https:"

.field public static final HTTP_CONNECTION_MANAGER:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

.field public static final HTTP_PARAMS:Lorg/apache/http/params/HttpParams;

.field private static final HTTP_SCHEME:Ljava/lang/String; = "http:"

.field private static final IF_MATCH:Ljava/lang/String; = "If-Match"

.field private static final MIN_GZIP_SIZE:I = 0x200

.field private static final TAG:Ljava/lang/String; = "GDataClient"

.field private static final USER_AGENT:Ljava/lang/String; = "GData/1.0; gzip"

.field private static final X_HTTP_METHOD_OVERRIDE:Ljava/lang/String; = "X-HTTP-Method-Override"


# instance fields
.field private mAuthToken:Ljava/lang/String;

.field private final mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v3, 0x4e20

    .line 70
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 71
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 72
    invoke-static {v0, v3}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 73
    invoke-static {v0, v3}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 74
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 75
    const-string v2, "GData/1.0; gzip"

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 76
    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->HTTP_PARAMS:Lorg/apache/http/params/HttpParams;

    .line 79
    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 80
    .local v1, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v4

    const/16 v5, 0x50

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 81
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v4

    const/16 v5, 0x1bb

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 85
    new-instance v2, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v2, v0, v1}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    sput-object v2, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->HTTP_CONNECTION_MANAGER:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .line 86
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->HTTP_CONNECTION_MANAGER:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->HTTP_PARAMS:Lorg/apache/http/params/HttpParams;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 96
    return-void
.end method

.method private callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V
    .locals 15
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "operation"    # Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    const-string v12, "GData-Version"

    const-string v13, "2"

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v12, "Accept-Encoding"

    const-string v13, "gzip"

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->mAuthToken:Ljava/lang/String;

    .line 166
    .local v1, "authToken":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 167
    const-string v12, "Authorization"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "GoogleLogin auth="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    .line 172
    .local v5, "etag":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 173
    const-string v12, "If-None-Match"

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v5}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_1
    const/4 v8, 0x0

    .line 179
    .local v8, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v12, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 186
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    .line 187
    .local v9, "status":I
    const/4 v10, 0x0

    .line 188
    .local v10, "stream":Ljava/io/InputStream;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 189
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v4, :cond_2

    .line 191
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v10

    .line 192
    if-eqz v10, :cond_2

    .line 193
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v7

    .line 194
    .local v7, "header":Lorg/apache/http/Header;
    if-eqz v7, :cond_2

    .line 195
    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "gzip"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 196
    new-instance v11, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v11, v10}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .end local v10    # "stream":Ljava/io/InputStream;
    .local v11, "stream":Ljava/io/InputStream;
    move-object v10, v11

    .line 203
    .end local v7    # "header":Lorg/apache/http/Header;
    .end local v11    # "stream":Ljava/io/InputStream;
    .restart local v10    # "stream":Ljava/io/InputStream;
    :cond_2
    const-string v12, "ETag"

    invoke-interface {v8, v12}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v6

    .line 204
    .local v6, "etagHeader":Lorg/apache/http/Header;
    move-object/from16 v0, p2

    iput v9, v0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outStatus:I

    .line 205
    if-eqz v6, :cond_6

    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    :goto_0
    move-object/from16 v0, p2

    iput-object v12, v0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    .line 206
    move-object/from16 v0, p2

    iput-object v10, v0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->outBody:Ljava/io/InputStream;

    .line 209
    if-eqz v4, :cond_5

    .line 211
    const/4 v2, 0x0

    .line 212
    .local v2, "bDelete":Z
    const-string v12, "X-HTTP-Method-Override"

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lorg/apache/http/client/methods/HttpUriRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v7

    .line 213
    .restart local v7    # "header":Lorg/apache/http/Header;
    if-eqz v7, :cond_3

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_3

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    const-string v13, "DELETE"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 214
    const/4 v2, 0x1

    .line 216
    :cond_3
    const/16 v12, 0x194

    if-eq v9, v12, :cond_4

    if-eqz v2, :cond_5

    .line 217
    :cond_4
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 220
    .end local v2    # "bDelete":Z
    .end local v7    # "header":Lorg/apache/http/Header;
    :cond_5
    return-void

    .line 180
    .end local v4    # "entity":Lorg/apache/http/HttpEntity;
    .end local v6    # "etagHeader":Lorg/apache/http/Header;
    .end local v9    # "status":I
    .end local v10    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    .line 181
    .local v3, "e":Ljava/io/IOException;
    const-string v12, "GDataClient"

    const-string v13, "Request failed: "

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    throw v3

    .line 205
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v4    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v6    # "etagHeader":Lorg/apache/http/Header;
    .restart local v9    # "status":I
    .restart local v10    # "stream":Ljava/io/InputStream;
    :cond_6
    const/4 v12, 0x0

    goto :goto_0
.end method

.method private getCompressedEntity([B)Lorg/apache/http/entity/ByteArrayEntity;
    .locals 5
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    array-length v3, p1

    const/16 v4, 0x200

    if-lt v3, v4, :cond_0

    .line 225
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    array-length v3, p1

    div-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 226
    .local v0, "byteOutput":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v2, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 227
    .local v2, "gzipOutput":Ljava/util/zip/GZIPOutputStream;
    invoke-virtual {v2, p1}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 228
    invoke-virtual {v2}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 229
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 233
    .end local v0    # "byteOutput":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "gzipOutput":Ljava/util/zip/GZIPOutputStream;
    .local v1, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    :goto_0
    return-object v1

    .line 231
    .end local v1    # "entity":Lorg/apache/http/entity/ByteArrayEntity;
    :cond_0
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, p1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .restart local v1    # "entity":Lorg/apache/http/entity/ByteArrayEntity;
    goto :goto_0
.end method

.method public static inputStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 7
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 237
    if-eqz p0, :cond_0

    .line 238
    const/4 v1, 0x0

    .line 240
    .local v1, "bytes":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    .end local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .local v2, "bytes":Ljava/io/ByteArrayOutputStream;
    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    .line 243
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .local v3, "bytesRead":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_1

    .line 244
    const/4 v5, 0x0

    invoke-virtual {v2, v0, v5, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 247
    .end local v0    # "buffer":[B
    .end local v3    # "bytesRead":I
    :catch_0
    move-exception v4

    move-object v1, v2

    .line 249
    .end local v2    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .local v4, "e":Ljava/io/IOException;
    :goto_1
    :try_start_2
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 254
    .end local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v5, 0x0

    :goto_2
    return-object v5

    .line 246
    .restart local v0    # "buffer":[B
    .restart local v2    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "bytesRead":I
    :cond_1
    :try_start_3
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 251
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_2

    .end local v0    # "buffer":[B
    .end local v2    # "bytes":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "bytesRead":I
    .restart local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    :catchall_0
    move-exception v5

    :goto_3
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5

    .end local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "bytes":Ljava/io/ByteArrayOutputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    goto :goto_3

    .line 247
    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method private static replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 90
    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "http:"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 93
    .end local p0    # "input":Ljava/lang/String;
    :cond_0
    return-object p0
.end method


# virtual methods
.method public delete(Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V
    .locals 4
    .param p1, "feedUrl"    # Ljava/lang/String;
    .param p2, "operation"    # Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 151
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    iget-object v0, p2, Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;->inOutEtag:Ljava/lang/String;

    .line 152
    .local v0, "etag":Ljava/lang/String;
    const-string v2, "X-HTTP-Method-Override"

    const-string v3, "DELETE"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v2, "If-Match"

    if-eqz v0, :cond_0

    .end local v0    # "etag":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-direct {p0, v1, p2}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 155
    return-void

    .line 153
    .restart local v0    # "etag":Ljava/lang/String;
    :cond_0
    const-string v0, "*"

    goto :goto_0
.end method

.method public get(Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V
    .locals 2
    .param p1, "feedUrl"    # Ljava/lang/String;
    .param p2, "operation"    # Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 110
    return-void
.end method

.method public post(Ljava/lang/String;[BLjava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V
    .locals 3
    .param p1, "feedUrl"    # Ljava/lang/String;
    .param p2, "data"    # [B
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "operation"    # Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->getCompressedEntity([B)Lorg/apache/http/entity/ByteArrayEntity;

    move-result-object v0

    .line 115
    .local v0, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    invoke-virtual {v0, p3}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 117
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 119
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 120
    invoke-direct {p0, v1, p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 121
    return-void
.end method

.method public put(Ljava/lang/String;[BLjava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V
    .locals 4
    .param p1, "feedUrl"    # Ljava/lang/String;
    .param p2, "data"    # [B
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "operation"    # Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->getCompressedEntity([B)Lorg/apache/http/entity/ByteArrayEntity;

    move-result-object v0

    .line 126
    .local v0, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    invoke-virtual {v0, p3}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 128
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 130
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    const-string v2, "X-HTTP-Method-Override"

    const-string v3, "PUT"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 132
    invoke-direct {p0, v1, p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 133
    return-void
.end method

.method public putStream(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V
    .locals 4
    .param p1, "feedUrl"    # Ljava/lang/String;
    .param p2, "stream"    # Ljava/io/InputStream;
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "operation"    # Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Lorg/apache/http/entity/InputStreamEntity;

    const-wide/16 v2, -0x1

    invoke-direct {v0, p2, v2, v3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 138
    .local v0, "entity":Lorg/apache/http/entity/InputStreamEntity;
    invoke-virtual {v0, p3}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 140
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->replaceHttpWithHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 142
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    const-string v2, "X-HTTP-Method-Override"

    const-string v3, "PUT"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 144
    invoke-direct {p0, v1, p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->callMethod(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/sec/android/gallery3d/remote/picasa/GDataClient$Operation;)V

    .line 145
    return-void
.end method

.method public setAuthToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataClient;->mAuthToken:Ljava/lang/String;

    .line 104
    return-void
.end method
