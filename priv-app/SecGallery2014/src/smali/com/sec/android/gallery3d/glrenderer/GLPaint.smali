.class public Lcom/sec/android/gallery3d/glrenderer/GLPaint;
.super Ljava/lang/Object;
.source "GLPaint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;
    }
.end annotation


# static fields
.field public static final FLAG_ANTI_ALIAS:I = 0x1


# instance fields
.field private mAlpha:F

.field private mColor:I

.field private mFlags:I

.field private mLineWidth:F

.field private mStyle:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mLineWidth:F

    .line 23
    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mColor:I

    .line 26
    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    .line 28
    sget-object v0, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->FILL:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mStyle:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    .line 29
    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mAlpha:F

    .line 74
    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mAlpha:F

    return v0
.end method

.method public getAntiAlias()Z
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mColor:I

    return v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mLineWidth:F

    return v0
.end method

.method public getStyle()Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mStyle:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    return-object v0
.end method

.method public setAlpha(F)V
    .locals 3
    .param p1, "a"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 127
    cmpg-float v2, p1, v0

    if-gez v2, :cond_1

    move p1, v0

    .end local p1    # "a":F
    :cond_0
    :goto_0
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mAlpha:F

    .line 128
    return-void

    .line 127
    .restart local p1    # "a":F
    :cond_1
    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    move p1, v1

    goto :goto_0
.end method

.method public setAntiAlias(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 59
    if-eqz p1, :cond_0

    .line 60
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mColor:I

    .line 34
    return-void
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mFlags:I

    .line 56
    return-void
.end method

.method public setLineWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 41
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 42
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mLineWidth:F

    .line 43
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStyle(Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;)V
    .locals 0
    .param p1, "style"    # Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->mStyle:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    .line 120
    return-void
.end method
