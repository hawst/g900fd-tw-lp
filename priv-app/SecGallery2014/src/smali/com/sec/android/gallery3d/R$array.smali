.class public final Lcom/sec/android/gallery3d/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final album_time_vi_degree:I = 0x7f090072

.field public static final album_view_album_count_font_size:I = 0x7f09004e

.field public static final album_view_album_font_size:I = 0x7f09004d

.field public static final album_view_end_affordance_row_degree:I = 0x7f090086

.field public static final album_view_grid_colume_count:I = 0x7f09008c

.field public static final album_view_grid_land_colume_count:I = 0x7f09008d

.field public static final album_view_hgap:I = 0x7f090051

.field public static final album_view_hpadding_left:I = 0x7f09004f

.field public static final album_view_hpadding_right:I = 0x7f090050

.field public static final album_view_name_textbox_height:I = 0x7f090059

.field public static final album_view_name_textbox_hoffset:I = 0x7f090056

.field public static final album_view_name_textbox_voffset:I = 0x7f090057

.field public static final album_view_name_textbox_width:I = 0x7f090058

.field public static final album_view_thumbnail_height:I = 0x7f090055

.field public static final album_view_thumbnail_width:I = 0x7f090054

.field public static final album_view_vgap:I = 0x7f090053

.field public static final album_view_vpadding_top:I = 0x7f090052

.field public static final barge_in_commands:I = 0x7f090042

.field public static final camera_flashmode_icons:I = 0x7f09000e

.field public static final camera_flashmode_largeicons:I = 0x7f09000f

.field public static final camera_id_entries:I = 0x7f090024

.field public static final camera_id_icons:I = 0x7f090026

.field public static final camera_id_labels:I = 0x7f090025

.field public static final camera_id_largeicons:I = 0x7f090027

.field public static final camera_recordlocation_icons:I = 0x7f090018

.field public static final camera_recordlocation_largeicons:I = 0x7f090019

.field public static final camera_wb_indicators:I = 0x7f09001f

.field public static final change_picture:I = 0x7f090034

.field public static final change_picture_ex:I = 0x7f090035

.field public static final contact_more_popup_items:I = 0x7f090036

.field public static final contact_more_popup_items_less:I = 0x7f090038

.field public static final contact_more_popup_items_tab:I = 0x7f090037

.field public static final detail_view_hgap:I = 0x7f09007b

.field public static final detail_view_hpadding_left:I = 0x7f09007a

.field public static final detail_view_icon_hpadding_left:I = 0x7f09007e

.field public static final detail_view_icon_vpadding_top:I = 0x7f09007f

.field public static final detail_view_strip_hpadding_left:I = 0x7f090080

.field public static final detail_view_strip_icon_hpadding_left:I = 0x7f090084

.field public static final detail_view_strip_icon_vpadding_bottom:I = 0x7f090085

.field public static final detail_view_strip_thumbnail_height:I = 0x7f090083

.field public static final detail_view_strip_thumbnail_width:I = 0x7f090082

.field public static final detail_view_strip_vpadding_top:I = 0x7f090081

.field public static final detail_view_thumbnail_height:I = 0x7f09007d

.field public static final detail_view_thumbnail_width:I = 0x7f09007c

.field public static final drawer_all:I = 0x7f090045

.field public static final drawer_all_icon_resource_ids:I = 0x7f090049

.field public static final drawer_easy:I = 0x7f090047

.field public static final drawer_easy_icon_resource_ids:I = 0x7f09004b

.field public static final drawer_filterby:I = 0x7f090046

.field public static final drawer_filterby_icon_resource_ids:I = 0x7f09004a

.field public static final drawer_myphone:I = 0x7f090044

.field public static final drawer_myphone_icon_resource_ids:I = 0x7f090048

.field public static final edit_burst_shot:I = 0x7f090043

.field public static final effects:I = 0x7f090092

.field public static final filters:I = 0x7f090095

.field public static final photo_view_grid_colume_count:I = 0x7f090089

.field public static final photo_view_grid_colume_count_land:I = 0x7f090090

.field public static final photo_view_grid_colume_count_with_split_panel:I = 0x7f09008a

.field public static final photo_view_grid_colume_count_with_split_panel_land:I = 0x7f090091

.field public static final photo_view_grid_item_gap:I = 0x7f09008b

.field public static final pref_camera_countdown_labels:I = 0x7f090033

.field public static final pref_camera_exposure_icons:I = 0x7f090032

.field public static final pref_camera_flashmode_entries:I = 0x7f09000b

.field public static final pref_camera_flashmode_entryvalues:I = 0x7f09000d

.field public static final pref_camera_flashmode_labels:I = 0x7f09000c

.field public static final pref_camera_focusmode_default_array:I = 0x7f090031

.field public static final pref_camera_focusmode_entries:I = 0x7f090008

.field public static final pref_camera_focusmode_entryvalues:I = 0x7f090009

.field public static final pref_camera_focusmode_labels:I = 0x7f09000a

.field public static final pref_camera_hdr_entries:I = 0x7f09002b

.field public static final pref_camera_hdr_entryvalues:I = 0x7f09002e

.field public static final pref_camera_hdr_icons:I = 0x7f09002d

.field public static final pref_camera_hdr_labels:I = 0x7f09002c

.field public static final pref_camera_picturesize_entries:I = 0x7f090006

.field public static final pref_camera_picturesize_entryvalues:I = 0x7f090007

.field public static final pref_camera_recordlocation_entries:I = 0x7f090016

.field public static final pref_camera_recordlocation_entryvalues:I = 0x7f090015

.field public static final pref_camera_recordlocation_labels:I = 0x7f090017

.field public static final pref_camera_scenemode_entries:I = 0x7f090020

.field public static final pref_camera_scenemode_entryvalues:I = 0x7f090023

.field public static final pref_camera_scenemode_icons:I = 0x7f090022

.field public static final pref_camera_scenemode_labels:I = 0x7f090021

.field public static final pref_camera_timer_sound_entries:I = 0x7f09002f

.field public static final pref_camera_timer_sound_entryvalues:I = 0x7f090030

.field public static final pref_camera_video_flashmode_entries:I = 0x7f090010

.field public static final pref_camera_video_flashmode_entryvalues:I = 0x7f090012

.field public static final pref_camera_video_flashmode_labels:I = 0x7f090011

.field public static final pref_camera_whitebalance_entries:I = 0x7f09001a

.field public static final pref_camera_whitebalance_entryvalues:I = 0x7f09001c

.field public static final pref_camera_whitebalance_labels:I = 0x7f09001b

.field public static final pref_video_effect_entries:I = 0x7f090028

.field public static final pref_video_effect_entryvalues:I = 0x7f090029

.field public static final pref_video_quality_entries:I = 0x7f090000

.field public static final pref_video_quality_entryvalues:I = 0x7f090001

.field public static final pref_video_time_lapse_frame_interval_duration_values:I = 0x7f090004

.field public static final pref_video_time_lapse_frame_interval_entries:I = 0x7f090003

.field public static final pref_video_time_lapse_frame_interval_entryvalues:I = 0x7f090002

.field public static final pref_video_time_lapse_frame_interval_units:I = 0x7f090005

.field public static final sorts:I = 0x7f090094

.field public static final speeds:I = 0x7f090093

.field public static final supportCellSizeArray:I = 0x7f09004c

.field public static final tab_names:I = 0x7f090039

.field public static final tab_names_for_help:I = 0x7f09003c

.field public static final tab_names_for_knox:I = 0x7f09003b

.field public static final tab_names_for_secret:I = 0x7f09003a

.field public static final tab_names_for_spc:I = 0x7f09003d

.field public static final thumbnail_view_end_affordance_row_degree:I = 0x7f090087

.field public static final thumbnail_view_hgap:I = 0x7f09005c

.field public static final thumbnail_view_hpadding_left:I = 0x7f09005a

.field public static final thumbnail_view_hpadding_right:I = 0x7f09005b

.field public static final thumbnail_view_new_album_divider_height:I = 0x7f090078

.field public static final thumbnail_view_new_album_hpadding_left:I = 0x7f090073

.field public static final thumbnail_view_new_album_textbox_voffset:I = 0x7f090079

.field public static final thumbnail_view_new_album_thumbnail_height:I = 0x7f090077

.field public static final thumbnail_view_new_album_thumbnail_width:I = 0x7f090076

.field public static final thumbnail_view_new_album_vpadding_bottom:I = 0x7f090075

.field public static final thumbnail_view_new_album_vpadding_top:I = 0x7f090074

.field public static final thumbnail_view_split_album_background_height:I = 0x7f09006c

.field public static final thumbnail_view_split_album_background_width:I = 0x7f09006b

.field public static final thumbnail_view_split_album_divider_height:I = 0x7f09006d

.field public static final thumbnail_view_split_album_hgap:I = 0x7f090062

.field public static final thumbnail_view_split_album_hpadding_left:I = 0x7f090061

.field public static final thumbnail_view_split_album_name_textbox_height:I = 0x7f09006a

.field public static final thumbnail_view_split_album_name_textbox_hoffset:I = 0x7f090067

.field public static final thumbnail_view_split_album_name_textbox_voffset:I = 0x7f090068

.field public static final thumbnail_view_split_album_name_textbox_width:I = 0x7f090069

.field public static final thumbnail_view_split_album_thumbnail_height:I = 0x7f090066

.field public static final thumbnail_view_split_album_thumbnail_width:I = 0x7f090065

.field public static final thumbnail_view_split_album_vgap:I = 0x7f090064

.field public static final thumbnail_view_split_album_vpadding_top:I = 0x7f090063

.field public static final thumbnail_view_thumbnail_height:I = 0x7f090060

.field public static final thumbnail_view_thumbnail_width:I = 0x7f09005f

.field public static final thumbnail_view_vgap:I = 0x7f09005e

.field public static final thumbnail_view_vpadding_top:I = 0x7f09005d

.field public static final time_folder_child_type:I = 0x7f090071

.field public static final time_folder_label_padding:I = 0x7f090070

.field public static final time_folder_screen:I = 0x7f09006e

.field public static final time_folder_screen_padding:I = 0x7f09006f

.field public static final time_view_gap:I = 0x7f090088

.field public static final time_view_grid_colume_count:I = 0x7f09008e

.field public static final time_view_grid_land_colume_count:I = 0x7f09008f

.field public static final video_effect_icons:I = 0x7f09002a

.field public static final video_flashmode_icons:I = 0x7f090013

.field public static final video_flashmode_largeicons:I = 0x7f090014

.field public static final view_by_choice:I = 0x7f09003e

.field public static final view_by_choice_chn:I = 0x7f090041

.field public static final view_format_by_choice:I = 0x7f09003f

.field public static final view_format_by_choice_chn:I = 0x7f090040

.field public static final whitebalance_icons:I = 0x7f09001d

.field public static final whitebalance_largeicons:I = 0x7f09001e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
