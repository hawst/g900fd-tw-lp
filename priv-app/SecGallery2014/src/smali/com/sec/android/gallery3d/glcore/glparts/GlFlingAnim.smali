.class public Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlFlingAnim.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;
    }
.end annotation


# instance fields
.field private mDamping:Z

.field private mDampingCoeff:F

.field private mDec:F

.field private mDecBounc:F

.field private mDstX:F

.field protected mFlingListener:Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

.field public mInitSpeed:F

.field private mLx:F

.field private mMaxX:F

.field private mMinX:F

.field public mSpeed:F

.field private mStopDelta:F

.field private mStopSpeed:F

.field private mSx:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 6
    const v0, 0x3f7ae148    # 0.98f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDec:F

    .line 7
    const v0, 0x3f59999a    # 0.85f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDecBounc:F

    .line 8
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDampingCoeff:F

    .line 9
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mStopSpeed:F

    .line 10
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mStopDelta:F

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mFlingListener:Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

    .line 96
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 6
    .param p1, "ratio"    # F

    .prologue
    const/4 v3, 0x1

    const/high16 v5, 0x42c80000    # 100.0f

    .line 59
    const/4 v0, 0x0

    .line 61
    .local v0, "needStop":Z
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDamping:Z

    if-nez v1, :cond_6

    .line 62
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDec:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    .line 63
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mLx:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    .line 64
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mMaxX:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_4

    .line 65
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mMaxX:F

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDstX:F

    .line 66
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDamping:Z

    .line 82
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mLx:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_2

    .line 83
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mFlingListener:Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

    if-eqz v1, :cond_1

    .line 84
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mFlingListener:Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;->onFlingProcess(F)V

    .line 85
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mLx:F

    .line 87
    :cond_2
    if-eqz v0, :cond_3

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->stop()V

    .line 89
    :cond_3
    return-void

    .line 67
    :cond_4
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mMinX:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5

    .line 68
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mMinX:F

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDstX:F

    .line 69
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDamping:Z

    goto :goto_0

    .line 70
    :cond_5
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mStopSpeed:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 71
    const/4 v0, 0x1

    goto :goto_0

    .line 74
    :cond_6
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDampingCoeff:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDstX:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    .line 75
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    .line 76
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDecBounc:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    .line 77
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mStopSpeed:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDstX:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mStopDelta:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 78
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDstX:F

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    .line 79
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSpeedRatio()F
    .locals 3

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDamping:Z

    if-nez v0, :cond_0

    .line 38
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mInitSpeed:F

    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mLastRatio:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mFlingListener:Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mFlingListener:Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;->onFlingEnd(F)V

    .line 94
    :cond_0
    return-void
.end method

.method public setFactors(FFF)V
    .locals 0
    .param p1, "decelleration"    # F
    .param p2, "bouncingDec"    # F
    .param p3, "damping"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDec:F

    .line 45
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDecBounc:F

    .line 46
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDampingCoeff:F

    .line 47
    return-void
.end method

.method public setFlingAnimationListener(Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mFlingListener:Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim$GlFlingAnimListener;

    .line 56
    return-void
.end method

.method public setRange(FF)V
    .locals 0
    .param p1, "minX"    # F
    .param p2, "maxX"    # F

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mMinX:F

    .line 33
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mMaxX:F

    .line 34
    return-void
.end method

.method public setStopSpeed(F)V
    .locals 1
    .param p1, "speed"    # F

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mStopSpeed:F

    .line 51
    const/high16 v0, 0x40800000    # 4.0f

    div-float v0, p1, v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mStopDelta:F

    .line 52
    return-void
.end method

.method public startFling(FF)V
    .locals 1
    .param p1, "startX"    # F
    .param p2, "speed"    # F

    .prologue
    .line 23
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mInitSpeed:F

    .line 24
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSpeed:F

    .line 25
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    .line 26
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mSx:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mLx:F

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->mDamping:Z

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/glparts/GlFlingAnim;->start()V

    .line 29
    return-void
.end method
