.class public final Lcom/sec/android/gallery3d/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ChnUsageAlertDialog:I = 0x7f0f0082

.field public static final CreateVideoClip:I = 0x7f0f0220

.field public static final LinearLayout:I = 0x7f0f0031

.field public static final ScrollView01:I = 0x7f0f00fb

.field public static final account_page_breadcrumbs:I = 0x7f0f0009

.field public static final acount_name:I = 0x7f0f000d

.field public static final action_MultipleMemolink:I = 0x7f0f029e

.field public static final action_add_tag:I = 0x7f0f0282

.field public static final action_add_weather_tag:I = 0x7f0f02aa

.field public static final action_allshare_change_player:I = 0x7f0f0299

.field public static final action_assign_name:I = 0x7f0f0273

.field public static final action_bar_album_name_count:I = 0x7f0f0016

.field public static final action_bar_title:I = 0x7f0f0010

.field public static final action_burst_shot_play:I = 0x7f0f02a3

.field public static final action_buttons:I = 0x7f0f002e

.field public static final action_camera:I = 0x7f0f0251

.field public static final action_camera_detail:I = 0x7f0f02c1

.field public static final action_camera_photo:I = 0x7f0f02c3

.field public static final action_cancel:I = 0x7f0f0022

.field public static final action_cluster_album:I = 0x7f0f025f

.field public static final action_cluster_faces:I = 0x7f0f0264

.field public static final action_cluster_location:I = 0x7f0f0261

.field public static final action_cluster_size:I = 0x7f0f0263

.field public static final action_cluster_tags:I = 0x7f0f0262

.field public static final action_cluster_time:I = 0x7f0f0260

.field public static final action_confirm:I = 0x7f0f0272

.field public static final action_copy:I = 0x7f0f027d

.field public static final action_copy_to_album:I = 0x7f0f02bc

.field public static final action_copy_to_clipboard:I = 0x7f0f02a7

.field public static final action_copy_to_event:I = 0x7f0f027f

.field public static final action_create_event_album:I = 0x7f0f02be

.field public static final action_create_story_album:I = 0x7f0f0275

.field public static final action_create_video_album:I = 0x7f0f02cd

.field public static final action_crop:I = 0x7f0f02a8

.field public static final action_delete:I = 0x7f0f026e

.field public static final action_delete_done:I = 0x7f0f027b

.field public static final action_delete_history:I = 0x7f0f02d8

.field public static final action_deselect_all:I = 0x7f0f02e3

.field public static final action_details:I = 0x7f0f029b

.field public static final action_done:I = 0x7f0f0024

.field public static final action_download_cloud:I = 0x7f0f028f

.field public static final action_download_slink:I = 0x7f0f02a1

.field public static final action_download_via_download_manager:I = 0x7f0f0276

.field public static final action_edit:I = 0x7f0f029c

.field public static final action_edit_burst_shot:I = 0x7f0f02b4

.field public static final action_edit_category:I = 0x7f0f02cb

.field public static final action_flashannotate:I = 0x7f0f02a5

.field public static final action_general_help:I = 0x7f0f0258

.field public static final action_get_text:I = 0x7f0f02b1

.field public static final action_gif_done:I = 0x7f0f02ce

.field public static final action_gif_maker:I = 0x7f0f02d5

.field public static final action_grid_view:I = 0x7f0f0293

.field public static final action_group_by:I = 0x7f0f0254

.field public static final action_help:I = 0x7f0f028a

.field public static final action_hide:I = 0x7f0f0271

.field public static final action_home:I = 0x7f0f0284

.field public static final action_imagenote:I = 0x7f0f02a4

.field public static final action_import:I = 0x7f0f027c

.field public static final action_last_share:I = 0x7f0f026d

.field public static final action_manage_offline:I = 0x7f0f0255

.field public static final action_manual_detection:I = 0x7f0f02b8

.field public static final action_memolink:I = 0x7f0f029d

.field public static final action_mms_save:I = 0x7f0f02b2

.field public static final action_more:I = 0x7f0f02c4

.field public static final action_moreinfo:I = 0x7f0f02b7

.field public static final action_motion:I = 0x7f0f02ae

.field public static final action_motion_picture:I = 0x7f0f02b3

.field public static final action_move:I = 0x7f0f027e

.field public static final action_move_to_album:I = 0x7f0f02c6

.field public static final action_move_to_knox:I = 0x7f0f0279

.field public static final action_move_to_secretbox:I = 0x7f0f0277

.field public static final action_mute:I = 0x7f0f02e2

.field public static final action_ok:I = 0x7f0f02dd

.field public static final action_photo_editor:I = 0x7f0f02ab

.field public static final action_photo_navigation:I = 0x7f0f02b0

.field public static final action_printer:I = 0x7f0f0285

.field public static final action_refresh:I = 0x7f0f02c9

.field public static final action_remove_from_category:I = 0x7f0f0297

.field public static final action_remove_from_event:I = 0x7f0f02b6

.field public static final action_remove_from_knox:I = 0x7f0f027a

.field public static final action_remove_from_secretbox:I = 0x7f0f0278

.field public static final action_remove_sound:I = 0x7f0f02b5

.field public static final action_remove_tag:I = 0x7f0f0274

.field public static final action_rename:I = 0x7f0f0270

.field public static final action_reorder:I = 0x7f0f0292

.field public static final action_rotate:I = 0x7f0f0298

.field public static final action_rotate_ccw:I = 0x7f0f0280

.field public static final action_rotate_cw:I = 0x7f0f0281

.field public static final action_save:I = 0x7f0f02de

.field public static final action_search:I = 0x7f0f028e

.field public static final action_search_text:I = 0x7f0f02d3

.field public static final action_select:I = 0x7f0f0253

.field public static final action_select_all:I = 0x7f0f0001

.field public static final action_send_to_other_devices:I = 0x7f0f02ad

.field public static final action_setas:I = 0x7f0f029a

.field public static final action_settings:I = 0x7f0f0257

.field public static final action_share:I = 0x7f0f026c

.field public static final action_share_panorama:I = 0x7f0f02df

.field public static final action_show:I = 0x7f0f02a2

.field public static final action_show_on_map:I = 0x7f0f02af

.field public static final action_signature:I = 0x7f0f02a6

.field public static final action_simple_edit:I = 0x7f0f02e0

.field public static final action_slideshow:I = 0x7f0f0252

.field public static final action_slideshow_setting:I = 0x7f0f026f

.field public static final action_sort_by_latest:I = 0x7f0f0290

.field public static final action_sort_by_oldest:I = 0x7f0f0291

.field public static final action_sstudio:I = 0x7f0f0283

.field public static final action_start:I = 0x7f0f02d9

.field public static final action_sync_picasa_albums:I = 0x7f0f0256

.field public static final action_toggle_full_caching:I = 0x7f0f0000

.field public static final action_trim:I = 0x7f0f02e1

.field public static final action_up_button:I = 0x7f0f024b

.field public static final action_upload:I = 0x7f0f02a0

.field public static final action_video_maker:I = 0x7f0f02ac

.field public static final action_video_trim:I = 0x7f0f029f

.field public static final action_weather_tag:I = 0x7f0f02a9

.field public static final add_account:I = 0x7f0f0250

.field public static final add_more_btn:I = 0x7f0f003c

.field public static final addmusic:I = 0x7f0f0218

.field public static final addmusicicon:I = 0x7f0f0219

.field public static final addtag_actionselector:I = 0x7f0f0053

.field public static final addtag_select_taglist:I = 0x7f0f0054

.field public static final air_browse_text:I = 0x7f0f0059

.field public static final album_header_image:I = 0x7f0f005c

.field public static final album_header_subtitle:I = 0x7f0f005e

.field public static final album_header_title:I = 0x7f0f005d

.field public static final album_set_item_count:I = 0x7f0f0060

.field public static final album_set_item_image:I = 0x7f0f0061

.field public static final album_set_item_title:I = 0x7f0f005f

.field public static final album_view_content_to_display:I = 0x7f0f0289

.field public static final album_view_download_cloud_group:I = 0x7f0f02ba

.field public static final album_view_edit_menu_group:I = 0x7f0f026b

.field public static final album_view_get_content_menu_group:I = 0x7f0f0286

.field public static final album_view_hidden_album:I = 0x7f0f0287

.field public static final album_view_multipick_menu_group:I = 0x7f0f02c5

.field public static final album_view_new_album:I = 0x7f0f028d

.field public static final album_view_view_menu_group:I = 0x7f0f028b

.field public static final allshare_player_list_layout:I = 0x7f0f006b

.field public static final alwaysScroll:I = 0x7f0f0003

.field public static final applist_image:I = 0x7f0f0062

.field public static final applist_text:I = 0x7f0f0063

.field public static final appwidget_empty_view:I = 0x7f0f006c

.field public static final appwidget_photo_item:I = 0x7f0f006d

.field public static final appwidget_stack_view:I = 0x7f0f006e

.field public static final arrow_layout:I = 0x7f0f01ba

.field public static final background_image:I = 0x7f0f01f5

.field public static final background_setting:I = 0x7f0f023a

.field public static final balloon_inner_layout:I = 0x7f0f017d

.field public static final balloon_item_title:I = 0x7f0f017b

.field public static final balloon_main_layout:I = 0x7f0f0179

.field public static final balloon_title_layout:I = 0x7f0f017a

.field public static final base_details:I = 0x7f0f00dc

.field public static final base_layout:I = 0x7f0f00f7

.field public static final bottom_img:I = 0x7f0f017f

.field public static final btn_close:I = 0x7f0f00d9

.field public static final btn_delete:I = 0x7f0f0120

.field public static final btn_history_delete_all:I = 0x7f0f011e

.field public static final btn_login:I = 0x7f0f000e

.field public static final btn_save:I = 0x7f0f00c0

.field public static final bua_media_vux:I = 0x7f0f028c

.field public static final button:I = 0x7f0f015e

.field public static final camera_button:I = 0x7f0f0012

.field public static final cancel:I = 0x7f0f0023

.field public static final cancel_button:I = 0x7f0f0288

.field public static final canvas_container:I = 0x7f0f0150

.field public static final canvas_default_background:I = 0x7f0f0151

.field public static final category_view_edit_menu_group:I = 0x7f0f0296

.field public static final category_view_menu_group:I = 0x7f0f0295

.field public static final change_player_check:I = 0x7f0f0074

.field public static final change_player_description:I = 0x7f0f0073

.field public static final change_player_dialog_title:I = 0x7f0f0076

.field public static final change_player_icon:I = 0x7f0f0071

.field public static final change_player_name:I = 0x7f0f0072

.field public static final check_box:I = 0x7f0f0210

.field public static final checkbox:I = 0x7f0f001d

.field public static final checked:I = 0x7f0f024e

.field public static final close_btn:I = 0x7f0f013e

.field public static final close_cancel_btn:I = 0x7f0f014d

.field public static final close_done_btn:I = 0x7f0f014e

.field public static final close_taglist_btn:I = 0x7f0f0113

.field public static final close_tool_bar:I = 0x7f0f014b

.field public static final color_picker_container:I = 0x7f0f0153

.field public static final comment_content:I = 0x7f0f007f

.field public static final comment_empty:I = 0x7f0f0080

.field public static final comment_from_icon:I = 0x7f0f0087

.field public static final comment_from_name:I = 0x7f0f0089

.field public static final comment_list:I = 0x7f0f0081

.field public static final comment_list_item_layout:I = 0x7f0f000b

.field public static final comment_text:I = 0x7f0f008a

.field public static final comment_time:I = 0x7f0f008b

.field public static final comment_title:I = 0x7f0f0088

.field public static final contact_btn_layout:I = 0x7f0f0090

.field public static final contact_call:I = 0x7f0f0091

.field public static final contact_chat_chn:I = 0x7f0f0099

.field public static final contact_email:I = 0x7f0f0093

.field public static final contact_more:I = 0x7f0f008f

.field public static final contact_more_area:I = 0x7f0f008d

.field public static final contact_msg:I = 0x7f0f0092

.field public static final contact_name:I = 0x7f0f008e

.field public static final contact_social:I = 0x7f0f0094

.field public static final contact_social_chn:I = 0x7f0f0095

.field public static final content_frame:I = 0x7f0f0028

.field public static final content_list:I = 0x7f0f009d

.field public static final content_subtitle:I = 0x7f0f009c

.field public static final content_to_display_dialog_layout:I = 0x7f0f009b

.field public static final contextual_auto_update:I = 0x7f0f0204

.field public static final contextual_checkbox:I = 0x7f0f00a1

.field public static final contextual_comment_preview:I = 0x7f0f0205

.field public static final contextual_data:I = 0x7f0f00a0

.field public static final contextual_weather:I = 0x7f0f00a2

.field public static final contextual_wlan_update:I = 0x7f0f0206

.field public static final countText:I = 0x7f0f00c2

.field public static final cover_close_btn:I = 0x7f0f00be

.field public static final date_tile_day:I = 0x7f0f015b

.field public static final date_tile_month:I = 0x7f0f0159

.field public static final date_tile_year:I = 0x7f0f015a

.field public static final delete:I = 0x7f0f0267

.field public static final detail_aperture:I = 0x7f0f00d5

.field public static final detail_exposure_time:I = 0x7f0f00d6

.field public static final detail_flash:I = 0x7f0f00d7

.field public static final detail_frag_header:I = 0x7f0f0008

.field public static final detail_iso:I = 0x7f0f00d3

.field public static final detail_model:I = 0x7f0f00d0

.field public static final detail_table:I = 0x7f0f00d2

.field public static final detail_view_help_menu_group:I = 0x7f0f02c0

.field public static final detail_white_balance:I = 0x7f0f00d4

.field public static final details_list:I = 0x7f0f00db

.field public static final disabled:I = 0x7f0f0004

.field public static final divide_details:I = 0x7f0f00da

.field public static final divide_extra_details:I = 0x7f0f00dd

.field public static final done:I = 0x7f0f0178

.field public static final done_and_cancel:I = 0x7f0f02b9

.field public static final drawer_help:I = 0x7f0f00e0

.field public static final drawer_layout:I = 0x7f0f0027

.field public static final drawer_layout_parent:I = 0x7f0f0026

.field public static final editComment:I = 0x7f0f007a

.field public static final edit_group:I = 0x7f0f0114

.field public static final effect_title:I = 0x7f0f0221

.field public static final empty_history:I = 0x7f0f0207

.field public static final eraser_btn:I = 0x7f0f0141

.field public static final event_view_edit_menu_group:I = 0x7f0f02bb

.field public static final event_view_view_menu_group:I = 0x7f0f02bd

.field public static final eventview_album:I = 0x7f0f00ec

.field public static final eventview_album_location:I = 0x7f0f00f5

.field public static final eventview_album_time:I = 0x7f0f00f6

.field public static final eventview_button:I = 0x7f0f02c8

.field public static final eventview_checkbox_off:I = 0x7f0f00f2

.field public static final eventview_checkbox_on:I = 0x7f0f00f3

.field public static final eventview_checkui:I = 0x7f0f00f1

.field public static final eventview_mapbutton:I = 0x7f0f00f4

.field public static final eventview_suggestion:I = 0x7f0f00f0

.field public static final eventview_thumbnail:I = 0x7f0f00ee

.field public static final eventview_thumbnail_container:I = 0x7f0f00ed

.field public static final eventview_thumbnail_video:I = 0x7f0f00ef

.field public static final expand_button:I = 0x7f0f017c

.field public static final expansion_mode_divider:I = 0x7f0f001c

.field public static final export:I = 0x7f0f0266

.field public static final fast_text:I = 0x7f0f01d7

.field public static final filterLinearLayout:I = 0x7f0f0228

.field public static final filter_details:I = 0x7f0f0107

.field public static final filter_group_arrow:I = 0x7f0f0108

.field public static final filter_group_event:I = 0x7f0f0103

.field public static final filter_group_location:I = 0x7f0f0104

.field public static final filter_group_pager:I = 0x7f0f0109

.field public static final filter_group_person:I = 0x7f0f0105

.field public static final filter_group_time:I = 0x7f0f0102

.field public static final filter_group_usertag:I = 0x7f0f0106

.field public static final filter_groups:I = 0x7f0f0101

.field public static final filter_lists:I = 0x7f0f00ff

.field public static final filter_panel:I = 0x7f0f00fe

.field public static final filter_title:I = 0x7f0f0226

.field public static final focus_help_body:I = 0x7f0f010f

.field public static final focus_help_close_btn:I = 0x7f0f0111

.field public static final focus_shot_help:I = 0x7f0f010d

.field public static final focus_text_body:I = 0x7f0f0110

.field public static final format_list:I = 0x7f0f009f

.field public static final format_subtitle:I = 0x7f0f009e

.field public static final frame_title:I = 0x7f0f00d8

.field public static final gallery_search_view_divider:I = 0x7f0f011a

.field public static final gallerysearch_view:I = 0x7f0f0019

.field public static final gl_root_group:I = 0x7f0f0176

.field public static final gl_root_view:I = 0x7f0f00bf

.field public static final googlemap:I = 0x7f0f01a0

.field public static final grayscale_image:I = 0x7f0f022c

.field public static final grayscale_text:I = 0x7f0f022d

.field public static final gridContainer:I = 0x7f0f005b

.field public static final grid_thumbnail:I = 0x7f0f017e

.field public static final gridlist_in_dialog:I = 0x7f0f0064

.field public static final head:I = 0x7f0f008c

.field public static final header_text:I = 0x7f0f006f

.field public static final help_bg:I = 0x7f0f0130

.field public static final help_body:I = 0x7f0f00e2

.field public static final help_close:I = 0x7f0f00e1

.field public static final help_close_btn:I = 0x7f0f010e

.field public static final help_custom_popup_tap:I = 0x7f0f012a

.field public static final help_face_name1:I = 0x7f0f012c

.field public static final help_face_tap:I = 0x7f0f012d

.field public static final help_hover:I = 0x7f0f0134

.field public static final help_hover_arrow:I = 0x7f0f0135

.field public static final help_item:I = 0x7f0f0133

.field public static final help_pan_tap:I = 0x7f0f01c2

.field public static final help_pan_text_1:I = 0x7f0f01c4

.field public static final help_pan_text_1_head:I = 0x7f0f01c5

.field public static final help_pan_text_2:I = 0x7f0f01bf

.field public static final help_pan_text_2_head:I = 0x7f0f01c0

.field public static final help_pan_text_complete:I = 0x7f0f01c6

.field public static final help_peek_tap:I = 0x7f0f01c9

.field public static final help_peek_text_1:I = 0x7f0f01ca

.field public static final help_peek_text_1_head:I = 0x7f0f01cb

.field public static final help_peek_text_2:I = 0x7f0f01c7

.field public static final help_peek_text_2_head:I = 0x7f0f01c8

.field public static final help_peek_text_complete:I = 0x7f0f01cc

.field public static final help_popup:I = 0x7f0f0129

.field public static final help_popup_face_head:I = 0x7f0f012f

.field public static final help_popup_face_text:I = 0x7f0f012e

.field public static final help_popup_head:I = 0x7f0f013a

.field public static final help_popup_tap:I = 0x7f0f013b

.field public static final help_popup_text:I = 0x7f0f0128

.field public static final help_popup_text2:I = 0x7f0f013c

.field public static final help_relative_layout:I = 0x7f0f0127

.field public static final help_scroll:I = 0x7f0f0131

.field public static final help_scroll_image:I = 0x7f0f0132

.field public static final help_tilt_tap_1:I = 0x7f0f01d1

.field public static final help_tilt_tap_2:I = 0x7f0f01d2

.field public static final help_tilt_text:I = 0x7f0f01ce

.field public static final help_tilt_text_complete:I = 0x7f0f01d0

.field public static final help_viewing_picture_detailview_cue_img:I = 0x7f0f01c3

.field public static final hint:I = 0x7f0f00fd

.field public static final histogram:I = 0x7f0f00d1

.field public static final histogram_info:I = 0x7f0f00ce

.field public static final history_frame:I = 0x7f0f010a

.field public static final history_icon:I = 0x7f0f011b

.field public static final history_layout:I = 0x7f0f011f

.field public static final history_list:I = 0x7f0f020f

.field public static final history_name:I = 0x7f0f011d

.field public static final history_time:I = 0x7f0f011c

.field public static final home_button:I = 0x7f0f0011

.field public static final hscroller:I = 0x7f0f0227

.field public static final icon:I = 0x7f0f0213

.field public static final image:I = 0x7f0f0112

.field public static final imageView1:I = 0x7f0f0136

.field public static final imageView2:I = 0x7f0f0137

.field public static final imageView3:I = 0x7f0f0138

.field public static final imageView4:I = 0x7f0f0139

.field public static final image_btn_glass:I = 0x7f0f0115

.field public static final image_divider_history:I = 0x7f0f0122

.field public static final image_icon:I = 0x7f0f000c

.field public static final image_noitems:I = 0x7f0f0161

.field public static final image_noitems_bg:I = 0x7f0f0163

.field public static final image_view:I = 0x7f0f00df

.field public static final imageview1:I = 0x7f0f0215

.field public static final img:I = 0x7f0f01b8

.field public static final img_current_location:I = 0x7f0f002d

.field public static final import_items:I = 0x7f0f026a

.field public static final ingest_fullsize_image:I = 0x7f0f015c

.field public static final ingest_fullsize_image_checkbox:I = 0x7f0f015d

.field public static final ingest_gridview:I = 0x7f0f0154

.field public static final ingest_switch_view:I = 0x7f0f0269

.field public static final ingest_view_pager:I = 0x7f0f0155

.field public static final ingest_warning_view:I = 0x7f0f0156

.field public static final ingest_warning_view_icon:I = 0x7f0f0157

.field public static final ingest_warning_view_text:I = 0x7f0f0158

.field public static final knox_first_img:I = 0x7f0f01db

.field public static final knox_first_text:I = 0x7f0f01dd

.field public static final knox_second_img:I = 0x7f0f01dc

.field public static final knox_second_text:I = 0x7f0f01de

.field public static final latest_comment:I = 0x7f0f0242

.field public static final latest_comment_empty:I = 0x7f0f0243

.field public static final layout:I = 0x7f0f021b

.field public static final layout_advanced:I = 0x7f0f00cb

.field public static final layout_container:I = 0x7f0f014f

.field public static final layout_divider:I = 0x7f0f004f

.field public static final layout_effect:I = 0x7f0f00c7

.field public static final layout_filter:I = 0x7f0f0225

.field public static final layout_id:I = 0x7f0f000f

.field public static final layout_music:I = 0x7f0f00c9

.field public static final layout_noitems_description:I = 0x7f0f015f

.field public static final layout_port:I = 0x7f0f021c

.field public static final layout_preview_category:I = 0x7f0f00b3

.field public static final layout_preview_date:I = 0x7f0f00af

.field public static final layout_preview_location:I = 0x7f0f00b1

.field public static final layout_preview_user_tags:I = 0x7f0f00b5

.field public static final layout_root:I = 0x7f0f00c5

.field public static final layout_speed:I = 0x7f0f0222

.field public static final layout_video:I = 0x7f0f0232

.field public static final layout_wrapper:I = 0x7f0f0247

.field public static final left_buttons:I = 0x7f0f0013

.field public static final left_drawer:I = 0x7f0f0029

.field public static final left_drawer_child:I = 0x7f0f002a

.field public static final left_view:I = 0x7f0f0200

.field public static final linearlayout1:I = 0x7f0f0214

.field public static final list_advanced:I = 0x7f0f00cc

.field public static final list_divider:I = 0x7f0f00e9

.field public static final list_effect:I = 0x7f0f00c8

.field public static final list_empty:I = 0x7f0f00cd

.field public static final list_header_title:I = 0x7f0f00e5

.field public static final list_history:I = 0x7f0f010b

.field public static final list_item:I = 0x7f0f0164

.field public static final list_item_icon:I = 0x7f0f00e6

.field public static final list_item_title:I = 0x7f0f00e7

.field public static final list_item_txtview:I = 0x7f0f0165

.field public static final list_music:I = 0x7f0f00ca

.field public static final list_speed:I = 0x7f0f0224

.field public static final list_top_divider:I = 0x7f0f00e4

.field public static final loading_comment_screen:I = 0x7f0f007c

.field public static final magazine_widget_button:I = 0x7f0f016e

.field public static final magazine_widget_camera_button:I = 0x7f0f016f

.field public static final magazine_widget_preference_button:I = 0x7f0f01ed

.field public static final main_frame:I = 0x7f0f01d8

.field public static final main_relativelayout:I = 0x7f0f0175

.field public static final map:I = 0x7f0f0190

.field public static final map_address:I = 0x7f0f0183

.field public static final map_grid_thumbnail:I = 0x7f0f0184

.field public static final map_list_thumbnail_layout:I = 0x7f0f0182

.field public static final map_marker:I = 0x7f0f0181

.field public static final map_relativelayout:I = 0x7f0f0180

.field public static final map_view_menu_group:I = 0x7f0f02c7

.field public static final menu_camera:I = 0x7f0f025b

.field public static final menu_cancel:I = 0x7f0f0259

.field public static final menu_done:I = 0x7f0f025a

.field public static final menu_help:I = 0x7f0f025e

.field public static final menu_search:I = 0x7f0f025c

.field public static final menu_send:I = 0x7f0f02e4

.field public static final menu_settings:I = 0x7f0f025d

.field public static final menu_vzcloud:I = 0x7f0f0294

.field public static final message:I = 0x7f0f006a

.field public static final message_1:I = 0x7f0f0050

.field public static final message_2:I = 0x7f0f01b9

.field public static final mode_list:I = 0x7f0f00b7

.field public static final moreButton:I = 0x7f0f0244

.field public static final more_comments:I = 0x7f0f0077

.field public static final moreinfo_body:I = 0x7f0f0197

.field public static final moreinfo_category_dialog_listitem_textview:I = 0x7f0f0185

.field public static final moreinfo_category_dialog_listview:I = 0x7f0f0186

.field public static final moreinfo_container:I = 0x7f0f0055

.field public static final moreinfo_details_list:I = 0x7f0f0199

.field public static final moreinfo_edit_layout:I = 0x7f0f002b

.field public static final moreinfo_expandable_subitem_delete:I = 0x7f0f0187

.field public static final moreinfo_expandable_subitem_delete_icon:I = 0x7f0f0189

.field public static final moreinfo_expandable_subitem_delete_textview:I = 0x7f0f0188

.field public static final moreinfo_expandablelayout:I = 0x7f0f018a

.field public static final moreinfo_item_content:I = 0x7f0f018b

.field public static final moreinfo_item_expandablelayout_container:I = 0x7f0f019c

.field public static final moreinfo_item_iconview:I = 0x7f0f018c

.field public static final moreinfo_item_title:I = 0x7f0f018d

.field public static final moreinfo_item_title_addcontent_imageview:I = 0x7f0f018e

.field public static final moreinfo_location_editor_gps_popup:I = 0x7f0f0191

.field public static final moreinfo_location_item_edit:I = 0x7f0f0192

.field public static final moreinfo_location_item_edit_marker:I = 0x7f0f0193

.field public static final moreinfo_location_item_edit_nolocation:I = 0x7f0f0194

.field public static final moreinfo_scrollview:I = 0x7f0f0196

.field public static final moreinfo_set_details:I = 0x7f0f0198

.field public static final moreinfo_set_expandable:I = 0x7f0f019b

.field public static final moreinfo_set_location:I = 0x7f0f019e

.field public static final moreinfo_set_location_map_textview:I = 0x7f0f019f

.field public static final moreinfo_set_simple:I = 0x7f0f01a1

.field public static final moreinfo_subcontainer:I = 0x7f0f0195

.field public static final moreinfo_usertag_addbutton:I = 0x7f0f01a7

.field public static final moreinfo_usertag_edit_actionselector:I = 0x7f0f01a3

.field public static final moreinfo_usertag_edit_container:I = 0x7f0f01a8

.field public static final moreinfo_usertag_edit_edittext_add_tag:I = 0x7f0f01a5

.field public static final moreinfo_usertag_edit_edittext_run_edit:I = 0x7f0f01a4

.field public static final moreinfo_usertag_edit_edittextbox:I = 0x7f0f01a2

.field public static final moreinfo_usertag_edit_listitem_checkbox:I = 0x7f0f01ab

.field public static final moreinfo_usertag_edit_listitem_textview:I = 0x7f0f01aa

.field public static final moreinfo_usertag_edittextbox:I = 0x7f0f01a6

.field public static final moreinfo_usertag_item_edit:I = 0x7f0f01ac

.field public static final moreinfo_usertag_item_edit_edittext_set:I = 0x7f0f019d

.field public static final moreinfo_usertag_item_edit_expandableset:I = 0x7f0f01b1

.field public static final moreinfo_usertag_item_edit_face_expandable:I = 0x7f0f01b2

.field public static final moreinfo_usertag_item_edit_face_textView:I = 0x7f0f01ae

.field public static final moreinfo_usertag_item_edit_subcat_expandable:I = 0x7f0f01b3

.field public static final moreinfo_usertag_item_edit_subcat_textView:I = 0x7f0f01af

.field public static final moreinfo_usertag_item_edit_textset:I = 0x7f0f01ad

.field public static final moreinfo_usertag_item_edit_usertag_expandable:I = 0x7f0f01b4

.field public static final moreinfo_usertag_item_edit_usertag_textView:I = 0x7f0f01b0

.field public static final moreinfo_usertag_noitems:I = 0x7f0f01b5

.field public static final moreinfo_usertag_taglistview:I = 0x7f0f01a9

.field public static final move_btn:I = 0x7f0f0144

.field public static final movie_view_root:I = 0x7f0f01df

.field public static final musicName:I = 0x7f0f0239

.field public static final musicSelect:I = 0x7f0f0238

.field public static final musicTitleView:I = 0x7f0f0237

.field public static final musicView:I = 0x7f0f0236

.field public static final music_divider:I = 0x7f0f0167

.field public static final music_name:I = 0x7f0f0166

.field public static final music_switch:I = 0x7f0f0168

.field public static final music_title:I = 0x7f0f0235

.field public static final name:I = 0x7f0f0216

.field public static final navigation_bar:I = 0x7f0f0017

.field public static final normal:I = 0x7f0f0005

.field public static final number_of_selected_items:I = 0x7f0f0014

.field public static final ocr_popup_cancel_btn:I = 0x7f0f01e2

.field public static final ocr_popup_item_icon:I = 0x7f0f01e4

.field public static final ocr_popup_item_title:I = 0x7f0f01e5

.field public static final ocr_popup_list:I = 0x7f0f01e3

.field public static final ocr_popup_title:I = 0x7f0f01e1

.field public static final onclick_wrapper:I = 0x7f0f016b

.field public static final open_btn:I = 0x7f0f014c

.field public static final open_cancel_btn:I = 0x7f0f0148

.field public static final open_done_btn:I = 0x7f0f0146

.field public static final open_right_btn_group:I = 0x7f0f0145

.field public static final open_tool_bar:I = 0x7f0f013d

.field public static final pen_btn:I = 0x7f0f013f

.field public static final pen_btn_color:I = 0x7f0f0140

.field public static final pen_btn_icon:I = 0x7f0f014a

.field public static final people_view_edit_menu_group:I = 0x7f0f02ca

.field public static final percentText:I = 0x7f0f00c3

.field public static final photo:I = 0x7f0f01e6

.field public static final photo_frame_content:I = 0x7f0f0169

.field public static final photo_frame_empty_view:I = 0x7f0f016c

.field public static final photo_frame_flipper:I = 0x7f0f016a

.field public static final photo_frame_flipper_item:I = 0x7f0f01e9

.field public static final photo_frame_flipper_item_0:I = 0x7f0f01ea

.field public static final photo_frame_flipper_item_1:I = 0x7f0f01eb

.field public static final photo_frame_flipper_item_2:I = 0x7f0f01ec

.field public static final photo_frame_flipper_magazine_item_0:I = 0x7f0f0173

.field public static final photo_frame_flipper_magazine_item_1:I = 0x7f0f0174

.field public static final photo_frame_preference_button:I = 0x7f0f01e8

.field public static final photo_view_edit_menu_group:I = 0x7f0f02cc

.field public static final photo_view_help_menu_group:I = 0x7f0f02c2

.field public static final photo_view_hidden_album:I = 0x7f0f02d4

.field public static final photo_view_multipick_menu_group:I = 0x7f0f02cf

.field public static final photo_view_new_album:I = 0x7f0f02d2

.field public static final photo_view_pick_menu_group:I = 0x7f0f02d0

.field public static final photo_view_view_menu_group:I = 0x7f0f02d1

.field public static final photopage_bottom_control_edit:I = 0x7f0f01f8

.field public static final photopage_bottom_control_panorama:I = 0x7f0f01f9

.field public static final photopage_bottom_control_tiny_planet:I = 0x7f0f01fa

.field public static final photopage_bottom_controls:I = 0x7f0f01f7

.field public static final photopage_progress_background:I = 0x7f0f01fc

.field public static final photopage_progress_bar:I = 0x7f0f01fb

.field public static final photopage_progress_bar_text:I = 0x7f0f01fe

.field public static final photopage_progress_foreground:I = 0x7f0f01fd

.field public static final pictureframe_blank_btn_add:I = 0x7f0f01e7

.field public static final pictureframe_default_widget:I = 0x7f0f016d

.field public static final player_list_title:I = 0x7f0f0075

.field public static final presentationVideoView:I = 0x7f0f00fa

.field public static final presentation_layout:I = 0x7f0f01ff

.field public static final preview:I = 0x7f0f00cf

.field public static final preview_category:I = 0x7f0f00a5

.field public static final preview_category_icon:I = 0x7f0f00b4

.field public static final preview_date:I = 0x7f0f00a3

.field public static final preview_date_icon:I = 0x7f0f00b0

.field public static final preview_image:I = 0x7f0f0051

.field public static final preview_location:I = 0x7f0f00a4

.field public static final preview_location_icon:I = 0x7f0f00b2

.field public static final preview_person:I = 0x7f0f00a9

.field public static final preview_user_tags:I = 0x7f0f00a6

.field public static final preview_user_tags_icon:I = 0x7f0f00b6

.field public static final preview_weather:I = 0x7f0f00a8

.field public static final print:I = 0x7f0f0268

.field public static final processing_messgae:I = 0x7f0f00c4

.field public static final progress:I = 0x7f0f00fc

.field public static final progressBar:I = 0x7f0f00c1

.field public static final progressBar_loading:I = 0x7f0f007d

.field public static final progressContainer:I = 0x7f0f005a

.field public static final progress_spinner:I = 0x7f0f0070

.field public static final quick_scroll_help:I = 0x7f0f0202

.field public static final qzone_checkbox:I = 0x7f0f004d

.field public static final qzone_icon:I = 0x7f0f004c

.field public static final qzone_name:I = 0x7f0f004e

.field public static final recommend_text:I = 0x7f0f0203

.field public static final redo_btn:I = 0x7f0f0142

.field public static final relativeLayout1:I = 0x7f0f012b

.field public static final remove:I = 0x7f0f010c

.field public static final renren_checkbox:I = 0x7f0f0048

.field public static final renren_icon:I = 0x7f0f0047

.field public static final renren_name:I = 0x7f0f0049

.field public static final renrenwang_checkbox:I = 0x7f0f0046

.field public static final right_view:I = 0x7f0f0201

.field public static final save:I = 0x7f0f0025

.field public static final scroller:I = 0x7f0f00ae

.field public static final search:I = 0x7f0f002c

.field public static final search_bar:I = 0x7f0f00ea

.field public static final search_close_btn:I = 0x7f0f0118

.field public static final search_edit_layout:I = 0x7f0f018f

.field public static final search_plate:I = 0x7f0f0208

.field public static final search_progress:I = 0x7f0f0119

.field public static final search_src_text:I = 0x7f0f00eb

.field public static final search_view:I = 0x7f0f001a

.field public static final search_view_edit_menu_group:I = 0x7f0f02d6

.field public static final search_view_menu_group:I = 0x7f0f02d7

.field public static final search_voice_btn:I = 0x7f0f0117

.field public static final search_voice_frame:I = 0x7f0f0209

.field public static final seekbar_set:I = 0x7f0f01d3

.field public static final select_all_check_box:I = 0x7f0f0020

.field public static final select_all_checkbox:I = 0x7f0f020c

.field public static final select_all_menu:I = 0x7f0f0018

.field public static final select_all_sperator:I = 0x7f0f020b

.field public static final select_all_text:I = 0x7f0f020d

.field public static final select_all_text_view:I = 0x7f0f0021

.field public static final select_count_text:I = 0x7f0f001e

.field public static final selected_filter_items:I = 0x7f0f0100

.field public static final selection_menu:I = 0x7f0f001b

.field public static final selection_mode_relative_layout:I = 0x7f0f020a

.field public static final selection_on_action_bar_layout:I = 0x7f0f001f

.field public static final sendComment:I = 0x7f0f007b

.field public static final sensitivity_seekbar:I = 0x7f0f01d6

.field public static final sensitivity_text:I = 0x7f0f01d4

.field public static final seperator:I = 0x7f0f0147

.field public static final sepia_image:I = 0x7f0f022e

.field public static final sepia_text:I = 0x7f0f022f

.field public static final setting_container:I = 0x7f0f0152

.field public static final setting_container_layout:I = 0x7f0f0007

.field public static final setting_fragment_layout:I = 0x7f0f0006

.field public static final setting_layout:I = 0x7f0f01f6

.field public static final setting_list:I = 0x7f0f020e

.field public static final setting_page_details_container:I = 0x7f0f000a

.field public static final share:I = 0x7f0f0265

.field public static final share_content_edit:I = 0x7f0f0036

.field public static final share_edit:I = 0x7f0f0032

.field public static final share_image:I = 0x7f0f0149

.field public static final share_image_layout:I = 0x7f0f003e

.field public static final share_multi_edit:I = 0x7f0f0039

.field public static final share_multi_edit_group:I = 0x7f0f023e

.field public static final share_multi_name:I = 0x7f0f023d

.field public static final share_multi_to:I = 0x7f0f023c

.field public static final share_names:I = 0x7f0f0035

.field public static final share_picture_ivImage:I = 0x7f0f003d

.field public static final share_qzone_group:I = 0x7f0f004a

.field public static final share_renren_group:I = 0x7f0f0045

.field public static final share_select_images:I = 0x7f0f003b

.field public static final share_sns_icons:I = 0x7f0f003f

.field public static final share_sns_multi_name:I = 0x7f0f023b

.field public static final share_sns_name:I = 0x7f0f0033

.field public static final share_to:I = 0x7f0f0034

.field public static final share_via_list:I = 0x7f0f0212

.field public static final share_weibo_group:I = 0x7f0f0040

.field public static final simple_image:I = 0x7f0f022a

.field public static final simple_text:I = 0x7f0f022b

.field public static final sina_checkbox:I = 0x7f0f0043

.field public static final sina_weibo_icon:I = 0x7f0f0241

.field public static final sinaweibo_checkbox:I = 0x7f0f0041

.field public static final sinaweibo_icon:I = 0x7f0f0042

.field public static final slideShowResume:I = 0x7f0f021e

.field public static final slideShowStop:I = 0x7f0f021d

.field public static final slideShowsettings:I = 0x7f0f021f

.field public static final slideeffect_simplelayout:I = 0x7f0f0229

.field public static final slideshow_scrollview:I = 0x7f0f021a

.field public static final slink_album_view_view_menu_group:I = 0x7f0f02da

.field public static final slink_charge_text:I = 0x7f0f0066

.field public static final slink_dialog_charges:I = 0x7f0f0068

.field public static final slink_image:I = 0x7f0f0067

.field public static final slink_load_content_text:I = 0x7f0f0065

.field public static final slink_network_mode:I = 0x7f0f00e8

.field public static final slink_view_multipick_menu_group:I = 0x7f0f02db

.field public static final slow_text:I = 0x7f0f01d5

.field public static final sns_acount_list:I = 0x7f0f0030

.field public static final sns_edit_list:I = 0x7f0f003a

.field public static final sns_fragments_layout:I = 0x7f0f0079

.field public static final sns_icon:I = 0x7f0f0097

.field public static final sns_icon_time:I = 0x7f0f009a

.field public static final sns_multi_edit:I = 0x7f0f023f

.field public static final sns_tabhost:I = 0x7f0f0078

.field public static final sns_text:I = 0x7f0f0096

.field public static final sns_time:I = 0x7f0f0098

.field public static final speed_title:I = 0x7f0f0223

.field public static final spinner_down_btn:I = 0x7f0f0038

.field public static final spinner_up_btn:I = 0x7f0f0240

.field public static final start_left_far:I = 0x7f0f01bb

.field public static final start_left_near:I = 0x7f0f01bc

.field public static final start_right_far:I = 0x7f0f01bd

.field public static final start_right_near:I = 0x7f0f01be

.field public static final start_trim:I = 0x7f0f024c

.field public static final status:I = 0x7f0f0177

.field public static final summary:I = 0x7f0f00de

.field public static final surface_view:I = 0x7f0f01e0

.field public static final surfaceviewlayout:I = 0x7f0f00f9

.field public static final tabhost:I = 0x7f0f00c6

.field public static final tablelayout:I = 0x7f0f019a

.field public static final tag_cloud_scroll_view:I = 0x7f0f0248

.field public static final tag_list_body:I = 0x7f0f0058

.field public static final tag_list_expand_button:I = 0x7f0f0126

.field public static final tag_list_icon:I = 0x7f0f0124

.field public static final tag_list_layout:I = 0x7f0f0246

.field public static final tag_list_scrollview:I = 0x7f0f0057

.field public static final tag_list_subcontainer:I = 0x7f0f0056

.field public static final tag_list_title:I = 0x7f0f0125

.field public static final tag_list_title_layout:I = 0x7f0f0123

.field public static final tag_list_title_line:I = 0x7f0f0245

.field public static final tap_layout:I = 0x7f0f01c1

.field public static final tencentqzone_checkbox:I = 0x7f0f004b

.field public static final text1:I = 0x7f0f00ad

.field public static final textView_loading:I = 0x7f0f007e

.field public static final text_body:I = 0x7f0f00e3

.field public static final text_date:I = 0x7f0f0211

.field public static final text_hint:I = 0x7f0f0116

.field public static final text_name:I = 0x7f0f0121

.field public static final text_noitems:I = 0x7f0f0162

.field public static final text_noitems_description:I = 0x7f0f0160

.field public static final textview:I = 0x7f0f0217

.field public static final textview1:I = 0x7f0f00f8

.field public static final thumbnail:I = 0x7f0f01f4

.field public static final tilt_tap:I = 0x7f0f01cf

.field public static final time_tag_cloud_view:I = 0x7f0f0249

.field public static final time_view_edit_menu_group:I = 0x7f0f02dc

.field public static final time_view_help_menu_group:I = 0x7f0f02bf

.field public static final title:I = 0x7f0f002f

.field public static final topLayout:I = 0x7f0f01cd

.field public static final top_left_action_bar_buttons_container:I = 0x7f0f0015

.field public static final trim_view_root:I = 0x7f0f024d

.field public static final tutorialLeftButton:I = 0x7f0f01d9

.field public static final tutorialRightButton:I = 0x7f0f01da

.field public static final txt_contextual_tag_info:I = 0x7f0f00a7

.field public static final txt_location:I = 0x7f0f00ab

.field public static final txt_person:I = 0x7f0f00ac

.field public static final txt_weather:I = 0x7f0f00aa

.field public static final unchecked:I = 0x7f0f024f

.field public static final undo_btn:I = 0x7f0f0143

.field public static final user_tag_cloud_view:I = 0x7f0f024a

.field public static final user_tag_edit:I = 0x7f0f0052

.field public static final username_edit:I = 0x7f0f0069

.field public static final usertag_image_noitems_bg:I = 0x7f0f01b7

.field public static final usertag_text_noitems:I = 0x7f0f01b6

.field public static final video_play_check:I = 0x7f0f0234

.field public static final video_title:I = 0x7f0f0233

.field public static final viewpager:I = 0x7f0f0002

.field public static final vintage_image:I = 0x7f0f0230

.field public static final vintage_text:I = 0x7f0f0231

.field public static final weather_mode_list:I = 0x7f0f00bd

.field public static final weather_preview_date:I = 0x7f0f00bc

.field public static final weather_preview_image:I = 0x7f0f00b8

.field public static final weather_preview_location:I = 0x7f0f00ba

.field public static final weather_preview_person:I = 0x7f0f00bb

.field public static final weather_preview_weather:I = 0x7f0f00b9

.field public static final weibo_name:I = 0x7f0f0044

.field public static final weibosdk_etEdit:I = 0x7f0f0037

.field public static final widget_desciption:I = 0x7f0f0172

.field public static final widget_name:I = 0x7f0f0170

.field public static final widget_preference_frametype:I = 0x7f0f01ee

.field public static final widget_preference_frametype_multi_left_rightupdown:I = 0x7f0f01f1

.field public static final widget_preference_frametype_multi_up_down:I = 0x7f0f01f2

.field public static final widget_preference_frametype_multi_up_downleftright:I = 0x7f0f01f3

.field public static final widget_preference_frametype_single:I = 0x7f0f01f0

.field public static final widget_preference_frametype_title:I = 0x7f0f01ef

.field public static final widget_start:I = 0x7f0f0171

.field public static final widget_type:I = 0x7f0f0083

.field public static final widget_type_album:I = 0x7f0f0084

.field public static final widget_type_photo:I = 0x7f0f0085

.field public static final widget_type_shuffle:I = 0x7f0f0086


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
