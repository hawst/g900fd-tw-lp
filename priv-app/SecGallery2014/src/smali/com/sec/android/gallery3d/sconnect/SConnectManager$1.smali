.class Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;
.super Ljava/lang/Object;
.source "SConnectManager.java"

# interfaces
.implements Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/sconnect/SConnectManager;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/sconnect/SConnectManager;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;->this$0:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected()V
    .locals 13

    .prologue
    .line 61
    const/4 v0, 0x0

    .line 65
    .local v0, "containInvaildItem":Z
    iget-object v8, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;->this$0:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    # getter for: Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->access$000(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 66
    .local v5, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalItemList()Ljava/util/LinkedList;

    move-result-object v2

    .line 68
    .local v2, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v7, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .local v4, "n":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 70
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 71
    .local v3, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v8, v3, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;

    if-eqz v8, :cond_1

    .line 72
    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 69
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 76
    if-nez v0, :cond_2

    .line 77
    const/4 v0, 0x1

    .line 84
    .end local v3    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    if-eqz v0, :cond_3

    .line 85
    iget-object v8, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;->this$0:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    # getter for: Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->access$000(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    const v9, 0x7f0e0107

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 88
    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    sget v9, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v8, v9, :cond_5

    .line 89
    iget-object v8, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;->this$0:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    # getter for: Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->access$000(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0114

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    sget v12, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 91
    .local v6, "text":Ljava/lang/CharSequence;
    iget-object v8, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;->this$0:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    # getter for: Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->access$000(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 95
    .end local v6    # "text":Ljava/lang/CharSequence;
    :goto_2
    return-void

    .line 81
    .restart local v3    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 93
    .end local v3    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    iget-object v8, p0, Lcom/sec/android/gallery3d/sconnect/SConnectManager$1;->this$0:Lcom/sec/android/gallery3d/sconnect/SConnectManager;

    # getter for: Lcom/sec/android/gallery3d/sconnect/SConnectManager;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->access$000(Lcom/sec/android/gallery3d/sconnect/SConnectManager;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    # invokes: Lcom/sec/android/gallery3d/sconnect/SConnectManager;->startSConnect(Landroid/app/Activity;Ljava/util/ArrayList;)V
    invoke-static {v8, v7}, Lcom/sec/android/gallery3d/sconnect/SConnectManager;->access$100(Landroid/app/Activity;Ljava/util/ArrayList;)V

    goto :goto_2
.end method
