.class public Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;
.super Landroid/os/AsyncTask;
.source "ChangePlayerListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NearbyIconDownloader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Handler;",
        "Landroid/net/Uri;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 144
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;->mUri:Landroid/net/Uri;

    .line 146
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/os/Handler;)Ljava/lang/Object;
    .locals 12
    .param p1, "handler"    # [Landroid/os/Handler;

    .prologue
    const/4 v11, 0x0

    .line 150
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;->mUri:Landroid/net/Uri;

    if-nez v8, :cond_0

    .line 185
    :goto_0
    return-object v11

    .line 153
    :cond_0
    const/4 v3, 0x0

    .line 155
    .local v3, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 156
    .end local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v4, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :try_start_1
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->mHttpClient:Landroid/net/http/AndroidHttpClient;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->access$100()Landroid/net/http/AndroidHttpClient;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 158
    .local v6, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    .line 159
    .local v7, "statusCode":I
    const/16 v8, 0xc8

    if-eq v7, v8, :cond_2

    .line 160
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->access$200()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " while retrieving bitmap"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 180
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v7    # "statusCode":I
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 181
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .local v1, "e":Ljava/io/IOException;
    .restart local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :goto_1
    if-eqz v3, :cond_1

    .line 182
    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 183
    :cond_1
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->access$200()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Error while retrieving bitmap"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "response":Lorg/apache/http/HttpResponse;
    .restart local v7    # "statusCode":I
    :cond_2
    :try_start_2
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 165
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    if-nez v2, :cond_3

    .line 166
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->access$200()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " while retrieving entity"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 170
    :cond_3
    const/4 v5, 0x0

    .line 172
    .local v5, "inputStream":Ljava/io/InputStream;
    :try_start_3
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 173
    invoke-static {v5}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 174
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->sIconCache:Ljava/util/HashMap;

    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;->mUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const/4 v8, 0x0

    aget-object v8, p1, v8

    invoke-static {v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Message;->sendToTarget()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    :try_start_4
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 178
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    move-object v3, v4

    .line 184
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    goto/16 :goto_0

    .line 177
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catchall_0
    move-exception v8

    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 178
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    throw v8
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 180
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v7    # "statusCode":I
    .restart local v3    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 141
    check-cast p1, [Landroid/os/Handler;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;->doInBackground([Landroid/os/Handler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
