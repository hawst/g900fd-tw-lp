.class public Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;
.super Ljava/lang/Object;
.source "ClusterAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/ClusterAlbumSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UpdateOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final ASSIGN_NAME:I = 0x2

.field public static final CONFIRM:I = 0x0

.field public static final REMOVE:I = 0x1

.field public static final UPDATE:I = 0x3


# instance fields
.field private mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private mJoinedName:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mOperationType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/ClusterAlbumSet;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "operationType"    # I
    .param p5, "joinedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 405
    .local p2, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mOperationType:I

    .line 406
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mAlbums:Ljava/util/ArrayList;

    .line 407
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mName:Ljava/lang/String;

    .line 408
    iput p4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mOperationType:I

    .line 409
    iput-object p5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mJoinedName:Ljava/lang/String;

    .line 410
    return-void
.end method

.method private assignName()V
    .locals 4

    .prologue
    .line 456
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 457
    .local v0, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mJoinedName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateFaces(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_0
    return-void
.end method

.method private confirm()V
    .locals 3

    .prologue
    .line 450
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 451
    .local v0, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->confirmFaces()V

    goto :goto_0

    .line 453
    :cond_0
    return-void
.end method

.method private remove()V
    .locals 7

    .prologue
    .line 436
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object v5, v0

    .line 437
    check-cast v5, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/Path;

    .line 438
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 439
    .local v4, "values":[Ljava/lang/String;
    const/16 v5, 0x9

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 440
    .local v3, "recommendedId":I
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->removeFaces()V

    .line 442
    const/4 v5, 0x1

    if-le v3, v5, :cond_0

    .line 443
    const-string v5, "remove"

    const-string v6, "remove clusterAlbumSet"

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    # getter for: Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v5}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->access$000(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/sec/samsung/gallery/access/face/PersonList;->remove(Landroid/content/Context;I)V

    goto :goto_0

    .line 447
    .end local v2    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v3    # "recommendedId":I
    .end local v4    # "values":[Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 394
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v2, 0x0

    .line 414
    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mOperationType:I

    packed-switch v0, :pswitch_data_0

    .line 427
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateMediaSet()V

    .line 428
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->mOperationType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 429
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    # getter for: Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->access$000(Lcom/sec/android/gallery3d/data/ClusterAlbumSet;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 432
    :cond_0
    return-object v2

    .line 416
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->confirm()V

    goto :goto_0

    .line 419
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->remove()V

    goto :goto_0

    .line 422
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet$UpdateOperation;->assignName()V

    goto :goto_0

    .line 414
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
