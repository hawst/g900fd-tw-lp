.class public Lcom/sec/android/gallery3d/data/HiddenAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "HiddenAlbum.java"


# static fields
.field private static final INVALID_COUNT:I = -0x1


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mBucketId:I

.field private mCount:I

.field private final mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mHiddenPath:Ljava/lang/String;

.field private final mIsImage:Z

.field private mItemCache:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mSourcePath:Ljava/lang/String;

.field private final mTable:Ljava/lang/String;

.field private final mWhereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/data/Path;IZ)V
    .locals 14
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "dbMgr"    # Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .param p3, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "bucketId"    # I
    .param p5, "isImage"    # Z

    .prologue
    .line 27
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/gallery3d/data/HiddenAlbum;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/data/Path;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const/4 v11, 0x0

    .line 29
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 30
    .local v10, "albumPath":Ljava/lang/String;
    const/4 v9, 0x0

    .line 31
    .local v9, "albumName":Ljava/lang/String;
    const/4 v13, 0x0

    .line 33
    .local v13, "sourcePath":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mTable:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PROJECTIONS:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 34
    if-eqz v11, :cond_1

    .line 35
    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 36
    if-eqz v10, :cond_0

    .line 37
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v10

    .line 39
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 40
    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 45
    :cond_1
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 47
    :goto_0
    iput-object v9, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mName:Ljava/lang/String;

    .line 48
    iput-object v10, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mHiddenPath:Ljava/lang/String;

    .line 49
    iput-object v13, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mSourcePath:Ljava/lang/String;

    .line 50
    return-void

    .line 42
    :catch_0
    move-exception v12

    .line 43
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/data/Path;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "dbMgr"    # Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .param p3, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "bucketId"    # I
    .param p5, "isImage"    # Z
    .param p6, "name"    # Ljava/lang/String;
    .param p7, "hiddenPath"    # Ljava/lang/String;
    .param p8, "sourcePath"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-static {}, Lcom/sec/android/gallery3d/data/HiddenAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p3, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I

    .line 55
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 56
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 57
    iput p4, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    .line 58
    iput-object p6, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mName:Ljava/lang/String;

    .line 59
    iput-boolean p5, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mIsImage:Z

    .line 60
    iput-object p7, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mHiddenPath:Ljava/lang/String;

    .line 61
    iput-object p8, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mSourcePath:Ljava/lang/String;

    .line 62
    if-eqz p5, :cond_0

    const-string v0, "images_hidden_album"

    :goto_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mTable:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mWhereClause:Ljava/lang/String;

    .line 65
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->HIDDEN_ALBUM_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 66
    return-void

    .line 62
    :cond_0
    const-string/jumbo v0, "video_hidden_album"

    goto :goto_0
.end method


# virtual methods
.method public delete()V
    .locals 5

    .prologue
    .line 99
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mTable:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHiddenAlbumItems(Ljava/lang/String;I)V

    .line 100
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget v3, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbum(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 101
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mHiddenPath:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, "hiddenFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mSourcePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v1, "sourceFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 105
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 106
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget v3, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHiddenAlbumInfo(I)Z

    .line 109
    .end local v0    # "hiddenFile":Ljava/io/File;
    .end local v1    # "sourceFile":Ljava/io/File;
    :cond_2
    return-void
.end method

.method public delete(Z)V
    .locals 0
    .param p1, "includeDir"    # Z

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/HiddenAlbum;->delete()V

    .line 95
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 5
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I

    sub-int/2addr v1, p1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 135
    if-gtz p2, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-object v0

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mItemCache:Ljava/util/List;

    if-nez v1, :cond_2

    .line 139
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mIsImage:Z

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mWhereClause:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->loadHiddenItems(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mItemCache:Ljava/util/List;

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mItemCache:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mItemCache:Ljava/util/List;

    add-int v2, p1, p2

    invoke-interface {v1, p1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 8

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 114
    const/4 v6, 0x0

    .line 116
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mTable:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mWhereClause:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 117
    if-eqz v6, :cond_1

    .line 118
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    :goto_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 128
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_1
    iget v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I

    return v0

    .line 120
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v7

    .line 123
    .local v7, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 125
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPathOnFileSystem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mHiddenPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSourcePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mSourcePath:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 88
    const-wide/16 v0, 0x5

    .line 89
    .local v0, "supported":J
    return-wide v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/HiddenSource;->syncHiddenFiles(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mItemCache:Ljava/util/List;

    .line 157
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mCount:I

    .line 158
    invoke-static {}, Lcom/sec/android/gallery3d/data/HiddenAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDataVersion:J

    .line 160
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDataVersion:J

    return-wide v0
.end method

.method public show()Z
    .locals 14

    .prologue
    const/4 v11, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/HiddenAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "hiddenAlbumPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/HiddenAlbum;->getSourcePath()Ljava/lang/String;

    move-result-object v9

    .line 167
    .local v9, "srcAlbumPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-nez v9, :cond_1

    :cond_0
    move v10, v11

    .line 201
    :goto_0
    return v10

    .line 169
    :cond_1
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 170
    .local v6, "srcAlbumFolder":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 172
    .local v7, "srcAlbumName":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 173
    .local v8, "srcAlbumParent":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v8, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 175
    .local v5, "srcAlbum":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    .line 176
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 179
    :cond_2
    const/4 v4, 0x1

    .line 180
    .local v4, "result":Z
    const/4 v3, 0x0

    .line 181
    .local v3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v12, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v12

    .line 182
    const/4 v10, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/HiddenAlbum;->getMediaItemCount()I

    move-result v13

    invoke-virtual {p0, v10, v13}, Lcom/sec/android/gallery3d/data/HiddenAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 183
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    .line 185
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v2, v10, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_4

    .line 186
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->show()Z

    move-result v10

    if-nez v10, :cond_3

    .line 187
    const/4 v4, 0x0

    .line 185
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 183
    .end local v2    # "i":I
    :catchall_0
    move-exception v10

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v10

    .line 192
    :cond_4
    if-eqz v4, :cond_6

    .line 193
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget v11, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbum(I)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 194
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    iget v11, p0, Lcom/sec/android/gallery3d/data/HiddenAlbum;->mBucketId:I

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHiddenAlbumInfo(I)Z

    .line 195
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 196
    .local v1, "hiddenFolder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 197
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 199
    .end local v1    # "hiddenFolder":Ljava/io/File;
    :cond_5
    const/4 v10, 0x1

    goto :goto_0

    :cond_6
    move v10, v11

    .line 201
    goto :goto_0
.end method
