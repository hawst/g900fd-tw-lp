.class public Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;
.super Lcom/sec/android/gallery3d/homesync/HomeSyncItem;
.source "HomeSyncLocalImage.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mPath:Lcom/sec/android/gallery3d/data/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 50
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 52
    iput-object p2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 54
    return-void
.end method


# virtual methods
.method public getContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 226
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0xd

    const/16 v3, 0xc

    .line 104
    invoke-super {p0}, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 108
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v1, :cond_0

    .line 110
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->getDateModifiedLong()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mMimeType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 120
    const/16 v1, 0x10

    iget v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mOrientation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 122
    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mDataUri:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->extractExifInfo(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)V

    .line 126
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    iget v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mWidth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 132
    :cond_1
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    iget v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mHeight:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->replaceDetail(ILjava/lang/Object;)V

    .line 160
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->isdrm:Z

    if-eqz v1, :cond_3

    .line 162
    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mDataUri:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDetails(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 166
    :cond_3
    return-object v0

    .line 140
    :cond_4
    const/16 v1, 0xc8

    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mDataUri:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 142
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 144
    iget v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mWidth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 146
    iget v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mHeight:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 150
    iget-wide v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mSize:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 152
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mSize:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mDataUri:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 236
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mHeight:I

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x2

    return v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mSize:J

    return-wide v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 92
    const-wide v0, 0x400080000044L

    .line 94
    .local v0, "operation":J
    return-wide v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mWidth:I

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 8
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->getDateModifiedLong()J

    move-result-wide v4

    iget-object v7, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mDataUri:Ljava/lang/String;

    move v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    return-object v1
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;

    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mDataUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    return-object v0
.end method
