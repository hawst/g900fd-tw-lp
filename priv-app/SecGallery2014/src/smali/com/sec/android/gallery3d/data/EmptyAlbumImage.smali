.class public Lcom/sec/android/gallery3d/data/EmptyAlbumImage;
.super Lcom/sec/android/gallery3d/data/ActionImage;
.source "EmptyAlbumImage.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmptyAlbumImage"


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 27
    const v0, 0x7f020410

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/data/ActionImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    .line 28
    return-void
.end method


# virtual methods
.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 32
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/ActionImage;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x2000

    or-long/2addr v0, v2

    return-wide v0
.end method
