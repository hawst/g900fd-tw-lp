.class public Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;
.super Ljava/lang/Object;
.source "FaceIndicatorView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LoadFaceJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 3336
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3337
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 3338
    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 3333
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 5
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 3342
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->loadFace()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v0

    .line 3343
    .local v0, "faces":[Lcom/sec/android/gallery3d/data/Face;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->updateFace(Lcom/sec/android/gallery3d/data/MediaItem;[Lcom/sec/android/gallery3d/data/Face;)V
    invoke-static {v1, v2, v0}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2500(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;Lcom/sec/android/gallery3d/data/MediaItem;[Lcom/sec/android/gallery3d/data/Face;)V

    .line 3344
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->this$0:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    # getter for: Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->access$2400(Lcom/sec/android/gallery3d/ui/FaceIndicatorView;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/FaceIndicatorView$LoadFaceJob;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3345
    const/4 v1, 0x0

    return-object v1
.end method
