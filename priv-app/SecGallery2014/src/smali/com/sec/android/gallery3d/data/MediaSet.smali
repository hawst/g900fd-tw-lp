.class public abstract Lcom/sec/android/gallery3d/data/MediaSet;
.super Lcom/sec/android/gallery3d/data/MediaObject;
.source "MediaSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/MediaSet$MultiSetSyncFuture;,
        Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;,
        Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;
    }
.end annotation


# static fields
.field public static final ALLINONE_LOCAL_ALBUM_FETCH_COUNT:I = 0x1388

.field private static final FUTURE_STUB:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final INDEX_NOT_FOUND:I = -0x1

.field public static final MEDIAITEM_BATCH_FETCH_COUNT:I = 0x20

.field public static final REMOTE_MEDIAITEM_BATCH_FETCH_COUNT:I = 0x1388

.field public static final SYNC_RESULT_CANCELLED:I = 0x1

.field public static final SYNC_RESULT_ERROR:I = 0x2

.field public static final SYNC_RESULT_SUCCESS:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCoverItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field protected mCoverVersion:J

.field protected mCustomName:Ljava/lang/String;

.field protected mListeners:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/ContentListener;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaSet;->TAG:Ljava/lang/String;

    .line 330
    new-instance v0, Lcom/sec/android/gallery3d/data/MediaSet$1;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/MediaSet$1;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaSet;->FUTURE_STUB:Lcom/sec/android/gallery3d/util/Future;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;J)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "version"    # J

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/data/MediaObject;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 55
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCustomName:Ljava/lang/String;

    .line 57
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCoverItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 58
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCoverVersion:J

    .line 211
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mListeners:Ljava/util/WeakHashMap;

    .line 75
    return-void
.end method


# virtual methods
.method public addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mListeners:Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    return-void
.end method

.method public cancelReload()V
    .locals 0

    .prologue
    .line 560
    return-void
.end method

.method public containsContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 497
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mListeners:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    const/4 v0, 0x1

    .line 500
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I
    .locals 9
    .param p1, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p2, "startIndex"    # I

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v6

    .line 287
    .local v6, "total":I
    const/4 v5, 0x0

    .line 288
    .local v5, "start":I
    :goto_0
    if-ge v5, v6, :cond_0

    .line 289
    const/16 v7, 0x20

    sub-int v8, v6, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 290
    .local v0, "count":I
    invoke-virtual {p0, v5, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 291
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .local v4, "n":I
    :goto_1
    if-ge v1, v4, :cond_2

    .line 292
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 293
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    add-int v7, p2, v5

    add-int/2addr v7, v1

    invoke-interface {p1, v7, v2}, Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;->consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 294
    const/4 v6, 0x0

    .line 298
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v4    # "n":I
    .end local v6    # "total":I
    :cond_0
    return v6

    .line 291
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v4    # "n":I
    .restart local v6    # "total":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 296
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    add-int/2addr v5, v0

    .line 297
    goto :goto_0
.end method

.method public enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V
    .locals 1
    .param p1, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

    .prologue
    .line 271
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I

    .line 272
    return-void
.end method

.method protected enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I
    .locals 5
    .param p1, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p2, "startIndex"    # I

    .prologue
    .line 304
    const/4 v3, 0x0

    .line 305
    .local v3, "start":I
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I

    move-result v4

    add-int/2addr v3, v4

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    .line 307
    .local v1, "m":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 308
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 309
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    .line 310
    add-int v4, p2, v3

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I

    move-result v4

    add-int/2addr v3, v4

    .line 307
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 312
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return v3
.end method

.method public enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V
    .locals 1
    .param p1, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

    .prologue
    .line 275
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I

    .line 276
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 444
    const/4 v0, -0x1

    return v0
.end method

.method public getBurstShotItems(J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "groupId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 549
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 10

    .prologue
    .line 103
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCoverVersion:J

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mDataVersion:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    if-lez v5, :cond_2

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemFirst()Ljava/util/ArrayList;

    move-result-object v2

    .line 106
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 107
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v5, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCoverItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 121
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    :goto_0
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mDataVersion:J

    iput-wide v6, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCoverVersion:J

    .line 123
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCoverItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v5

    .line 110
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v4

    .local v4, "n":I
    :goto_1
    if-ge v1, v4, :cond_0

    .line 111
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 112
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v3, :cond_4

    .line 110
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 114
    :cond_4
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 115
    .local v0, "cover":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_3

    .line 116
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCoverItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 4

    .prologue
    .line 248
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 251
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    .line 254
    .local v1, "value":Ljava/lang/String;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_0

    .line 255
    if-eqz v1, :cond_0

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 263
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 264
    return-object v0
.end method

.method public getEventAlbumTimeInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 534
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getIndexOf(Lcom/sec/android/gallery3d/data/Path;Ljava/util/ArrayList;)I
    .locals 5
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, -0x1

    .line 195
    if-nez p2, :cond_1

    move v0, v3

    .line 206
    :cond_0
    :goto_0
    return v0

    .line 199
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 201
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 202
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v1, :cond_2

    iget-object v4, v1, Lcom/sec/android/gallery3d/data/MediaObject;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-eq v4, p1, :cond_0

    .line 199
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    move v0, v3

    .line 206
    goto :goto_0
.end method

.method public getIndexOfItem(Lcom/sec/android/gallery3d/data/Path;I)I
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "hint"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v5, -0x1

    const/16 v3, 0x20

    .line 173
    add-int/lit8 v6, p2, -0x10

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 174
    .local v2, "start":I
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    .line 175
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOf(Lcom/sec/android/gallery3d/data/Path;Ljava/util/ArrayList;)I

    move-result v0

    .line 176
    .local v0, "index":I
    if-eq v0, v5, :cond_0

    .line 177
    add-int v3, v2, v0

    .line 187
    :goto_0
    return v3

    .line 180
    :cond_0
    if-nez v2, :cond_1

    move v2, v3

    .line 181
    :goto_1
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    .line 183
    :goto_2
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOf(Lcom/sec/android/gallery3d/data/Path;Ljava/util/ArrayList;)I

    move-result v0

    .line 184
    if-eq v0, v5, :cond_2

    .line 185
    add-int v3, v2, v0

    goto :goto_0

    :cond_1
    move v2, v4

    .line 180
    goto :goto_1

    .line 186
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v4, v3, :cond_3

    move v3, v5

    .line 187
    goto :goto_0

    .line 188
    :cond_3
    add-int/lit8 v2, v2, 0x20

    .line 189
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_2
.end method

.method public getIndexOfItemEx(Lcom/sec/android/gallery3d/data/Path;I)I
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "hint"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v6, -0x1

    .line 466
    const/16 v0, 0x20

    .line 467
    .local v0, "batchCnt":I
    instance-of v4, p0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v4, :cond_0

    move-object v4, p0

    check-cast v4, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v4, :cond_0

    .line 468
    const/16 v0, 0x1388

    .line 472
    :cond_0
    div-int/lit8 v4, v0, 0x2

    sub-int v4, p2, v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 473
    .local v3, "start":I
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 474
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOf(Lcom/sec/android/gallery3d/data/Path;Ljava/util/ArrayList;)I

    move-result v1

    .line 475
    .local v1, "index":I
    if-eq v1, v6, :cond_1

    .line 476
    add-int v4, v3, v1

    .line 488
    :goto_0
    return v4

    .line 479
    :cond_1
    if-nez v3, :cond_2

    move v3, v0

    .line 480
    :goto_1
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 481
    if-nez v2, :cond_4

    move v4, v6

    .line 482
    goto :goto_0

    :cond_2
    move v3, v5

    .line 479
    goto :goto_1

    .line 489
    :cond_3
    add-int/2addr v3, v0

    .line 490
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 484
    :cond_4
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOf(Lcom/sec/android/gallery3d/data/Path;Ljava/util/ArrayList;)I

    move-result v1

    .line 485
    if-eq v1, v6, :cond_5

    .line 486
    add-int v4, v3, v1

    goto :goto_0

    .line 487
    :cond_5
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v4, v0, :cond_3

    move v4, v6

    .line 488
    goto :goto_0
.end method

.method public abstract getKey()Ljava/lang/String;
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaItemEx(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 461
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemFirst()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 449
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemFirst(I)Ljava/util/ArrayList;
    .locals 2
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 452
    if-ne p1, v1, :cond_0

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    .line 455
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public getPathOnFileSystem()Ljava/lang/String;
    .locals 2

    .prologue
    .line 511
    sget-object v0, Lcom/sec/android/gallery3d/data/MediaSet;->TAG:Ljava/lang/String;

    const-string v1, "getPathOnFileSystem not supported"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const-string v0, ""

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 132
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public getSubMediaSetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 556
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public getTotalMediaItemCount()I
    .locals 5

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    .line 161
    .local v3, "total":I
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 162
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 163
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 164
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v4

    add-int/2addr v3, v4

    .line 161
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return v3
.end method

.method public hasLocation()Z
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x0

    return v0
.end method

.method public isAlbumSetEmpty()Z
    .locals 1

    .prologue
    .line 523
    const/4 v0, 0x1

    return v0
.end method

.method public isCameraAlbum()Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public isCameraRoll()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public isSuggestionEvent()Z
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x0

    return v0
.end method

.method public notifyContentChanged()V
    .locals 6

    .prologue
    .line 226
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mListeners:Ljava/util/WeakHashMap;

    invoke-virtual {v5}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v4

    .line 228
    .local v4, "listeners":[Ljava/lang/Object;
    const/4 v2, 0x0

    .local v2, "i":I
    array-length v0, v4

    .local v0, "count":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 229
    aget-object v3, v4, v2

    check-cast v3, Lcom/sec/android/gallery3d/data/ContentListener;

    .line 230
    .local v3, "listener":Lcom/sec/android/gallery3d/data/ContentListener;
    if-nez v3, :cond_0

    .line 228
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 232
    :cond_0
    invoke-interface {v3}, Lcom/sec/android/gallery3d/data/ContentListener;->onContentDirty()V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 237
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "listener":Lcom/sec/android/gallery3d/data/ContentListener;
    .end local v4    # "listeners":[Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 238
    .local v1, "e":Ljava/util/ConcurrentModificationException;
    invoke-virtual {v1}, Ljava/util/ConcurrentModificationException;->printStackTrace()V

    .line 240
    .end local v1    # "e":Ljava/util/ConcurrentModificationException;
    :cond_1
    return-void
.end method

.method public abstract reload()J
.end method

.method public removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mListeners:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    return-void
.end method

.method public requestSync(Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;",
            ")",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326
    const/4 v0, 0x0

    invoke-interface {p1, p0, v0}, Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;->onSyncDone(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 327
    sget-object v0, Lcom/sec/android/gallery3d/data/MediaSet;->FUTURE_STUB:Lcom/sec/android/gallery3d/util/Future;

    return-object v0
.end method

.method protected requestSyncOnMultipleSets([Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 1
    .param p1, "sets"    # [Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;",
            ")",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    new-instance v0, Lcom/sec/android/gallery3d/data/MediaSet$MultiSetSyncFuture;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet$MultiSetSyncFuture;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet$SyncListener;)V

    return-object v0
.end method

.method public setCameraAlbum(Z)V
    .locals 0
    .param p1, "isCamAlbum"    # Z

    .prologue
    .line 553
    return-void
.end method

.method public setCustomName(Ljava/lang/String;)V
    .locals 0
    .param p1, "customName"    # Ljava/lang/String;

    .prologue
    .line 527
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MediaSet;->mCustomName:Ljava/lang/String;

    .line 528
    return-void
.end method

.method public setForceReload(Z)V
    .locals 0
    .param p1, "forceReload"    # Z

    .prologue
    .line 519
    return-void
.end method

.method public setReorderAlbum(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 531
    return-void
.end method

.method public setSortByType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 516
    return-void
.end method

.method public setSuggestionEvent(I)V
    .locals 0
    .param p1, "isSuggestion"    # I

    .prologue
    .line 542
    return-void
.end method

.method public updateMediaSet()V
    .locals 0

    .prologue
    .line 506
    return-void
.end method
