.class public Lcom/sec/android/gallery3d/data/LocalAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "LocalAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final BUCKET_GROUP_BY:Ljava/lang/String; = "1) GROUP BY 1,(2"

.field private static final BUCKET_ORDER_BY:Ljava/lang/String; = "UPPER(bucket_display_name)"

.field public static final CAMERA_EXTERNAL_PATH:Ljava/lang/String;

.field public static final CAMERA_PATH:Ljava/lang/String;

.field public static final DCIM_PATH:Ljava/lang/String;

.field private static final EXTERNAL_MEDIA:Ljava/lang/String; = "external"

.field private static final FAKE_LOADING_COUNT:I = 0x1f4

.field private static final INDEX_DATA:I = 0x3

.field private static final INVALID_COUNT:I = -0x1

.field public static final PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

.field public static final PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

.field private static final PROJECTION_BUCKET:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "LocalAlbumSet"

.field private static final mBaseUri:Landroid/net/Uri;

.field private static final mWatchAlbumOrder:Landroid/net/Uri;

.field private static final mWatchUriImage:Landroid/net/Uri;

.field private static final mWatchUriVideo:Landroid/net/Uri;

.field private static final mWatchUris:[Landroid/net/Uri;

.field private static sbFakeLoading:Z

.field public static sbNeedFullLoading:Z


# instance fields
.field private mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mCachedCount:I

.field private mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private final mHandler:Landroid/os/Handler;

.field private mIsLoading:Z

.field private mLoadBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mNotifierOrder:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mSingleBucketId:I

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    const-string v0, "/local/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_ALL:Lcom/sec/android/gallery3d/data/Path;

    .line 56
    const-string v0, "/local/image"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    .line 57
    const-string v0, "/local/video"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    .line 59
    new-array v0, v2, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUris:[Landroid/net/Uri;

    .line 63
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/DCIM/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->DCIM_PATH:Ljava/lang/String;

    .line 64
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/DCIM/Camera/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->CAMERA_PATH:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/gallery3d/util/MediaSetUtils;->REMOVABLE_SD_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "DCIM/Camera/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->CAMERA_EXTERNAL_PATH:Ljava/lang/String;

    .line 75
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mBaseUri:Landroid/net/Uri;

    .line 76
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriImage:Landroid/net/Uri;

    .line 77
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriVideo:Landroid/net/Uri;

    .line 78
    const-string v0, "content://album_order"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchAlbumOrder:Landroid/net/Uri;

    .line 79
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "bucket_id"

    aput-object v1, v0, v3

    const-string v1, "media_type"

    aput-object v1, v0, v4

    const-string v1, "bucket_display_name"

    aput-object v1, v0, v2

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    .line 89
    sput-boolean v4, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbFakeLoading:Z

    .line 90
    sput-boolean v3, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbNeedFullLoading:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 119
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    .line 121
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "localDatabaseManager"    # Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .param p3, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/app/GalleryApp;I)V

    .line 126
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/app/GalleryApp;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "localDatabaseManager"    # Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .param p3, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "singleBucketId"    # I

    .prologue
    .line 129
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 130
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 131
    new-instance v0, Landroid/os/Handler;

    invoke-interface {p3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mHandler:Landroid/os/Handler;

    .line 132
    iput p4, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    .line 134
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUris:[Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p3}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 135
    invoke-interface {p3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0094

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mName:Ljava/lang/String;

    .line 136
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchAlbumOrder:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p3}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mNotifierOrder:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 137
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 138
    return-void
.end method

.method private static circularShiftRight([Ljava/lang/Object;II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p0, "array":[Ljava/lang/Object;, "[TT;"
    aget-object v1, p0, p2

    .line 262
    .local v1, "temp":Ljava/lang/Object;, "TT;"
    move v0, p2

    .local v0, "k":I
    :goto_0
    if-le v0, p1, :cond_0

    .line 263
    add-int/lit8 v2, v0, -0x1

    aget-object v2, p0, v2

    aput-object v2, p0, v0

    .line 262
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 265
    :cond_0
    aput-object v1, p0, p1

    .line 266
    return-void
.end method

.method private static findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I
    .locals 3
    .param p0, "entries"    # [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    .param p1, "bucketId"    # I

    .prologue
    .line 178
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v1, p0

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 179
    aget-object v2, p0, v0

    iget v2, v2, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    if-ne v2, p1, :cond_0

    .line 181
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 178
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getDcimAlbum(Lcom/sec/android/gallery3d/data/DataManager;[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;Lcom/sec/android/gallery3d/data/Path;I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 20
    .param p1, "manager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p2, "entries"    # [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    .param p3, "parent"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "dcimCnt"    # I

    .prologue
    .line 439
    const/4 v13, 0x0

    .local v13, "idx":I
    const/16 v17, 0x0

    .line 441
    .local v17, "mergedCnt":I
    const/4 v2, 0x0

    aget-object v2, p2, v2

    iget v11, v2, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    .line 442
    .local v11, "firstBucketId":I
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v19

    .line 444
    .local v19, "path":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    mul-int/lit8 v17, p4, 0x2

    .line 448
    :cond_0
    :goto_0
    sget-object v9, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 449
    .local v9, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move/from16 v0, v17

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v16, v0

    .line 450
    .local v16, "mediaSets":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v12, 0x0

    .local v12, "i":I
    move v14, v13

    .end local v13    # "idx":I
    .local v14, "idx":I
    :goto_1
    move/from16 v0, p4

    if-ge v12, v0, :cond_4

    .line 451
    aget-object v10, p2, v12

    .line 452
    .local v10, "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    and-int/lit8 v2, v2, 0x2

    if-lez v2, :cond_b

    .line 453
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "idx":I
    .restart local v13    # "idx":I
    const/4 v4, 0x2

    sget-object v5, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    iget v6, v10, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    iget-object v7, v10, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketName:Ljava/lang/String;

    iget-object v8, v10, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketPath:Ljava/lang/String;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getLocalAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v16, v14

    .line 456
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    and-int/lit8 v2, v2, 0x4

    if-lez v2, :cond_1

    .line 457
    add-int/lit8 v14, v13, 0x1

    .end local v13    # "idx":I
    .restart local v14    # "idx":I
    const/4 v4, 0x4

    sget-object v5, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    iget v6, v10, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    iget-object v7, v10, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketName:Ljava/lang/String;

    iget-object v8, v10, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketPath:Ljava/lang/String;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getLocalAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v16, v13

    move v13, v14

    .line 450
    .end local v14    # "idx":I
    .restart local v13    # "idx":I
    :cond_1
    add-int/lit8 v12, v12, 0x1

    move v14, v13

    .end local v13    # "idx":I
    .restart local v14    # "idx":I
    goto :goto_1

    .line 445
    .end local v9    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v10    # "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    .end local v12    # "i":I
    .end local v14    # "idx":I
    .end local v16    # "mediaSets":[Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v13    # "idx":I
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 446
    :cond_3
    move/from16 v17, p4

    goto :goto_0

    .line 462
    .end local v13    # "idx":I
    .restart local v9    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v12    # "i":I
    .restart local v14    # "idx":I
    .restart local v16    # "mediaSets":[Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v18

    .line 463
    .local v18, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v18, :cond_5

    move-object/from16 v0, v18

    instance-of v2, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v2, :cond_5

    move-object/from16 v15, v18

    .line 464
    check-cast v15, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    .local v15, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object v2, v15

    .line 465
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->changeSources([Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 478
    :goto_3
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v2, :cond_a

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 479
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->setCameraAlbum(Z)V

    .line 483
    :goto_4
    return-object v15

    .line 467
    .end local v15    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    .line 468
    :cond_6
    if-eqz v18, :cond_7

    move-object/from16 v15, v18

    .line 469
    check-cast v15, Lcom/sec/android/gallery3d/data/LocalAlbum;

    .restart local v15    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    goto :goto_3

    .line 471
    .end local v15    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_7
    new-instance v15, Lcom/sec/android/gallery3d/data/LocalAlbum;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_8

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, v19

    invoke-direct {v15, v0, v3, v11, v2}, Lcom/sec/android/gallery3d/data/LocalAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V

    .restart local v15    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    goto :goto_3

    .end local v15    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_8
    const/4 v2, 0x0

    goto :goto_5

    .line 474
    :cond_9
    new-instance v15, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v9, v2, v1}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    .restart local v15    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    goto :goto_3

    .line 481
    :cond_a
    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->setCameraAlbum(Z)V

    goto :goto_4

    .end local v15    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v18    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v10    # "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    :cond_b
    move v13, v14

    .end local v14    # "idx":I
    .restart local v13    # "idx":I
    goto/16 :goto_2
.end method

.method private getDisplayAlbumIds()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    const/4 v6, 0x0

    .line 275
    .local v6, "c":Landroid/database/Cursor;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v9, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "album_display_info_table"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "bucketid_order"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 280
    if-eqz v6, :cond_0

    .line 281
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 282
    .local v10, "orderlist":Ljava/lang/String;
    const-string v0, ";"

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 283
    .local v11, "temp":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v11

    if-ge v8, v0, :cond_0

    .line 284
    aget-object v0, v11, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 291
    .end local v8    # "i":I
    .end local v10    # "orderlist":Ljava/lang/String;
    .end local v11    # "temp":[Ljava/lang/String;
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 293
    :goto_1
    return-object v9

    .line 288
    :catch_0
    move-exception v7

    .line 289
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getLocalAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 17
    .param p1, "manager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p2, "type"    # I
    .param p3, "parent"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "id"    # I
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "filePath"    # Ljava/lang/String;

    .prologue
    .line 735
    invoke-virtual/range {p3 .. p4}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 736
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v14

    .line 737
    .local v14, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v14, :cond_3

    .line 738
    instance-of v1, v14, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-eqz v1, :cond_1

    move-object v1, v14

    .line 741
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v10

    .line 742
    .local v10, "bucketPath":Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->isCameraPath(Ljava/lang/String;)Z

    move-result v12

    .line 744
    .local v12, "isCamera":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCIMFolderMerge:Z

    if-eqz v1, :cond_2

    .line 745
    invoke-static {v10}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->isDcimPath(Ljava/lang/String;)Z

    move-result v13

    .line 749
    .local v13, "isDcim":Z
    :goto_0
    if-nez v12, :cond_0

    if-eqz v13, :cond_1

    :cond_0
    move-object v1, v14

    .line 750
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDCIMName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/data/LocalAlbum;->setName(Ljava/lang/String;)V

    .end local v10    # "bucketPath":Ljava/lang/String;
    .end local v12    # "isCamera":Z
    .end local v13    # "isDcim":Z
    :cond_1
    move-object v1, v14

    .line 754
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    move-object v1, v14

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 755
    check-cast v14, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v14    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object v1, v14

    .line 767
    :goto_1
    return-object v1

    .line 747
    .restart local v10    # "bucketPath":Ljava/lang/String;
    .restart local v12    # "isCamera":Z
    .restart local v14    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    move v13, v12

    .restart local v13    # "isDcim":Z
    goto :goto_0

    .line 758
    .end local v10    # "bucketPath":Ljava/lang/String;
    .end local v12    # "isCamera":Z
    .end local v13    # "isDcim":Z
    :cond_3
    packed-switch p2, :pswitch_data_0

    .line 769
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 760
    :pswitch_1
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v5, 0x1

    move/from16 v4, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 762
    :pswitch_2
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v5, 0x0

    move/from16 v4, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 764
    :pswitch_3
    const/4 v5, 0x2

    sget-object v6, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getLocalAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v15

    .line 765
    .local v15, "palb":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v5, 0x4

    sget-object v6, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getLocalAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v16

    .line 766
    .local v16, "valb":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v11, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 767
    .local v11, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v5, 0x0

    aput-object v15, v4, v5

    const/4 v5, 0x1

    aput-object v16, v4, v5

    invoke-direct {v1, v2, v11, v3, v4}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_1

    .line 758
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPathOnFileSystem(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 10
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "bucketId"    # I

    .prologue
    .line 540
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 541
    .local v1, "uri":Landroid/net/Uri;
    const-string v7, ""

    .line 543
    .local v7, "directoryPath":Ljava/lang/String;
    const/4 v6, 0x0

    .line 545
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 548
    .local v4, "args":[Ljava/lang/String;
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    const-string v3, "bucket_id = ?"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 550
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 552
    .local v9, "mItemPathStr":Ljava/lang/String;
    const/4 v0, 0x0

    const-string v2, "/"

    invoke-virtual {v9, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v9, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 557
    .end local v9    # "mItemPathStr":Ljava/lang/String;
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 559
    .end local v4    # "args":[Ljava/lang/String;
    :goto_0
    return-object v7

    .line 554
    :catch_0
    move-exception v8

    .line 555
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 557
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getPathOnFileSystem(Landroid/content/ContentResolver;Lcom/sec/android/gallery3d/data/Path;)Ljava/lang/String;
    .locals 5
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "albumPath"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 563
    const/4 v2, 0x0

    .line 565
    .local v2, "directoryPath":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 566
    .local v1, "albumPathStr":Ljava/lang/String;
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 568
    .local v0, "albumBucketId":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getPathOnFileSystem(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v2

    .line 570
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_0

    .line 573
    :cond_0
    return-object v2
.end method

.method private getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 145
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 146
    .local v1, "seqIndex":I
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "name":[Ljava/lang/String;
    array-length v2, v0

    add-int/lit8 v3, v1, 0x1

    if-ge v2, v3, :cond_1

    .line 148
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 145
    .end local v0    # "name":[Ljava/lang/String;
    .end local v1    # "seqIndex":I
    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    .line 151
    .restart local v0    # "name":[Ljava/lang/String;
    .restart local v1    # "seqIndex":I
    :cond_1
    aget-object v2, v0, v1

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTypeFromString(Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public static isCameraPath(Ljava/lang/String;)Z
    .locals 3
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 523
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 524
    .local v0, "dirName":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->CAMERA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->CAMERA_EXTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 525
    .local v1, "isDcim":Z
    :cond_1
    if-eqz v1, :cond_2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/data/OcrClustering;->OCR_FOLDER:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 527
    const/4 v1, 0x0

    .line 529
    :cond_2
    return v1
.end method

.method public static isDcimPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 514
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->DCIM_PATH:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 515
    .local v0, "isDcim":Z
    if-eqz v0, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/data/OcrClustering;->OCR_FOLDER:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 517
    const/4 v0, 0x0

    .line 519
    :cond_0
    return v0
.end method

.method private loadBucketEntries(Landroid/database/Cursor;)[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x1

    .line 487
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 488
    .local v0, "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;>;"
    const/4 v3, 0x0

    .line 489
    .local v3, "typeBits":I
    iget v4, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_0

    .line 490
    or-int/lit8 v3, v3, 0x2

    .line 492
    :cond_0
    iget v4, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_1

    .line 493
    or-int/lit8 v3, v3, 0x8

    .line 495
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 496
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    shl-int v4, v6, v4

    and-int/2addr v4, v3

    if-eqz v4, :cond_1

    .line 497
    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 498
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 501
    new-instance v1, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x2

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5, v2}, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 505
    .local v1, "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 506
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 510
    .end local v1    # "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    .end local v2    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    return-object v4
.end method


# virtual methods
.method fakeChange()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->fakeChange()V

    .line 256
    return-void
.end method

.method public getAlbumSetType()I
    .locals 1

    .prologue
    .line 773
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 157
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :goto_0
    return-object v1

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 160
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mAlbums:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTakenTimeofLatestItem()J
    .locals 4

    .prologue
    .line 702
    const-wide/16 v0, -0x1

    .line 703
    .local v0, "takenTime":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "latest_update_item"

    invoke-static {v2, v3, v0, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadLongKey(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    .line 704
    return-wide v0
.end method

.method public getTotalMediaItemCount()I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 578
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 579
    const/4 v7, 0x0

    .line 581
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v6, "(media_type=1 OR media_type=3)"

    .line 585
    .local v6, "WHERE_CLAUSE":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mBaseUri:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "media_type"

    aput-object v4, v2, v3

    const-string v3, "(media_type=1 OR media_type=3)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 588
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mCachedCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 590
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 594
    .end local v6    # "WHERE_CLAUSE":Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mCachedCount:I

    return v0

    .restart local v6    # "WHERE_CLAUSE":Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    move v0, v8

    .line 588
    goto :goto_0

    .line 590
    .end local v6    # "WHERE_CLAUSE":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getTotalMediaItems()[Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 598
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTotalMediaItems(I)[Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getTotalMediaItems(I)[Landroid/database/Cursor;
    .locals 13
    .param p1, "filterType"    # I

    .prologue
    const/4 v0, 0x2

    .line 602
    new-array v12, v0, [Landroid/database/Cursor;

    .line 603
    .local v12, "cursors":[Landroid/database/Cursor;
    const/4 v5, 0x0

    .line 604
    .local v5, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriImage:Landroid/net/Uri;

    .line 607
    .local v1, "imageUri":Landroid/net/Uri;
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    .line 608
    :try_start_0
    const-string v0, "Gallery_Performance"

    const-string v2, "LocalAlbumSet : getTotalMediaItems() - Start"

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    const/4 v3, 0x0

    .line 610
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 611
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    if-eqz v0, :cond_0

    .line 612
    const-string v3, "bucket_id = ?"

    .line 613
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 616
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    const/4 v7, 0x0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "_id"

    aput-object v8, v2, v6

    const/4 v6, 0x1

    const-string v8, "latitude"

    aput-object v8, v2, v6

    const/4 v6, 0x2

    const-string v8, "longitude"

    aput-object v8, v2, v6

    const/4 v6, 0x3

    const-string v8, "datetaken"

    aput-object v8, v2, v6

    const/4 v6, 0x4

    const-string v8, "mime_type"

    aput-object v8, v2, v6

    const/4 v6, 0x5

    const-string v8, "_data"

    aput-object v8, v2, v6

    const/4 v8, 0x6

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    :goto_0
    aput-object v6, v2, v8

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v12, v7

    .line 629
    .end local v3    # "selection":Ljava/lang/String;
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_3

    .line 630
    const/4 v3, 0x0

    .line 631
    .restart local v3    # "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 632
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    if-eqz v0, :cond_2

    .line 633
    const-string v3, "bucket_id = ?"

    .line 634
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 636
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_2
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriVideo:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v9, "_id"

    aput-object v9, v8, v2

    const/4 v2, 0x1

    const-string v9, "latitude"

    aput-object v9, v8, v2

    const/4 v2, 0x2

    const-string v9, "longitude"

    aput-object v9, v8, v2

    const/4 v2, 0x3

    const-string v9, "datetaken"

    aput-object v9, v8, v2

    const/4 v2, 0x4

    const-string v9, "mime_type"

    aput-object v9, v8, v2

    const/4 v2, 0x5

    const-string v9, "_data"

    aput-object v9, v8, v2

    const/4 v11, 0x0

    move-object v9, v3

    move-object v10, v4

    invoke-static/range {v6 .. v11}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v12, v0

    .line 649
    .end local v3    # "selection":Ljava/lang/String;
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_3
    :goto_1
    return-object v12

    .line 616
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_4
    const-string v6, "sef_file_type"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 646
    .end local v3    # "selection":Ljava/lang/String;
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public getTotalMediaItems(IIZ)[Landroid/database/Cursor;
    .locals 13
    .param p1, "filterType"    # I
    .param p2, "sortByType"    # I
    .param p3, "useFakeLoading"    # Z

    .prologue
    .line 653
    const/4 v0, 0x2

    new-array v12, v0, [Landroid/database/Cursor;

    .line 654
    .local v12, "cursors":[Landroid/database/Cursor;
    const/4 v5, 0x0

    .line 655
    .local v5, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriImage:Landroid/net/Uri;

    .line 658
    .local v1, "imageUri":Landroid/net/Uri;
    if-eqz p3, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbFakeLoading:Z

    if-eqz v0, :cond_0

    .line 659
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriImage:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "0,500"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 660
    if-nez p2, :cond_3

    .line 661
    const-string v5, "datetaken DESC"

    .line 664
    :goto_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbFakeLoading:Z

    .line 665
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbNeedFullLoading:Z

    .line 669
    :cond_0
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    .line 670
    :try_start_0
    const-string v0, "Gallery_Performance"

    const-string v2, "LocalAlbumSet : getTotalMediaItems() - Start"

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    const/4 v6, 0x0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "latitude"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "datetaken"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "mime_type"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v4, 0x6

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    aput-object v3, v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v12, v6

    .line 684
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    .line 685
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriVideo:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v8, v2

    const/4 v2, 0x1

    const-string v3, "latitude"

    aput-object v3, v8, v2

    const/4 v2, 0x2

    const-string v3, "longitude"

    aput-object v3, v8, v2

    const/4 v2, 0x3

    const-string v3, "datetaken"

    aput-object v3, v8, v2

    const/4 v2, 0x4

    const-string v3, "mime_type"

    aput-object v3, v8, v2

    const/4 v2, 0x5

    const-string v3, "_data"

    aput-object v3, v8, v2

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static/range {v6 .. v11}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v12, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 698
    :cond_2
    :goto_2
    return-object v12

    .line 663
    :cond_3
    const-string v5, "datetaken ASC"

    goto/16 :goto_0

    .line 671
    :cond_4
    :try_start_1
    const-string v3, "sef_file_type"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 695
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public declared-synchronized isLoading()Z
    .locals 1

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mIsLoading:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected loadSubMediaSets()Ljava/util/ArrayList;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    sget-object v4, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mBaseUri:Landroid/net/Uri;

    .line 300
    .local v4, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 301
    const/4 v15, 0x0

    .line 302
    .local v15, "cursor":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 304
    .local v17, "entries":[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    :try_start_0
    const-string v6, "1) GROUP BY 1,(2"

    .line 305
    .local v6, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    if-eqz v3, :cond_0

    .line 306
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bucket_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mSingleBucketId:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") and ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 308
    :cond_0
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v3, :cond_2

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "content://media/external/gallery_album"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .end local v4    # "uri":Landroid/net/Uri;
    sget-object v5, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "UPPER(bucket_display_name)"

    invoke-static/range {v3 .. v8}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 316
    :goto_0
    if-nez v15, :cond_3

    .line 317
    const-string v3, "LocalAlbumSet"

    const-string v5, "cannot open local database: "

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 430
    :cond_1
    return-object v14

    .line 312
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v5, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->PROJECTION_BUCKET:[Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "UPPER(bucket_display_name)"

    invoke-static/range {v3 .. v8}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    goto :goto_0

    .line 320
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->updateLatestAlbumeInfo()V

    .line 321
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->loadBucketEntries(Landroid/database/Cursor;)[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v17

    .line 323
    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 325
    const/16 v23, 0x0

    .line 326
    .local v23, "offset":I
    const/16 v16, 0x0

    .line 327
    .local v16, "dcimCnt":I
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v14, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 329
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getDisplayAlbumIds()Ljava/util/ArrayList;

    move-result-object v20

    .line 330
    .local v20, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v3, :cond_8

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_8

    .line 331
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v26

    .line 332
    .local v26, "size":I
    const/16 v19, 0x0

    .local v19, "i":I
    move/from16 v24, v23

    .end local v23    # "offset":I
    .local v24, "offset":I
    :goto_1
    move/from16 v0, v19

    move/from16 v1, v26

    if-ge v0, v1, :cond_4

    .line 333
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 334
    .local v21, "index":I
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_13

    .line 335
    add-int/lit8 v23, v24, 0x1

    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v24

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    .line 332
    :goto_2
    add-int/lit8 v19, v19, 0x1

    move/from16 v24, v23

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    goto :goto_1

    .line 323
    .end local v6    # "where":Ljava/lang/String;
    .end local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v14    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    .end local v16    # "dcimCnt":I
    .end local v19    # "i":I
    .end local v20    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v21    # "index":I
    .end local v24    # "offset":I
    .end local v26    # "size":I
    :catchall_0
    move-exception v3

    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 340
    .restart local v6    # "where":Ljava/lang/String;
    .restart local v8    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .restart local v14    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    .restart local v16    # "dcimCnt":I
    .restart local v19    # "i":I
    .restart local v20    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v24    # "offset":I
    .restart local v26    # "size":I
    :cond_4
    const/16 v19, 0x0

    :goto_3
    move-object/from16 v0, v17

    array-length v3, v0

    move/from16 v0, v19

    if-ge v0, v3, :cond_7

    .line 341
    aget-object v18, v17, v19

    .line 342
    .local v18, "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    move-object/from16 v0, v18

    iget v3, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    sget v5, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-eq v3, v5, :cond_5

    move-object/from16 v0, v18

    iget v3, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    sget v5, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SDCARD_CAMERA_BUCKET_ID:I

    if-ne v3, v5, :cond_6

    .line 343
    :cond_5
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    const/4 v5, 0x0

    aget-object v7, v17, v19

    aput-object v7, v3, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v5, v7}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getDcimAlbum(Lcom/sec/android/gallery3d/data/DataManager;[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;Lcom/sec/android/gallery3d/data/Path;I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    :goto_4
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    .line 346
    :cond_6
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v0, v18

    iget v11, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketName:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketPath:Ljava/lang/String;

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v13}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getLocalAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 350
    .end local v18    # "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    :cond_7
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v26

    .line 351
    const/16 v19, 0x0

    move/from16 v22, v26

    .local v22, "n":I
    :goto_5
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    .line 352
    move/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 351
    add-int/lit8 v19, v19, 0x1

    goto :goto_5

    .line 360
    .end local v19    # "i":I
    .end local v22    # "n":I
    .end local v24    # "offset":I
    .end local v26    # "size":I
    .restart local v23    # "offset":I
    :cond_8
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v3, :cond_9

    .line 361
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SECRETBOX_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 362
    .restart local v21    # "index":I
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_9

    .line 363
    move/from16 v25, v23

    .line 364
    .local v25, "pos":I
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    .line 365
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    const/4 v5, 0x0

    aget-object v7, v17, v25

    aput-object v7, v3, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v5, v7}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getDcimAlbum(Lcom/sec/android/gallery3d/data/DataManager;[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;Lcom/sec/android/gallery3d/data/Path;I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    add-int/lit8 v16, v16, 0x1

    move/from16 v23, v24

    .line 371
    .end local v21    # "index":I
    .end local v24    # "offset":I
    .end local v25    # "pos":I
    .restart local v23    # "offset":I
    :cond_9
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 372
    .restart local v21    # "index":I
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_a

    .line 373
    move/from16 v25, v23

    .line 374
    .restart local v25    # "pos":I
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    .line 375
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    const/4 v5, 0x0

    aget-object v7, v17, v25

    aput-object v7, v3, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v5, v7}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getDcimAlbum(Lcom/sec/android/gallery3d/data/DataManager;[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;Lcom/sec/android/gallery3d/data/Path;I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    add-int/lit8 v16, v16, 0x1

    move/from16 v23, v24

    .line 380
    .end local v24    # "offset":I
    .end local v25    # "pos":I
    .restart local v23    # "offset":I
    :cond_a
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SDCARD_CAMERA_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 381
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_b

    .line 382
    move/from16 v25, v23

    .line 383
    .restart local v25    # "pos":I
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    .line 384
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    const/4 v5, 0x0

    aget-object v7, v17, v25

    aput-object v7, v3, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v5, v7}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getDcimAlbum(Lcom/sec/android/gallery3d/data/DataManager;[Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;Lcom/sec/android/gallery3d/data/Path;I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    add-int/lit8 v16, v16, 0x1

    move/from16 v23, v24

    .line 389
    .end local v24    # "offset":I
    .end local v25    # "pos":I
    .restart local v23    # "offset":I
    :cond_b
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->COVER_CAMERA_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 390
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_c

    .line 391
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    move/from16 v23, v24

    .line 394
    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    :cond_c
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SDCARD_COVER_CAMERA_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 395
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_d

    .line 396
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    move/from16 v23, v24

    .line 399
    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    :cond_d
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->GEAR_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 400
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_e

    .line 401
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    move/from16 v23, v24

    .line 404
    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    :cond_e
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SCREENSHOUT_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 405
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_f

    .line 406
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    move/from16 v23, v24

    .line 409
    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    :cond_f
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->COLLAGE_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 410
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_10

    .line 411
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    move/from16 v23, v24

    .line 414
    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    :cond_10
    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->DOWNLOAD_BUCKET_ID:I

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->findBucket([Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;I)I

    move-result v21

    .line 415
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_11

    .line 416
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    move-object/from16 v0, v17

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->circularShiftRight([Ljava/lang/Object;II)V

    move/from16 v23, v24

    .line 422
    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    :cond_11
    move/from16 v19, v16

    .restart local v19    # "i":I
    :goto_6
    move-object/from16 v0, v17

    array-length v3, v0

    move/from16 v0, v19

    if-ge v0, v3, :cond_12

    .line 423
    aget-object v18, v17, v19

    .line 424
    .restart local v18    # "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mType:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    move-object/from16 v0, v18

    iget v11, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    move-object/from16 v0, v18

    iget-object v12, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketName:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketPath:Ljava/lang/String;

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v13}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getLocalAlbum(Lcom/sec/android/gallery3d/data/DataManager;ILcom/sec/android/gallery3d/data/Path;ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    add-int/lit8 v19, v19, 0x1

    goto :goto_6

    .line 427
    .end local v18    # "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    :cond_12
    const/16 v19, 0x0

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v22

    .restart local v22    # "n":I
    :goto_7
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    .line 428
    move/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 427
    add-int/lit8 v19, v19, 0x1

    goto :goto_7

    .end local v22    # "n":I
    .end local v23    # "offset":I
    .restart local v24    # "offset":I
    .restart local v26    # "size":I
    :cond_13
    move/from16 v23, v24

    .end local v24    # "offset":I
    .restart local v23    # "offset":I
    goto/16 :goto_2
.end method

.method public declared-synchronized onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 237
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;>;"
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLoadTask:Z

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p1, :cond_0

    .line 251
    :goto_0
    monitor-exit p0

    return-void

    .line 241
    :cond_0
    :try_start_1
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadBuffer:Ljava/util/ArrayList;

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mIsLoading:Z

    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadBuffer:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadBuffer:Ljava/util/ArrayList;

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/gallery3d/data/LocalAlbumSet$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet$1;-><init>(Lcom/sec/android/gallery3d/data/LocalAlbumSet;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reload()J
    .locals 5

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mNotifierOrder:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v3

    or-int/2addr v2, v3

    if-eqz v2, :cond_1

    .line 204
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLoadTask:Z

    if-eqz v2, :cond_2

    .line 205
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v2, :cond_0

    .line 206
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 210
    :cond_0
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mIsLoading:Z

    .line 211
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLoadTask:Z

    if-eqz v2, :cond_3

    .line 212
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/sec/android/gallery3d/data/LocalAlbumSet$AlbumsLoader;-><init>(Lcom/sec/android/gallery3d/data/LocalAlbumSet;Lcom/sec/android/gallery3d/data/LocalAlbumSet$1;)V

    invoke-virtual {v2, v3, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadTask:Lcom/sec/android/gallery3d/util/Future;

    .line 219
    :goto_1
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mCachedCount:I

    .line 220
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mDataVersion:J

    .line 221
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e025c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setDCIMName(Ljava/lang/String;)V

    .line 224
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadBuffer:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    .line 225
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadBuffer:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 226
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadBuffer:Ljava/util/ArrayList;

    .line 227
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 228
    .local v0, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 203
    .end local v0    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 208
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mNotifierOrder:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->clearDirty()V

    goto :goto_0

    .line 214
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->loadSubMediaSets()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mLoadBuffer:Ljava/util/ArrayList;

    .line 215
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mIsLoading:Z

    goto :goto_1

    .line 230
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mDataVersion:J

    .line 232
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mDataVersion:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-wide v2
.end method

.method public updateLatestAlbumeInfo()V
    .locals 14

    .prologue
    .line 708
    const/4 v7, 0x0

    .line 709
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 710
    .local v6, "bucketId":I
    const-wide/16 v12, -0x1

    .line 711
    .local v12, "takenTime":J
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTakenTimeofLatestItem()J

    move-result-wide v10

    .line 712
    .local v10, "savedTakenTime":J
    const-string v5, "datetaken DESC"

    .line 713
    .local v5, "orderClause":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mWatchUriImage:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 715
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "bucket_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "datetaken"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 719
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 720
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 721
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v12

    .line 726
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 728
    :goto_0
    cmp-long v0, v12, v10

    if-lez v0, :cond_1

    .line 729
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "latest_update_item"

    invoke-static {v0, v2, v12, v13}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;J)V

    .line 730
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "latest_update_album"

    invoke-static {v0, v2, v6}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 732
    :cond_1
    return-void

    .line 723
    :catch_0
    move-exception v8

    .line 724
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 726
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method
