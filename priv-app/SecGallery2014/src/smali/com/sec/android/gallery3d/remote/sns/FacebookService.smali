.class public Lcom/sec/android/gallery3d/remote/sns/FacebookService;
.super Ljava/lang/Object;
.source "FacebookService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;,
        Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;
    }
.end annotation


# static fields
.field private static final CONTACT_ID_QUERY:Ljava/lang/String; = "contact_id = "

.field public static final FACEBOOK_ID:Ljava/lang/String; = "facebook"

.field public static final FACEBOOK_PACKAGE_NAME:Ljava/lang/String; = "com.facebook.katana"


# instance fields
.field public final GET_DATA:I

.field mCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;",
            ">;"
        }
    .end annotation
.end field

.field private mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

.field mMediabitmap:Landroid/graphics/Bitmap;

.field mProfilebitmap:Landroid/graphics/Bitmap;

.field private mSNSRefreshListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;

.field private mSnsFacebook:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

.field mconn:Ljava/net/URLConnection;

.field mprofileconn:Ljava/net/URLConnection;

.field murl:Ljava/net/URL;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSnsFacebook:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    .line 31
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mCache:Ljava/util/HashMap;

    .line 74
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->GET_DATA:I

    .line 235
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/sns/FacebookService;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSNSRefreshListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/remote/sns/FacebookService;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;)Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/FacebookService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSnsFacebook:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    return-object p1
.end method


# virtual methods
.method public FBConnection(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 93
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    if-nez v1, :cond_0

    .line 94
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;-><init>(Lcom/sec/android/gallery3d/remote/sns/FacebookService;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    .line 95
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    invoke-virtual {p1, v1, v2, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public FBDisConnection(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    if-eqz v0, :cond_1

    .line 104
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20Chn:Z

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 106
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mFacebookConnection:Lcom/sec/android/gallery3d/remote/sns/FacebookService$FacebookConnection;

    .line 108
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public getInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    .locals 2
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mCache:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;

    .line 114
    .local v0, "userData":Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;
    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->info()Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    move-result-object v1

    .line 116
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLatestData(Landroid/content/Context;Ljava/lang/String;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSnsFacebook:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    if-nez v2, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    const-string v10, "-1"

    .line 133
    .local v10, "ID_NOT_FOUND":Ljava/lang/String;
    const-string v17, "-1"

    .line 134
    .local v17, "user_id":Ljava/lang/String;
    const-string v11, "-1"

    .line 136
    .local v11, "contact_id":Ljava/lang/String;
    const/4 v12, 0x0

    .line 139
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "entities"

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 143
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "contact_id"

    aput-object v6, v4, v5

    const-string v5, "mimetype = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v8, "vnd.android.cursor.item/vnd.facebook.profile"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 147
    if-eqz v12, :cond_4

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 148
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_4

    .line 149
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 150
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 153
    .end local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v14

    .line 154
    .local v14, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 159
    .end local v14    # "e":Ljava/lang/Exception;
    :goto_2
    const-string v2, "-1"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 164
    const/4 v15, 0x0

    .line 165
    .local v15, "rawCursor":Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 167
    .local v13, "dataCursor":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v7, "sourceid"

    aput-object v7, v6, v2

    const/4 v2, 0x1

    const-string v7, "_id"

    aput-object v7, v6, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "contact_id = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 171
    if-eqz v15, :cond_3

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 173
    :goto_3
    invoke-interface {v15}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_3

    .line 174
    const/4 v2, 0x0

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 175
    const/4 v2, 0x1

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 176
    .local v16, "rawId":Ljava/lang/String;
    if-eqz v13, :cond_2

    .line 177
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 178
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v2

    const-string v7, "raw_contact_id = ? and mimetype = ?"

    const/4 v2, 0x2

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v16, v8, v2

    const/4 v2, 0x1

    const-string/jumbo v9, "vnd.android.cursor.item/vnd.facebook.profile"

    aput-object v9, v8, v2

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 184
    if-eqz v13, :cond_5

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-lez v2, :cond_5

    .line 195
    .end local v16    # "rawId":Ljava/lang/String;
    :cond_3
    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 196
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 199
    :goto_4
    const-string v2, "-1"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 204
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSnsFacebook:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    new-instance v4, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v4, v0, v1}, Lcom/sec/android/gallery3d/remote/sns/FacebookService$1;-><init>(Lcom/sec/android/gallery3d/remote/sns/FacebookService;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v2, v0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;->getStatus(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 230
    :catch_1
    move-exception v14

    .line 231
    .local v14, "e":Landroid/os/RemoteException;
    invoke-virtual {v14}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 156
    .end local v13    # "dataCursor":Landroid/database/Cursor;
    .end local v14    # "e":Landroid/os/RemoteException;
    .end local v15    # "rawCursor":Landroid/database/Cursor;
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_4
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_2

    .end local v3    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v2

    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 187
    .restart local v13    # "dataCursor":Landroid/database/Cursor;
    .restart local v15    # "rawCursor":Landroid/database/Cursor;
    .restart local v16    # "rawId":Ljava/lang/String;
    :cond_5
    :try_start_4
    const-string v17, "-1"

    .line 189
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    .line 192
    .end local v16    # "rawId":Ljava/lang/String;
    :catch_2
    move-exception v14

    .line 193
    .local v14, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 195
    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 196
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_4

    .line 195
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    invoke-static {v15}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 196
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public hasInfo(Ljava/lang/String;)Z
    .locals 1
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method public registerSNSRefreshListener(Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSNSRefreshListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;

    .line 86
    return-void
.end method

.method public unregisterSNSRefreshListener()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->mSNSRefreshListener:Lcom/sec/android/gallery3d/ui/FaceIndicatorView$SNSRefreshListener;

    .line 90
    return-void
.end method
