.class public interface abstract Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;
.super Ljava/lang/Object;
.source "FilmStripView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/FilmStripView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onLongTap(I)V
.end method

.method public abstract onSelectionChanged(Lcom/sec/android/gallery3d/data/MediaItem;ZI)V
.end method

.method public abstract onSelectionModeChanged(Z)V
.end method

.method public abstract onSlotSelected(I)Z
.end method
