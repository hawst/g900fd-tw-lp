.class public Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;
.super Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;
.source "SinglePhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;
    }
.end annotation


# static fields
.field private static final MSG_UPDATE_IMAGE:I = 0x1

.field private static final SIZE_BACKUP:I = 0x400

.field private static final TAG:Ljava/lang/String; = "SinglePhotoDataAdapter"


# instance fields
.field private mAGIF:Lcom/quramsoft/agif/QuramAGIF;

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

.field private mDecodeDirect:Z

.field private mDecodeDirectTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mHasFullImage:Z

.field private mIsAlive:Z

.field private mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mLargeListener:Lcom/sec/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field private mLoadingState:I

.field private mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

.field private mTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mThumbListener:Lcom/sec/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;-><init>()V

    .line 61
    iput v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mDecodeDirect:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 133
    new-instance v0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$2;-><init>(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/sec/android/gallery3d/util/FutureListener;

    .line 159
    new-instance v0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$3;-><init>(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mThumbListener:Lcom/sec/android/gallery3d/util/FutureListener;

    .line 74
    invoke-static {p3}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 75
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    const-wide/16 v6, 0x40

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    .line 77
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PhotoView;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mIsAlive:Z

    .line 80
    new-instance v0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$1;-><init>(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mHandler:Landroid/os/Handler;

    .line 117
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 119
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 121
    return-void

    :cond_0
    move v0, v2

    .line 75
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mIsAlive:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;)Lcom/sec/android/gallery3d/app/LoadingListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mDecodeDirect:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mDecodeDirect:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->onDecodeDirectComplete(Lcom/sec/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->onDecodeLargeComplete(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->onDecodeThumbComplete(Lcom/sec/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private onDecodeDirectComplete(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 375
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/graphics/Bitmap;>;"
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 376
    .local v0, "backup":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image/golf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    new-instance v2, Lcom/sec/android/gallery3d/golf/GolfMgr;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/golf/GolfMgr;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/golf/GolfMgr;->CreateThumbnails(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 380
    :cond_0
    if-nez v0, :cond_1

    .line 381
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->updateBrokenImage()V

    .line 383
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 395
    .end local v0    # "backup":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 386
    .restart local v0    # "backup":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    .line 387
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->setScreenNail(Landroid/graphics/Bitmap;II)V

    .line 388
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 389
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 390
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 391
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyCurrentImageInvalidated()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 392
    .end local v0    # "backup":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 393
    .local v1, "t":Ljava/lang/Throwable;
    const-string v2, "SinglePhotoDataAdapter"

    const-string v3, "fail to decode thumb"

    invoke-static {v2, v3, v1}, Lcom/sec/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private onDecodeLargeComplete(Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;)V
    .locals 4
    .param p1, "bundle"    # Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;

    .prologue
    .line 181
    :try_start_0
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-nez v1, :cond_1

    .line 183
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mDecodeDirect:Z

    if-nez v1, :cond_0

    .line 184
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mDecodeDirect:Z

    .line 185
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mThumbListener:Lcom/sec/android/gallery3d/util/FutureListener;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mDecodeDirectTask:Lcom/sec/android/gallery3d/util/Future;

    .line 208
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->updateBrokenImage()V

    .line 189
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    .line 190
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "SinglePhotoDataAdapter"

    const-string v2, "fail to decode large"

    invoke-static {v1, v2, v0}, Lcom/sec/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 195
    .end local v0    # "t":Ljava/lang/Throwable;
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    iput v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    .line 196
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->backupImage:Landroid/graphics/Bitmap;

    iget-object v2, p1, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v2

    iget-object v3, p1, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->setScreenNail(Landroid/graphics/Bitmap;II)V

    .line 198
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->setRegionDecoder(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 199
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 203
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyCurrentImageInvalidated()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private onDecodeThumbComplete(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/graphics/Bitmap;>;"
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 213
    .local v0, "backup":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 214
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->updateBrokenImage()V

    .line 217
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 234
    .end local v0    # "backup":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 221
    .restart local v0    # "backup":Landroid/graphics/Bitmap;
    :cond_0
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    .line 223
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->setScreenNail(Landroid/graphics/Bitmap;II)V

    .line 224
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 226
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 227
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 229
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyCurrentImageInvalidated()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 231
    .end local v0    # "backup":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 232
    .local v1, "t":Ljava/lang/Throwable;
    const-string v2, "SinglePhotoDataAdapter"

    const-string v3, "fail to decode thumb"

    invoke-static {v2, v3, v1}, Lcom/sec/android/gallery3d/app/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setScreenNail(Landroid/graphics/Bitmap;II)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 174
    new-instance v0, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v0, p1}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    .line 175
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 176
    return-void
.end method


# virtual methods
.method public changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 439
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mIsAlive:Z

    .line 430
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 431
    return-void
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method public getImageRotation()I
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    return v0
.end method

.method public getImageRotation(I)I
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 297
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V
    .locals 1
    .param p1, "offset"    # I
    .param p2, "size"    # Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    .prologue
    const/4 v0, 0x0

    .line 286
    if-nez p1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v0

    iput v0, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 288
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    iput v0, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 293
    :goto_0
    return-void

    .line 290
    :cond_0
    iput v0, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 291
    iput v0, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public getLoadingState(I)I
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 363
    iget v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingState:I

    return v0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 338
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 302
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public is3DTour(I)Z
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v2, 0x400

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    return v0
.end method

.method public isCamera(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 312
    const/4 v0, 0x0

    return v0
.end method

.method public isDeletable(I)Z
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    return v0
.end method

.method public isGolfShot(I)Z
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    return v0
.end method

.method public isPanorama(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 317
    const/4 v0, 0x0

    return v0
.end method

.method public isStaticCamera(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 322
    const/4 v0, 0x0

    return v0
.end method

.method public isVideo(I)Z
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v2, 0x400

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveTo(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 275
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public moveToFastest(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 282
    return-void
.end method

.method public onAgifPlayRequests()V
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    .line 420
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    if-nez v0, :cond_0

    .line 425
    :goto_0
    return-void

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    invoke-virtual {v0}, Lcom/quramsoft/agif/QuramAGIF;->getQURAMWINKUTIL()Lcom/quramsoft/agif/QURAMWINKUTIL;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

    .line 424
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v1}, Lcom/quramsoft/agif/QURAMWINKUTIL;->updateAGIF(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 256
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 265
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 267
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;->recycle()V

    .line 269
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mBitmapScreenNail:Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    .line 271
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    if-nez v0, :cond_0

    .line 242
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mHasFullImage:Z

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLargeListener:Lcom/sec/android/gallery3d/util/FutureListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mThumbListener:Lcom/sec/android/gallery3d/util/FutureListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0
.end method

.method public setCurrentPhoto(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "indexHint"    # I

    .prologue
    .line 349
    return-void
.end method

.method public setFocusHintDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 354
    return-void
.end method

.method public setFocusHintPath(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 359
    return-void
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 435
    return-void
.end method

.method public setNeedFullImage(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 308
    return-void
.end method

.method public updateBrokenImage()V
    .locals 4

    .prologue
    .line 403
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a7

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 405
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->updateBrokenImage(Landroid/graphics/Bitmap;)V

    .line 406
    return-void
.end method

.method public updateBrokenImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "backup"    # Landroid/graphics/Bitmap;

    .prologue
    .line 409
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->setScreenNail(Landroid/graphics/Bitmap;II)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 412
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyCurrentImageInvalidated()V

    .line 413
    return-void
.end method

.method public useLoadingProgress(I)Z
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 454
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 455
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    .line 456
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "s":Ljava/lang/String;
    const-string v2, "http://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 458
    const/4 v2, 0x1

    .line 461
    .end local v0    # "s":Ljava/lang/String;
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
