.class public Lcom/sec/android/gallery3d/app/ContextualTagSetting;
.super Landroid/app/Activity;
.source "ContextualTagSetting.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final CONTEXTUAL_TAG_ENABLE:Ljava/lang/String; = "ContextualTagEnable"

.field public static final CONTEXTUAL_TAG_LIST:Ljava/lang/String; = "ContextualTagList"

.field private static isContextualTagEnabled:Z

.field private static isContextualTagEnabledInited:Z


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mCategoryLinear:Landroid/widget/LinearLayout;

.field private mCategoryTextView:Landroid/widget/TextView;

.field private mCheckedItem:I

.field private mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

.field private mContext:Landroid/content/Context;

.field mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

.field private mDateLinear:Landroid/widget/LinearLayout;

.field private mDateTextView:Landroid/widget/TextView;

.field private mDragSelectedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mLocationLinear:Landroid/widget/LinearLayout;

.field private mLocationTextView:Landroid/widget/TextView;

.field private mScrollview:Landroid/view/View;

.field private mTagBuddyPermissionHandler:Landroid/os/Handler;

.field private mTagLinear:Landroid/widget/LinearLayout;

.field private mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

.field private mUserTagsTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 636
    sput-boolean v0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabledInited:Z

    .line 637
    sput-boolean v0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 78
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    .line 80
    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 82
    new-instance v0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting$1;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mTagBuddyPermissionHandler:Landroid/os/Handler;

    .line 269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;

    .line 271
    new-instance v0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/app/ContextualTagSetting;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->processItemClick(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 53
    sput-boolean p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabledInited:Z

    return p0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 53
    sput-boolean p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabled:Z

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/ContextualTagSetting;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setVisiblePreviewText(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/ContextualTagSetting;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->updatePreviewTextVisible(IZ)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mScrollview:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getContextualTagEnable(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 639
    sget-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabledInited:Z

    if-nez v1, :cond_0

    .line 640
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 641
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "ContextualTagEnable"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabled:Z

    .line 642
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabledInited:Z

    .line 644
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabled:Z

    return v1
.end method

.method public static getContextualTagStatus(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 627
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 628
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "ContextualTagList"

    const/16 v2, 0x1111

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private initLayout(Z)V
    .locals 14
    .param p1, "isAutoUpdated"    # Z

    .prologue
    .line 150
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/Switch;->setChecked(Z)V

    .line 152
    if-eqz p1, :cond_0

    .line 153
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    .line 154
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    if-nez v9, :cond_0

    .line 155
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Switch;->setChecked(Z)V

    .line 159
    :cond_0
    const v9, 0x7f0f00b7

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ListView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    .line 160
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v1, "contextualData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/util/ContextualListClass;>;"
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e0103

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e0234

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v9, :cond_1

    .line 166
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e00a9

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListClass;

    const v10, 0x7f0e0235

    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/util/ContextualListClass;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_1
    new-instance v9, Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    invoke-direct {v9, p0, v1}, Lcom/sec/samsung/gallery/util/ContextualListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    .line 170
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 172
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 173
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit16 v9, v9, 0x1000

    const/16 v10, 0x1000

    if-ne v9, v10, :cond_2

    .line 174
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v9, :cond_6

    .line 175
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 180
    :cond_2
    :goto_0
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit16 v9, v9, 0x100

    const/16 v10, 0x100

    if-ne v9, v10, :cond_3

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 181
    :cond_3
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit8 v9, v9, 0x10

    const/16 v10, 0x10

    if-ne v9, v10, :cond_4

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x2

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 182
    :cond_4
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit8 v9, v9, 0x1

    const/4 v10, 0x1

    if-ne v9, v10, :cond_5

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x3

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 183
    :cond_5
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 184
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 185
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_f

    .line 188
    const/4 v5, 0x0

    .line 189
    .local v5, "listviewTotalheight":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getCount()I

    move-result v9

    if-ge v4, v9, :cond_7

    .line 190
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContextualListAdapter:Lcom/sec/samsung/gallery/util/ContextualListAdapter;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, v4, v10, v11}, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 191
    .local v0, "childView":Landroid/view/View;
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Landroid/view/View;->measure(II)V

    .line 193
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v10

    add-int/2addr v9, v10

    add-int/2addr v5, v9

    .line 189
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 177
    .end local v0    # "childView":Landroid/view/View;
    .end local v4    # "i":I
    .end local v5    # "listviewTotalheight":I
    :cond_6
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0

    .line 195
    .restart local v4    # "i":I
    .restart local v5    # "listviewTotalheight":I
    :cond_7
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iput v5, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 206
    .end local v4    # "i":I
    .end local v5    # "listviewTotalheight":I
    :cond_8
    :goto_2
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 207
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAlpha(F)V

    .line 212
    :goto_3
    const v9, 0x7f0f00af

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateLinear:Landroid/widget/LinearLayout;

    .line 213
    const v9, 0x7f0f00b1

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mLocationLinear:Landroid/widget/LinearLayout;

    .line 214
    const v9, 0x7f0f00b3

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCategoryLinear:Landroid/widget/LinearLayout;

    .line 215
    const v9, 0x7f0f00b5

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mTagLinear:Landroid/widget/LinearLayout;

    .line 216
    const v9, 0x7f0f00a4

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mLocationTextView:Landroid/widget/TextView;

    .line 217
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mLocationTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 218
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v9, :cond_9

    .line 219
    const v9, 0x7f0f00a6

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mUserTagsTextView:Landroid/widget/TextView;

    .line 220
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mUserTagsTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 222
    :cond_9
    const v9, 0x7f0f00a3

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateTextView:Landroid/widget/TextView;

    .line 224
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    if-eqz v9, :cond_a

    .line 225
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateTextView:Landroid/widget/TextView;

    const v10, -0xf0f10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 227
    :cond_a
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 229
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v9, :cond_b

    .line 230
    const v9, 0x7f0f00a5

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCategoryTextView:Landroid/widget/TextView;

    .line 231
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCategoryTextView:Landroid/widget/TextView;

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, -0x1000000

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 234
    :cond_b
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x400

    const/16 v11, 0x400

    invoke-virtual {v9, v10, v11}, Landroid/view/Window;->setFlags(II)V

    .line 238
    :cond_c
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 239
    iget v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setVisiblePreviewText(I)V

    .line 243
    :goto_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_d

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v9, :cond_d

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 245
    .local v2, "deco":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v8

    .line 246
    .local v8, "vto2":Landroid/view/ViewTreeObserver;
    new-instance v9, Lcom/sec/android/gallery3d/app/ContextualTagSetting$2;

    invoke-direct {v9, p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting$2;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)V

    invoke-virtual {v8, v9}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 254
    .end local v2    # "deco":Landroid/view/View;
    .end local v8    # "vto2":Landroid/view/ViewTreeObserver;
    :cond_d
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-nez v9, :cond_e

    .line 255
    const v9, 0x7f0f00ae

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mScrollview:Landroid/view/View;

    .line 256
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mScrollview:Landroid/view/View;

    if-eqz v9, :cond_12

    .line 257
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mScrollview:Landroid/view/View;

    new-instance v10, Lcom/sec/android/gallery3d/app/ContextualTagSetting$3;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting$3;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)V

    invoke-virtual {v9, v10}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 267
    :cond_e
    :goto_5
    return-void

    .line 196
    :cond_f
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-nez v9, :cond_8

    .line 197
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_8

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 199
    .local v3, "display":Landroid/view/Display;
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 200
    .local v7, "size":Landroid/graphics/Point;
    invoke-virtual {v3, v7}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 201
    const v9, 0x7f0f0051

    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 202
    .local v6, "previewImage":Landroid/widget/ImageView;
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Point;->x:I

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    iget v11, v11, Landroid/view/ViewGroup$LayoutParams;->width:I

    sub-int/2addr v10, v11

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto/16 :goto_2

    .line 209
    .end local v3    # "display":Landroid/view/Display;
    .end local v6    # "previewImage":Landroid/widget/ImageView;
    .end local v7    # "size":Landroid/graphics/Point;
    :cond_10
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const v10, 0x3e99999a    # 0.3f

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAlpha(F)V

    goto/16 :goto_3

    .line 241
    :cond_11
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setVisiblePreviewText(I)V

    goto/16 :goto_4

    .line 263
    :cond_12
    iget-object v9, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/widget/ListView;->scrollTo(II)V

    goto :goto_5
.end method

.method private processItemClick(Landroid/view/View;)V
    .locals 10
    .param p1, "arg1"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 446
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 447
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    .line 452
    .local v3, "sb":Landroid/util/SparseBooleanArray;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 453
    .local v5, "sp":Landroid/content/SharedPreferences;
    move-object v4, p1

    .line 455
    .local v4, "selectView":Landroid/view/View;
    const/4 v0, 0x0

    .line 457
    .local v0, "checkedItem":I
    if-eqz v4, :cond_0

    .line 458
    const v6, 0x7f0f00a1

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 462
    .local v2, "itemCheckBox":Landroid/widget/CheckBox;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatusdate"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 463
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuslocation"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 464
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuscategory"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 465
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatususertags"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 467
    if-eqz v2, :cond_2

    .line 468
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 469
    invoke-virtual {v2, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 474
    :goto_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 475
    invoke-virtual {v2, v8}, Landroid/widget/CheckBox;->sendAccessibilityEvent(I)V

    .line 478
    :cond_2
    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->size()I

    move-result v6

    if-eqz v6, :cond_8

    .line 479
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getCount()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_2
    if-ltz v1, :cond_8

    .line 480
    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 481
    const/4 v6, 0x3

    if-ne v1, v6, :cond_5

    .line 482
    const/4 v0, 0x1

    .line 483
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatususertags"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 479
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 471
    .end local v1    # "i":I
    :cond_4
    invoke-virtual {v2, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 484
    .restart local v1    # "i":I
    :cond_5
    const/4 v6, 0x2

    if-ne v1, v6, :cond_6

    .line 485
    add-int/lit8 v0, v0, 0x10

    .line 486
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuscategory"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_3

    .line 487
    :cond_6
    if-ne v1, v8, :cond_7

    .line 488
    add-int/lit16 v0, v0, 0x100

    .line 489
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatuslocation"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_3

    .line 491
    :cond_7
    add-int/lit16 v0, v0, 0x1000

    .line 492
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "checkstatusdate"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_3

    .line 498
    .end local v1    # "i":I
    :cond_8
    iput v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    .line 499
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setVisiblePreviewText(I)V

    .line 500
    iget v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    invoke-static {p0, v6}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagStatus(Landroid/content/Context;I)V

    .line 502
    if-nez v0, :cond_0

    .line 503
    invoke-static {p0, v9}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 504
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/Switch;->setChecked(Z)V

    goto/16 :goto_0
.end method

.method private processOnCheckedChanged(Z)V
    .locals 2
    .param p1, "ischecked"    # Z

    .prologue
    .line 421
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 423
    if-eqz p1, :cond_1

    .line 424
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 425
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setVisiblePreviewText(I)V

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 435
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->updatePreviewTextVisible(IZ)V

    goto :goto_0
.end method

.method public static setContextualTagEnable(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isChecked"    # Z

    .prologue
    .line 648
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 649
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ContextualTagEnable"

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 651
    .local v0, "isCommited":Z
    if-eqz v0, :cond_0

    .line 652
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabledInited:Z

    .line 653
    sput-boolean p1, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->isContextualTagEnabled:Z

    .line 655
    :cond_0
    return-void
.end method

.method public static setContextualTagStatus(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "checkedItem"    # I

    .prologue
    .line 632
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 633
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "ContextualTagList"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 634
    return-void
.end method

.method private setVisiblePreviewText(I)V
    .locals 4
    .param p1, "checkedItem"    # I

    .prologue
    .line 509
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_VERTICAL_TOP_MARGIN:I

    .line 513
    .local v1, "positionY":I
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 514
    .local v0, "point":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 515
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    if-le v2, v3, :cond_0

    .line 516
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_HORIZONTAL_TOP_MARGIN:I

    .line 519
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 520
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v1

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v3

    div-int v1, v2, v3

    .line 527
    :cond_1
    and-int/lit16 v2, p1, 0x110

    if-nez v2, :cond_5

    .line 528
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 533
    :goto_0
    and-int/lit8 v2, p1, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    .line 537
    and-int/lit16 v2, p1, 0x100

    if-nez v2, :cond_6

    .line 538
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 543
    :cond_2
    :goto_1
    and-int/lit16 v2, p1, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_3

    .line 547
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 549
    :cond_3
    and-int/lit16 v2, p1, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_4

    .line 553
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v1, v2

    .line 564
    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->updatePreviewTextVisible(IZ)V

    .line 568
    return-void

    .line 530
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_BIG:I

    sub-int/2addr v1, v2

    goto :goto_0

    .line 540
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_BIG:I

    sub-int/2addr v1, v2

    goto :goto_1
.end method

.method private updatePreviewTextVisible(IZ)V
    .locals 6
    .param p1, "checkedItem"    # I
    .param p2, "disableAll"    # Z

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x80

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 571
    if-eqz p2, :cond_4

    .line 572
    const/4 p1, 0x0

    .line 573
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v0, :cond_3

    .line 574
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 585
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-nez v0, :cond_1

    .line 586
    and-int/lit16 v0, p1, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_5

    .line 587
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 595
    :cond_1
    :goto_1
    and-int/lit16 v0, p1, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 596
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mLocationLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 604
    :goto_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_2

    .line 605
    and-int/lit8 v0, p1, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 606
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCategoryLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 614
    :goto_3
    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 615
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mTagLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 624
    :cond_2
    :goto_4
    return-void

    .line 576
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 579
    :cond_4
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 581
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 589
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_1

    .line 598
    :cond_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mLocationTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 599
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mLocationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 601
    :cond_7
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mLocationLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    .line 608
    :cond_8
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCategoryTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 609
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCategoryTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 611
    :cond_9
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCategoryLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 617
    :cond_a
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mUserTagsTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 618
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mUserTagsTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 620
    :cond_b
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mTagLinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_4
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "ischecked"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 400
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    if-ne v1, v3, :cond_1

    .line 401
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;

    const-string v2, "TagBuddyPermissionAlertDialogOff"

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    if-ne p2, v3, :cond_0

    .line 403
    invoke-virtual {p1, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 405
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;

    aput-object v1, v0, v4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mTagBuddyPermissionHandler:Landroid/os/Handler;

    aput-object v2, v0, v1

    .line 408
    .local v0, "params":[Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getGalleryID()I

    move-result v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(I)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_USAGE_ALERT_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 418
    .end local v0    # "params":[Ljava/lang/Object;
    :goto_0
    return-void

    .line 411
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->processOnCheckedChanged(Z)V

    goto :goto_0

    .line 414
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TAGB"

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 416
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->processOnCheckedChanged(Z)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 326
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 327
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 337
    :goto_0
    return-void

    .line 330
    :pswitch_0
    const v0, 0x7f030041

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContentView(I)V

    .line 331
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->initLayout(Z)V

    goto :goto_0

    .line 327
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x1000

    const/16 v5, 0x10

    const/4 v7, -0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 119
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 121
    const v2, 0x7f030041

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContentView(I)V

    .line 123
    iput-object p0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;

    .line 124
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 125
    const-string v2, "accessibility"

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 128
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 129
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 130
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 131
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 132
    invoke-virtual {v0, v3, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 134
    new-instance v2, Landroid/widget/Switch;

    invoke-direct {v2, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0128

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 137
    .local v1, "padding":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v1, v4, v1, v4}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v4, Landroid/app/ActionBar$LayoutParams;

    const v5, 0x800015

    invoke-direct {v4, v7, v7, v5}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3, v4}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 144
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 146
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->initLayout(Z)V

    .line 147
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 341
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    .line 342
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    if-nez v0, :cond_0

    .line 343
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 346
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 348
    invoke-static {}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->releaseInstance()V

    .line 349
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 442
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->processItemClick(Landroid/view/View;)V

    .line 443
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 667
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x42

    if-eq p2, v0, :cond_0

    const/16 v0, 0x17

    if-ne p2, v0, :cond_1

    .line 669
    :cond_0
    const/4 v0, 0x1

    .line 671
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 379
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 380
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 382
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v1, :cond_1

    .line 383
    const/16 v0, 0x111

    .line 387
    .local v0, "dateTagDefaultCheck":I
    :goto_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v1

    and-int/2addr v1, v0

    if-nez v1, :cond_0

    .line 388
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 389
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const v2, 0x3e99999a    # 0.3f

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAlpha(F)V

    .line 390
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 394
    .end local v0    # "dateTagDefaultCheck":I
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1

    .line 385
    :cond_1
    const/16 v0, 0x1111

    .restart local v0    # "dateTagDefaultCheck":I
    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 353
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 374
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 357
    :sswitch_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v2, :cond_1

    .line 358
    const/16 v0, 0x111

    .line 362
    .local v0, "dateTagDefaultCheck":I
    :goto_1
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v2

    and-int/2addr v2, v0

    if-nez v2, :cond_0

    .line 363
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 364
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const v3, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAlpha(F)V

    .line 365
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 367
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->finish()V

    goto :goto_0

    .line 360
    .end local v0    # "dateTagDefaultCheck":I
    :cond_1
    const/16 v0, 0x1111

    .restart local v0    # "dateTagDefaultCheck":I
    goto :goto_1

    .line 370
    .end local v0    # "dateTagDefaultCheck":I
    :sswitch_1
    iget v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    invoke-static {p0, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagStatus(Landroid/content/Context;I)V

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->finish()V

    goto :goto_0

    .line 353
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0259 -> :sswitch_0
        0x7f0f025a -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 299
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 300
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_3

    .line 301
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    .line 302
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 303
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 304
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 305
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setVisiblePreviewText(I)V

    .line 311
    :goto_0
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    .line 312
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v0, :cond_5

    .line 313
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 318
    :cond_0
    :goto_1
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 319
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 320
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mCheckedItem:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 322
    :cond_3
    return-void

    .line 307
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 308
    invoke-direct {p0, v3, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->updatePreviewTextVisible(IZ)V

    goto :goto_0

    .line 315
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 659
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 660
    const/4 v0, 0x1

    .line 662
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
