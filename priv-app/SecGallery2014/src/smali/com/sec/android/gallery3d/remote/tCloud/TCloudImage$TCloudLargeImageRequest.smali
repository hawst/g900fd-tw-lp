.class Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;
.super Ljava/lang/Object;
.source "TCloudImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TCloudLargeImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 10
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->access$100(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseImageUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget-wide v6, v6, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 222
    .local v4, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 224
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->access$100(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    move-result-object v6

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->access$200(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    iget-wide v8, v7, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->id:J

    invoke-virtual {v6, v5, v8, v9}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getImage(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 225
    .local v3, "path":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 226
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;->access$300(Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 232
    .end local v3    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 233
    const/4 v5, 0x0

    invoke-static {p1, v2, v5}, Lcom/sec/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    .line 234
    :cond_1
    return-object v0

    .line 227
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 229
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 230
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage$TCloudLargeImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method
