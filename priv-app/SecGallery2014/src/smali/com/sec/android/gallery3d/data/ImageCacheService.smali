.class public Lcom/sec/android/gallery3d/data/ImageCacheService;
.super Ljava/lang/Object;
.source "ImageCacheService.java"


# instance fields
.field private final mDiskCache:Lcom/sec/android/gallery3d/data/ImageCachable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/sec/android/gallery3d/data/ImageDiskCache;

    invoke-direct {v0, p1}, Lcom/sec/android/gallery3d/data/ImageDiskCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheService;->mDiskCache:Lcom/sec/android/gallery3d/data/ImageCachable;

    .line 30
    return-void
.end method

.method private makeKey(Lcom/sec/android/gallery3d/data/Path;JI)[B
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "timeModified"    # J
    .param p4, "type"    # I

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clearImageData(Lcom/sec/android/gallery3d/data/Path;JI)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "timeModified"    # J
    .param p4, "type"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/ImageCacheService;->makeKey(Lcom/sec/android/gallery3d/data/Path;JI)[B

    move-result-object v0

    .line 47
    .local v0, "key":[B
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ImageCacheService;->mDiskCache:Lcom/sec/android/gallery3d/data/ImageCachable;

    invoke-interface {v1, v0, p4}, Lcom/sec/android/gallery3d/data/ImageCachable;->clearImageData([BI)V

    .line 48
    return-void
.end method

.method public getCurrentDiskCacheSize()J
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ImageCacheService;->mDiskCache:Lcom/sec/android/gallery3d/data/ImageCachable;

    check-cast v0, Lcom/sec/android/gallery3d/data/ImageDiskCache;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ImageDiskCache;->getCurrentDiskCacheSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "timeModified"    # J
    .param p4, "type"    # I
    .param p5, "buffer"    # Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/ImageCacheService;->makeKey(Lcom/sec/android/gallery3d/data/Path;JI)[B

    move-result-object v0

    .line 35
    .local v0, "key":[B
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ImageCacheService;->mDiskCache:Lcom/sec/android/gallery3d/data/ImageCachable;

    invoke-interface {v1, v0, p4, p5}, Lcom/sec/android/gallery3d/data/ImageCachable;->getImageData([BILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v1

    return v1
.end method

.method public getImageDiskCacheMaxBytes()J
    .locals 2

    .prologue
    .line 55
    const-wide/32 v0, 0xc800000

    return-wide v0
.end method

.method public putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "timeModified"    # J
    .param p4, "type"    # I
    .param p5, "value"    # [B

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/ImageCacheService;->makeKey(Lcom/sec/android/gallery3d/data/Path;JI)[B

    move-result-object v0

    .line 41
    .local v0, "key":[B
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ImageCacheService;->mDiskCache:Lcom/sec/android/gallery3d/data/ImageCachable;

    invoke-interface {v1, v0, p4, p5}, Lcom/sec/android/gallery3d/data/ImageCachable;->putImageData([BI[B)V

    .line 42
    return-void
.end method
