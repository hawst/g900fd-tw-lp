.class final Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;
.super Ljava/lang/Object;
.source "PicasaContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SyncContext"
.end annotation


# instance fields
.field public accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

.field public final albumsAdded:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public albumsChanged:Z

.field public api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

.field public db:Landroid/database/sqlite/SQLiteDatabase;

.field public photosChanged:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 910
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 896
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    .line 902
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->albumsAdded:Ljava/util/ArrayList;

    .line 905
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->albumsChanged:Z

    .line 908
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->photosChanged:Z

    .line 911
    iget-object v0, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 912
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 920
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 921
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->albumsChanged:Z

    if-eqz v1, :cond_0

    .line 922
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 924
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->photosChanged:Z

    if-eqz v1, :cond_1

    .line 925
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 927
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->albumsChanged:Z

    .line 928
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->photosChanged:Z

    .line 929
    return-void
.end method

.method public login(Ljava/lang/String;)Z
    .locals 6
    .param p1, "user"    # Ljava/lang/String;

    .prologue
    .line 932
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    if-nez v5, :cond_0

    .line 933
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->reloadAccounts()V

    .line 935
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    .line 936
    .local v2, "authAccounts":[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    move-object v0, v2

    .local v0, "arr$":[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    .line 937
    .local v1, "auth":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    iget-object v5, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 938
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->api:Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;

    invoke-virtual {v5, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->setAuth(Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;)V

    .line 939
    const/4 v5, 0x1

    .line 942
    .end local v1    # "auth":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    :goto_1
    return v5

    .line 936
    .restart local v1    # "auth":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 942
    .end local v1    # "auth":Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public reloadAccounts()V
    .locals 1

    .prologue
    .line 915
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAuthenticatedAccounts(Landroid/content/Context;)[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->accounts:[Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;

    .line 916
    return-void
.end method
