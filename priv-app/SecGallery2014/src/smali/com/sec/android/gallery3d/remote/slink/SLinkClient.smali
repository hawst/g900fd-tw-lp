.class public Lcom/sec/android/gallery3d/remote/slink/SLinkClient;
.super Ljava/lang/Object;
.source "SLinkClient.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/slink/IOnDeviceUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;,
        Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final INDEX_STORAGE_ID:I = 0x0

.field private static final LOADERMANAGER_ID_SLINKCLIENT:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field public static final UPDATE_THROTTLE_TIME:I = 0x7d0

.field public static final sDeviceWhere:Ljava/lang/String; = "transport_type == ? AND (transport_type != ? AND network_mode != ? AND physical_type != ?)"

.field public static final sDeviceWhereArgs:[Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mEntry:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mOldEntry:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mSlinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    .line 57
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->sDeviceWhereArgs:[Ljava/lang/String;

    .line 64
    const-string/jumbo v0, "slink://slink"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContext:Landroid/content/Context;

    .line 46
    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mSlinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 49
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mOldEntry:Ljava/util/ArrayList;

    .line 67
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContext:Landroid/content/Context;

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentResolver:Landroid/content/ContentResolver;

    .line 69
    new-instance v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;Lcom/sec/android/gallery3d/remote/slink/SLinkClient$1;)V

    new-array v2, v4, [Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$SlinkBackgroundJob;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 70
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$1;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V

    const-string v2, "SLinkClient"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 77
    .local v0, "slink":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 78
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getDeviceName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 205
    :cond_0
    const-string v0, ""

    .line 207
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;->deviceName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getNetworkMode(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 212
    :cond_0
    const-string v0, ""

    .line 214
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;->networkMode:Ljava/lang/String;

    goto :goto_0
.end method

.method public getStorageCount()I
    .locals 4

    .prologue
    .line 137
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 138
    .local v0, "count":I
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "storage count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    return v0
.end method

.method public getStorageId(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 150
    :cond_0
    const/4 v0, -0x1

    .line 152
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;->id:I

    goto :goto_0
.end method

.method public getStorageIndex(I)I
    .locals 3
    .param p1, "storageId"    # I

    .prologue
    .line 156
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 157
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "positon":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 158
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;->id:I

    if-ne p1, v2, :cond_0

    .line 162
    .end local v0    # "positon":I
    :goto_1
    return v0

    .line 157
    .restart local v0    # "positon":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getStorageName(I)Ljava/lang/String;
    .locals 1
    .param p1, "subsetIndex"    # I

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 173
    :cond_0
    const-string v0, ""

    .line 175
    :goto_0
    return-object v0

    :cond_1
    if-ltz p1, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;->name:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public getStorageNames()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 195
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [Ljava/lang/String;

    .line 196
    .local v0, "buffer":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 197
    .local v3, "index":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;

    .line 198
    .local v1, "entry":Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .local v4, "index":I
    iget-object v5, v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;->name:Ljava/lang/String;

    aput-object v5, v0, v3

    move v3, v4

    .line 199
    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_0

    .line 200
    .end local v1    # "entry":Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;
    :cond_0
    return-object v0
.end method

.method public getStoragePath(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/slink/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStorageId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onStorageUpdated(I)V
    .locals 3
    .param p1, "storageId"    # I

    .prologue
    .line 186
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStorageUpdated(), uri = [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentResolver:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "slink://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 188
    return-void
.end method

.method public onStoragesUpdated()V
    .locals 3

    .prologue
    .line 180
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStorageUpdated(), uri = [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 182
    return-void
.end method

.method public queryDevices()I
    .locals 14

    .prologue
    const/4 v12, 0x0

    .line 220
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    const-string v1, "query devices()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :try_start_0
    iget-object v13, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    monitor-enter v13
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mOldEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 224
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mOldEntry:Ljava/util/ArrayList;

    .line 225
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 226
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string/jumbo v3, "transport_type == ? AND (transport_type != ? AND network_mode != ? AND physical_type != ?)"

    sget-object v4, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->sDeviceWhereArgs:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 229
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 230
    :try_start_2
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    const-string v1, "queryDevices : cursor is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 244
    :try_start_3
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v12

    .line 251
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 233
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    :try_start_4
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queryDevices : cursor.getCount() = [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 236
    .local v9, "id":I
    invoke-static {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    .line 237
    .local v10, "name":Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    .line 238
    .local v7, "deviceName":Ljava/lang/String;
    const-string v0, "network_mode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 240
    .local v11, "networkMode":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queryDevices : available device name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "network mode is"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;

    invoke-direct {v1, v9, v10, v7, v11}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$StorageEntry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 244
    .end local v7    # "deviceName":Ljava/lang/String;
    .end local v9    # "id":I
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "networkMode":Ljava/lang/String;
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 246
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_0

    .line 247
    :catch_0
    move-exception v8

    .line 248
    .local v8, "e":Ljava/lang/SecurityException;
    sput-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    .line 249
    invoke-virtual {v8}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 251
    .end local v8    # "e":Ljava/lang/SecurityException;
    :goto_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mEntry:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/16 :goto_0

    .line 244
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    :try_start_7
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 246
    monitor-exit v13
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2
.end method

.method public setLoaderManager(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->TAG:Ljava/lang/String;

    const-string v1, "set loader manager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mActivity:Landroid/app/Activity;

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mSlinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mSlinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 88
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$2;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mContentObserver:Landroid/database/ContentObserver;

    .line 102
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->mSlinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isActivateSignedStatus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-virtual {p1}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient$3;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 134
    :cond_1
    return-void
.end method
