.class Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;
.super Ljava/lang/Thread;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/CropView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetectFaceTask"
.end annotation


# instance fields
.field private mCallerRectF:Landroid/graphics/RectF;

.field private final mFaceBitmap:Landroid/graphics/Bitmap;

.field private mFaceCount:I

.field private final mFaces:[Landroid/media/FaceDetector$Face;

.field private mPreDetectedFaces:[Lcom/sec/android/gallery3d/data/Face;

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/CropView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/CropView;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 755
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 749
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/media/FaceDetector$Face;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    .line 756
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    .line 757
    const-string v0, "face-detect"

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->setName(Ljava/lang/String;)V

    .line 758
    return-void
.end method

.method private getFaceRect(Landroid/media/FaceDetector$Face;)Landroid/graphics/RectF;
    .locals 13
    .param p1, "face"    # Landroid/media/FaceDetector$Face;

    .prologue
    .line 781
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsCallerId:Z
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/CropView;->access$2800(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 782
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mCallerRectF:Landroid/graphics/RectF;

    .line 818
    :goto_0
    return-object v4

    .line 784
    :cond_0
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 785
    .local v3, "point":Landroid/graphics/PointF;
    invoke-virtual {p1, v3}, Landroid/media/FaceDetector$Face;->getMidPoint(Landroid/graphics/PointF;)V

    .line 787
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 788
    .local v8, "width":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 789
    .local v2, "height":I
    invoke-virtual {p1}, Landroid/media/FaceDetector$Face;->eyesDistance()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    mul-float v5, v9, v10

    .line 790
    .local v5, "rx":F
    move v6, v5

    .line 791
    .local v6, "ry":F
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v0

    .line 792
    .local v0, "aspect":F
    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v9, v0, v9

    if-eqz v9, :cond_1

    .line 793
    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v9, v0, v9

    if-lez v9, :cond_3

    .line 794
    mul-float v5, v6, v0

    .line 800
    :cond_1
    :goto_1
    new-instance v4, Landroid/graphics/RectF;

    iget v9, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v5

    iget v10, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v6

    iget v11, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v11, v5

    iget v12, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v12, v6

    invoke-direct {v4, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 801
    .local v4, "r":Landroid/graphics/RectF;
    const/4 v9, 0x0

    const/4 v10, 0x0

    int-to-float v11, v8

    int-to-float v12, v2

    invoke-virtual {v4, v9, v10, v11, v12}, Landroid/graphics/RectF;->intersect(FFFF)Z

    .line 803
    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v9, v0, v9

    if-eqz v9, :cond_2

    .line 804
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v9

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v10

    div-float/2addr v9, v10

    cmpl-float v9, v9, v0

    if-lez v9, :cond_4

    .line 805
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v9

    mul-float v7, v9, v0

    .line 806
    .local v7, "w":F
    iget v9, v4, Landroid/graphics/RectF;->left:F

    iget v10, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v9, v10

    sub-float/2addr v9, v7

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->left:F

    .line 807
    iget v9, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v9, v7

    iput v9, v4, Landroid/graphics/RectF;->right:F

    .line 814
    .end local v7    # "w":F
    :cond_2
    :goto_2
    iget v9, v4, Landroid/graphics/RectF;->left:F

    int-to-float v10, v8

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->left:F

    .line 815
    iget v9, v4, Landroid/graphics/RectF;->right:F

    int-to-float v10, v8

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->right:F

    .line 816
    iget v9, v4, Landroid/graphics/RectF;->top:F

    int-to-float v10, v2

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->top:F

    .line 817
    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v10, v2

    div-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 796
    .end local v4    # "r":Landroid/graphics/RectF;
    :cond_3
    div-float v6, v5, v0

    goto :goto_1

    .line 809
    .restart local v4    # "r":Landroid/graphics/RectF;
    :cond_4
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v9

    div-float v1, v9, v0

    .line 810
    .local v1, "h":F
    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v10, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v9, v10

    sub-float/2addr v9, v1

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    iput v9, v4, Landroid/graphics/RectF;->top:F

    .line 811
    iget v9, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v9, v1

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    goto :goto_2
.end method

.method private getFaceRect(Lcom/sec/android/gallery3d/data/Face;)Landroid/graphics/RectF;
    .locals 8
    .param p1, "face"    # Lcom/sec/android/gallery3d/data/Face;

    .prologue
    .line 862
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Face;->getFaceRect()Landroid/graphics/Rect;

    move-result-object v2

    .line 863
    .local v2, "r":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v3

    int-to-float v1, v3

    .line 864
    .local v1, "imageW":F
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v3

    int-to-float v0, v3

    .line 865
    .local v0, "imageH":F
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 866
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v3

    const/16 v4, 0x5a

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v3

    const/16 v4, 0x10e

    if-ne v3, v4, :cond_2

    .line 867
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v3

    int-to-float v1, v3

    .line 868
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v3

    int-to-float v0, v3

    .line 874
    :cond_1
    :goto_0
    new-instance v3, Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    div-float/2addr v4, v1

    iget v5, v2, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    div-float/2addr v5, v0

    iget v6, v2, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    div-float/2addr v6, v1

    iget v7, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    div-float/2addr v7, v0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v3

    .line 870
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v3

    int-to-float v1, v3

    .line 871
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v3

    int-to-float v0, v3

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 762
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceBitmap:Landroid/graphics/Bitmap;

    .line 763
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsCallerId:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2800(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 764
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 765
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->loadFace()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mPreDetectedFaces:[Lcom/sec/android/gallery3d/data/Face;

    .line 766
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mPreDetectedFaces:[Lcom/sec/android/gallery3d/data/Face;

    if-nez v3, :cond_1

    const/4 v3, 0x0

    :goto_0
    iput v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    .line 777
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$3000(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$3000(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 778
    return-void

    .line 766
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mPreDetectedFaces:[Lcom/sec/android/gallery3d/data/Face;

    array-length v3, v3

    goto :goto_0

    .line 769
    :cond_2
    :try_start_0
    new-instance v1, Landroid/media/FaceDetector;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x3

    invoke-direct {v1, v3, v4, v5}, Landroid/media/FaceDetector;-><init>(III)V

    .line 770
    .local v1, "detector":Landroid/media/FaceDetector;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    invoke-virtual {v1, v0, v3}, Landroid/media/FaceDetector;->findFaces(Landroid/graphics/Bitmap;[Landroid/media/FaceDetector$Face;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 771
    .end local v1    # "detector":Landroid/media/FaceDetector;
    :catch_0
    move-exception v2

    .line 772
    .local v2, "ome":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 773
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_1
.end method

.method public setCallerRect(Landroid/graphics/RectF;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x1

    .line 854
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsCallerId:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2802(Lcom/sec/android/gallery3d/ui/CropView;Z)Z

    .line 856
    iput v1, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    .line 857
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mCallerRectF:Landroid/graphics/RectF;

    .line 859
    :cond_0
    return-void
.end method

.method public updateFaces()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 822
    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    if-le v3, v4, :cond_3

    .line 823
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCropMenuHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$500(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 824
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCropMenuHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$500(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x6c

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 826
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 827
    const/4 v0, 0x0

    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 828
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$3100(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mPreDetectedFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->getFaceRect(Lcom/sec/android/gallery3d/data/Face;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->addFace(Landroid/graphics/RectF;)V

    .line 827
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 831
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    .restart local v1    # "n":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 832
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$3100(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->getFaceRect(Landroid/media/FaceDetector$Face;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->addFace(Landroid/graphics/RectF;)V

    .line 831
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 835
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$3100(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->setVisibility(I)V

    .line 836
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0036

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 851
    .end local v0    # "i":I
    .end local v1    # "n":I
    :goto_2
    return-void

    .line 837
    :cond_3
    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaceCount:I

    if-ne v3, v4, :cond_6

    .line 838
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceDetectionView:Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$3100(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/CropView$FaceHighlightView;->setVisibility(I)V

    .line 839
    const/4 v2, 0x0

    .line 840
    .local v2, "rectf":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsCallerId:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2800(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 841
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mCallerRectF:Landroid/graphics/RectF;

    .line 845
    :goto_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setFaceRectangle(Landroid/graphics/RectF;)V

    .line 846
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setVisibility(I)V

    goto :goto_2

    .line 843
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mFaceItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$2900(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mPreDetectedFaces:[Lcom/sec/android/gallery3d/data/Face;

    aget-object v3, v3, v5

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->getFaceRect(Lcom/sec/android/gallery3d/data/Face;)Landroid/graphics/RectF;

    move-result-object v2

    :goto_4
    goto :goto_3

    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->mFaces:[Landroid/media/FaceDetector$Face;

    aget-object v3, v3, v5

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->getFaceRect(Landroid/media/FaceDetector$Face;)Landroid/graphics/RectF;

    move-result-object v2

    goto :goto_4

    .line 848
    .end local v2    # "rectf":Landroid/graphics/RectF;
    :cond_6
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v3

    const/4 v4, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setInitRectangle(Landroid/graphics/RectF;)V
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$3200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;Landroid/graphics/RectF;)V

    .line 849
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$DetectFaceTask;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setVisibility(I)V

    goto :goto_2
.end method
