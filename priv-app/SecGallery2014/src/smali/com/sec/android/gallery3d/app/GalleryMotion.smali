.class public Lcom/sec/android/gallery3d/app/GalleryMotion;
.super Ljava/lang/Object;
.source "GalleryMotion.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/GalleryMotion$13;,
        Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    }
.end annotation


# static fields
.field private static final ANIMATION_DELAY:I = 0x1f4

.field private static final CHANGE_ANIMATION:I = 0xc8

.field private static final LAST_ANIMATION_DELAY:I = 0x7d0

.field private static final LAST_ANIMATION_INDEX:I = 0x0

.field public static final MASTER_MOTION:Ljava/lang/String; = "master_motion"

.field private static final MAX_ANIMATION_INDEX:I = 0x3

.field public static final MOTION_ENGINE:Ljava/lang/String; = "motion_engine"

.field public static final MOTION_PANNING:Ljava/lang/String; = "motion_pan_to_browse_image"

.field public static final MOTION_PANNING_PEEK:Ljava/lang/String; = "motion_peek_view_albums_list"

.field public static final MOTION_PANNING_SENSITIVITY:Ljava/lang/String; = "motion_pan_to_browse_image_sensitivity"

.field public static final MOTION_RECOGNITION_SERVICE:Ljava/lang/String; = "motion_recognition"

.field public static final MOTION_ZOOMING:Ljava/lang/String; = "motion_zooming"

.field private static final SCALE_LIMIT_TUTORIAL:F = 6.0f

.field private static final TAG:Ljava/lang/String;

.field private static final mAnimationPanningImage:[I

.field private static final mAnimationPeek:[I

.field private static final mAnimationTiltImage:[I


# instance fields
.field private TEXT_COLOR:I

.field private mAnimationHandler:Landroid/os/Handler;

.field private mAnimationIndex:I

.field private mAnimationView:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mMotionDialog:Landroid/app/Dialog;

.field private mMotionDialogOff:Z

.field private mOldSensitivity:I

.field private mTutorialView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion;->TAG:Ljava/lang/String;

    .line 78
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationTiltImage:[I

    .line 83
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationPanningImage:[I

    .line 90
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationPeek:[I

    return-void

    .line 78
    :array_0
    .array-data 4
        0x7f0203df
        0x7f0203e0
        0x7f0203e1
    .end array-data

    .line 83
    :array_1
    .array-data 4
        0x7f0203d9
        0x7f0203da
        0x7f0203db
        0x7f0203d9
        0x7f0203dc
        0x7f0203d9
        0x7f0203dd
    .end array-data

    .line 90
    :array_2
    .array-data 4
        0x7f0203e3
        0x7f0203e4
        0x7f0203e5
        0x7f0203e4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialogOff:Z

    .line 63
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    .line 74
    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mOldSensitivity:I

    .line 75
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mTutorialView:Landroid/view/View;

    .line 76
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    const/high16 v0, -0x1000000

    :goto_0
    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->TEXT_COLOR:I

    .line 122
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    .line 123
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->createMotionAnimationHander()V

    .line 124
    return-void

    .line 76
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/GalleryMotion;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMotion;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->updateAnimation()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/GalleryMotion;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMotion;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->stopAnimation()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/GalleryMotion;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMotion;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/app/GalleryMotion;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMotion;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/GalleryMotion;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMotion;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->startAnimation()V

    return-void
.end method

.method private createMotionAnimationHander()V
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion$1;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationHandler:Landroid/os/Handler;

    .line 283
    return-void
.end method

.method private startAnimation()V
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 471
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    .line 472
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->updateAnimation()V

    .line 474
    :cond_0
    return-void
.end method

.method private stopAnimation()V
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationHandler:Landroid/os/Handler;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 480
    :cond_0
    return-void
.end method

.method private updateAnimation()V
    .locals 6

    .prologue
    const/16 v5, 0xc8

    const/4 v4, 0x0

    .line 436
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CHANGE_ANIMATION : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 438
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 439
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v1, :cond_3

    .line 440
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationPanningImage:[I

    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 446
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    .line 449
    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v1, :cond_5

    .line 450
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationPanningImage:[I

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 451
    iput v4, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    .line 459
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationHandler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    .line 460
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->stopAnimation()V

    .line 461
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    if-nez v1, :cond_7

    .line 462
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 467
    :cond_2
    :goto_2
    return-void

    .line 441
    :cond_3
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v1, :cond_4

    .line 442
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationPeek:[I

    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 444
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationTiltImage:[I

    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 452
    :cond_5
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v1, :cond_6

    .line 453
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationPeek:[I

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 454
    iput v4, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    goto :goto_1

    .line 456
    :cond_6
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    rem-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationIndex:I

    goto :goto_1

    .line 464
    :cond_7
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2
.end method


# virtual methods
.method public getMotionSensitivity(Landroid/content/Context;)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x5

    .line 604
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 605
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v2, v3, :cond_1

    .line 606
    const-string v2, "motion_zooming_sensitivity"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 610
    :cond_0
    :goto_0
    return v1

    .line 607
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v2, v3, :cond_0

    .line 608
    const-string v2, "motion_pan_to_browse_image_sensitivity"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    .locals 5

    .prologue
    .line 567
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "MotionTest"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 572
    .local v1, "motionTest":I
    packed-switch v1, :pswitch_data_0

    .line 598
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .end local v1    # "motionTest":I
    :goto_0
    return-object v2

    .line 568
    :catch_0
    move-exception v0

    .line 569
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 574
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "motionTest":I
    :pswitch_0
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 576
    :pswitch_1
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 578
    :pswitch_2
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 580
    :pswitch_3
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 582
    :pswitch_4
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 584
    :pswitch_5
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 586
    :pswitch_6
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 588
    :pswitch_7
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 590
    :pswitch_8
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 592
    :pswitch_9
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 594
    :pswitch_a
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 596
    :pswitch_b
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    goto :goto_0

    .line 572
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public initImageScale(Lcom/sec/android/gallery3d/ui/PositionController;II)V
    .locals 6
    .param p1, "positionController"    # Lcom/sec/android/gallery3d/ui/PositionController;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/high16 v4, 0x40c00000    # 6.0f

    const/high16 v5, 0x40000000    # 2.0f

    .line 623
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 624
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v3, :cond_4

    .line 627
    :cond_0
    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->setScaleLimit(F)V

    .line 628
    const/high16 v2, 0x40000000    # 2.0f

    .line 629
    .local v2, "scaleValue":F
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_1

    .line 630
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 631
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0c0047

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v2, v3

    .line 633
    .end local v1    # "res":Landroid/content/res/Resources;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isOverHD(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 634
    const/high16 v2, 0x3f800000    # 1.0f

    .line 636
    :cond_2
    int-to-float v3, p2

    div-float/2addr v3, v5

    int-to-float v4, p3

    div-float/2addr v4, v5

    invoke-virtual {p1, v3, v4, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    .line 647
    .end local v2    # "scaleValue":F
    :cond_3
    :goto_0
    return-void

    .line 637
    :cond_4
    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v3, :cond_6

    .line 640
    :cond_5
    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->setScaleLimit(F)V

    .line 641
    int-to-float v3, p2

    div-float/2addr v3, v5

    int-to-float v4, p3

    div-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {p1, v3, v4, v5}, Lcom/sec/android/gallery3d/ui/PositionController;->zoomIn(FFF)V

    goto :goto_0

    .line 642
    :cond_6
    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v3, :cond_7

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v3, :cond_7

    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v3, :cond_3

    .line 645
    :cond_7
    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->setScaleLimit(F)V

    goto :goto_0
.end method

.method public isMotionDialogOff()Z
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v1, "MotionDialogOff"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialogOff:Z

    .line 234
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialogOff:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionDialogOff(Z)V

    .line 235
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialogOff:Z

    return v0
.end method

.method public isMotionEngineUse()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 248
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "master_motion"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 250
    .local v0, "isMotionEngineUse":Z
    :goto_0
    return v0

    .end local v0    # "isMotionEngineUse":Z
    :cond_0
    move v0, v1

    .line 248
    goto :goto_0
.end method

.method public isMotionPanningUse()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 260
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_pan_to_browse_image"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 262
    .local v0, "isMotionPanningUse":Z
    :goto_0
    return v0

    .end local v0    # "isMotionPanningUse":Z
    :cond_0
    move v0, v1

    .line 260
    goto :goto_0
.end method

.method public isMotionPeekUse()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 266
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_peek_view_albums_list"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 267
    .local v0, "isMotionPeekUse":Z
    :goto_0
    return v0

    .end local v0    # "isMotionPeekUse":Z
    :cond_0
    move v0, v1

    .line 266
    goto :goto_0
.end method

.method public isMotionTiltUse()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 254
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_zooming"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 256
    .local v0, "isMotionTiltUse":Z
    :goto_0
    return v0

    .end local v0    # "isMotionTiltUse":Z
    :cond_0
    move v0, v1

    .line 254
    goto :goto_0
.end method

.method public isMotionUse()Z
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionEngineUse()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionTiltUse()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionPanningUse()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public motionDialogInitialOff(Lcom/sec/android/gallery3d/util/MotionDetector;II)V
    .locals 13
    .param p1, "motionDetector"    # Lcom/sec/android/gallery3d/util/MotionDetector;
    .param p2, "motionDefaultWidth"    # I
    .param p3, "motionDefaultHeight"    # I

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    :goto_0
    return-void

    .line 351
    :cond_0
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 352
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/LayoutInflater;

    .line 354
    .local v8, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300a0

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 355
    .local v9, "layout":Landroid/view/View;
    const v0, 0x7f0f01b8

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    .line 356
    const v0, 0x7f0f0050

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 357
    .local v10, "message1":Landroid/widget/TextView;
    const v0, 0x7f0f01b9

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 360
    .local v11, "message2":Landroid/widget/TextView;
    const v0, 0x7f0f001d

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 362
    .local v2, "checkBox":Landroid/widget/CheckBox;
    const v0, 0x7f0e00d5

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(I)V

    .line 363
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->TEXT_COLOR:I

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 364
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVZW_MotionDialogText:Z

    if-eqz v0, :cond_1

    .line 365
    const v0, 0x7f0e00d6

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(I)V

    .line 369
    :goto_1
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->TEXT_COLOR:I

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 370
    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 371
    const v0, 0x7f0e00d1

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 372
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 374
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 375
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 376
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->TEXT_COLOR:I

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 377
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$6;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion$6;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 384
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$7;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion$7;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 392
    const/4 v7, 0x1

    .line 394
    .local v7, "calibrationOff":Z
    const v12, 0x7f0e00d0

    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move/from16 v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/app/GalleryMotion$8;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;Landroid/widget/CheckBox;Lcom/sec/android/gallery3d/util/MotionDetector;II)V

    invoke-virtual {v6, v12, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 411
    const v0, 0x7f0e0046

    new-instance v1, Lcom/sec/android/gallery3d/app/GalleryMotion$9;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion$9;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 419
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/gallery3d/app/GalleryMotion$10;

    invoke-direct {v1, p0, v6, v2}, Lcom/sec/android/gallery3d/app/GalleryMotion$10;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;Landroid/app/AlertDialog$Builder;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 367
    .end local v7    # "calibrationOff":Z
    :cond_1
    const v0, 0x7f0e00d7

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public motionDialogInitialOn()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 286
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 343
    :goto_0
    return-void

    .line 289
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 290
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 292
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f0300a1

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 293
    .local v3, "layout":Landroid/view/View;
    const v5, 0x7f0f01b8

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    .line 294
    const v5, 0x7f0f0050

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 295
    .local v4, "message1":Landroid/widget/TextView;
    const v5, 0x7f0f001d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 297
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const v5, 0x7f0e00d5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 298
    iget v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->TEXT_COLOR:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 300
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 301
    const v5, 0x7f0e00d1

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 302
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 304
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 305
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 306
    iget v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->TEXT_COLOR:I

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 307
    new-instance v5, Lcom/sec/android/gallery3d/app/GalleryMotion$2;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion$2;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    new-instance v5, Lcom/sec/android/gallery3d/app/GalleryMotion$3;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion$3;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 320
    const v5, 0x7f0e0070

    new-instance v6, Lcom/sec/android/gallery3d/app/GalleryMotion$4;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/gallery3d/app/GalleryMotion$4;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 330
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    new-instance v6, Lcom/sec/android/gallery3d/app/GalleryMotion$5;

    invoke-direct {v6, p0, v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMotion$5;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;Landroid/app/AlertDialog$Builder;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 651
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionSensitivity(Landroid/content/Context;I)V

    .line 652
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 658
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 664
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->stopAnimation()V

    .line 510
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->startAnimation()V

    .line 516
    :cond_0
    return-void
.end method

.method public setAirMotionTutorialView()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, -0x1

    .line 194
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 199
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0300a7

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 200
    .local v3, "tutorialView":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v3, v5}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 204
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const v5, 0x7f0f01d9

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 205
    .local v1, "leftBtn":Landroid/widget/Button;
    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 207
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const v5, 0x7f0f01da

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 208
    .local v2, "rightBtn":Landroid/widget/Button;
    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 209
    return-void
.end method

.method public setMotionActionBarTitle(Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;)V
    .locals 2
    .param p1, "motionTutorial"    # Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .prologue
    .line 212
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMotion$13;->$SwitchMap$com$sec$android$gallery3d$app$GalleryMotion$MotionType:[I

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    :goto_0
    return-void

    .line 216
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e00de

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 220
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e00df

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 224
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e00e0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public setMotionDialogOff(Z)V
    .locals 3
    .param p1, "motionDialogOff"    # Z

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialogOff:Z

    .line 240
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v1, "MotionDialogOff"

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialogOff:Z

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 241
    return-void
.end method

.method public setMotionEngineEnable(Z)V
    .locals 6
    .param p1, "calibrationOff"    # Z

    .prologue
    const/4 v5, 0x1

    .line 483
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "master_motion"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 484
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "motion_engine"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 485
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "motion_zooming"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 488
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.motions.MOTIONS_SETTINGS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 489
    .local v2, "settingIntent":Landroid/content/Intent;
    const-string v3, "isEnable"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 490
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 492
    if-nez p1, :cond_0

    .line 493
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 494
    .local v0, "calibrationIntent":Landroid/content/Intent;
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.GSensorSettings"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 497
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 502
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v4, "MotionCalibrationOff"

    invoke-static {v3, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 504
    .end local v0    # "calibrationIntent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 498
    .restart local v0    # "calibrationIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 499
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    sget-object v3, Lcom/sec/android/gallery3d/app/GalleryMotion;->TAG:Ljava/lang/String;

    const-string v4, "Activity Not Found"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public setMotionHelpView(Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;)V
    .locals 9
    .param p1, "motionTutorial"    # Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 166
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 169
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const/4 v0, 0x0

    .line 171
    .local v0, "helpView":Landroid/view/View;
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$13;->$SwitchMap$com$sec$android$gallery3d$app$GalleryMotion$MotionType:[I

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 191
    :goto_0
    return-void

    .line 174
    :pswitch_0
    const v5, 0x7f0300a4

    invoke-virtual {v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 175
    .local v3, "helpViewTilt":Landroid/view/View;
    move-object v0, v3

    .line 189
    .end local v3    # "helpViewTilt":Landroid/view/View;
    :goto_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v0, v6}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 178
    :pswitch_1
    const v5, 0x7f0300a2

    invoke-virtual {v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 179
    .local v1, "helpViewPanning":Landroid/view/View;
    move-object v0, v1

    .line 180
    goto :goto_1

    .line 182
    .end local v1    # "helpViewPanning":Landroid/view/View;
    :pswitch_2
    const v5, 0x7f0300a3

    invoke-virtual {v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 183
    .local v2, "helpViewPeek":Landroid/view/View;
    move-object v0, v2

    .line 184
    goto :goto_1

    .line 171
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setMotionSensitivity(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensitivity"    # I

    .prologue
    .line 614
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 616
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v1, v2, :cond_1

    .line 617
    const-string v1, "motion_zooming_sensitivity"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v1, v2, :cond_0

    .line 619
    const-string v1, "motion_pan_to_browse_image_sensitivity"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public setMotionTutorialView()V
    .locals 12

    .prologue
    const/4 v11, -0x1

    .line 127
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 129
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v5, 0x0

    .line 130
    .local v5, "seekBar":Landroid/widget/SeekBar;
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mTutorialView:Landroid/view/View;

    if-eqz v8, :cond_0

    .line 131
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mTutorialView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/ViewManager;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mTutorialView:Landroid/view/View;

    invoke-interface {v8, v9}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    .line 133
    :cond_0
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v8, :cond_3

    const v8, 0x7f0300a6

    :goto_0
    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mTutorialView:Landroid/view/View;

    .line 135
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 136
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mTutorialView:Landroid/view/View;

    const v9, 0x7f0f01d3

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 137
    .local v2, "ll":Landroid/widget/LinearLayout;
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 138
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 139
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    const v8, 0x7f0d01e4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 140
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    .end local v2    # "ll":Landroid/widget/LinearLayout;
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "res":Landroid/content/res/Resources;
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mTutorialView:Landroid/view/View;

    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v10, v11, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v9, v10}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    const v9, 0x7f0f01d6

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "seekBar":Landroid/widget/SeekBar;
    check-cast v5, Landroid/widget/SeekBar;

    .line 148
    .restart local v5    # "seekBar":Landroid/widget/SeekBar;
    const/16 v8, 0xa

    invoke-virtual {v5, v8}, Landroid/widget/SeekBar;->setMax(I)V

    .line 149
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getMotionSensitivity(Landroid/content/Context;)I

    move-result v8

    iput v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mOldSensitivity:I

    .line 150
    iget v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mOldSensitivity:I

    invoke-virtual {v5, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 151
    invoke-virtual {v5, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 153
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    const v9, 0x7f0f01d4

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 154
    .local v6, "sensitivity_text":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    const v9, 0x7f0f01d5

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 155
    .local v7, "slow_text":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    const v9, 0x7f0f01d7

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 157
    .local v0, "fast_text":Landroid/widget/TextView;
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-nez v8, :cond_2

    .line 158
    const-string v8, "#FFFFFF"

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 159
    const-string v8, "#FFFFFF"

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 160
    const-string v8, "#FFFFFF"

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 162
    :cond_2
    return-void

    .line 133
    .end local v0    # "fast_text":Landroid/widget/TextView;
    .end local v6    # "sensitivity_text":Landroid/widget/TextView;
    .end local v7    # "slow_text":Landroid/widget/TextView;
    :cond_3
    const v8, 0x7f0300a5

    goto/16 :goto_0
.end method

.method public showMotionTutorialInfoDialog()V
    .locals 7

    .prologue
    .line 519
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mMotionDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 562
    :goto_0
    return-void

    .line 522
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 523
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 525
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f0300a8

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 526
    .local v2, "layout":Landroid/view/View;
    const v5, 0x7f0f01b8

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mAnimationView:Landroid/widget/ImageView;

    .line 527
    const v5, 0x7f0f0050

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 528
    .local v3, "message1":Landroid/widget/TextView;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 530
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v4

    .line 532
    .local v4, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v4, v5, :cond_1

    .line 533
    const v5, 0x7f0e00d8

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 534
    const v5, 0x7f0e00d2

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 543
    :goto_1
    const v5, 0x7f0e00db

    new-instance v6, Lcom/sec/android/gallery3d/app/GalleryMotion$11;

    invoke-direct {v6, p0}, Lcom/sec/android/gallery3d/app/GalleryMotion$11;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 550
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    new-instance v6, Lcom/sec/android/gallery3d/app/GalleryMotion$12;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/gallery3d/app/GalleryMotion$12;-><init>(Lcom/sec/android/gallery3d/app/GalleryMotion;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 535
    :cond_1
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v4, v5, :cond_2

    .line 536
    const v5, 0x7f0e00d9

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 537
    const v5, 0x7f0e00d3

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 539
    :cond_2
    const v5, 0x7f0e00d5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 540
    const v5, 0x7f0e00d1

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
