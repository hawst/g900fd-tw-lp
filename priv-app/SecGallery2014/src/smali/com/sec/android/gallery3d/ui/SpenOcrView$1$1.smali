.class Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;
.super Ljava/lang/Object;
.source "SpenOcrView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/gallery3d/ui/SpenOcrView$1;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/SpenOcrView$1;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;->this$1:Lcom/sec/android/gallery3d/ui/SpenOcrView$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;->this$1:Lcom/sec/android/gallery3d/ui/SpenOcrView$1;

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mRecognizer:Lcom/dmc/ocr/OcrRecognizer;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;->this$1:Lcom/sec/android/gallery3d/ui/SpenOcrView$1;

    iget-object v1, v1, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$000(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;->this$1:Lcom/sec/android/gallery3d/ui/SpenOcrView$1;

    iget-object v2, v2, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    # getter for: Lcom/sec/android/gallery3d/ui/SpenOcrView;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SpenOcrView;->access$100(Lcom/sec/android/gallery3d/ui/SpenOcrView;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SpenOcrView$1$1;->this$1:Lcom/sec/android/gallery3d/ui/SpenOcrView$1;

    iget-object v3, v3, Lcom/sec/android/gallery3d/ui/SpenOcrView$1;->this$0:Lcom/sec/android/gallery3d/ui/SpenOcrView;

    iget-object v3, v3, Lcom/sec/android/gallery3d/ui/SpenOcrView;->mOCRRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v3}, Lcom/dmc/ocr/OcrRecognizer;->processTextRect(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V

    .line 84
    return-void
.end method
