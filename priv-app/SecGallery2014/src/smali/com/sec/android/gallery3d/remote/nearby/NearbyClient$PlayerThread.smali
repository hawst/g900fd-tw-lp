.class Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;
.super Ljava/lang/Thread;
.source "NearbyClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayerThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V
    .locals 0

    .prologue
    .line 709
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;

    .prologue
    .line 709
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 712
    const-class v3, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->setName(Ljava/lang/String;)V

    .line 713
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 716
    :cond_0
    const/4 v2, 0x0

    .line 717
    .local v2, "playInfo":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 718
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "playInfo":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;
    check-cast v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;

    .restart local v2    # "playInfo":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;
    goto :goto_0

    .line 720
    :cond_1
    if-nez v2, :cond_2

    .line 721
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "play info is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    :goto_1
    return-void

    .line 726
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    iget-object v4, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v5, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->prepareItem(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/samsung/android/allshare/Item;
    invoke-static {v3, v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1700(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/samsung/android/allshare/Item;

    move-result-object v1

    .line 727
    .local v1, "itemToShow":Lcom/samsung/android/allshare/Item;
    if-nez v1, :cond_3

    .line 728
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "playing image to player has been cancelled, item to show is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 733
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 734
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " item inserted while preparing item"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 768
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->quit()V

    goto :goto_1

    .line 739
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/ImageViewer;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/ImageViewer;

    move-result-object v3

    iget-object v4, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->viewer:Lcom/samsung/android/allshare/media/ImageViewer;

    if-eq v3, v4, :cond_6

    .line 740
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "stop previous viewer. is changed"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/ImageViewer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/ImageViewer;->stop()V

    .line 745
    :cond_6
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "show item to player : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->viewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    iget-object v4, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->viewer:Lcom/samsung/android/allshare/media/ImageViewer;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1002(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/ImageViewer;)Lcom/samsung/android/allshare/media/ImageViewer;

    .line 747
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z
    invoke-static {v3, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1102(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 748
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mIsViewerShowRequested:Z
    invoke-static {v3, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1202(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z

    .line 751
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/ImageViewer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/allshare/media/ImageViewer;->show(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    .line 754
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    iget-object v4, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->mediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v5, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;->item:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->updateDisplayItemInfo(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v3, v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$2000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 758
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 759
    :try_start_1
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "wait next item"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    const-wide/32 v4, 0xea60

    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 761
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 762
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 763
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "kill thread queue is empty"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 764
    :catch_0
    move-exception v0

    .line 765
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "interrupted"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 761
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
.end method
