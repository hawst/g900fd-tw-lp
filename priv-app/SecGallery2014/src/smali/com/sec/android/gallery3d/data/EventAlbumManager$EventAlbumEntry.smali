.class public Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;
.super Ljava/lang/Object;
.source "EventAlbumManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/EventAlbumManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventAlbumEntry"
.end annotation


# instance fields
.field public albumCreateTime:J

.field public bucketId:I

.field public name:Ljava/lang/String;


# direct methods
.method constructor <init>(IJLjava/lang/String;)V
    .locals 0
    .param p1, "bucketId"    # I
    .param p2, "albumCreateTime"    # J
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 1030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1031
    iput p1, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;->bucketId:I

    .line 1032
    iput-wide p2, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;->albumCreateTime:J

    .line 1033
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/EventAlbumManager$EventAlbumEntry;->name:Ljava/lang/String;

    .line 1034
    return-void
.end method
