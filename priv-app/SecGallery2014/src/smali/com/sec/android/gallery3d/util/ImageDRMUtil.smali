.class public Lcom/sec/android/gallery3d/util/ImageDRMUtil;
.super Ljava/lang/Object;
.source "ImageDRMUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;,
        Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMConsume;,
        Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;
    }
.end annotation


# static fields
.field private static final ACTION_DISPLAY:I = 0x7

.field private static final ACTION_PLAY:I = 0x1

.field private static final LICENSE_CATEGORY:Ljava/lang/String; = "license_category"

.field private static final OMA_PLUGIN_MIME:Ljava/lang/String; = "application/vnd.oma.drm.content"

.field public static final TYPE_DRMTYPE_CD:I = 0x1

.field public static final TYPE_DRMTYPE_FL:I = 0x0

.field public static final TYPE_DRMTYPE_NONE:I = -0x1

.field public static final TYPE_DRMTYPE_SD:I = 0x3

.field public static final TYPE_DRMTYPE_SSD:I = 0x2

.field private static final TYPE_POPUP_COUNT_REMAIN_1:I = 0x3

.field private static final TYPE_POPUP_COUNT_REMAIN_N:I = 0x4

.field private static final TYPE_POPUP_INVALID:I = 0x1

.field private static final TYPE_POPUP_MODE_SINGLE:I = 0x6

.field private static final TYPE_POPUP_NONE:I = 0x0

.field private static final TYPE_POPUP_RENDER_FIRST:I = 0x5

.field private static final TYPE_POPUP_UNLOCK:I = 0x2

.field private static final TYPE_POPUP_UNSUPPORT_FILETYPE:I = 0x7


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDrmClient:Landroid/drm/DrmManagerClient;

.field private mListener:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, "ImageDRMUtil"

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    .line 75
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    .line 76
    new-instance v0, Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/util/ImageDRMUtil;)Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mListener:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;

    return-object v0
.end method


# virtual methods
.method public consume(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    new-instance v1, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMConsume;

    invoke-direct {v1, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMConsume;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 231
    return-void
.end method

.method public deInitialize()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/drm/DrmManagerClient;->release()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 85
    :cond_0
    return-void
.end method

.method public getAvailableUses(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 107
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v1, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "originalMime":Ljava/lang/String;
    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0101

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 111
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0100

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getBrokenImageRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 275
    const/4 v1, 0x0

    .line 278
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 279
    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/sec/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 281
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 283
    return-object v0

    .line 281
    .end local v0    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v2
.end method

.method public getDRMInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 688
    new-instance v0, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;-><init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;Ljava/lang/String;)V

    return-object v0
.end method

.method public getDetails(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 8
    .param p1, "details"    # Lcom/sec/android/gallery3d/data/MediaDetails;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x133

    const/16 v6, 0x12c

    .line 578
    const/16 v3, 0x12b

    .line 579
    .local v3, "start":I
    const/16 v1, 0x134

    .line 580
    .local v1, "end":I
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmType(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1

    .line 581
    const/4 v4, 0x0

    invoke-virtual {p1, v6, v4}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 582
    const/16 v4, 0x135

    iget-object v5, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v6, 0x7f0e00fb

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 605
    :cond_0
    return-object p1

    .line 587
    :cond_1
    :goto_0
    add-int/lit8 v3, v3, 0x1

    if-gt v3, v1, :cond_0

    .line 588
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmInfo(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 589
    .local v0, "buffer":Ljava/lang/String;
    if-ne v3, v6, :cond_2

    .line 590
    invoke-virtual {p1, v3, v0}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_0

    .line 592
    :cond_2
    if-eq v3, v7, :cond_1

    .line 594
    const/16 v4, 0x134

    if-ne v3, v4, :cond_3

    .line 595
    if-eqz v0, :cond_3

    .line 596
    invoke-virtual {p0, v7, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmInfo(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 598
    .local v2, "max":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 601
    .end local v2    # "max":Ljava/lang/String;
    :cond_3
    if-eqz v0, :cond_1

    .line 602
    invoke-virtual {p1, v3, v0}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public getDrmInfo(ILjava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "type"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 537
    const/4 v3, 0x0

    .line 538
    .local v3, "rightDetails":Landroid/content/ContentValues;
    iget-object v5, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, p2}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 540
    .local v1, "originalMime":Ljava/lang/String;
    const-string v5, "image/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 541
    iget-object v5, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v6, 0x7

    invoke-virtual {v5, p2, v6}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v3

    .line 545
    :goto_0
    if-nez v3, :cond_1

    .line 573
    :goto_1
    return-object v4

    .line 543
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v6, 0x1

    invoke-virtual {v5, p2, v6}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v3

    goto :goto_0

    .line 547
    :cond_1
    const/4 v2, 0x0

    .line 548
    .local v2, "request":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 550
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getAvailableUses(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 552
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getLicenseCategory(Ljava/lang/String;)I

    move-result v0

    .line 553
    .local v0, "category":I
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getLicenseCategoryString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 555
    .end local v0    # "category":I
    :pswitch_2
    const-string v2, "license_available_time"

    .line 556
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_1

    .line 558
    :pswitch_3
    const-string v2, "license_expiry_time"

    .line 559
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_1

    .line 561
    :pswitch_4
    const-string v2, "license_start_time"

    .line 562
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_1

    .line 564
    :pswitch_5
    const-string v2, "license_original_interval"

    .line 565
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_1

    .line 567
    :pswitch_6
    const-string v2, "remaining_repeat_count"

    .line 568
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_1

    .line 570
    :pswitch_7
    const-string v2, "max_repeat_count"

    .line 571
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_1

    .line 548
    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "drmFilename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 609
    if-eqz p1, :cond_0

    .line 610
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x5

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 611
    .local v0, "mimeType":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 612
    const-string v2, ".dcf"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 613
    const-string v1, "application/vnd.oma.drm.content"

    .line 635
    .end local v0    # "mimeType":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 614
    .restart local v0    # "mimeType":Ljava/lang/String;
    :cond_1
    const-string v2, ".avi"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 615
    const-string/jumbo v1, "video/mux/AVI"

    goto :goto_0

    .line 616
    :cond_2
    const-string v2, ".mkv"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 617
    const-string/jumbo v1, "video/mux/MKV"

    goto :goto_0

    .line 618
    :cond_3
    const-string v2, ".divx"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 619
    const-string/jumbo v1, "video/mux/DivX"

    goto :goto_0

    .line 620
    :cond_4
    const-string v2, ".pyv"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 621
    const-string/jumbo v1, "video/vnd.ms-playready.media.pyv"

    goto :goto_0

    .line 622
    :cond_5
    const-string v2, ".pya"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 623
    const-string v1, "audio/vnd.ms-playready.media.pya"

    goto :goto_0

    .line 624
    :cond_6
    const-string v2, ".wmv"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 625
    const-string/jumbo v1, "video/x-ms-wmv"

    goto :goto_0

    .line 626
    :cond_7
    const-string v2, ".wma"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 627
    const-string v1, "audio/x-ms-wma"

    goto :goto_0

    .line 628
    :cond_8
    const-string v2, ".isma"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 629
    const-string v1, "audio/isma"

    goto :goto_0

    .line 630
    :cond_9
    const-string v2, ".ismv"

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 631
    const-string/jumbo v1, "video/ismv"

    goto :goto_0
.end method

.method public getDrmType(Ljava/lang/String;)I
    .locals 7
    .param p1, "filpath"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 153
    const/4 v2, -0x1

    .line 174
    :goto_0
    return v2

    .line 156
    :cond_0
    new-instance v1, Landroid/drm/DrmInfoRequest;

    const/16 v4, 0xe

    const-string v5, "application/vnd.oma.drm.content"

    invoke-direct {v1, v4, v5}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 158
    .local v1, "drmrequest":Landroid/drm/DrmInfoRequest;
    const-string v4, "drm_path"

    invoke-virtual {v1, v4, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, v1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 162
    .local v0, "drminfo":Landroid/drm/DrmInfo;
    const/4 v2, -0x1

    .line 163
    .local v2, "drmtype":I
    if-eqz v0, :cond_2

    .line 164
    const-string/jumbo v4, "type"

    invoke-virtual {v0, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 165
    .local v3, "type":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 166
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 167
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DrmType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 169
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "DrmType is null!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 172
    .end local v3    # "type":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "DrmInfo is null!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getLicenseCategory(Ljava/lang/String;)I
    .locals 5
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 178
    const/4 v0, 0x0

    .line 179
    .local v0, "cv":Landroid/content/ContentValues;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 180
    .local v2, "originalMime":Ljava/lang/String;
    const-string v3, "image/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 181
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v4, 0x7

    invoke-virtual {v3, p1, v4}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 185
    :goto_0
    const/4 v1, 0x0

    .line 186
    .local v1, "licenseCategory":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 187
    const-string v3, "license_category"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "licenseCategory":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 189
    .restart local v1    # "licenseCategory":Ljava/lang/String;
    :cond_0
    if-nez v1, :cond_2

    .line 190
    const/4 v3, -0x1

    .line 192
    :goto_1
    return v3

    .line 183
    .end local v1    # "licenseCategory":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v4, 0x1

    invoke-virtual {v3, p1, v4}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    goto :goto_0

    .line 192
    .restart local v1    # "licenseCategory":Ljava/lang/String;
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto :goto_1
.end method

.method public getLicenseCategoryString(I)Ljava/lang/String;
    .locals 7
    .param p1, "licenseCategoryType"    # I

    .prologue
    const v6, 0x7f0e0103

    const v5, 0x7f0e0102

    .line 197
    sparse-switch p1, :sswitch_data_0

    .line 223
    const/4 v0, 0x0

    .line 226
    .local v0, "licenseCategory":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 199
    .end local v0    # "licenseCategory":Ljava/lang/String;
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0106

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 200
    .restart local v0    # "licenseCategory":Ljava/lang/String;
    goto :goto_0

    .line 202
    .end local v0    # "licenseCategory":Ljava/lang/String;
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    .restart local v0    # "licenseCategory":Ljava/lang/String;
    goto :goto_0

    .line 205
    .end local v0    # "licenseCategory":Ljava/lang/String;
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    .restart local v0    # "licenseCategory":Ljava/lang/String;
    goto :goto_0

    .line 208
    .end local v0    # "licenseCategory":Ljava/lang/String;
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0104

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    .restart local v0    # "licenseCategory":Ljava/lang/String;
    goto :goto_0

    .line 211
    .end local v0    # "licenseCategory":Ljava/lang/String;
    :sswitch_4
    const-string v1, "%s/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 214
    .restart local v0    # "licenseCategory":Ljava/lang/String;
    goto :goto_0

    .line 216
    .end local v0    # "licenseCategory":Ljava/lang/String;
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0105

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 217
    .restart local v0    # "licenseCategory":Ljava/lang/String;
    goto :goto_0

    .line 219
    .end local v0    # "licenseCategory":Ljava/lang/String;
    :sswitch_6
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0109

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 220
    .restart local v0    # "licenseCategory":Ljava/lang/String;
    goto :goto_0

    .line 197
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_6
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x8 -> :sswitch_4
        0x10 -> :sswitch_5
    .end sparse-switch
.end method

.method public getOriginalMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPopupString(I)Ljava/lang/String;
    .locals 7
    .param p1, "popupType"    # I

    .prologue
    const v6, 0x7f0e00f4

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 321
    packed-switch p1, :pswitch_data_0

    .line 350
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 323
    :pswitch_0
    const-string v0, "%s. %s"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00f3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 328
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f0e00f5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 331
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f0e00f1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 334
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f0e00f0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 337
    :pswitch_4
    const-string v0, "%s %s"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00f6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00f2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 342
    :pswitch_5
    const-string v0, "%s."

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 346
    :pswitch_6
    const-string v0, "%s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0e010a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 321
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getPopupType(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 14
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v7, 0x6

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 355
    iget-object v11, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v12, "getPopupString - start"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 358
    .local v0, "filePath":Ljava/lang/String;
    const-string v11, ".ismv"

    invoke-virtual {v0, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 440
    :cond_0
    :goto_0
    return v6

    .line 362
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDRMInfo()Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    move-result-object v1

    .line 363
    .local v1, "info":Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;
    if-nez v1, :cond_2

    .line 364
    iget-object v11, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v12, "DRM info is not exist - try again"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDRMInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;

    move-result-object v1

    .line 368
    :cond_2
    iget v5, v1, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mRightType:I

    .line 369
    .local v5, "rightType":I
    if-ne v5, v8, :cond_4

    .line 370
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v10

    const-wide/16 v12, 0x1

    and-long/2addr v10, v12

    const-wide/16 v12, 0x0

    cmp-long v6, v10, v12

    if-nez v6, :cond_3

    move v6, v7

    .line 372
    goto :goto_0

    :cond_3
    move v6, v8

    .line 374
    goto :goto_0

    .line 376
    :cond_4
    iget v11, v1, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mDrmType:I

    packed-switch v11, :pswitch_data_0

    .line 403
    :cond_5
    iget v11, v1, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mLicenseCategory:I

    packed-switch v11, :pswitch_data_1

    :pswitch_0
    goto :goto_0

    .line 405
    :pswitch_1
    const/16 v11, 0x134

    invoke-virtual {p0, v11, v0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmInfo(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 407
    .local v3, "remain":Ljava/lang/String;
    if-nez v3, :cond_a

    .line 408
    iget-object v7, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v8, "invalid remain count : null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    .end local v3    # "remain":Ljava/lang/String;
    :pswitch_2
    if-eq v5, v10, :cond_6

    if-ne v5, v9, :cond_5

    .line 381
    :cond_6
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v10

    const-wide/16 v12, 0x1

    and-long/2addr v10, v12

    const-wide/16 v12, 0x0

    cmp-long v6, v10, v12

    if-nez v6, :cond_7

    move v6, v7

    .line 383
    goto :goto_0

    :cond_7
    move v6, v8

    .line 385
    goto :goto_0

    .line 391
    :pswitch_3
    if-eq v5, v10, :cond_8

    if-ne v5, v9, :cond_5

    .line 393
    :cond_8
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFLDRMOnly:Z

    if-eqz v6, :cond_9

    .line 394
    const/4 v6, 0x7

    goto :goto_0

    :cond_9
    move v6, v9

    .line 396
    goto :goto_0

    .line 411
    .restart local v3    # "remain":Ljava/lang/String;
    :cond_a
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 412
    .local v4, "remainCount":I
    if-nez v4, :cond_c

    .line 413
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v10

    const-wide/16 v12, 0x1

    and-long/2addr v10, v12

    const-wide/16 v12, 0x0

    cmp-long v6, v10, v12

    if-nez v6, :cond_b

    move v6, v7

    .line 415
    goto :goto_0

    :cond_b
    move v6, v8

    .line 417
    goto :goto_0

    .line 418
    :cond_c
    if-ne v4, v8, :cond_d

    move v6, v10

    .line 419
    goto :goto_0

    .line 420
    :cond_d
    if-ne v4, v9, :cond_0

    .line 421
    const/4 v6, 0x4

    goto/16 :goto_0

    .line 427
    .end local v3    # "remain":Ljava/lang/String;
    .end local v4    # "remainCount":I
    :pswitch_4
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isFirstView(Ljava/lang/String;)Z

    move-result v2

    .line 428
    .local v2, "isfirst":Z
    if-eqz v2, :cond_0

    .line 429
    const/4 v6, 0x5

    goto/16 :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 403
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public getRightType(Ljava/lang/String;)I
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 135
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 146
    :goto_0
    return v0

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 145
    .local v0, "rightStatus":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v3, "DRM file() - RightStatus(%s)"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isDrm(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 89
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 91
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOMADrmSkip:Z

    if-eqz v4, :cond_2

    const-string v4, "application/vnd.oma.drm.content"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 92
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "originalMime":Ljava/lang/String;
    const-string v4, "image/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 103
    .end local v0    # "mimeType":Ljava/lang/String;
    .end local v1    # "originalMime":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 95
    .restart local v0    # "mimeType":Ljava/lang/String;
    .restart local v1    # "originalMime":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, p1, v0}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 96
    goto :goto_0

    .line 98
    .end local v1    # "originalMime":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, p1, v0}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 99
    goto :goto_0
.end method

.method public isFirstView(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 234
    const/16 v2, 0x131

    invoke-virtual {p0, v2, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmInfo(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "start":Ljava/lang/String;
    const/16 v2, 0x130

    invoke-virtual {p0, v2, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmInfo(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "expiry":Ljava/lang/String;
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 237
    const/4 v2, 0x1

    .line 239
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isValidRights(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v1

    .line 127
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 128
    .local v0, "rightStatus":I
    if-nez v0, :cond_0

    .line 129
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public requestRight(Ljava/lang/String;)V
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 243
    new-instance v2, Landroid/drm/DrmInfoRequest;

    const/4 v6, 0x3

    const-string v7, "application/vnd.oma.drm.content"

    invoke-direct {v2, v6, v7}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 245
    .local v2, "mDrmInfoRequest_ILA":Landroid/drm/DrmInfoRequest;
    const-string v6, "drm_path"

    invoke-virtual {v2, v6, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 246
    iget-object v6, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v6, v2}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v3

    .line 248
    .local v3, "mDrmInfo_ILA":Landroid/drm/DrmInfo;
    const/4 v4, 0x0

    .line 249
    .local v4, "mLicense_url":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 250
    const-string v6, "URL"

    invoke-virtual {v3, v6}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "mLicense_url":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 253
    .restart local v4    # "mLicense_url":Ljava/lang/String;
    :cond_0
    if-eqz v3, :cond_1

    if-nez v4, :cond_3

    .line 254
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    const v7, 0x7f0e0108

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 272
    :cond_2
    :goto_0
    return-void

    .line 257
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 258
    .local v0, "browserIntent":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 259
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 260
    .local v5, "mUri":Landroid/net/Uri;
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 262
    iget-object v6, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/high16 v7, 0x10000

    invoke-virtual {v6, v0, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 267
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 268
    :catch_0
    move-exception v1

    .line 269
    .local v1, "ex":Landroid/content/ActivityNotFoundException;
    iget-object v6, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v7, "No suitable activity for launching license url"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDrmPopupListener(Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->mListener:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;

    .line 288
    return-void
.end method

.method public showAskPopupDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 5
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "popupType"    # I
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 446
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 447
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getPopupString(I)Ljava/lang/String;

    move-result-object v0

    .line 448
    .local v0, "body":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "popup type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    if-nez v0, :cond_0

    .line 450
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v3, "no popup body!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :goto_0
    return-void

    .line 453
    :cond_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 454
    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/android/gallery3d/util/ImageDRMUtil$1;

    invoke-direct {v3, p0, p2, p3, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$1;-><init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;ILcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 484
    const v2, 0x7f0e0046

    new-instance v3, Lcom/sec/android/gallery3d/util/ImageDRMUtil$2;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$2;-><init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 492
    new-instance v2, Lcom/sec/android/gallery3d/util/ImageDRMUtil$3;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$3;-><init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 505
    new-instance v2, Lcom/sec/android/gallery3d/util/ImageDRMUtil$4;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$4;-><init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public showPopupDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "popupType"    # I
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 514
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 515
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getPopupString(I)Ljava/lang/String;

    move-result-object v0

    .line 516
    .local v0, "body":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 517
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string v3, "no popup body!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :goto_0
    return-void

    .line 520
    :cond_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 521
    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/android/gallery3d/util/ImageDRMUtil$5;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$5;-><init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 527
    new-instance v2, Lcom/sec/android/gallery3d/util/ImageDRMUtil$6;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil$6;-><init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public verifyRights(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 5
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "photo"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 291
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 292
    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "start verifyRights for open popup."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 317
    :cond_0
    :goto_0
    return v1

    .line 296
    :cond_1
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getPopupType(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    .line 297
    .local v0, "popupType":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 303
    :pswitch_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->showAskPopupDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;ILcom/sec/android/gallery3d/data/MediaItem;)V

    move v1, v2

    .line 304
    goto :goto_0

    .line 308
    :pswitch_1
    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->showPopupDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;ILcom/sec/android/gallery3d/data/MediaItem;)V

    move v1, v2

    .line 309
    goto :goto_0

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
