.class public Lcom/sec/android/gallery3d/ui/EmptySetDrawer;
.super Ljava/lang/Object;
.source "EmptySetDrawer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/EmptySetDrawer$2;,
        Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;
    }
.end annotation


# static fields
.field private static final FONT_COLOR:I

.field private static final FONT_DARK_THEME:I = -0xa0a0b

.field private static final FONT_LIGHT_THEME:I = -0xff7b63


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mIsContactPick:Z

.field private mIsPersonkPick:Z

.field private mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    const v0, -0xff7b63

    :goto_0
    sput v0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->FONT_COLOR:I

    return-void

    :cond_0
    const v0, -0xa0a0b

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsPersonkPick:Z

    .line 64
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsContactPick:Z

    .line 67
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;

    .line 68
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->initialize()V

    .line 69
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayHeight:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isPersonPick()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isContactPick()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isUsePick()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->isLandscape()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800()I
    .locals 1

    .prologue
    .line 51
    sget v0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->FONT_COLOR:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;)Lcom/sec/samsung/gallery/util/DimensionUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 75
    new-instance v1, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;-><init>(Lcom/sec/android/gallery3d/ui/EmptySetDrawer;Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    .line 76
    return-void
.end method

.method private isContactPick()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "caller_id_pick"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsContactPick:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsContactPick:Z

    return v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/lang/NullPointerException;
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsContactPick:Z

    goto :goto_0
.end method

.method private isLandscape()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayHeight:I

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isPersonPick()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v1, v2, :cond_0

    .line 101
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsPersonkPick:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsPersonkPick:Z

    return v1

    .line 103
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsPersonkPick:Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/NullPointerException;
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mIsPersonkPick:Z

    goto :goto_0
.end method

.method private isUsePick()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-nez v2, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 90
    .local v0, "launchModeType":Lcom/sec/samsung/gallery/core/LaunchModeType;
    if-eqz v0, :cond_0

    .line 93
    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static removeNoItemLayout(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p0, "gallery"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 139
    if-nez p0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 141
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$1;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public removeLayout()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->removeLayout()V

    .line 131
    :cond_0
    return-void
.end method

.method public setLayout()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->setLayout()V

    .line 125
    :cond_0
    return-void
.end method

.method public setPopupTextVisible(Z)V
    .locals 1
    .param p1, "isVisible"    # Z

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mPopupDescription:Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer$PopupDescription;->showText(Z)V

    .line 165
    :cond_0
    return-void
.end method

.method public setScreenSize(Landroid/graphics/Point;)V
    .locals 1
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 157
    iget v0, p1, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayHeight:I

    .line 158
    iget v0, p1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->mDisplayWidth:I

    .line 159
    return-void
.end method
