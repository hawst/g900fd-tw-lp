.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FullImageJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private mDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

.field private mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1095
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1096
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1097
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V
    .locals 0
    .param p2, "decoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    .line 1100
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1101
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->mDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 1102
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v0, 0x0

    .line 1108
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->mDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-eqz v1, :cond_1

    .line 1109
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->mDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 1118
    :cond_0
    :goto_0
    return-object v0

    .line 1112
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isTemporaryItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1115
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1116
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 1089
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method
