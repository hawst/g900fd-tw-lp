.class public Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "WidgetPreferenceActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;,
        Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;
    }
.end annotation


# static fields
.field private static final LAYOUT_POSITION:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static mIntervalItems:[Ljava/lang/CharSequence;


# instance fields
.field public mAppWidgetId:I

.field private mIntervalSelected:I

.field private mLayoutCategory:Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;

.field private mLayoutChanger:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;

.field private mLayoutSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mListenerExists:Z

.field private mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaMountReceiver:Landroid/content/BroadcastReceiver;

.field private mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 41
    iput v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalSelected:I

    .line 45
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$2;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mMediaMountReceiver:Landroid/content/BroadcastReceiver;

    .line 76
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$3;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 130
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mListenerExists:Z

    .line 156
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$4;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 168
    new-instance v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutChanger:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;

    .line 303
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutChanger:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutCategory:Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    .prologue
    .line 36
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalSelected:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalSelected:I

    return p1
.end method

.method private getMediaEjectFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 88
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 89
    return-object v0
.end method

.method private getMediaMountFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "inFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 73
    return-object v0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 134
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-ne v4, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v5, 0x42

    if-eq v4, v5, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v5, 0x17

    if-eq v4, v5, :cond_1

    .line 138
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    .line 153
    :cond_0
    :goto_0
    return v3

    .line 140
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 141
    .local v1, "focusedView":Landroid/view/View;
    instance-of v4, v1, Landroid/widget/ListView;

    if-eqz v4, :cond_3

    move-object v2, v1

    .line 142
    check-cast v2, Landroid/widget/ListView;

    .line 144
    .local v2, "listView":Landroid/widget/ListView;
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mListenerExists:Z

    if-nez v4, :cond_2

    .line 145
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 146
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mListenerExists:Z

    .line 149
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutChanger:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$LayoutChanger;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 150
    .local v0, "eventConsumed":Z
    if-nez v0, :cond_0

    .line 153
    .end local v0    # "eventConsumed":Z
    .end local v2    # "listView":Landroid/widget/ListView;
    :cond_3
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "target":Ljava/util/List;, "Ljava/util/List<Landroid/preference/PreferenceActivity$Header;>;"
    const v0, 0x7f07001c

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 123
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x3

    const v7, 0x7f0e0138

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 94
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->TAG:Ljava/lang/String;

    const-string v3, "onCreate!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 97
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    .line 99
    new-array v2, v4, [Ljava/lang/CharSequence;

    sput-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    .line 100
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v7, v3}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 101
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v7, v3}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 102
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    const/4 v3, 0x2

    new-array v4, v5, [Ljava/lang/Object;

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v7, v4}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 104
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 105
    .local v0, "deleteFilter":Landroid/content/IntentFilter;
    const-string v2, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 106
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 107
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mMediaMountReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getMediaMountFilter()Landroid/content/IntentFilter;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 108
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getMediaEjectFilter()Landroid/content/IntentFilter;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 109
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mMediaMountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 117
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 118
    return-void
.end method

.method public setLayoutCategory(Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;)V
    .locals 0
    .param p1, "layoutCategory"    # Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mLayoutCategory:Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;

    .line 128
    return-void
.end method
