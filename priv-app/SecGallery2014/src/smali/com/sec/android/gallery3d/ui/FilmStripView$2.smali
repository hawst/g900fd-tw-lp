.class Lcom/sec/android/gallery3d/ui/FilmStripView$2;
.super Ljava/lang/Object;
.source "FilmStripView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/UserInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/FilmStripView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FilmStripView;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUserDobuleTap()V
    .locals 0

    .prologue
    .line 587
    return-void
.end method

.method public onUserFlingUp()V
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserFlingUp()V

    .line 582
    :cond_0
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteraction()V

    .line 576
    :cond_0
    return-void
.end method

.method public onUserInteractionBegin()V
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteractionBegin()V

    .line 562
    :cond_0
    return-void
.end method

.method public onUserInteractionEnd()V
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mFilmStripVisible:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$300(Lcom/sec/android/gallery3d/ui/FilmStripView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$2;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$200(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteractionEnd()V

    .line 569
    :cond_0
    return-void
.end method
