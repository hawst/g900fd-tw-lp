.class Lcom/sec/android/gallery3d/gadget/LocalImagesSource$1;
.super Landroid/database/ContentObserver;
.source "LocalImagesSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/gadget/LocalImagesSource;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/gadget/LocalImagesSource;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource$1;->this$0:Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource$1;->this$0:Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentDirty:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->access$002(Lcom/sec/android/gallery3d/gadget/LocalImagesSource;Z)Z

    .line 62
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource$1;->this$0:Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    # getter for: Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->access$100(Lcom/sec/android/gallery3d/gadget/LocalImagesSource;)Lcom/sec/android/gallery3d/data/ContentListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/LocalImagesSource$1;->this$0:Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    # getter for: Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->mContentListener:Lcom/sec/android/gallery3d/data/ContentListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->access$100(Lcom/sec/android/gallery3d/gadget/LocalImagesSource;)Lcom/sec/android/gallery3d/data/ContentListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/data/ContentListener;->onContentDirty()V

    .line 63
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onContent Changed!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    return-void
.end method
