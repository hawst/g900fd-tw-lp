.class Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
.super Ljava/lang/Object;
.source "DownloadCache.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/DownloadCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/io/File;",
        ">;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private mFuture:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mKey:Ljava/lang/String;

.field private mNoNetworkProxy:Z

.field private mProxySet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;",
            ">;"
        }
    .end annotation
.end field

.field private final mUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/DownloadCache;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;)V
    .locals 1
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mProxySet:Ljava/util/HashSet;

    .line 302
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mUrl:Ljava/lang/String;

    .line 303
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 307
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;-><init>(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;)V

    .line 308
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mKey:Ljava/lang/String;

    .line 309
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    return-object p1
.end method


# virtual methods
.method public addProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V
    .locals 1
    .param p1, "proxy"    # Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    .prologue
    .line 314
    # setter for: Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->mTask:Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    invoke-static {p1, p0}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->access$402(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;)Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    .line 315
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mProxySet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 316
    return-void
.end method

.method public onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 360
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Ljava/io/File;>;"
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 361
    .local v2, "file":Ljava/io/File;
    const-wide/16 v4, 0x0

    .line 362
    .local v4, "id":J
    if-eqz v2, :cond_0

    .line 363
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/sec/android/gallery3d/data/DownloadCache;->insertEntry(Ljava/lang/String;Ljava/io/File;)J
    invoke-static {v7, v8, v2}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$600(Lcom/sec/android/gallery3d/data/DownloadCache;Ljava/lang/String;Ljava/io/File;)J

    move-result-wide v4

    .line 366
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 367
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mProxySet:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->isEmpty()Z

    move-result v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 387
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;
    invoke-static {v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$500(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/util/HashMap;

    move-result-object v8

    monitor-enter v8

    .line 372
    const/4 v0, 0x0

    .line 373
    .local v0, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;
    invoke-static {v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$700(Lcom/sec/android/gallery3d/data/DownloadCache;)Lcom/sec/android/gallery3d/common/LruCache;

    move-result-object v9

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374
    if-eqz v2, :cond_3

    .line 375
    :try_start_1
    new-instance v1, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    invoke-direct {v1, v7, v4, v5, v2}, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;-><init>(Lcom/sec/android/gallery3d/data/DownloadCache;JLjava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 376
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .local v1, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->mEntryMap:Lcom/sec/android/gallery3d/common/LruCache;
    invoke-static {v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$700(Lcom/sec/android/gallery3d/data/DownloadCache;)Lcom/sec/android/gallery3d/common/LruCache;

    move-result-object v7

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mUrl:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mKey:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v1}, Lcom/sec/android/gallery3d/common/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_2

    .line 377
    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$200()Ljava/lang/String;

    move-result-object v7

    const-string v10, "Entrymap put fail"

    invoke-static {v7, v10}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_2
    move-object v0, v1

    .line 380
    .end local v1    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :cond_3
    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 381
    :try_start_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mProxySet:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    .line 382
    .local v6, "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->setResult(Lcom/sec/android/gallery3d/data/DownloadCache$Entry;)V

    goto :goto_1

    .line 386
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "proxy":Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v7

    .line 380
    :catchall_1
    move-exception v7

    :goto_2
    :try_start_5
    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v7

    .line 384
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;
    invoke-static {v7}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$500(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/util/HashMap;

    move-result-object v7

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mUrl:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    const/16 v9, 0x10

    # invokes: Lcom/sec/android/gallery3d/data/DownloadCache;->freeSomeSpaceIfNeed(I)V
    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$800(Lcom/sec/android/gallery3d/data/DownloadCache;I)V

    .line 386
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 380
    .end local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .end local v3    # "i$":Ljava/util/Iterator;
    .restart local v1    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    goto :goto_2
.end method

.method public removeProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V
    .locals 3
    .param p1, "proxy"    # Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$500(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mProxySet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mProxySet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 323
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->mTaskMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$500(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    :cond_0
    monitor-exit v1

    .line 326
    return-void

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/io/File;
    .locals 10
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x0

    .line 331
    invoke-interface {p1, v4}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    .line 333
    const/4 v2, 0x0

    .line 335
    .local v2, "tempFile":Ljava/io/File;
    :try_start_0
    new-instance v3, Ljava/net/URL;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mUrl:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 336
    .local v3, "url":Ljava/net/URL;
    const-string v4, "cache"

    const-string v5, ".tmp"

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->mRoot:Ljava/io/File;
    invoke-static {v6}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$100(Lcom/sec/android/gallery3d/data/DownloadCache;)Ljava/io/File;

    move-result-object v6

    invoke-static {v4, v5, v6}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 338
    const/4 v4, 0x2

    invoke-interface {p1, v4}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    .line 340
    const/4 v0, 0x0

    .line 341
    .local v0, "downloaded":Z
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mNoNetworkProxy:Z

    if-eqz v4, :cond_0

    .line 342
    invoke-static {p1, v3, v2}, Lcom/sec/android/gallery3d/data/DownloadUtils;->requestDownloadNoProxy(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/io/File;)Z

    move-result v0

    .line 347
    :goto_0
    const/4 v4, 0x0

    invoke-interface {p1, v4}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    if-eqz v0, :cond_1

    .line 352
    invoke-interface {p1, v9}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-object v4, v2

    .line 355
    .end local v0    # "downloaded":Z
    .end local v3    # "url":Ljava/net/URL;
    :goto_1
    return-object v4

    .line 344
    .restart local v0    # "downloaded":Z
    .restart local v3    # "url":Ljava/net/URL;
    :cond_0
    :try_start_1
    invoke-static {p1, v3, v2}, Lcom/sec/android/gallery3d/data/DownloadUtils;->requestDownload(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 352
    :cond_1
    invoke-interface {p1, v9}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    .line 354
    .end local v0    # "downloaded":Z
    .end local v3    # "url":Ljava/net/URL;
    :goto_2
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 355
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 349
    :catch_0
    move-exception v1

    .line 350
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/DownloadCache;->access$200()Ljava/lang/String;

    move-result-object v4

    const-string v5, "fail to download %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mUrl:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 352
    invoke-interface {p1, v9}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    goto :goto_2

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-interface {p1, v9}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    throw v4
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 292
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public setNoNetworkProxy(Z)V
    .locals 0
    .param p1, "noNetworkProxy"    # Z

    .prologue
    .line 391
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->mNoNetworkProxy:Z

    .line 392
    return-void
.end method
