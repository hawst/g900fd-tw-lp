.class Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;
.super Ljava/lang/Object;
.source "SNSImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/SNSImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectDecodeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;Lcom/sec/android/gallery3d/remote/sns/SNSImage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage$1;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;-><init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 110
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 111
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 112
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;->this$0:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    # getter for: Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->access$100(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    invoke-static {p1, v2, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->requestDecode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 114
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
