.class public Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.super Landroid/os/Handler;
.source "SynchronizedHandler.java"


# instance fields
.field private final mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 1
    .param p1, "root"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/GLRoot;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/ui/GLRoot;Landroid/os/Looper;)V
    .locals 1
    .param p1, "root"    # Lcom/sec/android/gallery3d/ui/GLRoot;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 35
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 36
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/GLRoot;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    .line 37
    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 44
    :try_start_0
    invoke-super {p0, p1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    .line 48
    return-void

    .line 46
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->mRoot:Lcom/sec/android/gallery3d/ui/GLRoot;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v0
.end method
