.class Lcom/sec/android/gallery3d/data/HiddenAlbumSet$1;
.super Ljava/lang/Object;
.source "HiddenAlbumSet.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->reload()J
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/HiddenAlbumSet;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/HiddenAlbumSet;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet$1;->this$0:Lcom/sec/android/gallery3d/data/HiddenAlbumSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 3
    .param p1, "album1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "album2"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 121
    .local v0, "r":I
    if-nez v0, :cond_0

    .line 122
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v1

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v2

    sub-int v0, v1, v2

    .line 124
    .end local v0    # "r":I
    :cond_0
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 118
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet$1;->compare(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    return v0
.end method
