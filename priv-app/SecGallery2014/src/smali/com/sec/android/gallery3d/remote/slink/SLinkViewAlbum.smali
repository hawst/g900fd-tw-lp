.class public Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "SLinkViewAlbum.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mIsDirty:Z

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mList:Ljava/util/ArrayList;

    .line 37
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 38
    return-void
.end method

.method private loadOrUpdateMediaItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Landroid/database/Cursor;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 95
    invoke-virtual {p2, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 96
    .local v0, "image":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    invoke-virtual {v0, p3}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->updateContent(Landroid/database/Cursor;)Z

    .line 97
    return-object v0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 6
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v3, "returnItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mList:Ljava/util/ArrayList;

    add-int v5, p1, p2

    invoke-virtual {v4, p1, v5}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 85
    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 86
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 91
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_0
    return-object v3

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 118
    const-wide/16 v0, 0x0

    .line 119
    .local v0, "supportedOperations":J
    return-wide v0
.end method

.method public init(Landroid/database/Cursor;)V
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x1

    .line 41
    sget-object v7, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->TAG:Ljava/lang/String;

    const-string v8, "init"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 44
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    .line 45
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mIsDirty:Z

    .line 47
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    const-string v8, "_id"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 48
    .local v3, "idIndex":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 51
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 52
    .local v4, "imageId":J
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    const-string v9, "device_id"

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 54
    .local v1, "deviceId":I
    const-string v7, "/slink/%d/%d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 55
    .local v0, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0, v7, v8}, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->loadOrUpdateMediaItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Landroid/database/Cursor;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 56
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mList:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 64
    .end local v0    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v1    # "deviceId":I
    .end local v4    # "imageId":J
    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    :goto_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mCursor:Landroid/database/Cursor;

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 65
    return-void

    .line 57
    :catch_0
    move-exception v2

    .line 58
    .local v2, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v2}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->TAG:Ljava/lang/String;

    const-string v1, "reload()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mIsDirty:Z

    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mDataVersion:J

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mIsDirty:Z

    .line 108
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->mDataVersion:J

    return-wide v0
.end method
