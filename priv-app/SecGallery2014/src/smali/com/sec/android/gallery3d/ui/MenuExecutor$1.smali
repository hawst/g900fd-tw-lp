.class Lcom/sec/android/gallery3d/ui/MenuExecutor$1;
.super Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.source "MenuExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/MenuExecutor;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/MenuExecutor;Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;->this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 100
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 102
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 103
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .line 104
    .local v1, "listener":Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;->onProgressStart()V

    goto :goto_0

    .line 109
    .end local v1    # "listener":Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;->this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    # invokes: Lcom/sec/android/gallery3d/ui/MenuExecutor;->stopTaskAndDismissDialog()V
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->access$000(Lcom/sec/android/gallery3d/ui/MenuExecutor;)V

    .line 110
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 111
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .line 112
    .restart local v1    # "listener":Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;->onProgressComplete(I)V

    .line 114
    .end local v1    # "listener":Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;->this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    # getter for: Lcom/sec/android/gallery3d/ui/MenuExecutor;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->access$100(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    goto :goto_0

    .line 118
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;->this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    # getter for: Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->access$200(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;->this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    # getter for: Lcom/sec/android/gallery3d/ui/MenuExecutor;->mPaused:Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->access$300(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;->this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    # getter for: Lcom/sec/android/gallery3d/ui/MenuExecutor;->mDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->access$200(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Landroid/app/ProgressDialog;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 119
    :cond_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 120
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;

    .line 121
    .restart local v1    # "listener":Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;->onProgressUpdate(I)V

    goto :goto_0

    .line 127
    .end local v1    # "listener":Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;
    :pswitch_3
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/MenuExecutor$1;->this$0:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    # getter for: Lcom/sec/android/gallery3d/ui/MenuExecutor;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->access$400(Lcom/sec/android/gallery3d/ui/MenuExecutor;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v3, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "MenuExecutor"

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
