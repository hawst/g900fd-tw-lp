.class public Lcom/sec/android/gallery3d/util/GalleryUtils;
.super Ljava/lang/Object;
.source "GalleryUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;,
        Lcom/sec/android/gallery3d/util/GalleryUtils$ScanListener;
    }
.end annotation


# static fields
.field private static final BASIC_BURST_SHOT_PLAY_SPEED:I = 0x1

.field private static final CAMERA_IMAGE_CACHE_MIN_BYTES:I = 0x200000

.field private static DCIM_NAME:Ljava/lang/String; = null

.field public static final DEFAULT_NEW_ALBUM_DIR:Ljava/lang/String;

.field private static final DIR_TYPE_IMAGE:Ljava/lang/String; = "vnd.android.cursor.dir/image"

.field private static final DIR_TYPE_VIDEO:Ljava/lang/String; = "vnd.android.cursor.dir/video"

.field private static final EARTH_RADIUS_METERS:D = 6367000.0

.field public static final FHD_PIXELS:I = 0x1fa400

.field private static final GB:I = 0x40000000

.field private static final HD_PIXELS:I = 0xe1000

.field private static final IMAGE_CACHE_MIN_BYTES:I = 0x1400000

.field public static final INTENT_FILE_RELAY_CANCEL:Ljava/lang/String; = "com.sec.knox.container.FileRelayCancel"

.field public static final INTENT_FILE_RELAY_COMPLETE:Ljava/lang/String; = "com.sec.knox.container.FileRelayComplete"

.field public static final INTENT_FILE_RELAY_DONE:Ljava/lang/String; = "com.sec.knox.container.FileRelayDone"

.field public static final INTENT_FILE_RELAY_EXIST:Ljava/lang/String; = "com.sec.knox.container.FileRelayExist"

.field public static final INTENT_FILE_RELAY_FAIL:Ljava/lang/String; = "com.sec.knox.container.FileRelayFail"

.field public static final INTENT_FILE_RELAY_PROGRESS:Ljava/lang/String; = "com.sec.knox.container.FileRelayProgress"

.field public static final INTENT_FILE_RELAY_REQUEST:Ljava/lang/String; = "com.sec.knox.container.FileRelayRequest"

.field private static final KB:J = 0x400L

.field private static final KEY_BURST_SHOT_PLAY_SPEED:Ljava/lang/String; = "burstshot-playspeed"

.field private static final KEY_CAMERA_UPDATE:Ljava/lang/String; = "camera-update"

.field private static final KEY_FACETAG_SWITCH:Ljava/lang/String; = "facetag-switch"

.field private static final KEY_FACETAG_SWITCH_FOR_NORMAL_MODE:Ljava/lang/String; = "facetag-switch-for-normal-mode"

.field private static final KEY_HAS_CAMERA:Ljava/lang/String; = "has-camera"

.field public static final KNOX_EXTRA_KEY_PACKAGE_NAME:Ljava/lang/String; = "PACKAGENAME"

.field public static final KNOX_EXTRA_KEY_PROGRESS:Ljava/lang/String; = "PROGRESS"

.field public static KNOX_PATH:Ljava/lang/String; = null

.field private static final LWHVGA_PIXELS:I = 0x2a300

.field private static final MAPS_CLASS_NAME:Ljava/lang/String; = "com.google.android.maps.MapsActivity"

.field private static final MAPS_PACKAGE_NAME:Ljava/lang/String; = "com.google.android.apps.maps"

.field private static final MB:I = 0x100000

.field public static final MIME_TYPE_ALL:Ljava/lang/String; = "*/*"

.field public static final MIME_TYPE_IMAGE:Ljava/lang/String; = "image/*"

.field public static final MIME_TYPE_PANORAMA360:Ljava/lang/String; = "application/vnd.google.panorama360+jpg"

.field public static final MIME_TYPE_VIDEO:Ljava/lang/String; = "video/*"

.field public static final PANORAMA_RATIO:F = 2.7f

.field public static final PERFORMANCE:Ljava/lang/String; = "Gallery_Performance"

.field public static PERFORMANCE_LAUNCH_FINNISH:Z = false

.field public static PERFORMANCE_TIME:J = 0x0L

.field public static PERFORMANCE_TIME_ENABLE:Z = false

.field private static final PREFIX_HAS_PHOTO_EDITOR:Ljava/lang/String; = "has-editor-"

.field private static final PREFIX_PHOTO_EDITOR_UPDATE:Ljava/lang/String; = "editor-update-"

.field private static final QHD_PIXELS:I = 0x7e900

.field private static final QVGA_PIXELS:I = 0x12c00

.field private static final RAD_PER_DEG:D = 0.017453292519943295

.field public static SECRETBOX_PATH:Ljava/lang/String; = null

.field public static final SIDESYNC_KMS_SINK_SERVICE_CLASS:Ljava/lang/String; = "com.sec.android.sidesync.kms.sink.service.SideSyncServerService"

.field public static final SIDESYNC_KMS_SOURCE_SERVICE_CLASS:Ljava/lang/String; = "com.sec.android.sidesync.kms.source.service.SideSyncService"

.field public static final SIDESYNC_SETTING_PRESENTATION_KEY:Ljava/lang/String; = "sidesync_source_presentation"

.field private static final SIDESYNC_SETTING_PRESENTATION_VALUE_OFF:I = 0x0

.field private static final SPLITTER:Ljava/lang/String; = "/"

.field private static final TABLET_MULTIWINDOWBAR_HEIGHT:I = 0x28

.field private static final TAG:Ljava/lang/String; = "GalleryUtils"

.field public static final VIBRATOR_VALUE:I = 0xc

.field private static final WQHD_HEIGHT_PIXELS:I = 0xa00

.field private static final WQHD_PIXELS:I = 0x384000

.field private static final WVGA_PIXELS:I = 0x5dc00

.field private static isFaceTagEnabled:Z

.field private static isFaceTagEnabledInited:Z

.field private static mAlbumNumberBuffer:Ljava/lang/StringBuffer;

.field public static final mBlackThemeListClickedColor:I

.field private static mBurstShotPlaySpeed:I

.field private static mDisplayMax:I

.field private static mDisplayPixels:I

.field private static mGalleryId:I

.field private static mIsBurstShotPlaySpeedInited:Z

.field private static mIsDisplayMaxInited:Z

.field private static mIsDisplayPixelsInited:Z

.field private static mIsMultiWindow:Z

.field private static mIsOverHD:Z

.field private static mIsOverHDInitialized:Z

.field private static mIsSecretModeOn:Z

.field private static mParenthesis:[Ljava/lang/String;

.field private static mScannerClient:Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;

.field private static mStorageDirectory:Ljava/lang/String;

.field public static final mWhiteThemeListClickedColor:I

.field private static sCameraAvailable:Z

.field private static sCameraAvailableInitialized:Z

.field private static volatile sCurrentThread:Ljava/lang/Thread;

.field private static sKnoxInstaller:Lcom/sec/knox/containeragent/ContainerInstallerManager;

.field private static sPixelDensity:F

.field private static volatile sWarned:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 180
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->sPixelDensity:F

    .line 181
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCameraAvailableInitialized:Z

    .line 187
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME_ENABLE:Z

    .line 188
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_TIME:J

    .line 189
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->PERFORMANCE_LAUNCH_FINNISH:Z

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Pictures/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->DEFAULT_NEW_ALBUM_DIR:Ljava/lang/String;

    .line 204
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsDisplayPixelsInited:Z

    .line 205
    sput v4, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayPixels:I

    .line 206
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsDisplayMaxInited:Z

    .line 207
    sput v4, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayMax:I

    .line 208
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsOverHD:Z

    .line 209
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsOverHDInitialized:Z

    .line 216
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsMultiWindow:Z

    .line 218
    const/16 v0, 0xad

    const/16 v1, 0xcd

    const/16 v2, 0xde

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mWhiteThemeListClickedColor:I

    .line 219
    const/16 v0, 0xf

    const/16 v1, 0x51

    const/16 v2, 0x73

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mBlackThemeListClickedColor:I

    .line 221
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsSecretModeOn:Z

    .line 253
    sput-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->sKnoxInstaller:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 786
    sput-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    .line 787
    sput-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->KNOX_PATH:Ljava/lang/String;

    .line 1031
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mStorageDirectory:Ljava/lang/String;

    .line 1248
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabledInited:Z

    .line 1249
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabled:Z

    .line 1279
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsBurstShotPlaySpeedInited:Z

    .line 1280
    sput v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mBurstShotPlaySpeed:I

    .line 2741
    sput v4, Lcom/sec/android/gallery3d/util/GalleryUtils;->mGalleryId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1139
    return-void
.end method

.method public static IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z
    .locals 2
    .param p0, "type"    # Lcom/sec/samsung/gallery/core/TabTagType;
    .param p1, "IsIncludeFace"    # Ljava/lang/Boolean;

    .prologue
    const/4 v0, 0x1

    .line 2349
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne p0, v1, :cond_1

    .line 2356
    :cond_0
    :goto_0
    return v0

    .line 2351
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq p0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq p0, v1, :cond_0

    .line 2356
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static accurateDistanceMeters(DDDD)D
    .locals 14
    .param p0, "lat1"    # D
    .param p2, "lng1"    # D
    .param p4, "lat2"    # D
    .param p6, "lng2"    # D

    .prologue
    .line 357
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    sub-double v8, p4, p0

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    .line 358
    .local v0, "dlat":D
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    sub-double v8, p6, p2

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 359
    .local v2, "dlng":D
    mul-double v6, v0, v0

    mul-double v8, v2, v2

    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double v4, v6, v8

    .line 360
    .local v4, "x":D
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    const-wide/16 v10, 0x0

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v12, v4

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    const-wide v8, 0x415849c600000000L    # 6367000.0

    mul-double/2addr v6, v8

    return-wide v6
.end method

.method public static addKeepScreenOnFlag(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2656
    if-nez p0, :cond_0

    .line 2657
    const-string v0, "GalleryUtils"

    const-string v1, "ignore add keep screen on flag"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2661
    :goto_0
    return-void

    .line 2660
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method public static addKeepScreenOnFlag(Landroid/view/Window;)V
    .locals 2
    .param p0, "window"    # Landroid/view/Window;

    .prologue
    .line 2664
    if-nez p0, :cond_0

    .line 2665
    const-string v0, "GalleryUtils"

    const-string v1, "ignore add keep screen on flag"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2670
    :goto_0
    return-void

    .line 2669
    :cond_0
    const/16 v0, 0x80

    invoke-virtual {p0, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method public static addNewFileToDB(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1124
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mScannerClient:Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;->scanPath(Ljava/lang/String;)V

    .line 1125
    return-void
.end method

.method public static allowDownloadBySettings(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1680
    const-string/jumbo v1, "setting_wifi_only"

    invoke-static {p0, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 1681
    .local v0, "isWifiOnly":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAccountSettings:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1682
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v1

    .line 1684
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static assertInMainThread(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 773
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 774
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 776
    :cond_0
    return-void
.end method

.method public static assertNotInRenderThread()V
    .locals 3

    .prologue
    .line 321
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->sWarned:Z

    if-nez v0, :cond_0

    .line 322
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCurrentThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    .line 323
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->sWarned:Z

    .line 324
    const-string v0, "GalleryUtils"

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Should not do this in render thread"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 327
    :cond_0
    return-void
.end method

.method public static cameraSwitchingBoost(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2448
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraSwitchBoost:Z

    if-eqz v0, :cond_0

    .line 2449
    const v0, 0x1e8480

    const/4 v1, 0x2

    const/16 v2, 0x7d0

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setDVFSLock(Landroid/content/Context;III)V

    .line 2451
    :cond_0
    return-void
.end method

.method public static checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z
    .locals 14
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1043
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTDataCache:Z

    if-eqz v9, :cond_1

    .line 1045
    :try_start_0
    new-instance v6, Landroid/os/StatFs;

    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExternalCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1046
    .local v6, "stat":Landroid/os/StatFs;
    invoke-virtual {v6}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v6}, Landroid/os/StatFs;->getBlockSize()I

    move-result v9

    int-to-long v12, v9

    mul-long v4, v10, v12

    .line 1047
    .local v4, "remaining":J
    const-wide/32 v10, 0x200000

    cmp-long v9, v4, v10

    if-gez v9, :cond_0

    .line 1048
    const-string v9, "GalleryUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkLowStorage "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-wide/16 v12, 0x400

    div-long v12, v4, v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "k, clear Cache."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    const/4 v9, 0x1

    invoke-static {p1, v9}, Lcom/sec/android/gallery3d/util/CacheManager;->removeOldFilesIfNecessary(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074
    .end local v4    # "remaining":J
    .end local v6    # "stat":Landroid/os/StatFs;
    :cond_0
    :goto_0
    return v7

    .line 1052
    :catch_0
    move-exception v1

    .line 1053
    .local v1, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    move v7, v8

    .line 1054
    goto :goto_0

    .line 1058
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExternalStorageFreeSize()J

    move-result-wide v4

    .line 1059
    .restart local v4    # "remaining":J
    const-wide/32 v10, 0x1400000

    cmp-long v9, v4, v10

    if-gez v9, :cond_2

    move v7, v8

    .line 1060
    goto :goto_0

    .line 1062
    :cond_2
    invoke-interface {p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v0

    .line 1063
    .local v0, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    const-wide/16 v2, 0x0

    .line 1064
    .local v2, "lowStorageThreadhold":J
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageDiskCacheMaxBytes()J

    move-result-wide v10

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getCurrentDiskCacheSize()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 1065
    cmp-long v9, v4, v2

    if-gez v9, :cond_0

    .line 1066
    const-string v7, "GalleryUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "context = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " / checkLowStorage "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-wide/16 v10, 0x400

    div-long v10, v4, v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "k"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move v7, v8

    .line 1067
    goto :goto_0

    .line 1071
    .end local v0    # "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    .end local v2    # "lowStorageThreadhold":J
    .end local v4    # "remaining":J
    :catch_1
    move-exception v1

    .line 1072
    .restart local v1    # "e":Ljava/lang/RuntimeException;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " checkLowStorageforMedia() for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/gallery3d/util/GalleryUtils;->mStorageDirectory:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1073
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    move v7, v8

    .line 1074
    goto :goto_0
.end method

.method public static checkPackageExist(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 2257
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2261
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 2258
    :catch_0
    move-exception v0

    .line 2259
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static checkUseRecommandDropboxPopup(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2759
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi$API;->isDealDevice(Landroid/content/Context;)I

    move-result v0

    .line 2760
    .local v0, "checResult":I
    const-string v4, "GalleryUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CloudIntegratorApi isDealDevice = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2761
    if-ne v0, v2, :cond_0

    .line 2766
    .end local v0    # "checResult":I
    :goto_0
    return v2

    .restart local v0    # "checResult":I
    :cond_0
    move v2, v3

    .line 2764
    goto :goto_0

    .line 2765
    .end local v0    # "checResult":I
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    move v2, v3

    .line 2766
    goto :goto_0
.end method

.method public static checkViewStateByFilterBySetting(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/android/gallery3d/app/ActivityState;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "statusMgr"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p2, "viewState"    # Lcom/sec/android/gallery3d/app/ActivityState;

    .prologue
    const/4 v6, 0x1

    .line 2484
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    .line 2485
    .local v4, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2487
    .local v1, "categoryStringResId":Ljava/lang/Integer;
    if-nez v1, :cond_1

    .line 2510
    .end local p2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    :goto_0
    return-void

    .line 2490
    .restart local p2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilterBySettingPrefKeyString(I)Ljava/lang/String;

    move-result-object v2

    .line 2492
    .local v2, "prefKey":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 2495
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 2497
    .local v3, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v3, v2, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2500
    instance-of v5, p2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v5, :cond_2

    if-eqz p2, :cond_2

    .line 2501
    check-cast p2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .end local p2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    goto :goto_0

    .line 2502
    .restart local p2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_2
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_3

    instance-of v5, p2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v5, :cond_3

    if-eqz p2, :cond_3

    .line 2503
    check-cast p2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .end local p2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->onBackPressed()V

    goto :goto_0

    .line 2505
    .restart local p2    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2506
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2507
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 2508
    const-class v5, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {p1, v5, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public static clearKeepScreenOnFlag(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2674
    if-nez p0, :cond_0

    .line 2675
    const-string v0, "GalleryUtils"

    const-string v1, "ignore clear keep screen on flag"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2679
    :goto_0
    return-void

    .line 2678
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public static clearKeepScreenOnFlag(Landroid/view/Window;)V
    .locals 2
    .param p0, "window"    # Landroid/view/Window;

    .prologue
    .line 2682
    if-nez p0, :cond_0

    .line 2683
    const-string v0, "GalleryUtils"

    const-string v1, "ignore add keep screen on flag"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2688
    :goto_0
    return-void

    .line 2687
    :cond_0
    const/16 v0, 0x80

    invoke-virtual {p0, v0}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public static closeCameraLens(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1675
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1676
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.camera.action.CLOSE_LENS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1677
    :cond_1
    return-void
.end method

.method public static convertInputStreamToByteArray(Ljava/io/InputStream;)[B
    .locals 5
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1535
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1536
    .local v1, "bis":Ljava/io/BufferedInputStream;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1537
    .local v2, "buf":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->read()I

    move-result v3

    .line 1538
    .local v3, "result":I
    :goto_0
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 1539
    int-to-byte v0, v3

    .line 1540
    .local v0, "b":B
    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 1541
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->read()I

    move-result v3

    .line 1542
    goto :goto_0

    .line 1543
    .end local v0    # "b":B
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method

.method public static determineTypeBits(Landroid/content/Context;Landroid/content/Intent;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 560
    const/4 v1, 0x0

    .line 561
    .local v1, "typeBits":I
    invoke-virtual {p1, p0}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 575
    .local v0, "type":Ljava/lang/String;
    const-string v2, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 576
    const-string v2, "*/*"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 577
    const/4 v1, 0x7

    .line 592
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 593
    const/4 v1, 0x3

    .line 596
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_INTENT_EXTRA_LOCAL_ONLY:Z

    if-eqz v2, :cond_2

    .line 597
    const-string v2, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 598
    or-int/lit8 v1, v1, 0x4

    .line 602
    :cond_2
    return v1

    .line 578
    :cond_3
    const-string v2, "image/*"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "vnd.android.cursor.dir/image"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 579
    :cond_4
    const/4 v1, 0x5

    goto :goto_0

    .line 580
    :cond_5
    const-string/jumbo v2, "video/*"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string/jumbo v2, "vnd.android.cursor.dir/video"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 581
    :cond_6
    const/4 v1, 0x6

    goto :goto_0

    .line 584
    :cond_7
    const-string v2, "*/*"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 585
    const/4 v1, 0x3

    goto :goto_0

    .line 586
    :cond_8
    const-string v2, "image/*"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string/jumbo v2, "vnd.android.cursor.dir/image"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 587
    :cond_9
    const/4 v1, 0x1

    goto :goto_0

    .line 588
    :cond_a
    const-string/jumbo v2, "video/*"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string/jumbo v2, "vnd.android.cursor.dir/video"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 589
    :cond_b
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static deviceRotationLock(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v7, 0x9

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1891
    const-string/jumbo v3, "window"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 1892
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1893
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 1894
    .local v1, "orientation":I
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDefaultRotationPhoneType:Z

    if-eqz v3, :cond_0

    .line 1895
    packed-switch v1, :pswitch_data_0

    .line 1929
    .end local p0    # "context":Landroid/content/Context;
    :goto_0
    return-void

    .line 1897
    .restart local p0    # "context":Landroid/content/Context;
    :pswitch_0
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1900
    .restart local p0    # "context":Landroid/content/Context;
    :pswitch_1
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1903
    .restart local p0    # "context":Landroid/content/Context;
    :pswitch_2
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1906
    .restart local p0    # "context":Landroid/content/Context;
    :pswitch_3
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1912
    .restart local p0    # "context":Landroid/content/Context;
    :cond_0
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 1914
    :pswitch_4
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1917
    .restart local p0    # "context":Landroid/content/Context;
    :pswitch_5
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1920
    .restart local p0    # "context":Landroid/content/Context;
    :pswitch_6
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1923
    .restart local p0    # "context":Landroid/content/Context;
    :pswitch_7
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 1895
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1912
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static deviceRotationLock(Landroid/content/Context;I)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "orientation"    # I

    .prologue
    .line 1932
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1933
    return-void
.end method

.method public static deviceRotationUnLock(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1936
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1937
    return-void
.end method

.method public static dpToPixel(F)F
    .locals 1
    .param p0, "dp"    # F

    .prologue
    .line 288
    sget v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->sPixelDensity:F

    mul-float/2addr v0, p0

    return v0
.end method

.method public static dpToPixel(I)I
    .locals 1
    .param p0, "dp"    # I

    .prologue
    .line 292
    int-to-float v0, p0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static enableScanner(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1128
    new-instance v0, Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mScannerClient:Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;

    .line 1129
    return-void
.end method

.method public static fakeBusy(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)V
    .locals 4
    .param p0, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p1, "timeout"    # I

    .prologue
    .line 371
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    .line 372
    .local v0, "cv":Landroid/os/ConditionVariable;
    new-instance v1, Lcom/sec/android/gallery3d/util/GalleryUtils$1;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils$1;-><init>(Landroid/os/ConditionVariable;)V

    invoke-interface {p0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 378
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/ConditionVariable;->block(J)Z

    .line 379
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V

    .line 380
    return-void
.end method

.method public static fastDistanceMeters(DDDD)D
    .locals 12
    .param p0, "latRad1"    # D
    .param p2, "lngRad1"    # D
    .param p4, "latRad2"    # D
    .param p6, "lngRad2"    # D

    .prologue
    .line 334
    sub-double v8, p0, p4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3f91df46a2529d39L    # 0.017453292519943295

    cmpl-double v8, v8, v10

    if-gtz v8, :cond_0

    sub-double v8, p2, p6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3f91df46a2529d39L    # 0.017453292519943295

    cmpl-double v8, v8, v10

    if-lez v8, :cond_1

    .line 336
    :cond_0
    invoke-static/range {p0 .. p7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->accurateDistanceMeters(DDDD)D

    move-result-wide v8

    .line 352
    :goto_0
    return-wide v8

    .line 339
    :cond_1
    sub-double v2, p0, p4

    .line 342
    .local v2, "sineLat":D
    sub-double v4, p2, p6

    .line 346
    .local v4, "sineLng":D
    add-double v8, p0, p4

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 347
    .local v0, "cosTerms":D
    mul-double/2addr v0, v0

    .line 348
    mul-double v8, v2, v2

    mul-double v10, v0, v4

    mul-double/2addr v10, v4

    add-double v6, v8, v10

    .line 349
    .local v6, "trigTerm":D
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 352
    const-wide v8, 0x415849c600000000L    # 6367000.0

    mul-double/2addr v8, v6

    goto :goto_0
.end method

.method public static formatDuration(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "duration"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 546
    div-int/lit16 v1, p1, 0xe10

    .line 547
    .local v1, "h":I
    mul-int/lit16 v4, v1, 0xe10

    sub-int v4, p1, v4

    div-int/lit8 v2, v4, 0x3c

    .line 548
    .local v2, "m":I
    mul-int/lit16 v4, v1, 0xe10

    mul-int/lit8 v5, v2, 0x3c

    add-int/2addr v4, v5

    sub-int v3, p1, v4

    .line 550
    .local v3, "s":I
    if-nez v1, :cond_0

    .line 551
    const v4, 0x7f0e002f

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 555
    .local v0, "durationValue":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 553
    .end local v0    # "durationValue":Ljava/lang/String;
    :cond_0
    const v4, 0x7f0e0030

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "durationValue":Ljava/lang/String;
    goto :goto_0
.end method

.method public static formatItemsSelectedSize(Landroid/content/Context;D)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "size"    # D

    .prologue
    .line 2293
    const-string v1, ""

    .line 2294
    .local v1, "value":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 2296
    .local v0, "language":Ljava/lang/String;
    const-string v2, "ar"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ar-rIL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "fa"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "ur"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ko"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2297
    :cond_0
    const-wide/high16 v2, 0x41d0000000000000L    # 1.073741824E9

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_1

    .line 2298
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.2f"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0341

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/high16 v6, 0x41d0000000000000L    # 1.073741824E9

    div-double v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2317
    :goto_0
    return-object v1

    .line 2299
    :cond_1
    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_2

    .line 2300
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0342

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    div-double v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2301
    :cond_2
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_3

    .line 2302
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0343

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    double-to-int v5, p1

    int-to-long v6, v5

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2304
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0344

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    double-to-int v5, p1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2307
    :cond_4
    const-wide/high16 v2, 0x41d0000000000000L    # 1.073741824E9

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_5

    .line 2308
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.2f "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0341

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/high16 v6, 0x41d0000000000000L    # 1.073741824E9

    div-double v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2309
    :cond_5
    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_6

    .line 2310
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0342

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    div-double v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2311
    :cond_6
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_7

    .line 2312
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0343

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    double-to-int v5, p1

    int-to-long v6, v5

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2314
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0e0344

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    double-to-int v5, p1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public static formatLatitudeLongitude(Ljava/lang/String;DD)Ljava/lang/String;
    .locals 5
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 478
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static get3DVideoType(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1986
    if-nez p0, :cond_1

    .line 1987
    const-string v6, "GalleryUtils"

    const-string v7, "get3DVideoType() path is null"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    .line 2014
    :cond_0
    :goto_0
    return-object v3

    .line 1991
    :cond_1
    const/4 v3, 0x0

    .line 1993
    .local v3, "is3d":Ljava/lang/String;
    :try_start_0
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1994
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual {v4, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1995
    const-class v6, Landroid/media/MediaMetadataRetriever;

    const-string v7, "METADATA_KEY_SEC_3DVIDEOTYPE"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 1996
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_2

    .line 1997
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    .line 1998
    .local v2, "fieldInt":I
    invoke-virtual {v4, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    .line 1999
    const-string v6, "GalleryUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "is3DVideo() METADATA_KEY_SEC_IS_3DVIDEO = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2003
    .end local v2    # "fieldInt":I
    :goto_1
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 2011
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    :goto_2
    if-nez v3, :cond_0

    move-object v3, v5

    .line 2014
    goto :goto_0

    .line 2001
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    :cond_2
    const-string v6, "GalleryUtils"

    const-string v7, "get3DVideoType - \'METADATA_KEY_SEC_3DVIDEOTYPE\' not exist"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 2004
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    :catch_0
    move-exception v0

    .line 2005
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 2006
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 2007
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_2

    .line 2008
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v0

    .line 2009
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public static getActionBarSize(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1474
    if-nez p0, :cond_0

    .line 1475
    const/4 v1, 0x0

    .line 1481
    .end local p0    # "context":Landroid/content/Context;
    .local v0, "tv":Landroid/util/TypedValue;
    :goto_0
    return v1

    .line 1476
    .end local v0    # "tv":Landroid/util/TypedValue;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1477
    .restart local v0    # "tv":Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x10102eb

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1478
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1479
    iget v1, v0, Landroid/util/TypedValue;->data:I

    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v1

    goto :goto_0

    .line 1481
    .restart local p0    # "context":Landroid/content/Context;
    :cond_1
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0
.end method

.method public static getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "itemCount"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1864
    sget-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mAlbumNumberBuffer:Ljava/lang/StringBuffer;

    if-nez v5, :cond_2

    .line 1865
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    sput-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mAlbumNumberBuffer:Ljava/lang/StringBuffer;

    .line 1867
    const v5, 0x7f0e01c0

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "%d"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    .line 1868
    sget-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    array-length v5, v5

    if-eq v5, v9, :cond_1

    .line 1869
    :cond_0
    new-array v5, v9, [Ljava/lang/String;

    const-string v6, " ("

    aput-object v6, v5, v7

    const-string v6, ")"

    aput-object v6, v5, v8

    sput-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    .line 1872
    :cond_1
    sget-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mAlbumNumberBuffer:Ljava/lang/StringBuffer;

    sget-object v6, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1875
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mAlbumNumberBuffer:Ljava/lang/StringBuffer;

    .line 1876
    .local v0, "albumNumberBuffer":Ljava/lang/StringBuffer;
    sget-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    if-nez v5, :cond_3

    .line 1877
    new-array v5, v9, [Ljava/lang/String;

    const-string v6, " ("

    aput-object v6, v5, v7

    const-string v6, ")"

    aput-object v6, v5, v8

    sput-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    .line 1879
    :cond_3
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mParenthesis:[Ljava/lang/String;

    .line 1882
    .local v1, "albumParenthesis":[Ljava/lang/String;
    aget-object v5, v1, v7

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1884
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-static {v5}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v4

    .line 1885
    .local v4, "num":Ljava/text/NumberFormat;
    int-to-long v2, p1

    .line 1887
    .local v2, "itemCount1":J
    invoke-virtual {v4, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v6, v1, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getAlbumPathFromMediaItemPath(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/String;
    .locals 4
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v3, -0x1

    .line 798
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 799
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 800
    .local v0, "filePath":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->CAMERA_PATH:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 801
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDCIMName()Ljava/lang/String;

    move-result-object v0

    .line 813
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 803
    .restart local v0    # "filePath":Ljava/lang/String;
    :cond_1
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 804
    .local v1, "idxOfslash":I
    if-le v1, v3, :cond_2

    .line 805
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 806
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 807
    if-le v1, v3, :cond_0

    .line 808
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 813
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "idxOfslash":I
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public static getApplicationLogo(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 1619
    if-nez p1, :cond_0

    .line 1620
    const/4 v0, 0x0

    .line 1623
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static getBucketId(Ljava/lang/String;)I
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 518
    if-nez p0, :cond_0

    .line 519
    const/4 v0, -0x1

    .line 521
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public static getBytes(Ljava/lang/String;)[B
    .locals 8
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 301
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    new-array v6, v7, [B

    .line 302
    .local v6, "result":[B
    const/4 v4, 0x0

    .line 303
    .local v4, "output":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v5, v4

    .end local v4    # "output":I
    .local v5, "output":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-char v1, v0, v2

    .line 304
    .local v1, "ch":C
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "output":I
    .restart local v4    # "output":I
    and-int/lit16 v7, v1, 0xff

    int-to-byte v7, v7

    aput-byte v7, v6, v5

    .line 305
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "output":I
    .restart local v5    # "output":I
    shr-int/lit8 v7, v1, 0x8

    int-to-byte v7, v7

    aput-byte v7, v6, v4

    .line 303
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 307
    .end local v1    # "ch":C
    :cond_0
    return-object v6
.end method

.method public static getBytesLong(Ljava/lang/String;J)[B
    .locals 11
    .param p0, "in"    # Ljava/lang/String;
    .param p1, "val"    # J

    .prologue
    const/16 v10, 0x8

    .line 666
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    add-int/lit8 v8, v8, 0x8

    new-array v7, v8, [B

    .line 667
    .local v7, "result":[B
    const/4 v5, 0x0

    .line 668
    .local v5, "output":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v6, v5

    .end local v5    # "output":I
    .local v6, "output":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-char v1, v0, v3

    .line 669
    .local v1, "ch":C
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "output":I
    .restart local v5    # "output":I
    and-int/lit16 v8, v1, 0xff

    int-to-byte v8, v8

    aput-byte v8, v7, v6

    .line 670
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "output":I
    .restart local v6    # "output":I
    shr-int/lit8 v8, v1, 0x8

    int-to-byte v8, v8

    aput-byte v8, v7, v5

    .line 668
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 672
    .end local v1    # "ch":C
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v10, :cond_1

    .line 673
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "output":I
    .restart local v5    # "output":I
    const-wide/16 v8, 0xff

    and-long/2addr v8, p1

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v7, v6

    .line 674
    shr-long/2addr p1, v10

    .line 672
    add-int/lit8 v2, v2, 0x1

    move v6, v5

    .end local v5    # "output":I
    .restart local v6    # "output":I
    goto :goto_1

    .line 676
    :cond_1
    return-object v7
.end method

.method public static getCloudIds(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1742
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1744
    .local v7, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 1746
    .local v6, "cloudCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data like\'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CLOUD_CACHE_DIR_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1749
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1751
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1752
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1755
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1758
    return-object v7

    .line 1755
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getCropSizeforSetCallerID(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2570
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    const v1, 0x1fa400

    if-lt v0, v1, :cond_0

    .line 2571
    const/16 v0, 0x2d0

    .line 2573
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x100

    goto :goto_0
.end method

.method public static getDCIMName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 783
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->DCIM_NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # J

    .prologue
    const/16 v3, 0x20

    .line 1016
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 1017
    .local v1, "shortDateFormat":Ljava/text/DateFormat;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1018
    .local v0, "dateString":Ljava/lang/StringBuffer;
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1019
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x181

    invoke-static {p0, p1, p2, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1028
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 1024
    :cond_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x141

    invoke-static {p0, p1, p2, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static getDeclaredIntField(Ljava/lang/Class;Ljava/lang/String;)I
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 1637
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 1645
    :goto_0
    return v1

    .line 1638
    :catch_0
    move-exception v0

    .line 1639
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    .line 1645
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :goto_1
    const/4 v1, -0x1

    goto :goto_0

    .line 1640
    :catch_1
    move-exception v0

    .line 1641
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 1642
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1643
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getDisplayHeight(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1448
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1449
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 1450
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1451
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v2
.end method

.method public static getDisplayMax(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 752
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsDisplayMaxInited:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayMax:I

    if-lez v1, :cond_0

    .line 753
    sget v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayPixels:I

    .line 759
    :goto_0
    return v1

    .line 755
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 756
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sput v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayMax:I

    .line 757
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsDisplayMaxInited:Z

    .line 759
    sget v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayMax:I

    goto :goto_0
.end method

.method public static getDisplayPixels(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 738
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsDisplayPixelsInited:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayPixels:I

    if-lez v2, :cond_0

    .line 739
    sget v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayPixels:I

    .line 747
    :goto_0
    return v2

    .line 741
    :cond_0
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 742
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 743
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 744
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v2, v3

    sput v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayPixels:I

    .line 745
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsDisplayPixelsInited:Z

    .line 747
    sget v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mDisplayPixels:I

    goto :goto_0
.end method

.method public static getDisplayWidth(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1441
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1442
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 1443
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1444
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v2
.end method

.method public static getDrawableWidth(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 2750
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2751
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 2752
    const/4 v1, 0x0

    .line 2754
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    goto :goto_0
.end method

.method public static getExifOrientation(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 853
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 854
    .local v0, "filePath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static getExifOrientation(Ljava/lang/String;)I
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 858
    const/4 v0, 0x0

    .line 859
    .local v0, "degree":I
    if-nez p0, :cond_0

    move v1, v0

    .line 890
    .end local v0    # "degree":I
    .local v1, "degree":I
    :goto_0
    return v1

    .line 861
    .end local v1    # "degree":I
    .restart local v0    # "degree":I
    :cond_0
    new-instance v3, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 863
    .local v3, "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    :try_start_0
    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 872
    sget v6, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_ORIENTATION:I

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    .line 873
    .local v5, "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    if-eqz v5, :cond_1

    .line 874
    const-wide/16 v6, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/gallery3d/exif/ExifTag;->forceGetValueAsLong(J)J

    move-result-wide v6

    long-to-int v4, v6

    .line 875
    .local v4, "orientation":I
    packed-switch v4, :pswitch_data_0

    .end local v4    # "orientation":I
    :cond_1
    :goto_1
    :pswitch_0
    move v1, v0

    .line 890
    .end local v0    # "degree":I
    .restart local v1    # "degree":I
    goto :goto_0

    .line 864
    .end local v1    # "degree":I
    .end local v5    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    .restart local v0    # "degree":I
    :catch_0
    move-exception v2

    .line 865
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move v1, v0

    .line 866
    .end local v0    # "degree":I
    .restart local v1    # "degree":I
    goto :goto_0

    .line 867
    .end local v1    # "degree":I
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "degree":I
    :catch_1
    move-exception v2

    .line 868
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move v1, v0

    .line 869
    .end local v0    # "degree":I
    .restart local v1    # "degree":I
    goto :goto_0

    .line 877
    .end local v1    # "degree":I
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "degree":I
    .restart local v4    # "orientation":I
    .restart local v5    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :pswitch_1
    const/16 v0, 0x5a

    .line 878
    goto :goto_1

    .line 880
    :pswitch_2
    const/16 v0, 0xb4

    .line 881
    goto :goto_1

    .line 883
    :pswitch_3
    const/16 v0, 0x10e

    .line 884
    goto :goto_1

    .line 875
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getExternalCacheDir(Landroid/content/Context;)Ljava/io/File;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1649
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTDataCache:Z

    if-eqz v0, :cond_0

    .line 1650
    new-instance v0, Ljava/io/File;

    const-string v1, "/tdata"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1652
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public static getExternalStorageFreeSize()J
    .locals 8

    .prologue
    .line 1037
    new-instance v2, Landroid/os/StatFs;

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mStorageDirectory:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1038
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v6, v3

    mul-long v0, v4, v6

    .line 1039
    .local v0, "remaining":J
    return-wide v0
.end method

.method public static getFaceTagStatusForNormalMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1273
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1274
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "facetag-switch-for-normal-mode"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 830
    invoke-virtual {p1}, Landroid/net/Uri;->isHierarchical()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 831
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    .line 849
    :goto_0
    return-object v9

    .line 834
    :cond_1
    const/4 v9, 0x0

    .line 835
    .local v9, "result":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 836
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 838
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 839
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 840
    const-string v1, "_data"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 841
    .local v6, "columnIndex":I
    const/4 v1, -0x1

    if-eq v6, v1, :cond_2

    .line 842
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 847
    .end local v6    # "columnIndex":I
    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 844
    :catch_0
    move-exception v8

    .line 845
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 847
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private static getFilterBySettingPrefKeyString(I)Ljava/lang/String;
    .locals 2
    .param p0, "stringResId"    # I

    .prologue
    .line 2513
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "filterby_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2514
    .local v0, "key":Ljava/lang/StringBuilder;
    sparse-switch p0, :sswitch_data_0

    .line 2540
    const/4 v1, 0x0

    .line 2543
    :goto_0
    return-object v1

    .line 2516
    :sswitch_0
    const-string v1, "Event"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2543
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2519
    :sswitch_1
    const-string v1, "People"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2522
    :sswitch_2
    const-string v1, "Scenery"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2525
    :sswitch_3
    const-string v1, "Documents"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2528
    :sswitch_4
    const-string v1, "Food"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2531
    :sswitch_5
    const-string v1, "Pets"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2534
    :sswitch_6
    const-string v1, "Vehicles"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2537
    :sswitch_7
    const-string v1, "Flower"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2514
    :sswitch_data_0
    .sparse-switch
        0x7f0e00a7 -> :sswitch_1
        0x7f0e0441 -> :sswitch_0
        0x7f0e0478 -> :sswitch_2
        0x7f0e0479 -> :sswitch_4
        0x7f0e047b -> :sswitch_5
        0x7f0e047c -> :sswitch_3
        0x7f0e047d -> :sswitch_6
        0x7f0e047f -> :sswitch_7
    .end sparse-switch
.end method

.method public static getGalleryID()I
    .locals 1

    .prologue
    .line 2746
    sget v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mGalleryId:I

    return v0
.end method

.method public static getGlobalSystemUiVisibility(Landroid/content/Context;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2321
    const/4 v2, -0x1

    .line 2322
    .local v2, "visibility":I
    if-nez p0, :cond_0

    move v3, v2

    .line 2343
    .end local v2    # "visibility":I
    .local v3, "visibility":I
    :goto_0
    return v3

    .line 2325
    .end local v3    # "visibility":I
    .restart local v2    # "visibility":I
    :cond_0
    const-string/jumbo v5, "window"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 2327
    .local v4, "wm":Landroid/view/WindowManager;
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "getGlobalSystemUiVisibility"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2328
    .local v1, "m":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .end local v1    # "m":Ljava/lang/reflect/Method;
    :goto_1
    move v3, v2

    .line 2343
    .end local v2    # "visibility":I
    .restart local v3    # "visibility":I
    goto :goto_0

    .line 2329
    .end local v3    # "visibility":I
    .restart local v2    # "visibility":I
    :catch_0
    move-exception v0

    .line 2330
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const/4 v2, -0x1

    .line 2331
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 2332
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2333
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v2, -0x1

    .line 2334
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 2335
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 2336
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const/4 v2, -0x1

    .line 2337
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 2338
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 2339
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const/4 v2, -0x1

    .line 2340
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getIndexFromMmsList(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;J)I
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "mid"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "J)I"
        }
    .end annotation

    .prologue
    .line 1818
    .local p1, "outUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v2, "content://mms/part"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1820
    .local v3, "partsUri":Landroid/net/Uri;
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string v6, "mid"

    aput-object v6, v4, v2

    const/4 v2, 0x2

    const-string v6, "ct"

    aput-object v6, v4, v2

    const/4 v2, 0x3

    const-string v6, "_data"

    aput-object v6, v4, v2

    .line 1822
    .local v4, "projection":[Ljava/lang/String;
    const-string v5, "mid = ?"

    .line 1823
    .local v5, "selection":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1824
    .local v10, "cursorFound":Landroid/database/Cursor;
    const-wide/16 v8, -0x1

    .line 1825
    .local v8, "currentId":J
    if-eqz p2, :cond_0

    .line 1826
    const/16 v2, 0x2f

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v8, v2

    .line 1827
    :cond_0
    const/4 v14, -0x1

    .line 1831
    .local v14, "matchingIndex":I
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v6, v7

    const-string v7, "_id"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1834
    if-eqz v10, :cond_3

    .line 1835
    :cond_1
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1837
    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1838
    .local v12, "foundId":J
    const-string v2, "ct"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1840
    .local v11, "foundtype":Ljava/lang/String;
    const-string v2, "image"

    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "video"

    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1841
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mms/part/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1843
    cmp-long v2, v12, v8

    if-nez v2, :cond_1

    .line 1844
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/lit8 v14, v2, -0x1

    goto :goto_0

    .line 1851
    .end local v11    # "foundtype":Ljava/lang/String;
    .end local v12    # "foundId":J
    :cond_3
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1854
    return v14

    .line 1851
    :catchall_0
    move-exception v2

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public static getKNOXVersion(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2170
    const/4 v0, 0x0

    .line 2171
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "isKnoxMode"

    invoke-static {p0, v1}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2173
    const-string v1, "2.0"

    const-string/jumbo v2, "version"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2174
    const/4 v1, 0x2

    .line 2179
    :goto_0
    return v1

    .line 2175
    :cond_0
    const-string v1, "1.0"

    const-string/jumbo v2, "version"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2176
    const/4 v1, 0x1

    goto :goto_0

    .line 2179
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 1455
    const-string/jumbo v7, "window"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    .line 1456
    .local v6, "wm":Landroid/view/WindowManager;
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1457
    .local v0, "d":Landroid/view/Display;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1458
    .local v4, "rect":Landroid/graphics/Rect;
    const/4 v5, 0x0

    .local v5, "width":I
    const/4 v2, 0x0

    .line 1461
    .local v2, "height":I
    :try_start_0
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1462
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v3}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 1463
    iget v5, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1464
    iget v2, v3, Landroid/util/DisplayMetrics;->heightPixels:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1469
    .end local v3    # "metrics":Landroid/util/DisplayMetrics;
    :goto_0
    invoke-virtual {v4, v8, v8, v5, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1470
    return-object v4

    .line 1465
    :catch_0
    move-exception v1

    .line 1466
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getMidFromMmsList(Landroid/content/Context;Landroid/net/Uri;)J
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "partUri"    # Landroid/net/Uri;

    .prologue
    .line 1800
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "mid"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "ct"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 1801
    .local v2, "projection":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 1802
    .local v8, "cursorCurrent":Landroid/database/Cursor;
    const-wide/16 v6, -0x1

    .line 1805
    .local v6, "currentMid":J
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1806
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1807
    const-string v0, "mid"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    .line 1810
    :cond_0
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1812
    return-wide v6

    .line 1810
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getMultiPickMaxCount(Landroid/app/Activity;)I
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2645
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2646
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "pick-max-item"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getParentPathOnFileSystem(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/String;
    .locals 4
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 817
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 818
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 820
    .local v0, "filePath":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 821
    .local v1, "idxOfslash":I
    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 822
    const/4 v2, 0x0

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 826
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "idxOfslash":I
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static getPlaySpeed(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 1291
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsBurstShotPlaySpeedInited:Z

    if-nez v1, :cond_0

    .line 1292
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1293
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "burstshot-playspeed"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mBurstShotPlaySpeed:I

    .line 1294
    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsBurstShotPlaySpeedInited:Z

    .line 1296
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    sget v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mBurstShotPlaySpeed:I

    return v1
.end method

.method public static getResizedIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 1595
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 1596
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const-string v9, "activity"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1597
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconDensity()I

    move-result v5

    .line 1599
    .local v5, "iconDpi":I
    const/4 v3, 0x0

    .line 1600
    .local v3, "icon":Landroid/graphics/drawable/Drawable;
    if-nez p1, :cond_0

    .line 1601
    const-string v9, "GalleryUtils"

    const-string v10, "info is null at getResizedIcon"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 1615
    .end local v3    # "icon":Landroid/graphics/drawable/Drawable;
    .local v4, "icon":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v4

    .line 1606
    .end local v4    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "icon":Landroid/graphics/drawable/Drawable;
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    move-result v6

    .line 1607
    .local v6, "iconId":I
    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 1608
    .local v1, "ci":Landroid/content/pm/ComponentInfo;
    iget-object v9, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v7, v9}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v8

    .line 1609
    .local v8, "res":Landroid/content/res/Resources;
    invoke-virtual {v8, v6, v5}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .end local v1    # "ci":Landroid/content/pm/ComponentInfo;
    .end local v6    # "iconId":I
    .end local v8    # "res":Landroid/content/res/Resources;
    :goto_1
    move-object v4, v3

    .line 1615
    .end local v3    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v4    # "icon":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 1610
    .end local v4    # "icon":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "icon":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    .line 1611
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 1612
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v2

    .line 1613
    .local v2, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v2}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getSDCardRemovedIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 1552
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1553
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1554
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1555
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1556
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1557
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1558
    return-object v0
.end method

.method public static getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1305
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 1306
    .local v2, "outSize":Landroid/graphics/Point;
    const-string/jumbo v5, "window"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 1308
    .local v4, "wm":Landroid/view/WindowManager;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1310
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v5, :cond_3

    instance-of v5, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-nez v5, :cond_0

    instance-of v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    if-eqz v5, :cond_3

    .line 1311
    :cond_0
    const/4 v1, 0x0

    .line 1312
    .local v1, "mw":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    instance-of v5, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v5, :cond_4

    .line 1314
    :try_start_0
    check-cast p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1322
    :cond_1
    :goto_0
    if-eqz v1, :cond_3

    .line 1323
    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v3

    .line 1324
    .local v3, "size":Landroid/graphics/Rect;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    iget v6, v2, Landroid/graphics/Point;->x:I

    if-lt v5, v6, :cond_2

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget v6, v2, Landroid/graphics/Point;->y:I

    if-ge v5, v6, :cond_3

    .line 1325
    :cond_2
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    iput v5, v2, Landroid/graphics/Point;->x:I

    .line 1326
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1327
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/lit8 v5, v5, -0x28

    iput v5, v2, Landroid/graphics/Point;->y:I

    .line 1334
    .end local v1    # "mw":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .end local v3    # "size":Landroid/graphics/Rect;
    :cond_3
    :goto_1
    return-object v2

    .line 1315
    .restart local v1    # "mw":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :catch_0
    move-exception v0

    .line 1316
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 1319
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_4
    instance-of v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    if-eqz v5, :cond_1

    .line 1320
    check-cast p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    goto :goto_0

    .line 1329
    .restart local v3    # "size":Landroid/graphics/Rect;
    :cond_5
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    iput v5, v2, Landroid/graphics/Point;->y:I

    goto :goto_1
.end method

.method public static getSecretBoxPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2107
    const-string v0, "/storage/Private"

    .line 2109
    .local v0, "scretBoxPath":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2113
    :goto_0
    return-object v0

    .line 2110
    :catch_0
    move-exception v1

    .line 2111
    .local v1, "t":Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSelectionModePrompt(I)I
    .locals 1
    .param p0, "typeBits"    # I

    .prologue
    .line 606
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_1

    .line 607
    and-int/lit8 v0, p0, 0x1

    if-nez v0, :cond_0

    const v0, 0x7f0e038b

    .line 611
    :goto_0
    return v0

    .line 607
    :cond_0
    const v0, 0x7f0e003a

    goto :goto_0

    .line 611
    :cond_1
    const v0, 0x7f0e038a

    goto :goto_0
.end method

.method public static getStorageUsageRate()F
    .locals 6

    .prologue
    .line 1114
    new-instance v1, Landroid/os/StatFs;

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->mStorageDirectory:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1115
    .local v1, "stat":Landroid/os/StatFs;
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1}, Landroid/os/StatFs;->getFreeBytes()J

    move-result-wide v4

    long-to-float v3, v4

    invoke-virtual {v1}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v4

    long-to-float v4, v4

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 1116
    .local v0, "rate":Ljava/lang/Float;
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    return v2
.end method

.method public static getString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringId"    # I

    .prologue
    .line 1300
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1301
    .local v0, "s":Ljava/lang/String;
    return-object v0
.end method

.method public static getVideoDuration(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 2025
    const/4 v0, 0x0

    .line 2026
    .local v0, "duration":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 2027
    const/4 v3, 0x0

    .line 2046
    :goto_0
    return-object v3

    .line 2030
    :cond_0
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 2032
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v2, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 2033
    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2040
    :try_start_1
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2041
    const/4 v2, 0x0

    :goto_1
    move-object v3, v0

    .line 2046
    goto :goto_0

    .line 2042
    :catch_0
    move-exception v1

    .line 2043
    .local v1, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 2034
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v1

    .line 2035
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2040
    :try_start_3
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2041
    const/4 v2, 0x0

    goto :goto_1

    .line 2042
    :catch_2
    move-exception v1

    .line 2043
    .local v1, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 2036
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catch_3
    move-exception v1

    .line 2037
    .restart local v1    # "ex":Ljava/lang/RuntimeException;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2040
    :try_start_5
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4

    .line 2041
    const/4 v2, 0x0

    goto :goto_1

    .line 2042
    :catch_4
    move-exception v1

    .line 2043
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 2039
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    .line 2040
    :try_start_6
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_5

    .line 2041
    const/4 v2, 0x0

    .line 2044
    :goto_2
    throw v3

    .line 2042
    :catch_5
    move-exception v1

    .line 2043
    .restart local v1    # "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_2
.end method

.method public static hasSaveAbleSDCardStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z
    .locals 10
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1098
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    sget-object v6, Lcom/sec/android/gallery3d/util/MediaSetUtils;->REMOVABLE_SD_DIR_PATH:Ljava/lang/String;

    invoke-direct {v1, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1099
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v8, v8

    mul-long v2, v6, v8

    .line 1100
    .local v2, "remaining":J
    const-wide/32 v6, 0x1400000

    cmp-long v6, v2, v6

    if-gez v6, :cond_0

    .line 1101
    const-string v5, "GalleryUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "context = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / checkLowStorage "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/16 v8, 0x400

    div-long v8, v2, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "k"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    invoke-interface {p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e0139

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1109
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v2    # "remaining":J
    :goto_0
    return v4

    .restart local v1    # "stat":Landroid/os/StatFs;
    .restart local v2    # "remaining":J
    :cond_0
    move v4, v5

    .line 1105
    goto :goto_0

    .line 1107
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v2    # "remaining":J
    :catch_0
    move-exception v0

    .line 1108
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " checkLowStorageforMedia() for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/gallery3d/util/GalleryUtils;->mStorageDirectory:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static hasSaveAbleStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z
    .locals 10
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1081
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    sget-object v6, Lcom/sec/android/gallery3d/util/GalleryUtils;->mStorageDirectory:Ljava/lang/String;

    invoke-direct {v1, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1082
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v8, v8

    mul-long v2, v6, v8

    .line 1083
    .local v2, "remaining":J
    const-wide/32 v6, 0x1400000

    cmp-long v6, v2, v6

    if-gez v6, :cond_0

    .line 1084
    const-string v5, "GalleryUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "context = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / checkLowStorage "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/16 v8, 0x400

    div-long v8, v2, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "k"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    invoke-interface {p0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e0139

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1092
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v2    # "remaining":J
    :goto_0
    return v4

    .restart local v1    # "stat":Landroid/os/StatFs;
    .restart local v2    # "remaining":J
    :cond_0
    move v4, v5

    .line 1088
    goto :goto_0

    .line 1090
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v2    # "remaining":J
    :catch_0
    move-exception v0

    .line 1091
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " checkLowStorageforMedia() for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/gallery3d/util/GalleryUtils;->mStorageDirectory:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static hasSpaceForSize(J)Z
    .locals 10
    .param p0, "size"    # J

    .prologue
    const/4 v4, 0x0

    .line 615
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    .line 616
    .local v3, "state":Ljava/lang/String;
    const-string v5, "mounted"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 627
    :cond_0
    :goto_0
    return v4

    .line 620
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 622
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 623
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v8, v5

    mul-long/2addr v6, v8

    cmp-long v5, v6, p0

    if-lez v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    .line 624
    .end local v2    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v0

    .line 625
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "GalleryUtils"

    const-string v6, "Fail to access external storage"

    invoke-static {v5, v6, v0}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static hideAndReleaseSpinner(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 769
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->getInstance(Landroid/app/Activity;)Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->hideAndRelease()V

    .line 770
    return-void
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 259
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v3, "window"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 261
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 262
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    sput v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->sPixelDensity:F

    .line 263
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 264
    .local v1, "r":Landroid/content/res/Resources;
    const v3, 0x7f0b000f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->setPlaceholderColor(I)V

    .line 266
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->initializeThumbnailSizes(Landroid/util/DisplayMetrics;Landroid/content/res/Resources;)V

    .line 267
    return-void
.end method

.method private static initializeThumbnailSizes(Landroid/util/DisplayMetrics;Landroid/content/res/Resources;)V
    .locals 3
    .param p0, "metrics"    # Landroid/util/DisplayMetrics;
    .param p1, "r"    # Landroid/content/res/Resources;

    .prologue
    .line 270
    iget v1, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 273
    .local v0, "maxPixels":I
    iget v1, p0, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->setThumbnailSizes(IF)V

    .line 274
    div-int/lit8 v1, v0, 0x2

    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->setMaxSide(I)V

    .line 275
    iget v1, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/TileImageView;->setDisplaySize(II)V

    .line 276
    return-void
.end method

.method public static insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 6
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2372
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCheckVersionOfContextProvider:Z

    if-nez v4, :cond_0

    .line 2390
    :goto_0
    return-void

    .line 2375
    :cond_0
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2380
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2381
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2382
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    invoke-virtual {v2, v4, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2383
    const-string v4, "feature"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2385
    const-string v4, "GalleryUtils"

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2386
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 2387
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "GalleryUtils"

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2388
    const-string v4, "GalleryUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 2395
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mCheckVersionOfContextProvider:Z

    if-nez v3, :cond_0

    .line 2419
    :goto_0
    return-void

    .line 2400
    :cond_0
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2401
    .local v2, "row":Landroid/content/ContentValues;
    const-string v3, "app_id"

    invoke-virtual {v2, v3, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2402
    const-string v3, "feature"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403
    const-string/jumbo v3, "value"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2407
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v3, "com.samsung.android.providers.context.log.action.REPORT_APP_STATUS_SURVEY"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2408
    const-string v3, "data"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2410
    const-string v3, "com.samsung.android.providers.context"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2412
    invoke-virtual {p3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2414
    const-string v3, "GalleryUtils"

    const-string v4, "ContextProvider insertion operation is performed."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2415
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 2416
    .local v1, "ex":Ljava/lang/Exception;
    const-string v3, "GalleryUtils"

    const-string v4, "Error while using the ContextProvider"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2417
    const-string v3, "GalleryUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static intColorToFloatARGBArray(I)[F
    .locals 4
    .param p0, "from"    # I

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 279
    const/4 v0, 0x4

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    return-object v0
.end method

.method public static isAlbumPickMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2641
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "album-pick"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isAnyCameraAvailable(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 425
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v3

    .line 426
    .local v3, "version":I
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 427
    .local v2, "prefs":Landroid/content/SharedPreferences;
    const-string v6, "camera-update"

    invoke-interface {v2, v6, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-eq v6, v3, :cond_1

    .line 428
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 429
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.media.action.STILL_IMAGE_CAMERA"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 431
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "camera-update"

    invoke-interface {v6, v7, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "has-camera"

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    move v4, v5

    :cond_0
    invoke-interface {v6, v7, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 435
    .end local v0    # "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_1
    const-string v4, "has-camera"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    return v4
.end method

.method public static isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    .line 2586
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGalleryWidget(Landroid/content/Context;)Z

    move-result v0

    .line 2587
    .local v0, "fromGalleryWidget":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2588
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x20

    and-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 2589
    if-eqz v0, :cond_1

    .line 2598
    :cond_0
    :goto_0
    return v1

    .line 2592
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x4

    and-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    .line 2593
    if-eqz v0, :cond_0

    .line 2596
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isBurstShot(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p0, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 2360
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/LocalImage;->isBurstShotImage()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCallWindow(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1351
    const/4 v1, 0x0

    .line 1352
    .local v1, "mIsCallWindow":Z
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 1353
    .local v2, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 1354
    .local v0, "callState":I
    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 1355
    const/4 v1, 0x1

    .line 1356
    :cond_0
    return v1
.end method

.method public static isCameraAvailable(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 439
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCameraAvailableInitialized:Z

    if-eqz v5, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCameraAvailable:Z

    .line 445
    :goto_0
    return v3

    .line 440
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 441
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/IntentHelper;->getCameraIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 442
    .local v1, "cameraIntent":Landroid/content/Intent;
    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 443
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCameraAvailableInitialized:Z

    .line 444
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCameraAvailable:Z

    .line 445
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCameraAvailable:Z

    goto :goto_0

    :cond_1
    move v3, v4

    .line 444
    goto :goto_1
.end method

.method public static isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2547
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2548
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "from-Camera"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryActivity;->APP_CAMERA:Ljava/lang/String;

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryActivity;->FACE_START_APP:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v0, v2

    .line 2550
    .local v0, "fromCamera":Z
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromLockscreen(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecureKeyguardLocked(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    return v2

    .end local v0    # "fromCamera":Z
    :cond_1
    move v0, v3

    .line 2548
    goto :goto_0

    .restart local v0    # "fromCamera":Z
    :cond_2
    move v2, v3

    .line 2550
    goto :goto_1
.end method

.method public static isCinePic(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 2
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 790
    if-eqz p0, :cond_0

    const-wide/16 v0, 0x20

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 791
    const/4 v0, 0x1

    .line 793
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isConnetedSideSync(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2057
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKMSRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPSSRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPSSRunningTablets(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2058
    :cond_0
    const/4 v0, 0x1

    .line 2060
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isContactDBAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1704
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public static isContactDBAvailable(Landroid/content/Context;Z)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bForce"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 1707
    sget-boolean v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsContactDBAvailable:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    move v8, v10

    .line 1737
    :goto_0
    return v8

    .line 1710
    :cond_0
    if-nez p0, :cond_1

    move v8, v10

    .line 1711
    goto :goto_0

    .line 1713
    :cond_1
    const/4 v8, 0x1

    .line 1714
    .local v8, "result":Z
    const/4 v9, 0x0

    .line 1715
    .local v9, "status":I
    const/4 v6, 0x0

    .line 1717
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$ProviderStatus;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "status"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1721
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1722
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1727
    :cond_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1729
    :goto_1
    const/4 v0, 0x3

    if-ne v9, v0, :cond_3

    .line 1730
    const/4 v8, 0x0

    .line 1731
    sput-boolean v11, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsContactDBAvailable:Z

    .line 1732
    const-string v0, "GalleryUtils"

    const-string v1, "checkContactDBLocaleState = CHANGING"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1724
    :catch_0
    move-exception v7

    .line 1725
    .local v7, "e":Landroid/database/CursorWindowAllocationException;
    :try_start_1
    invoke-virtual {v7}, Landroid/database/CursorWindowAllocationException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1727
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Landroid/database/CursorWindowAllocationException;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 1734
    :cond_3
    const/4 v8, 0x1

    .line 1735
    sput-boolean v10, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsContactDBAvailable:Z

    goto :goto_0
.end method

.method public static isCoverMode(Landroid/app/Activity;)Z
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2650
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2651
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "covermode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isEditorAvailable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 383
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v5

    .line 385
    .local v5, "version":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "editor-update-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 386
    .local v4, "updateKey":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "has-editor-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 388
    .local v0, "hasKey":Ljava/lang/String;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 389
    .local v3, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-eq v8, v5, :cond_1

    .line 390
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 391
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.EDIT"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v2, v8, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 393
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    move v6, v7

    :cond_0
    invoke-interface {v8, v0, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 398
    .end local v1    # "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v2    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_1
    invoke-interface {v3, v0, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    return v6
.end method

.method public static isEmailAvailable(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 1688
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.SENDTO"

    const-string v5, "mailto:"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1689
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1690
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 1691
    .local v0, "apk":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1692
    const/4 v3, 0x1

    .line 1694
    :cond_0
    return v3
.end method

.method public static isEmergencyMode(Landroid/app/Activity;)Z
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2365
    invoke-virtual {p0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v0

    .line 2366
    .local v0, "em":Lcom/sec/android/emergencymode/EmergencyManager;
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/EmergencyManager;->isEmergencyMode()Z

    move-result v1

    return v1
.end method

.method public static isEthernetConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1394
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v3, :cond_1

    .line 1404
    :cond_0
    :goto_0
    return v2

    .line 1397
    :cond_1
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1398
    .local v1, "manager":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_0

    .line 1401
    const/16 v3, 0x9

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1402
    .local v0, "ethernet":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 1404
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    goto :goto_0
.end method

.method public static isEventViewMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2422
    const/4 v0, 0x0

    .line 2423
    .local v0, "isInViewMode":Z
    instance-of v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v1, :cond_0

    move-object v1, p0

    .line 2424
    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    iget-boolean v0, v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsInViewMode:Z

    .line 2426
    :cond_0
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_1

    if-nez v0, :cond_1

    .line 2428
    const/4 v1, 0x1

    .line 2430
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isExist(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/Boolean;
    .locals 10
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 930
    const/4 v6, 0x0

    .line 932
    .local v6, "c":Landroid/database/Cursor;
    if-nez p1, :cond_0

    .line 933
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 956
    :goto_0
    return-object v0

    .line 935
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file:///"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 936
    new-instance v8, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 937
    .local v8, "filepath":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 938
    .end local v8    # "filepath":Ljava/io/File;
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://media"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 940
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 942
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 944
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 945
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 952
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 954
    .end local v2    # "projection":[Ljava/lang/String;
    :goto_1
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 947
    :catch_0
    move-exception v7

    .line 948
    .local v7, "e":Landroid/database/sqlite/SQLiteFullException;
    :try_start_1
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteFullException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 952
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 949
    .end local v7    # "e":Landroid/database/sqlite/SQLiteFullException;
    :catch_1
    move-exception v7

    .line 950
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 952
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 956
    :cond_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public static isExploreByTouchEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1970
    const-string v1, "accessibility"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1972
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    return v1
.end method

.method public static isExternalDisplayAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 403
    const-string v4, "display"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 404
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    const-string v4, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v0, v4}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v1

    .line 407
    .local v1, "displays":[Landroid/view/Display;
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isHoverZoomModeOn(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 408
    array-length v4, v1

    if-le v4, v2, :cond_1

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSideSyncPresentationOff(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 410
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    .line 408
    goto :goto_0

    .line 410
    :cond_2
    array-length v4, v1

    if-ne v4, v2, :cond_3

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSideSyncPresentationOff(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method public static isFaceTagAvailable(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1251
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabledInited:Z

    if-nez v1, :cond_0

    .line 1252
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1253
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "facetag-switch"

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mFaceTagDefaultValue:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabled:Z

    .line 1254
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabledInited:Z

    .line 1256
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabled:Z

    return v1
.end method

.method public static isFromGalleryWidget(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 2603
    :try_start_0
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo-pick"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2606
    .local v0, "e":Ljava/lang/NullPointerException;
    :goto_0
    return v1

    .line 2605
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_0
    move-exception v0

    .line 2606
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isFromGifMaker(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 2621
    :try_start_0
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo-pick-gifmaker"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2624
    .local v0, "e":Ljava/lang/NullPointerException;
    :goto_0
    return v1

    .line 2623
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_0
    move-exception v0

    .line 2624
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isFromInsideGallery(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 2612
    :try_start_0
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "pick-from-gallery"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2615
    .local v0, "e":Ljava/lang/NullPointerException;
    :goto_0
    return v1

    .line 2614
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_0
    move-exception v0

    .line 2615
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isFromLockscreen(Landroid/app/Activity;)Z
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2554
    const/4 v1, 0x0

    .line 2556
    .local v1, "isFromLocksceen":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "createdByLockscreen"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2560
    :goto_0
    return v1

    .line 2557
    :catch_0
    move-exception v0

    .line 2558
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isFullScreen(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 989
    const/4 v0, 0x1

    return v0
.end method

.method public static isGooglePlayServicesAvailable(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 2718
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v2, :cond_0

    .line 2729
    :goto_0
    return v1

    .line 2721
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 2724
    .local v0, "resultCode":I
    if-nez v0, :cond_1

    .line 2725
    const-string v2, "GalleryUtils"

    const-string v3, "google play sevice available!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2728
    :cond_1
    const-string v1, "GalleryUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "google play sevice unavailable : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2729
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isGuestMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2434
    const-string/jumbo v2, "user"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    .line 2435
    .local v1, "mUm":Landroid/os/UserManager;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    .line 2436
    .local v0, "info":Landroid/content/pm/UserInfo;
    const/4 v2, 0x0

    return v2
.end method

.method public static isHideBlockedItemAtPhotoViewState(Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 1
    .param p0, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 2285
    if-eqz p0, :cond_1

    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2287
    :cond_0
    const/4 v0, 0x1

    .line 2289
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isHoverZoomModeOn(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 414
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accessibility_magnifier"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isHoveringOn(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2577
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHovering(Landroid/content/Context;)Z

    move-result v2

    .line 2578
    .local v2, "isPenHoveringOn":Z
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getFingerAirView(Landroid/content/Context;)Z

    move-result v0

    .line 2579
    .local v0, "isFingerAirViewOn":Z
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getMouseHovering(Landroid/content/Context;)Z

    move-result v1

    .line 2580
    .local v1, "isMouseHoveringOn":Z
    if-nez v2, :cond_0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isIncomingCall(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2733
    const/4 v1, 0x0

    .line 2734
    .local v1, "mIsIncomingCall":Z
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 2735
    .local v2, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 2736
    .local v0, "callState":I
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 2737
    const/4 v1, 0x1

    .line 2738
    :cond_0
    return v1
.end method

.method public static isIntentRegistered(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1009
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1010
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    const/high16 v2, 0x10000

    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 1012
    .local v1, "receiverList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isJpeg(Ljava/lang/String;)Z
    .locals 2
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 962
    if-nez p0, :cond_1

    .line 963
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "image/jpeg"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "image/jpg"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isKMSRunning(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2065
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2067
    .local v0, "am":Landroid/app/ActivityManager;
    if-eqz v0, :cond_2

    .line 2068
    const v5, 0x7fffffff

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v4

    .line 2069
    .local v4, "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v4, :cond_2

    .line 2070
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 2071
    .local v3, "serviceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v5, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 2072
    .local v2, "mServiceName":Ljava/lang/String;
    const-string v5, "com.sec.android.sidesync.kms.sink.service.SideSyncServerService"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "com.sec.android.sidesync.kms.source.service.SideSyncService"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2074
    :cond_1
    const/4 v5, 0x1

    .line 2079
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mServiceName":Ljava/lang/String;
    .end local v3    # "serviceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    .end local v4    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :goto_0
    return v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static isKNOX(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2121
    const/4 v0, 0x0

    .line 2122
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "isKnoxMode"

    invoke-static {p0, v1}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2123
    const-string v1, "2.0"

    const-string/jumbo v2, "version"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2124
    const-string/jumbo v1, "true"

    const-string v2, "isKnoxMode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2125
    const/4 v1, 0x1

    .line 2130
    :goto_0
    return v1

    .line 2127
    :cond_0
    const-string v1, "1.0"

    const-string/jumbo v2, "version"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2128
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sec_container_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 2130
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isKNOX2(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2134
    const/4 v0, 0x0

    .line 2135
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 2137
    const-string v1, "2.0"

    const-string/jumbo v2, "version"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2138
    const/4 v1, 0x1

    .line 2141
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isKNOXInstallMode(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 2153
    const/4 v0, 0x0

    .line 2154
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 2156
    const-string v2, "2.0"

    const-string/jumbo v3, "version"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2157
    const-string/jumbo v2, "true"

    const-string v3, "isSupportMoveTo"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2158
    const/4 v1, 0x1

    .line 2166
    :cond_0
    :goto_0
    return v1

    .line 2162
    :cond_1
    const-string v2, "1.0"

    const-string/jumbo v3, "version"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2163
    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->sKnoxInstaller:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v2, :cond_0

    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->sKnoxInstaller:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v1}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->isKNOXFileRelayAvailable()Z

    move-result v1

    goto :goto_0
.end method

.method public static isKnoxEnabledStateOfKnox2(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2145
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX2(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2146
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 2147
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "true"

    const-string v2, "isKnoxMode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2149
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLandScapeModel(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1486
    const/4 v1, 0x0

    .line 1487
    .local v1, "result":Z
    const-string/jumbo v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 1488
    .local v3, "wm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1489
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 1490
    .local v2, "rotation":I
    packed-switch v2, :pswitch_data_0

    .line 1502
    :goto_0
    return v1

    .line 1493
    :pswitch_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v6

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v7

    if-ge v6, v7, :cond_0

    move v1, v4

    .line 1494
    :goto_1
    goto :goto_0

    :cond_0
    move v1, v5

    .line 1493
    goto :goto_1

    .line 1497
    :pswitch_1
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v6

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v7

    if-ge v6, v7, :cond_1

    move v1, v5

    .line 1498
    :goto_2
    goto :goto_0

    :cond_1
    move v1, v4

    .line 1497
    goto :goto_2

    .line 1490
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isLessEqualLwhvga(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 680
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    const v1, 0x2a300

    if-gt v0, v1, :cond_0

    .line 681
    const/4 v0, 0x1

    .line 683
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLonglifeModeOn(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2247
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsLognlifemodeEnabled:Z

    if-eqz v2, :cond_0

    .line 2248
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "longlife_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2252
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isMiniModeServiceRunning(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1658
    :try_start_0
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1660
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v3, 0x64

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 1662
    .local v2, "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v2, :cond_1

    .line 1663
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1664
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiniVideoPlayerService"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 1665
    const/4 v3, 0x1

    .line 1671
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "i":I
    .end local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :goto_1
    return v3

    .line 1663
    .restart local v0    # "am":Landroid/app/ActivityManager;
    .restart local v1    # "i":I
    .restart local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1668
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "i":I
    .end local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :catch_0
    move-exception v3

    .line 1671
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static isMmsItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p0, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x0

    .line 1364
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 1365
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_1

    .line 1378
    :cond_0
    :goto_0
    return v2

    .line 1369
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1370
    .local v1, "uriString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1374
    const-string v3, "content://mms/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1375
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isMmsUri(Ljava/lang/String;)Z
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 1437
    const-string v0, "content://mms/part/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isMultiPickMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2636
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 2637
    .local v0, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMultiWindow()Z
    .locals 1

    .prologue
    .line 1347
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsMultiWindow:Z

    return v0
.end method

.method public static isNearbyAvailable(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 8
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 968
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    const/4 v7, 0x0

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 969
    .local v3, "intent":Landroid/content/Intent;
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 970
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v3, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 972
    .local v2, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 973
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v6, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 974
    .local v4, "packageName":Ljava/lang/String;
    const-string v6, "com.sec.android.app.dlna"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 975
    const/4 v5, 0x1

    .line 978
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_1
    return v5
.end method

.method public static isNetworkConnected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1419
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v1

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v2

    or-int v0, v1, v2

    .line 1421
    .local v0, "isConnected":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v1, :cond_0

    .line 1422
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEthernetConnected(Landroid/content/Context;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1425
    :cond_0
    return v0
.end method

.method public static isOverHD(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    .line 715
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsOverHDInitialized:Z

    if-nez v3, :cond_0

    .line 716
    sput-boolean v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsOverHDInitialized:Z

    .line 717
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v1

    .line 718
    .local v1, "LCDSize":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 719
    .local v0, "LCDHeight":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 720
    .local v2, "LCDWidth":I
    mul-int v3, v0, v2

    const v4, 0xe1000

    if-lt v3, v4, :cond_1

    .line 721
    sput-boolean v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsOverHD:Z

    .line 726
    .end local v0    # "LCDHeight":I
    .end local v1    # "LCDSize":Landroid/graphics/Rect;
    .end local v2    # "LCDWidth":I
    :cond_0
    :goto_0
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsOverHD:Z

    return v3

    .line 723
    .restart local v0    # "LCDHeight":I
    .restart local v1    # "LCDSize":Landroid/graphics/Rect;
    .restart local v2    # "LCDWidth":I
    :cond_1
    const/4 v3, 0x0

    sput-boolean v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsOverHD:Z

    goto :goto_0
.end method

.method public static isOverWQHD(I)Z
    .locals 1
    .param p0, "size"    # I

    .prologue
    .line 734
    const/16 v0, 0xa00

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPSSRunning(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2084
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "sidesync_source_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2087
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isPSSRunningTablets(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2092
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "sidesync_sink_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 2095
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isPackageAvailable(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "updateKey"    # Ljava/lang/String;
    .param p3, "hasKey"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2265
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v3

    .line 2267
    .local v3, "version":I
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2268
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v2, p2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-eq v6, v3, :cond_3

    .line 2269
    const/4 v1, 0x0

    .line 2271
    .local v1, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2273
    if-eqz v1, :cond_1

    .line 2278
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, p2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    if-eqz v1, :cond_0

    move v4, v5

    :cond_0
    invoke-interface {v6, p3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2281
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return v5

    .line 2278
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, p2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    if-eqz v1, :cond_2

    move v4, v5

    :cond_2
    invoke-interface {v6, p3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2281
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_3
    :goto_1
    invoke-interface {v2, p3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    goto :goto_0

    .line 2275
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 2276
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string v6, "GalleryUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is not exist. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2278
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, p2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    if-eqz v1, :cond_4

    move v4, v5

    :cond_4
    invoke-interface {v6, p3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v6

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7, p2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    if-eqz v1, :cond_5

    :goto_2
    invoke-interface {v7, p3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    throw v6

    :cond_5
    move v5, v4

    goto :goto_2
.end method

.method public static isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 7
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 636
    const/4 v2, 0x0

    .line 637
    .local v2, "w":F
    const/4 v0, 0x0

    .line 638
    .local v0, "h":F
    if-nez p0, :cond_1

    .line 660
    :cond_0
    :goto_0
    return v3

    .line 640
    :cond_1
    instance-of v4, p0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_3

    move-object v1, p0

    .line 641
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 642
    .local v1, "image":Lcom/sec/android/gallery3d/data/LocalImage;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getBucketId()I

    move-result v4

    sget v5, Lcom/sec/android/gallery3d/util/MediaSetUtils;->PHOTO_SIGNATURE_BUCKET_ID:I

    if-eq v4, v5, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getBucketId()I

    move-result v4

    sget v5, Lcom/sec/android/gallery3d/util/MediaSetUtils;->PHOTO_SIGNATURE_BUCKET_ID_2:I

    if-eq v4, v5, :cond_0

    .line 648
    iget-boolean v4, v1, Lcom/sec/android/gallery3d/data/LocalImage;->isSoundScene:Z

    if-nez v4, :cond_0

    .line 650
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcWidth()I

    move-result v4

    int-to-float v2, v4

    .line 651
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcHeight()I

    move-result v4

    int-to-float v0, v4

    .line 657
    .end local v1    # "image":Lcom/sec/android/gallery3d/data/LocalImage;
    :cond_2
    :goto_1
    cmpl-float v4, v2, v6

    if-eqz v4, :cond_0

    cmpl-float v4, v0, v6

    if-eqz v4, :cond_0

    .line 660
    cmpl-float v4, v2, v0

    if-lez v4, :cond_5

    div-float v4, v2, v0

    :goto_2
    const v5, 0x402ccccd    # 2.7f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    .line 652
    :cond_3
    instance-of v4, p0, Lcom/sec/android/gallery3d/data/MtpImage;

    if-nez v4, :cond_4

    instance-of v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v4, :cond_4

    instance-of v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v4, :cond_2

    .line 654
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v4

    int-to-float v2, v4

    .line 655
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v4

    int-to-float v0, v4

    goto :goto_1

    .line 660
    :cond_5
    div-float v4, v0, v2

    goto :goto_2
.end method

.method public static isPhone(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 982
    new-instance v0, Landroid/telephony/TelephonyManager;

    invoke-direct {v0, p0}, Landroid/telephony/TelephonyManager;-><init>(Landroid/content/Context;)V

    .line 983
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isPickMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2630
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 2631
    .local v0, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isQHD(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 701
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    const v1, 0x7e900

    if-ne v0, v1, :cond_0

    .line 702
    const/4 v0, 0x1

    .line 704
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isQvga(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 687
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    const v1, 0x12c00

    if-ne v0, v1, :cond_0

    .line 688
    const/4 v0, 0x1

    .line 690
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isReadyPrivateMode(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2230
    const/4 v1, 0x0

    .line 2233
    .local v1, "isReady":Z
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isReady(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 2242
    :goto_0
    return v1

    .line 2234
    :catch_0
    move-exception v0

    .line 2235
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 2236
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 2237
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 2238
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_2
    move-exception v0

    .line 2239
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isSamsungMakerFilter(Lcom/sec/android/gallery3d/data/MediaDetails;)Z
    .locals 3
    .param p0, "mediaDetails"    # Lcom/sec/android/gallery3d/data/MediaDetails;

    .prologue
    .line 1769
    :try_start_0
    const-string v2, "SAMSUNG"

    const/16 v1, 0x64

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1772
    const/4 v1, 0x1

    .line 1777
    :goto_0
    return v1

    .line 1774
    :catch_0
    move-exception v0

    .line 1775
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "GalleryUtils"

    const-string v2, "isMakerFilter(), item is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1777
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSamsungMakerFilter(Ljava/lang/String;)Z
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1781
    if-nez p0, :cond_1

    .line 1782
    const/4 v3, 0x0

    .line 1795
    :cond_0
    :goto_0
    return v3

    .line 1784
    :cond_1
    const/4 v3, 0x0

    .line 1786
    .local v3, "result":Z
    :try_start_0
    new-instance v1, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 1787
    .local v1, "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V

    .line 1788
    sget v4, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_MAKE:I

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTagStringValue(I)Ljava/lang/String;

    move-result-object v2

    .line 1789
    .local v2, "maker":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1790
    const-string v4, "SAMSUNG"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 1792
    .end local v1    # "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v2    # "maker":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1793
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "GalleryUtils"

    const-string v5, "No TAG_MAKE : s"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isSecretDirMounted(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2206
    const/4 v1, 0x0

    .line 2208
    .local v1, "secretMode":Z
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 2216
    :goto_0
    const-string v2, "GalleryUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSecretDirMounted: secretMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2217
    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setSecretModeOn(Z)V

    .line 2218
    return-void

    .line 2209
    :catch_0
    move-exception v0

    .line 2210
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 2211
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 2212
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 2213
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_2
    move-exception v0

    .line 2214
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isSecretModeOn()Z
    .locals 1

    .prologue
    .line 2226
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsSecretModeOn:Z

    return v0
.end method

.method private static isSecureKeyguardLocked(Landroid/app/Activity;)Z
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2564
    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 2565
    .local v0, "kgm":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isShareNetWorkConnected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1430
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1431
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1432
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isSideSyncPresentationOff(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 418
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 419
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v2, "sidesync_source_presentation"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static isSignatureFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 2
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 2019
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoSignature:Z

    if-eqz v0, :cond_1

    instance-of v0, p0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getBucketId()I

    move-result v0

    sget v1, Lcom/sec/android/gallery3d/util/MediaSetUtils;->PHOTO_SIGNATURE_BUCKET_ID:I

    if-eq v0, v1, :cond_0

    check-cast p0, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage;->getBucketId()I

    move-result v0

    sget v1, Lcom/sec/android/gallery3d/util/MediaSetUtils;->PHOTO_SIGNATURE_BUCKET_ID_2:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSmsIntentRegistered(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1005
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSmsIntentRegistered(Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method public static isSmsIntentRegistered(Landroid/content/Context;Z)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "checkMms"    # Z

    .prologue
    .line 993
    if-eqz p1, :cond_0

    .line 994
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMmsEnabled:Z

    if-nez v1, :cond_0

    .line 995
    const/4 v1, 0x0

    .line 1001
    :goto_0
    return v1

    .line 999
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    const-string/jumbo v2, "smsto"

    const-string v3, ""

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1001
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isIntentRegistered(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isSupportDeleteFromMapView(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2441
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 2442
    .local v0, "stateManager":Lcom/sec/android/gallery3d/app/StateManager;
    const-class v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->hasStateClass(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTabletPickerMode(Landroid/content/Intent;)Z
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 2691
    if-nez p0, :cond_1

    .line 2713
    :cond_0
    :goto_0
    return v1

    .line 2694
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2698
    const-string v2, "set-as-wallpaper"

    invoke-virtual {p0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2702
    const-string v2, "senderIsVideoCall"

    invoke-virtual {p0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2706
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2707
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.PICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2711
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isTalkBackEnabled(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 1940
    const/16 v1, 0x3a

    .line 1941
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 1942
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    const-string v2, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    .line 1943
    .local v2, "TALKBACK_SERVICE_NAME":Ljava/lang/String;
    new-instance v7, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v9, 0x3a

    invoke-direct {v7, v9}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 1945
    .local v7, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    if-nez p0, :cond_1

    .line 1966
    :cond_0
    :goto_0
    return v8

    .line 1947
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "enabled_accessibility_services"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1949
    .local v6, "enabledServicesSetting":Ljava/lang/String;
    if-nez v6, :cond_2

    .line 1950
    const-string v6, ""

    .line 1953
    :cond_2
    move-object v3, v7

    .line 1955
    .local v3, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v3, v6}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 1957
    :cond_3
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1958
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v4

    .line 1959
    .local v4, "componentNameString":Ljava/lang/String;
    invoke-static {v4}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v5

    .line 1961
    .local v5, "enabledService":Landroid/content/ComponentName;
    if-eqz v5, :cond_3

    .line 1962
    const-string v9, "com.google.android.marvin.talkback"

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1963
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public static isUnderWvga(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 708
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    const v1, 0x5dc00

    if-ge v0, v1, :cond_0

    .line 709
    const/4 v0, 0x1

    .line 711
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidLocation(DD)Z
    .locals 4
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 471
    cmpl-double v0, p0, v2

    if-nez v0, :cond_0

    cmpl-double v0, p2, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isViewByAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1360
    invoke-static {}, Lcom/sec/android/gallery3d/app/FilterUtils;->isClustered()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWQHD(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 730
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    const v1, 0x384000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWifiConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1382
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1385
    .local v1, "manager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1386
    .local v2, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1389
    .end local v2    # "wifi":Landroid/net/NetworkInfo;
    :goto_0
    return v3

    .line 1387
    :catch_0
    move-exception v0

    .line 1388
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1389
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isWifiP2pConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1408
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1409
    .local v0, "manager":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 1415
    :cond_0
    :goto_0
    return v2

    .line 1412
    :cond_1
    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1413
    .local v1, "netInfoP2p":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    .line 1415
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    goto :goto_0
.end method

.method public static isWvga(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 694
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayPixels(Landroid/content/Context;)I

    move-result v0

    const v1, 0x5dc00

    if-ne v0, v1, :cond_0

    .line 695
    const/4 v0, 0x1

    .line 697
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static meterToPixel(F)I
    .locals 2
    .param p0, "meter"    # F

    .prologue
    .line 297
    const v0, 0x421d7ae1    # 39.37f

    mul-float/2addr v0, p0

    const/high16 v1, 0x43200000    # 160.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static needToHideTitle(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1514
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1515
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 1516
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1517
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v2, v3, :cond_0

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v2, v3

    const v3, 0x5dc00

    if-gt v2, v3, :cond_0

    .line 1519
    const/4 v2, 0x1

    .line 1521
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static notifyImageRotation(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1976
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v1, :cond_0

    .line 1977
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1978
    .local v0, "statusIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.gallery3d.IMAGE_ROTATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1979
    const-string v1, "filePath"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1980
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1982
    .end local v0    # "statusIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static panoramaPlayBoost(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2454
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePanoramaPlayBoost:Z

    if-eqz v0, :cond_0

    .line 2455
    const v0, 0x122870

    const/4 v1, 0x3

    const v2, 0x9c40

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setDVFSLock(Landroid/content/Context;III)V

    .line 2457
    :cond_0
    return-void
.end method

.method public static parseMimetypeFromHttpUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1562
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1563
    .local v0, "loweredUri":Ljava/lang/String;
    const-string v1, "jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1564
    :cond_0
    const-string v1, "image/jpeg"

    .line 1570
    :goto_0
    return-object v1

    .line 1565
    :cond_1
    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1566
    const-string v1, "image/png"

    goto :goto_0

    .line 1567
    :cond_2
    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1568
    const-string v1, "image/gif"

    goto :goto_0

    .line 1570
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static playSoundKeyClick(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1525
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1526
    .local v0, "audioManager":Landroid/media/AudioManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1527
    return-void
.end method

.method public static saveBitmapFile(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 10
    .param p0, "SavePath"    # Ljava/lang/String;
    .param p1, "FileName"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v7, 0x0

    .line 894
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 919
    :cond_0
    :goto_0
    return v7

    .line 897
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 898
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_2

    .line 899
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 901
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 902
    .local v5, "saveFile":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 903
    .local v6, "tempFile":Ljava/io/File;
    const/4 v2, 0x0

    .line 906
    .local v2, "out":Ljava/io/OutputStream;
    const/4 v4, 0x0

    .line 907
    .local v4, "result":Z
    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    move-result v4

    .line 908
    if-nez v4, :cond_3

    .line 909
    const-string v8, "GalleryUtils"

    const-string v9, "failed to create file : "

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    :cond_3
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 912
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x46

    invoke-virtual {p2, v8, v9, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 913
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 919
    const/4 v7, 0x1

    goto :goto_0

    .line 914
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v1

    .line 915
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 914
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public static final scanExternalStorage(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1216
    new-array v0, v2, [Ljava/lang/String;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1219
    .local v0, "paths":[Ljava/lang/String;
    new-array v1, v2, [Ljava/lang/String;

    const-string v2, "*/*"

    aput-object v2, v1, v3

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1222
    return-void
.end method

.method public static final scanImage(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1202
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "image/*"

    aput-object v2, v1, v3

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1207
    return-void
.end method

.method public static final scanImages(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "paths"    # [Ljava/lang/String;

    .prologue
    .line 1210
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "image/*"

    aput-object v2, v0, v1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1213
    return-void
.end method

.method public static final scanMedia(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1188
    new-array v0, v4, [Ljava/lang/String;

    aput-object p1, v0, v3

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "image/*"

    aput-object v2, v1, v3

    const-string/jumbo v2, "video/*"

    aput-object v2, v1, v4

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1193
    return-void
.end method

.method public static final scanMedias(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "paths"    # [Ljava/lang/String;

    .prologue
    .line 1196
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "image/*"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "video/*"

    aput-object v2, v0, v1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1199
    return-void
.end method

.method public static searchDirForPath(Ljava/io/File;I)Ljava/lang/String;
    .locals 7
    .param p0, "dir"    # Ljava/io/File;
    .param p1, "bucketId"    # I

    .prologue
    .line 527
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 528
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_3

    .line 529
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 530
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 531
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    .line 532
    .local v5, "path":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBucketId(Ljava/lang/String;)I

    move-result v6

    if-ne v6, p1, :cond_1

    .line 541
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "path":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v5

    .line 535
    .restart local v0    # "arr$":[Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v5    # "path":Ljava/lang/String;
    :cond_1
    invoke-static {v1, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->searchDirForPath(Ljava/io/File;I)Ljava/lang/String;

    move-result-object v5

    .line 536
    if-nez v5, :cond_0

    .line 529
    .end local v5    # "path":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 541
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static setDCIMName(Ljava/lang/String;)V
    .locals 0
    .param p0, "dcimName"    # Ljava/lang/String;

    .prologue
    .line 779
    sput-object p0, Lcom/sec/android/gallery3d/util/GalleryUtils;->DCIM_NAME:Ljava/lang/String;

    .line 780
    return-void
.end method

.method private static setDVFSLock(Landroid/content/Context;III)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "freqMin"    # I
    .param p2, "coreMin"    # I
    .param p3, "duration"    # I

    .prologue
    .line 2460
    if-lez p3, :cond_0

    const v1, 0x9c40

    if-le p3, v1, :cond_1

    .line 2480
    :cond_0
    :goto_0
    return-void

    .line 2463
    :cond_1
    const-string v1, "GalleryUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDVFSLock : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2465
    if-lez p1, :cond_2

    .line 2466
    new-instance v0, Landroid/os/DVFSHelper;

    const/16 v1, 0xc

    invoke-direct {v0, p0, v1}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 2467
    .local v0, "mCpuBooster":Landroid/os/DVFSHelper;
    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2468
    const-string v1, "CPU"

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 2469
    invoke-virtual {v0, p3}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 2473
    .end local v0    # "mCpuBooster":Landroid/os/DVFSHelper;
    :cond_2
    if-lez p2, :cond_0

    .line 2474
    new-instance v0, Landroid/os/DVFSHelper;

    const/16 v1, 0xe

    invoke-direct {v0, p0, v1}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 2475
    .restart local v0    # "mCpuBooster":Landroid/os/DVFSHelper;
    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2476
    const-string v1, "CORE_NUM"

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 2477
    invoke-virtual {v0, p3}, Landroid/os/DVFSHelper;->acquire(I)V

    goto :goto_0
.end method

.method public static setFaceTagStatus(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isOn"    # Z

    .prologue
    .line 1260
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1261
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "facetag-switch"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1262
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabledInited:Z

    .line 1263
    sput-boolean p1, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagEnabled:Z

    .line 1265
    :cond_0
    return-void
.end method

.method public static setFaceTagStatusForNormalMode(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isOn"    # Z

    .prologue
    .line 1268
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1269
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "facetag-switch-for-normal-mode"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1270
    return-void
.end method

.method public static setGalleryID(I)V
    .locals 0
    .param p0, "galleryId"    # I

    .prologue
    .line 2743
    sput p0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mGalleryId:I

    .line 2744
    return-void
.end method

.method public static setKNOXInstallMode(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "supportKnox"    # Z

    .prologue
    .line 2183
    move-object v0, p0

    .line 2184
    .local v0, "appContext":Landroid/content/Context;
    if-nez p1, :cond_0

    .line 2185
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->sKnoxInstaller:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 2203
    :goto_0
    return-void

    .line 2188
    :cond_0
    new-instance v1, Lcom/sec/knox/containeragent/ContainerInstallerManager;

    new-instance v2, Lcom/sec/android/gallery3d/util/GalleryUtils$2;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils$2;-><init>()V

    invoke-direct {v1, v0, v2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;-><init>(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    sput-object v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->sKnoxInstaller:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    goto :goto_0
.end method

.method public static setKNOXPath()V
    .locals 1

    .prologue
    .line 2117
    const-string v0, "/mnt_1/sdcard_1/Pictures"

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->KNOX_PATH:Ljava/lang/String;

    .line 2118
    return-void
.end method

.method public static setMultiWindow(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1338
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsMultiWindow:Z

    .line 1339
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    instance-of v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v0, :cond_0

    .line 1340
    check-cast p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsMultiWindow:Z

    .line 1342
    :cond_0
    return-void
.end method

.method public static setPlaySpeed(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "speed"    # I

    .prologue
    .line 1283
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1284
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "burstshot-playspeed"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1285
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsBurstShotPlaySpeedInited:Z

    .line 1286
    sput p1, Lcom/sec/android/gallery3d/util/GalleryUtils;->mBurstShotPlaySpeed:I

    .line 1288
    :cond_0
    return-void
.end method

.method public static setRenderThread()V
    .locals 1

    .prologue
    .line 317
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->sCurrentThread:Ljava/lang/Thread;

    .line 318
    return-void
.end method

.method public static setScanListener(Lcom/sec/android/gallery3d/util/GalleryUtils$ScanListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/sec/android/gallery3d/util/GalleryUtils$ScanListener;

    .prologue
    .line 1132
    sget-object v0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mScannerClient:Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils$ScannerClient;->setListener(Lcom/sec/android/gallery3d/util/GalleryUtils$ScanListener;)V

    .line 1133
    return-void
.end method

.method public static setSecretBox(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2100
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2104
    :goto_0
    return-void

    .line 2101
    :catch_0
    move-exception v0

    .line 2102
    .local v0, "t":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public static setSecretModeOn(Z)V
    .locals 3
    .param p0, "secretMode"    # Z

    .prologue
    .line 2221
    const-string v0, "GalleryUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSecretModeOn: boolean secretMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2222
    sput-boolean p0, Lcom/sec/android/gallery3d/util/GalleryUtils;->mIsSecretModeOn:Z

    .line 2223
    return-void
.end method

.method public static setSpinnerVisibility(Landroid/app/Activity;Z)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "visible"    # Z

    .prologue
    .line 765
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->getInstance(Landroid/app/Activity;)Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/util/SpinnerVisibilitySetter;->setSpinnerVisibility(Z)V

    .line 766
    return-void
.end method

.method public static setViewPointMatrix([FFFF)V
    .locals 4
    .param p0, "matrix"    # [F
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    const/4 v3, 0x0

    .line 510
    const/16 v0, 0x10

    const/4 v1, 0x0

    invoke-static {p0, v3, v0, v1}, Ljava/util/Arrays;->fill([FIIF)V

    .line 511
    const/4 v0, 0x5

    const/16 v1, 0xf

    neg-float v2, p3

    aput v2, p0, v1

    aput v2, p0, v0

    aput v2, p0, v3

    .line 512
    const/16 v0, 0x8

    aput p1, p0, v0

    .line 513
    const/16 v0, 0x9

    aput p2, p0, v0

    .line 514
    const/16 v0, 0xa

    const/16 v1, 0xb

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, p0, v1

    aput v2, p0, v0

    .line 515
    return-void
.end method

.method public static showOnMap(Landroid/content/Context;DD)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 487
    :try_start_0
    const-string v5, "http://maps.google.com/maps?f=q&q=(%f,%f)"

    invoke-static {v5, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatLatitudeLongitude(Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object v3

    .line 489
    .local v3, "uri":Ljava/lang/String;
    new-instance v0, Landroid/content/ComponentName;

    const-string v5, "com.google.android.apps.maps"

    const-string v6, "com.google.android.maps.MapsActivity"

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    .local v0, "compName":Landroid/content/ComponentName;
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 493
    .local v2, "mapsIntent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501
    .end local v0    # "compName":Landroid/content/ComponentName;
    .end local v3    # "uri":Ljava/lang/String;
    :goto_0
    return-void

    .line 494
    .end local v2    # "mapsIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 496
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "GalleryUtils"

    const-string v6, "GMM activity not found!"

    invoke-static {v5, v6, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 497
    const-string v5, "geo:%f,%f"

    invoke-static {v5, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatLatitudeLongitude(Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object v4

    .line 498
    .local v4, "url":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 499
    .restart local v2    # "mapsIntent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static split(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 1227
    const-string v1, "/"

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 1228
    .local v0, "values":[Ljava/lang/String;
    return-object v0
.end method

.method public static splitDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1232
    const/4 v0, 0x0

    .line 1233
    .local v0, "dsn":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 1234
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1235
    const-string v2, "/"

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 1236
    .local v1, "vs":[Ljava/lang/String;
    const/4 v2, 0x1

    aget-object v0, v1, v2

    .line 1242
    .end local v1    # "vs":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 1238
    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static startCameraActivity(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 449
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.media.action.STILL_IMAGE_CAMERA"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x14000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 453
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    :goto_0
    return-void

    .line 454
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "GalleryUtils"

    const-string v3, "Camera activity previously detected but cannot be found"

    invoke-static {v2, v3, v0}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 458
    const v2, 0x7f0e0062

    invoke-static {p0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public static startGalleryActivity(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 463
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 466
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 467
    return-void
.end method

.method public static stopDecoder(Lcom/sec/samsung/gallery/decoder/DecoderInterface;)V
    .locals 0
    .param p0, "decoderInterface"    # Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .prologue
    .line 1120
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->waitDecodingStop()V

    .line 1121
    return-void
.end method

.method public static final toMile(D)D
    .locals 2
    .param p0, "meter"    # D

    .prologue
    .line 366
    const-wide v0, 0x4099240000000000L    # 1609.0

    div-double v0, p0, v0

    return-wide v0
.end method

.method public static updateAgifAttribute(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const-wide/16 v2, 0x20

    .line 2050
    if-eqz p1, :cond_0

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasPendingAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2051
    invoke-static {p0, p1}, Lcom/quramsoft/agif/QURAMWINKUTIL;->isAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    invoke-virtual {p1, v2, v3, v0}, Lcom/sec/android/gallery3d/data/MediaItem;->setAttribute(JZ)V

    .line 2052
    const/4 v0, 0x0

    invoke-virtual {p1, v2, v3, v0}, Lcom/sec/android/gallery3d/data/MediaItem;->setPendingAttribute(JZ)V

    .line 2054
    :cond_0
    return-void
.end method
