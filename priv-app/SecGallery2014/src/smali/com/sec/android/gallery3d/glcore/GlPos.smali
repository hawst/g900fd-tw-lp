.class public Lcom/sec/android/gallery3d/glcore/GlPos;
.super Ljava/lang/Object;
.source "GlPos.java"


# instance fields
.field public mX:F

.field public mY:F

.field public mZ:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    .line 12
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    .line 13
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    .line 14
    return-void
.end method


# virtual methods
.method public set(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 17
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    .line 18
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    .line 19
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    .line 20
    return-void
.end method
