.class Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;
.super Ljava/lang/Object;
.source "GLES11Canvas.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GLState"
.end annotation


# instance fields
.field private mBlendEnabled:Z

.field private final mGL:Ljavax/microedition/khronos/opengles/GL11;

.field private mLineSmooth:Z

.field private mLineWidth:F

.field private mTexEnvMode:I

.field private mTexture2DEnabled:Z

.field private mTextureAlpha:F

.field private mTextureTarget:I


# direct methods
.method public constructor <init>(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 7
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const/16 v1, 0xde1

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 673
    const/16 v0, 0x1e01

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTexEnvMode:I

    .line 674
    iput v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureAlpha:F

    .line 675
    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureTarget:I

    .line 676
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mBlendEnabled:Z

    .line 677
    iput v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mLineWidth:F

    .line 678
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mLineSmooth:Z

    .line 680
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTexture2DEnabled:Z

    .line 684
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 687
    const/16 v0, 0xb50

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 690
    const/16 v0, 0xbd0

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 692
    const/16 v0, 0xc11

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 695
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnableClientState(I)V

    .line 696
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnableClientState(I)V

    .line 697
    invoke-interface {p1, v1}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 699
    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const v2, 0x45f00800    # 7681.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 705
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 706
    sget v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_RED:F

    sget v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_GREEN:F

    sget v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_BLUE:F

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glClearColor(FFFF)V

    .line 715
    :goto_0
    invoke-interface {p1, v6}, Ljavax/microedition/khronos/opengles/GL11;->glClearStencil(I)V

    .line 717
    const/16 v0, 0xbe2

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 718
    const/16 v0, 0x303

    invoke-interface {p1, v5, v0}, Ljavax/microedition/khronos/opengles/GL11;->glBlendFunc(II)V

    .line 721
    const/16 v0, 0xcf5

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glPixelStorei(II)V

    .line 722
    return-void

    .line 711
    :cond_0
    invoke-interface {p1, v4, v4, v4, v3}, Ljavax/microedition/khronos/opengles/GL11;->glClearColor(FFFF)V

    goto :goto_0
.end method


# virtual methods
.method public setBlendEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    const/16 v1, 0xbe2

    .line 779
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mBlendEnabled:Z

    if-ne v0, p1, :cond_0

    .line 786
    :goto_0
    return-void

    .line 780
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mBlendEnabled:Z

    .line 781
    if-eqz p1, :cond_1

    .line 782
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    goto :goto_0

    .line 784
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    goto :goto_0
.end method

.method public setColorMode(IF)V
    .locals 6
    .param p1, "color"    # I
    .param p2, "alpha"    # F

    .prologue
    const/4 v2, 0x0

    const/high16 v5, 0x437f0000    # 255.0f

    .line 750
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->isOpaque(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x3f733333    # 0.95f

    cmpg-float v1, p2, v1

    if-gez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 754
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureAlpha:F

    .line 756
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureTarget(I)V

    .line 758
    ushr-int/lit8 v1, p1, 0x18

    int-to-float v1, v1

    mul-float/2addr v1, p2

    const v2, 0x477fff00    # 65535.0f

    mul-float/2addr v1, v2

    div-float/2addr v1, v5

    div-float v0, v1, v5

    .line 759
    .local v0, "prealpha":F
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    shr-int/lit8 v2, p1, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    shr-int/lit8 v3, p1, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    and-int/lit16 v4, p1, 0xff

    int-to-float v4, v4

    mul-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    mul-float/2addr v5, v0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glColor4x(IIII)V

    .line 764
    return-void

    .end local v0    # "prealpha":F
    :cond_1
    move v1, v2

    .line 750
    goto :goto_0
.end method

.method public setLineSmooth(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    const/16 v1, 0xb20

    .line 790
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mLineSmooth:Z

    if-ne v0, p1, :cond_0

    .line 797
    :goto_0
    return-void

    .line 791
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mLineSmooth:Z

    .line 792
    if-eqz p1, :cond_1

    .line 793
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    goto :goto_0

    .line 795
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    goto :goto_0
.end method

.method public setLineWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 731
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mLineWidth:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 734
    :goto_0
    return-void

    .line 732
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mLineWidth:F

    .line 733
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, p1}, Ljavax/microedition/khronos/opengles/GL11;->glLineWidth(F)V

    goto :goto_0
.end method

.method public setTexEnvMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 725
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTexEnvMode:I

    if-ne v0, p1, :cond_0

    .line 728
    :goto_0
    return-void

    .line 726
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTexEnvMode:I

    .line 727
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    int-to-float v3, p1

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    goto :goto_0
.end method

.method public setTextureAlpha(F)V
    .locals 2
    .param p1, "alpha"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 737
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureAlpha:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 747
    :goto_0
    return-void

    .line 738
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureAlpha:F

    .line 739
    const v0, 0x3f733333    # 0.95f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    .line 741
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, v1, v1, v1, v1}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    .line 742
    const/16 v0, 0x1e01

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTexEnvMode(I)V

    goto :goto_0

    .line 744
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, p1, p1, p1, p1}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    .line 745
    const/16 v0, 0x2100

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTexEnvMode(I)V

    goto :goto_0
.end method

.method public setTextureTarget(I)V
    .locals 2
    .param p1, "target"    # I

    .prologue
    .line 768
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureTarget:I

    if-ne v0, p1, :cond_1

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureTarget:I

    if-eqz v0, :cond_2

    .line 770
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureTarget:I

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 772
    :cond_2
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureTarget:I

    .line 773
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureTarget:I

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->mTextureTarget:I

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    goto :goto_0
.end method
