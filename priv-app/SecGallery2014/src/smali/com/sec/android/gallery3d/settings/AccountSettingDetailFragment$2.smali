.class Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;
.super Ljava/lang/Object;
.source "AccountSettingDetailFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 11
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 88
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_wifi_only"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_contextual_tag"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_face_tag"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_dropbox_sync"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_play_sound_shot_tag"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_reply_management"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string v10, "hidden_items"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_filterby"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 96
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.settings.ACCOUNT_SYNC_SETTINGS"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    .local v4, "intent":Landroid/content/Intent;
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$100(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 98
    .local v3, "index":I
    if-ltz v3, :cond_1

    .line 99
    const-string v9, "account"

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$200(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->getAuthAccounts()Ljava/util/ArrayList;

    move-result-object v7

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mAccountPrefs:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$100(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;

    iget-object v7, v7, Lcom/sec/android/gallery3d/settings/AccountSettingManager$AuthAccount;->account:Landroid/accounts/Account;

    invoke-virtual {v4, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 102
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .end local v3    # "index":I
    .end local v4    # "intent":Landroid/content/Intent;
    .end local p1    # "preference":Landroid/preference/Preference;
    :cond_0
    :goto_0
    return v8

    .line 103
    .restart local v3    # "index":I
    .restart local v4    # "intent":Landroid/content/Intent;
    .restart local p1    # "preference":Landroid/preference/Preference;
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v7, "AccountSetting"

    const-string v9, "Activity Not Found"

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 108
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    const-string v7, "AccountSetting"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "wrong index : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 110
    .end local v3    # "index":I
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_filterby"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 111
    new-instance v4, Landroid/content/Intent;

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    const-class v9, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;

    invoke-direct {v4, v7, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    .restart local v4    # "intent":Landroid/content/Intent;
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 113
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_contextual_tag"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 114
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 116
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_5

    move v5, v8

    .line 117
    .local v5, "oldValue":Z
    :goto_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->setContextualTagEnable(Landroid/content/Context;Z)V

    .line 118
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagPref:Landroid/preference/Preference;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$300(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/preference/Preference;

    move-result-object v10

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    const v7, 0x7f0e00bc

    :goto_2
    invoke-virtual {v10, v7}, Landroid/preference/Preference;->setSummary(I)V

    .line 119
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # invokes: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->updateContextualTagPrefSwitchState()V
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$400(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    .line 120
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagChanged:Z
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$500(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 121
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # invokes: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startTagBuddyFragment()V
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$600(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V

    .line 122
    :cond_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # setter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContextualTagChanged:Z
    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$502(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;Z)Z

    goto/16 :goto_0

    .end local v5    # "oldValue":Z
    :cond_5
    move v5, v9

    .line 116
    goto :goto_1

    .line 118
    .restart local v5    # "oldValue":Z
    :cond_6
    const v7, 0x7f0e0187

    goto :goto_2

    .line 124
    .end local v5    # "oldValue":Z
    :cond_7
    new-instance v4, Landroid/content/Intent;

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    const-class v9, Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    invoke-direct {v4, v7, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 125
    .restart local v4    # "intent":Landroid/content/Intent;
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v7, :cond_8

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 126
    const/high16 v7, 0x14000000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 128
    :cond_8
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startActivity(Landroid/content/Intent;)V

    .line 129
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const-string v9, "TAGB"

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 131
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_9
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_dropbox_sync"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 132
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$200(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->isDropboxSignIn()Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$200(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->isCloudContentSyncOn()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 133
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.settings.CLOUD_SETTINGS"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    .restart local v4    # "intent":Landroid/content/Intent;
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 137
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    const-string v7, "AccountSetting"

    const-string v9, "Activity Not Found"

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 140
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_a
    new-instance v2, Landroid/content/Intent;

    const-string v7, "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 141
    .local v2, "i":Landroid/content/Intent;
    const-string v7, "com.sec.android.cloudagent"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 144
    .end local v2    # "i":Landroid/content/Intent;
    :cond_b
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "setting_face_tag"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 145
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    if-ne v7, v8, :cond_d

    .line 146
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    const-string v10, "FaceTagPermissionAlertDialogOff"

    invoke-static {v7, v10, v9}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_c

    move-object v7, p1

    check-cast v7, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    if-ne v7, v8, :cond_c

    .line 147
    const/4 v7, 0x4

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x2

    const/16 v10, 0xe

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v6, v7

    const/4 v7, 0x3

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mFaceTagPermissionHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$700(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/os/Handler;

    move-result-object v10

    aput-object v10, v6, v7

    .line 150
    .local v6, "params":[Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getGalleryID()I

    move-result v7

    invoke-static {v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(I)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v7

    const-string v10, "SHOW_USAGE_ALERT_DIALOG"

    invoke-virtual {v7, v10, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 151
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    const-string/jumbo v10, "setting_face_tag"

    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 152
    .local v1, "faceTag":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v1, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_0

    .line 154
    .end local v1    # "faceTag":Landroid/preference/CheckBoxPreference;
    .end local v6    # "params":[Ljava/lang/Object;
    :cond_c
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setFaceTagStatus(Landroid/content/Context;Z)V

    .line 155
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const-string v9, "FACE"

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 158
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_d
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setFaceTagStatus(Landroid/content/Context;Z)V

    .line 160
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const-string v9, "FACE"

    iget-object v10, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 162
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_e
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v9, "setting_play_sound_shot_tag"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 163
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v7

    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v9

    invoke-static {v7, v9}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->setSoundShotAutoPlay(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 164
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_f
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    const-string v9, "hidden_items"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 165
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 166
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v7, "com.sec.android.gallery3d"

    const-string v9, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v4, v7, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string v7, "hiddenitem"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$2;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 170
    :catch_2
    move-exception v0

    .line 171
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    const-string v7, "AccountSetting"

    const-string v9, "Activity Not Found"

    invoke-static {v7, v9}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method
