.class Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;
.super Landroid/os/Handler;
.source "AccountSettingDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 75
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 82
    :goto_0
    return-void

    .line 77
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    const-string/jumbo v2, "setting_face_tag"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 78
    .local v0, "faceTag":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment$1;->this$0:Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    # getter for: Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;->access$000(Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setFaceTagStatus(Landroid/content/Context;Z)V

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
    .end packed-switch
.end method
