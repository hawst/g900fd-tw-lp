.class public Lcom/sec/android/gallery3d/app/LockScreen;
.super Landroid/app/Activity;
.source "LockScreen.java"


# static fields
.field private static final IMAGE_TYPE:Ljava/lang/String; = "image/*"

.field private static final KEY_PICKED_ITEM:Ljava/lang/String; = "picked-item"

.field private static final KEY_STATE:Ljava/lang/String; = "activity-state"

.field private static final STATE_INIT:I = 0x0

.field private static final STATE_PHOTO_PICKED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "LockScreen"


# instance fields
.field private mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

.field private mPickedItem:Landroid/net/Uri;

.field private mState:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mState:I

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 116
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-nez v0, :cond_1

    .line 117
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 118
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/app/LockScreen;->setResult(I)V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/LockScreen;->finish()V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mState:I

    .line 124
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-eqz p3, :cond_0

    .line 125
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    if-eqz p1, :cond_0

    .line 58
    const-string v0, "activity-state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mState:I

    .line 59
    const-string v0, "picked-item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    .line 61
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 135
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->dismissDialog()V

    .line 138
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/16 v6, 0x401

    const/4 v5, 0x1

    .line 74
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/LockScreen;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v3, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 76
    new-instance v4, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/LockScreen;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/LockScreen;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v4, v3, p0, v5}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .line 77
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->showDialog()V

    .line 112
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/LockScreen;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 81
    .local v1, "intent":Landroid/content/Intent;
    iget v3, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mState:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 83
    :pswitch_0
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    .line 84
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    if-nez v3, :cond_1

    .line 85
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 87
    .local v2, "request":Landroid/content/Intent;
    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const-string v3, "com.sec.android.gallery3d"

    const-string v4, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    const-string v3, "image/*"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v3, "mode-crop"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 93
    const-string v3, "set-as-wallpaper"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 94
    const-string/jumbo v3, "wallpaper_type"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 96
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/app/LockScreen;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "LockScreen"

    const-string v4, "Activity Not Found"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 103
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "request":Landroid/content/Intent;
    :cond_1
    iput v5, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mState:I

    .line 107
    :pswitch_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    if-eqz v3, :cond_3

    .line 108
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    const-string v4, "image/*"

    const/4 v5, 0x0

    invoke-static {p0, v3, v4, v6, v5}, Lcom/sec/samsung/gallery/controller/CropImageCmd;->startCropImageActivity(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;IZ)V

    .line 109
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/LockScreen;->finish()V

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "saveState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    const-string v0, "activity-state"

    iget v1, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "picked-item"

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/LockScreen;->mPickedItem:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    :cond_0
    return-void
.end method
