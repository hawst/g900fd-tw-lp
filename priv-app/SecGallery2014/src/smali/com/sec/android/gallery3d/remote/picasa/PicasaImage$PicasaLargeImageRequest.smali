.class Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;
.super Ljava/lang/Object;
.source "PicasaImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PicasaLargeImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final mUrl:Ljava/net/URL;

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Ljava/net/URL;)V
    .locals 0
    .param p2, "url"    # Ljava/net/URL;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;->mUrl:Ljava/net/URL;

    .line 286
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v0, 0x0

    .line 290
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$900(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;->mUrl:Ljava/net/URL;

    invoke-virtual {v2, p1, v3, v0}, Lcom/sec/android/gallery3d/data/DownloadCache;->download(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v1

    .line 292
    .local v1, "entry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-nez v1, :cond_1

    .line 293
    const-string v2, "PicasaImage"

    const-string v3, "download failed "

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_0
    :goto_0
    return-object v0

    .line 298
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # invokes: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->rotateFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/DownloadCache$Entry;)V
    invoke-static {v2, p1, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$1000(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/DownloadCache$Entry;)V

    .line 300
    iget-object v2, v1, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p1, v2, v3}, Lcom/sec/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    .line 301
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->associateWith(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 281
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaLargeImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method
