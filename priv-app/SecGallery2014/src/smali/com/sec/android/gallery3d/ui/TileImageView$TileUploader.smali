.class Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;
.super Ljava/lang/Object;
.source "TileImageView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/TileImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TileUploader"
.end annotation


# instance fields
.field mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/TileImageView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/TileImageView;)V
    .locals 2

    .prologue
    .line 689
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 690
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;->mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/ui/TileImageView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/TileImageView$1;

    .prologue
    .line 689
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;-><init>(Lcom/sec/android/gallery3d/ui/TileImageView;)V

    return-void
.end method


# virtual methods
.method public onGLIdle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)Z
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "renderRequested"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 696
    if-eqz p2, :cond_1

    .line 717
    :cond_0
    :goto_0
    return v4

    .line 697
    :cond_1
    const/4 v2, 0x1

    .line 698
    .local v2, "quota":I
    const/4 v3, 0x0

    .line 699
    .local v3, "tile":Lcom/sec/android/gallery3d/ui/TileImageView$Tile;
    :cond_2
    :goto_1
    if-lez v2, :cond_3

    .line 700
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    monitor-enter v6

    .line 701
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;->this$0:Lcom/sec/android/gallery3d/ui/TileImageView;

    # getter for: Lcom/sec/android/gallery3d/ui/TileImageView;->mUploadQueue:Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/TileImageView;->access$400(Lcom/sec/android/gallery3d/ui/TileImageView;)Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/TileImageView$TileQueue;->pop()Lcom/sec/android/gallery3d/ui/TileImageView$Tile;

    move-result-object v3

    .line 702
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 703
    if-nez v3, :cond_5

    .line 716
    :cond_3
    if-nez v3, :cond_4

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/TileImageView$TileUploader;->mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 717
    :cond_4
    if-nez v3, :cond_0

    move v4, v5

    goto :goto_0

    .line 702
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 705
    :cond_5
    :try_start_2
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->isContentValid()Z

    move-result v6

    if-nez v6, :cond_2

    .line 706
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->isLoaded()Z

    move-result v1

    .line 707
    .local v1, "hasBeenLoaded":Z
    iget v6, v3, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->mTileState:I

    const/16 v7, 0x8

    if-ne v6, v7, :cond_7

    move v6, v4

    :goto_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 708
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->updateContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 709
    if-nez v1, :cond_6

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, p1, v6, v7}, Lcom/sec/android/gallery3d/ui/TileImageView$Tile;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    :try_end_2
    .catch Ljava/lang/AssertionError; {:try_start_2 .. :try_end_2} :catch_0

    .line 710
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_7
    move v6, v5

    .line 707
    goto :goto_2

    .line 712
    .end local v1    # "hasBeenLoaded":Z
    :catch_0
    move-exception v0

    .line 713
    .local v0, "e":Ljava/lang/AssertionError;
    const-string v6, "TileImageView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onGLIdle AssertionError"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
