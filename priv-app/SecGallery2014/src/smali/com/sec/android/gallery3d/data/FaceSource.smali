.class public Lcom/sec/android/gallery3d/data/FaceSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "FaceSource.java"


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mLocalImages:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/gallery3d/data/LocalImage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 1
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 13
    const-string v0, "face"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 11
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceSource;->mLocalImages:Landroid/util/SparseArray;

    .line 14
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FaceSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 15
    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 19
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FaceSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FaceSource;->mLocalImages:Landroid/util/SparseArray;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/util/SparseArray;)V

    return-object v0
.end method
