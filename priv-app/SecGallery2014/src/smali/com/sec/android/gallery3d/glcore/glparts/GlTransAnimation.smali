.class public Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlTransAnimation.java"


# instance fields
.field private mFromAlpha:F

.field private mFromScaleX:F

.field private mFromScaleY:F

.field private mFromX:F

.field private mFromY:F

.field private mFromZ:F

.field private mIsDelta:Z

.field private mLx:F

.field private mLy:F

.field private mLz:F

.field private mScaleX:F

.field private mScaleY:F

.field private mToAlpha:F

.field private mToX:F

.field private mToY:F

.field private mToZ:F


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFF)V
    .locals 9
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "delX"    # F
    .param p3, "delY"    # F
    .param p4, "delZ"    # F

    .prologue
    .line 38
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v6

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v5, p3

    move v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFFFFFZ)V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFFFFF)V
    .locals 9
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "fromX"    # F
    .param p3, "toX"    # F
    .param p4, "fromY"    # F
    .param p5, "toY"    # F
    .param p6, "fromZ"    # F
    .param p7, "toZ"    # F

    .prologue
    .line 34
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFFFFFZ)V

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFFFFFZ)V
    .locals 7
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "fromX"    # F
    .param p3, "toX"    # F
    .param p4, "fromY"    # F
    .param p5, "toY"    # F
    .param p6, "fromZ"    # F
    .param p7, "toZ"    # F
    .param p8, "isDelta"    # Z

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    .line 22
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->setTransition(FFFFFF)V

    .line 23
    iput-boolean p8, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mIsDelta:Z

    .line 24
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAlpha()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromAlpha:F

    .line 26
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromAlpha:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToAlpha:F

    .line 27
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromScaleX:F

    .line 28
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getScaleY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromScaleY:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mScaleY:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mScaleX:F

    .line 30
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 8
    .param p1, "ratio"    # F

    .prologue
    .line 80
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mIsDelta:Z

    if-eqz v4, :cond_2

    .line 81
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromX:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToX:F

    mul-float/2addr v5, p1

    add-float v1, v4, v5

    .line 82
    .local v1, "dx":F
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromY:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToY:F

    mul-float/2addr v5, p1

    add-float v2, v4, v5

    .line 83
    .local v2, "dy":F
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromZ:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToZ:F

    mul-float/2addr v5, p1

    add-float v3, v4, v5

    .line 89
    .local v3, "dz":F
    :goto_0
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLx:F

    invoke-static {v4, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLy:F

    invoke-static {v4, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLz:F

    invoke-static {v4, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-eqz v4, :cond_1

    .line 94
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 95
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLx:F

    .line 96
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLy:F

    .line 97
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLz:F

    .line 100
    :cond_1
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromAlpha:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToAlpha:F

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromAlpha:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v0, v4, v5

    .line 101
    .local v0, "da":F
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 102
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromScaleX:F

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mScaleX:F

    mul-float/2addr v6, p1

    add-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromScaleY:F

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mScaleY:F

    mul-float/2addr v7, p1

    add-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 104
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLx:F

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLy:F

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLz:F

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 105
    return-void

    .line 85
    .end local v0    # "da":F
    .end local v1    # "dx":F
    .end local v2    # "dy":F
    .end local v3    # "dz":F
    :cond_2
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromX:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToX:F

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromX:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v1, v4, v5

    .line 86
    .restart local v1    # "dx":F
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromY:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToY:F

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromY:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v2, v4, v5

    .line 87
    .restart local v2    # "dy":F
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromZ:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToZ:F

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromZ:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v3, v4, v5

    .restart local v3    # "dz":F
    goto :goto_0
.end method

.method public getCurX()F
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLx:F

    return v0
.end method

.method public getCurY()F
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLy:F

    return v0
.end method

.method public getCurZ()F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLz:F

    return v0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 111
    :cond_0
    return-void
.end method

.method public setAlpha(FF)V
    .locals 0
    .param p1, "fromA"    # F
    .param p2, "toA"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromAlpha:F

    .line 55
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToAlpha:F

    .line 56
    return-void
.end method

.method public setScale(FF)V
    .locals 0
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mScaleX:F

    .line 60
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mScaleY:F

    .line 61
    return-void
.end method

.method public setTransition(FFFFFF)V
    .locals 1
    .param p1, "fromX"    # F
    .param p2, "toX"    # F
    .param p3, "fromY"    # F
    .param p4, "toY"    # F
    .param p5, "fromZ"    # F
    .param p6, "toZ"    # F

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromX:F

    .line 43
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToX:F

    .line 44
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromY:F

    .line 45
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToY:F

    .line 46
    iput p5, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromZ:F

    .line 47
    iput p6, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mToZ:F

    .line 48
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromX:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLx:F

    .line 49
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromY:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLy:F

    .line 50
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mFromZ:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->mLz:F

    .line 51
    return-void
.end method
