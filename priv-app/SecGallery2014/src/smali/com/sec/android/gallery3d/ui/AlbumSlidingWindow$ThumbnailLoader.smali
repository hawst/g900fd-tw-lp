.class Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;
.super Lcom/sec/android/gallery3d/ui/BitmapLoader;
.source "AlbumSlidingWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbnailLoader"
.end annotation


# instance fields
.field private final mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private final mSlotIndex:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p2, "slotIndex"    # I
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/BitmapLoader;-><init>()V

    .line 327
    iput p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->mSlotIndex:I

    .line 328
    iput-object p3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 329
    return-void
.end method


# virtual methods
.method protected onLoadComplete(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$300(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 346
    return-void
.end method

.method protected submitBitmapTask(Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "l":Lcom/sec/android/gallery3d/util/FutureListener;, "Lcom/sec/android/gallery3d/util/FutureListener<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mMiniThumbMode:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mThreadPool:Lcom/sec/android/gallery3d/util/JobLimiter;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$200(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/util/JobLimiter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/JobLimiter;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    .line 339
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mThreadPool:Lcom/sec/android/gallery3d/util/JobLimiter;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$200(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/util/JobLimiter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/JobLimiter;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    goto :goto_0
.end method

.method public updateEntry()V
    .locals 6

    .prologue
    .line 350
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 351
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 367
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 352
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$400(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->mSlotIndex:I

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$400(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)[Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;

    move-result-object v5

    array-length v5, v5

    rem-int/2addr v4, v5

    aget-object v2, v3, v4

    .line 353
    .local v2, "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$500(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;-><init>(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;)V

    iput-object v3, v2, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    .line 354
    iget-object v3, v2, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    iput-object v3, v2, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->content:Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 356
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->mSlotIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->isActiveSlot(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 357
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$600(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    move-result-object v3

    iget-object v4, v2, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->addTexture(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    .line 358
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # --operator for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$706(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)I

    .line 359
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mActiveRequestCount:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$700(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # invokes: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->requestNonactiveImages()V
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$800(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)V

    .line 360
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$900(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$900(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$Listener;->onContentChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 364
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :catch_0
    move-exception v1

    .line 365
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 362
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$ThumbnailLoader;->this$0:Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->mTileUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;->access$600(Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    move-result-object v3

    iget-object v4, v2, Lcom/sec/android/gallery3d/ui/AlbumSlidingWindow$AlbumEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->addTexture(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
