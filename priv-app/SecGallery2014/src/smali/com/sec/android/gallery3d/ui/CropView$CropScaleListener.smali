.class Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/CropView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CropScaleListener"
.end annotation


# static fields
.field private static final MAX_SCALE_MULTIPLIER:I = 0x8


# instance fields
.field private isScaleMode:Z

.field private mScale:F

.field private savedScaleFactor:F

.field private start:Landroid/graphics/PointF;

.field private startDeltaFocus:Landroid/graphics/PointF;

.field private startFocus:Landroid/graphics/PointF;

.field private startScale:F

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/CropView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/CropView;)V
    .locals 1

    .prologue
    .line 1072
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 1074
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startScale:F

    .line 1075
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    .line 1076
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->isScaleMode:Z

    .line 1078
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->start:Landroid/graphics/PointF;

    .line 1079
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startFocus:Landroid/graphics/PointF;

    .line 1080
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startDeltaFocus:Landroid/graphics/PointF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/CropView;Lcom/sec/android/gallery3d/ui/CropView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/CropView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/CropView$1;

    .prologue
    .line 1072
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;-><init>(Lcom/sec/android/gallery3d/ui/CropView;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;Landroid/graphics/RectF;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    .param p1, "x1"    # Landroid/graphics/RectF;

    .prologue
    .line 1072
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->moveCenterPoint(Landroid/graphics/RectF;)V

    return-void
.end method

.method private moveCenterPoint(Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "identityRect"    # Landroid/graphics/RectF;

    .prologue
    .line 1119
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3302(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1120
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3402(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1121
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->validateCenterPoint()V

    .line 1123
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    .line 1124
    return-void
.end method

.method private validateCenterPoint()V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1100
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    div-float/2addr v4, v5

    div-float v2, v4, v6

    .line 1101
    .local v2, "minCenterX":F
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    sub-float v0, v4, v2

    .line 1102
    .local v0, "maxCenterX":F
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    div-float/2addr v4, v5

    div-float v3, v4, v6

    .line 1103
    .local v3, "minCenterY":F
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    sub-float v1, v4, v3

    .line 1105
    .local v1, "maxCenterY":F
    cmpg-float v4, v0, v2

    if-gez v4, :cond_0

    .line 1106
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$3302(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1111
    :goto_0
    cmpg-float v4, v1, v3

    if-gez v4, :cond_1

    .line 1112
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$3402(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1116
    :goto_1
    return-void

    .line 1108
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$3300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v5

    invoke-static {v5, v2, v0}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v5

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$3302(Lcom/sec/android/gallery3d/ui/CropView;F)F

    goto :goto_0

    .line 1114
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$3400(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v5

    invoke-static {v5, v3, v1}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v5

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$3402(Lcom/sec/android/gallery3d/ui/CropView;F)F

    goto :goto_1
.end method


# virtual methods
.method public getMaxScale()F
    .locals 6

    .prologue
    .line 1226
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$1900(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWvga(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$1900(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isUnderWvga(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    const/4 v2, 0x3

    .line 1227
    .local v2, "maxScaleMultiplier":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    int-to-float v5, v2

    mul-float v3, v4, v5

    .line 1228
    .local v3, "maxScaleWidth":F
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    int-to-float v5, v2

    mul-float v1, v4, v5

    .line 1229
    .local v1, "maxScaleHeight":F
    cmpg-float v4, v3, v1

    if-gez v4, :cond_3

    move v0, v1

    .line 1230
    .local v0, "maxScale":F
    :goto_1
    int-to-float v4, v2

    cmpg-float v4, v0, v4

    if-gez v4, :cond_1

    int-to-float v0, v2

    .end local v0    # "maxScale":F
    :cond_1
    return v0

    .line 1226
    .end local v1    # "maxScaleHeight":F
    .end local v2    # "maxScaleMultiplier":I
    .end local v3    # "maxScaleWidth":F
    :cond_2
    const/16 v2, 0x8

    goto :goto_0

    .restart local v1    # "maxScaleHeight":F
    .restart local v2    # "maxScaleMultiplier":I
    .restart local v3    # "maxScaleWidth":F
    :cond_3
    move v0, v3

    .line 1229
    goto :goto_1
.end method

.method public getMinScale()F
    .locals 4

    .prologue
    .line 1216
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 1217
    .local v1, "minScaleWidth":F
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 1218
    .local v0, "minScaleHeight":F
    cmpl-float v2, v1, v0

    if-lez v2, :cond_0

    .end local v0    # "minScaleHeight":F
    :goto_0
    return v0

    .restart local v0    # "minScaleHeight":F
    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 1212
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    return v0
.end method

.method public identityRect(Landroid/graphics/RectF;Z)Landroid/graphics/RectF;
    .locals 17
    .param p1, "input"    # Landroid/graphics/RectF;
    .param p2, "isScaling"    # Z

    .prologue
    .line 1176
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 1177
    .local v4, "output":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v13

    int-to-float v13, v13

    const/high16 v14, 0x3f000000    # 0.5f

    mul-float v2, v13, v14

    .line 1178
    .local v2, "offsetX":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v13

    int-to-float v13, v13

    const/high16 v14, 0x3f000000    # 0.5f

    mul-float v3, v13, v14

    .line 1179
    .local v3, "offsetY":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v13}, Lcom/sec/android/gallery3d/ui/CropView;->access$3300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v11

    .line 1180
    .local v11, "x":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v13}, Lcom/sec/android/gallery3d/ui/CropView;->access$3400(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v12

    .line 1181
    .local v12, "y":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getScale()F

    move-result v10

    .line 1183
    .local v10, "s":F
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v13, v2

    div-float/2addr v13, v10

    add-float/2addr v13, v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v14

    int-to-float v14, v14

    div-float v6, v13, v14

    .line 1184
    .local v6, "outputLeft":F
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v13, v2

    div-float/2addr v13, v10

    add-float/2addr v13, v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v14

    int-to-float v14, v14

    div-float v7, v13, v14

    .line 1185
    .local v7, "outputRight":F
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v13, v3

    div-float/2addr v13, v10

    add-float/2addr v13, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v14

    int-to-float v14, v14

    div-float v8, v13, v14

    .line 1186
    .local v8, "outputTop":F
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v13, v3

    div-float/2addr v13, v10

    add-float/2addr v13, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v14

    int-to-float v14, v14

    div-float v5, v13, v14

    .line 1188
    .local v5, "outputBottom":F
    new-instance v1, Landroid/graphics/RectF;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/high16 v15, 0x3f800000    # 1.0f

    const/high16 v16, 0x3f800000    # 1.0f

    move/from16 v0, v16

    invoke-direct {v1, v13, v14, v15, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1189
    .local v1, "max":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v13}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v13

    const/high16 v14, -0x40800000    # -1.0f

    cmpl-float v13, v13, v14

    if-eqz v13, :cond_0

    if-eqz p2, :cond_0

    .line 1190
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->height()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v15}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v15

    mul-float/2addr v14, v15

    add-float/2addr v13, v14

    sub-float/2addr v13, v2

    div-float/2addr v13, v10

    add-float/2addr v13, v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v14

    int-to-float v14, v14

    div-float v7, v13, v14

    .line 1191
    move-object/from16 v0, p1

    iget v13, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->width()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v15}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v15

    div-float/2addr v14, v15

    add-float/2addr v13, v14

    sub-float/2addr v13, v3

    div-float/2addr v13, v10

    add-float/2addr v13, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v14

    int-to-float v14, v14

    div-float v5, v13, v14

    .line 1192
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z
    invoke-static {v13}, Lcom/sec/android/gallery3d/ui/CropView;->access$2200(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1193
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v13}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v13

    # getter for: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;
    invoke-static {v13}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;

    move-result-object v9

    .line 1194
    .local v9, "r":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v13}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v13

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v14

    int-to-float v14, v14

    div-float/2addr v13, v14

    const/high16 v14, 0x3f800000    # 1.0f

    cmpl-float v13, v13, v14

    if-lez v13, :cond_1

    .line 1195
    const/4 v13, 0x0

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v14

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/CropView;->access$2400()F

    move-result v15

    mul-float/2addr v14, v15

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    invoke-virtual {v1, v13, v14}, Landroid/graphics/RectF;->inset(FF)V

    .line 1202
    .end local v9    # "r":Landroid/graphics/RectF;
    :cond_0
    :goto_0
    iget v13, v1, Landroid/graphics/RectF;->left:F

    iget v14, v1, Landroid/graphics/RectF;->right:F

    invoke-static {v6, v13, v14}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v13

    iput v13, v4, Landroid/graphics/RectF;->left:F

    .line 1203
    iget v13, v1, Landroid/graphics/RectF;->top:F

    iget v14, v1, Landroid/graphics/RectF;->bottom:F

    invoke-static {v8, v13, v14}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v13

    iput v13, v4, Landroid/graphics/RectF;->top:F

    .line 1204
    iget v13, v1, Landroid/graphics/RectF;->left:F

    iget v14, v1, Landroid/graphics/RectF;->right:F

    invoke-static {v7, v13, v14}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v13

    iput v13, v4, Landroid/graphics/RectF;->right:F

    .line 1205
    iget v13, v1, Landroid/graphics/RectF;->top:F

    iget v14, v1, Landroid/graphics/RectF;->bottom:F

    invoke-static {v5, v13, v14}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v13

    iput v13, v4, Landroid/graphics/RectF;->bottom:F

    .line 1207
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {v14}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v14

    invoke-virtual {v14, v4}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v14

    iput-object v14, v13, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    .line 1208
    return-object v4

    .line 1197
    .restart local v9    # "r":Landroid/graphics/RectF;
    :cond_1
    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v13

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/CropView;->access$2400()F

    move-result v14

    mul-float/2addr v13, v14

    const/high16 v14, 0x3f000000    # 0.5f

    mul-float/2addr v13, v14

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/graphics/RectF;->inset(FF)V

    goto :goto_0
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 1149
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getMinScale()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    .line 1150
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3302(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1151
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3402(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1152
    return-void
.end method

.method public inverseMapPoint(Landroid/graphics/PointF;)V
    .locals 6
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    .line 1155
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getScale()F

    move-result v0

    .line 1156
    .local v0, "s":F
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    sub-float/2addr v1, v2

    div-float/2addr v1, v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$3300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1, v3, v5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    iput v1, p1, Landroid/graphics/PointF;->x:F

    .line 1157
    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    sub-float/2addr v1, v2

    div-float/2addr v1, v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$3400(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1, v3, v5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    iput v1, p1, Landroid/graphics/PointF;->y:F

    .line 1158
    return-void
.end method

.method public isScaleMode()Z
    .locals 1

    .prologue
    .line 1222
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->isScaleMode:Z

    return v0
.end method

.method public mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 11
    .param p1, "input"    # Landroid/graphics/RectF;

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 1161
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1162
    .local v2, "output":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float v0, v6, v7

    .line 1163
    .local v0, "offsetX":F
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float v1, v6, v7

    .line 1164
    .local v1, "offsetY":F
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/CropView;->access$3300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v4

    .line 1165
    .local v4, "x":F
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/CropView;->access$3400(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v5

    .line 1166
    .local v5, "y":F
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getScale()F

    move-result v3

    .line 1167
    .local v3, "s":F
    iget v6, p1, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    sub-float/2addr v6, v4

    mul-float/2addr v6, v3

    add-float/2addr v6, v0

    iget v7, p1, Landroid/graphics/RectF;->top:F

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    sub-float/2addr v7, v5

    mul-float/2addr v7, v3

    add-float/2addr v7, v1

    iget v8, p1, Landroid/graphics/RectF;->right:F

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    sub-float/2addr v8, v4

    mul-float/2addr v8, v3

    add-float/2addr v8, v0

    iget v9, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v10}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v9, v10

    sub-float/2addr v9, v5

    mul-float/2addr v9, v3

    add-float/2addr v9, v1

    invoke-virtual {v2, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1172
    return-object v2
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1086
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startScale:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->setScale(F)V

    .line 1087
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getMaxScale()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1088
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->savedScaleFactor:F

    .line 1090
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->start:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startFocus:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startDeltaFocus:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    div-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->savedScaleFactor:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3302(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1091
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->start:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startFocus:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startDeltaFocus:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    div-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->savedScaleFactor:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3402(Lcom/sec/android/gallery3d/ui/CropView;F)F

    .line 1093
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->validateCenterPoint()V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, v1, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->identityRect(Landroid/graphics/RectF;Z)Landroid/graphics/RectF;

    move-result-object v1

    # setter for: Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->access$202(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 1096
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScale(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 1128
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mHighlightRectangle:Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/CropView;->access$600(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->isMoved()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1129
    const/4 v0, 0x0

    .line 1139
    :goto_0
    return v0

    .line 1131
    :cond_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->savedScaleFactor:F

    .line 1132
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startScale:F

    .line 1133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->isScaleMode:Z

    .line 1134
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->start:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterX:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1135
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->start:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mCenterY:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$3400(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 1136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startFocus:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1137
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startDeltaFocus:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startFocus:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 1138
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startDeltaFocus:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->startFocus:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/CropView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 1139
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 1144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->isScaleMode:Z

    .line 1145
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 1146
    return-void
.end method

.method public setScale(F)V
    .locals 2
    .param p1, "scale"    # F

    .prologue
    .line 1234
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getMinScale()F

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getMaxScale()F

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mScale:F

    .line 1235
    return-void
.end method
