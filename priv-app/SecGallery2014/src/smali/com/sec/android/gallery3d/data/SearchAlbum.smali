.class public Lcom/sec/android/gallery3d/data/SearchAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "SearchAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/SearchAlbum$UpdateOperation;
    }
.end annotation


# instance fields
.field private mAlbumKey:Ljava/lang/String;

.field private mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mConsumer:Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

.field private final mContext:Landroid/content/Context;

.field private mCover:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field public mGroupId:I

.field private mKeywordList:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaSetType:I

.field private mName:Ljava/lang/String;

.field private mNameID:Ljava/lang/String;

.field private mPathExs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mPaths:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field public mPersonId:I

.field private mSupportShare:Z

.field private mTempMediaItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mUntaggedAlbum:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v2, 0x0

    .line 82
    invoke-static {}, Lcom/sec/android/gallery3d/data/SearchAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPathExs:Ljava/util/HashSet;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mName:Ljava/lang/String;

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mSupportShare:Z

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mUntaggedAlbum:Z

    .line 60
    iput v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPersonId:I

    .line 61
    iput v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mGroupId:I

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mNameID:Ljava/lang/String;

    .line 64
    new-instance v0, Lcom/sec/android/gallery3d/data/SearchAlbum$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/SearchAlbum$1;-><init>(Lcom/sec/android/gallery3d/data/SearchAlbum;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mConsumer:Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

    .line 290
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaSetType:I

    .line 83
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mContext:Landroid/content/Context;

    .line 85
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 89
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/SearchAlbum;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/SearchAlbum;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/SearchAlbum;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/SearchAlbum;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/SearchAlbum;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/SearchAlbum;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/SearchAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private getClusterAlbumByKeyword(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .locals 6
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 611
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 612
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/ClusterAlbum;>;"
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v5, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->getAlbums()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .line 613
    .local v0, "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    if-eqz v0, :cond_0

    .line 615
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getName()Ljava/lang/String;

    move-result-object v1

    .line 616
    .local v1, "albumName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 619
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v5, :cond_1

    .line 620
    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 621
    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 622
    .local v4, "values":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v1, v4, v5

    .line 625
    .end local v4    # "values":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 626
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 629
    .end local v0    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v1    # "albumName":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/sec/android/gallery3d/data/ClusterAlbum;

    :goto_1
    return-object v5

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private runItems()V
    .locals 5

    .prologue
    .line 211
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v3

    .line 212
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    monitor-exit v3

    .line 227
    :goto_0
    return-void

    .line 214
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 216
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 217
    const/4 v0, 0x0

    .line 218
    .local v0, "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v3

    .line 219
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 220
    .end local v0    # "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local v1, "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 222
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mConsumer:Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 223
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v3

    .line 224
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 225
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 226
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 214
    .end local v1    # "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    .line 220
    .restart local v0    # "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_2
    move-exception v2

    :goto_1
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2

    .end local v0    # "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v1    # "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_3
    move-exception v2

    move-object v0, v1

    .end local v1    # "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v0    # "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    goto :goto_1
.end method

.method private updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "joinedName"    # Ljava/lang/String;
    .param p4, "personId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p3, :cond_0

    .line 523
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mContext:Landroid/content/Context;

    invoke-static {v3, p4, p3}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V

    .line 525
    :cond_0
    const/4 v2, 0x0

    .line 526
    .local v2, "recommendPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Path;

    .line 527
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v1, p4}, Lcom/sec/android/gallery3d/data/SearchAlbum;->updateOneFace(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V

    .line 529
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xd

    aget-object v3, v3, v4

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 530
    move-object v2, v1

    goto :goto_0

    .line 533
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_2
    if-eqz v2, :cond_3

    .line 534
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v2, p4}, Lcom/sec/android/gallery3d/data/SearchAlbum;->autoRecommend(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V

    .line 537
    :goto_1
    return-void

    .line 536
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->updateMediaSet()V

    goto :goto_1
.end method


# virtual methods
.method public addItems(Ljava/util/ArrayList;Z)V
    .locals 11
    .param p2, "noNeedCompare"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "itemPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v10, 0x2

    .line 169
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v5, "tempPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-nez p2, :cond_2

    .line 172
    const/4 v6, 0x0

    .line 173
    .local v6, "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v9

    .line 174
    :try_start_0
    new-instance v7, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    .end local v6    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local v7, "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 176
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 177
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    move-object v2, v3

    .line 178
    .local v2, "localPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v8

    const-string v9, "face"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 179
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 180
    .local v4, "str":[Ljava/lang/String;
    aget-object v1, v4, v10

    .line 181
    .local v1, "locaFileId":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v8, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 182
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 183
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "locaFileId":Ljava/lang/String;
    .end local v2    # "localPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "str":[Ljava/lang/String;
    .end local v7    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v6    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_0
    move-exception v8

    :goto_1
    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v8

    .line 185
    .end local v6    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "localPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .restart local v7    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_1
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 189
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "localPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v7    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_2
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v9

    .line 190
    :try_start_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v8}, Ljava/util/HashSet;->clear()V

    .line 191
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPathExs:Ljava/util/HashSet;

    invoke-virtual {v8}, Ljava/util/HashSet;->clear()V

    .line 192
    if-eqz p2, :cond_3

    .line 193
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v8, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 194
    monitor-exit v9

    .line 208
    :goto_2
    return-void

    .line 197
    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 198
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    move-object v2, v3

    .line 199
    .restart local v2    # "localPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v8

    const-string v10, "face"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 200
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v10, "/"

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 201
    .restart local v4    # "str":[Ljava/lang/String;
    const/4 v8, 0x2

    aget-object v1, v4, v8

    .line 202
    .restart local v1    # "locaFileId":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v8, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 204
    .end local v1    # "locaFileId":Ljava/lang/String;
    .end local v4    # "str":[Ljava/lang/String;
    :cond_5
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 205
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v8, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 207
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "localPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :catchall_1
    move-exception v8

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v8

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_6
    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 175
    .end local v0    # "i$":Ljava/util/Iterator;
    .restart local v7    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_2
    move-exception v8

    move-object v6, v7

    .end local v7    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v6    # "tempPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    goto :goto_1
.end method

.method public addKeyword(Ljava/lang/String;)Z
    .locals 7
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 92
    const/4 v3, 0x0

    .line 93
    .local v3, "result":Z
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v5

    .line 94
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    invoke-virtual {v6, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    move-result v3

    .line 95
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    if-eqz v3, :cond_1

    .line 98
    const/4 v0, 0x0

    .line 99
    .local v0, "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    monitor-enter v5

    .line 100
    :try_start_1
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getClusterAlbumByKeyword(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/ClusterAlbum;

    move-result-object v0

    .line 101
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v2, "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz v0, :cond_0

    .line 104
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_0

    .line 105
    aget-object v5, v0, v1

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 104
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v1    # "i":I
    .end local v2    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 101
    .restart local v0    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    .line 107
    .restart local v2    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    invoke-virtual {v5}, Ljava/util/LinkedHashSet;->size()I

    move-result v5

    if-ne v5, v4, :cond_2

    :goto_1
    invoke-virtual {p0, v2, v4}, Lcom/sec/android/gallery3d/data/SearchAlbum;->addItems(Ljava/util/ArrayList;Z)V

    .line 108
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->runItems()V

    .line 111
    .end local v0    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v2    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_1
    return v3

    .line 107
    .restart local v0    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .restart local v2    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public autoRecommend(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "personId"    # I

    .prologue
    .line 540
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v1

    .line 541
    .local v1, "values":[Ljava/lang/String;
    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 542
    .local v0, "id":I
    const/4 v2, 0x0

    invoke-static {p1, v2, v0, p3}, Lcom/sec/samsung/gallery/access/face/FaceList;->recommendFaces(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V

    .line 543
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 231
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 232
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPathExs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 233
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    .line 234
    return-void
.end method

.method public delete()V
    .locals 4

    .prologue
    .line 381
    new-instance v0, Lcom/sec/android/gallery3d/data/SearchAlbum$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/SearchAlbum$2;-><init>(Lcom/sec/android/gallery3d/data/SearchAlbum;)V

    .line 389
    .local v0, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 390
    return-void
.end method

.method protected enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I
    .locals 2
    .param p1, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p2, "startIndex"    # I

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v1

    .line 302
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mCover:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mCover:Lcom/sec/android/gallery3d/data/MediaItem;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mAlbumKey:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyword(I)Ljava/lang/String;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 154
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v1

    .line 155
    if-ltz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 156
    :cond_0
    const-string v0, ""

    monitor-exit v1

    .line 158
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, p1

    monitor-exit v1

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getKeywordCount()I
    .locals 2

    .prologue
    .line 163
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v1

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 237
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v1

    .line 238
    if-ltz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 239
    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    .line 240
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    monitor-exit v1

    goto :goto_0

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 4
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v2

    .line 282
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int v3, p1, p2

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 283
    .local v0, "end":I
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 284
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    monitor-exit v2

    .line 286
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v2

    goto :goto_0

    .line 287
    .end local v0    # "end":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getMediaItemCount()I
    .locals 2

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v1

    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaItemEx(II)Ljava/util/ArrayList;
    .locals 5
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 557
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    .line 558
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 559
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_0

    instance-of v4, v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v4, :cond_0

    .line 560
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 562
    .restart local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 564
    .end local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    return-object v3
.end method

.method public getMediaItems()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getMediaSetType()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaSetType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNameID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mNameID:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 10

    .prologue
    const-wide/high16 v8, 0x800000000000000L

    const-wide/high16 v6, 0x200000000000000L

    .line 360
    const-wide/16 v0, 0x5

    .line 361
    .local v0, "operation":J
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mSupportShare:Z

    if-nez v2, :cond_0

    .line 362
    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 365
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v2, :cond_1

    .line 366
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v2, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getClusterKind()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 367
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0068

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 376
    .end local v0    # "operation":J
    :cond_1
    :goto_0
    return-wide v0

    .line 369
    .restart local v0    # "operation":J
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mMediaSetType:I

    if-nez v2, :cond_3

    .line 370
    or-long v2, v0, v8

    or-long/2addr v2, v6

    const-wide/high16 v4, 0x400000000000000L

    or-long v0, v2, v4

    goto :goto_0

    .line 372
    :cond_3
    or-long v2, v0, v8

    or-long v0, v2, v6

    goto :goto_0

    .line 373
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v2, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getClusterKind()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    goto :goto_0
.end method

.method public getTotalMediaItemCount()I
    .locals 2

    .prologue
    .line 308
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v1

    .line 309
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isDirty()Z
    .locals 4

    .prologue
    .line 417
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mArcDataVersion:J

    invoke-static {}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getArcVersionNumber()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x1

    return v0
.end method

.method public isUntaggedAlbum()Z
    .locals 1

    .prologue
    .line 402
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mUntaggedAlbum:Z

    return v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->notifyContentChanged()V

    .line 356
    return-void
.end method

.method public reload()J
    .locals 20

    .prologue
    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/MediaSet;->getDataVersion()J

    move-result-wide v12

    .line 317
    .local v12, "oldVersion":J
    const-wide/16 v10, -0x1

    .line 318
    .local v10, "newVersion":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 319
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v10

    .line 320
    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    cmp-long v17, v10, v12

    if-lez v17, :cond_5

    .line 323
    const/4 v9, 0x0

    .line 324
    .local v9, "mContainsData":Z
    const/4 v7, 0x0

    .line 325
    .local v7, "index":I
    const/4 v14, 0x0

    .line 326
    .local v14, "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 327
    :try_start_1
    new-instance v15, Ljava/util/LinkedHashSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 328
    .end local v14    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .local v15, "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :try_start_2
    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 329
    invoke-virtual {v15}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 330
    .local v16, "word":Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 331
    .local v8, "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getClusterAlbumByKeyword(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/ClusterAlbum;

    move-result-object v4

    .line 332
    .local v4, "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    if-eqz v4, :cond_1

    .line 333
    const/4 v9, 0x1

    .line 334
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v0, v4

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v5, v0, :cond_0

    .line 335
    aget-object v17, v4, v5

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 334
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 320
    .end local v4    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v5    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "index":I
    .end local v8    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v9    # "mContainsData":Z
    .end local v15    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .end local v16    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v17

    :try_start_3
    monitor-exit v18
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v17

    .line 328
    .restart local v7    # "index":I
    .restart local v9    # "mContainsData":Z
    .restart local v14    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :catchall_1
    move-exception v17

    :goto_2
    :try_start_4
    monitor-exit v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v17

    .line 336
    .end local v14    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .restart local v4    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .restart local v5    # "i":I
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v8    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v15    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .restart local v16    # "word":Ljava/lang/String;
    :cond_0
    if-nez v7, :cond_2

    const/16 v17, 0x1

    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/gallery3d/data/SearchAlbum;->addItems(Ljava/util/ArrayList;Z)V

    .line 338
    .end local v5    # "i":I
    :cond_1
    add-int/lit8 v7, v7, 0x1

    .line 339
    goto :goto_0

    .line 336
    .restart local v5    # "i":I
    :cond_2
    const/16 v17, 0x0

    goto :goto_3

    .line 341
    .end local v4    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v5    # "i":I
    .end local v8    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v16    # "word":Ljava/lang/String;
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->runItems()V

    .line 342
    if-nez v9, :cond_4

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 344
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->clear()V

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPathExs:Ljava/util/HashSet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->clear()V

    .line 346
    monitor-exit v18
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 347
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/data/SearchAlbum;->nextVersionNumber()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataVersion:J

    .line 350
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "index":I
    .end local v9    # "mContainsData":Z
    .end local v15    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :goto_4
    return-wide v18

    .line 346
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v7    # "index":I
    .restart local v9    # "mContainsData":Z
    .restart local v15    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :catchall_2
    move-exception v17

    :try_start_6
    monitor-exit v18
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v17

    .line 350
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "index":I
    .end local v9    # "mContainsData":Z
    .end local v15    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :cond_5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataVersion:J

    move-wide/from16 v18, v0

    goto :goto_4

    .line 328
    .restart local v7    # "index":I
    .restart local v9    # "mContainsData":Z
    .restart local v15    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :catchall_3
    move-exception v17

    move-object v14, v15

    .end local v15    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .restart local v14    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    goto :goto_2
.end method

.method public removeKeyword(Ljava/lang/String;)Z
    .locals 12
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 115
    const/4 v6, 0x0

    .line 116
    .local v6, "result":Z
    const/4 v1, 0x0

    .line 117
    .local v1, "containsData":Z
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v11

    .line 118
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    invoke-virtual {v10, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    move-result v6

    .line 119
    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    if-eqz v6, :cond_4

    .line 122
    const/4 v4, 0x0

    .line 123
    .local v4, "index":I
    const/4 v7, 0x0

    .line 124
    .local v7, "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v11

    .line 125
    :try_start_1
    new-instance v8, Ljava/util/LinkedHashSet;

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    invoke-direct {v8, v10}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126
    .end local v7    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .local v8, "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :try_start_2
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 127
    invoke-virtual {v8}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 128
    .local v9, "word":Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v5, "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .line 130
    .local v0, "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    monitor-enter v11

    .line 131
    :try_start_3
    invoke-direct {p0, v9}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getClusterAlbumByKeyword(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/ClusterAlbum;

    move-result-object v0

    .line 132
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 133
    if-eqz v0, :cond_1

    .line 134
    const/4 v1, 0x1

    .line 135
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v10, v0

    if-ge v2, v10, :cond_0

    .line 136
    aget-object v10, v0, v2

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 119
    .end local v0    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "index":I
    .end local v5    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v8    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .end local v9    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v10

    :try_start_4
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v10

    .line 126
    .restart local v4    # "index":I
    .restart local v7    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :catchall_1
    move-exception v10

    :goto_2
    :try_start_5
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v10

    .line 132
    .end local v7    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .restart local v0    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .restart local v8    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .restart local v9    # "word":Ljava/lang/String;
    :catchall_2
    move-exception v10

    :try_start_6
    monitor-exit v11
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v10

    .line 138
    .restart local v2    # "i":I
    :cond_0
    if-nez v4, :cond_2

    const/4 v10, 0x1

    :goto_3
    invoke-virtual {p0, v5, v10}, Lcom/sec/android/gallery3d/data/SearchAlbum;->addItems(Ljava/util/ArrayList;Z)V

    .line 140
    .end local v2    # "i":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 141
    goto :goto_0

    .line 138
    .restart local v2    # "i":I
    :cond_2
    const/4 v10, 0x0

    goto :goto_3

    .line 143
    .end local v0    # "clusterAlbum":[Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v2    # "i":I
    .end local v5    # "itemPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v9    # "word":Ljava/lang/String;
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->runItems()V

    .line 146
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "index":I
    .end local v8    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :cond_4
    if-nez v1, :cond_5

    .line 147
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mKeywordList:Ljava/util/LinkedHashSet;

    monitor-enter v11

    .line 148
    :try_start_7
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;

    invoke-virtual {v10}, Ljava/util/HashSet;->clear()V

    .line 149
    monitor-exit v11

    .line 150
    :cond_5
    return v6

    .line 149
    :catchall_3
    move-exception v10

    monitor-exit v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v10

    .line 126
    .restart local v4    # "index":I
    .restart local v8    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    :catchall_4
    move-exception v10

    move-object v7, v8

    .end local v8    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    .restart local v7    # "tempKeywordList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    goto :goto_2
.end method

.method public setArcDataVersion()V
    .locals 2

    .prologue
    .line 413
    invoke-static {}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getArcVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mArcDataVersion:J

    .line 414
    return-void
.end method

.method public setKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mAlbumKey:Ljava/lang/String;

    .line 552
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mName:Ljava/lang/String;

    .line 265
    return-void
.end method

.method public setNameID(Ljava/lang/String;)V
    .locals 0
    .param p1, "nameId"    # Ljava/lang/String;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mNameID:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public setUntaggedAlbum(Z)V
    .locals 0
    .param p1, "untaggedAlbum"    # Z

    .prologue
    .line 398
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mUntaggedAlbum:Z

    .line 399
    return-void
.end method

.method public updateDataVersion()V
    .locals 2

    .prologue
    .line 586
    invoke-static {}, Lcom/sec/android/gallery3d/data/SearchAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataVersion:J

    .line 587
    return-void
.end method

.method public updateFaces(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "joinedName"    # Ljava/lang/String;

    .prologue
    .line 568
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 569
    .local v0, "personId":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1, p1, p2, v0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    .line 570
    return-void
.end method

.method public updateMediaItems(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 590
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v8, 0x0

    .line 591
    .local v8, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v7, 0x0

    .local v7, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .local v9, "n":I
    :goto_0
    if-ge v7, v9, :cond_2

    .line 593
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_1

    .line 594
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 595
    .restart local v8    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v6, 0x0

    .line 596
    .local v6, "cursor":Landroid/database/Cursor;
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget v2, v8, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 597
    .local v1, "uri":Landroid/net/Uri;
    const-string v5, "datetaken DESC, _id DESC"

    .line 599
    .local v5, "strOrderClause":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 600
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/data/LocalImage;->updateContent(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 603
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 591
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v5    # "strOrderClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 603
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v5    # "strOrderClause":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 606
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v5    # "strOrderClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    return-void
.end method

.method public updateMediaSet()V
    .locals 0

    .prologue
    .line 580
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->updateMediaSet()V

    .line 581
    invoke-static {}, Lcom/sec/android/gallery3d/data/SearchAlbum;->setVersion()V

    .line 582
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->onContentDirty()V

    .line 583
    return-void
.end method

.method public updateOneFace(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "personId"    # I

    .prologue
    .line 573
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 574
    .local v0, "pathS":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 575
    .local v1, "values":[Ljava/lang/String;
    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {p1, v2, p3}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    .line 576
    return-void
.end method

.method public updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 8
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "operationType"    # I
    .param p4, "joinedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 421
    .local p2, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const-string v0, "reload"

    const-string v1, "clusteralbum showProcessingDialog"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    .line 423
    .local v4, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v7

    new-instance v0, Lcom/sec/android/gallery3d/data/SearchAlbum$UpdateOperation;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mName:Ljava/lang/String;

    move-object v1, p0

    move-object v3, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/data/SearchAlbum$UpdateOperation;-><init>(Lcom/sec/android/gallery3d/data/SearchAlbum;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Context;ILjava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 424
    return-void
.end method

.method public updateSlotName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "joinedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 406
    .local p2, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p3, :cond_0

    .line 407
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/SearchAlbum;->mName:Ljava/lang/String;

    .line 409
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/sec/android/gallery3d/data/SearchAlbum;->updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;)V

    .line 410
    return-void
.end method
