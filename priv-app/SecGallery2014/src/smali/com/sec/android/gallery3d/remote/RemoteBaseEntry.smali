.class public abstract Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;
.super Ljava/lang/Object;
.source "RemoteBaseEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/RemoteBaseEntry$Column;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RemoteTableEntry"


# instance fields
.field public _id:J

.field protected isValid:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->isValid:Z

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->_id:J

    .line 33
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->_id:J

    .line 39
    return-void
.end method

.method public cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;)Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;",
            ">(",
            "Landroid/database/Cursor;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .local p2, "entry":Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;, "TT;"
    const/4 v9, 0x1

    .line 67
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v4

    .line 68
    .local v4, "fields":[Ljava/lang/reflect/Field;
    const/4 v5, 0x0

    .line 69
    .local v5, "i":I
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_9

    aget-object v2, v0, v6

    .line 70
    .local v2, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    .line 71
    .local v3, "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v8, Ljava/lang/String;

    if-ne v3, v8, :cond_0

    .line 72
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, p2, v8}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 90
    :goto_1
    add-int/lit8 v5, v5, 0x1

    .line 69
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 73
    :cond_0
    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v3, v8, :cond_2

    .line 74
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getShort(I)S

    move-result v8

    if-ne v8, v9, :cond_1

    move v8, v9

    :goto_2
    invoke-virtual {v2, p2, v8}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 93
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .end local v3    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "fields":[Ljava/lang/reflect/Field;
    .end local v5    # "i":I
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/IllegalAccessException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 74
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .restart local v0    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v2    # "field":Ljava/lang/reflect/Field;
    .restart local v3    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v4    # "fields":[Ljava/lang/reflect/Field;
    .restart local v5    # "i":I
    .restart local v6    # "i$":I
    .restart local v7    # "len$":I
    :cond_1
    const/4 v8, 0x0

    goto :goto_2

    .line 75
    :cond_2
    :try_start_1
    sget-object v8, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-ne v3, v8, :cond_3

    .line 76
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getShort(I)S

    move-result v8

    invoke-virtual {v2, p2, v8}, Ljava/lang/reflect/Field;->setShort(Ljava/lang/Object;S)V

    goto :goto_1

    .line 77
    :cond_3
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v3, v8, :cond_4

    .line 78
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v2, p2, v8}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    goto :goto_1

    .line 79
    :cond_4
    sget-object v8, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v3, v8, :cond_5

    .line 80
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v2, p2, v10, v11}, Ljava/lang/reflect/Field;->setLong(Ljava/lang/Object;J)V

    goto :goto_1

    .line 81
    :cond_5
    sget-object v8, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v3, v8, :cond_6

    .line 82
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    invoke-virtual {v2, p2, v8}, Ljava/lang/reflect/Field;->setFloat(Ljava/lang/Object;F)V

    goto :goto_1

    .line 83
    :cond_6
    sget-object v8, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v3, v8, :cond_7

    .line 84
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v2, p2, v8}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 85
    :cond_7
    const-class v8, [B

    if-ne v3, v8, :cond_8

    .line 86
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    invoke-virtual {v2, p2, v8}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 88
    :cond_8
    const-string v8, "RemoteTableEntry"

    const-string v10, "No writable Data"

    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 92
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .end local v3    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_9
    return-object p2
.end method

.method public doNotMerge(Ljava/lang/String;)Z
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method public getExtraSql(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getFields()[Ljava/lang/reflect/Field;
.end method

.method public abstract getTableName()Ljava/lang/String;
.end method

.method public needIndex(Ljava/lang/String;)Z
    .locals 1
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public abstract setProperty(Ljava/lang/Object;)V
.end method

.method public valid()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/RemoteBaseEntry;->isValid:Z

    return v0
.end method
