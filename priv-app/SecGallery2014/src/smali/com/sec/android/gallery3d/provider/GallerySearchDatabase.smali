.class public Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;
.super Ljava/lang/Object;
.source "GallerySearchDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;
    }
.end annotation


# static fields
.field public static final CONTEXTUAL_TAGS_DB:Ljava/lang/String; = "content://media/external/contextural_tags/"

.field public static final FACES_DATABASE:Ljava/lang/String; = "content://media/external/faces"

.field private static final KEY_EXTRA_FLAGS:Ljava/lang/String; = "suggest_extra_flags"

.field private static final KEY_FILEURI_STRING:Ljava/lang/String; = "suggest_data_to_share"

.field private static final KEY_FINENAME_STRING:Ljava/lang/String; = "suggest_text_5"

.field private static final KEY_IMAGE_DATE:Ljava/lang/String; = "suggest_text_1"

.field private static final KEY_IMAGE_TAG_CONTENTS_URI:Ljava/lang/String; = "suggest_tag_content_uri"

.field private static final KEY_IMAGE_TAG_CREATE_TIME:Ljava/lang/String; = "suggest_tag_create_time"

.field private static final KEY_IMAGE_TAG_ENCODE:Ljava/lang/String; = "suggest_tag_encode"

.field private static final KEY_IMAGE_TAG_TYPE:Ljava/lang/String; = "suggest_tag_type"

.field private static final KEY_IMAGE_TAG_VALUE:Ljava/lang/String; = "suggest_tag_value"

.field private static final KEY_IMAGE_URI:Ljava/lang/String; = "suggest_icon_1"

.field private static final KEY_INK_DATA:Ljava/lang/String; = "suggest_ink_data"

.field private static final KEY_LOCATION_STRING:Ljava/lang/String; = "suggest_text_3"

.field private static final KEY_MIME_TYPE:Ljava/lang/String; = "suggest_mime_type"

.field private static final KEY_PEOPLE_STRING:Ljava/lang/String; = "suggest_text_4"

.field private static final KEY_TARGET_TYPE:Ljava/lang/String; = "suggest_target_type"

.field private static final KEY_URI:Ljava/lang/String; = "suggest_uri"

.field private static final KEY_USERTAG_STRING:Ljava/lang/String; = "suggest_text_2"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

.field regexColumns:[Ljava/lang/String;

.field tagColumns:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    .line 59
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "suggest_icon_1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "suggest_text_1"

    aput-object v1, v0, v4

    const-string/jumbo v1, "suggest_uri"

    aput-object v1, v0, v5

    const-string/jumbo v1, "suggest_mime_type"

    aput-object v1, v0, v6

    const-string/jumbo v1, "suggest_text_2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "suggest_text_3"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "suggest_text_4"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "suggest_target_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "suggest_extra_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "suggest_ink_data"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "suggest_text_5"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "suggest_data_to_share"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "suggest_intent_data_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->regexColumns:[Ljava/lang/String;

    .line 74
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "suggest_tag_type"

    aput-object v1, v0, v3

    const-string/jumbo v1, "suggest_tag_value"

    aput-object v1, v0, v4

    const-string/jumbo v1, "suggest_tag_content_uri"

    aput-object v1, v0, v5

    const-string/jumbo v1, "suggest_tag_create_time"

    aput-object v1, v0, v6

    const-string/jumbo v1, "suggest_tag_encode"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "suggest_text_5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "suggest_intent_data_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->tagColumns:[Ljava/lang/String;

    .line 56
    iput-object p1, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    .line 57
    return-void
.end method

.method private addStrokeRow(Landroid/database/MatrixCursor;Lcom/sec/android/gallery3d/provider/SearchParser;)Landroid/database/MatrixCursor;
    .locals 29
    .param p1, "matrixCursor"    # Landroid/database/MatrixCursor;
    .param p2, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;

    .prologue
    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->select(Lcom/sec/android/gallery3d/provider/SearchParser;)Landroid/database/Cursor;

    move-result-object v11

    .line 332
    .local v11, "cur1":Landroid/database/Cursor;
    if-nez v11, :cond_0

    .line 411
    :goto_0
    return-object p1

    .line 336
    :cond_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 337
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 338
    .local v10, "count":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v14

    .line 339
    .local v14, "df":Ljava/text/DateFormat;
    const-string v22, "image/*"

    .line 341
    .local v22, "mimetype":Ljava/lang/String;
    const/4 v13, 0x0

    .line 342
    .local v13, "dateString":Ljava/lang/String;
    const/16 v20, 0x0

    .local v20, "inkData":Ljava/lang/String;
    const/16 v26, 0x0

    .local v26, "targetType":Ljava/lang/String;
    const/16 v17, 0x0

    .local v17, "extraFlag":Ljava/lang/String;
    const/16 v18, 0x0

    .line 343
    .local v18, "fileName":Ljava/lang/String;
    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "datetaken"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "_display_name"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "_data"

    aput-object v7, v6, v4

    .line 346
    .local v6, "projection":[Ljava/lang/String;
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_1
    move/from16 v0, v19

    if-ge v0, v10, :cond_8

    .line 347
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "/"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "media_id"

    invoke-interface {v11, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v11, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 348
    .local v27, "uriPass":Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 349
    .local v5, "uri":Landroid/net/Uri;
    const/16 v24, 0x0

    .line 350
    .local v24, "photoFrameUri":Landroid/net/Uri;
    const-string v4, "media_id"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v21

    .line 351
    .local v21, "mediaId":I
    const/4 v12, 0x0

    .line 353
    .local v12, "cur2":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    .line 354
    if-nez v12, :cond_1

    .line 398
    :try_start_2
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 404
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 406
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 407
    :catch_0
    move-exception v4

    goto/16 :goto_0

    .line 355
    :cond_1
    :try_start_4
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-gtz v4, :cond_2

    .line 356
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->delete(I)Z

    .line 357
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 398
    :try_start_5
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 346
    :goto_2
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 360
    :cond_2
    :try_start_6
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 361
    const-string v4, "datetaken"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 362
    .local v15, "dt":Ljava/lang/String;
    const-string v4, "_display_name"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v18

    .line 364
    if-eqz v15, :cond_3

    .line 365
    :try_start_7
    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v14, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v13

    .line 370
    :cond_3
    :goto_3
    :try_start_8
    const-string v4, "photo"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 371
    const-string v4, "photo"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    .line 372
    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 375
    :cond_4
    const-string/jumbo v4, "type"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    .line 376
    const-string v26, "0"

    .line 377
    const-string v17, "0x0000"

    .line 387
    :cond_5
    :goto_4
    const-string/jumbo v4, "word"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 388
    .local v28, "word":Ljava/lang/String;
    const-string v4, "region"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 389
    .local v25, "region":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getRegionString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 391
    const/16 v4, 0xd

    new-array v0, v4, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/4 v4, 0x0

    aput-object v5, v23, v4

    const/4 v4, 0x1

    aput-object v13, v23, v4

    const/4 v4, 0x2

    aput-object v24, v23, v4

    const/4 v4, 0x3

    aput-object v22, v23, v4

    const/4 v4, 0x4

    const/4 v7, 0x0

    aput-object v7, v23, v4

    const/4 v4, 0x5

    const/4 v7, 0x0

    aput-object v7, v23, v4

    const/4 v4, 0x6

    const/4 v7, 0x0

    aput-object v7, v23, v4

    const/4 v4, 0x7

    aput-object v26, v23, v4

    const/16 v4, 0x8

    aput-object v17, v23, v4

    const/16 v4, 0x9

    aput-object v20, v23, v4

    const/16 v4, 0xa

    aput-object v18, v23, v4

    const/16 v4, 0xb

    const/4 v7, 0x0

    aput-object v7, v23, v4

    const/16 v4, 0xc

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v23, v4

    .line 395
    .local v23, "objects":[Ljava/lang/Object;
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 396
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 398
    :try_start_9
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_2

    .line 401
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "count":I
    .end local v12    # "cur2":Landroid/database/Cursor;
    .end local v13    # "dateString":Ljava/lang/String;
    .end local v14    # "df":Ljava/text/DateFormat;
    .end local v15    # "dt":Ljava/lang/String;
    .end local v17    # "extraFlag":Ljava/lang/String;
    .end local v18    # "fileName":Ljava/lang/String;
    .end local v19    # "i":I
    .end local v20    # "inkData":Ljava/lang/String;
    .end local v21    # "mediaId":I
    .end local v22    # "mimetype":Ljava/lang/String;
    .end local v23    # "objects":[Ljava/lang/Object;
    .end local v24    # "photoFrameUri":Landroid/net/Uri;
    .end local v25    # "region":Ljava/lang/String;
    .end local v26    # "targetType":Ljava/lang/String;
    .end local v27    # "uriPass":Ljava/lang/String;
    .end local v28    # "word":Ljava/lang/String;
    :catch_1
    move-exception v16

    .line 402
    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_a
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 404
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 406
    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    goto/16 :goto_0

    .line 407
    :catch_2
    move-exception v4

    goto/16 :goto_0

    .line 367
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v5    # "uri":Landroid/net/Uri;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v10    # "count":I
    .restart local v12    # "cur2":Landroid/database/Cursor;
    .restart local v13    # "dateString":Ljava/lang/String;
    .restart local v14    # "df":Ljava/text/DateFormat;
    .restart local v15    # "dt":Ljava/lang/String;
    .restart local v17    # "extraFlag":Ljava/lang/String;
    .restart local v18    # "fileName":Ljava/lang/String;
    .restart local v19    # "i":I
    .restart local v20    # "inkData":Ljava/lang/String;
    .restart local v21    # "mediaId":I
    .restart local v22    # "mimetype":Ljava/lang/String;
    .restart local v24    # "photoFrameUri":Landroid/net/Uri;
    .restart local v26    # "targetType":Ljava/lang/String;
    .restart local v27    # "uriPass":Ljava/lang/String;
    :catch_3
    move-exception v16

    .line 368
    .local v16, "e":Ljava/lang/Exception;
    :try_start_c
    invoke-virtual/range {p1 .. p1}, Landroid/database/MatrixCursor;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_3

    .line 398
    .end local v15    # "dt":Ljava/lang/String;
    .end local v16    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :try_start_d
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 404
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "count":I
    .end local v12    # "cur2":Landroid/database/Cursor;
    .end local v13    # "dateString":Ljava/lang/String;
    .end local v14    # "df":Ljava/text/DateFormat;
    .end local v17    # "extraFlag":Ljava/lang/String;
    .end local v18    # "fileName":Ljava/lang/String;
    .end local v19    # "i":I
    .end local v20    # "inkData":Ljava/lang/String;
    .end local v21    # "mediaId":I
    .end local v22    # "mimetype":Ljava/lang/String;
    .end local v24    # "photoFrameUri":Landroid/net/Uri;
    .end local v26    # "targetType":Ljava/lang/String;
    .end local v27    # "uriPass":Ljava/lang/String;
    :catchall_1
    move-exception v4

    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 406
    :try_start_e
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5

    .line 408
    :goto_5
    throw v4

    .line 379
    .restart local v5    # "uri":Landroid/net/Uri;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v10    # "count":I
    .restart local v12    # "cur2":Landroid/database/Cursor;
    .restart local v13    # "dateString":Ljava/lang/String;
    .restart local v14    # "df":Ljava/text/DateFormat;
    .restart local v15    # "dt":Ljava/lang/String;
    .restart local v17    # "extraFlag":Ljava/lang/String;
    .restart local v18    # "fileName":Ljava/lang/String;
    .restart local v19    # "i":I
    .restart local v20    # "inkData":Ljava/lang/String;
    .restart local v21    # "mediaId":I
    .restart local v22    # "mimetype":Ljava/lang/String;
    .restart local v24    # "photoFrameUri":Landroid/net/Uri;
    .restart local v26    # "targetType":Ljava/lang/String;
    .restart local v27    # "uriPass":Ljava/lang/String;
    :cond_6
    :try_start_f
    const-string/jumbo v4, "type"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "1"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 380
    const-string v17, "0x0001"

    .line 381
    const-string v26, "1"

    goto/16 :goto_4

    .line 382
    :cond_7
    const-string/jumbo v4, "type"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "2"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 383
    const-string v17, "0x0002"

    .line 384
    const-string v26, "2"
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_4

    .line 404
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v12    # "cur2":Landroid/database/Cursor;
    .end local v15    # "dt":Ljava/lang/String;
    .end local v21    # "mediaId":I
    .end local v24    # "photoFrameUri":Landroid/net/Uri;
    .end local v27    # "uriPass":Ljava/lang/String;
    :cond_8
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 406
    :try_start_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4

    goto/16 :goto_0

    .line 407
    :catch_4
    move-exception v4

    goto/16 :goto_0

    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "count":I
    .end local v13    # "dateString":Ljava/lang/String;
    .end local v14    # "df":Ljava/text/DateFormat;
    .end local v17    # "extraFlag":Ljava/lang/String;
    .end local v18    # "fileName":Ljava/lang/String;
    .end local v19    # "i":I
    .end local v20    # "inkData":Ljava/lang/String;
    .end local v22    # "mimetype":Ljava/lang/String;
    .end local v26    # "targetType":Ljava/lang/String;
    :catch_5
    move-exception v7

    goto :goto_5
.end method

.method private getImageFilter(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "select"    # Ljava/lang/String;

    .prologue
    .line 611
    move-object v3, p1

    .line 612
    .local v3, "selection":Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 613
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "_data"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    const-string v4, "latitude"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    const-string v4, "longitude"

    aput-object v4, v2, v0

    const/4 v0, 0x4

    const-string v4, "datetaken"

    aput-object v4, v2, v0

    const/4 v0, 0x5

    const-string v4, "addr"

    aput-object v4, v2, v0

    const/4 v0, 0x6

    const-string v4, "_display_name"

    aput-object v4, v2, v0

    .line 614
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const-string v5, "datetaken DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 615
    .local v6, "cursor":Landroid/database/Cursor;
    return-object v6
.end method

.method private getKey(Lcom/sec/android/gallery3d/provider/SearchParser;)Ljava/lang/String;
    .locals 7
    .param p1, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;

    .prologue
    .line 790
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyString()Ljava/lang/String;

    move-result-object v0

    .line 791
    .local v0, "full_name":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 792
    const/4 v4, 0x0

    .line 808
    :cond_0
    return-object v4

    .line 795
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 797
    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 798
    .local v3, "names":[Ljava/lang/String;
    const-string v4, ""

    .line 799
    .local v4, "selection_part":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v3

    if-ge v1, v5, :cond_0

    .line 800
    aget-object v2, v3, v1

    .line 801
    .local v2, "name":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 802
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 803
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ( LOWER ( _display_name )  LIKE  \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "LOWER ( _display_name ) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NOT LIKE \'%.%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' )"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 799
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getLocationString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 538
    if-nez p1, :cond_1

    .line 546
    .end local p2    # "location":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p2

    .line 540
    .restart local p2    # "location":Ljava/lang/String;
    :cond_1
    const-string v2, "\\|"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 541
    .local v1, "locationValue":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 542
    aget-object v2, v1, v0

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 543
    aget-object p2, v1, v0

    goto :goto_0

    .line 541
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getPeopleString(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    if-nez p1, :cond_1

    .line 506
    const/4 v2, 0x0

    .line 514
    :cond_0
    return-object v2

    .line 507
    :cond_1
    const-string v2, ""

    .line 508
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;

    .line 509
    .local v0, "a":Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 510
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 511
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 512
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getPeriod(Lcom/sec/android/gallery3d/provider/SearchParser;)Ljava/lang/String;
    .locals 8
    .param p1, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;

    .prologue
    const-wide/16 v6, 0x0

    .line 812
    const-string v2, ""

    .line 813
    .local v2, "selection_part":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getStartTime()J

    move-result-wide v4

    .line 814
    .local v4, "stime":J
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getEndTime()J

    move-result-wide v0

    .line 815
    .local v0, "etime":J
    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    cmp-long v3, v0, v6

    if-eqz v3, :cond_0

    .line 816
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "( CAST ( datetaken AS DECIMAL)    >= "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " AND  CAST (datetaken AS DECIMAL) <= "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " )"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 819
    :cond_0
    return-object v2
.end method

.method private getPersonIdToName(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 937
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 938
    .local v8, "personId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v0, "content://media/external/persons"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 939
    .local v1, "persons_uri":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "name"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "_id"

    aput-object v4, v2, v0

    .line 940
    .local v2, "proj":[Ljava/lang/String;
    const-string v3, ""

    .line 943
    .local v3, "select":Ljava/lang/String;
    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v8, v9

    .line 960
    .end local v8    # "personId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_0
    return-object v8

    .line 945
    .restart local v8    # "personId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "name LIKE \'%"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "%\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 947
    const/4 v6, 0x0

    .line 949
    .local v6, "curs":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 950
    if-nez v6, :cond_1

    .line 957
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v8, v9

    goto :goto_0

    .line 951
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 953
    :cond_2
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 954
    .local v7, "pId":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 955
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 957
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "pId":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getQuerySelection(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 999
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, ""

    .line 1000
    .local v5, "result":Ljava/lang/String;
    const-string v2, ""

    .line 1002
    .local v2, "event":Ljava/lang/String;
    const/4 v4, 0x1

    .line 1004
    .local v4, "onlyUserTag":Z
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1005
    .local v6, "size":I
    if-lez v6, :cond_4

    .line 1006
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "(("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1008
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_3

    .line 1009
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1010
    if-eqz v4, :cond_1

    .line 1011
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(user_tags = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\')"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1017
    :goto_1
    const-string v7, "("

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    if-nez v3, :cond_2

    .line 1018
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1008
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1013
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "( scene_type = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\') OR ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "user_tags"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\') OR ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "subscene_type"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\')"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1020
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") AND ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 1023
    :cond_3
    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1024
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")) "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1030
    .end local v3    # "i":I
    .end local v6    # "size":I
    :cond_4
    :goto_3
    return-object v5

    .line 1027
    :catch_0
    move-exception v1

    .line 1028
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method private getRegionString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "region"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 519
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 520
    :cond_0
    const/4 v2, 0x0

    .line 533
    :cond_1
    return-object v2

    .line 522
    :cond_2
    const-string v4, "\\|"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 523
    .local v3, "wordSplit":[Ljava/lang/String;
    const-string v4, "\\|"

    invoke-virtual {p2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 524
    .local v1, "regionSplit":[Ljava/lang/String;
    const-string v2, ""

    .line 526
    .local v2, "returnRegion":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 527
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 528
    if-eqz v0, :cond_3

    .line 529
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 530
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 526
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getSelectionFilter(Lcom/sec/android/gallery3d/provider/SearchParser;Ljava/util/ArrayList;)Landroid/database/Cursor;
    .locals 17
    .param p1, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/provider/SearchParser;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 619
    .local p2, "selectedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getKey(Lcom/sec/android/gallery3d/provider/SearchParser;)Ljava/lang/String;

    move-result-object v7

    .line 620
    .local v7, "result":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getPeriod(Lcom/sec/android/gallery3d/provider/SearchParser;)Ljava/lang/String;

    move-result-object v5

    .line 621
    .local v5, "period":Ljava/lang/String;
    const-string v10, ""

    .line 624
    .local v10, "sql_query":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getPeopleName()Ljava/lang/String;

    move-result-object v4

    .line 625
    .local v4, "peopleName":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 626
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getPersonIdToName(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    if-eqz v14, :cond_1

    .line 627
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getPersonIdToName(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->parseContactIDToFaceID(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 628
    .local v3, "people":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v14, "FaceID : getSelectionFilter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "FaceID"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    :goto_0
    if-eqz v3, :cond_4

    .line 637
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_4

    .line 638
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "( ( faces_IDs <> \'\' AND ( "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 639
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 640
    .local v9, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v9, :cond_3

    .line 641
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 642
    .local v6, "person":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " faces_IDs = \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' OR "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " faces_IDs LIKE \'%,"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",%\' OR "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " faces_IDs LIKE \'%,"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\' OR "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " faces_IDs LIKE \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",%\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 644
    add-int/lit8 v14, v9, -0x1

    if-ge v1, v14, :cond_0

    .line 645
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " OR "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 640
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 630
    .end local v1    # "i":I
    .end local v3    # "people":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v6    # "person":I
    .end local v9    # "size":I
    :cond_1
    const/4 v3, 0x0

    .restart local v3    # "people":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 633
    .end local v3    # "people":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "people":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    goto/16 :goto_0

    .line 647
    .restart local v1    # "i":I
    .restart local v9    # "size":I
    :cond_3
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") )  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 651
    .end local v1    # "i":I
    .end local v9    # "size":I
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getWeather()I

    move-result v13

    .line 652
    .local v13, "weather":I
    if-lez v13, :cond_5

    const/4 v14, 0x6

    if-ge v13, v14, :cond_5

    .line 653
    const-string v14, ""

    invoke-virtual {v10, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_a

    .line 654
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " OR"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 657
    :goto_2
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " weather_ID = \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " OR weather_ID = \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    add-int/lit8 v15, v13, 0x64

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 660
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLocationValue()Ljava/lang/String;

    move-result-object v2

    .line 662
    .local v2, "location":Ljava/lang/String;
    if-nez p2, :cond_6

    .line 663
    new-instance p2, Ljava/util/ArrayList;

    .end local p2    # "selectedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct/range {p2 .. p2}, Ljava/util/ArrayList;-><init>()V

    .line 665
    .restart local p2    # "selectedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    const-string v14, ""

    invoke-virtual {v10, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_7

    .line 666
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->query(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    if-eqz v14, :cond_7

    .line 667
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->query(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 669
    :cond_7
    if-eqz v2, :cond_8

    .line 670
    const-string v10, "( "

    .line 671
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " addr LIKE \'%"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "%\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 672
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->queryToLocation(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    if-eqz v14, :cond_8

    .line 673
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->queryToLocation(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 676
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getUserDef()Ljava/util/ArrayList;

    move-result-object v11

    .line 677
    .local v11, "userdef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v11, :cond_c

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_c

    .line 678
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->queryToCategory(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v12

    .line 679
    .local v12, "userdefIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v12, :cond_c

    .line 680
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_b

    .line 681
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/lit8 v1, v14, -0x1

    .restart local v1    # "i":I
    :goto_3
    if-ltz v1, :cond_c

    .line 682
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 683
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 681
    :cond_9
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 656
    .end local v1    # "i":I
    .end local v2    # "location":Ljava/lang/String;
    .end local v11    # "userdef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "userdefIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a
    const-string v10, "( "

    goto/16 :goto_2

    .line 686
    .restart local v2    # "location":Ljava/lang/String;
    .restart local v11    # "userdef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "userdefIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 691
    .end local v12    # "userdefIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_c
    const-string v8, ""

    .line 692
    .local v8, "selection":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_f

    .line 693
    const-string v8, "_id in ("

    .line 694
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v1, v14, :cond_e

    .line 695
    if-lez v1, :cond_d

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 696
    :cond_d
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 694
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 698
    :cond_e
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 700
    if-eqz v7, :cond_12

    .line 701
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " OR "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 707
    .end local v1    # "i":I
    :cond_f
    :goto_5
    if-eqz v5, :cond_10

    const-string v14, ""

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_10

    .line 708
    if-eqz v7, :cond_13

    const-string v14, ""

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_13

    .line 709
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " AND "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 714
    :cond_10
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->allTagCheck()Z

    move-result v14

    if-nez v14, :cond_14

    if-eqz v7, :cond_11

    const-string v14, ""

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_14

    .line 715
    :cond_11
    const/4 v14, 0x0

    .line 717
    :goto_7
    return-object v14

    .line 703
    .restart local v1    # "i":I
    :cond_12
    move-object v7, v8

    goto :goto_5

    .line 711
    .end local v1    # "i":I
    :cond_13
    move-object v7, v5

    goto :goto_6

    .line 717
    :cond_14
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getImageFilter(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    goto :goto_7
.end method

.method private getUserTagString(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 562
    .local p1, "userDef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, ""

    .line 563
    .local v1, "userTagInfo":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 564
    if-lez v0, :cond_0

    .line 565
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 566
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 563
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 568
    :cond_1
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    .end local v1    # "userTagInfo":Ljava/lang/String;
    :cond_2
    return-object v1
.end method

.method private isLocationSame(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 551
    if-nez p1, :cond_1

    .line 558
    :cond_0
    :goto_0
    return v2

    .line 553
    :cond_1
    const-string v3, "\\|"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 554
    .local v1, "locationValue":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 555
    aget-object v3, v1, v0

    invoke-virtual {v3, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 556
    const/4 v2, 0x1

    goto :goto_0

    .line 554
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private onlyStrokeSearch(Lcom/sec/android/gallery3d/provider/SearchParser;)Landroid/database/MatrixCursor;
    .locals 31
    .param p1, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;

    .prologue
    .line 416
    new-instance v22, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->regexColumns:[Ljava/lang/String;

    const/16 v7, 0x3e8

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v7}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 417
    .local v22, "matrixCursor":Landroid/database/MatrixCursor;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->select(Lcom/sec/android/gallery3d/provider/SearchParser;)Landroid/database/Cursor;

    move-result-object v12

    .line 418
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 420
    .local v10, "c":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    .line 422
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 423
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v11

    .line 424
    .local v11, "count":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v14

    .line 425
    .local v14, "df":Ljava/text/DateFormat;
    const-string v24, "image/*"

    .line 427
    .local v24, "mimetype":Ljava/lang/String;
    const/4 v13, 0x0

    .line 428
    .local v13, "dateString":Ljava/lang/String;
    const/16 v21, 0x0

    .local v21, "inkData":Ljava/lang/String;
    const/16 v28, 0x0

    .local v28, "targetType":Ljava/lang/String;
    const/16 v17, 0x0

    .local v17, "extraFlag":Ljava/lang/String;
    const/16 v18, 0x0

    .line 430
    .local v18, "fileName":Ljava/lang/String;
    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "datetaken"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "_display_name"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "_data"

    aput-object v7, v6, v4

    .line 432
    .local v6, "projection":[Ljava/lang/String;
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    move/from16 v0, v20

    if-ge v0, v11, :cond_8

    .line 433
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "/"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "media_id"

    invoke-interface {v12, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v12, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 434
    .local v29, "uriPass":Ljava/lang/String;
    invoke-static/range {v29 .. v29}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 435
    .local v5, "uri":Landroid/net/Uri;
    const/16 v26, 0x0

    .line 436
    .local v26, "photoFrameUri":Landroid/net/Uri;
    const-string v4, "media_id"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 437
    .local v23, "mediaId":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 438
    if-nez v10, :cond_0

    .line 439
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->delete(I)Z

    .line 440
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 432
    :goto_1
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 442
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-gtz v4, :cond_2

    .line 443
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 444
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->delete(I)Z

    .line 445
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 446
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 489
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v11    # "count":I
    .end local v13    # "dateString":Ljava/lang/String;
    .end local v14    # "df":Ljava/text/DateFormat;
    .end local v17    # "extraFlag":Ljava/lang/String;
    .end local v18    # "fileName":Ljava/lang/String;
    .end local v20    # "i":I
    .end local v21    # "inkData":Ljava/lang/String;
    .end local v23    # "mediaId":I
    .end local v24    # "mimetype":Ljava/lang/String;
    .end local v26    # "photoFrameUri":Landroid/net/Uri;
    .end local v28    # "targetType":Ljava/lang/String;
    .end local v29    # "uriPass":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 490
    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 492
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 493
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 495
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 501
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    :goto_2
    return-object v22

    .line 449
    .restart local v5    # "uri":Landroid/net/Uri;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v11    # "count":I
    .restart local v13    # "dateString":Ljava/lang/String;
    .restart local v14    # "df":Ljava/text/DateFormat;
    .restart local v17    # "extraFlag":Ljava/lang/String;
    .restart local v18    # "fileName":Ljava/lang/String;
    .restart local v20    # "i":I
    .restart local v21    # "inkData":Ljava/lang/String;
    .restart local v23    # "mediaId":I
    .restart local v24    # "mimetype":Ljava/lang/String;
    .restart local v26    # "photoFrameUri":Landroid/net/Uri;
    .restart local v28    # "targetType":Ljava/lang/String;
    .restart local v29    # "uriPass":Ljava/lang/String;
    :cond_2
    :try_start_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 450
    const-string v4, "datetaken"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 451
    .local v15, "dt":Ljava/lang/String;
    const-string v4, "_display_name"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 452
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "_data"

    invoke-interface {v10, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v10, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v19

    .line 454
    .local v19, "fileUri":Landroid/net/Uri;
    if-eqz v15, :cond_3

    .line 455
    :try_start_4
    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v14, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v13

    .line 460
    :cond_3
    :goto_3
    :try_start_5
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 462
    const-string v4, "photo"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 463
    const-string v4, "photo"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v26

    .line 464
    invoke-virtual/range {v26 .. v26}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    .line 468
    :cond_4
    const-string/jumbo v4, "type"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    .line 469
    const-string v28, "0"

    .line 470
    const-string v17, "0x0000"

    .line 480
    :cond_5
    :goto_4
    const-string/jumbo v4, "word"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 481
    .local v30, "word":Ljava/lang/String;
    const-string v4, "region"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 482
    .local v27, "region":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getRegionString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 484
    const/16 v4, 0xd

    new-array v0, v4, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/4 v4, 0x0

    aput-object v5, v25, v4

    const/4 v4, 0x1

    aput-object v13, v25, v4

    const/4 v4, 0x2

    aput-object v26, v25, v4

    const/4 v4, 0x3

    aput-object v24, v25, v4

    const/4 v4, 0x4

    const/4 v7, 0x0

    aput-object v7, v25, v4

    const/4 v4, 0x5

    const/4 v7, 0x0

    aput-object v7, v25, v4

    const/4 v4, 0x6

    const/4 v7, 0x0

    aput-object v7, v25, v4

    const/4 v4, 0x7

    aput-object v28, v25, v4

    const/16 v4, 0x8

    aput-object v17, v25, v4

    const/16 v4, 0x9

    aput-object v21, v25, v4

    const/16 v4, 0xa

    aput-object v18, v25, v4

    const/16 v4, 0xb

    aput-object v19, v25, v4

    const/16 v4, 0xc

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v25, v4

    .line 485
    .local v25, "objects":[Ljava/lang/Object;
    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 487
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 492
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v11    # "count":I
    .end local v13    # "dateString":Ljava/lang/String;
    .end local v14    # "df":Ljava/text/DateFormat;
    .end local v15    # "dt":Ljava/lang/String;
    .end local v17    # "extraFlag":Ljava/lang/String;
    .end local v18    # "fileName":Ljava/lang/String;
    .end local v19    # "fileUri":Landroid/net/Uri;
    .end local v20    # "i":I
    .end local v21    # "inkData":Ljava/lang/String;
    .end local v23    # "mediaId":I
    .end local v24    # "mimetype":Ljava/lang/String;
    .end local v25    # "objects":[Ljava/lang/Object;
    .end local v26    # "photoFrameUri":Landroid/net/Uri;
    .end local v27    # "region":Ljava/lang/String;
    .end local v28    # "targetType":Ljava/lang/String;
    .end local v29    # "uriPass":Ljava/lang/String;
    .end local v30    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v4

    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 493
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 495
    :try_start_6
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 497
    :goto_5
    throw v4

    .line 457
    .restart local v5    # "uri":Landroid/net/Uri;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v11    # "count":I
    .restart local v13    # "dateString":Ljava/lang/String;
    .restart local v14    # "df":Ljava/text/DateFormat;
    .restart local v15    # "dt":Ljava/lang/String;
    .restart local v17    # "extraFlag":Ljava/lang/String;
    .restart local v18    # "fileName":Ljava/lang/String;
    .restart local v19    # "fileUri":Landroid/net/Uri;
    .restart local v20    # "i":I
    .restart local v21    # "inkData":Ljava/lang/String;
    .restart local v23    # "mediaId":I
    .restart local v24    # "mimetype":Ljava/lang/String;
    .restart local v26    # "photoFrameUri":Landroid/net/Uri;
    .restart local v28    # "targetType":Ljava/lang/String;
    .restart local v29    # "uriPass":Ljava/lang/String;
    :catch_1
    move-exception v16

    .line 458
    .local v16, "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual/range {v22 .. v22}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_3

    .line 472
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_6
    const-string/jumbo v4, "type"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "1"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 473
    const-string v17, "0x0001"

    .line 474
    const-string v28, "1"

    goto/16 :goto_4

    .line 475
    :cond_7
    const-string/jumbo v4, "type"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "2"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 476
    const-string v17, "0x0002"

    .line 477
    const-string v28, "2"
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_4

    .line 492
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v15    # "dt":Ljava/lang/String;
    .end local v19    # "fileUri":Landroid/net/Uri;
    .end local v23    # "mediaId":I
    .end local v26    # "photoFrameUri":Landroid/net/Uri;
    .end local v29    # "uriPass":Ljava/lang/String;
    :cond_8
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 493
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 495
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_2

    .line 496
    :catch_2
    move-exception v4

    goto/16 :goto_2

    .end local v6    # "projection":[Ljava/lang/String;
    .end local v11    # "count":I
    .end local v13    # "dateString":Ljava/lang/String;
    .end local v14    # "df":Ljava/text/DateFormat;
    .end local v17    # "extraFlag":Ljava/lang/String;
    .end local v18    # "fileName":Ljava/lang/String;
    .end local v20    # "i":I
    .end local v21    # "inkData":Ljava/lang/String;
    .end local v24    # "mimetype":Ljava/lang/String;
    .end local v28    # "targetType":Ljava/lang/String;
    :catch_3
    move-exception v7

    goto :goto_5

    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    :catch_4
    move-exception v4

    goto/16 :goto_2
.end method

.method private parseContactIDToFaceID(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 823
    .local p1, "contactID":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object v8, p1

    .line 824
    .local v8, "face_ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 825
    .local v9, "faces_ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 826
    .local v6, "cursor":Landroid/database/Cursor;
    const-string v0, "content://media/external/faces"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 827
    .local v1, "faces_uri":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "person_id"

    aput-object v4, v2, v0

    .line 828
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, ""

    .line 829
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 831
    .local v12, "size":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v12, :cond_1

    .line 832
    :try_start_0
    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 833
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " OR "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 834
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " person_id = \'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 831
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 836
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 837
    if-eqz v6, :cond_5

    .line 838
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 839
    const/4 v11, 0x0

    .line 842
    .local v11, "s":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 843
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 844
    :cond_3
    if-eqz v11, :cond_4

    .line 845
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 846
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 851
    .end local v11    # "s":Ljava/lang/String;
    :cond_5
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 853
    :goto_1
    return-object v9

    .line 848
    :catch_0
    move-exception v7

    .line 849
    .local v7, "e":Ljava/lang/NullPointerException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 851
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private parseFacesToContact(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 28
    .param p1, "faces_IDs"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 857
    if-nez p1, :cond_0

    .line 858
    const/4 v15, 0x0

    .line 915
    :goto_0
    return-object v15

    .line 859
    :cond_0
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 860
    .local v16, "contactsIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, ","

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 861
    .local v27, "values":[Ljava/lang/String;
    if-nez v27, :cond_1

    .line 862
    const/4 v15, 0x0

    goto :goto_0

    .line 863
    :cond_1
    move-object/from16 v13, v27

    .local v13, "arr$":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v22, 0x0

    .local v22, "i$":I
    :goto_1
    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_2

    aget-object v12, v13, v22

    .line 864
    .local v12, "a":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 863
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 865
    .end local v12    # "a":Ljava/lang/String;
    :cond_2
    const-string v2, "content://media/external/faces"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 866
    .local v3, "faces_uri":Landroid/net/Uri;
    const-string v2, "content://media/external/persons"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 867
    .local v14, "contact_uri":Landroid/net/Uri;
    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "name"

    aput-object v6, v8, v2

    .line 868
    .local v8, "proj":[Ljava/lang/String;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v2

    const/4 v2, 0x1

    const-string v6, "person_id"

    aput-object v6, v4, v2

    .line 869
    .local v4, "projection":[Ljava/lang/String;
    const-string v5, ""

    .line 870
    .local v5, "selection":Ljava/lang/String;
    const/16 v23, 0x0

    .line 871
    .local v23, "index":I
    const/16 v19, 0x0

    .line 872
    .local v19, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 873
    .local v17, "count":I
    const/16 v25, 0x0

    .line 874
    .local v25, "name":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 876
    .local v15, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_2
    move/from16 v0, v21

    move/from16 v1, v17

    if-ge v0, v1, :cond_4

    .line 877
    :try_start_0
    const-string v2, ""

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 878
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " OR "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 879
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " _id = \'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "\'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 876
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 881
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 882
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 884
    :cond_5
    const-string v2, "person_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 885
    .local v26, "s":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id = \'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "\'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    .line 886
    .local v9, "select":Ljava/lang/String;
    const/16 v18, 0x0

    .line 888
    .local v18, "curs":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v7, v14

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 889
    if-eqz v18, :cond_6

    .line 890
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    .line 891
    const-string v2, "name"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v25

    .line 894
    :cond_6
    :try_start_2
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 897
    if-eqz v26, :cond_9

    .line 898
    if-eqz v25, :cond_8

    .line 899
    const-string v2, "profile/Me"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 900
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "profile/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    const v7, 0x7f0e00c0

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 902
    :cond_7
    const-string v2, "/"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v23

    .line 903
    add-int/lit8 v2, v23, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 905
    :cond_8
    new-instance v2, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v2, v0, v6, v1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;-><init>(Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;ILjava/lang/String;)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 907
    :cond_9
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_5

    .line 913
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 894
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 908
    .end local v9    # "select":Ljava/lang/String;
    .end local v18    # "curs":Landroid/database/Cursor;
    .end local v26    # "s":Ljava/lang/String;
    :catch_0
    move-exception v20

    .line 909
    .local v20, "e":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 913
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 910
    .end local v20    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v2

    .line 913
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method private query(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 722
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 724
    .local v8, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725
    const-string p1, "media_type = \'1\'"

    .line 730
    :goto_0
    const-string v0, "content://media/external/contextural_tags/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 731
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    .line 732
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 735
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 736
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 738
    :cond_0
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 740
    .local v6, "_id":Ljava/lang/String;
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 741
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 742
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 749
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v4, v8

    .line 751
    .end local v6    # "_id":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 727
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ) AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "media_type = \'1\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 749
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_3
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 746
    :catch_0
    move-exception v0

    .line 749
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private queryToCategory(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 964
    iget-object v1, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 965
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 966
    .local v8, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 968
    .local v6, "cursorImage":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 970
    .local v3, "sqlStr":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getQuerySelection(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    .line 972
    if-eqz v3, :cond_0

    const-string v1, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 994
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v8, v11

    .end local v8    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v8

    .line 973
    .restart local v8    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 975
    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_IMAGE_TABLE_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "uri"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 977
    if-eqz v6, :cond_3

    .line 978
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 981
    :cond_2
    const-string/jumbo v1, "uri"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 982
    .local v10, "uri":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v10, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v10, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 983
    .local v9, "imageId":Ljava/lang/String;
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 984
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 994
    .end local v9    # "imageId":Ljava/lang/String;
    .end local v10    # "uri":Ljava/lang/String;
    :cond_3
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 990
    :catch_0
    move-exception v7

    .line 991
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 994
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v8, v11

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private queryToLocation(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 755
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 757
    .local v8, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 758
    const-string p1, "media_type = \'1\'"

    .line 762
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 763
    .local v9, "uri":Ljava/lang/String;
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 764
    .local v1, "uri2":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    .line 765
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 768
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 769
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 771
    :cond_0
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 773
    .local v6, "_id":Ljava/lang/String;
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 774
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 776
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 783
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v4, v8

    .line 785
    .end local v6    # "_id":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 760
    .end local v1    # "uri2":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v9    # "uri":Ljava/lang/String;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ) "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 783
    .restart local v1    # "uri2":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "uri":Ljava/lang/String;
    :cond_3
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 780
    :catch_0
    move-exception v0

    .line 783
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private setTableForRegex(Landroid/database/Cursor;I)Landroid/database/MatrixCursor;
    .locals 20
    .param p1, "c1"    # Landroid/database/Cursor;
    .param p2, "limit"    # I

    .prologue
    .line 193
    new-instance v13, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->regexColumns:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3e8

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v13, v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 194
    .local v13, "matrixCursor":Landroid/database/MatrixCursor;
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    .line 196
    .local v7, "df":Ljava/text/DateFormat;
    const/4 v6, 0x0

    .local v6, "dateString":Ljava/lang/String;
    const/4 v10, 0x0

    .line 199
    .local v10, "fileName":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 200
    .local v3, "count":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v3, :cond_1

    .line 201
    const-string v18, "datetaken"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 203
    .local v8, "dt":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 204
    :try_start_0
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 205
    .local v4, "date":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 210
    .end local v4    # "date":J
    :cond_0
    :goto_1
    const-string v18, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 211
    .local v2, "_id":Ljava/lang/String;
    const-string v18, "_display_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 212
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "file://"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 213
    .local v11, "fileUri":Landroid/net/Uri;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 214
    .local v17, "uri_pass":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 215
    .local v16, "uri":Landroid/net/Uri;
    const-string v14, "image/*"

    .line 216
    .local v14, "mimetype":Ljava/lang/String;
    const/16 v18, 0xd

    move/from16 v0, v18

    new-array v15, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    aput-object v16, v15, v18

    const/16 v18, 0x1

    aput-object v6, v15, v18

    const/16 v18, 0x2

    aput-object v16, v15, v18

    const/16 v18, 0x3

    aput-object v14, v15, v18

    const/16 v18, 0x4

    const/16 v19, 0x0

    aput-object v19, v15, v18

    const/16 v18, 0x5

    const/16 v19, 0x0

    aput-object v19, v15, v18

    const/16 v18, 0x6

    const/16 v19, 0x0

    aput-object v19, v15, v18

    const/16 v18, 0x7

    const/16 v19, 0x0

    aput-object v19, v15, v18

    const/16 v18, 0x8

    const/16 v19, 0x0

    aput-object v19, v15, v18

    const/16 v18, 0x9

    const/16 v19, 0x0

    aput-object v19, v15, v18

    const/16 v18, 0xa

    aput-object v10, v15, v18

    const/16 v18, 0xb

    aput-object v11, v15, v18

    const/16 v18, 0xc

    aput-object v2, v15, v18

    .line 217
    .local v15, "objects":[Ljava/lang/Object;
    invoke-virtual {v13, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 218
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 200
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 207
    .end local v2    # "_id":Ljava/lang/String;
    .end local v11    # "fileUri":Landroid/net/Uri;
    .end local v14    # "mimetype":Ljava/lang/String;
    .end local v15    # "objects":[Ljava/lang/Object;
    .end local v16    # "uri":Landroid/net/Uri;
    .end local v17    # "uri_pass":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 208
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_1

    .line 220
    .end local v8    # "dt":Ljava/lang/String;
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 221
    return-object v13
.end method

.method private setTableForRegexForTagInfo(Landroid/database/Cursor;ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/database/MatrixCursor;
    .locals 35
    .param p1, "c1"    # Landroid/database/Cursor;
    .param p2, "limit"    # I
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "people"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/MatrixCursor;"
        }
    .end annotation

    .prologue
    .line 225
    .local p5, "userDef":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p6, "selectedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v27, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->regexColumns:[Ljava/lang/String;

    const/16 v5, 0x3e8

    move-object/from16 v0, v27

    invoke-direct {v0, v2, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 226
    .local v27, "matrixCursor":Landroid/database/MatrixCursor;
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v17

    .line 229
    .local v17, "df":Ljava/text/DateFormat;
    const/4 v10, 0x0

    .line 230
    .local v10, "c":Landroid/database/Cursor;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "faces_IDs"

    aput-object v5, v4, v2

    .line 231
    .local v4, "projection":[Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "dateString":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "addr":Ljava/lang/String;
    const/16 v21, 0x0

    .line 232
    .local v21, "fileName":Ljava/lang/String;
    const/16 v29, 0x0

    .local v29, "nameInfo":Ljava/lang/String;
    const/16 v25, 0x0

    .local v25, "locationInfo":Ljava/lang/String;
    const/16 v33, 0x0

    .line 233
    .local v33, "userTagInfo":Ljava/lang/String;
    const/16 v24, 0x0

    .local v24, "latitude":Ljava/lang/String;
    const/16 v26, 0x0

    .line 237
    .local v26, "longitude":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 239
    .local v13, "count":I
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_0
    move/from16 v0, v23

    if-ge v0, v13, :cond_c

    .line 240
    :try_start_0
    const-string v2, "datetaken"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v18

    .line 242
    .local v18, "dt":Ljava/lang/String;
    if-eqz v18, :cond_0

    .line 243
    :try_start_1
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 244
    .local v14, "date":J
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v16

    .line 249
    .end local v14    # "date":J
    :cond_0
    :goto_1
    :try_start_2
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 250
    .local v8, "_id":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 251
    .local v32, "uri_pass":Ljava/lang/String;
    const-string v2, "latitude"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 252
    const-string v2, "longitude"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 253
    const-string v2, "_display_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file://"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "_data"

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 255
    .local v22, "fileUri":Landroid/net/Uri;
    invoke-static/range {v32 .. v32}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v31

    .line 256
    .local v31, "uri":Landroid/net/Uri;
    if-eqz v24, :cond_6

    if-eqz v26, :cond_6

    .line 257
    const-string v2, "addr"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 258
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v9, v1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->isLocationSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 259
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0e0221

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v9, v1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getLocationString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    aput-object v34, v6, v7

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    .line 266
    :goto_2
    const-string v2, "content://media/external/contextural_tags/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v2, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 267
    .local v3, "imageURI":Landroid/net/Uri;
    if-eqz v3, :cond_a

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 269
    if-eqz v10, :cond_9

    .line 270
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_8

    .line 271
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 272
    const-string v2, "faces_IDs"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 273
    .local v20, "face_tag":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->parseFacesToContact(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 274
    .local v12, "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    if-eqz v12, :cond_7

    if-eqz p4, :cond_7

    .line 275
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 276
    .local v11, "contactCount":I
    if-eqz v11, :cond_1

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0e0224

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getPeopleString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v34

    aput-object v34, v6, v7

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    .line 278
    const-string/jumbo v2, "|"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 279
    const/4 v2, 0x0

    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    .line 291
    .end local v11    # "contactCount":I
    .end local v20    # "face_tag":Ljava/lang/String;
    :cond_1
    :goto_3
    const-string v28, "image/*"

    .line 297
    .local v28, "mimetype":Ljava/lang/String;
    :goto_4
    if-eqz p6, :cond_b

    move-object/from16 v0, p6

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 298
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getUserTagString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v33

    .line 303
    :goto_5
    if-eqz v10, :cond_2

    .line 304
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_2
    const/16 v2, 0xd

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/4 v2, 0x0

    aput-object v31, v30, v2

    const/4 v2, 0x1

    aput-object v16, v30, v2

    const/4 v2, 0x2

    aput-object v31, v30, v2

    const/4 v2, 0x3

    aput-object v28, v30, v2

    const/4 v2, 0x4

    aput-object v33, v30, v2

    const/4 v2, 0x5

    aput-object v25, v30, v2

    const/4 v2, 0x6

    aput-object v29, v30, v2

    const/4 v2, 0x7

    const/4 v5, 0x0

    aput-object v5, v30, v2

    const/16 v2, 0x8

    const/4 v5, 0x0

    aput-object v5, v30, v2

    const/16 v2, 0x9

    const/4 v5, 0x0

    aput-object v5, v30, v2

    const/16 v2, 0xa

    aput-object v21, v30, v2

    const/16 v2, 0xb

    aput-object v22, v30, v2

    const/16 v2, 0xc

    aput-object v8, v30, v2

    .line 306
    .local v30, "objects":[Ljava/lang/Object;
    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 307
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 239
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_0

    .line 246
    .end local v3    # "imageURI":Landroid/net/Uri;
    .end local v8    # "_id":Ljava/lang/String;
    .end local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    .end local v22    # "fileUri":Landroid/net/Uri;
    .end local v28    # "mimetype":Ljava/lang/String;
    .end local v30    # "objects":[Ljava/lang/Object;
    .end local v31    # "uri":Landroid/net/Uri;
    .end local v32    # "uri_pass":Ljava/lang/String;
    :catch_0
    move-exception v19

    .line 247
    .local v19, "e":Ljava/lang/Exception;
    invoke-virtual/range {v27 .. v27}, Landroid/database/MatrixCursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 309
    .end local v18    # "dt":Ljava/lang/String;
    .end local v19    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 312
    if-eqz v10, :cond_3

    .line 313
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 314
    :cond_3
    if-eqz v27, :cond_4

    .line 315
    invoke-virtual/range {v27 .. v27}, Landroid/database/MatrixCursor;->close()V

    .line 317
    :cond_4
    :goto_6
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 318
    return-object v27

    .line 261
    .restart local v8    # "_id":Ljava/lang/String;
    .restart local v18    # "dt":Ljava/lang/String;
    .restart local v22    # "fileUri":Landroid/net/Uri;
    .restart local v31    # "uri":Landroid/net/Uri;
    .restart local v32    # "uri_pass":Ljava/lang/String;
    :cond_5
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 263
    :cond_6
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 283
    .restart local v3    # "imageURI":Landroid/net/Uri;
    .restart local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    .restart local v20    # "face_tag":Ljava/lang/String;
    :cond_7
    const/16 v29, 0x0

    goto :goto_3

    .line 286
    .end local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    .end local v20    # "face_tag":Ljava/lang/String;
    :cond_8
    const/4 v12, 0x0

    .restart local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    goto :goto_3

    .line 289
    .end local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    :cond_9
    const/4 v12, 0x0

    .restart local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    goto :goto_3

    .line 293
    .end local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    :cond_a
    const/16 v28, 0x0

    .line 294
    .restart local v28    # "mimetype":Ljava/lang/String;
    const/4 v12, 0x0

    .restart local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    goto/16 :goto_4

    .line 300
    :cond_b
    const/16 v33, 0x0

    goto :goto_5

    .line 312
    .end local v3    # "imageURI":Landroid/net/Uri;
    .end local v8    # "_id":Ljava/lang/String;
    .end local v12    # "contactID_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/provider/GallerySearchDatabase$Contact;>;"
    .end local v18    # "dt":Ljava/lang/String;
    .end local v22    # "fileUri":Landroid/net/Uri;
    .end local v28    # "mimetype":Ljava/lang/String;
    .end local v31    # "uri":Landroid/net/Uri;
    .end local v32    # "uri_pass":Ljava/lang/String;
    :cond_c
    if-eqz v10, :cond_d

    .line 313
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 314
    :cond_d
    if-eqz v27, :cond_4

    .line 315
    invoke-virtual/range {v27 .. v27}, Landroid/database/MatrixCursor;->close()V

    goto :goto_6

    .line 312
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_e

    .line 313
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 314
    :cond_e
    if-eqz v27, :cond_f

    .line 315
    invoke-virtual/range {v27 .. v27}, Landroid/database/MatrixCursor;->close()V

    :cond_f
    throw v2
.end method

.method private setTableForTag(Landroid/database/Cursor;ILjava/lang/String;Ljava/lang/String;)Landroid/database/MatrixCursor;
    .locals 24
    .param p1, "c1"    # Landroid/database/Cursor;
    .param p2, "limit"    # I
    .param p3, "startId"    # Ljava/lang/String;
    .param p4, "endId"    # Ljava/lang/String;

    .prologue
    .line 84
    new-instance v17, Landroid/database/MatrixCursor;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->tagColumns:[Ljava/lang/String;

    const/16 v2, 0x3e8

    move-object/from16 v0, v17

    invoke-direct {v0, v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 87
    .local v17, "matrixCursor":Landroid/database/MatrixCursor;
    const/4 v15, 0x0

    .local v15, "latitude":Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "longitude":Ljava/lang/String;
    const/4 v7, 0x0

    .line 88
    .local v7, "_id":Ljava/lang/String;
    const/16 v20, 0x0

    .local v20, "time":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "addr":Ljava/lang/String;
    const/4 v14, 0x0

    .line 89
    .local v14, "fileName":Ljava/lang/String;
    const/16 v22, 0x0

    .line 90
    .local v22, "uri":Landroid/net/Uri;
    const/4 v10, 0x0

    .line 91
    .local v10, "cursorLocation":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 94
    .local v9, "cursorFace":Landroid/database/Cursor;
    const/4 v1, 0x7

    :try_start_0
    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "latitude"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "longitude"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "datetaken"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "addr"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "_display_name"

    aput-object v2, v3, v1

    .line 95
    .local v3, "projectionLocation":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 96
    .local v4, "selectionLocation":Ljava/lang/String;
    const/4 v5, 0x0

    .line 97
    .local v5, "selectionLocationArg":[Ljava/lang/String;
    const-string v6, " _id ASC"

    .line 99
    .local v6, "orderBy":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 100
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    .line 101
    const-string v4, "_id >= ? AND _id < ?"

    .line 103
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 104
    .local v19, "startLoop":I
    if-eqz p4, :cond_4

    .line 105
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v13, v1, 0x1

    .line 110
    .local v13, "endLoop":I
    :goto_0
    const/4 v1, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 111
    const/4 v1, 0x1

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 114
    .end local v13    # "endLoop":I
    .end local v19    # "startLoop":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 116
    if-eqz v10, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 120
    :cond_1
    const-string v1, "_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 122
    const-string v1, "datetaken"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 123
    const-string v1, "latitude"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 124
    const-string v1, "longitude"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 125
    const-string v1, "addr"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 126
    const-string v1, "_display_name"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 129
    if-eqz v15, :cond_2

    if-eqz v16, :cond_2

    .line 130
    const-string v21, "0"

    .line 131
    .local v21, "type":Ljava/lang/String;
    const-string v12, "1"

    .line 132
    .local v12, "encode":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 133
    move-object/from16 v23, v8

    .line 134
    .local v23, "value":Ljava/lang/String;
    const/4 v1, 0x7

    new-array v0, v1, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/4 v1, 0x0

    aput-object v21, v18, v1

    const/4 v1, 0x1

    aput-object v23, v18, v1

    const/4 v1, 0x2

    aput-object v22, v18, v1

    const/4 v1, 0x3

    aput-object v20, v18, v1

    const/4 v1, 0x4

    aput-object v12, v18, v1

    const/4 v1, 0x5

    aput-object v14, v18, v1

    const/4 v1, 0x6

    aput-object v7, v18, v1

    .line 135
    .local v18, "objects":[Ljava/lang/Object;
    invoke-virtual/range {v17 .. v18}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 138
    .end local v12    # "encode":Ljava/lang/String;
    .end local v18    # "objects":[Ljava/lang/Object;
    .end local v21    # "type":Ljava/lang/String;
    .end local v23    # "value":Ljava/lang/String;
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 145
    :cond_3
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 189
    .end local v3    # "projectionLocation":[Ljava/lang/String;
    .end local v4    # "selectionLocation":Ljava/lang/String;
    .end local v5    # "selectionLocationArg":[Ljava/lang/String;
    .end local v6    # "orderBy":Ljava/lang/String;
    :goto_1
    return-object v17

    .line 107
    .restart local v3    # "projectionLocation":[Ljava/lang/String;
    .restart local v4    # "selectionLocation":Ljava/lang/String;
    .restart local v5    # "selectionLocationArg":[Ljava/lang/String;
    .restart local v6    # "orderBy":Ljava/lang/String;
    .restart local v19    # "startLoop":I
    :cond_4
    add-int/lit8 v13, v19, 0x1

    .restart local v13    # "endLoop":I
    goto/16 :goto_0

    .line 142
    .end local v3    # "projectionLocation":[Ljava/lang/String;
    .end local v4    # "selectionLocation":Ljava/lang/String;
    .end local v5    # "selectionLocationArg":[Ljava/lang/String;
    .end local v6    # "orderBy":Ljava/lang/String;
    .end local v13    # "endLoop":I
    .end local v19    # "startLoop":I
    :catch_0
    move-exception v11

    .line 143
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "GallerySearchDatabase"

    const-string v2, "Location tag exception!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method


# virtual methods
.method public declared-synchronized getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;
    .locals 1

    .prologue
    .line 322
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getImages(Lcom/sec/android/gallery3d/provider/SearchParser;Z)Landroid/database/Cursor;
    .locals 9
    .param p1, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;
    .param p2, "isTag"    # Z

    .prologue
    const/4 v0, 0x0

    .line 573
    const/4 v1, 0x0

    .line 574
    .local v1, "curs":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 576
    .local v8, "matrixCursor":Landroid/database/MatrixCursor;
    if-eqz p2, :cond_1

    .line 577
    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getImageFilter(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 578
    if-nez v1, :cond_0

    .line 605
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 607
    :goto_0
    return-object v0

    .line 580
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLimit()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getStartId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getEndId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->setTableForTag(Landroid/database/Cursor;ILjava/lang/String;Ljava/lang/String;)Landroid/database/MatrixCursor;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 605
    :goto_1
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_2
    move-object v0, v8

    .line 607
    goto :goto_0

    .line 582
    :cond_1
    :try_start_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 583
    .local v6, "selectedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1, v6}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getSelectionFilter(Lcom/sec/android/gallery3d/provider/SearchParser;Ljava/util/ArrayList;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 584
    if-nez v1, :cond_2

    .line 605
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 587
    :cond_2
    :try_start_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getSearchFilter()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 588
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLocationValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getPeopleName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getUserDef()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 589
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLimit()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLocationValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getPeopleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getUserDef()Ljava/util/ArrayList;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->setTableForRegexForTagInfo(Landroid/database/Cursor;ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/database/MatrixCursor;

    move-result-object v8

    .line 593
    :goto_3
    invoke-direct {p0, v8, p1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->addStrokeRow(Landroid/database/MatrixCursor;Lcom/sec/android/gallery3d/provider/SearchParser;)Landroid/database/MatrixCursor;

    move-result-object v8

    goto :goto_1

    .line 591
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getLimit()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->setTableForRegex(Landroid/database/Cursor;I)Landroid/database/MatrixCursor;

    move-result-object v8

    goto :goto_3

    .line 597
    :cond_5
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->onlyStrokeSearch(Lcom/sec/android/gallery3d/provider/SearchParser;)Landroid/database/MatrixCursor;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v8

    goto :goto_1

    .line 602
    .end local v6    # "selectedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v7

    .line 603
    .local v7, "e":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 605
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v7    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method
