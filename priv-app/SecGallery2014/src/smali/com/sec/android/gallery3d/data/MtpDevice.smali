.class public Lcom/sec/android/gallery3d/data/MtpDevice;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "MtpDevice.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MtpDevice"


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mDeviceId:I

.field private final mDeviceName:Ljava/lang/String;

.field private final mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mJpegChildren:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/mtp/MtpObjectInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

.field private mMtpDeviceCoverItem:Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;

.field private final mName:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mStopRequested:Z

.field private mTempJpegChildren:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/mtp/MtpObjectInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;ILcom/sec/android/gallery3d/data/MtpContext;)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "deviceId"    # I
    .param p4, "mtpContext"    # Lcom/sec/android/gallery3d/data/MtpContext;

    .prologue
    .line 74
    invoke-static {p4, p3}, Lcom/sec/android/gallery3d/data/MtpDeviceSet;->getDeviceName(Lcom/sec/android/gallery3d/data/MtpContext;I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/MtpDevice;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MtpContext;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MtpContext;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "deviceId"    # I
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "mtpContext"    # Lcom/sec/android/gallery3d/data/MtpContext;

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/android/gallery3d/data/MtpDevice;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 59
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 60
    iput p3, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mDeviceId:I

    .line 61
    invoke-static {p3}, Landroid/hardware/usb/UsbDevice;->getDeviceName(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mDeviceName:Ljava/lang/String;

    .line 62
    iput-object p5, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    .line 63
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mName:Ljava/lang/String;

    .line 64
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    const-string v1, "mtp://mtp"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/mtp/item/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mJpegChildren:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mTempJpegChildren:Ljava/util/List;

    .line 70
    return-void
.end method

.method private collectJpegChildren(IILjava/util/ArrayList;)V
    .locals 5
    .param p1, "storageId"    # I
    .param p2, "objectId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/mtp/MtpObjectInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/mtp/MtpObjectInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v0, "dirChildren":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/mtp/MtpObjectInfo;>;"
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/gallery3d/data/MtpDevice;->queryChildren(IILjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 101
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 102
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mStopRequested:Z

    if-eqz v4, :cond_1

    .line 108
    :cond_0
    return-void

    .line 104
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/mtp/MtpObjectInfo;

    .line 105
    .local v2, "info":Landroid/mtp/MtpObjectInfo;
    invoke-virtual {v2}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v4

    invoke-direct {p0, p1, v4, p3}, Lcom/sec/android/gallery3d/data/MtpDevice;->collectJpegChildren(IILjava/util/ArrayList;)V

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getObjectInfo(Lcom/sec/android/gallery3d/data/MtpContext;II)Landroid/mtp/MtpObjectInfo;
    .locals 2
    .param p0, "mtpContext"    # Lcom/sec/android/gallery3d/data/MtpContext;
    .param p1, "deviceId"    # I
    .param p2, "objectId"    # I

    .prologue
    .line 141
    invoke-static {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "deviceName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MtpContext;->getMtpClient()Lcom/sec/android/gallery3d/data/MtpClient;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/gallery3d/data/MtpClient;->getObjectInfo(Ljava/lang/String;I)Landroid/mtp/MtpObjectInfo;

    move-result-object v1

    return-object v1
.end method

.method private loadItems()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/mtp/MtpObjectInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/mtp/MtpObjectInfo;>;"
    const-string v4, "MtpDevice"

    const-string v5, "loadItems start"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MtpContext;->getMtpClient()Lcom/sec/android/gallery3d/data/MtpClient;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/MtpClient;->getStorageList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 84
    .local v3, "storageList":Ljava/util/List;, "Ljava/util/List<Landroid/mtp/MtpStorageInfo;>;"
    if-nez v3, :cond_0

    .line 85
    const-string v4, "MtpDevice"

    const-string/jumbo v5, "storage list is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :goto_0
    return-object v2

    .line 89
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/mtp/MtpStorageInfo;

    .line 90
    .local v1, "info":Landroid/mtp/MtpStorageInfo;
    invoke-virtual {v1}, Landroid/mtp/MtpStorageInfo;->getStorageId()I

    move-result v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5, v2}, Lcom/sec/android/gallery3d/data/MtpDevice;->collectJpegChildren(IILjava/util/ArrayList;)V

    goto :goto_1

    .line 92
    .end local v1    # "info":Landroid/mtp/MtpStorageInfo;
    :cond_1
    const-string v4, "MtpDevice"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadItems end.  result size : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private queryChildren(IILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "storageId"    # I
    .param p2, "objectId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/mtp/MtpObjectInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/mtp/MtpObjectInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p3, "jpeg":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/mtp/MtpObjectInfo;>;"
    .local p4, "dir":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/mtp/MtpObjectInfo;>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MtpContext;->getMtpClient()Lcom/sec/android/gallery3d/data/MtpClient;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v4, v5, p1, p2}, Lcom/sec/android/gallery3d/data/MtpClient;->getObjectList(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v0

    .line 115
    .local v0, "children":Ljava/util/List;, "Ljava/util/List<Landroid/mtp/MtpObjectInfo;>;"
    const-string v4, "MtpDevice"

    const-string v5, "queryChildren start"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    if-nez v0, :cond_1

    .line 117
    const-string v4, "MtpDevice"

    const-string v5, "children is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_0
    return-void

    .line 121
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/mtp/MtpObjectInfo;

    .line 122
    .local v3, "obj":Landroid/mtp/MtpObjectInfo;
    invoke-virtual {v3}, Landroid/mtp/MtpObjectInfo;->getFormat()I

    move-result v1

    .line 123
    .local v1, "format":I
    sparse-switch v1, :sswitch_data_0

    .line 133
    const-string v4, "MtpDevice"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "other type: name = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/mtp/MtpObjectInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", format = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 126
    :sswitch_0
    const-string v4, "MtpDevice"

    const-string v5, "Add MTP JPEG"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :sswitch_1
    invoke-virtual {p4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 123
    :sswitch_data_0
    .sparse-switch
        0x3001 -> :sswitch_1
        0x3801 -> :sswitch_0
        0x3808 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public cancelReload()V
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mStopRequested:Z

    .line 317
    return-void
.end method

.method public getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    .line 299
    const-string v1, "MtpDevice"

    const-string v2, "getCoverMediaItem :: MTP device"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 301
    .local v0, "coverItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_1

    .line 302
    const-string v1, "MtpDevice"

    const-string v2, "getCoverMediaItem is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpDeviceCoverItem:Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;

    if-nez v1, :cond_0

    .line 305
    new-instance v1, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MtpDevice;->getDataVersion()J

    move-result-wide v4

    sub-long/2addr v4, v6

    invoke-direct {v1, p0, v2, v4, v5}, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;-><init>(Lcom/sec/android/gallery3d/data/MtpDevice;Lcom/sec/android/gallery3d/data/Path;J)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpDeviceCoverItem:Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpDeviceCoverItem:Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;

    .line 307
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MtpDevice;->getDataVersion()J

    move-result-wide v2

    sub-long/2addr v2, v6

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mCoverVersion:J

    .line 308
    const-string v1, "MtpDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCoverMediaItem :: MTP device getDataVersion"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MtpDevice;->getDataVersion()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const-string v1, "MtpDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCoverMediaItem :: MTP device mCoverVersion"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mCoverVersion:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :goto_0
    return-object v0

    .line 311
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpDeviceCoverItem:Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 13
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v10, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mJpegChildren:Ljava/util/List;

    .line 151
    .local v10, "jpegChildren":Ljava/util/List;, "Ljava/util/List<Landroid/mtp/MtpObjectInfo;>;"
    :goto_0
    const-string v2, "MtpDevice"

    const-string v3, "getMediaItem"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .local v11, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move v6, p1

    .line 155
    .local v6, "begin":I
    add-int v2, p1, p2

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 157
    .local v8, "end":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    .line 158
    .local v7, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    move v9, v6

    .local v9, "i":I
    :goto_1
    if-ge v9, v8, :cond_2

    .line 159
    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/mtp/MtpObjectInfo;

    .line 160
    .local v4, "child":Landroid/mtp/MtpObjectInfo;
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 161
    .local v1, "childPath":Lcom/sec/android/gallery3d/data/Path;
    const-string v2, "MtpDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "childPath : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    sget-object v12, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v12

    .line 163
    :try_start_0
    invoke-virtual {v7, v1}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MtpImage;

    .line 164
    .local v0, "image":Lcom/sec/android/gallery3d/data/MtpImage;
    if-nez v0, :cond_1

    .line 165
    const-string v2, "MtpDevice"

    const-string v3, "Create new MtpImage"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpImage;

    .end local v0    # "image":Lcom/sec/android/gallery3d/data/MtpImage;
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget v3, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mDeviceId:I

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/MtpImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;ILandroid/mtp/MtpObjectInfo;Lcom/sec/android/gallery3d/data/MtpContext;)V

    .line 172
    .restart local v0    # "image":Lcom/sec/android/gallery3d/data/MtpImage;
    :goto_2
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 148
    .end local v0    # "image":Lcom/sec/android/gallery3d/data/MtpImage;
    .end local v1    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "child":Landroid/mtp/MtpObjectInfo;
    .end local v6    # "begin":I
    .end local v7    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v8    # "end":I
    .end local v9    # "i":I
    .end local v10    # "jpegChildren":Ljava/util/List;, "Ljava/util/List<Landroid/mtp/MtpObjectInfo;>;"
    .end local v11    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mTempJpegChildren:Ljava/util/List;

    goto :goto_0

    .line 169
    .restart local v0    # "image":Lcom/sec/android/gallery3d/data/MtpImage;
    .restart local v1    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v4    # "child":Landroid/mtp/MtpObjectInfo;
    .restart local v6    # "begin":I
    .restart local v7    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .restart local v8    # "end":I
    .restart local v9    # "i":I
    .restart local v10    # "jpegChildren":Ljava/util/List;, "Ljava/util/List<Landroid/mtp/MtpObjectInfo;>;"
    .restart local v11    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_1
    :try_start_1
    const-string v2, "MtpDevice"

    const-string v3, "Reuse MtpImage"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/MtpImage;->updateContent(Landroid/mtp/MtpObjectInfo;)V

    goto :goto_2

    .line 173
    .end local v0    # "image":Lcom/sec/android/gallery3d/data/MtpImage;
    :catchall_0
    move-exception v2

    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 175
    .end local v1    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "child":Landroid/mtp/MtpObjectInfo;
    :cond_2
    return-object v11
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mJpegChildren:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 210
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDirty()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->notifyDirty()V

    .line 228
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 190
    const-string v2, "MtpDevice"

    const-string v3, "reload start"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    const-string v2, "MtpDevice"

    const-string v3, "dirty"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mStopRequested:Z

    .line 194
    invoke-static {}, Lcom/sec/android/gallery3d/data/MtpDevice;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mDataVersion:J

    .line 195
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/MtpDevice;->loadItems()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mJpegChildren:Ljava/util/List;

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MtpDevice;->notifyContentChanged()V

    .line 199
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mJpegChildren:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 200
    .local v1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/mtp/MtpObjectInfo;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mTempJpegChildren:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 201
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 202
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mTempJpegChildren:Ljava/util/List;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 205
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice;->mDataVersion:J

    return-wide v2
.end method
