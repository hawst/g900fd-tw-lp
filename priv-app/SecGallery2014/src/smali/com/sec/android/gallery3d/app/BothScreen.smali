.class public Lcom/sec/android/gallery3d/app/BothScreen;
.super Landroid/app/Activity;
.source "BothScreen.java"


# static fields
.field private static final IMAGE_TYPE:Ljava/lang/String; = "image/*"

.field private static final KEY_PICKED_ITEM:Ljava/lang/String; = "picked-item"

.field private static final KEY_STATE:Ljava/lang/String; = "activity-state"

.field private static final STATE_INIT:I = 0x0

.field private static final STATE_PHOTO_PICKED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BothScreen"


# instance fields
.field private mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

.field private mPickedItem:Landroid/net/Uri;

.field private mState:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mState:I

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 105
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-nez v0, :cond_1

    .line 106
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 107
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/app/BothScreen;->setResult(I)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BothScreen;->finish()V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mState:I

    .line 113
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-eqz p3, :cond_0

    .line 114
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    if-eqz p1, :cond_0

    .line 53
    const-string v0, "activity-state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mState:I

    .line 54
    const-string v0, "picked-item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    .line 56
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 124
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->dismissDialog()V

    .line 127
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v5, 0x402

    const/4 v4, 0x1

    .line 70
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BothScreen;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v2, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    new-instance v3, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BothScreen;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BothScreen;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v3, v2, p0, v4}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .line 73
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mCheckStorageDialog:Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->showDialog()V

    .line 101
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BothScreen;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 77
    .local v0, "intent":Landroid/content/Intent;
    iget v2, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mState:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 79
    :pswitch_0
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    .line 80
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    if-nez v2, :cond_1

    .line 81
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 83
    .local v1, "request":Landroid/content/Intent;
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BothScreen;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string v2, "mode-crop"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 87
    const-string v2, "set-as-wallpaper"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 88
    const-string/jumbo v2, "wallpaper_type"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 89
    invoke-virtual {p0, v1, v4}, Lcom/sec/android/gallery3d/app/BothScreen;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 92
    .end local v1    # "request":Landroid/content/Intent;
    :cond_1
    iput v4, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mState:I

    .line 96
    :pswitch_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    if-eqz v2, :cond_3

    .line 97
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    const-string v3, "image/*"

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v5, v4}, Lcom/sec/samsung/gallery/controller/CropImageCmd;->startCropImageActivity(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;IZ)V

    .line 98
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BothScreen;->finish()V

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "saveState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 61
    const-string v0, "activity-state"

    iget v1, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "picked-item"

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/BothScreen;->mPickedItem:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 65
    :cond_0
    return-void
.end method
