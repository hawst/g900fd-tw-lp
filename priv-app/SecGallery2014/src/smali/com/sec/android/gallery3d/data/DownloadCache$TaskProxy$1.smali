.class Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy$1;
.super Ljava/lang/Object;
.source "DownloadCache.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->get(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy$1;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 3

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy$1;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    # getter for: Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->mTask:Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->access$400(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy$1;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DownloadCache$DownloadTask;->removeProxy(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;)V

    .line 412
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy$1;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    monitor-enter v1

    .line 413
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy$1;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->mIsCancelled:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;->access$902(Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;Z)Z

    .line 414
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy$1;->this$0:Lcom/sec/android/gallery3d/data/DownloadCache$TaskProxy;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 415
    monitor-exit v1

    .line 416
    return-void

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
