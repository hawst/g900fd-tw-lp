.class abstract Lcom/sec/android/gallery3d/ui/PositionController$Animatable;
.super Ljava/lang/Object;
.source "PositionController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PositionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Animatable"
.end annotation


# instance fields
.field public mAnimationDuration:I

.field public mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field public mAnimationKind:I

.field public mAnimationStartTime:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/PositionController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController$1;

    .prologue
    .line 1544
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;-><init>()V

    return-void
.end method

.method private static applyInterpolationCurve(IF)F
    .locals 3
    .param p0, "kind"    # I
    .param p1, "progress"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1604
    sub-float v0, v2, p1

    .line 1605
    .local v0, "f":F
    packed-switch p0, :pswitch_data_0

    .line 1625
    :goto_0
    :pswitch_0
    return p1

    .line 1611
    :pswitch_1
    sub-float p1, v2, v0

    .line 1612
    goto :goto_0

    .line 1616
    :pswitch_2
    mul-float v1, v0, v0

    sub-float p1, v2, v1

    .line 1617
    goto :goto_0

    .line 1619
    :pswitch_3
    mul-float v1, v0, v0

    mul-float/2addr v1, v0

    sub-float p1, v2, v1

    .line 1620
    goto :goto_0

    .line 1622
    :pswitch_4
    mul-float v1, v0, v0

    mul-float/2addr v1, v0

    mul-float/2addr v1, v0

    mul-float/2addr v1, v0

    sub-float p1, v2, v1

    goto :goto_0

    .line 1605
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public advanceAnimation()Z
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const-wide/16 v8, -0x2

    const/4 v4, 0x0

    .line 1562
    iget-wide v6, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationStartTime:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_0

    .line 1600
    :goto_0
    return v4

    .line 1565
    :cond_0
    iget-wide v6, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationStartTime:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 1566
    iput-wide v10, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationStartTime:J

    .line 1567
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->startSnapback()Z

    move-result v4

    goto :goto_0

    .line 1571
    :cond_1
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationDuration:I

    if-nez v5, :cond_5

    .line 1572
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1579
    .local v1, "progress":F
    :goto_1
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v1, v5

    if-ltz v5, :cond_6

    .line 1580
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1585
    :goto_2
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->interpolate(F)Z

    move-result v0

    .line 1587
    .local v0, "done":Z
    if-eqz v0, :cond_4

    .line 1589
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationKind:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_2

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationKind:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 1590
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 1593
    :cond_3
    iput-wide v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationStartTime:J

    .line 1595
    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationKind:I

    const/16 v5, 0xc

    if-ne v4, v5, :cond_4

    .line 1596
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->stopSnapback()V

    .line 1600
    :cond_4
    const/4 v4, 0x1

    goto :goto_0

    .line 1574
    .end local v0    # "done":Z
    .end local v1    # "progress":F
    :cond_5
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v2

    .line 1575
    .local v2, "now":J
    iget-wide v6, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationStartTime:J

    sub-long v6, v2, v6

    long-to-float v5, v6

    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationDuration:I

    int-to-float v6, v6

    div-float v1, v5, v6

    .restart local v1    # "progress":F
    goto :goto_1

    .line 1582
    .end local v2    # "now":J
    :cond_6
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->mAnimationKind:I

    invoke-static {v5, v1}, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;->applyInterpolationCurve(IF)F

    move-result v1

    goto :goto_2
.end method

.method protected abstract interpolate(F)Z
.end method

.method public abstract startSnapback()Z
.end method

.method public abstract startSnapback(I)Z
.end method

.method public stopSnapback()V
    .locals 0

    .prologue
    .line 1554
    return-void
.end method
