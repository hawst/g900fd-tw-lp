.class public Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
.super Ljava/lang/Object;
.source "GalleryMultiWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GalleryMultiWindow"

.field private static final TYPE_MULTI_NONE:I = -0x1

.field private static final TYPE_MULTI_PINUP:I = 0x1

.field private static final TYPE_MULTI_SPLIT:I

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private static mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;


# instance fields
.field private final SPLIT_MENU_CLOSE_ID:I

.field private final SPLIT_MENU_SPLIT_ID:I

.field private final SPLIT_MENU_SWITCH_ID:I

.field private final TYPE_GROUP_NONE:I

.field private final TYPE_GROUP_SPLIT:I

.field private isSelectionMode:Z

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mDropDownReloadNeeded:Z

.field private mMenuItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMultiWindowMode:I

.field private mMultiWindowSize:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 319
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$2;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$2;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->SPLIT_MENU_SPLIT_ID:I

    .line 67
    const/16 v0, 0x7d1

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->SPLIT_MENU_CLOSE_ID:I

    .line 68
    const/16 v0, 0x3eb

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->SPLIT_MENU_SWITCH_ID:I

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->TYPE_GROUP_NONE:I

    .line 71
    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->TYPE_GROUP_SPLIT:I

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowMode:I

    .line 76
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowSize:Landroid/graphics/Point;

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mDropDownReloadNeeded:Z

    .line 82
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    .line 84
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 86
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    .line 87
    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowMode:I

    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->registerLayoutListener(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 91
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isSelectionMode:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isSelectionMode:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isPickMode(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mDropDownReloadNeeded:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mDropDownReloadNeeded:Z

    return p1
.end method

.method private finishGallery()V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->finishAllViewState()V

    .line 276
    return-void
.end method

.method private isPickMode(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 173
    .local v0, "mStatusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v1, v2, :cond_1

    .line 176
    :cond_0
    const/4 v1, 0x1

    .line 178
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeExtras(Z)Landroid/content/Intent;
    .locals 3
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 306
    const/4 v0, 0x3

    .line 307
    .local v0, "flag":I
    const/4 v1, 0x0

    .line 308
    .local v1, "intent":Landroid/content/Intent;
    if-nez p1, :cond_0

    .line 309
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v2

    or-int/2addr v0, v2

    .line 315
    :cond_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    .line 316
    return-object v1
.end method

.method private registerLayoutListener(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 140
    .local v0, "deco":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 141
    .local v1, "vto2":Landroid/view/ViewTreeObserver;
    new-instance v2, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;-><init>(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 168
    return-void
.end method

.method private startActivity(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 296
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->makeExtras(Z)Landroid/content/Intent;

    move-result-object v0

    .line 297
    .local v0, "extras":Landroid/content/Intent;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 298
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 299
    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 302
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 303
    return-void
.end method


# virtual methods
.method public getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 135
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method public getMultiWindowRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    .line 127
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    goto :goto_0
.end method

.method public getZone()I
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    .line 291
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v0

    .line 292
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFullScreenMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 104
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    .line 105
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 14
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 182
    sget-object v10, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v10, :cond_0

    iget v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowMode:I

    if-eqz v10, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v2, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 187
    .local v2, "launcherMultiWindowIntent":Landroid/content/Intent;
    const-string v10, "android.intent.category.MULTIWINDOW_LAUNCHER"

    invoke-virtual {v2, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 189
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v10, 0x80

    invoke-virtual {v4, v2, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 192
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 193
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_3

    .line 194
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 195
    .local v5, "r":Landroid/content/pm/ResolveInfo;
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 196
    new-instance v7, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;

    iget-object v10, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-direct {v7, p0, v10, v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;-><init>(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Landroid/content/pm/ComponentInfo;Landroid/content/pm/PackageManager;)V

    .line 197
    .local v7, "splitItem":Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    .end local v7    # "splitItem":Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 200
    .end local v5    # "r":Landroid/content/pm/ResolveInfo;
    :cond_3
    sget-object v10, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mComparator:Ljava/util/Comparator;

    invoke-static {v1, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 202
    const/16 v10, 0x3e9

    const v11, 0x7f0e025a

    invoke-interface {p1, v13, v10, v12, v11}, Landroid/view/Menu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v8

    .line 205
    .local v8, "splitMenu":Landroid/view/SubMenu;
    if-eqz v8, :cond_4

    .line 206
    const v10, 0x7f02032c

    invoke-interface {v8, v10}, Landroid/view/SubMenu;->setIcon(I)Landroid/view/SubMenu;

    .line 207
    invoke-interface {v8}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x2

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 210
    :cond_4
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->clear()V

    .line 211
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    new-instance v11, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;

    invoke-direct {v11, p0, v13}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;-><init>(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Z)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 214
    if-eqz v8, :cond_5

    .line 215
    const/4 v0, 0x0

    :goto_2
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_5

    .line 216
    add-int/lit16 v11, v0, 0x3eb

    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v13, v11, v12, v10}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 218
    add-int/lit16 v10, v0, 0x3eb

    invoke-interface {v8, v10}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 219
    .local v9, "subMenu":Landroid/view/MenuItem;
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-interface {v9, v10}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 223
    .end local v9    # "subMenu":Landroid/view/MenuItem;
    :cond_5
    const/16 v10, 0x7d1

    const v11, 0x7f0e0259

    invoke-interface {p1, v12, v10, v12, v11}, Landroid/view/Menu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v6

    .line 225
    .local v6, "splitCancel":Landroid/view/SubMenu;
    if-eqz v6, :cond_6

    .line 226
    const v10, 0x7f02032d

    invoke-interface {v6, v10}, Landroid/view/SubMenu;->setIcon(I)Landroid/view/SubMenu;

    .line 227
    invoke-interface {v6}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v10

    invoke-interface {v10, v12}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 231
    :cond_6
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMenuOperation(Landroid/view/Menu;)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 101
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/16 v7, 0x7d1

    const/4 v5, 0x0

    .line 235
    sget-object v6, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowMode:I

    if-eqz v6, :cond_1

    .line 268
    :cond_0
    :goto_0
    return v5

    .line 239
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 240
    .local v1, "itemId":I
    if-ne v1, v7, :cond_2

    .line 241
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->finishGallery()V

    .line 242
    const/4 v5, 0x1

    goto :goto_0

    .line 243
    :cond_2
    if-ge v1, v7, :cond_0

    const/16 v6, 0x3eb

    if-lt v1, v6, :cond_0

    .line 244
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    add-int/lit16 v2, v6, -0x3eb

    .line 245
    .local v2, "listIndex":I
    if-ltz v2, :cond_3

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v2, v6, :cond_4

    .line 246
    :cond_3
    const-string v6, "GalleryMultiWindow"

    const-string v7, "indexing item is not available"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 250
    :cond_4
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMenuItems:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;

    .line 255
    .local v4, "split":Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->getCompName()Landroid/content/ComponentName;

    move-result-object v3

    .line 256
    .local v3, "name":Landroid/content/ComponentName;
    if-eqz v3, :cond_0

    .line 257
    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->ci:Landroid/content/pm/ComponentInfo;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;->access$500(Lcom/sec/android/gallery3d/app/GalleryMultiWindow$SplitMenuItem;)Landroid/content/pm/ComponentInfo;

    move-result-object v6

    instance-of v6, v6, Landroid/content/pm/ActivityInfo;

    if-eqz v6, :cond_0

    .line 259
    :try_start_0
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->startActivity(Landroid/content/ComponentName;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "GalleryMultiWindow"

    const-string v7, "Activity Not Found"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public updateMenuOperation(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 272
    return-void
.end method

.method public updateMultiWindowSize(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 112
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowSize:Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->getMultiWindowRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 113
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowSize:Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->getMultiWindowRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 120
    :goto_0
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowSize:Landroid/graphics/Point;

    iget v2, v1, Landroid/graphics/Point;->y:I

    add-int/lit8 v2, v2, -0x30

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 123
    :cond_0
    return-void

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 117
    .local v0, "wm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindowSize:Landroid/graphics/Point;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    goto :goto_0
.end method
