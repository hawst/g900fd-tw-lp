.class Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "GalleryCoverMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/GalleryCoverMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 7
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v6, 0x0

    .line 263
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 264
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 265
    # getter for: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SCover : State Open"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    const/4 v3, 0x3

    # invokes: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->requestSystemKeyEvent(IZ)V
    invoke-static {v2, v3, v6}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$400(Lcom/sec/android/gallery3d/app/GalleryCoverMode;IZ)V

    .line 267
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    # invokes: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->getSettingTimeOut()J
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$500(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)J

    move-result-wide v4

    # invokes: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setScreenOffTime(JZ)V
    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$600(Lcom/sec/android/gallery3d/app/GalleryCoverMode;JZ)V

    .line 268
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$200(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->convertFromTranslucent()V

    .line 274
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    # invokes: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->setLayoutParams()V
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$700(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)V

    .line 277
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$800(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 278
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mCoverStateListenerList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$800(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 279
    .local v1, "listener":Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;->onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V

    goto :goto_1

    .line 281
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 271
    :cond_1
    # getter for: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SCover : State Close"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryCoverMode$2;->this$0:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryCoverMode;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->access$200(Lcom/sec/android/gallery3d/app/GalleryCoverMode;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 281
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282
    return-void
.end method
