.class public Lcom/sec/android/gallery3d/provider/GallerySearchProvider;
.super Landroid/content/ContentProvider;
.source "GallerySearchProvider.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.gallery3d.provider.GallerySearchProvider"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final FINDO_REGEX_SEARCH_SUGGEST:I = 0x4

.field private static final FINDO_TAG_SEARCH_SUGGEST:I = 0x6

.field private static final SUGGEST_URI_PATH_REGEX_QUERY:Ljava/lang/String; = "search_suggest_regex_query"

.field private static final SUGGEST_URI_PATH_TAG_QUERY:Ljava/lang/String; = "search_suggest_tag_query"

.field private static final TAG:Ljava/lang/String; = "GallerySearchProvider"

.field private static mGallerySearchDB:Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;

.field private static final mMather:Landroid/content/UriMatcher;


# instance fields
.field private mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "content://com.sec.android.gallery3d.provider.GallerySearchProvider/gallery"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 30
    invoke-static {}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->buildMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mMather:Landroid/content/UriMatcher;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    return-void
.end method

.method private static buildMatcher()Landroid/content/UriMatcher;
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x4

    .line 36
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 37
    .local v0, "matcher":Landroid/content/UriMatcher;
    const-string v1, "com.sec.android.gallery3d.provider.GallerySearchProvider"

    const-string v2, "search_suggest_regex_query"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 38
    const-string v1, "com.sec.android.gallery3d.provider.GallerySearchProvider"

    const-string v2, "search_suggest_regex_query/*"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 40
    const-string v1, "com.sec.android.gallery3d.provider.GallerySearchProvider"

    const-string v2, "search_suggest_tag_query"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 41
    const-string v1, "com.sec.android.gallery3d.provider.GallerySearchProvider"

    const-string v2, "search_suggest_tag_query/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 43
    return-object v0
.end method

.method private static checkFindoSearch(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 92
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_0

    .line 93
    const-string v1, "com.sec.feature.findo"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 95
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getFindoRegexSuggest(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getFindoSuggest(Landroid/net/Uri;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getFindoSuggest(Landroid/net/Uri;Z)Landroid/database/Cursor;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "isTag"    # Z

    .prologue
    .line 100
    new-instance v0, Lcom/sec/android/gallery3d/provider/SearchParser;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/provider/SearchParser;-><init>()V

    .line 101
    .local v0, "sp":Lcom/sec/android/gallery3d/provider/SearchParser;
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->parse(Landroid/net/Uri;)V

    .line 103
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getUserDef()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/provider/SearchParser;->allTagCheck()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 104
    const/4 v1, 0x0

    .line 106
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mGallerySearchDB:Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getImages(Lcom/sec/android/gallery3d/provider/SearchParser;Z)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method private getFindoTagSuggest(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 110
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getFindoSuggest(Landroid/net/Uri;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mMather:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :pswitch_1
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0

    .line 48
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v4, p0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    invoke-virtual {v4, p2}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 127
    .local v2, "row":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 128
    invoke-static {p1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 129
    .local v0, "notiUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 133
    .end local v0    # "notiUri":Landroid/net/Uri;
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mGallerySearchDB:Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;

    .line 61
    sget-object v0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mGallerySearchDB:Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/provider/GallerySearchDatabase;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 71
    const-string v1, "GallerySearchProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "query "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    sget-object v1, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->mMather:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 86
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->checkFindoSearch(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getFindoRegexSuggest(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    .line 83
    :cond_0
    :goto_0
    return-object v0

    .line 80
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->checkFindoSearch(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/provider/GallerySearchProvider;->getFindoTagSuggest(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;

    .prologue
    .line 120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
