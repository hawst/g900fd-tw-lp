.class Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;
.super Ljava/lang/Object;
.source "ContextualTagSetting.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/ContextualTagSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$900(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 282
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 4
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 286
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$900(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 287
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$900(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    # invokes: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->processItemClick(Landroid/view/View;)V
    invoke-static {v3, v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$1000(Lcom/sec/android/gallery3d/app/ContextualTagSetting;Landroid/view/View;)V

    .line 286
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 289
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$000(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v0

    .line 290
    .local v0, "checkedItem":I
    if-lez v0, :cond_1

    .line 291
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mActionBarSwitch:Landroid/widget/Switch;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$700(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Landroid/widget/Switch;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 293
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$900(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 294
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 1
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .param p6, "arg4"    # Z
    .param p7, "arg5"    # Z
    .param p8, "arg6"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagSetting$4;->this$0:Lcom/sec/android/gallery3d/app/ContextualTagSetting;

    # getter for: Lcom/sec/android/gallery3d/app/ContextualTagSetting;->mDragSelectedList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->access$900(Lcom/sec/android/gallery3d/app/ContextualTagSetting;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    return-void
.end method
