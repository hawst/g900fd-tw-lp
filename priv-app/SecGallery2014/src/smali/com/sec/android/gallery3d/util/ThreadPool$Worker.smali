.class Lcom/sec/android/gallery3d/util/ThreadPool$Worker;
.super Ljava/lang/Object;
.source "ThreadPool.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/Future;
.implements Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/ThreadPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Lcom/sec/android/gallery3d/util/Future",
        "<TT;>;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Worker"


# instance fields
.field private mCancelListener:Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;

.field private volatile mIsCancelled:Z

.field private mIsDone:Z

.field private mJob:Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mListener:Lcom/sec/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mMode:I

.field private mNetworkPoolThread:Z

.field private mResult:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mWaitOnResource:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

.field final synthetic this$0:Lcom/sec/android/gallery3d/util/ThreadPool;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/util/ThreadPool;Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<TT;>;",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    .local p2, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<TT;>;"
    .local p3, "listener":Lcom/sec/android/gallery3d/util/FutureListener;, "Lcom/sec/android/gallery3d/util/FutureListener<TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;-><init>(Lcom/sec/android/gallery3d/util/ThreadPool;Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;Z)V

    .line 177
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/util/ThreadPool;Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;Z)V
    .locals 0
    .param p4, "networkPool"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<TT;>;",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 179
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    .local p2, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<TT;>;"
    .local p3, "listener":Lcom/sec/android/gallery3d/util/FutureListener;, "Lcom/sec/android/gallery3d/util/FutureListener<TT;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->this$0:Lcom/sec/android/gallery3d/util/ThreadPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mJob:Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    .line 181
    iput-object p3, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mListener:Lcom/sec/android/gallery3d/util/FutureListener;

    .line 182
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mNetworkPoolThread:Z

    .line 183
    return-void
.end method

.method private acquireResource(Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;)Z
    .locals 1
    .param p1, "counter"    # Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .prologue
    .line 305
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    :goto_0
    monitor-enter p0

    .line 306
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsCancelled:Z

    if-eqz v0, :cond_0

    .line 307
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .line 308
    const/4 v0, 0x0

    monitor-exit p0

    .line 331
    :goto_1
    return v0

    .line 310
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .line 311
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    monitor-enter p1

    .line 314
    :try_start_1
    iget v0, p1, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;->value:I

    if-lez v0, :cond_1

    .line 315
    iget v0, p1, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;->value:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;->value:I

    .line 316
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 327
    monitor-enter p0

    .line 328
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .line 329
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 331
    const/4 v0, 0x1

    goto :goto_1

    .line 311
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 319
    :cond_1
    :try_start_4
    invoke-virtual {p1}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 324
    :goto_2
    :try_start_5
    monitor-exit p1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 329
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 320
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method private modeToCounter(I)Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 294
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->this$0:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v0, v0, Lcom/sec/android/gallery3d/util/ThreadPool;->mCpuCounter:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .line 299
    :goto_0
    return-object v0

    .line 296
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->this$0:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v0, v0, Lcom/sec/android/gallery3d/util/ThreadPool;->mNetworkCounter:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    goto :goto_0

    .line 299
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseResource(Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;)V
    .locals 1
    .param p1, "counter"    # Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    .prologue
    .line 335
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    monitor-enter p1

    .line 336
    :try_start_0
    iget v0, p1, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;->value:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;->value:I

    .line 337
    invoke-virtual {p1}, Ljava/lang/Object;->notifyAll()V

    .line 338
    monitor-exit p1

    .line 339
    return-void

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 2

    .prologue
    .line 212
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsCancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 222
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 213
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsCancelled:Z

    .line 214
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    if-eqz v0, :cond_2

    .line 215
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mWaitOnResource:Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 217
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 219
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;->onCancel()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 217
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 238
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "Worker"

    const-string v2, "ingore exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 236
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 244
    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mResult:Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 290
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    iget v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mMode:I

    return v0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 226
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsCancelled:Z

    return v0
.end method

.method public declared-synchronized isDone()Z
    .locals 1

    .prologue
    .line 231
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    const/4 v2, 0x1

    .line 188
    const/4 v1, 0x0

    .line 192
    .local v1, "result":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->setMode(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mJob:Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    invoke-interface {v2, p0}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 200
    .end local v1    # "result":Ljava/lang/Object;, "TT;"
    :cond_0
    :goto_0
    monitor-enter p0

    .line 201
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->setMode(I)Z

    .line 202
    iput-object v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mResult:Ljava/lang/Object;

    .line 203
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsDone:Z

    .line 204
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 205
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mListener:Lcom/sec/android/gallery3d/util/FutureListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mListener:Lcom/sec/android/gallery3d/util/FutureListener;

    invoke-interface {v2, p0}, Lcom/sec/android/gallery3d/util/FutureListener;->onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V

    .line 207
    :cond_1
    return-void

    .line 195
    .restart local v1    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v0

    .line 196
    .local v0, "ex":Ljava/lang/Throwable;
    const-string v2, "Worker"

    const-string v3, "Exception in running a job"

    invoke-static {v2, v3, v0}, Lcom/sec/android/gallery3d/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 205
    .end local v0    # "ex":Ljava/lang/Throwable;
    .end local v1    # "result":Ljava/lang/Object;, "TT;"
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public declared-synchronized setCancelListener(Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;

    .prologue
    .line 256
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;

    .line 257
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mIsCancelled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mCancelListener:Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/ThreadPool$CancelListener;->onCancel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    :cond_0
    monitor-exit p0

    return-void

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMode(I)Z
    .locals 5
    .param p1, "mode"    # I

    .prologue
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    const/4 v1, 0x0

    .line 265
    iget v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mMode:I

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->modeToCounter(I)Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    move-result-object v0

    .line 266
    .local v0, "rc":Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->releaseResource(Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;)V

    .line 267
    :cond_0
    iput v1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mMode:I

    .line 270
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mNetworkPoolThread:Z

    if-nez v2, :cond_2

    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 271
    iget-object v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->this$0:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v3, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mJob:Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    iget-object v4, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mListener:Lcom/sec/android/gallery3d/util/FutureListener;

    # invokes: Lcom/sec/android/gallery3d/util/ThreadPool;->submitToNetworkPool(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;
    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ThreadPool;->access$100(Lcom/sec/android/gallery3d/util/ThreadPool;Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    .line 272
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mListener:Lcom/sec/android/gallery3d/util/FutureListener;

    .line 285
    :cond_1
    :goto_0
    return v1

    .line 277
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->modeToCounter(I)Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;

    move-result-object v0

    .line 278
    if-eqz v0, :cond_3

    .line 279
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->acquireResource(Lcom/sec/android/gallery3d/util/ThreadPool$ResourceCounter;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 282
    iput p1, p0, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->mMode:I

    .line 285
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public waitDone()V
    .locals 0

    .prologue
    .line 249
    .local p0, "this":Lcom/sec/android/gallery3d/util/ThreadPool$Worker;, "Lcom/sec/android/gallery3d/util/ThreadPool$Worker<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/util/ThreadPool$Worker;->get()Ljava/lang/Object;

    .line 250
    return-void
.end method
