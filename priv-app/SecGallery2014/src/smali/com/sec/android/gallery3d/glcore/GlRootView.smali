.class public Lcom/sec/android/gallery3d/glcore/GlRootView;
.super Landroid/opengl/GLSurfaceView;
.source "GlRootView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Lcom/sec/android/gallery3d/ui/GLRoot;
.implements Lcom/sec/android/gallery3d/util/FutureListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "WrongCall"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;,
        Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;,
        Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;,
        Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;,
        Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;,
        Lcom/sec/android/gallery3d/glcore/GlRootView$RotationListener;,
        Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;,
        Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/opengl/GLSurfaceView;",
        "Landroid/opengl/GLSurfaceView$Renderer;",
        "Lcom/sec/android/gallery3d/ui/GLRoot;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Lcom/sec/android/gallery3d/glcore/GlObject;",
        ">;"
    }
.end annotation


# static fields
.field public static final ACTION_ACCESSIBILITY_FLING:I = 0x3eb

.field public static final ACTION_GENERIC_ENTER:I = 0x3e8

.field public static final ACTION_GENERIC_EXIT:I = 0x3e9

.field public static final ACTION_GENERIC_MOVE:I = 0x3ea

.field private static final CMD_SYS_RESET:I = 0x2

.field private static final CMD_SYS_SURFACECHG:I = 0x1

.field private static final CMD_TARGET_LAYER:I = 0x1

.field private static final CMD_TARGET_OBJ:I = 0x3

.field private static final CMD_TARGET_VIEW:I = 0x2

.field private static final DEBUG_DRAWING_STAT:Z = false

.field private static final DEBUG_FPS:Z = false

.field private static final DEBUG_INVALIDATE:Z = false

.field private static final DEBUG_PROFILE:Z = false

.field private static final DEBUG_PROFILE_SLOW_ONLY:Z = false

.field private static final FLAG_INITIALIZED:I = 0x1

.field private static final FLAG_NEED_LAYOUT:I = 0x2

.field private static final MAX_CMD_QUEUE_SIZE:I = 0x400

.field private static final RENDER_LOGIC_GED:I = 0x1

.field private static final RENDER_LOGIC_NONE:I = 0x0

.field private static final RENDER_LOGIC_SEC:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field public static WHITE_THEME_CLEARCOLOR_BLUE:F

.field public static WHITE_THEME_CLEARCOLOR_GREEN:F

.field public static WHITE_THEME_CLEARCOLOR_RED:F


# instance fields
.field private mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

.field private mAirButton:Lcom/samsung/android/airbutton/AirButtonImpl;

.field private mAirButtonViewer:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

.field private mAllTextures:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/sec/android/gallery3d/glrenderer/BasicTexture;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

.field private final mAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/anim/CanvasAnimation;",
            ">;"
        }
    .end annotation
.end field

.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

.field public mBgColorAlpha:F

.field public mBgColorBlue:F

.field public mBgColorGreen:F

.field public mBgColorRed:F

.field private mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

.field private mClipRect:Landroid/graphics/Rect;

.field private mClipRetryCount:I

.field private mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

.field private mCmdQueueRD:I

.field private mCmdQueueWR:I

.field private mCompensation:I

.field private mCompensationMatrix:Landroid/graphics/Matrix;

.field private mContentView:Lcom/sec/android/gallery3d/ui/GLView;

.field private mContext:Landroid/content/Context;

.field private mCurTime:J

.field private mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDisplayRotation:I

.field public mDragDownFromSelectAll:Z

.field private mFgndDraw:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;",
            ">;"
        }
    .end annotation
.end field

.field private mFlags:I

.field private mFrameCount:I

.field private mFrameCountingStart:J

.field private mFreeze:Z

.field private final mFreezeCondition:Ljava/util/concurrent/locks/Condition;

.field private mGL:Ljavax/microedition/khronos/opengles/GL11;

.field private mGLObjectNoDetach:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation
.end field

.field private mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

.field private mGlActive:Z

.field private mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

.field private mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

.field public mHeight:I

.field private final mIdleListeners:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mIdleRunner:Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;

.field private mInDownState:Z

.field private mInHoverState:Z

.field private mInvalidateColor:I

.field private mLastDrawFinishTime:J

.field private mMotionEventType:I

.field public mMotionTarget:Ljava/lang/Object;

.field private mMotionTargetType:I

.field public mObjectHoveringAppeared:Z

.field public mObjectPosChanged:Z

.field public mObjectZPosChanged:Z

.field private mOrientationSource:Lcom/sec/android/gallery3d/ui/OrientationSource;

.field public mPadBottom:I

.field public mPadLeft:I

.field public mPadRight:I

.field public mPadTop:I

.field private mRefreshEnabled:Z

.field private final mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private mRenderLogic:I

.field public volatile mRenderRequested:Z

.field private mRequestRenderOnAnimationFrame:Ljava/lang/Runnable;

.field public mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field public mRootLayerNew:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field public mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field public mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mRotationDetector:Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;

.field private mRotationListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation
.end field

.field private final mRunningAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/anim/Animation;",
            ">;"
        }
    .end annotation
.end field

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mShowAirButton:Z

.field private mStatusQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

.field private mThread:Ljava/lang/Thread;

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mToolType:I

.field public mUploadBmCache:Ljava/lang/Object;

.field private mUsePostOnAnimation:Z

.field public mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const v1, 0x3f7ae148    # 0.98f

    .line 107
    const-class v0, Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    .line 183
    sput v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_RED:F

    .line 184
    sput v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_GREEN:F

    .line 185
    sput v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_BLUE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 246
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 247
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/16 v1, 0x8

    const/4 v12, 0x0

    const/4 v4, 0x0

    .line 250
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 110
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCount:I

    .line 111
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCountingStart:J

    .line 114
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInvalidateColor:I

    .line 140
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensationMatrix:Landroid/graphics/Matrix;

    .line 143
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    .line 145
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    .line 151
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    .line 154
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;

    invoke-direct {v0, p0, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlRootView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleRunner:Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;

    .line 156
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 157
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFreezeCondition:Ljava/util/concurrent/locks/Condition;

    .line 162
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInDownState:Z

    .line 170
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    .line 172
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRect:Landroid/graphics/Rect;

    .line 173
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRunningAnimations:Ljava/util/ArrayList;

    .line 175
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInHoverState:Z

    .line 176
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    .line 177
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUsePostOnAnimation:Z

    .line 189
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 190
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayerNew:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 191
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 192
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    .line 193
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionEventType:I

    .line 194
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    .line 196
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    .line 197
    const/16 v0, 0x400

    new-array v0, v0, [Lcom/sec/android/gallery3d/glcore/GlCmd;

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    .line 198
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    .line 199
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 200
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLObjectNoDetach:Ljava/util/ArrayList;

    .line 202
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFgndDraw:Ljava/util/LinkedList;

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationListener:Ljava/util/ArrayList;

    .line 204
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    .line 212
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 213
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 214
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectHoveringAppeared:Z

    .line 215
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRefreshEnabled:Z

    .line 219
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAllTextures:Ljava/util/WeakHashMap;

    .line 220
    new-instance v0, Lcom/sec/android/gallery3d/glcore/TUtils;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    .line 222
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUploadBmCache:Ljava/lang/Object;

    .line 224
    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorRed:F

    .line 225
    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorGreen:F

    .line 226
    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorBlue:F

    .line 227
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorAlpha:F

    .line 231
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mShowAirButton:Z

    .line 232
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mToolType:I

    .line 235
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDragDownFromSelectAll:Z

    .line 376
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlRootView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView$1;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRequestRenderOnAnimationFrame:Ljava/lang/Runnable;

    .line 2413
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    .line 251
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    .line 252
    invoke-virtual {p0, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 253
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setEGLContextClientVersion(I)V

    .line 254
    sget-boolean v0, Lcom/sec/android/gallery3d/common/ApiHelper;->USE_888_PIXEL_FORMAT:Z

    if-eqz v0, :cond_0

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v5, v1

    move v6, v4

    .line 255
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setEGLConfigChooser(IIIIII)V

    .line 260
    :goto_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    .line 261
    iput-object p0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 262
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlConfig;

    invoke-direct {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlConfig;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    .line 263
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->createRootObject()V

    .line 266
    invoke-virtual {p0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 267
    sget-boolean v0, Lcom/sec/android/gallery3d/common/ApiHelper;->USE_888_PIXEL_FORMAT:Z

    if-eqz v0, :cond_1

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 277
    :goto_1
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;

    invoke-direct {v1, p0, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlRootView$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 278
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlRootView$RotationListener;

    invoke-direct {v1, p0, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView$RotationListener;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlRootView$1;)V

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector$OnRotationGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationDetector:Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;

    .line 279
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 280
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 282
    return-void

    .line 257
    :cond_0
    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x5

    move-object v5, p0

    move v9, v4

    move v10, v1

    move v11, v4

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setEGLConfigChooser(IIIIII)V

    goto :goto_0

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    goto :goto_1
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationListener:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/ui/GLView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/TUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->superRequestRender()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayDeque;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/glcore/GlRootView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private createRootObject()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1421
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {v0, v2, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1422
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 1423
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v2, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1424
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 1425
    return-void
.end method

.method private dispatchHoverEventInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "parentObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    .line 2587
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2589
    .local v0, "childList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 2592
    .local v4, "tgtObject":Ljava/lang/Object;
    iget v6, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v2, v6, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_5

    .line 2593
    aget-object v1, v0, v2

    .line 2594
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v1, :cond_0

    iget-boolean v6, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v6, :cond_0

    iget v6, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v7, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-eq v6, v7, :cond_1

    .line 2592
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 2596
    :cond_1
    iget v6, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v6, :cond_3

    invoke-direct {p0, v1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->dispatchHoverEventInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/MotionEvent;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2613
    .end local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v4    # "tgtObject":Ljava/lang/Object;
    :cond_2
    :goto_1
    return v5

    .line 2598
    .restart local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local v4    # "tgtObject":Ljava/lang/Object;
    :cond_3
    invoke-virtual {v1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2599
    if-eq v1, v4, :cond_2

    .line 2600
    if-eqz v4, :cond_4

    instance-of v6, v4, Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v6, :cond_4

    .line 2601
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 2602
    .local v3, "newEvent":Landroid/view/MotionEvent;
    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 2603
    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlObject;

    .end local v4    # "tgtObject":Ljava/lang/Object;
    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    .line 2604
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 2606
    .end local v3    # "newEvent":Landroid/view/MotionEvent;
    :cond_4
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 2607
    const/4 v6, 0x3

    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    .line 2608
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInHoverState:Z

    goto :goto_1

    .line 2613
    .end local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local v4    # "tgtObject":Ljava/lang/Object;
    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private dispatchTouchEventInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "parentObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    .line 2521
    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2523
    .local v1, "childList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 2524
    .local v4, "tempObject":Ljava/lang/Object;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2525
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v5, v7

    .line 2526
    .local v5, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v6, v7

    .line 2529
    .local v6, "y":I
    iget v7, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v3, v7, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_7

    .line 2530
    aget-object v2, v1, v3

    .line 2531
    .local v2, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v2, :cond_0

    iget-boolean v7, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v7, :cond_0

    iget v7, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v9, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-eq v7, v9, :cond_1

    .line 2529
    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 2533
    :cond_1
    const/16 v7, 0x3ea

    if-ne v0, v7, :cond_3

    if-ne v4, v2, :cond_3

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2550
    .end local v2    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    :goto_1
    return v8

    .line 2536
    .restart local v2    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_3
    iget v7, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v7, :cond_4

    invoke-direct {p0, v2, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->dispatchTouchEventInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/MotionEvent;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2538
    :cond_4
    invoke-virtual {v2, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2539
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v7, :cond_5

    const/16 v7, 0x3e8

    if-ne v0, v7, :cond_5

    .line 2540
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    if-eqz v7, :cond_5

    .line 2541
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    const/16 v9, 0x80

    invoke-virtual {v7, v2, v9}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(Lcom/sec/android/gallery3d/glcore/GlObject;I)Z

    .line 2543
    :cond_5
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 2544
    const/4 v7, 0x3

    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    .line 2545
    if-nez v0, :cond_6

    move v7, v8

    :goto_2
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionEventType:I

    goto :goto_1

    :cond_6
    const/4 v7, 0x2

    goto :goto_2

    .line 2550
    .end local v2    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_7
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private finalizeGL()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2314
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finalizeView layer = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", content = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", GlAvala"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2316
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-eqz v0, :cond_0

    .line 2317
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlBackground;->destroy()V

    .line 2318
    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    .line 2320
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 2321
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObject(Z)V

    .line 2322
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v0, :cond_1

    .line 2323
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->destroy()V

    .line 2324
    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2326
    :cond_1
    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayerNew:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2327
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->clearAllAnimation()V

    .line 2328
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/TUtils;->finalizeTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2329
    return-void
.end method

.method private initBackground(ZZ)V
    .locals 4
    .param p1, "useBlending"    # Z
    .param p2, "useTransitioner"    # Z

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-nez v0, :cond_0

    .line 1591
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlBackground;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlBackground;-><init>(Landroid/content/Context;Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    .line 1592
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlBackground;->enableBlending(ZZ)V

    .line 1593
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setClearByColor(Z)V

    .line 1595
    :cond_0
    return-void

    .line 1593
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initializeGL(Z)V
    .locals 6
    .param p1, "force"    # Z

    .prologue
    const/4 v5, 0x0

    .line 1650
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeGL = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", force = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1652
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1663
    :goto_0
    return-void

    .line 1654
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    .line 1655
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorRed:F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorGreen:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorBlue:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorAlpha:F

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glClearColor(FFFF)V

    .line 1656
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/TUtils;->initTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 1657
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-nez v0, :cond_1

    .line 1658
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->setObjectVertex()V

    .line 1662
    :goto_1
    invoke-direct {p0, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->initBackground(ZZ)V

    goto :goto_0

    .line 1660
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;-><init>(Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    goto :goto_1
.end method

.method private isCmdQueueNotEmpty()Z
    .locals 4

    .prologue
    .line 1357
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 1359
    .local v1, "index":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v3

    .line 1360
    :cond_0
    :goto_0
    :try_start_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-eq v1, v2, :cond_2

    .line 1361
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    aget-object v0, v2, v1

    .line 1362
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    if-eqz v2, :cond_1

    .line 1363
    const/4 v2, 0x1

    monitor-exit v3

    .line 1369
    .end local v0    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    :goto_1
    return v2

    .line 1364
    .restart local v0    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 1365
    const/16 v2, 0x400

    if-ne v1, v2, :cond_0

    .line 1366
    const/4 v1, 0x0

    goto :goto_0

    .line 1368
    .end local v0    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    :cond_2
    monitor-exit v3

    .line 1369
    const/4 v2, 0x0

    goto :goto_1

    .line 1368
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private isRenderRequired()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1376
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v7, :cond_0

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isCmdQueueNotEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1416
    :cond_0
    :goto_0
    return v5

    .line 1380
    :cond_1
    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_4

    .line 1382
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    .line 1383
    .local v1, "animationMgr":Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;
    iget v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mMaxUsed:I

    .line 1384
    .local v0, "aniSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v0, :cond_4

    .line 1385
    iget-object v7, v1, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mAnimationSet:[Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    aget-object v2, v7, v3

    .line 1386
    .local v2, "glAnim":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    if-nez v2, :cond_3

    .line 1384
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1388
    :cond_3
    iget v7, v2, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    if-eqz v7, :cond_2

    goto :goto_0

    .line 1394
    .end local v0    # "aniSize":I
    .end local v1    # "animationMgr":Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;
    .end local v2    # "glAnim":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .end local v3    # "i":I
    :cond_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    monitor-enter v7

    .line 1395
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    invoke-virtual {v8}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1396
    monitor-exit v7

    move v5, v6

    goto :goto_0

    .line 1397
    :cond_5
    iget-object v8, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    invoke-virtual {v8}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;

    .line 1398
    .local v4, "listener":Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1402
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1404
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    invoke-interface {v4, v7, v8}, Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;->onGLIdle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v7

    if-nez v7, :cond_6

    .line 1407
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v5, v6

    goto :goto_0

    .line 1398
    .end local v4    # "listener":Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 1407
    .restart local v4    # "listener":Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1413
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    monitor-enter v6

    .line 1414
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    invoke-virtual {v7, v4}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 1415
    monitor-exit v6

    goto :goto_0

    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    .line 1407
    :catchall_2
    move-exception v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5
.end method

.method private isWideMode()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2447
    const/4 v0, 0x0

    .line 2448
    .local v0, "isWideMode":Z
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2449
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    move v0, v3

    .line 2458
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v4

    .line 2449
    goto :goto_0

    .line 2451
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    const-string/jumbo v6, "window"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 2452
    .local v2, "windowManager":Landroid/view/WindowManager;
    if-eqz v2, :cond_0

    .line 2453
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 2454
    .local v1, "rotation":I
    if-eq v1, v3, :cond_3

    const/4 v5, 0x3

    if-ne v1, v5, :cond_4

    :cond_3
    move v0, v3

    .line 2455
    :goto_1
    sget-object v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isWideMode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move v0, v4

    .line 2454
    goto :goto_1
.end method

.method private layoutContentPane()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 405
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    and-int/lit8 v5, v5, -0x3

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getWidth()I

    move-result v4

    .line 408
    .local v4, "w":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getHeight()I

    move-result v2

    .line 409
    .local v2, "h":I
    const/4 v1, 0x0

    .line 410
    .local v1, "displayRotation":I
    const/4 v0, 0x0

    .line 413
    .local v0, "compensation":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mOrientationSource:Lcom/sec/android/gallery3d/ui/OrientationSource;

    if-eqz v5, :cond_3

    .line 414
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mOrientationSource:Lcom/sec/android/gallery3d/ui/OrientationSource;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/ui/OrientationSource;->getDisplayRotation()I

    move-result v1

    .line 415
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mOrientationSource:Lcom/sec/android/gallery3d/ui/OrientationSource;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/ui/OrientationSource;->getCompensation()I

    move-result v0

    .line 421
    :goto_0
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    if-eq v5, v0, :cond_0

    .line 422
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    .line 423
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    rem-int/lit16 v5, v5, 0xb4

    if-eqz v5, :cond_4

    .line 424
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensationMatrix:Landroid/graphics/Matrix;

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 426
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensationMatrix:Landroid/graphics/Matrix;

    neg-int v6, v4

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    neg-int v7, v2

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 428
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensationMatrix:Landroid/graphics/Matrix;

    div-int/lit8 v6, v2, 0x2

    int-to-float v6, v6

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 433
    :cond_0
    :goto_1
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDisplayRotation:I

    .line 436
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    rem-int/lit16 v5, v5, 0xb4

    if-eqz v5, :cond_1

    .line 437
    move v3, v4

    .line 438
    .local v3, "tmp":I
    move v4, v2

    .line 439
    move v2, v3

    .line 443
    .end local v3    # "tmp":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    if-eqz v2, :cond_2

    .line 444
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v5, v9, v9, v4, v2}, Lcom/sec/android/gallery3d/ui/GLView;->layout(IIII)V

    .line 448
    :cond_2
    return-void

    .line 417
    :cond_3
    const/4 v1, 0x0

    .line 418
    const/4 v0, 0x0

    goto :goto_0

    .line 430
    :cond_4
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensationMatrix:Landroid/graphics/Matrix;

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    int-to-float v6, v6

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    div-int/lit8 v8, v2, 0x2

    int-to-float v8, v8

    invoke-virtual {v5, v6, v7, v8}, Landroid/graphics/Matrix;->setRotate(FFF)V

    goto :goto_1
.end method

.method private needLockForKeyEvent(I)Z
    .locals 2
    .param p1, "keyCode"    # I

    .prologue
    const/4 v0, 0x1

    .line 2474
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    if-ne v1, v0, :cond_1

    const/16 v1, 0xa8

    if-eq p1, v1, :cond_0

    const/16 v1, 0xa9

    if-eq p1, v1, :cond_0

    const/16 v1, 0x126

    if-ne p1, v1, :cond_1

    .line 2481
    :cond_0
    const/4 v0, 0x0

    .line 2484
    :cond_1
    return v0
.end method

.method private onCommand()V
    .locals 14

    .prologue
    const/16 v13, 0x400

    .line 2062
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 2063
    .local v8, "curTime":J
    iget v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    .line 2065
    .local v12, "toIndex":I
    const/4 v7, 0x0

    .line 2067
    .local v7, "postponeCmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    :goto_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    if-eq v0, v12, :cond_c

    .line 2068
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v1

    .line 2069
    if-eqz v7, :cond_2

    .line 2070
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    aput-object v7, v0, v2

    .line 2071
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-ne v0, v13, :cond_0

    .line 2072
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    .line 2073
    :cond_0
    const/4 v7, 0x0

    .line 2074
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    if-ne v0, v2, :cond_2

    .line 2076
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 2077
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    if-ne v0, v13, :cond_1

    .line 2078
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 2079
    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 2089
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2082
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    aget-object v6, v0, v2

    .line 2083
    .local v6, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 2084
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    if-ne v0, v13, :cond_3

    .line 2085
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 2086
    :cond_3
    if-eqz v6, :cond_4

    iget-boolean v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    if-nez v0, :cond_5

    .line 2087
    :cond_4
    monitor-exit v1

    goto :goto_0

    .line 2089
    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2090
    iget-wide v10, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mExpTime:J

    .line 2091
    .local v10, "expireTime":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-eqz v0, :cond_6

    cmp-long v0, v10, v8

    if-gez v0, :cond_b

    .line 2092
    :cond_6
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_VIEW:I

    if-ne v0, v1, :cond_7

    .line 2093
    iget-object v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlView;

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    iget-object v2, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    iget v3, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    iget v4, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    iget v5, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlView;->onCommand(ILjava/lang/Object;III)V

    goto :goto_0

    .line 2094
    :cond_7
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_OBJ:I

    if-ne v0, v1, :cond_8

    .line 2095
    iget-object v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    iget-object v2, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    iget v3, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    iget v4, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    iget v5, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->onCommand(ILjava/lang/Object;III)V

    goto/16 :goto_0

    .line 2096
    :cond_8
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_LAYER:I

    if-ne v0, v1, :cond_9

    .line 2097
    iget-object v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    iget-object v2, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    iget v3, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    iget v4, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    iget v5, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onCommand(ILjava/lang/Object;III)V

    goto/16 :goto_0

    .line 2098
    :cond_9
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_SYS:I

    if-ne v0, v1, :cond_a

    .line 2099
    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    iget-object v2, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    iget v3, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    iget v4, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    iget v5, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->procCommand(ILjava/lang/Object;III)V

    goto/16 :goto_0

    .line 2101
    :cond_a
    iget-object v0, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlHandler;

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    iget-object v2, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParmObj:Ljava/lang/Object;

    iget v3, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm1:I

    iget v4, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm2:I

    iget v5, v6, Lcom/sec/android/gallery3d/glcore/GlCmd;->mParm3:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlHandler;->onMessage(ILjava/lang/Object;III)V

    goto/16 :goto_0

    .line 2104
    :cond_b
    move-object v7, v6

    goto/16 :goto_0

    .line 2107
    .end local v6    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    .end local v10    # "expireTime":J
    :cond_c
    if-eqz v7, :cond_f

    .line 2108
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v1

    .line 2109
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    aput-object v7, v0, v2

    .line 2110
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-ne v0, v13, :cond_d

    .line 2111
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    .line 2112
    :cond_d
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    if-ne v0, v2, :cond_e

    .line 2113
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "onCommand Too many pending cmd - !! "

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2115
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_e
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2117
    :cond_f
    return-void
.end method

.method private onDragInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/DragEvent;)Z
    .locals 6
    .param p1, "parentObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "Event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v3, 0x1

    .line 2617
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2621
    .local v0, "childList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-ge v2, v4, :cond_3

    .line 2622
    aget-object v1, v0, v2

    .line 2623
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v1, :cond_0

    iget-boolean v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v4, :cond_0

    iget v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v5, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-eq v4, v5, :cond_1

    .line 2621
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2625
    :cond_1
    invoke-virtual {v1, p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2632
    .end local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :goto_1
    return v3

    .line 2627
    .restart local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    iget v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v4, :cond_0

    .line 2628
    invoke-direct {p0, v1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onDragInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/DragEvent;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    .line 2632
    .end local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private onDrawFrameLocked(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 22
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 588
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayerNew:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 589
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v18, :cond_0

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    .line 591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->clearAccessibilityFocus()V

    .line 594
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayerNew:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->replaceLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 598
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 600
    const/4 v13, 0x1

    .line 606
    .local v13, "newRenderLogic":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v13, v0, :cond_2

    .line 607
    sget-object v18, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "renderLogic is changed : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " -> "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    move/from16 v16, v0

    .line 609
    .local v16, "preRenderLogic":I
    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    .line 610
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v13, v0, :cond_9

    .line 611
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v18, :cond_8

    const/16 v18, 0x1

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    .line 612
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->resetGL(Z)V

    .line 616
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->reset()V

    .line 625
    .end local v16    # "preRenderLogic":I
    :cond_2
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onCommand()V

    .line 626
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    .line 627
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 628
    .local v9, "gl11":Ljavax/microedition/khronos/opengles/GL11;
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v13, v0, :cond_11

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    if-nez v18, :cond_3

    .line 632
    new-instance v18, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v9, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;-><init>(Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    move/from16 v20, v0

    invoke-interface/range {v18 .. v20}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setSize(II)V

    .line 636
    :cond_3
    const/16 v18, 0x4100

    move/from16 v0, v18

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/opengles/GL11;->glClear(I)V

    .line 638
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCurTime:J

    .line 639
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->processAnimation()Z

    move-result v10

    .line 643
    .local v10, "hasMoreAnimation":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->deleteRecycledResources()V

    .line 646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->resetUploadLimit()V

    .line 648
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    .line 650
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x2

    if-eqz v18, :cond_4

    .line 651
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->layoutContentPane()V

    .line 654
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    const/16 v19, -0x1

    invoke-interface/range {v18 .. v19}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 655
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    move/from16 v18, v0

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->rotateCanvas(I)V

    .line 656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 659
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 662
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_b

    .line 663
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->get()J

    move-result-wide v14

    .line 664
    .local v14, "now":J
    const/4 v11, 0x0

    .local v11, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v12

    .local v12, "n":I
    :goto_3
    if-ge v11, v12, :cond_a

    .line 665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/gallery3d/anim/CanvasAnimation;->setStartTime(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 664
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 601
    .end local v9    # "gl11":Ljavax/microedition/khronos/opengles/GL11;
    .end local v10    # "hasMoreAnimation":Z
    .end local v11    # "i":I
    .end local v12    # "n":I
    .end local v13    # "newRenderLogic":I
    .end local v14    # "now":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v18, v0

    if-eqz v18, :cond_7

    .line 602
    const/4 v13, 0x2

    .restart local v13    # "newRenderLogic":I
    goto/16 :goto_0

    .line 604
    .end local v13    # "newRenderLogic":I
    :cond_7
    const/4 v13, 0x0

    .restart local v13    # "newRenderLogic":I
    goto/16 :goto_0

    .line 614
    .restart local v16    # "preRenderLogic":I
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->resetGL()V

    goto/16 :goto_1

    .line 617
    :cond_9
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v13, v0, :cond_2

    .line 618
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    goto/16 :goto_2

    .line 667
    .end local v16    # "preRenderLogic":I
    .restart local v9    # "gl11":Ljavax/microedition/khronos/opengles/GL11;
    .restart local v10    # "hasMoreAnimation":Z
    .restart local v11    # "i":I
    .restart local v12    # "n":I
    .restart local v14    # "now":J
    :cond_a
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 674
    .end local v11    # "i":I
    .end local v12    # "n":I
    .end local v14    # "now":J
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->uploadLimitReached()Z

    move-result v18

    if-eqz v18, :cond_c

    .line 675
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 678
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 679
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleRunner:Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->enable()V

    .line 680
    :cond_d
    monitor-exit v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 691
    if-nez v10, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->uploadLimitReached()Z

    move-result v18

    if-eqz v18, :cond_f

    .line 692
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 749
    .end local v10    # "hasMoreAnimation":Z
    :cond_f
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v18, v0

    if-eqz v18, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->uploadCenterLargeThumbnail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v18

    if-eqz v18, :cond_10

    .line 750
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 754
    :cond_10
    :goto_5
    return-void

    .line 669
    .restart local v10    # "hasMoreAnimation":Z
    :catch_0
    move-exception v7

    .line 670
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 680
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v18

    :try_start_3
    monitor-exit v19
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v18

    .line 694
    .end local v10    # "hasMoreAnimation":Z
    :cond_11
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v13, v0, :cond_15

    .line 695
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    .line 696
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRefreshEnabled:Z

    move/from16 v18, v0

    if-nez v18, :cond_12

    .line 697
    sget-object v18, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string v19, "Skip update screen"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    const/16 v18, 0x4100

    move/from16 v0, v18

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/opengles/GL11;->glClear(I)V

    goto :goto_5

    .line 701
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->reset(Ljavax/microedition/khronos/opengles/GL11;)V
    invoke-static {v0, v9}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->access$400(Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;Ljavax/microedition/khronos/opengles/GL11;)V

    .line 706
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    move/from16 v18, v0

    if-lez v18, :cond_13

    .line 707
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    .line 708
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRect:Landroid/graphics/Rect;

    .line 709
    .local v6, "clip":Landroid/graphics/Rect;
    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v20

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glScissor(IIII)V

    .line 713
    .end local v6    # "clip":Landroid/graphics/Rect;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    move-object/from16 v18, v0

    if-eqz v18, :cond_14

    .line 714
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/glcore/GlBackground;->draw()V

    .line 717
    :cond_14
    const/16 v18, 0x1701

    move/from16 v0, v18

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/glcore/GlConfig;->getProspectMatrix()[F

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v9, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 719
    const/16 v18, 0x1700

    move/from16 v0, v18

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 722
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCurTime:J

    .line 723
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCurTime:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onPosition(J)V

    .line 733
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFgndDraw:Ljava/util/LinkedList;

    .line 734
    .local v8, "fgndDraw":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;>;"
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v17

    .line 735
    .local v17, "size":I
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_6
    move/from16 v0, v17

    if-ge v11, v0, :cond_f

    .line 736
    invoke-virtual {v8, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;

    move-object/from16 v0, v18

    invoke-interface {v0, v9}, Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;->onDraw(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 735
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 740
    .end local v8    # "fgndDraw":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;>;"
    .end local v11    # "i":I
    .end local v17    # "size":I
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    move/from16 v18, v0

    if-lez v18, :cond_16

    .line 741
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    .line 742
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRect:Landroid/graphics/Rect;

    .line 743
    .restart local v6    # "clip":Landroid/graphics/Rect;
    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v20

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-interface {v9, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glScissor(IIII)V

    .line 745
    .end local v6    # "clip":Landroid/graphics/Rect;
    :cond_16
    const/16 v18, 0x4100

    move/from16 v0, v18

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/opengles/GL11;->glClear(I)V

    goto/16 :goto_4
.end method

.method private onPosition(J)V
    .locals 7
    .param p1, "curTime"    # J

    .prologue
    const/4 v6, 0x0

    .line 2121
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    iget v3, v4, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mMaxUsed:I

    .line 2122
    .local v3, "numObject":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 2123
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    iget-object v4, v4, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mAnimationSet:[Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    aget-object v0, v4, v2

    .line 2124
    .local v0, "glAnim":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    if-nez v0, :cond_1

    .line 2122
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2126
    :cond_1
    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    if-eqz v4, :cond_0

    .line 2127
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->process(J)V

    goto :goto_1

    .line 2130
    .end local v0    # "glAnim":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v4, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v4, :cond_4

    .line 2131
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2132
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-boolean v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    if-eqz v4, :cond_3

    .line 2133
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPositionsInter(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2134
    iput-boolean v6, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 2136
    :cond_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlObject;->draw(Ljavax/microedition/khronos/opengles/GL11;ZZ)V

    .line 2137
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onPositionInter(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 2139
    .end local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_4
    return-void
.end method

.method private onPositionInter(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 10
    .param p1, "parentObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2142
    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2144
    .local v1, "childList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 2146
    .local v0, "childCount":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 2149
    .local v2, "gl11":Ljavax/microedition/khronos/opengles/GL11;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_7

    .line 2150
    aget-object v3, v1, v4

    .line 2151
    .local v3, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v3, :cond_0

    iget-boolean v6, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v6, :cond_0

    iget v6, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v7, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-eq v6, v7, :cond_1

    .line 2149
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2153
    :cond_1
    iget-boolean v6, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    if-eqz v6, :cond_2

    .line 2154
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPositionsInter(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2155
    iput-boolean v9, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 2157
    :cond_2
    iget-boolean v5, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mUseClipping:Z

    .line 2158
    .local v5, "useClipping":Z
    if-eqz v5, :cond_3

    .line 2159
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget-object v7, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->setClipRect(Landroid/graphics/Rect;)V

    .line 2161
    :cond_3
    iget-boolean v6, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mPostDraw:Z

    if-eqz v6, :cond_6

    .line 2162
    invoke-virtual {v3, v2, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlObject;->draw(Ljavax/microedition/khronos/opengles/GL11;ZZ)V

    .line 2163
    iget v6, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v6, :cond_4

    .line 2164
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onPositionInter(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 2166
    :cond_4
    invoke-virtual {v3, v2, v9, v8}, Lcom/sec/android/gallery3d/glcore/GlObject;->draw(Ljavax/microedition/khronos/opengles/GL11;ZZ)V

    .line 2173
    :cond_5
    :goto_2
    if-eqz v5, :cond_0

    .line 2174
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->resetClipRect()V

    goto :goto_1

    .line 2168
    :cond_6
    invoke-virtual {v3, v2, v8, v8}, Lcom/sec/android/gallery3d/glcore/GlObject;->draw(Ljavax/microedition/khronos/opengles/GL11;ZZ)V

    .line 2169
    iget v6, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v6, :cond_5

    .line 2170
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onPositionInter(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_2

    .line 2177
    .end local v3    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v5    # "useClipping":Z
    :cond_7
    return-void
.end method

.method private outputFps()V
    .locals 8

    .prologue
    .line 531
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 532
    .local v0, "now":J
    iget-wide v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCountingStart:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 533
    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCountingStart:J

    .line 540
    :cond_0
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCount:I

    .line 541
    return-void

    .line 534
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCountingStart:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x3b9aca00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 535
    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fps: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCount:I

    int-to-double v4, v4

    const-wide v6, 0x41cdcd6500000000L    # 1.0E9

    mul-double/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCountingStart:J

    sub-long v6, v0, v6

    long-to-double v6, v6

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    iput-wide v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCountingStart:J

    .line 538
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFrameCount:I

    goto :goto_0
.end method

.method private procCommand(ILjava/lang/Object;III)V
    .locals 0
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 2051
    packed-switch p1, :pswitch_data_0

    .line 2059
    :goto_0
    return-void

    .line 2053
    :pswitch_0
    invoke-virtual {p0, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->procSurfaceChanged(II)V

    goto :goto_0

    .line 2056
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->reset()V

    goto :goto_0

    .line 2051
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private processAnimation()Z
    .locals 10

    .prologue
    .line 1324
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    .line 1325
    .local v2, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/anim/CanvasAnimation;>;"
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRunningAnimations:Ljava/util/ArrayList;

    .line 1328
    .local v6, "runningAnimations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/anim/Animation;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1329
    .local v0, "aniSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_1

    .line 1330
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/anim/Animation;

    .line 1331
    .local v1, "animation":Lcom/sec/android/gallery3d/anim/Animation;
    if-nez v1, :cond_0

    .line 1329
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1333
    :cond_0
    iget-wide v8, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCurTime:J

    invoke-virtual {v1, v8, v9}, Lcom/sec/android/gallery3d/anim/Animation;->setStartTime(J)V

    .line 1334
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1336
    .end local v1    # "animation":Lcom/sec/android/gallery3d/anim/Animation;
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1339
    const/4 v3, 0x0

    .line 1340
    .local v3, "hasPendingAnim":Z
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1341
    add-int/lit8 v4, v0, -0x1

    :goto_2
    if-ltz v4, :cond_3

    .line 1342
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/anim/Animation;

    .line 1343
    .restart local v1    # "animation":Lcom/sec/android/gallery3d/anim/Animation;
    iget-wide v8, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCurTime:J

    invoke-virtual {v1, v8, v9}, Lcom/sec/android/gallery3d/anim/Animation;->calculate(J)Z

    .line 1344
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/anim/Animation;->isActive()Z

    move-result v5

    .line 1345
    .local v5, "isActive":Z
    if-nez v5, :cond_2

    .line 1346
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1348
    :cond_2
    or-int/2addr v3, v5

    .line 1341
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 1350
    .end local v1    # "animation":Lcom/sec/android/gallery3d/anim/Animation;
    .end local v5    # "isActive":Z
    :cond_3
    if-eqz v3, :cond_4

    .line 1351
    const/4 v7, 0x1

    .line 1352
    :goto_3
    return v7

    :cond_4
    const/4 v7, 0x0

    goto :goto_3
.end method

.method private registerForStylusPenEvent()V
    .locals 3

    .prologue
    .line 2425
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/dmc/ocr/OcrUtils;->isOCRAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2426
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2427
    .local v0, "observer":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    if-eqz v1, :cond_1

    .line 2433
    .end local v0    # "observer":Landroid/view/ViewTreeObserver;
    :cond_0
    :goto_0
    return-void

    .line 2430
    .restart local v0    # "observer":Landroid/view/ViewTreeObserver;
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlRootView$1;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    .line 2431
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewTreeObserver;->addOnStylusButtonEventListener(Landroid/content/Context;Landroid/view/ViewTreeObserver$OnStylusButtonEventListener;)V

    goto :goto_0
.end method

.method private removeAllObjectInter(Lcom/sec/android/gallery3d/glcore/GlObject;Z)V
    .locals 4
    .param p1, "parentObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "useNoDetach"    # Z

    .prologue
    .line 1793
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1796
    .local v0, "childList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v3, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_4

    .line 1797
    aget-object v1, v0, v2

    .line 1798
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v1, :cond_0

    .line 1796
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1800
    :cond_0
    iget v3, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v3, :cond_1

    .line 1801
    invoke-direct {p0, v1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObjectInter(Lcom/sec/android/gallery3d/glcore/GlObject;Z)V

    .line 1802
    :cond_1
    if-nez p2, :cond_2

    .line 1803
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_1

    .line 1804
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLObjectNoDetach:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1805
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAllListeners()V

    .line 1806
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 1807
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Ljava/lang/Object;)V

    goto :goto_1

    .line 1809
    :cond_3
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_1

    .line 1812
    .end local v1    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_4
    return-void
.end method

.method private removeForStylusPenEvent()V
    .locals 2

    .prologue
    .line 2438
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    if-nez v1, :cond_0

    .line 2444
    :goto_0
    return-void

    .line 2441
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2442
    .local v0, "observer":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnStylusButtonEventListener(Landroid/view/ViewTreeObserver$OnStylusButtonEventListener;)V

    .line 2443
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStylusEventListener:Lcom/sec/android/gallery3d/glcore/GlRootView$StylusEventListener;

    goto :goto_0
.end method

.method private reset()V
    .locals 4

    .prologue
    .line 2297
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 2298
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    const v1, 0x8892

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 2300
    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 2301
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 2303
    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 2304
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 2306
    const/16 v1, 0xbe2

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 2307
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/high16 v3, 0x46040000    # 8448.0f

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 2309
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/TUtils;->setObjectVertex()V

    .line 2311
    return-void
.end method

.method private resetGL()V
    .locals 1

    .prologue
    .line 1602
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->resetGL(Z)V

    .line 1603
    return-void
.end method

.method private resetGL(Z)V
    .locals 8
    .param p1, "needClearColor"    # Z

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 1605
    const/16 v3, 0x10

    new-array v2, v3, [F

    .line 1606
    .local v2, "prjMatrix":[F
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 1608
    .local v1, "gl":Ljavax/microedition/khronos/opengles/GL11;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getWidth()I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    .line 1609
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    .line 1610
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    invoke-interface {v1, v6, v6, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glViewport(IIII)V

    .line 1611
    const/16 v3, 0x1701

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 1612
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 1613
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 1615
    .local v0, "aspect":F
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlConfig;->setSize(FF)V

    .line 1617
    invoke-static {v2, v6}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1618
    const/high16 v3, 0x41f00000    # 30.0f

    const/high16 v4, 0x44fa0000    # 2000.0f

    invoke-static {v2, v3, v0, v7, v4}, Lcom/sec/android/gallery3d/glcore/TUtils;->glhPerspectivef2([FFFFF)V

    .line 1619
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/glcore/GlConfig;->setProspectMatrix([F)V

    .line 1620
    invoke-interface {v1, v2, v6}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 1622
    if-eqz p1, :cond_0

    .line 1623
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorRed:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorGreen:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorBlue:F

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorAlpha:F

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL11;->glClearColor(FFFF)V

    .line 1624
    :cond_0
    invoke-interface {v1, v7}, Ljavax/microedition/khronos/opengles/GL11;->glClearDepthf(F)V

    .line 1625
    const/16 v3, 0xbe2

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 1627
    const/16 v3, 0xc50

    const/16 v4, 0x1102

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glHint(II)V

    .line 1630
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isSlideShow()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-boolean v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->isSlideShowActive:Z

    if-nez v3, :cond_2

    .line 1631
    :cond_1
    const/16 v3, 0xb71

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 1634
    :cond_2
    const v3, 0x8074

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glEnableClientState(I)V

    .line 1637
    const/16 v3, 0xde1

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 1640
    const v3, 0x8078

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glEnableClientState(I)V

    .line 1643
    const v3, 0x84c0

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 1644
    const/16 v3, 0x1700

    invoke-interface {v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 1645
    return-void
.end method

.method private rotateCanvas(I)V
    .locals 8
    .param p1, "degrees"    # I

    .prologue
    const/4 v7, 0x0

    .line 757
    if-nez p1, :cond_0

    .line 769
    :goto_0
    return-void

    .line 758
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getWidth()I

    move-result v3

    .line 759
    .local v3, "w":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getHeight()I

    move-result v2

    .line 760
    .local v2, "h":I
    div-int/lit8 v0, v3, 0x2

    .line 761
    .local v0, "cx":I
    div-int/lit8 v1, v2, 0x2

    .line 762
    .local v1, "cy":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    int-to-float v5, v0

    int-to-float v6, v1

    invoke-interface {v4, v5, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 763
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    int-to-float v5, p1

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v4, v5, v7, v7, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->rotate(FFFF)V

    .line 764
    rem-int/lit16 v4, p1, 0xb4

    if-eqz v4, :cond_1

    .line 765
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    neg-int v5, v1

    int-to-float v5, v5

    neg-int v6, v0

    int-to-float v6, v6

    invoke-interface {v4, v5, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    goto :goto_0

    .line 767
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    neg-int v5, v0

    int-to-float v5, v5

    neg-int v6, v1

    int-to-float v6, v6

    invoke-interface {v4, v5, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    goto :goto_0
.end method

.method private superRequestRender()V
    .locals 0

    .prologue
    .line 384
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 385
    return-void
.end method


# virtual methods
.method public addObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 2
    .param p1, "glObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 1725
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 1726
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iput-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    .line 1727
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1728
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->add(Ljava/util/ArrayList;)V

    .line 1729
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1730
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    .line 1732
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 1733
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_1

    .line 1734
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 1736
    :cond_1
    return-void
.end method

.method public addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 3
    .param p1, "Object"    # [Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 1940
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 1941
    aget-object v1, p1, v0

    if-eqz v1, :cond_0

    .line 1942
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLObjectNoDetach:Ljava/util/ArrayList;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1940
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1944
    :cond_1
    return-void
.end method

.method public addOnForegroundDrawer(Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;)V
    .locals 1
    .param p1, "drawer"    # Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;

    .prologue
    .line 1713
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFgndDraw:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1714
    return-void
.end method

.method public addOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;

    .prologue
    .line 305
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 306
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 307
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleRunner:Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->enable()V

    .line 309
    :cond_0
    monitor-exit v1

    .line 310
    return-void

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 1
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 2267
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2271
    :cond_0
    :goto_0
    return-void

    .line 2270
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V
    .locals 3
    .param p1, "newLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;

    .prologue
    .line 2396
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 2398
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "attachLayer = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2399
    if-eqz p1, :cond_0

    .line 2400
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setRootView(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    .line 2401
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setLayerCallback(Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 2403
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayerNew:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2404
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_1

    .line 2405
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string v1, "exit attachLayer"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2409
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 2411
    return-void

    .line 2408
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string v2, "exit attachLayer"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2409
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public cancelRenderObjectInThreadPool(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 2636
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 2638
    .local v0, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Lcom/sec/android/gallery3d/glcore/GlObject;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 2639
    :cond_0
    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->onFreeBitmap()V

    .line 2640
    return-void
.end method

.method public checkThread()V
    .locals 2

    .prologue
    .line 1885
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1886
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Only GLThread can call this"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1888
    :cond_0
    return-void
.end method

.method public cleanForegroundDrawer()V
    .locals 1

    .prologue
    .line 1721
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFgndDraw:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 1722
    return-void
.end method

.method public clearAllAnimation()V
    .locals 4

    .prologue
    .line 1831
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    iget v2, v3, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mMaxUsed:I

    .line 1832
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1833
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mAnimationSet:[Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    aget-object v0, v3, v1

    .line 1834
    .local v0, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    if-nez v0, :cond_0

    .line 1832
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1836
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_1

    .line 1838
    .end local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->clear()V

    .line 1839
    return-void
.end method

.method public clearCanvas()V
    .locals 1

    .prologue
    .line 2513
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 2514
    return-void
.end method

.method public clearCommandQueue()V
    .locals 3

    .prologue
    .line 2289
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v1

    .line 2290
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2291
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    .line 2292
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 2293
    monitor-exit v1

    .line 2294
    return-void

    .line 2293
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearNoDetach()V
    .locals 1

    .prologue
    .line 1815
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLObjectNoDetach:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1816
    return-void
.end method

.method public clearStatus()V
    .locals 2

    .prologue
    .line 1910
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1911
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 1912
    monitor-exit v1

    .line 1913
    return-void

    .line 1912
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xa

    const/16 v5, 0x8

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v3, 0x0

    .line 1105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1106
    .local v0, "action":I
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v2, :cond_1

    .line 1107
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHoveringUI:Z

    if-eqz v2, :cond_0

    if-ne v0, v5, :cond_0

    .line 1108
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1109
    .local v1, "cancel":Landroid/view/MotionEvent;
    invoke-virtual {v1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1110
    invoke-virtual {v1, v4, v4}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1111
    invoke-super {p0, v1}, Landroid/opengl/GLSurfaceView;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 1112
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 1123
    .end local v1    # "cancel":Landroid/view/MotionEvent;
    :goto_0
    if-ne v0, v5, :cond_4

    .line 1124
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->isActive()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1125
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1146
    :goto_1
    return v2

    .line 1114
    :cond_0
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 1117
    :cond_1
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 1128
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v2, :cond_3

    .line 1129
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_1

    :cond_3
    move v2, v3

    .line 1130
    goto :goto_1

    .line 1133
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    if-eqz v2, :cond_6

    :cond_5
    move v2, v3

    .line 1134
    goto :goto_1

    .line 1137
    :cond_6
    const/16 v2, 0x9

    if-ne v0, v2, :cond_8

    .line 1138
    const/16 v2, 0x3e8

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1145
    :cond_7
    :goto_2
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move v2, v3

    .line 1146
    goto :goto_1

    .line 1139
    :cond_8
    if-ne v0, v6, :cond_9

    .line 1140
    const/16 v2, 0x3e9

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_2

    .line 1141
    :cond_9
    const/4 v2, 0x7

    if-ne v0, v2, :cond_7

    .line 1142
    const/16 v2, 0x3ea

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_2
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 19
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHovering(Landroid/content/Context;)Z

    move-result v14

    .line 1185
    .local v14, "isPenHoveringOn":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getFingerAirView(Landroid/content/Context;)Z

    move-result v6

    .line 1186
    .local v6, "isFingerAirViewOn":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getMouseHovering(Landroid/content/Context;)Z

    move-result v12

    .line 1187
    .local v12, "isMouseHoveringOn":Z
    if-nez v14, :cond_0

    if-nez v6, :cond_0

    if-eqz v12, :cond_6

    :cond_0
    const/4 v10, 0x1

    .line 1191
    .local v10, "isHoveringOn":Z
    :goto_0
    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHoveringListScroll(Landroid/content/Context;)Z

    move-result v17

    if-nez v17, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getMouseHoveringListScroll(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_7

    :cond_2
    const/4 v8, 0x1

    .line 1194
    .local v8, "isHoverListScrollOn":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHoveringInformationPreview(Landroid/content/Context;)Z

    move-result v13

    .line 1195
    .local v13, "isPenHoverInfoPreviewOn":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getFingerAirViewInformationPreview(Landroid/content/Context;)Z

    move-result v5

    .line 1196
    .local v5, "isFingerAirViewInformationPreviewOn":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getMouseHoveringInformationPreview(Landroid/content/Context;)Z

    move-result v11

    .line 1197
    .local v11, "isMouseHoveringInformationPreviewOn":Z
    if-nez v13, :cond_3

    if-nez v5, :cond_3

    if-eqz v11, :cond_8

    :cond_3
    const/4 v7, 0x1

    .line 1204
    .local v7, "isHoverInfoPreviewOn":Z
    :goto_2
    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mToolType:I

    .line 1206
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->isHoveringIconPenSelect()Z

    move-result v9

    .line 1208
    .local v9, "isHoverPenSelect":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 1209
    if-nez v9, :cond_4

    .line 1210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->isHoveringIconPenSelectView(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1211
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToPenSelect()V

    .line 1219
    :cond_4
    :goto_3
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHoveringUI:Z

    if-eqz v17, :cond_5

    if-eqz v10, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isSlideShow()Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1220
    :cond_5
    const/4 v4, 0x0

    .line 1307
    :goto_4
    return v4

    .line 1187
    .end local v5    # "isFingerAirViewInformationPreviewOn":Z
    .end local v7    # "isHoverInfoPreviewOn":Z
    .end local v8    # "isHoverListScrollOn":Z
    .end local v9    # "isHoverPenSelect":Z
    .end local v10    # "isHoveringOn":Z
    .end local v11    # "isMouseHoveringInformationPreviewOn":Z
    .end local v13    # "isPenHoverInfoPreviewOn":Z
    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1191
    .restart local v10    # "isHoveringOn":Z
    :cond_7
    const/4 v8, 0x0

    goto :goto_1

    .line 1197
    .restart local v5    # "isFingerAirViewInformationPreviewOn":Z
    .restart local v8    # "isHoverListScrollOn":Z
    .restart local v11    # "isMouseHoveringInformationPreviewOn":Z
    .restart local v13    # "isPenHoverInfoPreviewOn":Z
    :cond_8
    const/4 v7, 0x0

    goto :goto_2

    .line 1214
    .restart local v7    # "isHoverInfoPreviewOn":Z
    .restart local v9    # "isHoverPenSelect":Z
    :cond_9
    if-eqz v9, :cond_4

    .line 1215
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToDefault()V

    goto :goto_3

    .line 1221
    :cond_a
    if-nez v14, :cond_b

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 1222
    const/4 v4, 0x0

    goto :goto_4

    .line 1223
    :cond_b
    if-nez v6, :cond_c

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 1224
    const/4 v4, 0x0

    goto :goto_4

    .line 1225
    :cond_c
    if-nez v12, :cond_d

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_d

    .line 1226
    const/4 v4, 0x0

    goto :goto_4

    .line 1228
    :cond_d
    if-nez v13, :cond_11

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_11

    .line 1229
    const/4 v7, 0x0

    .line 1235
    :cond_e
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 1237
    .local v3, "action":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getShowAirButton()Z

    move-result v17

    if-eqz v17, :cond_f

    .line 1238
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;

    move-result-object v2

    .line 1239
    .local v2, "aB":Lcom/samsung/android/airbutton/AirButtonImpl;
    if-eqz v2, :cond_f

    .line 1240
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/airbutton/AirButtonImpl;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1243
    .end local v2    # "aB":Lcom/samsung/android/airbutton/AirButtonImpl;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1245
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    move-object/from16 v17, v0

    if-eqz v17, :cond_13

    .line 1246
    if-eqz v8, :cond_1b

    .line 1247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    .line 1248
    .local v4, "handled":Z
    const/16 v17, 0x9

    move/from16 v0, v17

    if-ne v3, v0, :cond_10

    if-eqz v4, :cond_10

    .line 1249
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInHoverState:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1305
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_4

    .line 1230
    .end local v3    # "action":I
    .end local v4    # "handled":Z
    :cond_11
    if-nez v5, :cond_12

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_12

    .line 1231
    const/4 v7, 0x0

    goto :goto_5

    .line 1232
    :cond_12
    if-nez v11, :cond_e

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v17

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_e

    .line 1233
    const/4 v7, 0x0

    goto :goto_5

    .line 1254
    .restart local v3    # "action":I
    :cond_13
    const/4 v4, 0x0

    .line 1255
    .restart local v4    # "handled":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    move-object/from16 v16, v0

    .line 1256
    .local v16, "obj":Ljava/lang/Object;
    if-eqz v16, :cond_17

    .line 1257
    const/16 v17, 0x9

    move/from16 v0, v17

    if-eq v3, v0, :cond_14

    const/16 v17, 0xa

    move/from16 v0, v17

    if-ne v3, v0, :cond_16

    .line 1259
    :cond_14
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 1260
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_15

    .line 1261
    move-object/from16 v0, v16

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    .line 1264
    :goto_6
    const/16 v17, 0xa

    move/from16 v0, v17

    if-ne v3, v0, :cond_17

    .line 1265
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInHoverState:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1266
    const/4 v4, 0x1

    .line 1305
    .end local v4    # "handled":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_4

    .line 1263
    .restart local v4    # "handled":Z
    :cond_15
    :try_start_2
    move-object/from16 v0, v16

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_6

    .line 1268
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_17

    .line 1269
    move-object/from16 v0, v16

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    .line 1272
    :cond_17
    if-nez v4, :cond_1b

    if-eqz v7, :cond_1b

    .line 1273
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v15

    .line 1274
    .local v15, "newEvent":Landroid/view/MotionEvent;
    const/16 v17, 0x9

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1277
    if-eqz v8, :cond_18

    .line 1278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v17, v0

    if-eqz v17, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v17

    if-eqz v17, :cond_18

    .line 1279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 1280
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    .line 1281
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInHoverState:Z

    .line 1282
    invoke-virtual {v15}, Landroid/view/MotionEvent;->recycle()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1283
    const/4 v4, 0x1

    .line 1305
    .end local v4    # "handled":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_4

    .line 1288
    .restart local v4    # "handled":Z
    :cond_18
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    move/from16 v17, v0

    if-lez v17, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v15}, Lcom/sec/android/gallery3d/glcore/GlRootView;->dispatchHoverEventInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/MotionEvent;)Z

    move-result v17

    if-eqz v17, :cond_19

    .line 1290
    invoke-virtual {v15}, Landroid/view/MotionEvent;->recycle()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1291
    const/4 v4, 0x1

    .line 1305
    .end local v4    # "handled":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_4

    .line 1295
    .restart local v4    # "handled":Z
    :cond_19
    if-eqz v16, :cond_1a

    :try_start_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1a

    check-cast v16, Lcom/sec/android/gallery3d/glcore/GlObject;

    .end local v16    # "obj":Ljava/lang/Object;
    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v17

    if-nez v17, :cond_1a

    .line 1297
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 1298
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInHoverState:Z

    .line 1301
    :cond_1a
    invoke-virtual {v15}, Landroid/view/MotionEvent;->recycle()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1305
    .end local v4    # "handled":Z
    .end local v15    # "newEvent":Landroid/view/MotionEvent;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1307
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1305
    :catchall_0
    move-exception v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v17
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1532
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v0, :cond_0

    .line 1533
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1535
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v11, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/16 v10, 0x3e8

    const/4 v3, 0x1

    .line 774
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    instance-of v7, v7, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isVisibleState()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    move v3, v8

    .line 885
    :goto_0
    return v3

    .line 777
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 803
    .local v1, "action":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    if-eqz v7, :cond_2

    .line 804
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->dispatchTouchEvent(Landroid/view/MotionEvent;)V

    .line 807
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 809
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v7, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 810
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationDetector:Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;

    invoke-virtual {v7, p1}, Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 812
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v7, :cond_5

    .line 813
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v7, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 814
    .local v3, "handled":Z
    if-nez v1, :cond_4

    if-eqz v3, :cond_4

    .line 815
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInDownState:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 885
    :cond_3
    :goto_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 816
    :cond_4
    :try_start_1
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v7, :cond_3

    if-ne v1, v10, :cond_3

    .line 817
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    if-eqz v7, :cond_3

    .line 818
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    const/16 v8, 0x80

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 885
    .end local v3    # "handled":Z
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v7

    .line 822
    :cond_5
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    if-eqz v7, :cond_9

    .line 823
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 824
    .local v4, "obj":Ljava/lang/Object;
    if-eqz v1, :cond_6

    if-eq v1, v10, :cond_6

    const/16 v7, 0x3ea

    if-eq v1, v7, :cond_6

    const/16 v7, 0xd3

    if-eq v1, v7, :cond_6

    const/16 v7, 0x3eb

    if-ne v1, v7, :cond_f

    .line 827
    :cond_6
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 828
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 829
    .local v2, "cancel":Landroid/view/MotionEvent;
    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionEventType:I

    if-ne v7, v9, :cond_b

    .line 830
    invoke-virtual {v2, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 834
    :goto_2
    if-eq v1, v10, :cond_7

    const/16 v7, 0x3ea

    if-eq v1, v7, :cond_7

    const/16 v7, 0x3eb

    if-eq v1, v7, :cond_7

    .line 836
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/view/MotionEvent;->setAction(I)V

    .line 838
    :cond_7
    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    if-ne v7, v9, :cond_c

    .line 839
    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlView;

    .end local v4    # "obj":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 849
    :cond_8
    :goto_3
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 867
    .end local v2    # "cancel":Landroid/view/MotionEvent;
    :cond_9
    if-eqz v1, :cond_a

    if-eq v1, v10, :cond_a

    const/16 v7, 0x3ea

    if-eq v1, v7, :cond_a

    const/16 v7, 0xd3

    if-eq v1, v7, :cond_a

    const/16 v7, 0x3eb

    if-ne v1, v7, :cond_16

    .line 870
    :cond_a
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v7, v7, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v7, :cond_14

    .line 871
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {p0, v7, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->dispatchTouchEventInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/MotionEvent;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v7

    if-eqz v7, :cond_14

    .line 885
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 832
    .restart local v2    # "cancel":Landroid/view/MotionEvent;
    .restart local v4    # "obj":Ljava/lang/Object;
    :cond_b
    const/4 v7, 0x3

    :try_start_3
    invoke-virtual {v2, v7}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_2

    .line 840
    :cond_c
    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    if-ne v7, v11, :cond_e

    .line 841
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v5, v7

    .line 842
    .local v5, "x":I
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v6, v7

    .line 843
    .local v6, "y":I
    const/16 v7, 0x3ea

    if-ne v1, v7, :cond_d

    move-object v0, v4

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object v7, v0

    invoke-virtual {v7, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v7

    if-nez v7, :cond_8

    .line 844
    :cond_d
    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlObject;

    .end local v4    # "obj":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_3

    .line 847
    .end local v5    # "x":I
    .end local v6    # "y":I
    .restart local v4    # "obj":Ljava/lang/Object;
    :cond_e
    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlLayer;

    .end local v4    # "obj":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onTouch(Landroid/view/MotionEvent;)Z

    goto :goto_3

    .line 851
    .end local v2    # "cancel":Landroid/view/MotionEvent;
    .restart local v4    # "obj":Ljava/lang/Object;
    :cond_f
    if-eq v1, v11, :cond_10

    if-eq v1, v3, :cond_10

    if-eq v1, v10, :cond_10

    const/16 v7, 0xd4

    if-eq v1, v7, :cond_10

    const/16 v7, 0x3eb

    if-ne v1, v7, :cond_11

    .line 854
    :cond_10
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInDownState:Z

    .line 855
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 857
    :cond_11
    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    if-ne v7, v9, :cond_12

    .line 858
    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlView;

    .end local v4    # "obj":Ljava/lang/Object;
    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 885
    :goto_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 859
    .restart local v4    # "obj":Ljava/lang/Object;
    :cond_12
    :try_start_4
    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    if-ne v7, v11, :cond_13

    .line 860
    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlObject;

    .end local v4    # "obj":Ljava/lang/Object;
    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_4

    .line 862
    .restart local v4    # "obj":Ljava/lang/Object;
    :cond_13
    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlLayer;

    .end local v4    # "obj":Ljava/lang/Object;
    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_4

    .line 874
    :cond_14
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlLayer;->isActive()Z

    move-result v7

    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v7, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 875
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iput-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 876
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTargetType:I

    .line 877
    if-nez v1, :cond_15

    move v7, v3

    :goto_5
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionEventType:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 885
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    :cond_15
    move v7, v9

    .line 877
    goto :goto_5

    .line 885
    :cond_16
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v7}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v3, v8

    goto/16 :goto_0
.end method

.method public extractSmartClipData(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I
    .locals 11
    .param p1, "resultElement"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;
    .param p2, "croppedArea"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1152
    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->extractSmartClipData(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I

    move-result v8

    .line 1155
    .local v8, "extractionResult":I
    new-array v9, v10, [I

    .line 1156
    .local v9, "screenOffsetOfView":[I
    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getLocationOnScreen([I)V

    .line 1157
    aget v4, v9, v0

    .line 1158
    .local v4, "scrOffsetX":I
    aget v5, v9, v6

    .line 1160
    .local v5, "scrOffsetY":I
    const/4 v3, 0x0

    .line 1161
    .local v3, "metaAreaRect":Landroid/graphics/Rect;
    invoke-interface {p2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    .local v2, "cropRect":Landroid/graphics/Rect;
    move-object v7, p1

    .line 1162
    check-cast v7, Lcom/samsung/android/smartclip/SmartClipDataElementImpl;

    .line 1163
    .local v7, "elementImpl":Lcom/samsung/android/smartclip/SmartClipDataElementImpl;
    invoke-virtual {v7}, Lcom/samsung/android/smartclip/SmartClipDataElementImpl;->getExtractionMode()I

    move-result v1

    if-ne v1, v10, :cond_1

    .line 1164
    .local v6, "isDragAndDropMode":Z
    :goto_0
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "extractSmartClipData : croppedArea = "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;->getRect()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " / GlRootViewOffset = "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, ", "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1167
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v0, :cond_0

    .line 1168
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->extractSmartClipDataInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)Landroid/graphics/Rect;

    move-result-object v3

    .line 1172
    :cond_0
    if-eqz v3, :cond_2

    .line 1173
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "extractSmartClipData : Meta data rect = "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    invoke-interface {p1, v3}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->setMetaAreaRect(Landroid/graphics/Rect;)V

    .line 1175
    new-instance v0, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    const-string v1, "plain_text"

    const-string v10, ""

    invoke-direct {v0, v1, v10}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;->addTag(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;)V

    .line 1179
    :goto_1
    return v8

    .end local v6    # "isDragAndDropMode":Z
    :cond_1
    move v6, v0

    .line 1163
    goto :goto_0

    .line 1177
    .restart local v6    # "isDragAndDropMode":Z
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string v1, "extractSmartClipData : Could not find any meta data"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public extractSmartClipDataInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)Landroid/graphics/Rect;
    .locals 13
    .param p1, "parentObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "cropRect"    # Landroid/graphics/Rect;
    .param p3, "metaAreaRect"    # Landroid/graphics/Rect;
    .param p4, "scrOffsetX"    # I
    .param p5, "scrOffsetY"    # I
    .param p6, "isDragAndDropMode"    # Z

    .prologue
    .line 2555
    iget-object v9, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2557
    .local v9, "childList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 2560
    .local v12, "objectRect":Landroid/graphics/Rect;
    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v10, v2, -0x1

    .local v10, "i":I
    :goto_0
    if-ltz v10, :cond_4

    .line 2561
    aget-object v3, v9, v10

    .line 2562
    .local v3, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v3, :cond_0

    iget-boolean v2, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v2, :cond_0

    iget v2, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v4, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-eq v2, v4, :cond_1

    .line 2560
    :cond_0
    :goto_1
    add-int/lit8 v10, v10, -0x1

    goto :goto_0

    .line 2564
    :cond_1
    invoke-virtual {v3, v12}, Lcom/sec/android/gallery3d/glcore/GlObject;->getGlObjectRect(Landroid/graphics/Rect;)V

    .line 2565
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {v12, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 2566
    iget v2, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v2, :cond_2

    move-object v2, p0

    move-object v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    .line 2567
    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/gallery3d/glcore/GlRootView;->extractSmartClipDataInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)Landroid/graphics/Rect;

    move-result-object p3

    .line 2570
    :cond_2
    invoke-virtual {v12}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-lez v2, :cond_0

    invoke-static {v12, p2}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2571
    if-eqz p3, :cond_3

    .line 2572
    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2575
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 2577
    :cond_3
    new-instance p3, Landroid/graphics/Rect;

    .end local p3    # "metaAreaRect":Landroid/graphics/Rect;
    move-object/from16 v0, p3

    invoke-direct {v0, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 2578
    .restart local p3    # "metaAreaRect":Landroid/graphics/Rect;
    if-eqz p6, :cond_0

    move-object/from16 v11, p3

    .line 2583
    .end local v3    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local p3    # "metaAreaRect":Landroid/graphics/Rect;
    .local v11, "metaAreaRect":Ljava/lang/Object;
    :goto_2
    return-object v11

    .end local v11    # "metaAreaRect":Ljava/lang/Object;
    .restart local p3    # "metaAreaRect":Landroid/graphics/Rect;
    :cond_4
    move-object/from16 v11, p3

    .restart local v11    # "metaAreaRect":Ljava/lang/Object;
    goto :goto_2
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 1073
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unfreeze()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->finalize()V

    .line 1077
    return-void

    .line 1075
    :catchall_0
    move-exception v0

    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->finalize()V

    throw v0
.end method

.method public freeze()V
    .locals 2

    .prologue
    .line 983
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 984
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFreeze:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 986
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 988
    return-void

    .line 986
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 3

    .prologue
    .line 286
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    if-nez v0, :cond_0

    .line 288
    new-instance v0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    .line 291
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    goto :goto_0
.end method

.method public getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;
    .locals 1

    .prologue
    .line 2500
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAirButton:Lcom/samsung/android/airbutton/AirButtonImpl;

    return-object v0
.end method

.method public getAirButtonViewer()Lcom/sec/samsung/gallery/view/common/AirButtonViewer;
    .locals 1

    .prologue
    .line 2492
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAirButtonViewer:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    return-object v0
.end method

.method public getAllTextures()Ljava/util/WeakHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/sec/android/gallery3d/glrenderer/BasicTexture;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAllTextures:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method public getCompensation()I
    .locals 1

    .prologue
    .line 972
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensation:I

    return v0
.end method

.method public getCompensationMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 977
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCompensationMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getContentPane()Lcom/sec/android/gallery3d/ui/GLView;
    .locals 1

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    return-object v0
.end method

.method public getDisplayMetrics()Landroid/util/DisplayMetrics;
    .locals 2

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    if-nez v0, :cond_0

    .line 1312
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 1313
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1316
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method public getDisplayRotation()I
    .locals 1

    .prologue
    .line 967
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDisplayRotation:I

    return v0
.end method

.method public getGLContentView()Lcom/sec/android/gallery3d/ui/GLView;
    .locals 1

    .prologue
    .line 2285
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    return-object v0
.end method

.method public getGLContext()Ljavax/microedition/khronos/opengles/GL11;
    .locals 1

    .prologue
    .line 2462
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    return-object v0
.end method

.method public getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;
    .locals 1

    .prologue
    .line 2466
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    return-object v0
.end method

.method public getGlConfig()Lcom/sec/android/gallery3d/glcore/GlConfig;
    .locals 1

    .prologue
    .line 2470
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    return-object v0
.end method

.method public getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;
    .locals 1

    .prologue
    .line 2281
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    return-object v0
.end method

.method public getShowAirButton()Z
    .locals 1

    .prologue
    .line 2509
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mShowAirButton:Z

    return v0
.end method

.method public getToolType()I
    .locals 1

    .prologue
    .line 2517
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mToolType:I

    return v0
.end method

.method public hasCommand(Ljava/lang/Object;I)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "cmdMsg"    # I

    .prologue
    .line 1974
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v3

    .line 1975
    :try_start_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 1976
    .local v1, "index":I
    :cond_0
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-eq v1, v2, :cond_3

    .line 1977
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    aget-object v0, v2, v1

    .line 1978
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    if-nez v0, :cond_2

    .line 1982
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 1983
    const/16 v2, 0x400

    if-ne v1, v2, :cond_0

    .line 1984
    const/4 v1, 0x0

    goto :goto_0

    .line 1979
    :cond_2
    iget-object v2, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    if-ne p1, v2, :cond_1

    iget v2, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    if-ne p2, v2, :cond_1

    .line 1980
    const/4 v2, 0x1

    monitor-exit v3

    .line 1987
    .end local v0    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    :goto_1
    return v2

    .line 1986
    :cond_3
    monitor-exit v3

    .line 1987
    const/4 v2, 0x0

    goto :goto_1

    .line 1986
    .end local v1    # "index":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public hasStencil()Z
    .locals 1

    .prologue
    .line 1090
    const/4 v0, 0x0

    return v0
.end method

.method public isBackgroundTransitionEnabled()Z
    .locals 1

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlBackground;->isTransitionEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSlideShow()Z
    .locals 2

    .prologue
    .line 2747
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2748
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->isSlideShowMode:Z

    .line 2753
    :goto_0
    return v0

    .line 2750
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    if-eqz v0, :cond_1

    .line 2751
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->isSlideShowMode()Z

    move-result v0

    goto :goto_0

    .line 2753
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lockRefresh()V
    .locals 1

    .prologue
    .line 1877
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRefreshEnabled:Z

    .line 1878
    return-void
.end method

.method public lockRenderThread()V
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 930
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 1066
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unfreeze()V

    .line 1067
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onDetachedFromWindow()V

    .line 1068
    return-void
.end method

.method public onDrag(Landroid/view/DragEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v1, 0x1

    .line 1428
    const/4 v0, 0x0

    .line 1430
    .local v0, "retCode":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1432
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v2, :cond_0

    .line 1433
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchDragEvent(Landroid/view/DragEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1440
    .end local v0    # "retCode":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1442
    :goto_0
    return v0

    .line 1434
    .restart local v0    # "retCode":Z
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {p0, v2, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onDragInter(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/view/DragEvent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 1440
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v1

    goto :goto_0

    .line 1436
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchDragEvent(Landroid/view/DragEvent;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 1440
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 545
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->update()V

    .line 551
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 553
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFreeze:Z

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFreezeCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->awaitUninterruptibly()V

    goto :goto_0

    .line 558
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onDrawFrameLocked(Ljavax/microedition/khronos/opengles/GL10;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 560
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 578
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isRenderRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 582
    :cond_1
    return-void

    .line 560
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2649
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Lcom/sec/android/gallery3d/glcore/GlObject;>;"
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2652
    .local v1, "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v1, :cond_1

    .line 2653
    const-string v2, "GlCanvas"

    const-string v3, "onFutureDone null~~~~~"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2665
    :cond_0
    :goto_0
    return-void

    .line 2656
    :cond_1
    iget-object v0, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 2658
    .local v0, "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    iget v2, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    sget v3, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_RUNNING:I

    if-ne v2, v3, :cond_0

    .line 2661
    sget v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_PROCESSED:I

    iput v2, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 2662
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_0

    .line 2663
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v6, 0x118

    const/16 v5, 0x117

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1446
    const/4 v0, 0x0

    .line 1448
    .local v0, "retCode":Z
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->needLockForKeyEvent(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1449
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1451
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_8

    .line 1452
    const/16 v3, 0x50

    if-ne p1, v3, :cond_2

    .line 1486
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->needLockForKeyEvent(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1487
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_1
    :goto_0
    return v1

    .line 1455
    :cond_2
    const/16 v3, 0x1b

    if-ne p1, v3, :cond_4

    .line 1486
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->needLockForKeyEvent(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1487
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_3
    move v1, v2

    goto :goto_0

    .line 1458
    :cond_4
    if-eq p1, v6, :cond_5

    if-ne p1, v5, :cond_6

    :cond_5
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-nez v3, :cond_6

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v3, v4, :cond_6

    .line 1486
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->needLockForKeyEvent(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1487
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1462
    :cond_6
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_7

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_7

    if-eq p1, v6, :cond_7

    if-ne p1, v5, :cond_8

    :cond_7
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMiniModeServiceRunning(Landroid/content/Context;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-eqz v3, :cond_8

    .line 1486
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->needLockForKeyEvent(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1487
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1470
    :cond_8
    :try_start_3
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v1, :cond_9

    .line 1471
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    if-eqz v1, :cond_9

    .line 1472
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->clearAccessibilityFocus()V

    .line 1475
    :cond_9
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v1, :cond_a

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    if-ne v1, v2, :cond_a

    .line 1476
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1478
    :cond_a
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_b

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    if-ne v1, v4, :cond_b

    .line 1479
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1481
    :cond_b
    if-nez v0, :cond_c

    .line 1482
    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    .line 1486
    :cond_c
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->needLockForKeyEvent(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1487
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_d
    move v1, v0

    goto/16 :goto_0

    .line 1486
    :catchall_0
    move-exception v1

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->needLockForKeyEvent(I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1487
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_e
    throw v1
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1514
    const/4 v0, 0x0

    .line 1515
    .local v0, "retCode":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1517
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1518
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1520
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1521
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1523
    :cond_1
    if-nez v0, :cond_2

    .line 1524
    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1527
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1492
    const/4 v0, 0x0

    .line 1493
    .local v0, "retCode":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1495
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1496
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1498
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1499
    const/16 v1, 0x118

    if-eq p1, v1, :cond_1

    const/16 v1, 0x117

    if-ne p1, v1, :cond_4

    .line 1500
    :cond_1
    const/4 v0, 0x0

    .line 1505
    :cond_2
    :goto_0
    if-nez v0, :cond_3

    .line 1506
    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1509
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    .line 1502
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchKeyUp(ILandroid/view/KeyEvent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1509
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 453
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestLayoutContentPane()V

    .line 454
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 945
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unfreeze()V

    .line 946
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 947
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause mGlActive = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    if-nez v0, :cond_0

    .line 958
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 939
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onResume()V

    .line 940
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume mGlActive = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 7
    .param p1, "gl1"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 495
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSurfaceChanged: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", gl10: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUsePostOnAnimation:Z

    .line 499
    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->initializeGL(Z)V

    .line 504
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    if-eq v0, p2, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    if-ne v0, p3, :cond_1

    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    if-ne v0, p2, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    if-ne v0, p3, :cond_2

    .line 505
    :cond_1
    const/4 v5, 0x0

    move-object v0, p0

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->sendCommand(IIIILjava/lang/Object;)V

    .line 506
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    .line 507
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    .line 510
    :cond_2
    const/4 v0, -0x4

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 511
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setRenderThread()V

    .line 512
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mThread:Ljava/lang/Thread;

    move-object v6, p1

    .line 518
    check-cast v6, Ljavax/microedition/khronos/opengles/GL11;

    .line 519
    .local v6, "gl":Ljavax/microedition/khronos/opengles/GL11;
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    if-ne v0, v6, :cond_4

    :goto_0
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    if-eqz v0, :cond_3

    .line 522
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    invoke-interface {v0, p2, p3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setSize(II)V

    .line 525
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v4, v4, p2, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 526
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mClipRetryCount:I

    .line 528
    return-void

    :cond_4
    move v1, v4

    .line 519
    goto :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 4
    .param p1, "gl1"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 463
    sget-object v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSurfaceCreated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p1

    .line 465
    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 466
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    if-eqz v1, :cond_0

    .line 468
    sget-object v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLObject has changed from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 472
    :try_start_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 473
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;

    invoke-direct {v1, v0, p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;-><init>(Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 474
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->invalidateAllTextures()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 479
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->initializeGL(Z)V

    .line 484
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setRenderMode(I)V

    .line 486
    return-void

    .line 476
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public procSurfaceChanged(II)V
    .locals 5
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2027
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    if-nez v0, :cond_1

    .line 2028
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string v1, "mGL is null. Do not procSurfaceChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2047
    :cond_0
    :goto_0
    return-void

    .line 2032
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "procSurfaceChanged = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2033
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    .line 2034
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    .line 2035
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v0, v3, v3, p1, p2}, Ljavax/microedition/khronos/opengles/GL11;->glViewport(IIII)V

    .line 2036
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlConfig;->setSize(FF)V

    .line 2037
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v0, :cond_2

    .line 2038
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    .line 2040
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mState:I

    if-ne v0, v4, :cond_0

    .line 2041
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isWideMode()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchSurfaceChanged(IIZZ)V

    .line 2042
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v0, :cond_0

    .line 2043
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    if-eqz v0, :cond_0

    .line 2044
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAccessibliityNodeProvider:Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->refreshAccessibilityFocus()V

    goto :goto_0
.end method

.method public registerLaunchedAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/anim/CanvasAnimation;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    return-void
.end method

.method public removeAllAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 4
    .param p1, "glObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 1819
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    iget v2, v3, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mMaxUsed:I

    .line 1820
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 1821
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->mAnimationSet:[Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    aget-object v0, v3, v1

    .line 1822
    .local v0, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    if-nez v0, :cond_1

    .line 1820
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1824
    :cond_1
    iget-object v3, v0, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-ne v3, p1, :cond_0

    .line 1825
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_1

    .line 1828
    .end local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    :cond_2
    return-void
.end method

.method public removeAllCommand()V
    .locals 5

    .prologue
    .line 1957
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v3

    .line 1958
    :try_start_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 1959
    .local v1, "index":I
    :cond_0
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-eq v1, v2, :cond_2

    .line 1961
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    aget-object v0, v2, v1

    .line 1962
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    if-eqz v0, :cond_1

    .line 1963
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    .line 1964
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    const/4 v4, 0x0

    aput-object v4, v2, v1

    .line 1966
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 1967
    const/16 v2, 0x400

    if-ne v1, v2, :cond_0

    .line 1968
    const/4 v1, 0x0

    goto :goto_0

    .line 1970
    .end local v0    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    :cond_2
    monitor-exit v3

    .line 1971
    return-void

    .line 1970
    .end local v1    # "index":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public removeAllObject()V
    .locals 1

    .prologue
    .line 1770
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObject(Z)V

    .line 1771
    return-void
.end method

.method public removeAllObject(Z)V
    .locals 4
    .param p1, "useNoDetach"    # Z

    .prologue
    .line 1774
    if-nez p1, :cond_2

    .line 1775
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {p0, v3, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObjectInter(Lcom/sec/android/gallery3d/glcore/GlObject;Z)V

    .line 1787
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-nez v3, :cond_1

    .line 1788
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLUtil:Lcom/sec/android/gallery3d/glcore/TUtils;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/TUtils;->clearTexture()V

    .line 1790
    :cond_1
    return-void

    .line 1777
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1780
    .local v0, "childList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 1781
    aget-object v1, v0, v2

    .line 1782
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLObjectNoDetach:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1780
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1784
    :cond_3
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObjectInter(Lcom/sec/android/gallery3d/glcore/GlObject;Z)V

    goto :goto_1
.end method

.method public removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/anim/Animation;

    .prologue
    .line 1094
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRunningAnimations:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1095
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRunningAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1096
    monitor-exit v1

    .line 1097
    return-void

    .line 1096
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 1842
    if-nez p1, :cond_0

    .line 1849
    :goto_0
    return-void

    .line 1845
    :cond_0
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mAnimState:I

    if-eqz v0, :cond_1

    .line 1846
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 1848
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->remove(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public removeCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V
    .locals 5
    .param p1, "remCmd"    # Lcom/sec/android/gallery3d/glcore/GlCmd;

    .prologue
    .line 1991
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v3

    .line 1992
    :try_start_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 1993
    .local v1, "index":I
    :cond_0
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-eq v1, v2, :cond_3

    .line 1994
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    aget-object v0, v2, v1

    .line 1995
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    if-nez v0, :cond_2

    .line 2001
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 2002
    const/16 v2, 0x400

    if-ne v1, v2, :cond_0

    .line 2003
    const/4 v1, 0x0

    goto :goto_0

    .line 1997
    :cond_2
    iget-object v2, p1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    iget-object v4, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    if-ne v2, v4, :cond_1

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmd:I

    if-ne v2, v4, :cond_1

    .line 1998
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    const/4 v4, 0x0

    aput-object v4, v2, v1

    .line 1999
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    goto :goto_1

    .line 2005
    .end local v0    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    .end local v1    # "index":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "index":I
    :cond_3
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2006
    return-void
.end method

.method public removeCommand(Ljava/lang/Object;)V
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 2009
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v3

    .line 2010
    :try_start_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 2011
    .local v1, "index":I
    :cond_0
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-eq v1, v2, :cond_3

    .line 2012
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    aget-object v0, v2, v1

    .line 2013
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    if-nez v0, :cond_2

    .line 2019
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 2020
    const/16 v2, 0x400

    if-ne v1, v2, :cond_0

    .line 2021
    const/4 v1, 0x0

    goto :goto_0

    .line 2015
    :cond_2
    iget-object v2, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    if-ne p1, v2, :cond_1

    .line 2016
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    const/4 v4, 0x0

    aput-object v4, v2, v1

    .line 2017
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mValid:Z

    goto :goto_1

    .line 2023
    .end local v0    # "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    .end local v1    # "index":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "index":I
    :cond_3
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2024
    return-void
.end method

.method public removeForegroundDrawer(Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;)V
    .locals 1
    .param p1, "drawer"    # Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;

    .prologue
    .line 1717
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFgndDraw:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1718
    return-void
.end method

.method public removeObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 10
    .param p1, "glObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 1739
    if-eqz p1, :cond_0

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v3, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-eq v2, v3, :cond_1

    .line 1767
    :cond_0
    :goto_0
    return-void

    .line 1742
    :cond_1
    sget v2, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYING:I

    iput v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 1744
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    if-ne v2, p1, :cond_2

    .line 1745
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1746
    .local v0, "now":J
    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-wide v2, v0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 1748
    .local v8, "cancelEvent":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v8}, Lcom/sec/android/gallery3d/glcore/GlObject;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1749
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 1750
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1757
    .end local v0    # "now":J
    .end local v8    # "cancelEvent":Landroid/view/MotionEvent;
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAllListeners()V

    .line 1758
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onDestroy()V

    .line 1759
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 1760
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Ljava/lang/Object;)V

    .line 1761
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 1762
    sget v2, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYED:I

    iput v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 1763
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeChildFromRootObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 1764
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_0

    .line 1765
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1752
    :catch_0
    move-exception v9

    .line 1753
    .local v9, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 1754
    .end local v9    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v9

    .line 1755
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public removeOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;

    .prologue
    .line 1707
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 1708
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    .line 1709
    monitor-exit v1

    .line 1710
    return-void

    .line 1709
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 1
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 2274
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2278
    :cond_0
    :goto_0
    return-void

    .line 2277
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRotationListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public replaceLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 13
    .param p1, "newLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v12, 0x0

    .line 2332
    const/4 v9, 0x0

    .line 2333
    .local v9, "layerAnimation":Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;
    const/4 v10, 0x0

    .line 2335
    .local v10, "oldLayer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enter replaceLayer = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", new = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2336
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v2, :cond_1

    .line 2337
    iget-object v10, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2338
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2339
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    if-ne v2, v10, :cond_0

    .line 2340
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mMotionTarget:Ljava/lang/Object;

    .line 2341
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2342
    .local v0, "now":J
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2344
    .local v8, "cancelEvent":Landroid/view/MotionEvent;
    invoke-virtual {v10, v8}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onTouch(Landroid/view/MotionEvent;)Z

    .line 2345
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 2347
    .end local v0    # "now":J
    .end local v8    # "cancelEvent":Landroid/view/MotionEvent;
    :cond_0
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/glcore/GlLayer;->pause()V

    .line 2348
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/glcore/GlLayer;->destroy()V

    .line 2349
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLObjectNoDetach:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v11, 0x1

    .line 2350
    .local v11, "useNoDetach":Z
    :goto_0
    invoke-virtual {p0, v11}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObject(Z)V

    .line 2351
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->clearAllAnimation()V

    .line 2352
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllCommand()V

    .line 2353
    iget-object v9, v10, Lcom/sec/android/gallery3d/glcore/GlLayer;->mPendingLayerAnimation:Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;

    .line 2354
    iput-object v12, v10, Lcom/sec/android/gallery3d/glcore/GlLayer;->mPendingLayerAnimation:Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;

    .line 2355
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGLObjectNoDetach:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2356
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 2357
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectHoveringAppeared:Z

    .line 2360
    .end local v11    # "useNoDetach":Z
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2361
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v2, :cond_4

    .line 2362
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    if-eqz v2, :cond_2

    .line 2363
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->yieldAllTextures()V

    .line 2364
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->deleteRecycledResources()V

    .line 2365
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 2366
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAllTextures:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->clear()V

    .line 2367
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->reset()V

    .line 2369
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v2, :cond_3

    .line 2370
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/GLView;->detachFromRoot()V

    .line 2371
    iput-object v12, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    .line 2373
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isWideMode()Z

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchSurfaceChanged(IIZZ)V

    .line 2374
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->create()V

    .line 2375
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->resume()V

    .line 2376
    if-eqz v9, :cond_4

    .line 2377
    iput-object v10, v9, Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;->mOldLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2378
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iput-object v2, v9, Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;->mNewLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 2379
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;->onPrepared()V

    .line 2382
    :cond_4
    if-eqz v10, :cond_5

    .line 2383
    invoke-virtual {v10, v12}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setRootView(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    .line 2385
    :cond_5
    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string v3, "exit replaceLayer"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2386
    return-void

    :cond_6
    move v11, v7

    .line 2349
    goto :goto_0
.end method

.method public requestLayout()V
    .locals 0

    .prologue
    .line 1917
    return-void
.end method

.method public requestLayoutContentPane()V
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 402
    :goto_0
    return-void

    .line 395
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2

    .line 400
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 397
    :cond_2
    :try_start_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFlags:I

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 400
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 366
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-eqz v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 368
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    .line 369
    sget-boolean v0, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_POST_ON_ANIMATION:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUsePostOnAnimation:Z

    if-eqz v0, :cond_1

    .line 370
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRequestRenderOnAnimationFrame:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->postOnAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 372
    :cond_1
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    goto :goto_0
.end method

.method public requestRenderForced()V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->superRequestRender()V

    .line 357
    return-void
.end method

.method public requestRenderObjectInThreadPool(Lcom/sec/android/gallery3d/glcore/GlObject;)Lcom/sec/android/gallery3d/util/Future;
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ")",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2643
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlRenderRequest;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    return-object v0
.end method

.method public restoreStatus()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1903
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1904
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    .line 1905
    .local v0, "obj":Ljava/lang/Object;
    monitor-exit v2

    .line 1906
    return-object v0

    .line 1905
    .end local v0    # "obj":Ljava/lang/Object;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public saveStatus(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1892
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1893
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    .line 1894
    monitor-exit v1

    .line 1898
    :goto_0
    return-void

    .line 1894
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1896
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mStatusQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public sendCommand(IIIILjava/lang/Object;)V
    .locals 6
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1949
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;)V

    .line 1950
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_SYS:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 1951
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 1952
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 1953
    return-void
.end method

.method public setAirButtonImpl(Lcom/samsung/android/airbutton/AirButtonImpl;)V
    .locals 0
    .param p1, "airButton"    # Lcom/samsung/android/airbutton/AirButtonImpl;

    .prologue
    .line 2496
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAirButton:Lcom/samsung/android/airbutton/AirButtonImpl;

    .line 2497
    return-void
.end method

.method public setAirButtonViewer(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)V
    .locals 0
    .param p1, "airButtonViewer"    # Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    .prologue
    .line 2488
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAirButtonViewer:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    .line 2489
    return-void
.end method

.method public setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 1852
    if-nez p1, :cond_0

    .line 1857
    :goto_0
    return-void

    .line 1855
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimationMgr:Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$AnimationManager;->add(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1856
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    goto :goto_0
.end method

.method public setBgResource(I)V
    .locals 1
    .param p1, "rsrcID"    # I

    .prologue
    .line 1930
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-nez v0, :cond_0

    .line 1932
    :goto_0
    return-void

    .line 1931
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setFirstImage(I)V

    goto :goto_0
.end method

.method public setClearByColor(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1935
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-nez v0, :cond_0

    .line 1937
    :goto_0
    return-void

    .line 1936
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setClearByColor(Z)V

    goto :goto_0
.end method

.method public setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V
    .locals 5
    .param p1, "cmd"    # Lcom/sec/android/gallery3d/glcore/GlCmd;

    .prologue
    const/16 v4, 0x400

    .line 1860
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    monitor-enter v1

    .line 1861
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueue:[Lcom/sec/android/gallery3d/glcore/GlCmd;

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    aput-object p1, v0, v2

    .line 1862
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    if-ne v0, v4, :cond_0

    .line 1863
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    .line 1864
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueWR:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    if-ne v0, v2, :cond_1

    .line 1866
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 1867
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    if-ne v0, v4, :cond_1

    .line 1868
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCmdQueueRD:I

    .line 1870
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1871
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_2

    .line 1872
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 1874
    :cond_2
    return-void

    .line 1870
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V
    .locals 9
    .param p1, "content"    # Lcom/sec/android/gallery3d/ui/GLView;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 314
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-ne v2, p1, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v2, :cond_3

    .line 316
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInDownState:Z

    if-eqz v2, :cond_4

    .line 317
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 318
    .local v0, "now":J
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 320
    .local v8, "cancelEvent":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v2, v8}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 321
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 322
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mInDownState:Z

    .line 333
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 334
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRunningAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 336
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/GLView;->detachFromRoot()V

    .line 338
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    if-eqz v2, :cond_2

    .line 339
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->yieldAllTextures()V

    .line 340
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mAllTextures:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->clear()V

    .line 341
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->clear()V

    .line 344
    .end local v0    # "now":J
    .end local v8    # "cancelEvent":Landroid/view/MotionEvent;
    :cond_3
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    .line 345
    if-eqz p1, :cond_0

    .line 347
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayerNew:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 349
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/ui/GLView;->attachToRoot(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestLayoutContentPane()V

    goto :goto_0

    .line 325
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 326
    .restart local v0    # "now":J
    const/16 v4, 0xa

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 328
    .restart local v8    # "cancelEvent":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mContentView:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v2, v8}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    .line 329
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_1
.end method

.method public setDecoderInterface(Lcom/sec/samsung/gallery/decoder/DecoderInterface;)V
    .locals 0
    .param p1, "decoderInterface"    # Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 242
    return-void
.end method

.method public setGlBackgroundColor(FFFF)V
    .locals 0
    .param p1, "red"    # F
    .param p2, "green"    # F
    .param p3, "blue"    # F
    .param p4, "alpha"    # F

    .prologue
    .line 2772
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorAlpha:F

    .line 2773
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorRed:F

    .line 2774
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorGreen:F

    .line 2775
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBgColorBlue:F

    .line 2776
    return-void
.end method

.method public setLightsOutMode(Z)V
    .locals 2
    .param p1, "enabled"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1004
    sget-boolean v1, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_SET_SYSTEM_UI_VISIBILITY:Z

    if-nez v1, :cond_0

    .line 1013
    :goto_0
    return-void

    .line 1006
    :cond_0
    const/4 v0, 0x0

    .line 1007
    .local v0, "flags":I
    if-eqz p1, :cond_1

    .line 1008
    sget-boolean v1, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_VIEW_SYSTEM_UI_FLAG_LAYOUT_STABLE:Z

    if-eqz v1, :cond_1

    .line 1009
    or-int/lit16 v0, v0, 0x100

    .line 1012
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public setNextImage(I)V
    .locals 1
    .param p1, "rsrcID"    # I

    .prologue
    .line 1920
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-nez v0, :cond_0

    .line 1922
    :goto_0
    return-void

    .line 1921
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setNextImage(I)V

    goto :goto_0
.end method

.method public setNextImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1925
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    if-nez v0, :cond_0

    .line 1927
    :goto_0
    return-void

    .line 1926
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mBackground:Lcom/sec/android/gallery3d/glcore/GlBackground;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlBackground;->setNextImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setOrientationSource(Lcom/sec/android/gallery3d/ui/OrientationSource;)V
    .locals 0
    .param p1, "source"    # Lcom/sec/android/gallery3d/ui/OrientationSource;

    .prologue
    .line 962
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mOrientationSource:Lcom/sec/android/gallery3d/ui/OrientationSource;

    .line 963
    return-void
.end method

.method public setPadding(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 2389
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mPadLeft:I

    .line 2390
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mPadRight:I

    .line 2391
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mPadTop:I

    .line 2392
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mPadBottom:I

    .line 2393
    return-void
.end method

.method public setPostOnAnimation(Z)V
    .locals 0
    .param p1, "usePostOnAnimation"    # Z

    .prologue
    .line 1081
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUsePostOnAnimation:Z

    .line 1082
    return-void
.end method

.method public setShowAirButton(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 2504
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AirButton - setShowAirButton : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2505
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mShowAirButton:Z

    .line 2506
    return-void
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 2759
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLogic:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 2760
    :cond_0
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->setVisibility(I)V

    .line 2762
    :cond_1
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 1021
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unfreeze()V

    .line 1022
    invoke-super {p0, p1, p2, p3, p4}, Landroid/opengl/GLSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 1024
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "surfaceChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1030
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unfreeze()V

    .line 1031
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 1033
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->registerForStylusPenEvent()V

    .line 1036
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUploadBmCache:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 1037
    invoke-static {}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->createUploadBitmapCache()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUploadBmCache:Ljava/lang/Object;

    .line 1040
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v2, 0x0

    .line 1044
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unfreeze()V

    .line 1045
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 1047
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    if-nez v0, :cond_0

    .line 1062
    :goto_0
    return-void

    .line 1051
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->replaceLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 1052
    invoke-static {}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->releaseInstance()V

    .line 1053
    invoke-static {}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->releaseInstance()V

    .line 1054
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUploadBmCache:Ljava/lang/Object;

    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->destroyUploadBitmapCache(Ljava/lang/Object;)V

    .line 1055
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeForStylusPenEvent()V

    .line 1056
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 1057
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->finalizeGL()V

    .line 1058
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->clearCommandQueue()V

    .line 1059
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUploadBmCache:Ljava/lang/Object;

    .line 1060
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mGlActive:Z

    goto :goto_0
.end method

.method public unfreeze()V
    .locals 2

    .prologue
    .line 993
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 994
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFreeze:Z

    .line 995
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mFreezeCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 997
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 999
    return-void

    .line 997
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public unlockRefresh()V
    .locals 1

    .prologue
    .line 1881
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRefreshEnabled:Z

    .line 1882
    return-void
.end method

.method public unlockRenderThread()V
    .locals 1

    .prologue
    .line 934
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 935
    return-void
.end method
