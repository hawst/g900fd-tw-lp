.class public Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;
.super Ljava/lang/Object;
.source "AlbumSetDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateContent;,
        Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;,
        Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;,
        Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$MySourceListener;,
        Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;
    }
.end annotation


# static fields
.field private static final INDEX_NONE:I = -0x1

.field private static final MIN_LOAD_COUNT:I = 0x4

.field private static final MSG_RUN_OBJECT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AlbumSetDataAdapter"


# instance fields
.field private mActiveEnd:I

.field private mActiveStart:I

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mContentEnd:I

.field private mContentStart:I

.field private final mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

.field private final mData:[Lcom/sec/android/gallery3d/data/MediaSet;

.field private mDataListener:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;

.field private final mItemVersion:[J

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field private final mMainHandler:Landroid/os/Handler;

.field private mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

.field private final mSetVersion:[J

.field private mSize:I

.field private final mSource:Lcom/sec/android/gallery3d/data/MediaSet;

.field private final mSourceListener:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$MySourceListener;

.field private mSourceVersion:J

.field private final mTotalCount:[I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 4
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "albumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "cacheSize"    # I

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    .line 58
    iput v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    .line 60
    iput v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    .line 61
    iput v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    .line 64
    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSourceVersion:J

    .line 73
    new-instance v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$MySourceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$MySourceListener;-><init>(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSourceListener:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$MySourceListener;

    .line 76
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 77
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 78
    new-array v0, p3, [Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    .line 79
    new-array v0, p3, [Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaSet;

    .line 80
    new-array v0, p3, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mTotalCount:[I

    .line 81
    new-array v0, p3, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mItemVersion:[J

    .line 82
    new-array v0, p3, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSetVersion:[J

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mItemVersion:[J

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSetVersion:[J

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 86
    new-instance v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$1;-><init>(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mMainHandler:Landroid/os/Handler;

    .line 96
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->loadData()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;
    .param p1, "x1"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mItemVersion:[J

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)[Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mTotalCount:[I

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)Lcom/sec/samsung/gallery/view/adapter/ReloadTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSetVersion:[J

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;
    .param p1, "x1"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSourceVersion:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;
    .param p1, "x1"    # J

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSourceVersion:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSize:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;
    .param p1, "x1"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSize:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;

    return-object v0
.end method

.method private assertIsActive(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    if-lt p1, v0, :cond_0

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "%s not in (%s, %s)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_0
    return-void
.end method

.method private clearSlot(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v1, v0, p1

    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v1, v0, p1

    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mTotalCount:[I

    const/4 v1, 0x0

    aput v1, v0, p1

    .line 170
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mItemVersion:[J

    aput-wide v2, v0, p1

    .line 171
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSetVersion:[J

    aput-wide v2, v0, p1

    .line 172
    return-void
.end method

.method private executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 317
    .local v1, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<TT;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mMainHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 320
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 322
    :goto_0
    return-object v2

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/InterruptedException;
    const/4 v2, 0x0

    goto :goto_0

    .line 323
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 324
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private loadData()Z
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 330
    const/4 v1, 0x0

    .line 331
    .local v1, "updateComplete":Z
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v4

    .line 333
    .local v4, "version":J
    new-instance v3, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$GetUpdateInfo;-><init>(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;J)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;

    .line 334
    .local v0, "info":Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;
    if-nez v0, :cond_0

    .line 335
    const/4 v1, 0x1

    move v2, v1

    .line 361
    .end local v1    # "updateComplete":Z
    .local v2, "updateComplete":I
    :goto_0
    return v2

    .line 339
    .end local v2    # "updateComplete":I
    .restart local v1    # "updateComplete":Z
    :cond_0
    iget-wide v6, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->version:J

    cmp-long v3, v6, v4

    if-eqz v3, :cond_1

    .line 340
    iput-wide v4, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->version:J

    .line 341
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v3

    iput v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->size:I

    .line 347
    iget v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->index:I

    iget v6, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->size:I

    if-lt v3, v6, :cond_1

    .line 348
    iput v8, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->index:I

    .line 351
    :cond_1
    iget v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->index:I

    if-eq v3, v8, :cond_3

    .line 352
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v6, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->index:I

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->item:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 353
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->item:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v3, :cond_2

    move v2, v1

    .line 354
    .restart local v2    # "updateComplete":I
    goto :goto_0

    .line 356
    .end local v2    # "updateComplete":I
    :cond_2
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->item:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->cover:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 357
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->item:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    iput v3, v0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;->totalCount:I

    .line 359
    :cond_3
    new-instance v3, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateContent;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateContent;-><init>(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$UpdateInfo;)V

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move v2, v1

    .line 361
    .restart local v2    # "updateComplete":I
    goto :goto_0
.end method

.method private setContentWindow(II)V
    .locals 6
    .param p1, "contentStart"    # I
    .param p2, "contentEnd"    # I

    .prologue
    .line 175
    iget v5, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    if-ne p1, v5, :cond_0

    iget v5, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    if-ne p2, v5, :cond_0

    .line 197
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v2, v5

    .line 178
    .local v2, "length":I
    iget v4, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    .line 179
    .local v4, "start":I
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    .line 181
    .local v0, "end":I
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    .line 182
    iput p2, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    .line 184
    if-ge p1, v0, :cond_1

    if-lt v4, p2, :cond_2

    .line 185
    :cond_1
    move v1, v4

    .local v1, "i":I
    move v3, v0

    .local v3, "n":I
    :goto_1
    if-ge v1, v3, :cond_4

    .line 186
    rem-int v5, v1, v2

    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->clearSlot(I)V

    .line 185
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 189
    .end local v1    # "i":I
    .end local v3    # "n":I
    :cond_2
    move v1, v4

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, p1, :cond_3

    .line 190
    rem-int v5, v1, v2

    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->clearSlot(I)V

    .line 189
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 192
    :cond_3
    move v1, p2

    move v3, v0

    .restart local v3    # "n":I
    :goto_3
    if-ge v1, v3, :cond_4

    .line 193
    rem-int v5, v1, v2

    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->clearSlot(I)V

    .line 192
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 196
    :cond_4
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V

    goto :goto_0
.end method


# virtual methods
.method public findSet(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 5
    .param p1, "id"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 156
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v3

    .line 157
    .local v1, "length":I
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    if-ge v0, v3, :cond_1

    .line 158
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaSet;

    rem-int v4, v0, v1

    aget-object v2, v3, v4

    .line 159
    .local v2, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    if-ne p1, v3, :cond_0

    .line 163
    .end local v0    # "i":I
    .end local v2    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_1
    return v0

    .line 157
    .restart local v0    # "i":I
    .restart local v2    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    .end local v2    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getActiveStart()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    return v0
.end method

.method public getCoverItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->assertIsActive(I)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v1, v1

    rem-int v1, p1, v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->assertIsActive(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mData:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v1

    rem-int v1, p1, v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTotalCount(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->assertIsActive(I)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mTotalCount:[I

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mTotalCount:[I

    array-length v1, v1

    rem-int v1, p1, v1

    aget v0, v0, v1

    return v0
.end method

.method public isActive(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 146
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->terminate()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSourceListener:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$MySourceListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 102
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSourceListener:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$MySourceListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 106
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    new-instance v1, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$2;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$2;-><init>(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->setOnLoadDataListener(Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->start()V

    .line 117
    return-void
.end method

.method public setActiveWindow(II)V
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x0

    .line 200
    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    if-ne p1, v3, :cond_1

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    if-ne p2, v3, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    if-gt p1, p2, :cond_3

    sub-int v3, p2, p1

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v5, v5

    if-gt v3, v5, :cond_3

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSize:I

    if-gt p2, v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 205
    iput p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveStart:I

    .line 206
    iput p2, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mActiveEnd:I

    .line 208
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mCoverItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v2, v3

    .line 210
    .local v2, "length":I
    if-eq p1, p2, :cond_0

    .line 212
    add-int v3, p1, p2

    div-int/lit8 v3, v3, 0x2

    div-int/lit8 v5, v2, 0x2

    sub-int/2addr v3, v5

    iget v5, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSize:I

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 214
    .local v1, "contentStart":I
    add-int v3, v1, v2

    iget v4, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSize:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 215
    .local v0, "contentEnd":I
    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    if-gt v3, p1, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentEnd:I

    if-lt v3, p2, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mContentStart:I

    sub-int v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_0

    .line 217
    :cond_2
    invoke-direct {p0, v1, v0}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->setContentWindow(II)V

    goto :goto_0

    .end local v0    # "contentEnd":I
    .end local v1    # "contentStart":I
    .end local v2    # "length":I
    :cond_3
    move v3, v4

    .line 202
    goto :goto_1
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 234
    return-void
.end method

.method public setModelListener(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mDataListener:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;

    .line 230
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->mSize:I

    return v0
.end method
