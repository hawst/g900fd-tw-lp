.class public Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;
.super Ljava/lang/Object;
.source "ClusterAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/ClusterAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UpdateOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final ASSIGN_NAME:I = 0x2

.field public static final CONFIRM:I = 0x0

.field public static final REMOVE:I = 0x1

.field public static final UPDATE:I = 0x3


# instance fields
.field private mAlbumName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mJoinedName:Ljava/lang/String;

.field private final mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mOperationType:I

.field private mPersonId:I

.field private mUpdatePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/ClusterAlbum;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Context;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p2, "albumName"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "operationType"    # I
    .param p6, "joinedName"    # Ljava/lang/String;
    .param p7, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")V"
        }
    .end annotation

    .prologue
    .line 474
    .local p3, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mOperationType:I

    .line 467
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mPersonId:I

    .line 475
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    .line 476
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    .line 477
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    .line 478
    iput p5, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mOperationType:I

    .line 479
    iput-object p6, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mJoinedName:Ljava/lang/String;

    .line 480
    iput-object p7, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 481
    return-void
.end method

.method private assignName()V
    .locals 5

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mJoinedName:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mPersonId:I

    # invokes: Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->access$100(Lcom/sec/android/gallery3d/data/ClusterAlbum;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    .line 561
    return-void
.end method

.method private confirm(Ljava/lang/String;)V
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 542
    const/4 v3, 0x0

    .line 543
    .local v3, "pathS":Ljava/lang/String;
    const/4 v6, 0x0

    .line 544
    .local v6, "values":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 545
    .local v1, "id":I
    const/4 v5, 0x0

    .line 546
    .local v5, "recommendedId":I
    const/4 v4, 0x0

    .line 548
    .local v4, "personId":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/Path;

    .line 549
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    .line 550
    const-string v7, "/"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 551
    const/4 v7, 0x3

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 552
    const/16 v7, 0x9

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 553
    const/16 v7, 0xa

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 554
    if-eq v5, v4, :cond_0

    .line 555
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    invoke-static {v7, v1, v5}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    goto :goto_0

    .line 557
    .end local v2    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    return-void
.end method

.method private remove(Ljava/lang/String;)V
    .locals 10
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 513
    const/4 v4, 0x0

    .line 514
    .local v4, "pathS":Ljava/lang/String;
    const/4 v7, 0x0

    .line 515
    .local v7, "values":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 517
    .local v2, "id":I
    const/4 v5, 0x0

    .line 518
    .local v5, "recommendedId":I
    const/4 v0, 0x0

    .line 519
    .local v0, "bRemoveAll":Z
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->access$000(Lcom/sec/android/gallery3d/data/ClusterAlbum;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 520
    const/4 v0, 0x1

    .line 521
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->access$000(Lcom/sec/android/gallery3d/data/ClusterAlbum;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 522
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 523
    .local v6, "splits":[Ljava/lang/String;
    const/16 v8, 0x9

    aget-object v8, v6, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 526
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v6    # "splits":[Ljava/lang/String;
    :cond_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 527
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->access$000(Lcom/sec/android/gallery3d/data/ClusterAlbum;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 528
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    .line 529
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 530
    const/4 v8, 0x3

    aget-object v8, v7, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 531
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    invoke-static {v8, v2}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknown(Landroid/content/Context;I)V

    goto :goto_0

    .line 534
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    if-eqz v0, :cond_2

    const/4 v8, 0x1

    if-le v5, v8, :cond_2

    .line 535
    const-string v8, "remove"

    const-string v9, "remove clusterAlbum"

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    invoke-static {v8, v5}, Lcom/sec/samsung/gallery/access/face/PersonList;->remove(Landroid/content/Context;I)V

    .line 538
    :cond_2
    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 461
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 485
    const/4 v0, 0x0

    .line 487
    .local v0, "currentAlbum":Ljava/lang/String;
    iget v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mOperationType:I

    packed-switch v1, :pswitch_data_0

    .line 506
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v1, :cond_0

    .line 507
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateMediaSet()V

    .line 509
    :cond_0
    const/4 v1, 0x0

    return-object v1

    .line 489
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->confirm(Ljava/lang/String;)V

    .line 490
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateMediaSet()V

    .line 491
    const-string v1, "reload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CONFIRM s_updateAlbum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 494
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->remove(Ljava/lang/String;)V

    .line 495
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateMediaSet()V

    .line 496
    const-string v1, "reload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REMOVE s_updateAlbum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 499
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mPersonId:I

    .line 500
    const-string v1, "reload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ASSIGN_NAME s_updateAlbum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->mPersonId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/ClusterAlbum$UpdateOperation;->assignName()V

    goto/16 :goto_0

    .line 487
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
