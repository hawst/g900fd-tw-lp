.class public Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;
.super Ljava/lang/Object;
.source "GallerySearchAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/GallerySearchAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UpdateOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final ASSIGN_NAME:I = 0x2

.field public static final CONFIRM:I = 0x0

.field public static final REMOVE:I = 0x1


# instance fields
.field private mAlbumName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mJoinedName:Ljava/lang/String;

.field private mOperationType:I

.field private mPersonId:I

.field private mUpdatePaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .param p2, "albumName"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "operationType"    # I
    .param p6, "joinedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 387
    .local p3, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mOperationType:I

    .line 382
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mPersonId:I

    .line 388
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    .line 389
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    .line 390
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    .line 391
    iput p5, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mOperationType:I

    .line 392
    iput-object p6, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mJoinedName:Ljava/lang/String;

    .line 393
    return-void
.end method

.method private assignName()V
    .locals 5

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mJoinedName:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mPersonId:I

    # invokes: Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->access$300(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    .line 467
    return-void
.end method

.method private confirm(Ljava/lang/String;)V
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 448
    const/4 v3, 0x0

    .line 449
    .local v3, "pathS":Ljava/lang/String;
    const/4 v6, 0x0

    .line 450
    .local v6, "values":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 451
    .local v1, "id":I
    const/4 v5, 0x0

    .line 452
    .local v5, "recommendedId":I
    const/4 v4, 0x0

    .line 454
    .local v4, "personId":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/Path;

    .line 455
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    .line 456
    const-string v7, "/"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 457
    const/4 v7, 0x3

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 458
    const/16 v7, 0x9

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 459
    const/16 v7, 0xa

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 460
    if-eq v5, v4, :cond_0

    .line 461
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    invoke-static {v7, v1, v5}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    goto :goto_0

    .line 463
    .end local v2    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    return-void
.end method

.method private remove(Ljava/lang/String;)V
    .locals 11
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 420
    const/4 v4, 0x0

    .line 421
    .local v4, "pathS":Ljava/lang/String;
    const/4 v7, 0x0

    .line 422
    .local v7, "values":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 423
    .local v2, "id":I
    const/4 v5, 0x0

    .line 424
    .local v5, "recommendedId":I
    const/4 v0, 0x0

    .line 425
    .local v0, "bRemoveAll":Z
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->access$200(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 426
    const/4 v0, 0x1

    .line 427
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->access$200(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;)Ljava/util/ArrayList;

    move-result-object v8

    new-array v9, v10, [Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/sec/android/gallery3d/data/Path;

    aget-object v3, v8, v10

    .line 428
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 429
    .local v6, "splits":[Ljava/lang/String;
    const/16 v8, 0x9

    aget-object v8, v6, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 432
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v6    # "splits":[Ljava/lang/String;
    :cond_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mUpdatePaths:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 433
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->access$200(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 434
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    .line 435
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 436
    const/4 v8, 0x3

    aget-object v8, v7, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 437
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    invoke-static {v8, v2}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknown(Landroid/content/Context;I)V

    goto :goto_0

    .line 440
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    if-eqz v0, :cond_2

    const/4 v8, 0x1

    if-le v5, v8, :cond_2

    .line 441
    const-string v8, "remove"

    const-string v9, "remove clusterAlbum"

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    invoke-static {v8, v5}, Lcom/sec/samsung/gallery/access/face/PersonList;->remove(Landroid/content/Context;I)V

    .line 444
    :cond_2
    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 376
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 397
    const/4 v0, 0x0

    .line 399
    .local v0, "currentAlbum":Ljava/lang/String;
    iget v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mOperationType:I

    packed-switch v1, :pswitch_data_0

    .line 416
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 401
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->confirm(Ljava/lang/String;)V

    .line 402
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateMediaSet()V

    .line 403
    const-string v1, "reload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CONFIRM s_updateAlbum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 406
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->remove(Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->this$0:Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateMediaSet()V

    .line 408
    const-string v1, "reload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REMOVE s_updateAlbum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 411
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mPersonId:I

    .line 412
    const-string v1, "reload"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ASSIGN_NAME s_updateAlbum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->mPersonId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;->assignName()V

    goto :goto_0

    .line 399
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
