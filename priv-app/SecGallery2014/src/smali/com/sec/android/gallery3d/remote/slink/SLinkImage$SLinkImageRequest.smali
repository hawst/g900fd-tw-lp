.class Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;
.super Lcom/sec/android/gallery3d/data/ImageCacheRequest;
.source "SLinkImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SLinkImageRequest"
.end annotation


# static fields
.field private static final SLINK_SCREENAIL_SIZE:I = 0x400

.field private static final SLINK_THUMB_SIZE:I = 0x140


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JII)V
    .locals 10
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p4, "timeModified"    # J
    .param p6, "type"    # I
    .param p7, "targetSize"    # I

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 196
    const/4 v8, 0x1

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JIIZ)V

    .line 199
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->setUseCache(Z)V

    .line 200
    return-void
.end method


# virtual methods
.method public onDecodeOriginal(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "targetSize"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 204
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 205
    const/4 v0, 0x0

    .line 228
    :cond_0
    :goto_0
    return-object v0

    .line 208
    :cond_1
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 209
    .local v10, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 211
    if-ne p2, v11, :cond_2

    const/16 v6, 0x400

    .line 213
    .local v6, "imageSize":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->access$000(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    move v7, v6

    move v9, v8

    invoke-static/range {v1 .. v10}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JJIIZZLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 215
    .local v0, "thumb":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 216
    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->setUseCache(Z)V

    .line 217
    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Get thumbnail return null from SLPF"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->setBroken(Z)V

    .line 219
    if-ne p2, v11, :cond_3

    .line 220
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a7

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    :goto_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;)Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->width:I

    .line 224
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;)Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->height:I

    .line 225
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    # getter for: Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;
    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;)Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    move-result-object v1

    iput v8, v1, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->rotation:I

    goto :goto_0

    .line 211
    .end local v0    # "thumb":Landroid/graphics/Bitmap;
    .end local v6    # "imageSize":I
    :cond_2
    const/16 v6, 0x140

    goto :goto_1

    .line 222
    .restart local v0    # "thumb":Landroid/graphics/Bitmap;
    .restart local v6    # "imageSize":I
    :cond_3
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method
