.class public Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;
.super Landroid/app/Activity;
.source "FilterbySettingActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mIsUpdated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->mIsUpdated:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static getUpdateState()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->mIsUpdated:Z

    return v0
.end method

.method public static resetUpdateState()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->mIsUpdated:Z

    .line 68
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x400

    .line 22
    sget-object v0, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 24
    const v0, 0x7f110039

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->setTheme(I)V

    .line 26
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 29
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 32
    :cond_1
    const v0, 0x7f030062

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->setContentView(I)V

    .line 34
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 43
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 48
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 45
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->finish()V

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->mIsUpdated:Z

    .line 54
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 55
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 61
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    const v1, 0x7f0f0007

    new-instance v2, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 62
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 63
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 64
    return-void
.end method
