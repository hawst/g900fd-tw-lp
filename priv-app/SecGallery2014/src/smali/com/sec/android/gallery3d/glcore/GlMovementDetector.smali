.class public Lcom/sec/android/gallery3d/glcore/GlMovementDetector;
.super Ljava/lang/Object;
.source "GlMovementDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;
    }
.end annotation


# static fields
.field public static final CRIT_DISTANCE:I = 0x2d

.field public static DETECT_CLICK:I = 0x0

.field public static DETECT_LONGCLICK:I = 0x0

.field public static DETECT_MOVE:I = 0x0

.field private static final LONG_PRESSED_EVT:I = 0x1

.field private static final MIN_FLING_VELOCITY:F = 500.0f

.field private static final MOUSE_SCROLL_SPEED:F = 100.0f

.field private static final PAST_COUNT:I = 0x4

.field private static final TAG:Ljava/lang/String; = "GlMovementDetector"

.field public static final THRESHOLD_DISTANCE:I = 0x14

.field private static sInstance:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;


# instance fields
.field private mClickActive:Z

.field private mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

.field private mDifX:F

.field private mDifY:F

.field private mHandled:Z

.field private mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mInThreshold:Z

.field private mLMs:[J

.field private mLX:[I

.field private mLY:[I

.field private mLongPressedOccur:J

.field private mLongTmActive:Z

.field private mLxIndex:I

.field private mMovX:I

.field private mMovY:I

.field private mMovedOverClickLimit:Z

.field private mPressMs:J

.field private mPressX:I

.field private mPressY:I

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mTEvtCount:I

.field private mVelocityX:F

.field private mVelocityY:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->DETECT_MOVE:I

    .line 12
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->DETECT_CLICK:I

    .line 13
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->DETECT_LONGCLICK:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 1
    .param p1, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)V
    .locals 7
    .param p1, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x1f4

    const/4 v1, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-wide v4, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongPressedOccur:J

    .line 26
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 27
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 28
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .line 31
    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    .line 33
    new-array v1, v3, [I

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLX:[I

    .line 34
    new-array v1, v3, [I

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLY:[I

    .line 35
    new-array v1, v3, [J

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLMs:[J

    .line 39
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mInThreshold:Z

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    .line 41
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z

    .line 42
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovedOverClickLimit:Z

    .line 64
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 65
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .line 68
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "long_press_timeout"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongPressedOccur:J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 76
    :goto_0
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;-><init>(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 88
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    iput-wide v4, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongPressedOccur:J

    goto :goto_0

    .line 72
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_1
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/NullPointerException;
    iput-wide v4, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongPressedOccur:J

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .prologue
    .line 8
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlMovementDetector;
    .param p1, "x1"    # Z

    .prologue
    .line 8
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;)Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .prologue
    .line 8
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlMovementDetector;
    .param p1, "x1"    # Z

    .prologue
    .line 8
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z

    return p1
.end method

.method private calcMovement(IIJI)V
    .locals 9
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "eventTime"    # J
    .param p5, "action"    # I

    .prologue
    .line 95
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLX:[I

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    aput p1, v5, v6

    .line 96
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLY:[I

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    aput p2, v5, v6

    .line 97
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLMs:[J

    iget v6, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    aput-wide p3, v5, v6

    .line 98
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    .line 99
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_0

    .line 100
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    .line 101
    :cond_0
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mTEvtCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mTEvtCount:I

    .line 102
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    sub-int v5, p1, v5

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovX:I

    .line 103
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    sub-int v5, p2, v5

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovY:I

    .line 105
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovX:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    const/16 v6, 0x2d

    if-gt v5, v6, :cond_1

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovY:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    const/16 v6, 0x2d

    if-le v5, v6, :cond_2

    .line 106
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovedOverClickLimit:Z

    .line 107
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z

    .line 110
    :cond_2
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    if-eqz v5, :cond_3

    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovedOverClickLimit:Z

    if-eqz v5, :cond_3

    .line 111
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 112
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    .line 115
    :cond_3
    const/4 v5, 0x1

    if-eq p5, v5, :cond_4

    const/4 v5, 0x3

    if-ne p5, v5, :cond_5

    .line 116
    :cond_4
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mTEvtCount:I

    const/4 v6, 0x4

    if-lt v5, v6, :cond_6

    .line 117
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    .line 120
    .local v4, "fIndex":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLX:[I

    aget v5, v5, v4

    sub-int v2, p1, v5

    .line 121
    .local v2, "dx":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLY:[I

    aget v5, v5, v4

    sub-int v3, p2, v5

    .line 122
    .local v3, "dy":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLMs:[J

    aget-wide v6, v5, v4

    sub-long v0, p3, v6

    .line 123
    .local v0, "duration":J
    int-to-float v5, v2

    const/high16 v6, 0x44fa0000    # 2000.0f

    mul-float/2addr v5, v6

    long-to-float v6, v0

    div-float/2addr v5, v6

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityX:F

    .line 124
    int-to-float v5, v3

    const/high16 v6, 0x44fa0000    # 2000.0f

    mul-float/2addr v5, v6

    long-to-float v6, v0

    div-float/2addr v5, v6

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityY:F

    .line 125
    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityX:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x43fa0000    # 500.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_5

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityY:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x43fa0000    # 500.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_5

    .line 126
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityX:F

    .line 127
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityY:F

    .line 130
    .end local v0    # "duration":J
    .end local v2    # "dx":I
    .end local v3    # "dy":I
    .end local v4    # "fIndex":I
    :cond_5
    return-void

    .line 119
    :cond_6
    const/4 v4, 0x0

    .restart local v4    # "fIndex":I
    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlMovementDetector;
    .locals 2
    .param p0, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 49
    const-class v1, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 52
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized releaseInstance()V
    .locals 2

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->sInstance:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    monitor-exit v0

    return-void

    .line 56
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public onTouch(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 141
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    .line 142
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 143
    .local v6, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    .line 144
    .local v4, "ms":J
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v0, v1

    .line 145
    .local v0, "nx":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v8, v1

    .line 147
    .local v8, "ny":I
    if-nez v6, :cond_0

    .line 148
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    .line 149
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    .line 150
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    .line 151
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    .line 152
    iput-wide v4, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressMs:J

    .line 153
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLX:[I

    const/4 v7, 0x0

    iget v10, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    aput v10, v1, v7

    .line 154
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLY:[I

    const/4 v7, 0x0

    iget v10, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    aput v10, v1, v7

    .line 155
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLMs:[J

    const/4 v7, 0x0

    iget-wide v10, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressMs:J

    aput-wide v10, v1, v7

    .line 156
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLxIndex:I

    .line 157
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mTEvtCount:I

    .line 158
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    iget v10, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    invoke-interface {v1, v7, v10}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onPress(II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    .line 159
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    .end local v4    # "ms":J
    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongPressedOccur:J

    .end local v6    # "action":I
    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 160
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mInThreshold:Z

    .line 161
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    .line 162
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z

    .line 163
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovedOverClickLimit:Z

    move v3, v8

    .end local v8    # "ny":I
    .local v3, "ny":I
    move v2, v0

    .line 217
    .end local v0    # "nx":I
    .local v2, "nx":I
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    :goto_1
    return v1

    .line 164
    .end local v2    # "nx":I
    .end local v3    # "ny":I
    .restart local v0    # "nx":I
    .restart local v4    # "ms":J
    .restart local v6    # "action":I
    .restart local v8    # "ny":I
    :cond_0
    const/4 v1, 0x2

    if-ne v6, v1, :cond_3

    .line 165
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mInThreshold:Z

    if-eqz v1, :cond_2

    .line 166
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v7, 0x14

    if-ge v1, v7, :cond_1

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    sub-int v1, v8, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v7, 0x14

    if-ge v1, v7, :cond_1

    .line 168
    const/4 v1, 0x1

    move v3, v8

    .end local v8    # "ny":I
    .restart local v3    # "ny":I
    move v2, v0

    .end local v0    # "nx":I
    .restart local v2    # "nx":I
    goto :goto_1

    .line 170
    .end local v2    # "nx":I
    .end local v3    # "ny":I
    .restart local v0    # "nx":I
    .restart local v8    # "ny":I
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    sub-int v1, v0, v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    .line 171
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    sub-int v1, v8, v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    .line 172
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mInThreshold:Z

    .line 175
    :cond_2
    int-to-float v1, v0

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    sub-float/2addr v1, v7

    float-to-int v2, v1

    .line 176
    .end local v0    # "nx":I
    .restart local v2    # "nx":I
    int-to-float v1, v8

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    sub-float/2addr v1, v7

    float-to-int v3, v1

    .line 181
    .end local v8    # "ny":I
    .restart local v3    # "ny":I
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    const v7, 0x3f8ccccd    # 1.1f

    div-float/2addr v1, v7

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    .line 182
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    const v7, 0x3f8ccccd    # 1.1f

    div-float/2addr v1, v7

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    move-object v1, p0

    .line 184
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->calcMovement(IIJI)V

    .line 185
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    iget v10, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressX:I

    sub-int v10, v2, v10

    iget v11, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mPressY:I

    sub-int v11, v3, v11

    invoke-interface {v7, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onMove(II)Z

    move-result v7

    or-int/2addr v1, v7

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    goto :goto_0

    .line 186
    .end local v2    # "nx":I
    .end local v3    # "ny":I
    .restart local v0    # "nx":I
    .restart local v8    # "ny":I
    :cond_3
    const/16 v1, 0x8

    if-ne v6, v1, :cond_4

    .line 187
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    const/high16 v7, 0x42c80000    # 100.0f

    mul-float/2addr v1, v7

    float-to-int v9, v1

    .line 188
    .local v9, "scroll":I
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    const/4 v10, 0x0

    invoke-interface {v7, v10, v9, v0, v8}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onScroll(IIII)Z

    move-result v7

    or-int/2addr v1, v7

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    move v3, v8

    .end local v8    # "ny":I
    .restart local v3    # "ny":I
    move v2, v0

    .line 189
    .end local v0    # "nx":I
    .restart local v2    # "nx":I
    goto/16 :goto_0

    .end local v2    # "nx":I
    .end local v3    # "ny":I
    .end local v9    # "scroll":I
    .restart local v0    # "nx":I
    .restart local v8    # "ny":I
    :cond_4
    const/4 v1, 0x1

    if-ne v6, v1, :cond_7

    .line 190
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    if-eqz v1, :cond_5

    .line 191
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 192
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    .line 194
    :cond_5
    int-to-float v1, v0

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    sub-float/2addr v1, v7

    float-to-int v2, v1

    .line 195
    .end local v0    # "nx":I
    .restart local v2    # "nx":I
    int-to-float v1, v8

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    sub-float/2addr v1, v7

    float-to-int v3, v1

    .end local v8    # "ny":I
    .restart local v3    # "ny":I
    move-object v1, p0

    .line 196
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->calcMovement(IIJI)V

    .line 197
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovedOverClickLimit:Z

    if-nez v1, :cond_6

    .line 198
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onClick()Z

    move-result v7

    or-int/2addr v1, v7

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    .line 200
    :cond_6
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z

    .line 201
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    iget v10, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovX:I

    iget v11, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovY:I

    iget v12, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityX:F

    float-to-int v12, v12

    iget v13, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityY:F

    float-to-int v13, v13

    invoke-interface {v7, v10, v11, v12, v13}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onRelease(IIII)Z

    move-result v7

    or-int/2addr v1, v7

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    goto/16 :goto_0

    .line 202
    .end local v2    # "nx":I
    .end local v3    # "ny":I
    .restart local v0    # "nx":I
    .restart local v8    # "ny":I
    :cond_7
    const/4 v1, 0x3

    if-ne v6, v1, :cond_9

    .line 203
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    if-eqz v1, :cond_8

    .line 204
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 205
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z

    .line 207
    :cond_8
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z

    .line 208
    int-to-float v1, v0

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifX:F

    sub-float/2addr v1, v7

    float-to-int v2, v1

    .line 209
    .end local v0    # "nx":I
    .restart local v2    # "nx":I
    int-to-float v1, v8

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDifY:F

    sub-float/2addr v1, v7

    float-to-int v3, v1

    .end local v8    # "ny":I
    .restart local v3    # "ny":I
    move-object v1, p0

    .line 210
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->calcMovement(IIJI)V

    .line 211
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovX:I

    iget v10, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mMovY:I

    iget v11, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityX:F

    float-to-int v11, v11

    iget v12, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mVelocityY:F

    float-to-int v12, v12

    invoke-interface {v1, v7, v10, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onRelease(IIII)Z

    .line 212
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    goto/16 :goto_0

    .line 213
    .end local v2    # "nx":I
    .end local v3    # "ny":I
    .restart local v0    # "nx":I
    .restart local v8    # "ny":I
    :cond_9
    const/16 v1, 0x3e8

    if-eq v6, v1, :cond_a

    const/16 v1, 0x3ea

    if-eq v6, v1, :cond_a

    const/16 v1, 0x3eb

    if-ne v6, v1, :cond_b

    .line 215
    :cond_a
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v10, v10

    invoke-interface {v1, v7, v10}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onGenericMotionTouch(II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mHandled:Z

    :cond_b
    move v3, v8

    .end local v8    # "ny":I
    .restart local v3    # "ny":I
    move v2, v0

    .end local v0    # "nx":I
    .restart local v2    # "nx":I
    goto/16 :goto_0
.end method

.method public onTouch(Landroid/view/MotionEvent;Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .prologue
    .line 133
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .line 134
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setMoveDetectorListener(Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .line 222
    return-void
.end method
