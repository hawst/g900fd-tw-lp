.class public Lcom/sec/android/gallery3d/ui/DetailsHelper;
.super Ljava/lang/Object;
.source "DetailsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/DetailsHelper$ResolutionResolvingListener;,
        Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;,
        Lcom/sec/android/gallery3d/ui/DetailsHelper$CloseListener;,
        Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsSource;
    }
.end annotation


# static fields
.field private static sAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;


# instance fields
.field private mContainer:Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/GLView;Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsSource;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "rootPane"    # Lcom/sec/android/gallery3d/ui/GLView;
    .param p3, "source"    # Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsSource;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView;

    invoke-direct {v0, p1, p3}, Lcom/sec/android/gallery3d/ui/DialogDetailsView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsSource;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->mContainer:Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;

    .line 55
    return-void
.end method

.method public static getDetailsName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # I

    .prologue
    .line 103
    sparse-switch p1, :sswitch_data_0

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 105
    :sswitch_0
    const v0, 0x7f0e0071

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 107
    :sswitch_1
    const v0, 0x7f0e0072

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    :sswitch_2
    const v0, 0x7f0e0073

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 111
    :sswitch_3
    const v0, 0x7f0e0074

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 113
    :sswitch_4
    const v0, 0x7f0e0075

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 115
    :sswitch_5
    const v0, 0x7f0e0076

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    :sswitch_6
    const v0, 0x7f0e0077

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 119
    :sswitch_7
    const v0, 0x7f0e0078

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :sswitch_8
    const v0, 0x7f0e0079

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 123
    :sswitch_9
    const v0, 0x7f0e0387

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 125
    :sswitch_a
    const v0, 0x7f0e0388

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    :sswitch_b
    const v0, 0x7f0e007a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129
    :sswitch_c
    const v0, 0x7f0e007b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 131
    :sswitch_d
    const v0, 0x7f0e007c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 133
    :sswitch_e
    const v0, 0x7f0e007d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 135
    :sswitch_f
    const v0, 0x7f0e007e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 137
    :sswitch_10
    const v0, 0x7f0e007f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 139
    :sswitch_11
    const v0, 0x7f0e0080

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 141
    :sswitch_12
    const v0, 0x7f0e0081

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 103
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_9
        0x4 -> :sswitch_2
        0x9 -> :sswitch_3
        0xc -> :sswitch_5
        0xd -> :sswitch_6
        0xe -> :sswitch_a
        0x10 -> :sswitch_7
        0x11 -> :sswitch_8
        0x64 -> :sswitch_b
        0x65 -> :sswitch_c
        0x66 -> :sswitch_d
        0x67 -> :sswitch_f
        0x68 -> :sswitch_10
        0x6a -> :sswitch_e
        0x6b -> :sswitch_11
        0x6c -> :sswitch_12
        0xc8 -> :sswitch_4
    .end sparse-switch
.end method

.method public static pause()V
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->sAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->sAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->cancel()V

    .line 92
    :cond_0
    return-void
.end method

.method public static resolveAddress(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;[DLcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;
    .locals 1
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p1, "latlng"    # [D
    .param p2, "listener"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->sAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    sput-object v0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->sAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    .line 81
    :goto_0
    sget-object v0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->sAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->resolveAddress([DLcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 79
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->sAddressResolver:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->cancel()V

    goto :goto_0
.end method

.method public static resolveResolution(Ljava/lang/String;Lcom/sec/android/gallery3d/ui/DetailsHelper$ResolutionResolvingListener;)V
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/DetailsHelper$ResolutionResolvingListener;

    .prologue
    .line 85
    invoke-static {p0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 86
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-interface {p1, v1, v2}, Lcom/sec/android/gallery3d/ui/DetailsHelper$ResolutionResolvingListener;->onResolutionAvailable(II)V

    goto :goto_0
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->mContainer:Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;->hide()V

    .line 100
    return-void
.end method

.method public reloadDetails()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->mContainer:Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;->reloadDetails()V

    .line 68
    return-void
.end method

.method public setCloseListener(Lcom/sec/android/gallery3d/ui/DetailsHelper$CloseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/DetailsHelper$CloseListener;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->mContainer:Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;->setCloseListener(Lcom/sec/android/gallery3d/ui/DetailsHelper$CloseListener;)V

    .line 72
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsHelper;->mContainer:Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/DetailsHelper$DetailsViewContainer;->show()V

    .line 96
    return-void
.end method
