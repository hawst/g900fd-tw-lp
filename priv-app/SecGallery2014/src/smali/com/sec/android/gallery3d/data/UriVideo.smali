.class public Lcom/sec/android/gallery3d/data/UriVideo;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "UriVideo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;
    }
.end annotation


# static fields
.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final STATE_INIT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "UriImage"


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mContentType:Ljava/lang/String;

.field private mFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private mHeight:I

.field private final mUri:Landroid/net/Uri;

.field private mWidth:I

.field private mfilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "/uri/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/UriVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;Landroid/net/Uri;)V
    .locals 2
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 56
    invoke-static {}, Lcom/sec/android/gallery3d/data/UriVideo;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 57
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    .line 58
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 59
    const-string v0, "file"

    invoke-virtual {p3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v0

    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->isdrm:Z

    .line 67
    :cond_0
    :goto_0
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/data/UriVideo;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mContentType:Ljava/lang/String;

    .line 68
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/UriVideo;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mfilePath:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mfilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mfilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->isdrm:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/UriVideo;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriVideo;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/UriVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriVideo;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/data/UriVideo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriVideo;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mWidth:I

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/data/UriVideo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/UriVideo;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mHeight:I

    return p1
.end method

.method private getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 71
    const-string v3, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 72
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/UriVideo;->isdrm:Z

    if-eqz v3, :cond_1

    .line 73
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 101
    :cond_0
    :goto_0
    return-object v2

    .line 77
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "extension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "type":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 84
    if-nez v2, :cond_4

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriVideo;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_4

    const-string v3, ".gif"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 87
    const-string v2, "image/gif"

    goto :goto_0

    .line 90
    .end local v0    # "extension":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "type":Ljava/lang/String;
    :cond_2
    const-string v3, "http"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 91
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->parseMimetypeFromHttpUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 93
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/data/UriVideo;->isdrm:Z

    if-eqz v3, :cond_4

    .line 94
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/data/UriVideo;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 95
    .restart local v1    # "filePath":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 96
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 101
    .end local v1    # "filePath":Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private isSharable()Z
    .locals 2

    .prologue
    .line 244
    const-string v0, "file"

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 208
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 214
    return-void

    .line 212
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 1

    .prologue
    .line 195
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 197
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 229
    const-string v0, "file"

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 236
    :goto_0
    return-object v0

    .line 232
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->isdrm:Z

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/data/UriVideo;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 236
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 248
    const/4 v6, 0x0

    .line 249
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 252
    .local v8, "filePath":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string v3, "_data is not null"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 257
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 258
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 260
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 266
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v9, v8

    .line 268
    .end local v8    # "filePath":Ljava/lang/String;
    .local v9, "filePath":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 263
    .end local v9    # "filePath":Ljava/lang/String;
    .restart local v8    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 264
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "UriImage"

    const-string v1, "createFromUri: Exception"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v9, v8

    .line 268
    .end local v8    # "filePath":Ljava/lang/String;
    .restart local v9    # "filePath":Ljava/lang/String;
    goto :goto_0

    .line 266
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v9    # "filePath":Ljava/lang/String;
    .restart local v8    # "filePath":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 273
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x4

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriVideo;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/UriVideo;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 168
    const-wide v0, 0x8010000080L

    .line 170
    .local v0, "operations":J
    return-wide v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;-><init>(Lcom/sec/android/gallery3d/data/UriVideo;I)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot regquest a large image to a local video!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public rotate(I)V
    .locals 0
    .param p1, "degrees"    # I

    .prologue
    .line 175
    return-void
.end method
