.class public Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FullImageListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private mFuture:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private final mPath:Lcom/sec/android/gallery3d/data/Path;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1325
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1326
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 1327
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1328
    return-void
.end method


# virtual methods
.method public onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1332
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 1333
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1336
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->setInitialScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;I)V

    .line 1338
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateFullImage(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1344
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLaunchTranslucent:Z

    if-eqz v0, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->showRootView()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 1346
    :cond_0
    return-void
.end method
