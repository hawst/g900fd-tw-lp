.class Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;
.super Landroid/app/Presentation;
.source "CameraViewFinderHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraViewFinderPresentation"
.end annotation


# instance fields
.field private mGLRenderTask:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

.field private mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Landroid/content/Context;Landroid/view/Display;)V
    .locals 0
    .param p2, "outerContext"    # Landroid/content/Context;
    .param p3, "display"    # Landroid/view/Display;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    .line 415
    invoke-direct {p0, p2, p3}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    .line 416
    return-void
.end method

.method private startGLRenderTask()V
    .locals 4

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mGLRenderTask:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

    if-nez v0, :cond_0

    .line 470
    new-instance v0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mGLRenderTask:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

    .line 471
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mGLRenderTask:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->start()V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 428
    invoke-super {p0}, Landroid/app/Presentation;->dismiss()V

    .line 430
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mGLRenderTask:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

    if-eqz v1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mGLRenderTask:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->cancel()V

    .line 432
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mGLRenderTask:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;

    .line 435
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$700(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->isPaused()Z

    move-result v0

    .line 436
    .local v0, "isPaused":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_2

    .line 437
    if-nez v0, :cond_1

    .line 439
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 440
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$700(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->pause()V

    .line 441
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$800(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->pause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 447
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 448
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onPause()V

    .line 449
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 452
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onResume()V

    .line 453
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 454
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 456
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 457
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$800(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->resume()V

    .line 458
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$700(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resume()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 460
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 463
    :cond_3
    return-void

    .line 443
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v1

    .line 460
    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v1
.end method

.method protected initializeGLSurfaceView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 476
    const v1, 0x7f0f00bf

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 477
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    if-nez v1, :cond_0

    .line 478
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    .line 479
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    if-eqz v1, :cond_2

    .line 480
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$700(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->isPaused()Z

    move-result v0

    .line 481
    .local v0, "isPaused":Z
    if-nez v0, :cond_1

    .line 483
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 484
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$700(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->pause()V

    .line 485
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$800(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->pause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 490
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 491
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 492
    if-nez v0, :cond_2

    .line 494
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 495
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$700(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resume()V

    .line 496
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$800(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->resume()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 498
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 502
    .end local v0    # "isPaused":Z
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->startGLRenderTask()V

    .line 503
    return-void

    :cond_3
    move-object v1, v2

    .line 478
    goto :goto_0

    .line 487
    .restart local v0    # "isPaused":Z
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$900(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v1

    .line 498
    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 420
    invoke-super {p0, p1}, Landroid/app/Presentation;->onCreate(Landroid/os/Bundle;)V

    .line 421
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 422
    const v0, 0x7f03005c

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->setContentView(I)V

    .line 423
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;->initializeGLSurfaceView()V

    .line 424
    return-void
.end method
