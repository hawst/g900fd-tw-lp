.class public Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;
.super Ljava/lang/Object;
.source "ImageDRMUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/ImageDRMUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DRMInfo"
.end annotation


# instance fields
.field public final mDrmType:I

.field public mFilePath:Ljava/lang/String;

.field public final mLicenseCategory:I

.field public final mOriginalMime:Ljava/lang/String;

.field public final mRightType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/util/ImageDRMUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/util/ImageDRMUtil;Ljava/lang/String;)V
    .locals 1
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 678
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->this$0:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 679
    iput-object p2, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mFilePath:Ljava/lang/String;

    .line 680
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getDrmType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mDrmType:I

    .line 681
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mOriginalMime:Ljava/lang/String;

    .line 682
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getLicenseCategory(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mLicenseCategory:I

    .line 683
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getRightType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/util/ImageDRMUtil$DRMInfo;->mRightType:I

    .line 684
    return-void
.end method
