.class public Lcom/sec/android/gallery3d/data/GallerySearchAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "GallerySearchAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;
    }
.end annotation


# static fields
.field private static K_TEST_MODE:Z

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final mIDComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private static mSortByOldest:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBasePathMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mConsumer:Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

.field private final mContext:Landroid/content/Context;

.field private mCover:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mImageProjection:[Ljava/lang/String;

.field private mIsRequestKeywordFromSearchPress:Z

.field private mKeyword:Ljava/lang/String;

.field private mMediaItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaSetType:I

.field private mName:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mOperation:Ljava/lang/String;

.field private mPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mSupportShare:Z

.field private mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

.field private mTempMediaItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoProjection:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    sput-boolean v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->K_TEST_MODE:Z

    .line 72
    sput-boolean v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mSortByOldest:Z

    .line 143
    new-instance v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$2;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$2;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mComparator:Ljava/util/Comparator;

    .line 554
    new-instance v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$4;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$4;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mIDComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 8
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 85
    invoke-static {}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 52
    const-class v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mKeyword:Ljava/lang/String;

    .line 63
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mIsRequestKeywordFromSearchPress:Z

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mName:Ljava/lang/String;

    .line 70
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mSupportShare:Z

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    .line 73
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-eqz v0, :cond_1

    const-string v0, " AND "

    :goto_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mOperation:Ljava/lang/String;

    .line 76
    new-instance v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$1;-><init>(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mConsumer:Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

    .line 254
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaSetType:I

    .line 619
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "date"

    aput-object v1, v0, v3

    const-string/jumbo v1, "title"

    aput-object v1, v0, v4

    const-string/jumbo v1, "uri"

    aput-object v1, v0, v5

    const-string v1, "mimetype"

    aput-object v1, v0, v6

    const-string v1, "named_location"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "person_names"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "calendar_event"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "user_tags"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "scene_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "subscene_type"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mImageProjection:[Ljava/lang/String;

    .line 632
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "date"

    aput-object v1, v0, v3

    const-string/jumbo v1, "title"

    aput-object v1, v0, v4

    const-string/jumbo v1, "uri"

    aput-object v1, v0, v5

    const-string v1, "mimetype"

    aput-object v1, v0, v6

    const-string v1, "named_location"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "user_tags"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mVideoProjection:[Ljava/lang/String;

    .line 86
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    .line 88
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 89
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mBasePathMap:Ljava/util/HashMap;

    .line 91
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->makeBaseItemList()V

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 95
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    .line 96
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/data/CategoryAlbum;->UPDATE_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p3}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 97
    return-void

    .line 73
    :cond_1
    const-string v0, " OR "

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 51
    sget-boolean v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mSortByOldest:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/GallerySearchAlbum;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private createEventSelectionStatement()Ljava/lang/StringBuffer;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 884
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 885
    .local v4, "result":Ljava/lang/StringBuffer;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 887
    .local v5, "selection_part":Ljava/lang/StringBuffer;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 888
    .local v2, "eventFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    iget-object v7, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 889
    .local v6, "size":I
    if-gtz v6, :cond_0

    .line 910
    .end local v2    # "eventFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .end local v6    # "size":I
    :goto_0
    return-object v4

    .line 892
    .restart local v2    # "eventFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .restart local v6    # "size":I
    :cond_0
    const-string v7, "("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 893
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v6, :cond_2

    .line 894
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 895
    iget-object v7, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "\'"

    const-string v9, "\\\'"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 896
    .local v1, "event":Ljava/lang/String;
    const-string v7, "calendar_event"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 897
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 898
    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 899
    if-nez v3, :cond_1

    .line 900
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 893
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 902
    :cond_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mOperation:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 906
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "eventFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .end local v3    # "i":I
    .end local v6    # "size":I
    :catch_0
    move-exception v0

    .line 907
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 908
    invoke-virtual {v4, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    .line 905
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "eventFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .restart local v3    # "i":I
    .restart local v6    # "size":I
    :cond_2
    :try_start_1
    const-string v7, " ) "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private createKeywordSelectionStatement(Ljava/lang/String;Z)Ljava/lang/StringBuffer;
    .locals 4
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "isImage"    # Z

    .prologue
    const/4 v3, 0x1

    .line 695
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 696
    .local v0, "result":Ljava/lang/StringBuffer;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 742
    :cond_0
    :goto_0
    return-object v0

    .line 700
    :cond_1
    sget-boolean v1, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->K_TEST_MODE:Z

    if-eqz v1, :cond_3

    .line 701
    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 702
    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 703
    const-string v1, " OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "named_location"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 705
    const-string v1, " OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "user_tags"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 707
    if-ne p2, v3, :cond_2

    .line 708
    const-string v1, " OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "person_names"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 711
    const-string v1, " OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "calendar_event"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 714
    const-string v1, " OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "scene_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 716
    const-string v1, " OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "subscene_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 719
    :cond_2
    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 721
    :cond_3
    const-string v1, "\'"

    const-string v2, "\\\'"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 722
    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 723
    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 725
    const-string v1, "OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "named_location"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 727
    const-string v1, "OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "user_tags"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'% )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 729
    if-ne p2, v3, :cond_4

    .line 730
    const-string v1, "OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "person_names"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "%\' )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 732
    const-string v1, "OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "calendar_event"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "%\' )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 734
    const-string v1, "OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "scene_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "%\' )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 736
    const-string v1, "OR ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "subscene_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "%\' )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 739
    :cond_4
    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method

.method private createLocationSelectionStatement()Ljava/lang/StringBuffer;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 846
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 847
    .local v4, "result":Ljava/lang/StringBuffer;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 849
    .local v5, "selection_part":Ljava/lang/StringBuffer;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 850
    .local v3, "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    iget-object v7, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 851
    .local v6, "size":I
    if-gtz v6, :cond_0

    .line 880
    .end local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .end local v6    # "size":I
    :goto_0
    return-object v4

    .line 854
    .restart local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .restart local v6    # "size":I
    :cond_0
    const-string v7, "("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 855
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_3

    .line 856
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 858
    sget-boolean v7, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->K_TEST_MODE:Z

    if-eqz v7, :cond_1

    .line 859
    iget-object v7, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 860
    .local v1, "event":Ljava/lang/String;
    const-string v7, "named_location"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 861
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 869
    :goto_2
    if-nez v2, :cond_2

    .line 870
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 855
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 863
    .end local v1    # "event":Ljava/lang/String;
    :cond_1
    iget-object v7, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "\'"

    const-string v9, "\\\'"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 864
    .restart local v1    # "event":Ljava/lang/String;
    const-string v7, "named_location"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 865
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 866
    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 876
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .end local v6    # "size":I
    :catch_0
    move-exception v0

    .line 877
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 878
    invoke-virtual {v4, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    .line 872
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .restart local v6    # "size":I
    :cond_2
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mOperation:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 875
    .end local v1    # "event":Ljava/lang/String;
    :cond_3
    const-string v7, " ) "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private createPeopleSelectionStatement()Ljava/lang/StringBuffer;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 807
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 808
    .local v4, "result":Ljava/lang/StringBuffer;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 811
    .local v5, "selection_part":Ljava/lang/StringBuffer;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 812
    .local v3, "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    iget-object v7, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 813
    .local v6, "size":I
    if-gtz v6, :cond_0

    .line 842
    .end local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .end local v6    # "size":I
    :goto_0
    return-object v4

    .line 816
    .restart local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .restart local v6    # "size":I
    :cond_0
    const-string v7, "("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 817
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_3

    .line 818
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 820
    sget-boolean v7, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->K_TEST_MODE:Z

    if-eqz v7, :cond_1

    .line 821
    iget-object v7, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 822
    .local v1, "event":Ljava/lang/String;
    const-string v7, "person_names"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 823
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 831
    :goto_2
    if-nez v2, :cond_2

    .line 832
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 817
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 825
    .end local v1    # "event":Ljava/lang/String;
    :cond_1
    iget-object v7, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "\'"

    const-string v9, "\\\'"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 826
    .restart local v1    # "event":Ljava/lang/String;
    const-string v7, "person_names"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 827
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 828
    const-string v7, "\'"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 838
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .end local v6    # "size":I
    :catch_0
    move-exception v0

    .line 839
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 840
    invoke-virtual {v4, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    .line 834
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v3    # "locationFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    .restart local v6    # "size":I
    :cond_2
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mOperation:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 837
    .end local v1    # "event":Ljava/lang/String;
    :cond_3
    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private createTimeSelectionStatement()Ljava/lang/StringBuffer;
    .locals 14

    .prologue
    .line 914
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 915
    .local v4, "result":Ljava/lang/StringBuffer;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 917
    .local v5, "selection_part":Ljava/lang/StringBuffer;
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    iget-object v11, v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 918
    .local v10, "timeFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    iget-object v11, v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 919
    .local v6, "size":I
    if-gtz v6, :cond_0

    .line 951
    .end local v6    # "size":I
    .end local v10    # "timeFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :goto_0
    return-object v4

    .line 923
    .restart local v6    # "size":I
    .restart local v10    # "timeFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :cond_0
    const-string v11, "("

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 924
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v6, :cond_4

    .line 925
    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 926
    new-instance v7, Ljava/util/StringTokenizer;

    iget-object v11, v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-string v12, ","

    invoke-direct {v7, v11, v12}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    .local v7, "stringtokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 928
    .local v8, "stime":J
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 929
    .local v2, "etime":J
    const-wide/16 v12, 0x0

    cmp-long v11, v8, v12

    if-gez v11, :cond_1

    const-wide/16 v8, 0x0

    .line 930
    :cond_1
    const-wide/16 v12, 0x0

    cmp-long v11, v2, v12

    if-gtz v11, :cond_2

    .line 924
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 933
    :cond_2
    const-string v11, "( "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, "date"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " >= "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 934
    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 935
    const-string v11, " AND "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, "date"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " <= "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 936
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 937
    const-string v11, " )"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 939
    if-nez v1, :cond_3

    .line 940
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 947
    .end local v1    # "i":I
    .end local v2    # "etime":J
    .end local v6    # "size":I
    .end local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .end local v8    # "stime":J
    .end local v10    # "timeFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :catch_0
    move-exception v0

    .line 948
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 949
    const/4 v11, 0x0

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    .line 942
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    .restart local v2    # "etime":J
    .restart local v6    # "size":I
    .restart local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .restart local v8    # "stime":J
    .restart local v10    # "timeFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :cond_3
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mOperation:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 945
    .end local v2    # "etime":J
    .end local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .end local v8    # "stime":J
    :cond_4
    const-string v11, " ) "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private createUserTagSelectionStatement(Z)Ljava/lang/StringBuffer;
    .locals 11
    .param p1, "isImage"    # Z

    .prologue
    const/4 v10, 0x1

    .line 746
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 747
    .local v3, "result":Ljava/lang/StringBuffer;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 750
    .local v4, "selection_part":Ljava/lang/StringBuffer;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 751
    .local v6, "userTagFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    iget-object v7, v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 752
    .local v5, "size":I
    if-gtz v5, :cond_0

    .line 803
    .end local v3    # "result":Ljava/lang/StringBuffer;
    .end local v5    # "size":I
    .end local v6    # "userTagFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :goto_0
    return-object v3

    .line 755
    .restart local v3    # "result":Ljava/lang/StringBuffer;
    .restart local v5    # "size":I
    .restart local v6    # "userTagFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :cond_0
    const-string v7, "("

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 756
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_5

    .line 757
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 758
    sget-boolean v7, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->K_TEST_MODE:Z

    if-eqz v7, :cond_2

    .line 759
    iget-object v7, v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 760
    .local v1, "event":Ljava/lang/String;
    if-ne p1, v10, :cond_1

    .line 761
    const-string v7, "( "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "scene_type"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 762
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 763
    const-string v7, ") OR ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 764
    const-string/jumbo v7, "user_tags"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 765
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 766
    const-string v7, ") OR ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 767
    const-string/jumbo v7, "subscene_type"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 768
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 769
    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 792
    :goto_2
    if-nez v2, :cond_4

    .line 793
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 756
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 771
    :cond_1
    const-string v7, "( "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string/jumbo v8, "user_tags"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 799
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v5    # "size":I
    .end local v6    # "userTagFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :catch_0
    move-exception v0

    .line 800
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 801
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 776
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "i":I
    .restart local v5    # "size":I
    .restart local v6    # "userTagFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    :cond_2
    :try_start_1
    iget-object v7, v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "\'"

    const-string v9, "\\\'"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 777
    .restart local v1    # "event":Ljava/lang/String;
    if-ne p1, v10, :cond_3

    .line 778
    const-string v7, "(( "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "scene_type"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 779
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 780
    const-string v7, "\') OR ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 781
    const-string/jumbo v7, "user_tags"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 782
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 783
    const-string v7, "\') OR ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 784
    const-string/jumbo v7, "subscene_type"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 785
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 786
    const-string v7, "\'))"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 788
    :cond_3
    const-string v7, "( "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string/jumbo v8, "user_tags"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " = \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\')"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 795
    :cond_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mOperation:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 798
    .end local v1    # "event":Ljava/lang/String;
    :cond_5
    const-string v7, ")"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private getClustering(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .param p1, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 955
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 956
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 957
    .local v10, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    .line 958
    .local v7, "cursorImage":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 960
    .local v8, "cursorVideo":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 963
    .local v3, "sqlStr":Ljava/lang/String;
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mIsRequestKeywordFromSearchPress:Z

    if-eqz v1, :cond_1

    .line 965
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getQuerySelection(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 969
    :goto_0
    if-eqz v3, :cond_0

    const-string v1, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 970
    :cond_0
    const/4 v1, 0x0

    .line 1019
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1020
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_1
    return-object v1

    .line 967
    :cond_1
    :try_start_1
    const-string v1, ""

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getQuerySelection(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 971
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 973
    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_IMAGE_TABLE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mImageProjection:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 974
    if-eqz v7, :cond_4

    .line 975
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 976
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    const-string v2, "[DCMQuery[Image] : cusor success! "

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    const/4 v6, 0x0

    .line 979
    .local v6, "count":I
    :cond_3
    add-int/lit8 v6, v6, 0x1

    .line 980
    const-string/jumbo v1, "uri"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 981
    .local v12, "uri":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v12, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v12, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 982
    .local v11, "imageId":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 983
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3

    .line 984
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[DCMQuery[Image] : cusor count = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    .end local v6    # "count":I
    .end local v11    # "imageId":Ljava/lang/String;
    .end local v12    # "uri":Ljava/lang/String;
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mIsRequestKeywordFromSearchPress:Z

    if-eqz v1, :cond_7

    .line 991
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getQuerySelection(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 995
    :goto_2
    if-eqz v3, :cond_6

    .line 996
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 998
    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_VIDEO_TABLE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mVideoProjection:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 999
    if-eqz v8, :cond_6

    .line 1000
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1001
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    const-string v2, "[DCMQuery[Video] : cusor success! "

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    const/4 v6, 0x0

    .line 1004
    .restart local v6    # "count":I
    :cond_5
    add-int/lit8 v6, v6, 0x1

    .line 1005
    const-string/jumbo v1, "uri"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1006
    .restart local v12    # "uri":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v12, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v12, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 1007
    .local v13, "videoId":Ljava/lang/String;
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1009
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[DCMQuery[Video] : cusor count = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    .end local v6    # "count":I
    .end local v12    # "uri":Ljava/lang/String;
    .end local v13    # "videoId":Ljava/lang/String;
    :cond_6
    invoke-static {v10}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1014
    invoke-direct {p0, v10}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getPath(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 1019
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1020
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 993
    :cond_7
    :try_start_2
    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getQuerySelection(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    goto/16 :goto_2

    .line 1015
    :catch_0
    move-exception v9

    .line 1016
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1017
    const/4 v1, 0x0

    .line 1019
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1020
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 1019
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1020
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private getPath(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 574
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move-object v5, v6

    .line 596
    :cond_1
    :goto_0
    return-object v5

    .line 576
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 577
    .local v4, "size":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v7, :cond_3

    move-object v5, v6

    .line 578
    goto :goto_0

    .line 579
    :cond_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    monitor-enter v7

    .line 580
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .line 581
    .local v0, "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItemCount()I

    move-result v8

    if-gtz v8, :cond_5

    .line 582
    :cond_4
    monitor-exit v7

    move-object v5, v6

    goto :goto_0

    .line 583
    :cond_5
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mBasePathMap:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v8

    if-gtz v8, :cond_6

    .line 584
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->makeBaseItemList()V

    .line 586
    :cond_6
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 588
    .local v5, "tempPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_7

    .line 589
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 590
    .local v2, "m":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mBasePathMap:Ljava/util/HashMap;

    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 591
    .local v3, "pathtemp":Lcom/sec/android/gallery3d/data/Path;
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mBasePathMap:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 586
    .end local v0    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v1    # "i":I
    .end local v2    # "m":Ljava/lang/Integer;
    .end local v3    # "pathtemp":Lcom/sec/android/gallery3d/data/Path;
    .end local v5    # "tempPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 594
    .restart local v0    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .restart local v1    # "i":I
    .restart local v5    # "tempPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-gtz v7, :cond_1

    move-object v5, v6

    .line 595
    goto :goto_0
.end method

.method private getQuerySelection(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "isImage"    # Z

    .prologue
    .line 646
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 649
    .local v0, "result":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    if-nez v2, :cond_0

    .line 650
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    .line 653
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getExistFilterMask()I

    move-result v2

    if-eqz v2, :cond_5

    .line 656
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->createTimeSelectionStatement()Ljava/lang/StringBuffer;

    move-result-object v1

    .line 657
    .local v1, "tempResult":Ljava/lang/StringBuffer;
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 658
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 662
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->createEventSelectionStatement()Ljava/lang/StringBuffer;

    move-result-object v1

    .line 663
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 664
    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 668
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->createLocationSelectionStatement()Ljava/lang/StringBuffer;

    move-result-object v1

    .line 669
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 670
    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 674
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->createPeopleSelectionStatement()Ljava/lang/StringBuffer;

    move-result-object v1

    .line 675
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 676
    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 680
    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->createUserTagSelectionStatement(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 681
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 682
    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 686
    .end local v1    # "tempResult":Ljava/lang/StringBuffer;
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->createKeywordSelectionStatement(Ljava/lang/String;Z)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 687
    .restart local v1    # "tempResult":Ljava/lang/StringBuffer;
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_6

    .line 688
    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 690
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private makeBaseItemList()V
    .locals 8

    .prologue
    .line 100
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v6, :cond_1

    .line 101
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    const-string v7, "mClusterAlbumSet is null"

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v6, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getAlbums()Ljava/util/ArrayList;

    move-result-object v1

    .line 105
    .local v1, "albumSets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/ClusterAlbum;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 107
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .line 108
    .local v0, "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItemCount()I

    move-result v6

    if-gtz v6, :cond_3

    .line 109
    :cond_2
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mBasePathMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    goto :goto_0

    .line 112
    :cond_3
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mBasePathMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 113
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v4

    .line 114
    .local v4, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItemCount()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 115
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/Path;

    .line 116
    .local v5, "tempPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v5, :cond_4

    .line 117
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 118
    .local v3, "id":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mBasePathMap:Ljava/util/HashMap;

    invoke-virtual {v6, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    .end local v3    # "id":Ljava/lang/Integer;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private declared-synchronized runItems(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, "tempPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 172
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 173
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mConsumer:Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    sget-object v2, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mComparator:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 187
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 188
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 167
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 180
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 182
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method private updateKeyword(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mKeyword:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->notifyDirty()V

    .line 137
    return-void
.end method

.method private updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "joinedName"    # Ljava/lang/String;
    .param p4, "personId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 471
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p3, :cond_0

    .line 472
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    invoke-static {v3, p4, p3}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V

    .line 474
    :cond_0
    const/4 v2, 0x0

    .line 475
    .local v2, "recommendPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Path;

    .line 476
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v1, p4}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateOneFace(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V

    .line 478
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xd

    aget-object v3, v3, v4

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 479
    move-object v2, v1

    goto :goto_0

    .line 482
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_2
    if-eqz v2, :cond_3

    .line 483
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v2, p4}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->autoRecommend(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V

    .line 486
    :goto_1
    return-void

    .line 485
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateMediaSet()V

    goto :goto_1
.end method


# virtual methods
.method public addKeyword(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateKeyword(Ljava/lang/String;)V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mIsRequestKeywordFromSearchPress:Z

    .line 126
    return-void
.end method

.method public addTag(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "filterType"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;
    .param p3, "categoryType"    # I

    .prologue
    .line 610
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->addTag(Ljava/lang/String;Ljava/lang/String;I)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->notifyDirty()V

    .line 612
    return-void
.end method

.method public autoRecommend(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "personId"    # I

    .prologue
    .line 489
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v1

    .line 490
    .local v1, "values":[Ljava/lang/String;
    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 491
    .local v0, "id":I
    const/4 v2, 0x0

    invoke-static {p1, v2, v0, p3}, Lcom/sec/samsung/gallery/access/face/FaceList;->recommendFaces(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V

    .line 492
    return-void
.end method

.method public changeKeyword(Ljava/lang/String;)Z
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 129
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateKeyword(Ljava/lang/String;)V

    .line 130
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mIsRequestKeywordFromSearchPress:Z

    .line 131
    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 196
    :cond_1
    return-void
.end method

.method public clusteringSearchResult()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 303
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getExistFilterMask()I

    move-result v0

    if-nez v0, :cond_3

    .line 304
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 308
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 314
    :cond_2
    :goto_0
    return v1

    .line 313
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mKeyword:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getClustering(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->runItems(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public delete()V
    .locals 4

    .prologue
    .line 346
    new-instance v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$3;-><init>(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;)V

    .line 354
    .local v0, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 355
    return-void
.end method

.method protected enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)I
    .locals 2
    .param p1, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .param p2, "startIndex"    # I

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public freeFilterList()V
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->freeFilter()V

    .line 601
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateKeyword(Ljava/lang/String;)V

    .line 602
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mIsRequestKeywordFromSearchPress:Z

    .line 603
    return-void
.end method

.method public getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mCover:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mCover:Lcom/sec/android/gallery3d/data/MediaItem;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 199
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    :cond_0
    move-object v1, v2

    .line 204
    :goto_0
    return-object v1

    .line 202
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 203
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    move-object v1, v2

    .line 204
    goto :goto_0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 7
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    monitor-enter v5

    .line 238
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int v6, p1, p2

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 239
    .local v1, "end":I
    const/4 v2, 0x0

    .line 241
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-ltz p1, :cond_0

    if-le p1, v1, :cond_1

    .line 242
    :cond_0
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move-object v2, v3

    .line 246
    .end local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_0
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v4, v2

    .line 249
    :goto_1
    return-object v4

    .line 244
    :cond_1
    :try_start_3
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move-object v2, v3

    .end local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 249
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    monitor-exit v5

    goto :goto_1

    .line 251
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "end":I
    .end local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4
.end method

.method public getMediaItemCount()I
    .locals 2

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 232
    :cond_0
    return v0
.end method

.method public getMediaItemEx(II)Ljava/util/ArrayList;
    .locals 5
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 502
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    .line 503
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 504
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_0

    instance-of v4, v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v4, :cond_0

    .line 505
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 507
    .restart local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 509
    .end local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    return-object v3
.end method

.method public getMediaItems()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getMediaSetType()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaSetType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 10

    .prologue
    const-wide/high16 v8, 0x800000000000000L

    const-wide/high16 v6, 0x200000000000000L

    .line 319
    const-wide/16 v2, 0x5

    .line 320
    .local v2, "operation":J
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mSupportShare:Z

    if-nez v1, :cond_0

    .line 321
    const-wide/16 v4, -0x5

    and-long/2addr v2, v4

    .line 324
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_1

    .line 326
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getClusterKind()I

    move-result v1

    const/4 v4, 0x4

    if-ne v1, v4, :cond_4

    .line 327
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    const v5, 0x7f0e0068

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 341
    .end local v2    # "operation":J
    :cond_1
    :goto_0
    return-wide v2

    .line 329
    .restart local v2    # "operation":J
    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mMediaSetType:I

    if-nez v1, :cond_3

    .line 330
    or-long v4, v2, v8

    or-long/2addr v4, v6

    const-wide/high16 v6, 0x400000000000000L

    or-long v2, v4, v6

    goto :goto_0

    .line 332
    :cond_3
    or-long v4, v2, v8

    or-long v2, v4, v6

    goto :goto_0

    .line 334
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getClusterKind()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/4 v4, 0x5

    if-ne v1, v4, :cond_1

    goto :goto_0

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Ljava/lang/ClassCastException;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    const-string v4, "ClassCastException at getSupportedOperations."

    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    return-object v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 1031
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public getTotalMediaItemCount()I
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x1

    return v0
.end method

.method public isSearchKeyEmpty()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 292
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mKeyword:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mKeyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 298
    :cond_0
    :goto_0
    return v0

    .line 295
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getExistFilterMask()I

    move-result v1

    if-nez v1, :cond_0

    .line 298
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 1026
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->notifyContentChanged()V

    .line 1027
    return-void
.end method

.method public reload()J
    .locals 6

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v2

    .line 276
    .local v2, "newVersion":J
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataVersion:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 277
    .local v0, "isDataChanged":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 278
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->notifyDirty()V

    .line 279
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->TAG:Ljava/lang/String;

    const-string v4, "makeBaseItemList start"

    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->makeBaseItemList()V

    .line 281
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mClusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getDataVersion()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataVersion:J

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->clusteringSearchResult()Z

    .line 285
    if-nez v0, :cond_1

    .line 286
    invoke-static {}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataVersion:J

    .line 288
    :cond_1
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataVersion:J

    return-wide v4

    .line 276
    .end local v0    # "isDataChanged":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "filterType"    # Ljava/lang/String;
    .param p2, "tagType"    # Ljava/lang/String;

    .prologue
    .line 615
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mTagFilterList:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->removeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->notifyDirty()V

    .line 617
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mName:Ljava/lang/String;

    .line 219
    return-void
.end method

.method public setSortByOldest(Z)V
    .locals 0
    .param p1, "isOldest"    # Z

    .prologue
    .line 140
    sput-boolean p1, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mSortByOldest:Z

    .line 141
    return-void
.end method

.method public updateDataVersion()V
    .locals 2

    .prologue
    .line 531
    invoke-static {}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataVersion:J

    .line 532
    return-void
.end method

.method public updateFaces(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "joinedName"    # Ljava/lang/String;

    .prologue
    .line 513
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/access/face/PersonList;->addPerson(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 514
    .local v0, "personId":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1, p1, p2, v0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateSelectedFaces(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    .line 515
    return-void
.end method

.method public updateMediaItems(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 535
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v8, 0x0

    .line 536
    .local v8, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v7, 0x0

    .local v7, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .local v9, "n":I
    :goto_0
    if-ge v7, v9, :cond_2

    .line 538
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_1

    .line 539
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 540
    .restart local v8    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v6, 0x0

    .line 541
    .local v6, "cursor":Landroid/database/Cursor;
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget v2, v8, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 542
    .local v1, "uri":Landroid/net/Uri;
    const-string v5, "datetaken DESC, _id DESC"

    .line 544
    .local v5, "strOrderClause":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 546
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/data/LocalImage;->updateContent(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 536
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v5    # "strOrderClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 549
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v5    # "strOrderClause":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 552
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v5    # "strOrderClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    return-void
.end method

.method public updateMediaSet()V
    .locals 0

    .prologue
    .line 525
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->updateMediaSet()V

    .line 526
    invoke-static {}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->setVersion()V

    .line 527
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->onContentDirty()V

    .line 528
    return-void
.end method

.method public updateOneFace(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "personId"    # I

    .prologue
    .line 518
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 519
    .local v0, "pathS":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 520
    .local v1, "values":[Ljava/lang/String;
    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {p1, v2, p3}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPerson(Landroid/content/Context;II)V

    .line 521
    return-void
.end method

.method public updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 8
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "operationType"    # I
    .param p4, "joinedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 370
    .local p2, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const-string v0, "reload"

    const-string v1, "clusteralbum showProcessingDialog"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    .line 372
    .local v4, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v7

    new-instance v0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mName:Ljava/lang/String;

    move-object v1, p0

    move-object v3, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$UpdateOperation;-><init>(Lcom/sec/android/gallery3d/data/GallerySearchAlbum;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Context;ILjava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 374
    return-void
.end method

.method public updateSlotName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "joinedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 363
    .local p2, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p3, :cond_0

    .line 364
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mName:Ljava/lang/String;

    .line 365
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;)V

    .line 366
    return-void
.end method
