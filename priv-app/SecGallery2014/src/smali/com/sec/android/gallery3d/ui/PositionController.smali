.class public Lcom/sec/android/gallery3d/ui/PositionController;
.super Ljava/lang/Object;
.source "PositionController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/PositionController$1;,
        Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;,
        Lcom/sec/android/gallery3d/ui/PositionController$Gap;,
        Lcom/sec/android/gallery3d/ui/PositionController$Box;,
        Lcom/sec/android/gallery3d/ui/PositionController$Platform;,
        Lcom/sec/android/gallery3d/ui/PositionController$Animatable;,
        Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;,
        Lcom/sec/android/gallery3d/ui/PositionController$AnimationListener;,
        Lcom/sec/android/gallery3d/ui/PositionController$Listener;
    }
.end annotation


# static fields
.field public static final ANIM_KIND_CAPTURE:I = 0xa

.field public static final ANIM_KIND_DELETE:I = 0x9

.field public static final ANIM_KIND_FLING:I = 0x6

.field public static final ANIM_KIND_FLING_X:I = 0x8

.field public static final ANIM_KIND_NONE:I = -0x1

.field public static final ANIM_KIND_OPENING:I = 0x5

.field public static final ANIM_KIND_PANNING_SCROLL:I = 0xb

.field public static final ANIM_KIND_PANORAMA_SCROLL:I = 0xc

.field public static final ANIM_KIND_SCALE:I = 0x1

.field public static final ANIM_KIND_SCROLL:I = 0x0

.field public static final ANIM_KIND_SLIDE:I = 0x3

.field public static final ANIM_KIND_SNAPBACK:I = 0x2

.field public static final ANIM_KIND_ZOOM:I = 0x4

.field private static final ANIM_TIME:[I

.field private static final BOX_MAX:I = 0x3

.field public static final CAPTURE_ANIMATION_TIME:I = 0x2bc

.field private static final CENTER_OUT_INDEX:[I

.field private static final DEFAULT_DELETE_ANIMATION_DURATION:I = 0xc8

.field private static final FILM_MODE_LANDSCAPE_HEIGHT:F = 0.7f

.field private static final FILM_MODE_LANDSCAPE_WIDTH:F = 0.7f

.field private static final FILM_MODE_PORTRAIT_HEIGHT:F = 0.48f

.field private static final FILM_MODE_PORTRAIT_WIDTH:F = 0.7f

.field private static final HORIZONTAL_SLACK:I = 0x0

.field public static final IMAGE_AT_BOTTOM_EDGE:I = 0x8

.field public static final IMAGE_AT_LEFT_EDGE:I = 0x1

.field public static final IMAGE_AT_RIGHT_EDGE:I = 0x2

.field public static final IMAGE_AT_TOP_EDGE:I = 0x4

.field private static final IMAGE_GAP:I

.field private static final INVALID_ANIM_DURATION:I = -0x1

.field private static final LAST_ANIMATION:J = -0x2L

.field private static final MAX_DELETE_ANIMATION_DURATION:I = 0x190

.field private static final NO_ANIMATION:J = -0x1L

.field private static final PANORAMA_MAX_ANIM_TIME:I = 0x9c40

.field private static final PANORAMA_MAX_RATIO:F = 5.8f

.field private static final PANORAMA_MIN_ANIM_TIME:I = 0x3a98

.field private static final PANORAMA_MIN_RATIO:F = 2.7f

.field private static SCALE_LIMIT:F = 0.0f

.field private static final SCALE_MAX_EXTRA:F = 1.0f

.field private static final SCALE_MIN_EXTRA:F = 0.999f

.field public static final SNAPBACK_ANIMATION_TIME:I = 0x258

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field private mAnimationListener:Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

.field private mBoundBottom:I

.field private mBoundLeft:I

.field private mBoundRight:I

.field private mBoundTop:I

.field private mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/RangeArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/PositionController$Box;",
            ">;"
        }
    .end annotation
.end field

.field private mConstrained:Z

.field private mConstrainedFrame:Landroid/graphics/Rect;

.field private mExtraScalingRange:Z

.field private mFilmMode:Z

.field private mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

.field private mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;

.field private mFocusX:F

.field private mFocusY:F

.field private mGaps:Lcom/sec/android/gallery3d/util/RangeArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/RangeArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/PositionController$Gap;",
            ">;"
        }
    .end annotation
.end field

.field private mHasNext:Z

.field private mHasPrev:Z

.field private mInScale:Z

.field private mIsScalePrepare:Z

.field private mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

.field private volatile mOpenAnimationRect:Landroid/graphics/Rect;

.field private mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;

.field private mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

.field mPopFromTop:Z

.field private mRects:Lcom/sec/android/gallery3d/util/RangeArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/RangeArray",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mTempBoxes:Lcom/sec/android/gallery3d/util/RangeArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/RangeArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/PositionController$Box;",
            ">;"
        }
    .end annotation
.end field

.field private mTempGaps:Lcom/sec/android/gallery3d/util/RangeArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/RangeArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/PositionController$Gap;",
            ">;"
        }
    .end annotation
.end field

.field private mViewH:I

.field private mViewW:I

.field private mViewer:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const-class v2, Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->TAG:Ljava/lang/String;

    .line 71
    const/16 v2, 0xd

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    sput-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->ANIM_TIME:[I

    .line 95
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCustomMaxZoomLevel:Z

    if-eqz v2, :cond_1

    const v2, 0x41555556

    :goto_0
    sput v2, Lcom/sec/android/gallery3d/ui/PositionController;->SCALE_LIMIT:F

    .line 119
    const/4 v2, 0x7

    new-array v2, v2, [I

    sput-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->CENTER_OUT_INDEX:[I

    .line 121
    const/16 v2, 0x10

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v2

    sput v2, Lcom/sec/android/gallery3d/ui/PositionController;->IMAGE_GAP:I

    .line 230
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->CENTER_OUT_INDEX:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 231
    add-int/lit8 v2, v0, 0x1

    div-int/lit8 v1, v2, 0x2

    .line 232
    .local v1, "j":I
    and-int/lit8 v2, v0, 0x1

    if-nez v2, :cond_0

    neg-int v1, v1

    .line 233
    :cond_0
    sget-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->CENTER_OUT_INDEX:[I

    aput v1, v2, v0

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 95
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_1
    const/high16 v2, 0x41200000    # 10.0f

    goto :goto_0

    .line 235
    .restart local v0    # "i":I
    :cond_2
    return-void

    .line 71
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x258
        0xc8
        0x96
        0x12c
        0x0
        0x0
        0x0
        0x0
        0x2bc
        0x190
        0x9c40
    .end array-data
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PositionController$Listener;)V
    .locals 9
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "viewer"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p4, "listener"    # Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, -0x3

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mExtraScalingRange:Z

    .line 108
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    .line 132
    const/16 v3, 0x4b0

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    .line 133
    const/16 v3, 0x4b0

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    .line 165
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    .line 172
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrained:Z

    .line 187
    new-instance v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-direct {v3, p0, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;-><init>(Lcom/sec/android/gallery3d/ui/PositionController;Lcom/sec/android/gallery3d/ui/PositionController$1;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 188
    new-instance v3, Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/util/RangeArray;-><init>(II)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    .line 191
    new-instance v3, Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-direct {v3, v4, v8}, Lcom/sec/android/gallery3d/util/RangeArray;-><init>(II)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    .line 192
    new-instance v3, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    invoke-direct {v3, p0, v6}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;-><init>(Lcom/sec/android/gallery3d/ui/PositionController;Lcom/sec/android/gallery3d/ui/PositionController$1;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    .line 195
    new-instance v3, Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/util/RangeArray;-><init>(II)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    .line 196
    new-instance v3, Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-direct {v3, v4, v8}, Lcom/sec/android/gallery3d/util/RangeArray;-><init>(II)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    .line 200
    new-instance v3, Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/util/RangeArray;-><init>(II)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    .line 206
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mIsScalePrepare:Z

    .line 262
    iput-object p4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    .line 263
    iput-object p3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewer:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 264
    new-instance v3, Lcom/sec/android/gallery3d/ui/FlingScroller;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/ui/FlingScroller;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;

    .line 265
    new-instance v3, Lcom/sec/android/gallery3d/common/Scroller;

    invoke-direct {v3, p2, v6, v7}, Lcom/sec/android/gallery3d/common/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;

    .line 266
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 269
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    if-eqz v3, :cond_0

    .line 270
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->initPlatform()V

    .line 271
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iput-object v4, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 273
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    if-eqz v3, :cond_1

    .line 274
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iput-object v4, v3, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 276
    :cond_1
    const/4 v2, -0x3

    .local v2, "i":I
    :goto_0
    if-gt v2, v5, :cond_2

    .line 277
    new-instance v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    invoke-direct {v0, p0, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Box;-><init>(Lcom/sec/android/gallery3d/ui/PositionController;Lcom/sec/android/gallery3d/ui/PositionController$1;)V

    .line 278
    .local v0, "box":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 279
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->initBox(I)V

    .line 280
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iput-object v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 281
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 276
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 284
    .end local v0    # "box":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    :cond_2
    const/4 v2, -0x3

    :goto_1
    if-ge v2, v5, :cond_3

    .line 285
    new-instance v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    invoke-direct {v1, p0, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Gap;-><init>(Lcom/sec/android/gallery3d/ui/PositionController;Lcom/sec/android/gallery3d/ui/PositionController$1;)V

    .line 286
    .local v1, "gap":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 287
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->initGap(I)V

    .line 288
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iput-object v3, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 284
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 290
    .end local v1    # "gap":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    :cond_3
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/PositionController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mExtraScalingRange:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/PositionController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/ui/PositionController;FI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;
    .param p1, "x1"    # F
    .param p2, "x2"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(FI)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/ui/PositionController;F)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;
    .param p1, "x1"    # F

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->viewWiderThanScaledImage(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/ui/PositionController;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusX:F

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/ui/PositionController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/gallery3d/ui/PositionController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/gallery3d/ui/PositionController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrained:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/ui/PositionController;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/gallery3d/ui/PositionController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/ui/PositionController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    return v0
.end method

.method static synthetic access$2100()[I
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/gallery3d/ui/PositionController;->ANIM_TIME:[I

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/common/Scroller;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/gallery3d/ui/PositionController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasNext:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/gallery3d/ui/PositionController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasPrev:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/FlingScroller;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/gallery3d/ui/PositionController;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;
    .param p1, "x1"    # F

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(F)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/gallery3d/ui/PositionController;F)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;
    .param p1, "x1"    # F

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->viewTallerThanScaledImage(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/gallery3d/ui/PositionController;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusY:F

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/gallery3d/ui/PositionController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/gallery3d/ui/PositionController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/PositionController$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/PositionController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mInScale:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/util/RangeArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    return-object v0
.end method

.method private calculateStableBound(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    .line 1525
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(FI)V

    .line 1526
    return-void
.end method

.method private calculateStableBound(FI)V
    .locals 6
    .param p1, "scale"    # F
    .param p2, "horizontalSlack"    # I

    .prologue
    const/4 v5, 0x0

    .line 1500
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1503
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;F)I

    move-result v2

    .line 1504
    .local v2, "w":I
    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;F)I

    move-result v1

    .line 1507
    .local v1, "h":I
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    add-int/lit8 v3, v3, 0x1

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v4, v2, 0x1

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    sub-int/2addr v3, p2

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    .line 1508
    div-int/lit8 v3, v2, 0x2

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    add-int/2addr v3, p2

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    .line 1509
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    add-int/lit8 v3, v3, 0x1

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v4, v1, 0x1

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    .line 1510
    div-int/lit8 v3, v1, 0x2

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    .line 1514
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->viewTallerThanScaledImage(F)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1515
    iput v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    iput v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    .line 1519
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->viewWiderThanScaledImage(F)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1520
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v3, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    .line 1522
    :cond_1
    return-void
.end method

.method private canScroll()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 684
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 685
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-wide v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 692
    :goto_0
    :sswitch_0
    return v1

    .line 686
    :cond_0
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    sparse-switch v3, :sswitch_data_0

    move v1, v2

    .line 692
    goto :goto_0

    .line 686
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_0
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method private convertBoxToRect(I)V
    .locals 10
    .param p1, "i"    # I

    .prologue
    .line 1062
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1063
    .local v1, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 1064
    .local v4, "r":Landroid/graphics/Rect;
    iget v8, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v9, v9, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    add-int/2addr v8, v9

    iget v9, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    div-int/lit8 v9, v9, 0x2

    add-int v7, v8, v9

    .line 1065
    .local v7, "y":I
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v5

    .line 1066
    .local v5, "w":I
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v3

    .line 1067
    .local v3, "h":I
    if-nez p1, :cond_0

    .line 1068
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v8, v8, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget v9, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    div-int/lit8 v9, v9, 0x2

    add-int v6, v8, v9

    .line 1069
    .local v6, "x":I
    div-int/lit8 v8, v5, 0x2

    sub-int v8, v6, v8

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 1070
    iget v8, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v5

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 1082
    .end local v6    # "x":I
    :goto_0
    div-int/lit8 v8, v3, 0x2

    sub-int v8, v7, v8

    iput v8, v4, Landroid/graphics/Rect;->top:I

    .line 1083
    iget v8, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v8, v3

    iput v8, v4, Landroid/graphics/Rect;->bottom:I

    .line 1084
    return-void

    .line 1071
    :cond_0
    if-lez p1, :cond_1

    .line 1072
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    add-int/lit8 v9, p1, -0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1073
    .local v0, "a":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    add-int/lit8 v9, p1, -0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 1074
    .local v2, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget v8, v0, Landroid/graphics/Rect;->right:I

    iget v9, v2, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    add-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->left:I

    .line 1075
    iget v8, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v5

    iput v8, v4, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 1077
    .end local v0    # "a":Landroid/graphics/Rect;
    .end local v2    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    add-int/lit8 v9, p1, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1078
    .restart local v0    # "a":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 1079
    .restart local v2    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget v8, v0, Landroid/graphics/Rect;->left:I

    iget v9, v2, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    sub-int/2addr v8, v9

    iput v8, v4, Landroid/graphics/Rect;->right:I

    .line 1080
    iget v8, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v5

    iput v8, v4, Landroid/graphics/Rect;->left:I

    goto :goto_0
.end method

.method private debugMoveBox([I)V
    .locals 5
    .param p1, "fromIndex"    # [I

    .prologue
    .line 1154
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "moveBox:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1155
    .local v2, "s":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 1156
    aget v1, p1, v0

    .line 1157
    .local v1, "j":I
    const v3, 0x7fffffff

    if-ne v1, v3, :cond_0

    .line 1158
    const-string v3, " N"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1155
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1160
    :cond_0
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1161
    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1164
    .end local v1    # "j":I
    :cond_1
    sget-object v3, Lcom/sec/android/gallery3d/ui/PositionController;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    return-void
.end method

.method private dumpRect(I)V
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 1046
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1047
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1048
    .local v0, "r":Landroid/graphics/Rect;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rect "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1049
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1050
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1051
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1052
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1053
    const-string v2, ") ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1054
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1055
    const-string/jumbo v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1056
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1057
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1058
    sget-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1059
    return-void
.end method

.method private dumpState()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 1028
    const/4 v0, -0x3

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 1029
    sget-object v3, Lcom/sec/android/gallery3d/ui/PositionController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Gap "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    iget v2, v2, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1032
    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x7

    if-ge v0, v2, :cond_1

    .line 1033
    sget-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->CENTER_OUT_INDEX:[I

    aget v2, v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->dumpRect(I)V

    .line 1032
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1036
    :cond_1
    const/4 v0, -0x3

    :goto_2
    if-gt v0, v5, :cond_4

    .line 1037
    add-int/lit8 v1, v0, 0x1

    .local v1, "j":I
    :goto_3
    if-gt v1, v5, :cond_3

    .line 1038
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-static {v2, v3}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1039
    sget-object v2, Lcom/sec/android/gallery3d/ui/PositionController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and rect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "intersects!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1036
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1043
    .end local v1    # "j":I
    :cond_4
    return-void
.end method

.method private gapToSide(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I
    .locals 3
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .prologue
    .line 508
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    int-to-float v0, v0

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v1

    iget v2, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private getDefaultGapSize(I)I
    .locals 5
    .param p1, "i"    # I

    .prologue
    .line 489
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/sec/android/gallery3d/ui/PositionController;->IMAGE_GAP:I

    .line 492
    :goto_0
    return v2

    .line 490
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 491
    .local v0, "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 492
    .local v1, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    sget v2, Lcom/sec/android/gallery3d/ui/PositionController;->IMAGE_GAP:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->gapToSide(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v3

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->gapToSide(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0
.end method

.method private getMaximalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F
    .locals 1
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .prologue
    .line 1473
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v0

    .line 1475
    :goto_0
    return v0

    .line 1474
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrained:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v0

    goto :goto_0

    .line 1475
    :cond_1
    sget v0, Lcom/sec/android/gallery3d/ui/PositionController;->SCALE_LIMIT:F

    goto :goto_0
.end method

.method private getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F
    .locals 8
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .prologue
    .line 1444
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1445
    .local v4, "wFactor":F
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1448
    .local v0, "hFactor":F
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-nez v5, :cond_1

    iget-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrained:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-ne p1, v5, :cond_1

    .line 1450
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 1451
    .local v3, "viewW":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 1457
    .local v2, "viewH":I
    :goto_0
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-eqz v5, :cond_0

    .line 1458
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    if-le v5, v6, :cond_2

    .line 1459
    const v4, 0x3f333333    # 0.7f

    .line 1460
    const v0, 0x3ef5c28f    # 0.48f

    .line 1467
    :cond_0
    :goto_1
    int-to-float v5, v3

    mul-float/2addr v5, v4

    iget v6, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    int-to-float v6, v2

    mul-float/2addr v6, v0

    iget v7, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1469
    .local v1, "s":F
    sget v5, Lcom/sec/android/gallery3d/ui/PositionController;->SCALE_LIMIT:F

    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    move-result v5

    return v5

    .line 1453
    .end local v1    # "s":F
    .end local v2    # "viewH":I
    .end local v3    # "viewW":I
    :cond_1
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    .line 1454
    .restart local v3    # "viewW":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    .restart local v2    # "viewH":I
    goto :goto_0

    .line 1462
    :cond_2
    const v4, 0x3f333333    # 0.7f

    .line 1463
    const v0, 0x3f333333    # 0.7f

    goto :goto_1
.end method

.method private getPanoramaAnimDuration(F)I
    .locals 5
    .param p1, "ratio"    # F

    .prologue
    .line 2135
    const/16 v2, 0x61a8

    .line 2136
    .local v2, "timeGap":I
    const v1, 0x40466667    # 3.1000001f

    .line 2137
    .local v1, "ratioGap":F
    int-to-float v3, v2

    div-float/2addr v3, v1

    const v4, 0x402ccccd    # 2.7f

    sub-float v4, p1, v4

    mul-float/2addr v3, v4

    const v4, 0x466a6000    # 15000.0f

    add-float/2addr v3, v4

    float-to-int v0, v3

    .line 2138
    .local v0, "duration":I
    const v3, 0x9c40

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    return v3
.end method

.method public static getScaleLimit()F
    .locals 1

    .prologue
    .line 2110
    sget v0, Lcom/sec/android/gallery3d/ui/PositionController;->SCALE_LIMIT:F

    return v0
.end method

.method private getTargetScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F
    .locals 4
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .prologue
    .line 1537
    iget-wide v0, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mToScale:F

    goto :goto_0
.end method

.method private heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I
    .locals 2
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .prologue
    .line 1001
    iget v0, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    int-to-float v0, v0

    iget v1, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;F)I
    .locals 2
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .param p2, "scale"    # F

    .prologue
    .line 1011
    iget v0, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    int-to-float v0, v0

    mul-float/2addr v0, p2

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private initBox(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 1106
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1107
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    .line 1108
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    .line 1109
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mUseViewSize:Z

    .line 1110
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    .line 1111
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMaximalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMax:F

    .line 1112
    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    .line 1113
    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 1114
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    .line 1115
    const/4 v1, -0x1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    .line 1116
    return-void
.end method

.method private initBox(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "size"    # Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    .prologue
    const/4 v2, 0x0

    .line 1120
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    if-nez v1, :cond_1

    .line 1121
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->initBox(I)V

    .line 1134
    :goto_0
    return-void

    .line 1124
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1125
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    .line 1126
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    .line 1127
    iput-boolean v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mUseViewSize:Z

    .line 1128
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    .line 1129
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMaximalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMax:F

    .line 1130
    iput v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    .line 1131
    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 1132
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    .line 1133
    const/4 v1, -0x1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    goto :goto_0
.end method

.method private initGap(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 1139
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 1140
    .local v0, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->getDefaultGapSize(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mDefaultSize:I

    .line 1141
    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mDefaultSize:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    .line 1142
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mAnimationStartTime:J

    .line 1143
    return-void
.end method

.method private initGap(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "size"    # I

    .prologue
    .line 1146
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 1147
    .local v0, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->getDefaultGapSize(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mDefaultSize:I

    .line 1148
    iput p2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    .line 1149
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mAnimationStartTime:J

    .line 1150
    return-void
.end method

.method private initPlatform()V
    .locals 4

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->updateDefaultXY()V

    .line 1098
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1099
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultY:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    .line 1100
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    .line 1101
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    const/4 v1, -0x1

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    .line 1102
    return-void
.end method

.method private static isAlmostEqual(FF)Z
    .locals 2
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 1479
    sub-float v0, p0, p1

    .line 1480
    .local v0, "diff":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    neg-float v0, v0

    .end local v0    # "diff":F
    :cond_0
    const v1, 0x3ca3d70a    # 0.02f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private layoutAndSetPosition()V
    .locals 2

    .prologue
    .line 1020
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 1021
    sget-object v1, Lcom/sec/android/gallery3d/ui/PositionController;->CENTER_OUT_INDEX:[I

    aget v1, v1, v0

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->convertBoxToRect(I)V

    .line 1020
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1024
    :cond_0
    return-void
.end method

.method private redraw()V
    .locals 1

    .prologue
    .line 918
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->layoutAndSetPosition()V

    .line 919
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->invalidate()V

    .line 920
    return-void
.end method

.method private scrollPage(IIII)V
    .locals 10
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "kind"    # I
    .param p4, "duration"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 708
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->canScroll()Z

    move-result v6

    if-nez v6, :cond_0

    .line 753
    .end local p1    # "dx":I
    :goto_0
    return-void

    .line 710
    .restart local p1    # "dx":I
    :cond_0
    const/4 v6, -0x1

    if-eq p4, v6, :cond_1

    .line 711
    sget-object v6, Lcom/sec/android/gallery3d/ui/PositionController;->ANIM_TIME:[I

    aput p4, v6, p3

    .line 714
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v6, v5}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 715
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 717
    .local v1, "p":Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    iget v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(F)V

    .line 719
    iget v6, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v7

    if-nez v7, :cond_2

    iget v7, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-direct {p0, v7}, Lcom/sec/android/gallery3d/ui/PositionController;->viewWiderThanScaledImage(F)Z

    move-result v7

    if-eqz v7, :cond_2

    move p1, v5

    .end local p1    # "dx":I
    :cond_2
    add-int v3, v6, p1

    .line 720
    .local v3, "x":I
    iget v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    add-int v4, v6, p2

    .line 724
    .local v4, "y":I
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    if-eq v6, v7, :cond_3

    .line 725
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    if-ge v4, v6, :cond_5

    .line 726
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    sub-int/2addr v7, v4

    const/4 v8, 0x2

    invoke-interface {v6, v7, v8}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onPull(II)V

    .line 732
    :cond_3
    :goto_1
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    invoke-static {v4, v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v4

    .line 736
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasPrev:Z

    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v6

    if-nez v6, :cond_6

    :cond_4
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    if-le v3, v6, :cond_6

    .line 737
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    sub-int v2, v3, v6

    .line 738
    .local v2, "pixels":I
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    invoke-interface {v6, v2, v9}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onPull(II)V

    .line 739
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 740
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    .line 741
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    .line 752
    .end local v2    # "pixels":I
    :goto_2
    iget v5, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-direct {p0, v3, v4, v5, p3}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    goto :goto_0

    .line 727
    :cond_5
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    if-le v4, v6, :cond_3

    .line 728
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    sub-int v7, v4, v7

    invoke-interface {v6, v7, v5}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onPull(II)V

    goto :goto_1

    .line 742
    :cond_6
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasNext:Z

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v6

    if-nez v6, :cond_8

    :cond_7
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    if-ge v3, v6, :cond_8

    .line 743
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    sub-int v2, v6, v3

    .line 744
    .restart local v2    # "pixels":I
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    const/4 v7, 0x3

    invoke-interface {v6, v2, v7}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onPull(II)V

    .line 745
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setImageChanging(Z)V

    .line 746
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    .line 747
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    .line 748
    goto :goto_2

    .line 749
    .end local v2    # "pixels":I
    :cond_8
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v5, v9}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    goto :goto_2
.end method

.method private setBoxSize(IIIZ)Z
    .locals 6
    .param p1, "i"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "isViewSize"    # Z

    .prologue
    const/4 v3, 0x0

    .line 373
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 374
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-boolean v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mUseViewSize:Z

    .line 377
    .local v2, "wasViewSize":Z
    if-nez v2, :cond_1

    if-eqz p4, :cond_1

    .line 422
    :cond_0
    :goto_0
    return v3

    .line 379
    :cond_1
    iput-boolean p4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mUseViewSize:Z

    .line 381
    iget v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    if-ne p2, v4, :cond_2

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    if-eq p3, v4, :cond_0

    .line 392
    :cond_2
    if-le p2, p3, :cond_6

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    int-to-float v3, v3

    int-to-float v4, p2

    div-float v1, v3, v4

    .line 396
    .local v1, "ratio":F
    :goto_1
    iput p2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    .line 397
    iput p3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    .line 402
    if-eqz v2, :cond_3

    if-eqz p4, :cond_4

    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-nez v3, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->isPanoramaScrolling()Z

    move-result v3

    if-nez v3, :cond_8

    .line 403
    :cond_4
    if-nez p1, :cond_7

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mIsScalePrepare:Z

    if-nez v3, :cond_7

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_7

    .line 404
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    mul-float/2addr v3, v1

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 405
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mFromScale:F

    mul-float/2addr v3, v1

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mFromScale:F

    .line 406
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mToScale:F

    mul-float/2addr v3, v1

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mToScale:F

    .line 417
    :goto_2
    if-nez p1, :cond_5

    .line 418
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusX:F

    div-float/2addr v3, v1

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusX:F

    .line 419
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusY:F

    div-float/2addr v3, v1

    iput v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusY:F

    .line 422
    :cond_5
    const/4 v3, 0x1

    goto :goto_0

    .line 392
    .end local v1    # "ratio":F
    :cond_6
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    int-to-float v3, v3

    int-to-float v4, p3

    div-float v1, v3, v4

    goto :goto_1

    .line 408
    .restart local v1    # "ratio":F
    :cond_7
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v3

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 409
    const-wide/16 v4, -0x1

    iput-wide v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    goto :goto_2

    .line 412
    :cond_8
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    mul-float/2addr v3, v1

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 413
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mFromScale:F

    mul-float/2addr v3, v1

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mFromScale:F

    .line 414
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mToScale:F

    mul-float/2addr v3, v1

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mToScale:F

    goto :goto_2
.end method

.method private snapAndRedraw()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 923
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->startSnapback()Z

    .line 924
    const/4 v0, -0x3

    .local v0, "i":I
    :goto_0
    if-gt v0, v2, :cond_0

    .line 925
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->startSnapback()Z

    .line 924
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 927
    :cond_0
    const/4 v0, -0x3

    :goto_1
    if-ge v0, v2, :cond_1

    .line 928
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->startSnapback()Z

    .line 927
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 930
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->startSnapback()Z

    .line 931
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    .line 932
    return-void
.end method

.method private snapAndRedraw(I)V
    .locals 3
    .param p1, "kind"    # I

    .prologue
    const/4 v2, 0x3

    .line 2122
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->startSnapback(I)Z

    .line 2123
    const/4 v0, -0x3

    .local v0, "i":I
    :goto_0
    if-gt v0, v2, :cond_0

    .line 2124
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->startSnapback(I)Z

    .line 2123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2126
    :cond_0
    const/4 v0, -0x3

    :goto_1
    if-ge v0, v2, :cond_1

    .line 2127
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->startSnapback(I)Z

    .line 2126
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2129
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->startSnapback(I)Z

    .line 2130
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    .line 2131
    return-void
.end method

.method private startAnimation(IIFI)Z
    .locals 3
    .param p1, "targetX"    # I
    .param p2, "targetY"    # I
    .param p3, "targetScale"    # F
    .param p4, "kind"    # I

    .prologue
    .line 936
    const/4 v0, 0x0

    .line 937
    .local v0, "changed":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v2, v2, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultY:I

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController$Platform;->doAnimation(III)Z
    invoke-static {v1, p1, v2, p4}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->access$400(Lcom/sec/android/gallery3d/ui/PositionController$Platform;III)Z

    move-result v1

    or-int/2addr v0, v1

    .line 938
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController$Box;->doAnimation(IFI)Z
    invoke-static {v1, p2, p3, p4}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->access$500(Lcom/sec/android/gallery3d/ui/PositionController$Box;IFI)Z

    move-result v1

    or-int/2addr v0, v1

    .line 939
    if-eqz v0, :cond_1

    .line 940
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    .line 946
    :cond_0
    :goto_0
    return v0

    .line 942
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationListener:Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    if-eqz v1, :cond_0

    .line 943
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationListener:Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;->onAnimationEnd()V

    goto :goto_0
.end method

.method private startOpeningAnimationIfNeeded()Z
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 426
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mOpenAnimationRect:Landroid/graphics/Rect;

    if-nez v6, :cond_1

    .line 449
    :cond_0
    :goto_0
    return v4

    .line 427
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v6, v4}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 428
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-boolean v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mUseViewSize:Z

    if-nez v6, :cond_0

    .line 431
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mOpenAnimationRect:Landroid/graphics/Rect;

    .line 432
    .local v3, "r":Landroid/graphics/Rect;
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mOpenAnimationRect:Landroid/graphics/Rect;

    .line 434
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iput v7, v6, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 435
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    iput v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    .line 436
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    iget v7, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 438
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v6, v6, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iget v7, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    invoke-direct {p0, v6, v4, v7, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 443
    const/4 v2, -0x1

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_2

    .line 444
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 445
    .local v1, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    iput v4, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    .line 446
    iget v4, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mDefaultSize:I

    invoke-virtual {v1, v4, v9}, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->doAnimation(II)Z

    .line 443
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v1    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    :cond_2
    move v4, v5

    .line 449
    goto :goto_0
.end method

.method private updateScaleAndGapLimit()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 474
    const/4 v2, -0x3

    .local v2, "i":I
    :goto_0
    if-gt v2, v4, :cond_0

    .line 475
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 476
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v3

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    .line 477
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMaximalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v3

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMax:F

    .line 474
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 480
    .end local v0    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    :cond_0
    const/4 v2, -0x3

    :goto_1
    if-ge v2, v4, :cond_1

    .line 481
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 482
    .local v1, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->getDefaultGapSize(I)I

    move-result v3

    iput v3, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mDefaultSize:I

    .line 480
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 484
    .end local v1    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    :cond_1
    return-void
.end method

.method private viewTallerThanScaledImage(F)Z
    .locals 3
    .param p1, "scale"    # F

    .prologue
    const/4 v1, 0x0

    .line 1529
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;F)I

    move-result v0

    if-lt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private viewWiderThanScaledImage(F)Z
    .locals 3
    .param p1, "scale"    # F

    .prologue
    const/4 v1, 0x0

    .line 1533
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;F)I

    move-result v0

    if-lt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I
    .locals 2
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .prologue
    .line 996
    iget v0, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    int-to-float v0, v0

    iget v1, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;F)I
    .locals 2
    .param p1, "b"    # Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .param p2, "scale"    # F

    .prologue
    .line 1006
    iget v0, p1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    int-to-float v0, v0

    mul-float/2addr v0, p2

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public advanceAnimation()Z
    .locals 8

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x0

    .line 952
    const/4 v0, 0x0

    .line 953
    .local v0, "changed":Z
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->advanceAnimation()Z

    move-result v3

    or-int/2addr v0, v3

    .line 955
    const/4 v2, -0x3

    .local v2, "i":I
    :goto_0
    if-gt v2, v6, :cond_0

    .line 956
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->advanceAnimation()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    or-int/2addr v0, v3

    .line 955
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 958
    :catch_0
    move-exception v1

    .line 959
    .local v1, "e":Ljava/lang/NullPointerException;
    sget-object v3, Lcom/sec/android/gallery3d/ui/PositionController;->TAG:Ljava/lang/String;

    const-string v5, "NullPointerException at advanceAnimation"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const/4 v2, -0x3

    :goto_1
    if-ge v2, v6, :cond_3

    .line 962
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_1
    move v3, v4

    .line 979
    :goto_2
    return v3

    .line 964
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->advanceAnimation()Z

    move-result v3

    or-int/2addr v0, v3

    .line 961
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 966
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->advanceAnimation()Z

    move-result v3

    or-int/2addr v0, v3

    .line 968
    if-eqz v0, :cond_5

    .line 969
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkAnimation(Z)V

    .line 970
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    .line 976
    :goto_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    const-wide/16 v6, -0x2

    cmp-long v3, v4, v6

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationListener:Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    if-eqz v3, :cond_4

    .line 977
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationListener:Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;->onAnimationEnd()V

    :cond_4
    move v3, v0

    .line 979
    goto :goto_2

    .line 972
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkAnimation(Z)V

    goto :goto_3
.end method

.method public beginScale(FF)V
    .locals 5
    .param p1, "focusX"    # F
    .param p2, "focusY"    # F

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 602
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr p1, v2

    .line 603
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr p2, v2

    .line 604
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 605
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 606
    .local v1, "p":Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mInScale:Z

    .line 607
    iget v2, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    int-to-float v2, v2

    sub-float v2, p1, v2

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    div-float/2addr v2, v3

    add-float/2addr v2, v4

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusX:F

    .line 608
    iget v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    int-to-float v2, v2

    sub-float v2, p2, v2

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    div-float/2addr v2, v3

    add-float/2addr v2, v4

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusY:F

    .line 609
    return-void
.end method

.method public endScale()V
    .locals 1

    .prologue
    .line 654
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mInScale:Z

    .line 655
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw()V

    .line 656
    return-void
.end method

.method public existAnimationListener()Z
    .locals 1

    .prologue
    .line 2118
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationListener:Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fitViewToImageLeft()V
    .locals 4

    .prologue
    .line 2090
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    sget v2, Lcom/sec/android/gallery3d/ui/PositionController;->SCALE_LIMIT:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2091
    .local v0, "scale":F
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 2092
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->skipAnimation()V

    .line 2093
    return-void
.end method

.method public fitViewToImageTop()V
    .locals 4

    .prologue
    .line 2096
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    sget v2, Lcom/sec/android/gallery3d/ui/PositionController;->SCALE_LIMIT:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2097
    .local v0, "scale":F
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 2098
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->skipAnimation()V

    .line 2099
    return-void
.end method

.method public flingFilmX(I)Z
    .locals 13
    .param p1, "velocityX"    # I

    .prologue
    const/4 v2, 0x0

    .line 832
    if-nez p1, :cond_1

    .line 847
    :cond_0
    :goto_0
    return v2

    .line 834
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 835
    .local v9, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 838
    .local v11, "p":Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    iget v10, v11, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    .line 839
    .local v10, "defaultX":I
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasPrev:Z

    if-nez v0, :cond_2

    iget v0, v11, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    if-ge v0, v10, :cond_0

    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasNext:Z

    if-nez v0, :cond_3

    iget v0, v11, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    if-le v0, v10, :cond_0

    .line 844
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;

    iget v1, v11, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    move v3, p1

    move v4, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/gallery3d/common/Scroller;->fling(IIIIIIII)V

    .line 846
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/common/Scroller;->getFinalX()I

    move-result v12

    .line 847
    .local v12, "targetX":I
    iget v0, v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    iget v1, v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    const/16 v2, 0x8

    invoke-direct {p0, v12, v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    move-result v2

    goto :goto_0
.end method

.method public flingFilmY(II)I
    .locals 8
    .param p1, "boxIndex"    # I
    .param p2, "velocityY"    # I

    .prologue
    const/16 v7, 0x9

    .line 855
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v5, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 858
    .local v1, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v3

    .line 860
    .local v3, "h":I
    const/4 v0, 0x3

    .line 861
    .local v0, "FUZZY":I
    if-ltz p2, :cond_0

    if-nez p2, :cond_1

    iget v5, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    if-gtz v5, :cond_1

    .line 862
    :cond_0
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    neg-int v5, v5

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v6, v3, 0x1

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    sub-int v4, v5, v0

    .line 869
    .local v4, "targetY":I
    :goto_0
    if-eqz p2, :cond_2

    .line 870
    iget v5, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    sub-int v5, v4, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v6

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    float-to-int v2, v5

    .line 872
    .local v2, "duration":I
    const/16 v5, 0x190

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 878
    :goto_1
    sget-object v5, Lcom/sec/android/gallery3d/ui/PositionController;->ANIM_TIME:[I

    aput v2, v5, v7

    .line 879
    iget v5, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController$Box;->doAnimation(IFI)Z
    invoke-static {v1, v4, v5, v7}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->access$500(Lcom/sec/android/gallery3d/ui/PositionController$Box;IFI)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 880
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    .line 883
    .end local v2    # "duration":I
    :goto_2
    return v2

    .line 864
    .end local v4    # "targetY":I
    :cond_1
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    add-int/lit8 v5, v5, 0x1

    div-int/lit8 v5, v5, 0x2

    div-int/lit8 v6, v3, 0x2

    add-int/2addr v5, v6

    add-int v4, v5, v0

    .restart local v4    # "targetY":I
    goto :goto_0

    .line 874
    :cond_2
    const/16 v2, 0xc8

    .restart local v2    # "duration":I
    goto :goto_1

    .line 883
    :cond_3
    const/4 v2, -0x1

    goto :goto_2
.end method

.method public flingPage(II)Z
    .locals 14
    .param p1, "velocityX"    # I
    .param p2, "velocityY"    # I

    .prologue
    .line 800
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 801
    .local v9, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 804
    .local v11, "p":Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    iget v0, v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->viewWiderThanScaledImage(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->viewTallerThanScaledImage(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 806
    const/4 v0, 0x0

    .line 828
    :goto_0
    return v0

    .line 811
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageAtEdges()I

    move-result v10

    .line 812
    .local v10, "edges":I
    if-lez p1, :cond_1

    and-int/lit8 v0, v10, 0x1

    if-nez v0, :cond_2

    :cond_1
    if-gez p1, :cond_3

    and-int/lit8 v0, v10, 0x2

    if-eqz v0, :cond_3

    .line 814
    :cond_2
    const/4 p1, 0x0

    .line 816
    :cond_3
    if-lez p2, :cond_4

    and-int/lit8 v0, v10, 0x4

    if-nez v0, :cond_5

    :cond_4
    if-gez p2, :cond_6

    and-int/lit8 v0, v10, 0x8

    if-eqz v0, :cond_6

    .line 818
    :cond_5
    const/16 p2, 0x0

    .line 821
    :cond_6
    if-nez p1, :cond_7

    if-nez p2, :cond_7

    const/4 v0, 0x0

    goto :goto_0

    .line 823
    :cond_7
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;

    iget v1, v11, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget v2, v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    move v3, p1

    move/from16 v4, p2

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/gallery3d/ui/FlingScroller;->fling(IIIIIIII)V

    .line 825
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FlingScroller;->getFinalX()I

    move-result v12

    .line 826
    .local v12, "targetX":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FlingScroller;->getFinalY()I

    move-result v13

    .line 827
    .local v13, "targetY":I
    sget-object v0, Lcom/sec/android/gallery3d/ui/PositionController;->ANIM_TIME:[I

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FlingScroller;->getDuration()I

    move-result v2

    aput v2, v0, v1

    .line 828
    iget v0, v9, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    const/4 v1, 0x6

    invoke-direct {p0, v12, v13, v0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    move-result v0

    goto :goto_0
.end method

.method public forceImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "s"    # Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    .prologue
    .line 344
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    if-nez v1, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 346
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    .line 347
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    iput v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    goto :goto_0
.end method

.method public getFilmRatio()F
    .locals 1

    .prologue
    .line 1423
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmRatio:Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mCurrentRatio:F

    return v0
.end method

.method public getImageAtEdges()I
    .locals 5

    .prologue
    .line 1390
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1391
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 1392
    .local v2, "p":Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(F)V

    .line 1393
    const/4 v1, 0x0

    .line 1394
    .local v1, "edges":I
    iget v3, v2, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    if-gt v3, v4, :cond_0

    .line 1395
    or-int/lit8 v1, v1, 0x2

    .line 1397
    :cond_0
    iget v3, v2, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    if-lt v3, v4, :cond_1

    .line 1398
    or-int/lit8 v1, v1, 0x1

    .line 1400
    :cond_1
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    if-gt v3, v4, :cond_2

    .line 1401
    or-int/lit8 v1, v1, 0x8

    .line 1403
    :cond_2
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    if-lt v3, v4, :cond_3

    .line 1404
    or-int/lit8 v1, v1, 0x4

    .line 1406
    :cond_3
    return v1
.end method

.method public getImageHeight()I
    .locals 3

    .prologue
    .line 1377
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1378
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageH:I

    return v1
.end method

.method public getImageScale()F
    .locals 3

    .prologue
    .line 1382
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1383
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    if-eqz v0, :cond_0

    .line 1384
    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 1386
    :goto_0
    return v1

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getImageWidth()I
    .locals 3

    .prologue
    .line 1372
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1373
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mImageW:I

    return v1
.end method

.method public getMinScale()F
    .locals 2

    .prologue
    .line 2106
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    return v0
.end method

.method public getPosition(I)Landroid/graphics/Rect;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    return-object v0
.end method

.method public getUseViewSize()Z
    .locals 2

    .prologue
    .line 2102
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mUseViewSize:Z

    return v0
.end method

.method public hasDeletingBox()Z
    .locals 3

    .prologue
    .line 1431
    const/4 v0, -0x3

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 1432
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    .line 1433
    const/4 v1, 0x1

    .line 1436
    :goto_1
    return v1

    .line 1431
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1436
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hitTest(II)I
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 891
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x7

    if-ge v0, v3, :cond_1

    .line 892
    sget-object v3, Lcom/sec/android/gallery3d/ui/PositionController;->CENTER_OUT_INDEX:[I

    aget v1, v3, v0

    .line 893
    .local v1, "j":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 894
    .local v2, "r":Landroid/graphics/Rect;
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 899
    .end local v1    # "j":I
    .end local v2    # "r":Landroid/graphics/Rect;
    :goto_1
    return v1

    .line 891
    .restart local v1    # "j":I
    .restart local v2    # "r":Landroid/graphics/Rect;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 899
    .end local v1    # "j":I
    .end local v2    # "r":Landroid/graphics/Rect;
    :cond_1
    const v1, 0x7fffffff

    goto :goto_1
.end method

.method public inOpeningAnimation()Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 984
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    cmp-long v0, v2, v6

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public inSlidingAnimation()Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x3

    const/4 v1, 0x0

    .line 2054
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    cmp-long v0, v2, v6

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget-wide v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public isAtMinimalScale()Z
    .locals 3

    .prologue
    .line 1361
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1362
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    iget v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->isAlmostEqual(FF)Z

    move-result v1

    return v1
.end method

.method public isCenter()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1366
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1367
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v2, v2, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v3, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isFullScreen()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/high16 v3, 0x447a0000    # 1000.0f

    .line 2065
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v0

    mul-float/2addr v0, v3

    float-to-int v2, v0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getMinimalScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    if-ne v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isInScale()Z
    .locals 1

    .prologue
    .line 2079
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mInScale:Z

    return v0
.end method

.method public isPanoramaScrolling()Z
    .locals 2

    .prologue
    .line 2061
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrolling()Z
    .locals 4

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->isHoldingDown()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSliding()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2070
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewer:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->isInTransition()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public moveBox([IZZZ[Lcom/sec/android/gallery3d/ui/PhotoView$Size;Z)V
    .locals 19
    .param p1, "fromIndex"    # [I
    .param p2, "hasPrev"    # Z
    .param p3, "hasNext"    # Z
    .param p4, "constrained"    # Z
    .param p5, "sizes"    # [Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    .param p6, "isFilmStripJump"    # Z

    .prologue
    .line 1187
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/ui/PositionController;->mHasPrev:Z

    .line 1188
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/ui/PositionController;->mHasNext:Z

    .line 1191
    new-instance v7, Lcom/sec/android/gallery3d/util/RangeIntArray;

    const/16 v17, -0x3

    const/16 v18, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v7, v0, v1, v2}, Lcom/sec/android/gallery3d/util/RangeIntArray;-><init>([III)V

    .line 1194
    .local v7, "from":Lcom/sec/android/gallery3d/util/RangeIntArray;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PositionController;->layoutAndSetPosition()V

    .line 1195
    const/4 v9, -0x3

    .local v9, "i":I
    :goto_0
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v9, v0, :cond_0

    .line 1196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1197
    .local v4, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mRects:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/Rect;

    .line 1198
    .local v14, "r":Landroid/graphics/Rect;
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    move/from16 v18, v0

    div-int/lit8 v18, v18, 0x2

    sub-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    .line 1195
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1202
    .end local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v14    # "r":Landroid/graphics/Rect;
    :cond_0
    const/4 v9, -0x3

    :goto_1
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v9, v0, :cond_1

    .line 1203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1202
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1206
    :cond_1
    const/4 v9, -0x3

    :goto_2
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ge v9, v0, :cond_2

    .line 1207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1206
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 1212
    :cond_2
    const/4 v9, -0x3

    :goto_3
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v9, v0, :cond_4

    .line 1213
    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/util/RangeIntArray;->get(I)I

    move-result v10

    .line 1214
    .local v10, "j":I
    const v17, 0x7fffffff

    move/from16 v0, v17

    if-ne v10, v0, :cond_3

    .line 1212
    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 1215
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    goto :goto_4

    .line 1220
    .end local v10    # "j":I
    :cond_4
    const/4 v9, -0x3

    :goto_5
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ge v9, v0, :cond_7

    .line 1221
    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/util/RangeIntArray;->get(I)I

    move-result v10

    .line 1222
    .restart local v10    # "j":I
    const v17, 0x7fffffff

    move/from16 v0, v17

    if-ne v10, v0, :cond_6

    .line 1220
    :cond_5
    :goto_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 1223
    :cond_6
    add-int/lit8 v17, v9, 0x1

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/util/RangeIntArray;->get(I)I

    move-result v11

    .line 1224
    .local v11, "k":I
    const v17, 0x7fffffff

    move/from16 v0, v17

    if-eq v11, v0, :cond_5

    .line 1225
    add-int/lit8 v17, v10, 0x1

    move/from16 v0, v17

    if-ne v0, v11, :cond_5

    .line 1226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    goto :goto_6

    .line 1232
    .end local v10    # "j":I
    .end local v11    # "k":I
    :cond_7
    const/4 v11, -0x3

    .line 1233
    .restart local v11    # "k":I
    const/4 v9, -0x3

    :goto_7
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v9, v0, :cond_a

    .line 1234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_8

    .line 1233
    :goto_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 1235
    :cond_8
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    if-nez v17, :cond_9

    .line 1236
    add-int/lit8 v11, v11, 0x1

    goto :goto_9

    .line 1238
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v18, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "k":I
    .local v12, "k":I
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1239
    add-int/lit8 v17, v9, 0x3

    aget-object v17, p5, v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v9, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->initBox(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V

    move v11, v12

    .end local v12    # "k":I
    .restart local v11    # "k":I
    goto :goto_8

    .line 1247
    :cond_a
    const/4 v6, -0x3

    .local v6, "first":I
    :goto_a
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v6, v0, :cond_b

    .line 1248
    invoke-virtual {v7, v6}, Lcom/sec/android/gallery3d/util/RangeIntArray;->get(I)I

    move-result v17

    const v18, 0x7fffffff

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_e

    .line 1250
    :cond_b
    const/4 v13, 0x3

    .local v13, "last":I
    :goto_b
    const/16 v17, -0x3

    move/from16 v0, v17

    if-lt v13, v0, :cond_c

    .line 1251
    invoke-virtual {v7, v13}, Lcom/sec/android/gallery3d/util/RangeIntArray;->get(I)I

    move-result v17

    const v18, 0x7fffffff

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_f

    .line 1255
    :cond_c
    const/16 v17, 0x3

    move/from16 v0, v17

    if-le v6, v0, :cond_d

    .line 1256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    .line 1257
    const/4 v13, 0x0

    move v6, v13

    .line 1265
    :cond_d
    const/16 v17, 0x0

    add-int/lit8 v18, v6, 0x1

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v9

    :goto_c
    if-ge v9, v13, :cond_12

    .line 1266
    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/util/RangeIntArray;->get(I)I

    move-result v17

    const v18, 0x7fffffff

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_10

    .line 1265
    :goto_d
    add-int/lit8 v9, v9, 0x1

    goto :goto_c

    .line 1247
    .end local v13    # "last":I
    :cond_e
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    .line 1250
    .restart local v13    # "last":I
    :cond_f
    add-int/lit8 v13, v13, -0x1

    goto :goto_b

    .line 1267
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    add-int/lit8 v18, v9, -0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1268
    .local v3, "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1269
    .restart local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v15

    .line 1270
    .local v15, "wa":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v16

    .line 1271
    .local v16, "wb":I
    iget v0, v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    move/from16 v17, v0

    add-int v17, v17, v15

    div-int/lit8 v18, v15, 0x2

    sub-int v17, v17, v18

    div-int/lit8 v18, v16, 0x2

    add-int v17, v17, v18

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getDefaultGapSize(I)I

    move-result v18

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    .line 1273
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPopFromTop:Z

    move/from16 v17, v0

    if-eqz v17, :cond_11

    .line 1274
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    goto :goto_d

    .line 1276
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    goto/16 :goto_d

    .line 1281
    .end local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v15    # "wa":I
    .end local v16    # "wb":I
    :cond_12
    const/16 v17, -0x1

    add-int/lit8 v18, v13, -0x1

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v9

    :goto_e
    if-le v9, v6, :cond_15

    .line 1282
    invoke-virtual {v7, v9}, Lcom/sec/android/gallery3d/util/RangeIntArray;->get(I)I

    move-result v17

    const v18, 0x7fffffff

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_13

    .line 1281
    :goto_f
    add-int/lit8 v9, v9, -0x1

    goto :goto_e

    .line 1283
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    add-int/lit8 v18, v9, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1284
    .restart local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1285
    .restart local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v15

    .line 1286
    .restart local v15    # "wa":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v16

    .line 1287
    .restart local v16    # "wb":I
    iget v0, v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    move/from16 v17, v0

    div-int/lit8 v18, v15, 0x2

    sub-int v17, v17, v18

    div-int/lit8 v18, v16, 0x2

    sub-int v18, v16, v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getDefaultGapSize(I)I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    .line 1289
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPopFromTop:Z

    move/from16 v17, v0

    if-eqz v17, :cond_14

    .line 1290
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    goto :goto_f

    .line 1292
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->heightOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    goto :goto_f

    .line 1297
    .end local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v15    # "wa":I
    .end local v16    # "wb":I
    :cond_15
    const/4 v11, -0x3

    .line 1298
    const/4 v9, -0x3

    :goto_10
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ge v9, v0, :cond_19

    .line 1299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_16

    .line 1298
    :goto_11
    add-int/lit8 v9, v9, 0x1

    goto :goto_10

    .line 1300
    :cond_16
    :goto_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    if-nez v17, :cond_17

    .line 1301
    add-int/lit8 v11, v11, 0x1

    goto :goto_12

    .line 1303
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mTempGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v18, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "k":I
    .restart local v12    # "k":I
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/gallery3d/util/RangeArray;->put(ILjava/lang/Object;)V

    .line 1304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1305
    .restart local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    add-int/lit8 v18, v9, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1306
    .restart local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v15

    .line 1307
    .restart local v15    # "wa":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v16

    .line 1308
    .restart local v16    # "wb":I
    if-lt v9, v6, :cond_18

    if-ge v9, v13, :cond_18

    .line 1309
    iget v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    move/from16 v17, v0

    iget v0, v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    div-int/lit8 v18, v16, 0x2

    sub-int v17, v17, v18

    div-int/lit8 v18, v15, 0x2

    sub-int v18, v15, v18

    sub-int v8, v17, v18

    .line 1310
    .local v8, "g":I
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v8}, Lcom/sec/android/gallery3d/ui/PositionController;->initGap(II)V

    move v11, v12

    .line 1311
    .end local v12    # "k":I
    .restart local v11    # "k":I
    goto :goto_11

    .line 1312
    .end local v8    # "g":I
    .end local v11    # "k":I
    .restart local v12    # "k":I
    :cond_18
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->initGap(I)V

    move v11, v12

    .end local v12    # "k":I
    .restart local v11    # "k":I
    goto :goto_11

    .line 1318
    .end local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v15    # "wa":I
    .end local v16    # "wb":I
    :cond_19
    add-int/lit8 v9, v6, -0x1

    :goto_13
    const/16 v17, -0x3

    move/from16 v0, v17

    if-lt v9, v0, :cond_1a

    .line 1319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    add-int/lit8 v18, v9, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1320
    .restart local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1321
    .restart local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v15

    .line 1322
    .restart local v15    # "wa":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v16

    .line 1323
    .restart local v16    # "wb":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 1324
    .local v8, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget v0, v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    move/from16 v17, v0

    div-int/lit8 v18, v15, 0x2

    sub-int v17, v17, v18

    div-int/lit8 v18, v16, 0x2

    sub-int v18, v16, v18

    sub-int v17, v17, v18

    iget v0, v8, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    .line 1318
    add-int/lit8 v9, v9, -0x1

    goto :goto_13

    .line 1327
    .end local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v8    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    .end local v15    # "wa":I
    .end local v16    # "wb":I
    :cond_1a
    add-int/lit8 v9, v13, 0x1

    :goto_14
    const/16 v17, 0x3

    move/from16 v0, v17

    if-gt v9, v0, :cond_1b

    .line 1328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    add-int/lit8 v18, v9, -0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1329
    .restart local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1330
    .restart local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v15

    .line 1331
    .restart local v15    # "wa":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->widthOf(Lcom/sec/android/gallery3d/ui/PositionController$Box;)I

    move-result v16

    .line 1332
    .restart local v16    # "wb":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    add-int/lit8 v18, v9, -0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 1333
    .restart local v8    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget v0, v3, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    move/from16 v17, v0

    add-int v17, v17, v15

    div-int/lit8 v18, v15, 0x2

    sub-int v17, v17, v18

    div-int/lit8 v18, v16, 0x2

    add-int v17, v17, v18

    iget v0, v8, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v4, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    .line 1327
    add-int/lit8 v9, v9, 0x1

    goto :goto_14

    .line 1337
    .end local v3    # "a":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v4    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    .end local v8    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    .end local v15    # "wa":I
    .end local v16    # "wb":I
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAbsoluteX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    move/from16 v18, v0

    sub-int v5, v17, v18

    .line 1338
    .local v5, "dx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    move/from16 v18, v0

    add-int v18, v18, v5

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromX:I

    move/from16 v18, v0

    add-int v18, v18, v5

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromX:I

    .line 1340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    move/from16 v18, v0

    add-int v18, v18, v5

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    .line 1341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFlingOffset:I

    move/from16 v18, v0

    add-int v18, v18, v5

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFlingOffset:I

    .line 1343
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrained:Z

    move/from16 v17, v0

    move/from16 v0, v17

    move/from16 v1, p4

    if-eq v0, v1, :cond_1c

    .line 1344
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrained:Z

    .line 1345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->updateDefaultXY()V

    .line 1346
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PositionController;->updateScaleAndGapLimit()V

    .line 1349
    :cond_1c
    if-eqz p6, :cond_1d

    .line 1350
    const/16 v17, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw(I)V

    .line 1354
    :goto_15
    return-void

    .line 1352
    :cond_1d
    const/16 v17, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw(I)V

    goto :goto_15
.end method

.method public panoramaScroll(IIIF)V
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "kind"    # I
    .param p4, "ratio"    # F

    .prologue
    .line 704
    invoke-direct {p0, p4}, Lcom/sec/android/gallery3d/ui/PositionController;->getPanoramaAnimDuration(F)I

    move-result v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(IIII)V

    .line 705
    return-void
.end method

.method public resetToFullView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 597
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 598
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iget v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    const/4 v3, 0x4

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 599
    return-void
.end method

.method public resetToFullViewNoAnimation()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2075
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    const/4 v2, 0x2

    invoke-direct {p0, v1, v3, v0, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 2076
    return-void
.end method

.method public scaleBy(FFF)I
    .locals 8
    .param p1, "s"    # F
    .param p2, "focusX"    # F
    .param p3, "focusY"    # F

    .prologue
    const/4 v4, 0x1

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    .line 617
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr p2, v6

    .line 618
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    sub-float/2addr p3, v6

    .line 619
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v6, v5}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 620
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 627
    .local v1, "p":Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getTargetScale(Lcom/sec/android/gallery3d/ui/PositionController$Box;)F

    move-result v6

    mul-float/2addr v6, p1

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->clampScale(F)F

    move-result p1

    .line 628
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-eqz v6, :cond_5

    iget v2, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 629
    .local v2, "x":I
    :goto_0
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-eqz v6, :cond_6

    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    .line 633
    .local v3, "y":I
    :goto_1
    invoke-direct {p0, p1, v5}, Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(FI)V

    .line 634
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    if-le v2, v6, :cond_0

    .line 635
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    .line 637
    :cond_0
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    if-ge v2, v6, :cond_1

    .line 638
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    .line 640
    :cond_1
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    if-ge v3, v6, :cond_2

    .line 641
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    .line 643
    :cond_2
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    if-le v3, v6, :cond_3

    .line 644
    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    .line 647
    :cond_3
    invoke-direct {p0, v2, v3, p1, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 648
    iget v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    cmpg-float v6, p1, v6

    if-gez v6, :cond_7

    const/4 v4, -0x1

    .line 650
    :cond_4
    :goto_2
    return v4

    .line 628
    .end local v2    # "x":I
    .end local v3    # "y":I
    :cond_5
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusX:F

    mul-float/2addr v6, p1

    sub-float v6, p2, v6

    add-float/2addr v6, v7

    float-to-int v2, v6

    goto :goto_0

    .line 629
    .restart local v2    # "x":I
    :cond_6
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFocusY:F

    mul-float/2addr v6, p1

    sub-float v6, p3, v6

    add-float/2addr v6, v7

    float-to-int v3, v6

    goto :goto_1

    .line 649
    .restart local v3    # "y":I
    :cond_7
    iget v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMax:F

    cmpl-float v6, p1, v6

    if-gtz v6, :cond_4

    move v4, v5

    .line 650
    goto :goto_2
.end method

.method public scrollFilmX(I)V
    .locals 9
    .param p1, "dx"    # I

    .prologue
    const/4 v8, 0x0

    .line 756
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->canScroll()Z

    move-result v3

    if-nez v3, :cond_0

    .line 788
    :goto_0
    return-void

    .line 758
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v8}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 759
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    .line 763
    .local v1, "p":Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    iget-wide v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    .line 764
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    .line 774
    :cond_1
    :sswitch_0
    iget v3, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    add-int v2, v3, p1

    .line 778
    .local v2, "x":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v3, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    sub-int/2addr v2, v3

    .line 779
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasPrev:Z

    if-nez v3, :cond_3

    if-lez v2, :cond_3

    .line 780
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    const/4 v4, 0x1

    invoke-interface {v3, v2, v4}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onPull(II)V

    .line 781
    const/4 v2, 0x0

    .line 786
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v3, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    add-int/2addr v2, v3

    .line 787
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-direct {p0, v2, v3, v4, v8}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    goto :goto_0

    .line 782
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mHasNext:Z

    if-nez v3, :cond_2

    if-gez v2, :cond_2

    .line 783
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    neg-int v4, v2

    const/4 v5, 0x3

    invoke-interface {v3, v4, v5}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onPull(II)V

    .line 784
    const/4 v2, 0x0

    goto :goto_1

    .line 764
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_0
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method public scrollFilmY(II)V
    .locals 4
    .param p1, "boxIndex"    # I
    .param p2, "dy"    # I

    .prologue
    .line 791
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->canScroll()Z

    move-result v2

    if-nez v2, :cond_0

    .line 797
    :goto_0
    return-void

    .line 793
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 794
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    add-int v1, v2, p2

    .line 795
    .local v1, "y":I
    iget v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController$Box;->doAnimation(IFI)Z
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->access$500(Lcom/sec/android/gallery3d/ui/PositionController$Box;IFI)Z

    .line 796
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    goto :goto_0
.end method

.method public scrollPage(II)V
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 696
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(III)V

    .line 697
    return-void
.end method

.method public scrollPage(III)V
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "kind"    # I

    .prologue
    .line 700
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(IIII)V

    .line 701
    return-void
.end method

.method public setAnimationListener(Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    .prologue
    .line 2114
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationListener:Lcom/sec/android/gallery3d/ui/PositionController$SimpleAnimationListener;

    .line 2115
    return-void
.end method

.method public setConstrainedFrame(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "cFrame"    # Landroid/graphics/Rect;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->updateDefaultXY()V

    .line 339
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->updateScaleAndGapLimit()V

    .line 340
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw()V

    goto :goto_0
.end method

.method public setExtraScalingRange(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mExtraScalingRange:Z

    if-ne v0, p1, :cond_1

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mExtraScalingRange:Z

    .line 465
    if-nez p1, :cond_0

    .line 466
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw()V

    goto :goto_0
.end method

.method public setFilmMode(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 453
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-ne p1, v0, :cond_0

    .line 460
    :goto_0
    return-void

    .line 454
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    .line 456
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->updateDefaultXY()V

    .line 457
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->updateScaleAndGapLimit()V

    .line 458
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->stopAnimation()V

    .line 459
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw()V

    goto :goto_0
.end method

.method public setImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "s"    # Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    .param p3, "cFrame"    # Landroid/graphics/Rect;

    .prologue
    .line 352
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    if-nez v1, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    const/4 v0, 0x0

    .line 355
    .local v0, "needUpdate":Z
    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v1, p3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 356
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;

    invoke-virtual {v1, p3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 357
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->updateDefaultXY()V

    .line 358
    const/4 v0, 0x1

    .line 360
    :cond_2
    iget v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    iget v2, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    const/4 v3, 0x0

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->setBoxSize(IIIZ)Z

    move-result v1

    or-int/2addr v0, v1

    .line 362
    if-eqz v0, :cond_0

    .line 363
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->updateScaleAndGapLimit()V

    .line 364
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw()V

    goto :goto_0
.end method

.method public setOpenAnimationRect(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mOpenAnimationRect:Landroid/graphics/Rect;

    .line 295
    return-void
.end method

.method public setPopFromTop(Z)V
    .locals 0
    .param p1, "top"    # Z

    .prologue
    .line 1427
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPopFromTop:Z

    .line 1428
    return-void
.end method

.method public setScaleLimit(F)V
    .locals 1
    .param p1, "scaleLimit"    # F

    .prologue
    .line 2048
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    .line 2049
    sput p1, Lcom/sec/android/gallery3d/ui/PositionController;->SCALE_LIMIT:F

    .line 2051
    :cond_0
    return-void
.end method

.method public setScalePrepared(Z)V
    .locals 0
    .param p1, "prepare"    # Z

    .prologue
    .line 368
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mIsScalePrepare:Z

    .line 369
    return-void
.end method

.method public setViewSize(II)V
    .locals 6
    .param p1, "viewW"    # I
    .param p2, "viewH"    # I

    .prologue
    const/4 v5, 0x0

    .line 298
    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    if-ne p1, v4, :cond_1

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    if-ne p2, v4, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v3

    .line 302
    .local v3, "wasMinimal":Z
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    .line 303
    iput p2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->isPanoramaScrolling()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 305
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->initPlatform()V

    .line 308
    :cond_2
    const/4 v2, -0x3

    .local v2, "i":I
    :goto_1
    const/4 v4, 0x3

    if-gt v2, v4, :cond_3

    .line 309
    const/4 v4, 0x1

    invoke-direct {p0, v2, p1, p2, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->setBoxSize(IIIZ)Z

    .line 308
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 312
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->updateScaleAndGapLimit()V

    .line 316
    if-eqz v3, :cond_4

    .line 317
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 318
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    iput v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 323
    .end local v0    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->isPanoramaScrolling()Z

    move-result v4

    if-nez v4, :cond_0

    .line 324
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->startOpeningAnimationIfNeeded()Z

    move-result v4

    if-nez v4, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->skipToFinalPosition()V

    .line 327
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 328
    .local v1, "box":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    if-eqz v1, :cond_0

    iget v4, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullView()V

    goto :goto_0
.end method

.method public skipAnimation()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const-wide/16 v6, -0x1

    .line 531
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 532
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v4, v4, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    iput v4, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 533
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v4, v4, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToY:I

    iput v4, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    .line 534
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iput-wide v6, v3, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    .line 536
    :cond_0
    const/4 v2, -0x3

    .local v2, "i":I
    :goto_0
    if-gt v2, v8, :cond_2

    .line 537
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 538
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-wide v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 536
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 539
    :cond_1
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mToY:I

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    .line 540
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mToScale:F

    iput v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    .line 541
    iput-wide v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    goto :goto_1

    .line 543
    .end local v0    # "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    :cond_2
    const/4 v2, -0x3

    :goto_2
    if-ge v2, v8, :cond_4

    .line 544
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 545
    .local v1, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget-wide v4, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mAnimationStartTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 543
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 546
    :cond_3
    iget v3, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mToGap:I

    iput v3, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mCurrentGap:I

    .line 547
    iput-wide v6, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mAnimationStartTime:J

    goto :goto_3

    .line 551
    .end local v1    # "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkAnimation(Z)V

    .line 553
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    .line 554
    return-void
.end method

.method public skipToFinalPosition()V
    .locals 3

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->stopAnimation()V

    .line 562
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 563
    .local v0, "box":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationKind:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 564
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullView()V

    .line 568
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->skipAnimation()V

    .line 569
    return-void

    .line 566
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw()V

    goto :goto_0
.end method

.method public snapback()V
    .locals 0

    .prologue
    .line 557
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->snapAndRedraw()V

    .line 558
    return-void
.end method

.method public startCaptureAnimationSlide(I)V
    .locals 8
    .param p1, "offset"    # I

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0xa

    .line 669
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 670
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 671
    .local v2, "n":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    .line 673
    .local v1, "g":Lcom/sec/android/gallery3d/ui/PositionController$Gap;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v4, v4, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v5, v5, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultY:I

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController$Platform;->doAnimation(III)Z
    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->access$400(Lcom/sec/android/gallery3d/ui/PositionController$Platform;III)Z

    .line 675
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController$Box;->doAnimation(IFI)Z
    invoke-static {v0, v7, v3, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->access$500(Lcom/sec/android/gallery3d/ui/PositionController$Box;IFI)Z

    .line 676
    iget v3, v2, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController$Box;->doAnimation(IFI)Z
    invoke-static {v2, v7, v3, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Box;->access$500(Lcom/sec/android/gallery3d/ui/PositionController$Box;IFI)Z

    .line 677
    iget v3, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mDefaultSize:I

    invoke-virtual {v1, v3, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->doAnimation(II)Z

    .line 678
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PositionController;->redraw()V

    .line 679
    return-void
.end method

.method public startHorizontalSlide()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 660
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 661
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v1, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iget v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    const/4 v3, 0x3

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 662
    return-void
.end method

.method public stopAnimation()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const-wide/16 v2, -0x1

    .line 513
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iput-wide v2, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    .line 514
    const/4 v0, -0x3

    .local v0, "i":I
    :goto_0
    if-gt v0, v4, :cond_0

    .line 515
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    iput-wide v2, v1, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mAnimationStartTime:J

    .line 514
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 517
    :cond_0
    const/4 v0, -0x3

    :goto_1
    if-ge v0, v4, :cond_1

    .line 518
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mGaps:Lcom/sec/android/gallery3d/util/RangeArray;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;

    iput-wide v2, v1, Lcom/sec/android/gallery3d/ui/PositionController$Gap;->mAnimationStartTime:J

    .line 517
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 526
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->checkAnimation(Z)V

    .line 528
    return-void
.end method

.method public stopScrolling()V
    .locals 4

    .prologue
    .line 1417
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1420
    :goto_0
    return-void

    .line 1418
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/common/Scroller;->forceFinished(Z)V

    .line 1419
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v2, v2, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    iput v2, v0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromX:I

    goto :goto_0
.end method

.method public zoomIn(FFF)V
    .locals 10
    .param p1, "tapX"    # F
    .param p2, "tapY"    # F
    .param p3, "targetScale"    # F

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    .line 576
    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr p1, v7

    .line 577
    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr p2, v7

    .line 578
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 580
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget v7, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMax:F

    invoke-static {p3, v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result p3

    .line 583
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mPlatform:Lcom/sec/android/gallery3d/ui/PositionController$Platform;

    iget v7, v7, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    int-to-float v7, v7

    sub-float v7, p1, v7

    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    div-float v3, v7, v8

    .line 584
    .local v3, "tempX":F
    iget v7, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentY:I

    int-to-float v7, v7

    sub-float v7, p2, v7

    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    div-float v4, v7, v8

    .line 586
    .local v4, "tempY":F
    neg-float v7, v3

    mul-float/2addr v7, p3

    add-float/2addr v7, v9

    float-to-int v5, v7

    .line 587
    .local v5, "x":I
    neg-float v7, v4

    mul-float/2addr v7, p3

    add-float/2addr v7, v9

    float-to-int v6, v7

    .line 589
    .local v6, "y":I
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(F)V

    .line 590
    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I

    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I

    invoke-static {v5, v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 591
    .local v1, "targetX":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundTop:I

    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController;->mBoundBottom:I

    invoke-static {v6, v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    .line 593
    .local v2, "targetY":I
    const/4 v7, 0x4

    invoke-direct {p0, v1, v2, p3, v7}, Lcom/sec/android/gallery3d/ui/PositionController;->startAnimation(IIFI)Z

    .line 594
    return-void
.end method
