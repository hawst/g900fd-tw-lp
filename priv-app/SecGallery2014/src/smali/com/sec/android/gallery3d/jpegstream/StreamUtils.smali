.class public Lcom/sec/android/gallery3d/jpegstream/StreamUtils;
.super Ljava/lang/Object;
.source "StreamUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static byteToIntArray([I[BLjava/nio/ByteOrder;)Z
    .locals 5
    .param p0, "output"    # [I
    .param p1, "input"    # [B
    .param p2, "endianness"    # Ljava/nio/ByteOrder;

    .prologue
    .line 32
    array-length v3, p1

    array-length v4, p1

    rem-int/lit8 v4, v4, 0x4

    sub-int v2, v3, v4

    .line 33
    .local v2, "length":I
    array-length v3, p0

    mul-int/lit8 v3, v3, 0x4

    if-ge v3, v2, :cond_0

    .line 34
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v4, "Output array is too short to hold input"

    invoke-direct {v3, v4}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 36
    :cond_0
    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    if-ne p2, v3, :cond_1

    .line 37
    const/4 v0, 0x0

    .local v0, "i":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_2

    .line 38
    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    add-int/lit8 v4, v1, 0x1

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v3, v4

    add-int/lit8 v4, v1, 0x2

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    add-int/lit8 v4, v1, 0x3

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    aput v3, p0, v0

    .line 37
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x4

    goto :goto_0

    .line 42
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_2

    .line 43
    add-int/lit8 v3, v1, 0x3

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    add-int/lit8 v4, v1, 0x2

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v3, v4

    add-int/lit8 v4, v1, 0x1

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    aget-byte v4, p1, v1

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    aput v3, p0, v0

    .line 42
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x4

    goto :goto_1

    .line 47
    :cond_2
    array-length v3, p1

    rem-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_2
    return v3

    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static byteToIntArray([B)[I
    .locals 1
    .param p0, "input"    # [B

    .prologue
    .line 60
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/jpegstream/StreamUtils;->byteToIntArray([BLjava/nio/ByteOrder;)[I

    move-result-object v0

    return-object v0
.end method

.method public static byteToIntArray([BLjava/nio/ByteOrder;)[I
    .locals 2
    .param p0, "input"    # [B
    .param p1, "endianness"    # Ljava/nio/ByteOrder;

    .prologue
    .line 51
    array-length v1, p0

    div-int/lit8 v1, v1, 0x4

    new-array v0, v1, [I

    .line 52
    .local v0, "output":[I
    invoke-static {v0, p0, p1}, Lcom/sec/android/gallery3d/jpegstream/StreamUtils;->byteToIntArray([I[BLjava/nio/ByteOrder;)Z

    .line 53
    return-object v0
.end method

.method public static pixelSize(I)I
    .locals 1
    .param p0, "format"    # I

    .prologue
    .line 68
    sparse-switch p0, :sswitch_data_0

    .line 77
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 71
    :sswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 73
    :sswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 75
    :sswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 68
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_0
        0x104 -> :sswitch_0
    .end sparse-switch
.end method
