.class public Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ChangePlayerListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;,
        Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mHttpClient:Landroid/net/http/AndroidHttpClient;

.field public static sIconCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDataChangedHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->TAG:Ljava/lang/String;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->sIconCache:Ljava/util/HashMap;

    .line 132
    const-string v0, "nearbyIcon"

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->initDataChangedHandler()V

    .line 60
    return-void
.end method

.method static synthetic access$100()Landroid/net/http/AndroidHttpClient;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private initDataChangedHandler()V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$1;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->mDataChangedHandler:Landroid/os/Handler;

    .line 70
    return-void
.end method


# virtual methods
.method public clearIconCache()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->sIconCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 55
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 75
    if-nez p2, :cond_2

    .line 77
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v9, 0x7f03002c

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 79
    new-instance v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;

    const/4 v6, 0x0

    invoke-direct {v3, v6}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;-><init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$1;)V

    .line 80
    .local v3, "holder":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;
    const v6, 0x7f0f0071

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    .line 81
    const v6, 0x7f0f0073

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    .line 82
    const v6, 0x7f0f0072

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mPlayerName:Landroid/widget/TextView;

    .line 83
    const v6, 0x7f0f0074

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckedTextView;

    iput-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mPlayerCheck:Landroid/widget/CheckedTextView;

    .line 84
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    .line 99
    .local v4, "item":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDeviceType()I

    move-result v1

    .line 100
    .local v1, "deviceType":I
    if-ne v1, v7, :cond_6

    .line 101
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v6, :cond_4

    .line 102
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    const v9, 0x7f020362

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 108
    :goto_0
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "device_name"

    invoke-static {v9, v10}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :cond_1
    :goto_1
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mPlayerName:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDeviceName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v9, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mPlayerCheck:Landroid/widget/CheckedTextView;

    if-nez p1, :cond_a

    move v6, v7

    :goto_2
    invoke-virtual {v9, v6}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    move-object v5, p2

    .line 129
    .end local v1    # "deviceType":I
    .end local v3    # "holder":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;
    .end local v4    # "item":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    :goto_3
    return-object v5

    .line 85
    :catch_0
    move-exception v2

    .line 86
    .local v2, "e":Landroid/view/InflateException;
    invoke-virtual {v2}, Landroid/view/InflateException;->printStackTrace()V

    goto :goto_3

    .line 90
    .end local v2    # "e":Landroid/view/InflateException;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;

    .line 91
    .restart local v3    # "holder":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    if-eqz v6, :cond_3

    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    if-eqz v6, :cond_3

    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mPlayerName:Landroid/widget/TextView;

    if-eqz v6, :cond_3

    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mPlayerCheck:Landroid/widget/CheckedTextView;

    if-nez v6, :cond_0

    .line 92
    :cond_3
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->TAG:Ljava/lang/String;

    const-string v7, "ViewHolder has null value!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 103
    .restart local v1    # "deviceType":I
    .restart local v4    # "item":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 104
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    const v9, 0x7f02035d

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 106
    :cond_5
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    const v9, 0x7f020366

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 110
    :cond_6
    const/4 v6, 0x2

    if-ne v1, v6, :cond_9

    .line 111
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getIconUri()Landroid/net/Uri;

    move-result-object v6

    if-nez v6, :cond_7

    .line 112
    .local v5, "uriString":Ljava/lang/String;
    :goto_4
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->sIconCache:Ljava/util/HashMap;

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 113
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_8

    .line 114
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    const v9, 0x7f0203f0

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 115
    new-instance v6, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getIconUri()Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v6, v9}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;-><init>(Landroid/net/Uri;)V

    new-array v9, v7, [Landroid/os/Handler;

    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->mDataChangedHandler:Landroid/os/Handler;

    aput-object v10, v9, v8

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 119
    :goto_5
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    const v9, 0x7f0e0124

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 111
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "uriString":Ljava/lang/String;
    :cond_7
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getIconUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 117
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "uriString":Ljava/lang/String;
    :cond_8
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    new-instance v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-direct {v9, v10, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_5

    .line 120
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "uriString":Ljava/lang/String;
    :cond_9
    const/4 v6, 0x3

    if-ne v1, v6, :cond_1

    .line 121
    iget-object v9, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDevice()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/display/WifiDisplay;

    invoke-virtual {v6}, Landroid/hardware/display/WifiDisplay;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getWfdIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 123
    iget-object v6, v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    const v9, 0x7f0e0125

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_a
    move v6, v8

    .line 127
    goto/16 :goto_2
.end method
