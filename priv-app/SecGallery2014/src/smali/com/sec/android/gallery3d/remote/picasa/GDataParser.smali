.class public final Lcom/sec/android/gallery3d/remote/picasa/GDataParser;
.super Ljava/lang/Object;
.source "GDataParser.java"

# interfaces
.implements Lorg/xml/sax/ContentHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;
    }
.end annotation


# static fields
.field public static final APP_NAMESPACE:Ljava/lang/String; = "http://www.w3.org/2007/app"

.field public static final ATOM_NAMESPACE:Ljava/lang/String; = "http://www.w3.org/2005/Atom"

.field private static final ENTRY_ELEMENT:Ljava/lang/String; = "entry"

.field public static final EXIF_NAMESPACE:Ljava/lang/String; = "http://schemas.google.com/photos/exif/2007"

.field private static final FEED_ELEMENT:Ljava/lang/String; = "feed"

.field public static final GD_NAMESPACE:Ljava/lang/String; = "http://schemas.google.com/g/2005"

.field public static final GML_NAMESPACE:Ljava/lang/String; = "http://www.opengis.net/gml"

.field public static final GPHOTO_NAMESPACE:Ljava/lang/String; = "http://schemas.google.com/photos/2007"

.field public static final MEDIA_RSS_NAMESPACE:Ljava/lang/String; = "http://search.yahoo.com/mrss/"

.field private static final NUM_LEVELS:I = 0x5

.field private static final STATE_DOCUMENT:I = 0x0

.field private static final STATE_ENTRY:I = 0x2

.field private static final STATE_FEED:I = 0x1


# instance fields
.field private final mAttributes:[Lorg/xml/sax/helpers/AttributesImpl;

.field private mEntry:Lcom/sec/android/gallery3d/remote/picasa/Entry;

.field private mHandler:Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;

.field private mLevel:I

.field private final mName:[Ljava/lang/String;

.field private mState:I

.field private final mUri:[Ljava/lang/String;

.field private final mValue:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x5

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mEntry:Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .line 45
    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mHandler:Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;

    .line 46
    iput v2, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mState:I

    .line 47
    iput v2, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mLevel:I

    .line 48
    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mUri:[Ljava/lang/String;

    .line 49
    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mName:[Ljava/lang/String;

    .line 50
    new-array v2, v4, [Lorg/xml/sax/helpers/AttributesImpl;

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mAttributes:[Lorg/xml/sax/helpers/AttributesImpl;

    .line 51
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mValue:Ljava/lang/StringBuilder;

    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mAttributes:[Lorg/xml/sax/helpers/AttributesImpl;

    .line 59
    .local v0, "attributes":[Lorg/xml/sax/helpers/AttributesImpl;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-eq v1, v4, :cond_0

    .line 60
    new-instance v2, Lorg/xml/sax/helpers/AttributesImpl;

    invoke-direct {v2}, Lorg/xml/sax/helpers/AttributesImpl;-><init>()V

    aput-object v2, v0, v1

    .line 59
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method

.method private endProperty()V
    .locals 6

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mLevel:I

    .line 140
    .local v0, "level":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mEntry:Lcom/sec/android/gallery3d/remote/picasa/Entry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mUri:[Ljava/lang/String;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mName:[Ljava/lang/String;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mAttributes:[Lorg/xml/sax/helpers/AttributesImpl;

    aget-object v4, v4, v0

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/remote/picasa/Entry;->setPropertyFromXml(Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/String;)V

    .line 141
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mLevel:I

    .line 142
    return-void
.end method

.method public static parseAtomTimestamp(Ljava/lang/String;)J
    .locals 4
    .param p0, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 73
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 74
    .local v0, "time":Landroid/text/format/Time;
    invoke-virtual {v0, p0}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 75
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    return-wide v2
.end method

.method private startProperty(Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 129
    iget v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mLevel:I

    add-int/lit8 v0, v1, 0x1

    .line 130
    .local v0, "level":I
    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mLevel:I

    .line 131
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mValue:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 132
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mUri:[Ljava/lang/String;

    aput-object p1, v1, v0

    .line 133
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mName:[Ljava/lang/String;

    aput-object p2, v1, v0

    .line 134
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mAttributes:[Lorg/xml/sax/helpers/AttributesImpl;

    aget-object v1, v1, v0

    invoke-virtual {v1, p3}, Lorg/xml/sax/helpers/AttributesImpl;->setAttributes(Lorg/xml/sax/Attributes;)V

    .line 135
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 146
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 170
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 106
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mLevel:I

    if-lez v0, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->endProperty()V

    .line 125
    :goto_0
    return-void

    .line 111
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mState:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 113
    :pswitch_0
    new-instance v0, Lorg/xml/sax/SAXException;

    invoke-direct {v0}, Lorg/xml/sax/SAXException;-><init>()V

    throw v0

    .line 115
    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mState:I

    goto :goto_0

    .line 118
    :pswitch_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mState:I

    .line 119
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mHandler:Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mEntry:Lcom/sec/android/gallery3d/remote/picasa/Entry;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;->handleEntry(Lcom/sec/android/gallery3d/remote/picasa/Entry;)V

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 178
    return-void
.end method

.method public ignorableWhitespace([CII)V
    .locals 0
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 150
    return-void
.end method

.method public processingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 154
    return-void
.end method

.method public setDocumentLocator(Lorg/xml/sax/Locator;)V
    .locals 0
    .param p1, "locator"    # Lorg/xml/sax/Locator;

    .prologue
    .line 158
    return-void
.end method

.method public setEntry(Lcom/sec/android/gallery3d/remote/picasa/Entry;)V
    .locals 0
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mEntry:Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .line 66
    return-void
.end method

.method public setHandler(Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mHandler:Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;

    .line 70
    return-void
.end method

.method public skippedEntity(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 162
    return-void
.end method

.method public startDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 166
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 103
    :goto_0
    return-void

    .line 82
    :pswitch_0
    const-string v0, "http://www.w3.org/2005/Atom"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "feed"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mState:I

    goto :goto_0

    .line 85
    :cond_0
    new-instance v0, Lorg/xml/sax/SAXException;

    invoke-direct {v0}, Lorg/xml/sax/SAXException;-><init>()V

    throw v0

    .line 90
    :pswitch_1
    const-string v0, "http://www.w3.org/2005/Atom"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "entry"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mState:I

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->mEntry:Lcom/sec/android/gallery3d/remote/picasa/Entry;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/Entry;->clear()V

    goto :goto_0

    .line 94
    :cond_1
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->startProperty(Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    .line 98
    :pswitch_2
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->startProperty(Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 174
    return-void
.end method
