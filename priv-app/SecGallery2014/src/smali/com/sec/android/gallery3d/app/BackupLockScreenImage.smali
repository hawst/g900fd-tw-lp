.class public Lcom/sec/android/gallery3d/app/BackupLockScreenImage;
.super Landroid/app/IntentService;
.source "BackupLockScreenImage.java"


# static fields
.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;

.field private static final fileName:Ljava/lang/String; = "lockscreen_wallpaper"

.field private static final fileNameRipple:Ljava/lang/String; = "lockscreen_wallpaper_ripple"


# instance fields
.field cr:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->TAG:Ljava/lang/String;

    .line 23
    const-string v0, "content://com.sec.android.sCloudWallpaperBackupProvider/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "BackupLockScreenImage"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method private getLockScreen(Ljava/lang/String;)V
    .locals 14
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 90
    const/4 v6, 0x0

    .line 91
    .local v6, "fis":Ljava/io/FileInputStream;
    const/4 v8, 0x0

    .line 92
    .local v8, "fos":Ljava/io/FileOutputStream;
    new-instance v3, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v13}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".jpg"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v3, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 94
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 96
    :try_start_0
    sget-object v12, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v12

    const-string v13, "filename"

    invoke-virtual {v12, v13, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 98
    .local v5, "fileuri":Landroid/net/Uri;
    iget-object v12, p0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->cr:Landroid/content/ContentResolver;

    const/4 v13, 0x0

    invoke-virtual {v12, v5, v13}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 99
    .local v4, "fileOp":Landroid/os/ParcelFileDescriptor;
    if-nez v4, :cond_1

    .line 117
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 118
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 121
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 101
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v12

    invoke-direct {v9, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .local v9, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 103
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    if-eqz v7, :cond_2

    .line 105
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileInputStream;->available()I

    move-result v11

    .line 106
    .local v11, "size":I
    new-array v0, v11, [B

    .line 108
    .local v0, "buffer":[B
    invoke-virtual {v7, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    .line 109
    .local v10, "len":I
    const/4 v12, 0x0

    invoke-virtual {v9, v0, v12, v10}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 117
    .end local v0    # "buffer":[B
    .end local v10    # "len":I
    .end local v11    # "size":I
    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 118
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .line 119
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 111
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 112
    .local v2, "e1":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 117
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 118
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 113
    .end local v2    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 114
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    sget-object v12, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->TAG:Ljava/lang/String;

    const-string v13, "IOException occurred "

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 117
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 118
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .line 117
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v12

    :goto_3
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 118
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v12

    .line 117
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v12

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v12

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 113
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v1

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 111
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v2

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v2

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private setLockScreen(Ljava/lang/String;)V
    .locals 16
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 34
    sget-object v12, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "trying to copy the file here"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    const/4 v6, 0x0

    .line 36
    .local v6, "fis":Ljava/io/FileInputStream;
    const/4 v8, 0x0

    .line 38
    .local v8, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    sget-object v12, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v12}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v12

    const-string v13, "filename"

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 39
    .local v5, "fileuri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->cr:Landroid/content/ContentResolver;

    const/4 v13, 0x0

    invoke-virtual {v12, v5, v13}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 40
    .local v4, "fileOp":Landroid/os/ParcelFileDescriptor;
    if-nez v4, :cond_0

    .line 84
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 85
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 87
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 42
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    :cond_0
    :try_start_1
    new-instance v7, Ljava/io/FileInputStream;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    if-eqz v7, :cond_4

    .line 44
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileInputStream;->available()I

    move-result v11

    .line 45
    .local v11, "length":I
    if-gtz v11, :cond_1

    .line 46
    sget-object v12, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->TAG:Ljava/lang/String;

    const-string v13, "Corrupt file.. Need to check !!"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 85
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 49
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :cond_1
    :try_start_3
    new-instance v3, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v13}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".jpg"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v3, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 50
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_2

    .line 51
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 53
    :cond_2
    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Ljava/io/File;->setReadable(ZZ)Z

    .line 54
    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Ljava/io/File;->setWritable(ZZ)Z

    .line 56
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 57
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .local v9, "fos":Ljava/io/FileOutputStream;
    :try_start_4
    new-array v1, v11, [B

    .line 58
    .local v1, "buffer":[B
    :goto_1
    invoke-virtual {v7, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v11

    if-lez v11, :cond_3

    .line 59
    const/4 v12, 0x0

    invoke-virtual {v9, v1, v12, v11}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 79
    .end local v1    # "buffer":[B
    :catch_0
    move-exception v2

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .line 80
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "length":I
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 84
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 85
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "length":I
    :cond_3
    move-object v8, v9

    .line 62
    .end local v1    # "buffer":[B
    .end local v3    # "file":Ljava/io/File;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "length":I
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :cond_4
    :try_start_6
    const-string v12, "lockscreen_wallpaper_ripple"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "lockscreen_wallpaper_path_ripple"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getFilesDir()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "lockscreen_wallpaper_ripple"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".jpg"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 70
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "lockscreen_wallpaper"

    const/4 v14, 0x1

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "lockscreen_wallpaper_transparent"

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 76
    new-instance v10, Landroid/content/Intent;

    const-string v12, "com.sec.android.gallery3d.LOCKSCREEN_IMAGE_CHANGED"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    .local v10, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 84
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 85
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v6, v7

    .line 86
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 67
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "intent":Landroid/content/Intent;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :cond_5
    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "lockscreen_wallpaper_path"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getFilesDir()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".jpg"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_3

    .line 79
    :catch_1
    move-exception v2

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 81
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    :catch_2
    move-exception v2

    .line 82
    .local v2, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_8
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 84
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 85
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 84
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v12

    :goto_5
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 85
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v12

    .line 84
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .restart local v5    # "fileuri":Landroid/net/Uri;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v12

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "length":I
    :catchall_2
    move-exception v12

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 81
    .end local v3    # "file":Ljava/io/File;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "length":I
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v2

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "length":I
    :catch_4
    move-exception v2

    move-object v8, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 79
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileOp":Landroid/os/ParcelFileDescriptor;
    .end local v5    # "fileuri":Landroid/net/Uri;
    .end local v11    # "length":I
    :catch_5
    move-exception v2

    goto/16 :goto_2
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 125
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->cr:Landroid/content/ContentResolver;

    .line 127
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SET_LOCKSCREEN_ACTION_GALLERY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    sget-object v0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->TAG:Ljava/lang/String;

    const-string v1, "SET_LOCKSCREEN_ACTION_GALLERY received"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const-string v0, "lockscreen_wallpaper"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->setLockScreen(Ljava/lang/String;)V

    .line 131
    const-string v0, "lockscreen_wallpaper_ripple"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->setLockScreen(Ljava/lang/String;)V

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GET_LOCKSCREEN_ACTION_GALLERY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    sget-object v0, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->TAG:Ljava/lang/String;

    const-string v1, "GET_LOCKSCREEN_ACTION_GALLERY received"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const-string v0, "lockscreen_wallpaper"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getLockScreen(Ljava/lang/String;)V

    .line 135
    const-string v0, "lockscreen_wallpaper_ripple"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/BackupLockScreenImage;->getLockScreen(Ljava/lang/String;)V

    goto :goto_0
.end method
