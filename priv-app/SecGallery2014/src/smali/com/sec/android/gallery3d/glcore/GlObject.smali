.class public Lcom/sec/android/gallery3d/glcore/GlObject;
.super Ljava/lang/Object;
.source "GlObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;,
        Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;,
        Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;,
        Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;,
        Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;,
        Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;,
        Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;,
        Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;
    }
.end annotation


# static fields
.field public static final BLEND_TYPE_DEFAULT:I = 0x0

.field public static final BLEND_TYPE_SRC_ALPHA:I = 0x1

.field public static final BLEND_TYPE_SRC_ONE:I = 0x0

.field public static final CANVAS_MIX_ADD:I = 0x0

.field public static final CANVAS_MIX_ADJUST:I = 0x1

.field public static final CANVAS_MIX_DEFAULT:I = 0x0

.field static final MSG_HOVER_TIMOUT:I = 0x0

.field static final SCREEN_HORIZONTAL:I = 0x0

.field static final SCREEN_VERTICAL:I = 0x1

.field public static STATE_CREATED:I = 0x0

.field public static STATE_DESTROYED:I = 0x0

.field public static STATE_DESTROYING:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GlObject"

.field private static final VISIBLE_LEVEL:I = 0x8

.field private static final VISIBLE_MASK:I = 0xff

.field private static mPosMatrix:[F

.field private static mProjViewMatrix:[F

.field private static mTempMatrix:[F

.field private static mTempMatrix2:[F

.field private static mTexCoordArray:[F


# instance fields
.field private final DEF_CHILD_COUNT:I

.field private final MAX_ALPHA_COUNT:I

.field private final MAX_POS_COUNT:I

.field public isLacalDrag:Z

.field public mAlpha:F

.field public mAlphaCnt:I

.field public mAlphaEx:[F

.field public mAlphaInh:F

.field public mAnimation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlAnimationBase;",
            ">;"
        }
    .end annotation
.end field

.field private mBlendType:I

.field public mBorderColor:I

.field private mBorderMatrix:[F

.field private mBorderRect:Landroid/graphics/RectF;

.field private mBorderRectChanged:Z

.field public mBorderVisible:Z

.field public mBorderWidth:F

.field private mCanvasMixRatio:F

.field private mCanvasMixSrc:F

.field mCanvasSubFlags:I

.field mCanvasSubVisibility:Z

.field private mCenterOrigin:Z

.field public mCenterX:F

.field public mCenterY:F

.field public mCenterZ:F

.field public mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

.field public mChildCount:I

.field public mChildMax:I

.field public mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field public mClipRect:Landroid/graphics/Rect;

.field private mColorB:F

.field private mColorG:F

.field private mColorR:F

.field private mCustomTexCoord:Ljava/nio/FloatBuffer;

.field private mCustomTexCoordSub:Ljava/nio/FloatBuffer;

.field public mCx:[F

.field public mCy:[F

.field public mCz:[F

.field public mDimCnt:I

.field public mDimEx:[F

.field private mDimFactor:F

.field mDoneBind:Z

.field public mDragListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

.field public mEmptyFill:Z

.field public mEmptyFillBlue:I

.field public mEmptyFillColor:I

.field public mEmptyFillGreen:I

.field public mEmptyFillRed:I

.field private mExtChanged:Z

.field public mFuture:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation
.end field

.field public mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

.field public mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field public mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field public mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field public mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

.field public mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

.field public mGlViewSub:Lcom/sec/android/gallery3d/glcore/GlView;

.field public mHeight:F

.field private mHoverActive:Z

.field private mHoverEnter:Z

.field public mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

.field public mInhRotate:Z

.field public mInhScale:Z

.field public mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field public mLongClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

.field mMDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

.field mMPenSelectDetectorListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

.field public mMatrixDisp:[F

.field public mMatrixVal:[F

.field public mMatrixValEx:[F

.field public mMatrixValInh:[F

.field private mMixType:I

.field protected mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

.field public mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field mNeedLayout:Z

.field mNeedRender:Z

.field public mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

.field protected mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

.field public mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

.field public mPitch:F

.field public mPosChanged:Z

.field protected mPosMaxUsed:I

.field protected mPosUsed:[Z

.field public mPostDraw:Z

.field private mProjMatrixUpdate:Z

.field private mReqTexCoord:Z

.field private mReqTexCoordSub:Z

.field public mRoll:F

.field public mRotationListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

.field public mScaleX:F

.field public mScaleY:F

.field public mState:I

.field public mTextCoordSub:Landroid/graphics/RectF;

.field public mTextCoords:Landroid/graphics/RectF;

.field public mTextureUtils:Lcom/sec/android/gallery3d/glcore/TUtils;

.field public mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

.field public mUseClipping:Z

.field public mUseRotationGesture:Z

.field public mVertexRotation:I

.field public mVertexRotationSub:I

.field mVisible:Z

.field mVisibleEx:I

.field public mWidth:F

.field private mXlt:F

.field private mXrb:F

.field public mYaw:F

.field private mYlt:F

.field private mYrb:F

.field public mZOrder:I

.field public mx:F

.field public my:F

.field public mz:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 28
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    .line 29
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYING:I

    .line 30
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYED:I

    .line 119
    new-array v0, v1, [F

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjViewMatrix:[F

    .line 120
    new-array v0, v1, [F

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    .line 123
    new-array v0, v1, [F

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    .line 125
    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTexCoordArray:[F

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V
    .locals 10
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v3, 0x5

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->MAX_POS_COUNT:I

    .line 36
    const/4 v3, 0x5

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->MAX_ALPHA_COUNT:I

    .line 37
    const/16 v3, 0x8

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->DEF_CHILD_COUNT:I

    .line 50
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 52
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 53
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlViewSub:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 54
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 55
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 57
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 58
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 61
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 62
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    .line 63
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDoneBind:Z

    .line 64
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 65
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 66
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 67
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubVisibility:Z

    .line 68
    const/16 v3, 0xff

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    .line 69
    const/16 v3, 0xff

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubFlags:I

    .line 70
    sget v3, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 74
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mZOrder:I

    .line 80
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterX:F

    .line 81
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterY:F

    .line 82
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterZ:F

    .line 83
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterOrigin:Z

    .line 90
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRectChanged:Z

    .line 91
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mUseClipping:Z

    .line 92
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverEnter:Z

    .line 93
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPostDraw:Z

    .line 94
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mUseRotationGesture:Z

    .line 95
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    .line 96
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    .line 97
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    .line 98
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    .line 99
    const/4 v3, 0x5

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    .line 100
    const/4 v3, 0x5

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    .line 101
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    .line 102
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    .line 103
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    .line 104
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderVisible:Z

    .line 105
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFill:Z

    .line 106
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhScale:Z

    .line 107
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhRotate:Z

    .line 108
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderWidth:F

    .line 109
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderColor:I

    .line 110
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_0

    const v3, -0x131314

    :goto_0
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    .line 111
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    shr-int/lit8 v3, v3, 0x8

    const v4, 0xff00

    and-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillRed:I

    .line 112
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillGreen:I

    .line 113
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    const v4, 0xff00

    and-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillBlue:I

    .line 114
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    .line 115
    const/16 v3, 0x10

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    .line 116
    const/16 v3, 0x10

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    .line 117
    const/16 v3, 0x10

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValEx:[F

    .line 118
    const/16 v3, 0x10

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixDisp:[F

    .line 121
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    .line 127
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTextCoords:Landroid/graphics/RectF;

    .line 128
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTextCoordSub:Landroid/graphics/RectF;

    .line 130
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClipRect:Landroid/graphics/Rect;

    .line 131
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRect:Landroid/graphics/RectF;

    .line 133
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    .line 134
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorR:F

    .line 135
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorG:F

    .line 136
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorB:F

    .line 137
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    .line 138
    iput v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    .line 139
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotation:I

    .line 140
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotationSub:I

    .line 142
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 143
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    .line 144
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 145
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDragListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    .line 146
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mLongClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 147
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 148
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .line 149
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRotationListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

    .line 150
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 151
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    .line 155
    iput-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 161
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    .line 162
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->isLacalDrag:Z

    .line 164
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBlendType:I

    .line 165
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMixType:I

    .line 1786
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlObject$1;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/glcore/GlObject$1;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .line 1851
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlObject$2;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/glcore/GlObject$2;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMPenSelectDetectorListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    .line 168
    const/4 v1, 0x5

    .line 170
    .local v1, "maxPosCount":I
    iput-object p0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 171
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    .line 172
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    .line 173
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 175
    new-array v3, v1, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    .line 176
    new-array v3, v1, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    .line 177
    new-array v3, v1, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    .line 178
    new-array v2, v1, [Z

    .line 179
    .local v2, "posUsed":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 180
    aput-boolean v5, v2, v0

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 110
    .end local v0    # "i":I
    .end local v1    # "maxPosCount":I
    .end local v2    # "posUsed":[Z
    :cond_0
    const v3, -0xd0d0d1

    goto/16 :goto_0

    .line 182
    .restart local v0    # "i":I
    .restart local v1    # "maxPosCount":I
    .restart local v2    # "posUsed":[Z
    :cond_1
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mx:F

    .line 183
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->my:F

    .line 184
    iput v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mz:F

    .line 185
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput v8, v3, v5

    .line 186
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput v8, v3, v5

    .line 187
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput v8, v3, v5

    .line 188
    aput-boolean v9, v2, v5

    .line 189
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    .line 190
    iput v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    .line 191
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 192
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aput v7, v3, v5

    .line 193
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aput v7, v3, v5

    .line 194
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 195
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 196
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoord:Z

    .line 197
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoordSub:Z

    .line 199
    if-eqz p1, :cond_2

    .line 200
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->addObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 201
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-static {v3}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->getInstance(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 202
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-static {v3}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->getInstance(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    .line 203
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 204
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v3, :cond_2

    .line 205
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v9, v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 206
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v9, v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 207
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTextureUtils:Lcom/sec/android/gallery3d/glcore/TUtils;

    .line 208
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/TUtils;->mCoordBuffer:Ljava/nio/FloatBuffer;

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoord:Ljava/nio/FloatBuffer;

    .line 209
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoord:Ljava/nio/FloatBuffer;

    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoordSub:Ljava/nio/FloatBuffer;

    .line 212
    :cond_2
    return-void
.end method

.method private setBorderRectMatrix()V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 417
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRect:Landroid/graphics/RectF;

    .line 418
    .local v2, "nRect":Landroid/graphics/RectF;
    iget v5, v2, Landroid/graphics/RectF;->right:F

    iget v6, v2, Landroid/graphics/RectF;->left:F

    sub-float v3, v5, v6

    .line 419
    .local v3, "scaleX":F
    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iget v6, v2, Landroid/graphics/RectF;->top:F

    sub-float v4, v5, v6

    .line 420
    .local v4, "scaleY":F
    iget v5, v2, Landroid/graphics/RectF;->right:F

    iget v6, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v6

    div-float v0, v5, v8

    .line 421
    .local v0, "centerX":F
    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    iget v6, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v6

    div-float v1, v5, v8

    .line 423
    .local v1, "centerY":F
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    if-nez v5, :cond_0

    .line 424
    const/16 v5, 0x20

    new-array v5, v5, [F

    iput-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    .line 426
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    invoke-static {v5, v7}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 427
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    const/4 v6, 0x0

    invoke-static {v5, v7, v0, v1, v6}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 428
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v5, v7, v3, v4, v6}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 429
    return-void
.end method


# virtual methods
.method public addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I
    .locals 7
    .param p1, "child"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/16 v5, 0x8

    const/4 v6, 0x0

    .line 253
    iget-object v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 254
    .local v2, "parent":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v2, :cond_0

    .line 255
    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeChild(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    .line 258
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v4, :cond_1

    .line 259
    new-array v4, v5, [Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 260
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    .line 261
    const/4 v0, 0x0

    .line 274
    .local v0, "addPos":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aput-object p1, v4, v0

    .line 275
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 276
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 277
    return v0

    .line 262
    .end local v0    # "addPos":I
    :cond_1
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    if-lt v4, v5, :cond_2

    .line 264
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    int-to-float v4, v4

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float/2addr v4, v5

    float-to-int v1, v4

    .line 265
    .local v1, "newLen":I
    new-array v3, v1, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 267
    .local v3, "temp":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 268
    iput-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 269
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    .line 270
    .restart local v0    # "addPos":I
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    goto :goto_0

    .line 272
    .end local v0    # "addPos":I
    .end local v1    # "newLen":I
    .end local v3    # "temp":[Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .restart local v0    # "addPos":I
    goto :goto_0
.end method

.method public addPos(II)V
    .locals 6
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 951
    const/4 v0, 0x0

    .line 953
    .local v0, "posChangedZ":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_2

    .line 954
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v2, v1, p2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v3, v3, p1

    add-float/2addr v2, v3

    aput v2, v1, p2

    .line 955
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v2, v1, p2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v3, v3, p1

    add-float/2addr v2, v3

    aput v2, v1, p2

    .line 956
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v2, v1, p2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v3, v3, p1

    add-float/2addr v2, v3

    aput v2, v1, p2

    .line 957
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_1

    .line 958
    const/4 v0, 0x1

    .line 960
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 961
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_2

    .line 962
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v5, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 963
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    or-int/2addr v2, v0

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 964
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_2

    .line 965
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 969
    :cond_2
    return-void
.end method

.method public applyRendered(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 2
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const/4 v1, 0x0

    .line 2222
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2223
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 2224
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 2225
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDoneBind:Z

    .line 2226
    return-void
.end method

.method public cancelAsyncRender()V
    .locals 2

    .prologue
    .line 2229
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    sget v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_RUNNING:I

    if-ne v0, v1, :cond_0

    .line 2230
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    sget v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_SUSPENDED:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 2232
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->cancelRenderObjectInThreadPool(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 2234
    :cond_0
    return-void
.end method

.method public checkPosIn(II)Z
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1930
    const/4 v8, 0x1

    .line 1932
    .local v8, "point":I
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    if-eqz v0, :cond_3

    .line 1933
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v7

    .line 1934
    .local v7, "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGlConfig()Lcom/sec/android/gallery3d/glcore/GlConfig;

    move-result-object v6

    .line 1935
    .local v6, "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlConfig;->getProspectMatrix()[F

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix2:[F

    .line 1936
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjViewMatrix:[F

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix2:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixDisp:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1937
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1938
    const/4 v8, 0x0

    .line 1939
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v1, 0xc

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    aput v2, v0, v1

    .line 1940
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v1, 0xd

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    aput v2, v0, v1

    .line 1941
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v1, 0xe

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    aput v2, v0, v1

    .line 1942
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjViewMatrix:[F

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1943
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v1, 0xf

    aget v0, v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1971
    .end local v6    # "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    .end local v7    # "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    :goto_0
    return v0

    .line 1944
    .restart local v6    # "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    .restart local v7    # "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    :cond_0
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xc

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xf

    aget v2, v2, v3

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    .line 1945
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xd

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xf

    aget v2, v2, v3

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    .line 1947
    const/4 v8, 0x2

    .line 1948
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v1, 0xc

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    aput v2, v0, v1

    .line 1949
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v1, 0xd

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    aput v2, v0, v1

    .line 1950
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v1, 0xe

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    aput v2, v0, v1

    .line 1951
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjViewMatrix:[F

    const/4 v3, 0x0

    sget-object v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1952
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v1, 0xf

    aget v0, v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1953
    :cond_1
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xc

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xf

    aget v2, v2, v3

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    .line 1954
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xd

    aget v2, v2, v3

    mul-float/2addr v1, v2

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    const/16 v3, 0xf

    aget v2, v2, v3

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    .line 1956
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 1957
    iget v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    .line 1958
    .local v9, "tmp":F
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    .line 1959
    iput v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    .line 1961
    .end local v9    # "tmp":F
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 1962
    iget v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    .line 1963
    .restart local v9    # "tmp":F
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    .line 1964
    iput v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    .line 1967
    .end local v6    # "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    .end local v7    # "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    .end local v9    # "tmp":F
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    int-to-float v1, p1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    int-to-float v0, p1

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    int-to-float v1, p2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    int-to-float v0, p2

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    .line 1969
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1971
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public clearHoveringState()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1697
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    .line 1698
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverEnter:Z

    .line 1699
    return-void
.end method

.method public copyPos(II)V
    .locals 5
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 930
    const/4 v0, 0x0

    .line 932
    .local v0, "posChangedZ":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    .line 933
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v2, v2, p1

    aput v2, v1, p2

    .line 934
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v2, v2, p1

    aput v2, v1, p2

    .line 935
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v2, v2, p1

    aput v2, v1, p2

    .line 936
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, p1

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_1

    .line 937
    const/4 v0, 0x1

    .line 939
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 940
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_2

    .line 941
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v4, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 942
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    or-int/2addr v2, v0

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 943
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_2

    .line 944
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 948
    :cond_2
    return-void
.end method

.method protected dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v8, 0x0

    .line 2046
    const/4 v5, 0x0

    .line 2047
    .local v5, "retCode":Z
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v9

    float-to-int v6, v9

    .line 2048
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v9

    float-to-int v7, v9

    .line 2050
    .local v7, "y":I
    :try_start_0
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2051
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 2052
    .local v0, "clipData":Landroid/content/ClipData;
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v9

    const-string v10, "cropUri"

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v9

    const-string v10, "selectedUri"

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2053
    :cond_0
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->isLacalDrag:Z

    .line 2057
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    .line 2058
    .local v1, "count":I
    if-lez v1, :cond_2

    .line 2059
    const/4 v3, 0x0

    .line 2060
    .local v3, "index":I
    invoke-virtual {v0, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    .line 2061
    .local v4, "item":Landroid/content/ClipData$Item;
    iget-object v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDragListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    iget-object v10, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;->onDrop(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/net/Uri;)Z

    move-result v5

    .end local v0    # "clipData":Landroid/content/ClipData;
    .end local v1    # "count":I
    .end local v3    # "index":I
    .end local v4    # "item":Landroid/content/ClipData$Item;
    :cond_2
    move v8, v5

    .line 2067
    :goto_1
    return v8

    .line 2054
    .restart local v0    # "clipData":Landroid/content/ClipData;
    :cond_3
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v9

    const-string v10, "galleryUri"

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2055
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->isLacalDrag:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2064
    .end local v0    # "clipData":Landroid/content/ClipData;
    :catch_0
    move-exception v2

    .line 2065
    .local v2, "e":Ljava/lang/NullPointerException;
    goto :goto_1
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v7, 0xa

    const/4 v1, 0x0

    .line 2097
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    if-nez v4, :cond_1

    .line 2122
    :cond_0
    :goto_0
    return v1

    .line 2100
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2101
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 2102
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 2105
    .local v3, "y":I
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2106
    const-string v4, "GlObject"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DispathcHoverEvent Enter : Action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mHoverEnter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverEnter:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2107
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 2108
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverEnter:Z

    if-eqz v4, :cond_0

    .line 2109
    invoke-virtual {p0, v7, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->onHover(III)Z

    goto :goto_0

    .line 2113
    :cond_2
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverEnter:Z

    .line 2114
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    neg-float v4, v4

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    neg-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2115
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->onHover(III)Z

    move-result v1

    .line 2116
    .local v1, "ret":Z
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto :goto_0

    .line 2119
    .end local v1    # "ret":Z
    :cond_3
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverEnter:Z

    if-eqz v4, :cond_0

    .line 2120
    invoke-virtual {p0, v7, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->onHover(III)Z

    goto :goto_0
.end method

.method protected dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2071
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 2072
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 2073
    .local v3, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2076
    .local v0, "action":I
    if-eqz v0, :cond_0

    const/16 v4, 0x3e8

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3ea

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3eb

    if-ne v0, v4, :cond_2

    .line 2078
    :cond_0
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2079
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    neg-float v4, v4

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    neg-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2080
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 2081
    .local v1, "ret":Z
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2093
    .end local v1    # "ret":Z
    :goto_0
    return v1

    .line 2084
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2086
    :cond_2
    const/16 v4, 0xd3

    if-eq v0, v4, :cond_3

    const/16 v4, 0xd4

    if-eq v0, v4, :cond_3

    const/16 v4, 0xd5

    if-ne v0, v4, :cond_4

    .line 2087
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 2088
    .restart local v1    # "ret":Z
    goto :goto_0

    .line 2090
    .end local v1    # "ret":Z
    :cond_4
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    neg-float v4, v4

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    neg-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2091
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 2092
    .restart local v1    # "ret":Z
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto :goto_0
.end method

.method public draw(Ljavax/microedition/khronos/opengles/GL11;ZZ)V
    .locals 11
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p2, "procMatrix"    # Z
    .param p3, "procDisp"    # Z

    .prologue
    .line 436
    if-eqz p2, :cond_7

    .line 437
    iget-object v10, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 438
    .local v10, "parent":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    if-eqz v0, :cond_6

    .line 439
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 441
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 442
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterOrigin:Z

    if-eqz v0, :cond_e

    .line 443
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mx:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->my:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mz:F

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 446
    :goto_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 448
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 449
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 450
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 451
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 452
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    .line 453
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 455
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhRotate:Z

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhScale:Z

    if-eqz v0, :cond_f

    .line 456
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 472
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 473
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterOrigin:Z

    if-nez v0, :cond_6

    .line 474
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterX:F

    neg-float v2, v2

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterY:F

    neg-float v3, v3

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterZ:F

    neg-float v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 477
    :cond_6
    if-eqz v10, :cond_14

    .line 478
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixDisp:[F

    const/4 v1, 0x0

    iget-object v2, v10, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValEx:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 479
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValEx:[F

    const/4 v1, 0x0

    iget-object v2, v10, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValEx:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 480
    iget v0, v10, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    .line 487
    .end local v10    # "parent":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_7
    :goto_2
    if-eqz p3, :cond_d

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    if-eqz v0, :cond_d

    .line 488
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 489
    .local v7, "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    iget-object v8, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 491
    .local v8, "glCanvasSub":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixDisp:[F

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 492
    if-eqz v7, :cond_8

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFill:Z

    if-eqz v0, :cond_15

    .line 493
    :cond_8
    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 494
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mBlendType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_9

    .line 496
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->setBlendType(I)V

    .line 498
    :cond_9
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillRed:I

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillGreen:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillBlue:I

    const v3, 0x477fff00    # 65535.0f

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glColor4x(IIII)V

    .line 499
    const/4 v0, 0x6

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 500
    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 542
    :cond_a
    :goto_3
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderVisible:Z

    if-eqz v0, :cond_d

    .line 543
    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 544
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderColor:I

    shr-int/lit8 v0, v0, 0x8

    const v1, 0xff00

    and-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderColor:I

    const v2, 0xff00

    and-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderColor:I

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    const v3, 0x477fff00    # 65535.0f

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glColor4x(IIII)V

    .line 545
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_c

    .line 546
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRectChanged:Z

    if-eqz v0, :cond_b

    .line 547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRectChanged:Z

    .line 548
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setBorderRectMatrix()V

    .line 550
    :cond_b
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixDisp:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderMatrix:[F

    const/16 v1, 0x10

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 554
    :cond_c
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderWidth:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->setBorderWidth(F)V

    .line 555
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderWidth:F

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glPointSize(F)V

    .line 556
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 557
    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 558
    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 561
    .end local v7    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .end local v8    # "glCanvasSub":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    :cond_d
    return-void

    .line 445
    .restart local v10    # "parent":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_e
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mx:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterX:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->my:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterY:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mz:F

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterZ:F

    add-float/2addr v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    goto/16 :goto_0

    .line 458
    :cond_f
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mx:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->my:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mz:F

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 460
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhRotate:Z

    if-eqz v0, :cond_12

    .line 461
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_10

    .line 462
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 463
    :cond_10
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_11

    .line 464
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 465
    :cond_11
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_12

    .line 466
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 468
    :cond_12
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhScale:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_13

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    .line 469
    :cond_13
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    goto/16 :goto_1

    .line 482
    :cond_14
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixVal:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixDisp:[F

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 483
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValInh:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixValEx:[F

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 484
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    goto/16 :goto_2

    .line 502
    .end local v10    # "parent":Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local v7    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .restart local v8    # "glCanvasSub":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    :cond_15
    iget v0, v7, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    sget v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_NONE:I

    if-eq v0, v1, :cond_1c

    .line 503
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->onAsyncRender(Ljavax/microedition/khronos/opengles/GL11;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 508
    :goto_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mBlendType:I

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBlendType:I

    if-eq v0, v1, :cond_16

    .line 509
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBlendType:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->setBlendType(I)V

    .line 511
    :cond_16
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mVertexRotation:I

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotation:I

    if-eq v0, v1, :cond_17

    .line 512
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotation:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->setVertexRotation(I)V

    .line 514
    :cond_17
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    mul-float v6, v0, v1

    .line 515
    .local v6, "factor":F
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    mul-float v9, v6, v0

    .line 516
    .local v9, "mixFactor":F
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorR:F

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorG:F

    mul-float/2addr v1, v9

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorB:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    mul-float/2addr v3, v4

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    .line 517
    const/16 v0, 0xde1

    iget v1, v7, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mTextureId:I

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 518
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoord:Z

    if-eqz v0, :cond_18

    .line 519
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoord:Z

    .line 520
    iget v0, v7, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    int-to-float v0, v0

    iget v1, v7, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    int-to-float v1, v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCustomTexCoord(FFZ)V

    .line 522
    :cond_18
    const/4 v0, 0x2

    const/16 v1, 0x1406

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoord:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 523
    const/4 v0, 0x6

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 524
    if-eqz v8, :cond_a

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubVisibility:Z

    if-eqz v0, :cond_a

    .line 525
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_19

    .line 526
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    mul-float v9, v6, v0

    .line 527
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorR:F

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorG:F

    mul-float/2addr v1, v9

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorB:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaInh:F

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    mul-float/2addr v3, v4

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glColor4f(FFFF)V

    .line 529
    :cond_19
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->mVertexRotation:I

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotationSub:I

    if-eq v0, v1, :cond_1a

    .line 530
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlState:Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotationSub:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView$GlState;->setVertexRotation(I)V

    .line 532
    :cond_1a
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoordSub:Z

    if-eqz v0, :cond_1b

    .line 533
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoordSub:Z

    .line 534
    iget v0, v8, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    int-to-float v0, v0

    iget v1, v8, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCustomTexCoord(FFZ)V

    .line 536
    :cond_1b
    const/4 v0, 0x2

    const/16 v1, 0x1406

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoordSub:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 537
    const/16 v0, 0xde1

    iget v1, v8, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mTextureId:I

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 538
    const/4 v0, 0x6

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    goto/16 :goto_3

    .line 506
    .end local v6    # "factor":F
    .end local v9    # "mixFactor":F
    :cond_1c
    invoke-virtual {p0, p1, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->onRender(Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto/16 :goto_4
.end method

.method public forceRender()V
    .locals 2

    .prologue
    .line 2136
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    .line 2137
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->prepare(Z)V

    .line 2138
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLContext()Ljavax/microedition/khronos/opengles/GL11;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2140
    :cond_0
    return-void
.end method

.method public getAbsX()F
    .locals 5

    .prologue
    .line 634
    move-object v1, p0

    .line 635
    .local v1, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v2, 0x0

    .line 638
    .local v2, "sx":F
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v4, v4, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq v1, v4, :cond_3

    .line 639
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v4, :cond_2

    .line 640
    iget-object v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_1

    .line 641
    iget-object v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v4, v4, v0

    add-float/2addr v2, v4

    .line 639
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 643
    :cond_2
    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 644
    if-nez v1, :cond_0

    move v3, v2

    .line 647
    .end local v0    # "i":I
    .end local v2    # "sx":F
    .local v3, "sx":F
    :goto_1
    return v3

    .end local v3    # "sx":F
    .restart local v2    # "sx":F
    :cond_3
    move v3, v2

    .end local v2    # "sx":F
    .restart local v3    # "sx":F
    goto :goto_1
.end method

.method public getAbsY()F
    .locals 5

    .prologue
    .line 651
    move-object v1, p0

    .line 652
    .local v1, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v2, 0x0

    .line 655
    .local v2, "sy":F
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v4, v4, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq v1, v4, :cond_3

    .line 656
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v4, :cond_2

    .line 657
    iget-object v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_1

    .line 658
    iget-object v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v4, v4, v0

    add-float/2addr v2, v4

    .line 656
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 660
    :cond_2
    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 661
    if-nez v1, :cond_0

    move v3, v2

    .line 664
    .end local v0    # "i":I
    .end local v2    # "sy":F
    .local v3, "sy":F
    :goto_1
    return v3

    .end local v3    # "sy":F
    .restart local v2    # "sy":F
    :cond_3
    move v3, v2

    .end local v2    # "sy":F
    .restart local v3    # "sy":F
    goto :goto_1
.end method

.method public getAbsZ()F
    .locals 5

    .prologue
    .line 668
    move-object v1, p0

    .line 669
    .local v1, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v2, 0x0

    .line 672
    .local v2, "sz":F
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v4, v4, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq v1, v4, :cond_3

    .line 673
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v4, :cond_2

    .line 674
    iget-object v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_1

    .line 675
    iget-object v4, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v4, v4, v0

    add-float/2addr v2, v4

    .line 673
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 677
    :cond_2
    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 678
    if-nez v1, :cond_0

    move v3, v2

    .line 681
    .end local v0    # "i":I
    .end local v2    # "sz":F
    .local v3, "sz":F
    :goto_1
    return v3

    .end local v3    # "sz":F
    .restart local v2    # "sz":F
    :cond_3
    move v3, v2

    .end local v2    # "sz":F
    .restart local v3    # "sz":F
    goto :goto_1
.end method

.method public getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    return-object v0
.end method

.method public getAlpha()F
    .locals 1

    .prologue
    .line 1342
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    return v0
.end method

.method public getAlpha(I)F
    .locals 1
    .param p1, "hnd"    # I

    .prologue
    .line 1046
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    if-le p1, v0, :cond_0

    .line 1047
    const/4 v0, 0x0

    .line 1049
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getAlphaHnd()I
    .locals 4

    .prologue
    .line 1055
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    if-gt v0, v1, :cond_1

    .line 1056
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aget v1, v1, v0

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 1061
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 1055
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1059
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    .line 1060
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    .line 1061
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    goto :goto_1
.end method

.method public getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .locals 1

    .prologue
    .line 1455
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    return-object v0
.end method

.method public getCanvasSub()Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .locals 1

    .prologue
    .line 1459
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    return-object v0
.end method

.method public getCenterX()F
    .locals 2

    .prologue
    .line 699
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getCenterY()F
    .locals 2

    .prologue
    .line 703
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getChildCount()I
    .locals 1

    .prologue
    .line 386
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    return v0
.end method

.method public getDX(I)F
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 733
    if-gez p1, :cond_1

    .line 738
    :cond_0
    :goto_0
    return v0

    .line 735
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getDY(I)F
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 742
    if-gez p1, :cond_1

    .line 747
    :cond_0
    :goto_0
    return v0

    .line 744
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    .line 745
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getDZ(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 751
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    .line 752
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v0, v0, p1

    .line 754
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDim()F
    .locals 1

    .prologue
    .line 1338
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    return v0
.end method

.method public getDim(I)F
    .locals 1
    .param p1, "hnd"    # I

    .prologue
    .line 1114
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    if-le p1, v0, :cond_0

    .line 1115
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1117
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getDimHnd()I
    .locals 4

    .prologue
    .line 1123
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    if-gt v0, v1, :cond_1

    .line 1124
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aget v1, v1, v0

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 1129
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 1123
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1127
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    .line 1128
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    .line 1129
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    goto :goto_1
.end method

.method public getGlObjectRect(Landroid/graphics/Rect;)V
    .locals 14
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v13, 0x0

    const/16 v12, 0xd

    const/16 v11, 0xc

    const/16 v10, 0xf

    const/4 v1, 0x0

    .line 2002
    const/4 v8, 0x1

    .line 2005
    .local v8, "point":I
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    if-eqz v0, :cond_3

    .line 2006
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLUtilInstance()Lcom/sec/android/gallery3d/glcore/TUtils;

    move-result-object v7

    .line 2007
    .local v7, "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGlConfig()Lcom/sec/android/gallery3d/glcore/GlConfig;

    move-result-object v6

    .line 2008
    .local v6, "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlConfig;->getProspectMatrix()[F

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix2:[F

    .line 2009
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjViewMatrix:[F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix2:[F

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMatrixDisp:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 2010
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 2011
    const/4 v8, 0x0

    .line 2012
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    aget v2, v2, v1

    aput v2, v0, v11

    .line 2013
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    aput v2, v0, v12

    .line 2014
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v2, 0xe

    iget-object v3, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    aput v3, v0, v2

    .line 2015
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjViewMatrix:[F

    sget-object v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 2016
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v0, v0, v10

    cmpl-float v0, v0, v13

    if-nez v0, :cond_1

    .line 2043
    .end local v6    # "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    .end local v7    # "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    :cond_0
    :goto_0
    return-void

    .line 2017
    .restart local v6    # "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    .restart local v7    # "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    :cond_1
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget v2, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    sget-object v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v3, v3, v11

    mul-float/2addr v2, v3

    sget-object v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v3, v3, v10

    div-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    .line 2018
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget v2, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    sget-object v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v3, v3, v12

    mul-float/2addr v2, v3

    sget-object v3, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v3, v3, v10

    div-float/2addr v2, v3

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    .line 2020
    const/4 v8, 0x2

    .line 2021
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    aput v2, v0, v11

    .line 2022
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    iget-object v2, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    aput v2, v0, v12

    .line 2023
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    const/16 v2, 0xe

    iget-object v3, v7, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    const/16 v4, 0x8

    aget v3, v3, v4

    aput v3, v0, v2

    .line 2024
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjViewMatrix:[F

    sget-object v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMatrix:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 2025
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v0, v0, v10

    cmpl-float v0, v0, v13

    if-eqz v0, :cond_0

    .line 2026
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v2, v2, v11

    mul-float/2addr v1, v2

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v2, v2, v10

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    .line 2027
    iget v0, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget v1, v6, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v2, v2, v12

    mul-float/2addr v1, v2

    sget-object v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mTempMatrix:[F

    aget v2, v2, v10

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    .line 2029
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 2030
    iget v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    .line 2031
    .local v9, "tmp":F
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    .line 2032
    iput v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    .line 2034
    .end local v9    # "tmp":F
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 2035
    iget v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    .line 2036
    .restart local v9    # "tmp":F
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    .line 2037
    iput v9, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    .line 2040
    .end local v6    # "glConfig":Lcom/sec/android/gallery3d/glcore/GlConfig;
    .end local v7    # "glUtil":Lcom/sec/android/gallery3d/glcore/TUtils;
    .end local v9    # "tmp":F
    :cond_3
    if-eqz p1, :cond_0

    .line 2041
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    float-to-int v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    float-to-int v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0
.end method

.method public getHeight(Z)F
    .locals 2
    .param p1, "includeScale"    # Z

    .prologue
    .line 764
    if-eqz p1, :cond_0

    .line 765
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    mul-float/2addr v0, v1

    .line 766
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    goto :goto_0
.end method

.method public getObjectRect()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1920
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1921
    .local v0, "r":Landroid/graphics/Rect;
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    float-to-int v3, v3

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1922
    return-object v0
.end method

.method public getParent()Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    return-object v0
.end method

.method public getPitch()F
    .locals 1

    .prologue
    .line 1350
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    return v0
.end method

.method public getProxity(II)F
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v6, 0x1

    .line 1979
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->getGlObjectRect(Landroid/graphics/Rect;)V

    .line 1981
    int-to-float v4, p1

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    const/4 v1, -0x1

    .line 1985
    .local v1, "xSide":I
    :goto_0
    int-to-float v4, p2

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    const/4 v3, -0x1

    .line 1989
    .local v3, "ySide":I
    :goto_1
    if-nez v1, :cond_4

    if-nez v3, :cond_4

    .line 1990
    const/4 v4, 0x0

    .line 1998
    :goto_2
    return v4

    .line 1982
    .end local v1    # "xSide":I
    .end local v3    # "ySide":I
    :cond_0
    int-to-float v4, p1

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    const/4 v1, 0x1

    .restart local v1    # "xSide":I
    goto :goto_0

    .line 1983
    .end local v1    # "xSide":I
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "xSide":I
    goto :goto_0

    .line 1986
    :cond_2
    int-to-float v4, p2

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    const/4 v3, 0x1

    .restart local v3    # "ySide":I
    goto :goto_1

    .line 1987
    .end local v3    # "ySide":I
    :cond_3
    const/4 v3, 0x0

    .restart local v3    # "ySide":I
    goto :goto_1

    .line 1992
    :cond_4
    if-ne v1, v6, :cond_5

    int-to-float v4, p1

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXrb:F

    sub-float v0, v4, v5

    .line 1995
    .local v0, "xDst":F
    :goto_3
    if-ne v3, v6, :cond_6

    int-to-float v4, p2

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYrb:F

    sub-float v2, v4, v5

    .line 1998
    .local v2, "yDst":F
    :goto_4
    mul-float v4, v0, v0

    mul-float v5, v2, v2

    add-float/2addr v4, v5

    goto :goto_2

    .line 1993
    .end local v0    # "xDst":F
    .end local v2    # "yDst":F
    :cond_5
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    int-to-float v5, p1

    sub-float v0, v4, v5

    .restart local v0    # "xDst":F
    goto :goto_3

    .line 1996
    :cond_6
    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    int-to-float v5, p2

    sub-float v2, v4, v5

    .restart local v2    # "yDst":F
    goto :goto_4
.end method

.method public getRoll()F
    .locals 1

    .prologue
    .line 1354
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    return v0
.end method

.method public getScaleX()F
    .locals 1

    .prologue
    .line 1330
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    return v0
.end method

.method public getScaleY()F
    .locals 1

    .prologue
    .line 1334
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    return v0
.end method

.method public getView()Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1

    .prologue
    .line 1463
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    return-object v0
.end method

.method public getViewSub()Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlViewSub:Lcom/sec/android/gallery3d/glcore/GlView;

    return-object v0
.end method

.method public getVisibility()Z
    .locals 1

    .prologue
    .line 629
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    return v0
.end method

.method public getWidth(Z)F
    .locals 2
    .param p1, "includeScale"    # Z

    .prologue
    .line 758
    if-eqz p1, :cond_0

    .line 759
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    mul-float/2addr v0, v1

    .line 760
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    goto :goto_0
.end method

.method public getX()F
    .locals 3

    .prologue
    .line 689
    const/4 v1, 0x0

    .line 690
    .local v1, "sx":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v2, :cond_1

    .line 691
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 692
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    .line 690
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 695
    :cond_1
    return v1
.end method

.method public getXlt()F
    .locals 1

    .prologue
    .line 2289
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mXlt:F

    return v0
.end method

.method public getY()F
    .locals 3

    .prologue
    .line 710
    const/4 v1, 0x0

    .line 711
    .local v1, "sy":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v2, :cond_1

    .line 712
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 713
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    .line 711
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 716
    :cond_1
    return v1
.end method

.method public getYaw()F
    .locals 1

    .prologue
    .line 1346
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    return v0
.end method

.method public getYlt()F
    .locals 1

    .prologue
    .line 2292
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYlt:F

    return v0
.end method

.method public getZ()F
    .locals 3

    .prologue
    .line 723
    const/4 v1, 0x0

    .line 724
    .local v1, "sz":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v2, :cond_1

    .line 725
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 726
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    .line 724
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 729
    :cond_1
    return v1
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 2126
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    .line 2127
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    .line 2129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_1

    .line 2130
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    .line 2132
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 2133
    return-void
.end method

.method public isIncluded(FF)Z
    .locals 1
    .param p1, "cx"    # F
    .param p2, "cy"    # F

    .prologue
    .line 1358
    const/4 v0, 0x0

    return v0
.end method

.method public isSelectedAt(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1926
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 330
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v4, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v4, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    const/4 v5, 0x1

    if-gt v4, v5, :cond_1

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 334
    .local v0, "child":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 335
    .local v1, "count":I
    const/4 v2, -0x1

    .line 338
    .local v2, "detect":I
    aget-object v4, v0, v6

    if-eq v4, p0, :cond_0

    .line 341
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_2

    .line 342
    aget-object v4, v0, v3

    if-ne v4, p0, :cond_3

    .line 343
    move v2, v3

    .line 347
    :cond_2
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 349
    move v3, v2

    :goto_2
    if-lez v3, :cond_4

    .line 350
    add-int/lit8 v4, v3, -0x1

    aget-object v4, v0, v4

    aput-object v4, v0, v3

    .line 349
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 341
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 352
    :cond_4
    aput-object p0, v0, v6

    goto :goto_0
.end method

.method public moveToLast()V
    .locals 6

    .prologue
    .line 356
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v4, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v4, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    const/4 v5, 0x1

    if-gt v4, v5, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v0, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 360
    .local v0, "child":[Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, v4, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 361
    .local v1, "count":I
    const/4 v2, -0x1

    .line 364
    .local v2, "detect":I
    add-int/lit8 v4, v1, -0x1

    aget-object v4, v0, v4

    if-eq v4, p0, :cond_0

    .line 367
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    add-int/lit8 v4, v1, -0x1

    if-ge v3, v4, :cond_2

    .line 368
    aget-object v4, v0, v3

    if-ne v4, p0, :cond_3

    .line 369
    move v2, v3

    .line 373
    :cond_2
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 375
    move v3, v2

    :goto_2
    add-int/lit8 v4, v1, -0x1

    if-ge v3, v4, :cond_4

    .line 376
    add-int/lit8 v4, v3, 0x1

    aget-object v4, v0, v4

    aput-object v4, v0, v3

    .line 375
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 367
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 378
    :cond_4
    add-int/lit8 v4, v1, -0x1

    aput-object p0, v0, v4

    goto :goto_0
.end method

.method public onAsyncRender(Ljavax/microedition/khronos/opengles/GL11;)Z
    .locals 8
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const/16 v7, 0xde1

    const/4 v1, 0x0

    const v6, 0xff00

    .line 2174
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget v0, v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 2176
    .local v0, "state":I
    sget v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_RUNNING:I

    if-eq v0, v2, :cond_0

    sget v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_SUSPENDED:I

    if-ne v0, v2, :cond_1

    .line 2178
    :cond_0
    invoke-interface {p1, v7}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 2179
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    shr-int/lit8 v2, v2, 0x8

    and-int/2addr v2, v6

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    and-int/2addr v3, v6

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    shr-int/lit8 v5, v5, 0x10

    and-int/2addr v5, v6

    invoke-interface {p1, v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glColor4x(IIII)V

    .line 2180
    const/4 v2, 0x6

    const/4 v3, 0x4

    invoke-interface {p1, v2, v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 2181
    invoke-interface {p1, v7}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 2188
    :goto_0
    return v1

    .line 2184
    :cond_1
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_PROCESSED:I

    if-ne v0, v1, :cond_2

    .line 2185
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2186
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    sget v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_APPLIED:I

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 2188
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onClicked()Z
    .locals 1

    .prologue
    .line 1650
    const/4 v0, 0x0

    return v0
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 0
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 1627
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1618
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 1619
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 1620
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 1622
    :cond_0
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 1623
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    .line 1624
    return-void
.end method

.method protected onDraw(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 0
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 1630
    return-void
.end method

.method public onHover(III)Z
    .locals 5
    .param p1, "action"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 1674
    const/4 v0, 0x1

    .line 1675
    .local v0, "retVal":Z
    const-string v1, "GlObject"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Hover action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mHoverActive: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1677
    const/16 v1, 0x9

    if-eq p1, v1, :cond_0

    const/4 v1, 0x7

    if-ne p1, v1, :cond_3

    .line 1679
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    if-nez v1, :cond_2

    .line 1680
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;->onHoverEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    move-result v0

    .line 1681
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    .line 1693
    :cond_1
    :goto_0
    return v0

    .line 1682
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;->onHoverMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1683
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    goto :goto_0

    .line 1685
    :cond_3
    const/16 v1, 0xa

    if-ne p1, v1, :cond_1

    .line 1686
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    if-eqz v1, :cond_4

    .line 1687
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;->onHoverExit(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    .line 1689
    :cond_4
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverActive:Z

    .line 1690
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverEnter:Z

    .line 1691
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLongClicked()Z
    .locals 1

    .prologue
    .line 1654
    const/4 v0, 0x0

    return v0
.end method

.method protected onMoved(II)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 1641
    const/4 v0, 0x0

    return v0
.end method

.method protected onPostDraw(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 0
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 1633
    return-void
.end method

.method protected onPressed(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1637
    const/4 v0, 0x0

    return v0
.end method

.method protected onReleased(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 1645
    const/4 v0, 0x0

    return v0
.end method

.method public onRender(Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlCanvas;)V
    .locals 7
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p2, "glCanvas"    # Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2145
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    if-eqz v6, :cond_8

    :cond_0
    move v3, v5

    .line 2146
    .local v3, "updateLayout":Z
    :goto_0
    iget-boolean v6, p2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentValid:Z

    if-nez v6, :cond_9

    move v2, v5

    .line 2148
    .local v2, "updateCanvas":Z
    :goto_1
    if-nez v2, :cond_1

    if-eqz v3, :cond_3

    .line 2149
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 2150
    .local v1, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p2, v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->prepare(Z)V

    .line 2151
    if-eqz v1, :cond_2

    iget-object v5, p2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    if-eqz v5, :cond_2

    .line 2152
    iget v5, p2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    iget v6, p2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->layout(II)V

    .line 2153
    iget-object v5, p2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->render(Landroid/graphics/Canvas;)V

    .line 2155
    :cond_2
    invoke-virtual {p2, p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2157
    .end local v1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 2158
    .local v0, "glCanvasSub":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    if-eqz v0, :cond_6

    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubVisibility:Z

    if-eqz v5, :cond_6

    if-nez v3, :cond_4

    iget-boolean v5, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentValid:Z

    if-nez v5, :cond_6

    .line 2159
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlViewSub:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 2160
    .restart local v1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->prepare(Z)V

    .line 2161
    if-eqz v1, :cond_5

    iget-object v5, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    if-eqz v5, :cond_5

    .line 2162
    iget v5, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    iget v6, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->layout(II)V

    .line 2163
    iget-object v5, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->render(Landroid/graphics/Canvas;)V

    .line 2165
    :cond_5
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 2167
    .end local v1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_6
    if-eqz v3, :cond_7

    .line 2168
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 2169
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 2171
    :cond_7
    return-void

    .end local v0    # "glCanvasSub":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .end local v2    # "updateCanvas":Z
    .end local v3    # "updateLayout":Z
    :cond_8
    move v3, v4

    .line 2145
    goto :goto_0

    .restart local v3    # "updateLayout":Z
    :cond_9
    move v2, v4

    .line 2146
    goto :goto_1
.end method

.method protected onRotate(Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;)V
    .locals 0
    .param p1, "rotationDetector"    # Lcom/sec/android/gallery3d/glcore/GlRotationGestureDetector;

    .prologue
    .line 1671
    return-void
.end method

.method public onTouch(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1658
    const/4 v0, 0x0

    .line 1660
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v1, :cond_0

    .line 1661
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1662
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    if-eqz v1, :cond_1

    .line 1663
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->onTouch(Landroid/view/MotionEvent;Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1664
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    if-eqz v1, :cond_2

    .line 1665
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectDetect:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMPenSelectDetectorListener:Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector;->onTouch(Landroid/view/MotionEvent;Lcom/sec/android/gallery3d/glcore/GlPenSelectDetector$GlPenSelectDetectorListener;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1667
    :cond_2
    return v0
.end method

.method public procAsyncRender()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2194
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    if-eqz v6, :cond_3

    :cond_0
    move v3, v5

    .line 2195
    .local v3, "updateLayout":Z
    :goto_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-boolean v6, v6, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentValid:Z

    if-nez v6, :cond_4

    move v2, v5

    .line 2197
    .local v2, "updateCanvas":Z
    :goto_1
    if-nez v2, :cond_1

    if-eqz v3, :cond_7

    .line 2198
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 2199
    .local v0, "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 2200
    .local v1, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->prepare(Z)V

    .line 2201
    iget v5, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    sget v6, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_POSTPONED:I

    if-ne v5, v6, :cond_5

    .line 2202
    iput-boolean v4, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentValid:Z

    .line 2219
    .end local v0    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .end local v1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_2
    :goto_2
    return-void

    .end local v2    # "updateCanvas":Z
    .end local v3    # "updateLayout":Z
    :cond_3
    move v3, v4

    .line 2194
    goto :goto_0

    .restart local v3    # "updateLayout":Z
    :cond_4
    move v2, v4

    .line 2195
    goto :goto_1

    .line 2205
    .restart local v0    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .restart local v1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    .restart local v2    # "updateCanvas":Z
    :cond_5
    if-eqz v1, :cond_7

    .line 2206
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    if-eqz v5, :cond_6

    .line 2207
    iget v5, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    iget v6, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->layout(II)V

    .line 2209
    :cond_6
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    if-eqz v5, :cond_7

    .line 2210
    iget-object v5, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->render(Landroid/graphics/Canvas;)V

    .line 2214
    .end local v0    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .end local v1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_7
    if-eqz v3, :cond_2

    .line 2215
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 2216
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 2217
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    sget v5, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_PROCESSED:I

    iput v5, v4, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    goto :goto_2
.end method

.method public releasePosIndex(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 972
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 973
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ne v0, v1, :cond_0

    .line 974
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    .line 975
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 976
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_3

    .line 977
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v0, v0, p1

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v0, v0, p1

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v0, v0, p1

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 978
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput v2, v0, p1

    .line 979
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput v2, v0, p1

    .line 980
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput v2, v0, p1

    .line 982
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 983
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v3, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 984
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_3

    .line 985
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 988
    :cond_3
    return-void
.end method

.method public remove()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 217
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v2, :cond_3

    .line 218
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    sget v3, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_CREATED:I

    if-eq v2, v3, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    sget v2, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYING:I

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 222
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 223
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 224
    iput-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    .line 226
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->onDestroy()V

    .line 227
    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 228
    sget v2, Lcom/sec/android/gallery3d/glcore/GlObject;->STATE_DESTROYED:I

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mState:I

    .line 232
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v2, :cond_6

    .line 233
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    if-ge v1, v2, :cond_5

    .line 234
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v0, v2, v1

    .line 235
    .local v0, "child":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v0, :cond_4

    .line 233
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 230
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_1

    .line 237
    .restart local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local v1    # "i":I
    :cond_4
    iput-object v4, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 238
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    goto :goto_3

    .line 240
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_5
    iput-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 241
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 242
    iput v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildMax:I

    .line 244
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v2, :cond_0

    .line 245
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeChild(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    goto :goto_0
.end method

.method public removeAllAnimation()V
    .locals 1

    .prologue
    .line 1531
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_0

    .line 1532
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1533
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    .line 1537
    :goto_0
    return-void

    .line 1535
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_0
.end method

.method public removeAllChild()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 312
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-nez v2, :cond_1

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_3

    .line 318
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v0, v2, v1

    .line 319
    .local v0, "child":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aput-object v3, v2, v1

    .line 320
    iput-object v3, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 321
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq p0, v2, :cond_2

    .line 323
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 317
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 326
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_3
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    goto :goto_0
.end method

.method public removeAllCommand()V
    .locals 1

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 1553
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Ljava/lang/Object;)V

    .line 1555
    :cond_0
    return-void
.end method

.method public removeAllListeners()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1702
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 1703
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1704
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mLongClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 1705
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDragListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    .line 1706
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .line 1707
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 1708
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    .line 1709
    return-void
.end method

.method public removeAlphaHnd(I)V
    .locals 2
    .param p1, "hnd"    # I

    .prologue
    .line 1065
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    if-lez v0, :cond_0

    if-lez p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    if-le p1, v0, :cond_1

    .line 1072
    :cond_0
    :goto_0
    return-void

    .line 1068
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    if-ne p1, v0, :cond_2

    .line 1069
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    .line 1071
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    const/high16 v1, -0x40800000    # -1.0f

    aput v1, v0, p1

    goto :goto_0
.end method

.method public removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 1540
    if-nez p1, :cond_0

    .line 1549
    :goto_0
    return-void

    .line 1541
    :cond_0
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1542
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_2

    .line 1543
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1544
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1545
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    goto :goto_0

    .line 1547
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public removeChild(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 6
    .param p1, "child"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 282
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq v3, p0, :cond_1

    .line 307
    :cond_0
    :goto_0
    return v2

    .line 283
    :cond_1
    const/4 v1, -0x1

    .line 286
    .local v1, "removePos":I
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 287
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v3, v3, v0

    if-ne v3, p1, :cond_3

    .line 288
    move v1, v0

    .line 292
    :cond_2
    const/4 v3, -0x1

    if-eq v1, v3, :cond_6

    .line 293
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 294
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    aput-object v3, v2, v0

    .line 293
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 286
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 296
    :cond_4
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 297
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    aput-object v5, v2, v3

    .line 298
    iput-object v5, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 299
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq p0, v2, :cond_5

    .line 301
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 303
    :cond_5
    const/4 v2, 0x1

    goto :goto_0

    .line 306
    :cond_6
    const-string v3, "GlObject"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removeChild = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "(count = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") isn\'t exist !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeChildFromRootObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 6
    .param p1, "child"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v5, 0x0

    .line 395
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eq v2, p0, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    const/4 v1, -0x1

    .line 398
    .local v1, "removePos":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    if-ge v0, v2, :cond_2

    .line 399
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_3

    .line 400
    move v1, v0

    .line 404
    :cond_2
    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    .line 405
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 406
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    aput-object v3, v2, v0

    .line 405
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 398
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 408
    :cond_4
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    .line 409
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChild:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    aput-object v5, v2, v3

    .line 410
    iput-object v5, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    goto :goto_0

    .line 413
    :cond_5
    const-string v2, "GlObject"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeChildFromRootObject = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mChildCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") isn\'t exist !!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeCommand(I)V
    .locals 3
    .param p1, "command"    # I

    .prologue
    const/4 v2, 0x0

    .line 1611
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_0

    .line 1615
    :goto_0
    return-void

    .line 1612
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 1613
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 1614
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public removeDimHnd(I)V
    .locals 2
    .param p1, "hnd"    # I

    .prologue
    .line 1133
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    if-lez v0, :cond_0

    if-lez p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    if-le p1, v0, :cond_1

    .line 1140
    :cond_0
    :goto_0
    return-void

    .line 1136
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    if-ne p1, v0, :cond_2

    .line 1137
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    .line 1139
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    const/high16 v1, -0x40800000    # -1.0f

    aput v1, v0, p1

    goto :goto_0
.end method

.method public removePreLoadedCanvas()V
    .locals 1

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    .line 1363
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    .line 1364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 1366
    :cond_0
    return-void
.end method

.method public requestAsyncRender()V
    .locals 3

    .prologue
    .line 2237
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget v0, v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 2239
    .local v0, "state":I
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_PROCESSED:I

    if-ne v0, v1, :cond_1

    .line 2240
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->onFreeBitmap()V

    .line 2244
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRenderObjectInThreadPool(Lcom/sec/android/gallery3d/glcore/GlObject;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 2245
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    sget v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_RUNNING:I

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 2246
    return-void

    .line 2241
    :cond_1
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_RUNNING:I

    if-ne v0, v1, :cond_0

    .line 2242
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->cancelRenderObjectInThreadPool(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_0
.end method

.method public resetPos()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 991
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 992
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aput-boolean v4, v1, v0

    .line 993
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput v2, v1, v0

    .line 994
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput v2, v1, v0

    .line 995
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput v2, v1, v0

    .line 991
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 997
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aput-boolean v3, v1, v4

    .line 998
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    .line 999
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 1000
    return-void
.end method

.method public setAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 1014
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(FI)V

    .line 1015
    return-void
.end method

.method public setAlpha(FI)V
    .locals 7
    .param p1, "alpha"    # F
    .param p2, "hnd"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1018
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 1021
    .local v1, "oldVisible":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    if-gt p2, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aget v2, v2, p2

    cmpl-float v2, p1, v2

    if-nez v2, :cond_1

    .line 1043
    :cond_0
    :goto_0
    return-void

    .line 1024
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aput p1, v2, p2

    .line 1025
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aget v2, v2, v4

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    .line 1026
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaCnt:I

    if-gt v0, v2, :cond_3

    .line 1027
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aget v2, v2, v0

    cmpl-float v2, v2, v5

    if-ltz v2, :cond_2

    .line 1028
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlphaEx:[F

    aget v3, v3, v0

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    .line 1026
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1031
    :cond_3
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_4

    .line 1032
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 1037
    :goto_2
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_0

    .line 1038
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v6, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 1039
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_0

    .line 1040
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1033
    :cond_4
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    const/16 v3, 0xff

    if-ne v2, v3, :cond_5

    .line 1034
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    goto :goto_2

    .line 1036
    :cond_5
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    goto :goto_2
.end method

.method public setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 1520
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1521
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 1522
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1523
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    .line 1524
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1528
    :goto_0
    return-void

    .line 1526
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public setBlendType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 432
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBlendType:I

    .line 433
    return-void
.end method

.method public setBorderColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 1239
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderColor:I

    if-ne v0, p1, :cond_1

    .line 1245
    :cond_0
    :goto_0
    return-void

    .line 1241
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderColor:I

    .line 1242
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1243
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setBorderRect(Landroid/graphics/RectF;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRect:Landroid/graphics/RectF;

    .line 1273
    .local v0, "nRect":Landroid/graphics/RectF;
    if-eqz v0, :cond_1

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 1286
    :cond_0
    :goto_0
    return-void

    .line 1278
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRect:Landroid/graphics/RectF;

    if-nez v1, :cond_2

    .line 1279
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p1, Landroid/graphics/RectF;->left:F

    iget v3, p1, Landroid/graphics/RectF;->top:F

    iget v4, p1, Landroid/graphics/RectF;->right:F

    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRect:Landroid/graphics/RectF;

    .line 1282
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRectChanged:Z

    .line 1283
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_0

    .line 1284
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1281
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderRect:Landroid/graphics/RectF;

    iget v2, p1, Landroid/graphics/RectF;->left:F

    iget v3, p1, Landroid/graphics/RectF;->top:F

    iget v4, p1, Landroid/graphics/RectF;->right:F

    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method

.method public setBorderVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1212
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderVisible:Z

    if-ne v0, p1, :cond_1

    .line 1218
    :cond_0
    :goto_0
    return-void

    .line 1214
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderVisible:Z

    .line 1215
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1216
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setBorderWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 1230
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderWidth:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 1236
    :cond_0
    :goto_0
    return-void

    .line 1232
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mBorderWidth:F

    .line 1233
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1234
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .prologue
    const/4 v2, 0x1

    .line 1369
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-ne v0, p1, :cond_1

    .line 1394
    :cond_0
    :goto_0
    return-void

    .line 1372
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_3

    .line 1373
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget v1, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    .line 1374
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    if-eqz v0, :cond_2

    .line 1375
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;->onDetached(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1377
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mManualRecycle:Z

    if-nez v0, :cond_3

    .line 1378
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->recycle()V

    .line 1381
    :cond_3
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1382
    if-eqz p1, :cond_5

    .line 1383
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    .line 1384
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    if-eqz v0, :cond_4

    .line 1385
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;->onAttached(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1387
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    .line 1389
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 1390
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 1391
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setCanvasMixRatio(F)V
    .locals 2
    .param p1, "ratio"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1504
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 1517
    :cond_0
    :goto_0
    return-void

    .line 1506
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    .line 1507
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMixType:I

    if-nez v0, :cond_2

    .line 1508
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    .line 1512
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    .line 1513
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1514
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1510
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    sub-float v0, v1, v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    goto :goto_1
.end method

.method public setCanvasMixType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1488
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMixType:I

    if-ne v0, p1, :cond_1

    .line 1501
    :cond_0
    :goto_0
    return-void

    .line 1490
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMixType:I

    .line 1491
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMixType:I

    if-nez v0, :cond_2

    .line 1492
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    .line 1496
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    .line 1497
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1498
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1494
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixRatio:F

    sub-float v0, v1, v0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasMixSrc:F

    goto :goto_1
.end method

.method public setCanvasSub(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V
    .locals 3
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .prologue
    const/4 v2, 0x1

    .line 1397
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-ne v0, p1, :cond_1

    .line 1422
    :cond_0
    :goto_0
    return-void

    .line 1400
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_3

    .line 1401
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget v1, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    .line 1402
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    if-eqz v0, :cond_2

    .line 1403
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;->onDetached(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1405
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mManualRecycle:Z

    if-nez v0, :cond_3

    .line 1406
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->recycle()V

    .line 1409
    :cond_3
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1410
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 1411
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 1412
    if-eqz p1, :cond_5

    .line 1413
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    .line 1414
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    if-eqz v0, :cond_4

    .line 1415
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;->onAttached(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1417
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    .line 1419
    :cond_5
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1420
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setCanvasSubVisiblity(ZI)V
    .locals 5
    .param p1, "visibility"    # Z
    .param p2, "level"    # I

    .prologue
    const/4 v2, 0x1

    .line 1471
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubVisibility:Z

    .line 1474
    .local v1, "oldVisible":Z
    const/16 v3, 0x8

    if-lt p2, v3, :cond_1

    .line 1485
    :cond_0
    :goto_0
    return-void

    .line 1476
    :cond_1
    shl-int v0, v2, p2

    .line 1477
    .local v0, "checkBit":I
    if-eqz p1, :cond_2

    .line 1478
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubFlags:I

    or-int/2addr v3, v0

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubFlags:I

    .line 1481
    :goto_1
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubFlags:I

    const/16 v4, 0xff

    if-ne v3, v4, :cond_3

    :goto_2
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubVisibility:Z

    .line 1482
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubVisibility:Z

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_0

    .line 1483
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1480
    :cond_2
    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubFlags:I

    xor-int/lit8 v4, v0, -0x1

    and-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCanvasSubFlags:I

    goto :goto_1

    .line 1481
    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public setCenter(FFF)V
    .locals 3
    .param p1, "cx"    # F
    .param p2, "cy"    # F
    .param p3, "cz"    # F

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1198
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterX:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterY:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterZ:F

    cmpl-float v0, p3, v0

    if-nez v0, :cond_1

    .line 1209
    :cond_0
    :goto_0
    return-void

    .line 1200
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterX:F

    .line 1201
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterY:F

    .line 1202
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterZ:F

    .line 1203
    cmpl-float v0, p1, v2

    if-nez v0, :cond_2

    cmpl-float v0, p2, v2

    if-nez v0, :cond_2

    cmpl-float v0, p3, v2

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCenterOrigin:Z

    .line 1204
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 1205
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 1206
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1207
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1203
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .prologue
    .line 1716
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1717
    return-void
.end method

.method public setClipRect(IIII)V
    .locals 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1304
    if-ge p1, p3, :cond_0

    .line 1305
    move v2, p1

    .line 1306
    .local v2, "tLeft":I
    sub-int v4, p3, p1

    add-int/lit8 v3, v4, 0x1

    .line 1311
    .local v3, "tWidth":I
    :goto_0
    if-ge p2, p4, :cond_1

    .line 1312
    move v0, p2

    .line 1313
    .local v0, "tBottom":I
    sub-int v4, p4, p2

    add-int/lit8 v1, v4, 0x1

    .line 1318
    .local v1, "tHeight":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClipRect:Landroid/graphics/Rect;

    if-nez v4, :cond_2

    .line 1319
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v2, v0, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClipRect:Landroid/graphics/Rect;

    .line 1323
    :goto_2
    return-void

    .line 1308
    .end local v0    # "tBottom":I
    .end local v1    # "tHeight":I
    .end local v2    # "tLeft":I
    .end local v3    # "tWidth":I
    :cond_0
    move v2, p3

    .line 1309
    .restart local v2    # "tLeft":I
    sub-int v4, p1, p3

    add-int/lit8 v3, v4, 0x1

    .restart local v3    # "tWidth":I
    goto :goto_0

    .line 1315
    :cond_1
    move v0, p4

    .line 1316
    .restart local v0    # "tBottom":I
    sub-int v4, p2, p4

    add-int/lit8 v1, v4, 0x1

    .restart local v1    # "tHeight":I
    goto :goto_1

    .line 1321
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v0, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_2
.end method

.method public setClippingEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 1326
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mUseClipping:Z

    .line 1327
    return-void
.end method

.method public setColor(FFF)V
    .locals 0
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F

    .prologue
    .line 1143
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorR:F

    .line 1144
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorG:F

    .line 1145
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mColorB:F

    .line 1146
    return-void
.end method

.method public setCommand(IIII)V
    .locals 2
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I

    .prologue
    .line 1571
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 1572
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_OBJ:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 1573
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 1574
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 1575
    return-void
.end method

.method public setCommand(IIIILjava/lang/Object;)V
    .locals 6
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1580
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;)V

    .line 1581
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_OBJ:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 1582
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 1583
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 1584
    return-void
.end method

.method public setCommandDelayed(IIIIJ)V
    .locals 9
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "msecDelay"    # J

    .prologue
    .line 1590
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p5

    .line 1591
    .local v6, "expTime":J
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIIJ)V

    .line 1592
    .local v1, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_OBJ:I

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 1593
    iput-object p0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 1594
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 1595
    return-void
.end method

.method public setCommandDelayed(IIIILjava/lang/Object;J)V
    .locals 8
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;
    .param p6, "msecDelay"    # J

    .prologue
    .line 1601
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p6

    .line 1602
    .local v6, "expTime":J
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;J)V

    .line 1603
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_OBJ:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 1604
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 1605
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    .line 1606
    return-void
.end method

.method public setCustomTexCoord(FFFFFF)V
    .locals 3
    .param p1, "left"    # F
    .param p2, "right"    # F
    .param p3, "top"    # F
    .param p4, "bottom"    # F
    .param p5, "width"    # F
    .param p6, "height"    # F

    .prologue
    .line 2249
    const/16 v1, 0x8

    new-array v0, v1, [F

    .line 2251
    .local v0, "tex":[F
    const/4 v1, 0x0

    div-float v2, p1, p5

    aput v2, v0, v1

    .line 2252
    const/4 v1, 0x1

    div-float v2, p3, p6

    aput v2, v0, v1

    .line 2254
    const/4 v1, 0x2

    div-float v2, p2, p5

    aput v2, v0, v1

    .line 2255
    const/4 v1, 0x3

    div-float v2, p3, p6

    aput v2, v0, v1

    .line 2257
    const/4 v1, 0x4

    div-float v2, p2, p5

    aput v2, v0, v1

    .line 2258
    const/4 v1, 0x5

    div-float v2, p4, p6

    aput v2, v0, v1

    .line 2260
    const/4 v1, 0x6

    div-float v2, p1, p5

    aput v2, v0, v1

    .line 2261
    const/4 v1, 0x7

    div-float v2, p4, p6

    aput v2, v0, v1

    .line 2263
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoord:Ljava/nio/FloatBuffer;

    .line 2264
    return-void
.end method

.method public setCustomTexCoord(FFZ)V
    .locals 4
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "isMain"    # Z

    .prologue
    .line 2267
    sget-object v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mTexCoordArray:[F

    .line 2268
    .local v1, "texArray":[F
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTextCoords:Landroid/graphics/RectF;

    .line 2270
    .local v0, "tex":Landroid/graphics/RectF;
    :goto_0
    const/4 v2, 0x0

    iget v3, v0, Landroid/graphics/RectF;->left:F

    div-float/2addr v3, p1

    aput v3, v1, v2

    .line 2271
    const/4 v2, 0x1

    iget v3, v0, Landroid/graphics/RectF;->top:F

    div-float/2addr v3, p2

    aput v3, v1, v2

    .line 2272
    const/4 v2, 0x2

    iget v3, v0, Landroid/graphics/RectF;->right:F

    div-float/2addr v3, p1

    aput v3, v1, v2

    .line 2273
    const/4 v2, 0x3

    iget v3, v0, Landroid/graphics/RectF;->top:F

    div-float/2addr v3, p2

    aput v3, v1, v2

    .line 2274
    const/4 v2, 0x4

    iget v3, v0, Landroid/graphics/RectF;->right:F

    div-float/2addr v3, p1

    aput v3, v1, v2

    .line 2275
    const/4 v2, 0x5

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v3, p2

    aput v3, v1, v2

    .line 2276
    const/4 v2, 0x6

    iget v3, v0, Landroid/graphics/RectF;->left:F

    div-float/2addr v3, p1

    aput v3, v1, v2

    .line 2277
    const/4 v2, 0x7

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v3, p2

    aput v3, v1, v2

    .line 2278
    if-eqz p3, :cond_1

    .line 2279
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoord:Ljava/nio/FloatBuffer;

    .line 2282
    :goto_1
    return-void

    .line 2268
    .end local v0    # "tex":Landroid/graphics/RectF;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTextCoordSub:Landroid/graphics/RectF;

    goto :goto_0

    .line 2281
    .restart local v0    # "tex":Landroid/graphics/RectF;
    :cond_1
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoordSub:Ljava/nio/FloatBuffer;

    goto :goto_1
.end method

.method public setCustomTexCoord([F)V
    .locals 1
    .param p1, "texcoord"    # [F

    .prologue
    .line 2285
    invoke-static {p1}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCustomTexCoord:Ljava/nio/FloatBuffer;

    .line 2286
    return-void
.end method

.method public setDim(F)V
    .locals 1
    .param p1, "dim"    # F

    .prologue
    .line 1075
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    .line 1083
    :goto_0
    return-void

    .line 1078
    :cond_0
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    .line 1079
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_1

    .line 1080
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 1082
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setDim(FI)V

    goto :goto_0
.end method

.method public setDim(FI)V
    .locals 7
    .param p1, "dim"    # F
    .param p2, "hnd"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1086
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 1089
    .local v1, "oldVisible":Z
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    if-gt p2, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aget v2, v2, p2

    cmpl-float v2, p1, v2

    if-nez v2, :cond_1

    .line 1111
    :cond_0
    :goto_0
    return-void

    .line 1092
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aput p1, v2, p2

    .line 1093
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aget v2, v2, v4

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    .line 1094
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimCnt:I

    if-gt v0, v2, :cond_3

    .line 1095
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aget v2, v2, v0

    cmpl-float v2, v2, v5

    if-ltz v2, :cond_2

    .line 1096
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimEx:[F

    aget v3, v3, v0

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    .line 1094
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1099
    :cond_3
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDimFactor:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_4

    .line 1100
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 1105
    :goto_2
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_0

    .line 1106
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v6, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 1107
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_0

    .line 1108
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 1101
    :cond_4
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    const/16 v3, 0xff

    if-ne v2, v3, :cond_5

    .line 1102
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    goto :goto_2

    .line 1104
    :cond_5
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    goto :goto_2
.end method

.method public setDragListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    .prologue
    .line 1720
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mDragListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    .line 1721
    return-void
.end method

.method public setEmptyFill(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1221
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFill:Z

    if-ne v0, p1, :cond_1

    .line 1227
    :cond_0
    :goto_0
    return-void

    .line 1223
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFill:Z

    .line 1224
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1225
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setEmptyFillColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    const v1, 0xff00

    .line 1249
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    if-ne v0, p1, :cond_1

    .line 1259
    :cond_0
    :goto_0
    return-void

    .line 1251
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillColor:I

    .line 1253
    shr-int/lit8 v0, p1, 0x8

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillRed:I

    .line 1254
    and-int v0, p1, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillGreen:I

    .line 1255
    and-int/lit16 v0, p1, 0xff

    shl-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mEmptyFillBlue:I

    .line 1256
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1257
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V
    .locals 0
    .param p1, "genericMotionListener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .prologue
    .line 1739
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGenericMotionListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 1740
    return-void
.end method

.method public setHoverListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .prologue
    .line 1732
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 1733
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectHoveringAppeared:Z

    .line 1735
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHoverListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .line 1736
    return-void
.end method

.method public setLongClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .prologue
    .line 1724
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mLongClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 1725
    return-void
.end method

.method public setMetricInherit(ZZ)V
    .locals 0
    .param p1, "inheritScale"    # Z
    .param p2, "inheritRotate"    # Z

    .prologue
    .line 925
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhScale:Z

    .line 926
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mInhRotate:Z

    .line 927
    return-void
.end method

.method public setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .prologue
    .line 1712
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mMoveListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 1713
    return-void
.end method

.method public setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    .prologue
    .line 1743
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPenSelectListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    .line 1744
    return-void
.end method

.method public setPitch(F)V
    .locals 2
    .param p1, "pitch"    # F

    .prologue
    const/4 v1, 0x1

    .line 1159
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 1166
    :cond_0
    :goto_0
    return-void

    .line 1160
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    .line 1161
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 1162
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 1163
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setPos(FFF)V
    .locals 4
    .param p1, "cx"    # F
    .param p2, "cy"    # F
    .param p3, "cz"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 806
    const/4 v0, 0x0

    .line 808
    .local v0, "posChangedZ":Z
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v1, v1, v2

    cmpl-float v1, v1, p1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v1, v1, v2

    cmpl-float v1, v1, p2

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, v2

    cmpl-float v1, v1, p3

    if-eqz v1, :cond_2

    .line 809
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput p1, v1, v2

    .line 810
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput p2, v1, v2

    .line 811
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, v2

    cmpl-float v1, v1, p3

    if-eqz v1, :cond_1

    .line 812
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput p3, v1, v2

    .line 813
    const/4 v0, 0x1

    .line 815
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 816
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_2

    .line 817
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v3, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 818
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    or-int/2addr v2, v0

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 819
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_2

    .line 820
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 824
    :cond_2
    return-void
.end method

.method public setPos(FFFI)V
    .locals 6
    .param p1, "cx"    # F
    .param p2, "cy"    # F
    .param p3, "cz"    # F
    .param p4, "validBit"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 858
    const/4 v0, 0x0

    .line 859
    .local v0, "posChanged":Z
    const/4 v1, 0x0

    .line 861
    .local v1, "posChangedZ":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v2, v2, v4

    cmpl-float v2, v2, p1

    if-eqz v2, :cond_0

    and-int/lit8 v2, p4, 0x1

    if-ne v2, v5, :cond_0

    .line 862
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput p1, v2, v4

    .line 863
    const/4 v0, 0x1

    .line 865
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v2, v2, v4

    cmpl-float v2, v2, p2

    if-eqz v2, :cond_1

    and-int/lit8 v2, p4, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 866
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput p2, v2, v4

    .line 867
    const/4 v0, 0x1

    .line 869
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v2, v2, v4

    cmpl-float v2, v2, p3

    if-eqz v2, :cond_2

    and-int/lit8 v2, p4, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 870
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput p3, v2, v4

    .line 871
    const/4 v0, 0x1

    .line 872
    const/4 v1, 0x1

    .line 874
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 875
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 876
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v5, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 877
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v3, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    or-int/2addr v3, v1

    iput-boolean v3, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 878
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_3

    .line 879
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 882
    :cond_3
    return-void
.end method

.method public setPos(IFFF)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "cx"    # F
    .param p3, "cy"    # F
    .param p4, "cz"    # F

    .prologue
    const/4 v2, 0x1

    .line 781
    const/4 v0, 0x0

    .line 783
    .local v0, "posChangedZ":Z
    if-lez p1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-lt p1, v1, :cond_1

    .line 803
    :cond_0
    :goto_0
    return-void

    .line 785
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    .line 787
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v1, v1, p1

    cmpl-float v1, v1, p2

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v1, v1, p1

    cmpl-float v1, v1, p3

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, p1

    cmpl-float v1, v1, p4

    if-eqz v1, :cond_0

    .line 788
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput p2, v1, p1

    .line 789
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput p3, v1, p1

    .line 790
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v1, v1, p1

    cmpl-float v1, v1, p4

    if-eqz v1, :cond_3

    .line 791
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput p4, v1, p1

    .line 792
    const/4 v0, 0x1

    .line 794
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 795
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_0

    .line 796
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 797
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    or-int/2addr v2, v0

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 798
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_0

    .line 799
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setPos(IFFFI)V
    .locals 5
    .param p1, "index"    # I
    .param p2, "cx"    # F
    .param p3, "cy"    # F
    .param p4, "cz"    # F
    .param p5, "validBit"    # I

    .prologue
    const/4 v4, 0x1

    .line 827
    const/4 v0, 0x0

    .line 828
    .local v0, "posChanged":Z
    const/4 v1, 0x0

    .line 830
    .local v1, "posChangedZ":Z
    if-lez p1, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-lt p1, v2, :cond_1

    .line 855
    :cond_0
    :goto_0
    return-void

    .line 832
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v2, v2, p1

    if-eqz v2, :cond_0

    .line 834
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v2, v2, p1

    cmpl-float v2, v2, p2

    if-eqz v2, :cond_2

    and-int/lit8 v2, p5, 0x1

    if-ne v2, v4, :cond_2

    .line 835
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput p2, v2, p1

    .line 836
    const/4 v0, 0x1

    .line 838
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v2, v2, p1

    cmpl-float v2, v2, p3

    if-eqz v2, :cond_3

    and-int/lit8 v2, p5, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 839
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput p3, v2, p1

    .line 840
    const/4 v0, 0x1

    .line 842
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v2, v2, p1

    cmpl-float v2, v2, p4

    if-eqz v2, :cond_4

    and-int/lit8 v2, p5, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 843
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput p4, v2, p1

    .line 844
    const/4 v0, 0x1

    .line 845
    const/4 v1, 0x1

    .line 847
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 848
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosChanged:Z

    .line 849
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v4, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 850
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v3, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    or-int/2addr v3, v1

    iput-boolean v3, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 851
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_0

    .line 852
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setPosIndex()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 903
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v2, :cond_0

    .line 904
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_1

    .line 908
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v2, :cond_2

    .line 909
    move v1, v0

    .line 916
    .local v1, "newIndex":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    const/4 v3, 0x1

    aput-boolean v3, v2, v1

    .line 917
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aput v4, v2, v1

    .line 918
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aput v4, v2, v1

    .line 919
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aput v4, v2, v1

    .line 920
    .end local v1    # "newIndex":I
    :goto_2
    return v1

    .line 903
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 910
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    const/4 v3, 0x5

    if-ge v2, v3, :cond_3

    .line 911
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    .line 912
    .restart local v1    # "newIndex":I
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    goto :goto_1

    .line 914
    .end local v1    # "newIndex":I
    :cond_3
    const/4 v1, -0x1

    goto :goto_2
.end method

.method public setPositionsInter(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 4
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 566
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mx:F

    .line 567
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->my:F

    .line 568
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mz:F

    .line 569
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosMaxUsed:I

    if-ge v0, v1, :cond_1

    .line 570
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPosUsed:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 571
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mx:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCx:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mx:F

    .line 572
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->my:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCy:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->my:F

    .line 573
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mz:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mCz:[F

    aget v2, v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mz:F

    .line 569
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 576
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 577
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 578
    return-void
.end method

.method public setPostDraw(Z)V
    .locals 1
    .param p1, "usePostDraw"    # Z

    .prologue
    .line 1262
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPostDraw:Z

    if-ne v0, p1, :cond_1

    .line 1268
    :cond_0
    :goto_0
    return-void

    .line 1264
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mPostDraw:Z

    .line 1265
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1266
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setRoll(F)V
    .locals 2
    .param p1, "roll"    # F

    .prologue
    const/4 v1, 0x1

    .line 1169
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 1176
    :cond_0
    :goto_0
    return-void

    .line 1170
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRoll:F

    .line 1171
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 1172
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 1173
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setRotationGesture(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mUseRotationGesture:Z

    if-ne p1, v0, :cond_1

    .line 1566
    :cond_0
    :goto_0
    return-void

    .line 1561
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mUseRotationGesture:Z

    .line 1562
    if-eqz p1, :cond_2

    .line 1563
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_0

    .line 1565
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto :goto_0
.end method

.method public setRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

    .prologue
    .line 1728
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mRotationListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

    .line 1729
    return-void
.end method

.method public setScale(FF)V
    .locals 2
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F

    .prologue
    const/4 v1, 0x1

    .line 1003
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_1

    .line 1011
    :cond_0
    :goto_0
    return-void

    .line 1004
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    .line 1005
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    .line 1006
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 1007
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 1008
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1009
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setSize(FF)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    const/4 v1, 0x1

    .line 770
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_1

    .line 778
    :cond_0
    :goto_0
    return-void

    .line 771
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    .line 772
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    .line 773
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 774
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 775
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setTexCoordSub(FFFF)V
    .locals 1
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTextCoordSub:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1295
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoordSub:Z

    .line 1296
    return-void
.end method

.method public setTexCoords(FFFF)V
    .locals 1
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mTextCoords:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mReqTexCoord:Z

    .line 1291
    return-void
.end method

.method public setVertexRotation(I)V
    .locals 2
    .param p1, "degree"    # I

    .prologue
    .line 1179
    if-ltz p1, :cond_1

    move v0, p1

    .line 1180
    .local v0, "convDegree":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotation:I

    if-ne v1, v0, :cond_2

    .line 1185
    :cond_0
    :goto_1
    return-void

    .line 1179
    .end local v0    # "convDegree":I
    :cond_1
    add-int/lit16 v0, p1, 0x168

    goto :goto_0

    .line 1181
    .restart local v0    # "convDegree":I
    :cond_2
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotation:I

    .line 1182
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_0

    .line 1183
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_1
.end method

.method public setVertexRotationSub(I)V
    .locals 2
    .param p1, "degree"    # I

    .prologue
    .line 1188
    if-ltz p1, :cond_1

    move v0, p1

    .line 1189
    .local v0, "convDegree":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotationSub:I

    if-ne v1, v0, :cond_2

    .line 1194
    :cond_0
    :goto_1
    return-void

    .line 1188
    .end local v0    # "convDegree":I
    :cond_1
    add-int/lit16 v0, p1, 0x168

    goto :goto_0

    .line 1190
    .restart local v0    # "convDegree":I
    :cond_2
    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVertexRotationSub:I

    .line 1191
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_0

    .line 1192
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_1
.end method

.method public setView(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    const/4 v0, 0x1

    .line 1425
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1426
    if-eqz p1, :cond_0

    .line 1427
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/glcore/GlView;->setObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 1429
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 1430
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 1431
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_1

    .line 1432
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 1434
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_2

    .line 1435
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    .line 1437
    :cond_2
    return-void
.end method

.method public setViewSub(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    const/4 v0, 0x1

    .line 1440
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlViewSub:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1441
    if-eqz p1, :cond_0

    .line 1442
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/glcore/GlView;->setObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 1444
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 1445
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 1446
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_1

    .line 1447
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 1449
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_2

    .line 1450
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    .line 1452
    :cond_2
    return-void
.end method

.method public setVisibility(Z)V
    .locals 4
    .param p1, "visibility"    # Z

    .prologue
    const/4 v3, 0x0

    .line 581
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 583
    .local v0, "oldVisible":Z
    if-eqz p1, :cond_1

    .line 584
    const/16 v1, 0xff

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    .line 588
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 589
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 593
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_0

    .line 594
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 595
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v1, :cond_0

    .line 596
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 599
    :cond_0
    return-void

    .line 586
    :cond_1
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    goto :goto_0

    .line 591
    :cond_2
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    goto :goto_1
.end method

.method public setVisibility(ZI)V
    .locals 6
    .param p1, "visibility"    # Z
    .param p2, "level"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 602
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 605
    .local v1, "oldVisible":Z
    const/16 v2, 0x8

    if-lt p2, v2, :cond_1

    .line 626
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    shl-int v0, v4, p2

    .line 608
    .local v0, "checkBit":I
    if-eqz p1, :cond_2

    .line 609
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    or-int/2addr v2, v0

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    .line 613
    :goto_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    .line 614
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    .line 620
    :goto_2
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v2, :cond_0

    .line 621
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v4, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 622
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_0

    .line 623
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 611
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    xor-int/lit8 v3, v0, -0x1

    and-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    goto :goto_1

    .line 615
    :cond_3
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisibleEx:I

    const/16 v3, 0xff

    if-ne v2, v3, :cond_4

    .line 616
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    goto :goto_2

    .line 618
    :cond_4
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mVisible:Z

    goto :goto_2
.end method

.method public setYaw(F)V
    .locals 2
    .param p1, "yaw"    # F

    .prologue
    const/4 v1, 0x1

    .line 1149
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 1156
    :cond_0
    :goto_0
    return-void

    .line 1150
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    .line 1151
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mExtChanged:Z

    .line 1152
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mProjMatrixUpdate:Z

    .line 1153
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 1154
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method

.method public setZOrder(I)V
    .locals 2
    .param p1, "zOrder"    # I

    .prologue
    const/4 v1, 0x1

    .line 885
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mZOrder:I

    if-ne p1, v0, :cond_1

    .line 897
    :cond_0
    :goto_0
    return-void

    .line 888
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mZOrder:I

    .line 889
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    .line 892
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectPosChanged:Z

    .line 893
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mObjectZPosChanged:Z

    .line 894
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v0, :cond_0

    .line 895
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0
.end method
