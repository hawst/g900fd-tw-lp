.class Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;
.super Ljava/lang/Object;
.source "PicasaContentProvider.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncAlbumPhotos(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;Landroid/content/SyncResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$db:Landroid/database/sqlite/SQLiteDatabase;

.field final synthetic val$displayIndex:[I

.field final synthetic val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

.field final synthetic val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

.field final synthetic val$photoSchema:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

.field final synthetic val$syncResult:Landroid/content/SyncResult;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;[ILcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;Ljava/lang/String;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 671
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$displayIndex:[I

    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$account:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$photoSchema:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    iput-object p7, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$syncResult:Landroid/content/SyncResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleEntry(Lcom/sec/android/gallery3d/remote/picasa/Entry;)V
    .locals 14
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .prologue
    .line 673
    move-object v5, p1

    check-cast v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 678
    .local v5, "photo":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 679
    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    .line 680
    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 681
    .local v2, "lastDotIndex":I
    const/4 v8, -0x1

    if-eq v2, v8, :cond_0

    .line 682
    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 683
    .local v0, "imageExt":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 684
    iget-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    .line 689
    .end local v0    # "imageExt":Ljava/lang/String;
    .end local v2    # "lastDotIndex":I
    :cond_0
    iget-wide v6, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->id:J

    .line 690
    .local v6, "photoId":J
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$displayIndex:[I

    const/4 v9, 0x0

    aget v4, v8, v9

    .line 691
    .local v4, "newDisplayIndex":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iput-wide v6, v8, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->id:J

    .line 692
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    invoke-static {v8, v9}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 693
    .local v1, "index":I
    if-ltz v1, :cond_4

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    aget-object v3, v8, v1

    .line 694
    .local v3, "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    :goto_0
    if-eqz v3, :cond_1

    iget-wide v8, v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->dateEdited:J

    iget-wide v10, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateEdited:J

    cmp-long v8, v8, v10

    if-ltz v8, :cond_1

    iget v8, v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->displayIndex:I

    if-eq v8, v4, :cond_2

    .line 698
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$account:Ljava/lang/String;

    iput-object v8, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->syncAccount:Ljava/lang/String;

    .line 699
    iput v4, v5, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->displayIndex:I

    .line 700
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$photoSchema:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v8, v9, v5}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/Entry;)J

    .line 701
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$syncResult:Landroid/content/SyncResult;

    iget-object v8, v8, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v8, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v8, Landroid/content/SyncStats;->numUpdates:J

    .line 710
    :cond_2
    if-eqz v3, :cond_3

    .line 711
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->survived:Z

    .line 715
    :cond_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$2;->val$displayIndex:[I

    const/4 v9, 0x0

    add-int/lit8 v10, v4, 0x1

    aput v10, v8, v9

    .line 716
    return-void

    .line 693
    .end local v3    # "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method
