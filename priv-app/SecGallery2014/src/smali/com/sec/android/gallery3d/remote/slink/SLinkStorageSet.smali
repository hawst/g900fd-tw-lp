.class public Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "SLinkStorageSet.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "sLinkClient"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 27
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->TAG:Ljava/lang/String;

    const-string v1, "created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 29
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .line 30
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 31
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 58
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStorageId(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 59
    .local v0, "subsetPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    return-object v1
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 53
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStorageCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mDataVersion:J

    .line 43
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorageSet;->mDataVersion:J

    return-wide v0
.end method
