.class Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;
.super Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 0
    .param p2, "root"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 739
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 740
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    .line 741
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x6

    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 745
    iget v4, p1, Landroid/os/Message;->what:I

    sparse-switch v4, :sswitch_data_0

    .line 847
    new-instance v4, Ljava/lang/AssertionError;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(I)V

    throw v4

    .line 747
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGestureRecognizer:Lcom/sec/android/gallery3d/ui/GestureRecognizer;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$800(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/GestureRecognizer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/GestureRecognizer;->cancelScale()V

    .line 748
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->setExtraScalingRange(Z)V

    .line 749
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCancelExtraScalingPending:Z
    invoke-static {v4, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1002(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 753
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->switchFocus()V
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1100(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    goto :goto_0

    .line 759
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget v5, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->captureAnimationDone(I)V
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1200(Lcom/sec/android/gallery3d/ui/PhotoView;I)V

    goto :goto_0

    .line 765
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mListener:Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Listener;

    move-result-object v7

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/gallery3d/data/Path;

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v7, v4, v8}, Lcom/sec/android/gallery3d/ui/PhotoView$Listener;->onDeleteImage(Lcom/sec/android/gallery3d/data/Path;I)V

    .line 775
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 776
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 777
    .local v1, "m":Landroid/os/Message;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v4

    const-wide/16 v8, 0x7d0

    invoke-virtual {v4, v1, v8, v9}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 779
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1400(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v7

    sub-int/2addr v4, v7

    add-int/lit8 v2, v4, 0x1

    .line 780
    .local v2, "numberOfPictures":I
    if-ne v2, v10, :cond_2

    .line 781
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1400(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v7

    invoke-interface {v4, v7}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isCamera(I)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v7

    invoke-interface {v4, v7}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isCamera(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 782
    :cond_1
    add-int/lit8 v2, v2, -0x1

    .line 785
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-gt v2, v5, :cond_3

    move v4, v5

    :goto_1
    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->showUndoBar(Z)V

    goto :goto_0

    :cond_3
    move v4, v6

    goto :goto_1

    .line 789
    .end local v1    # "m":Landroid/os/Message;
    .end local v2    # "numberOfPictures":I
    :sswitch_4
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1300(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->hasMessages(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 790
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v5, -0x5

    # &= operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$572(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 791
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->snapback()V
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1600(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    goto/16 :goto_0

    .line 796
    :sswitch_5
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->checkHideUndoBar(I)V
    invoke-static {v4, v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1700(Lcom/sec/android/gallery3d/ui/PhotoView;I)V

    goto/16 :goto_0

    .line 800
    :sswitch_6
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/16 v5, 0x8

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->checkHideUndoBar(I)V
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1700(Lcom/sec/android/gallery3d/ui/PhotoView;I)V

    goto/16 :goto_0

    .line 805
    :sswitch_7
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->onTransitionComplete()V
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1800(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    goto/16 :goto_0

    .line 809
    :sswitch_8
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1900(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v4

    if-nez v4, :cond_0

    .line 811
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getLoadingSpinner()Lcom/sec/android/gallery3d/ui/ProgressSpinner;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2000(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/ProgressSpinner;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/ProgressSpinner;->startAnimation()V

    .line 812
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1902(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 813
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    goto/16 :goto_0

    .line 818
    :sswitch_9
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getLevelCount()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v4

    if-nez v4, :cond_0

    .line 821
    const/16 v4, 0x66

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->removeMessages(I)V

    .line 822
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v5, 0x3

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1902(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 825
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v4, :cond_4

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v4, :cond_4

    .line 826
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 827
    .local v3, "statusIntent":Landroid/content/Intent;
    const-string v4, "com.android.image.playstatechanged"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 828
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 829
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/content/ContextWrapper;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 833
    .end local v3    # "statusIntent":Landroid/content/Intent;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    goto/16 :goto_0

    .line 837
    :sswitch_a
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hidePreDisplayScreen()V

    goto/16 :goto_0

    .line 841
    :sswitch_b
    new-instance v0, Landroid/content/Intent;

    const-string v4, "GalleryOnLockscreen"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 842
    .local v0, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyHandler;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 745
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x65 -> :sswitch_7
        0x66 -> :sswitch_8
        0x67 -> :sswitch_9
        0x68 -> :sswitch_a
        0x69 -> :sswitch_b
    .end sparse-switch
.end method
