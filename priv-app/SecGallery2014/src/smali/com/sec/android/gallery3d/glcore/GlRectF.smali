.class public Lcom/sec/android/gallery3d/glcore/GlRectF;
.super Ljava/lang/Object;
.source "GlRectF.java"


# instance fields
.field public mHeight:F

.field public mLeft:F

.field public mTop:F

.field public mWidth:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mLeft:F

    .line 12
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mTop:F

    .line 13
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mWidth:F

    .line 14
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mHeight:F

    .line 15
    return-void
.end method


# virtual methods
.method public set(FFFF)V
    .locals 0
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 18
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mLeft:F

    .line 19
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mTop:F

    .line 20
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mWidth:F

    .line 21
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mHeight:F

    .line 22
    return-void
.end method

.method public setPos(FF)V
    .locals 0
    .param p1, "left"    # F
    .param p2, "top"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mLeft:F

    .line 26
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mTop:F

    .line 27
    return-void
.end method

.method public setSize(FF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mWidth:F

    .line 31
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRectF;->mHeight:F

    .line 32
    return-void
.end method
