.class Lcom/sec/android/gallery3d/ui/ContextualTagView$1;
.super Landroid/os/Handler;
.source "ContextualTagView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContextualTagView;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$1;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    .line 197
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 210
    :pswitch_0
    new-instance v0, Ljava/lang/AssertionError;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 199
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$1;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->hide(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$000(Lcom/sec/android/gallery3d/ui/ContextualTagView;Z)V

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 203
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$1;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    # setter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$102(Lcom/sec/android/gallery3d/ui/ContextualTagView;Z)Z

    .line 204
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$1;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->isContextualTagHelpOff()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$1;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->showContextualTagHelpView(II)V

    goto :goto_0

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
