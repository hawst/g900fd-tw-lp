.class Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;
.super Landroid/os/AsyncTask;
.source "ImageNoteView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/ImageNoteView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadBackgroundImageTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ImageNoteView;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 485
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "arg"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 496
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const-string v2, "LoadBackgroundImageTask > SaveBitmapToFileCache "

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    invoke-static {v0, v2, v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$100(Lcom/sec/android/gallery3d/ui/ImageNoteView;Ljava/lang/String;Z)V

    .line 499
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 500
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 502
    const/4 v7, 0x0

    .line 503
    .local v7, "bmReversedImageNoteBmp":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    # getter for: Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$200(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 504
    # getter for: Lcom/sec/android/gallery3d/ui/ImageNoteView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "doInBackground.. mPhotoView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :goto_0
    return-object v8

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    # getter for: Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$200(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    # getter for: Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$200(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    # getter for: Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$200(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 509
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    # getter for: Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$200(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 510
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    # setter for: Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;
    invoke-static {v0, v8}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$202(Lcom/sec/android/gallery3d/ui/ImageNoteView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 513
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->BGIMAGE_PATH:Ljava/lang/String;

    :goto_1
    # invokes: Lcom/sec/android/gallery3d/ui/ImageNoteView;->saveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    invoke-static {v2, v7, v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$400(Lcom/sec/android/gallery3d/ui/ImageNoteView;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 514
    if-eqz v7, :cond_1

    .line 515
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 516
    const/4 v7, 0x0

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const-string v2, "LoadBackgroundImageTask > SaveBitmapToFileCache "

    # invokes: Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    invoke-static {v0, v2, v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$100(Lcom/sec/android/gallery3d/ui/ImageNoteView;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    move-object v0, v8

    .line 513
    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 483
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 524
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const-string v1, "LoadBackgroundImageTask > onPostExecute"

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$100(Lcom/sec/android/gallery3d/ui/ImageNoteView;Ljava/lang/String;Z)V

    .line 525
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const-string v1, "LoadBackgroundImageTask > onPostExecute"

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$100(Lcom/sec/android/gallery3d/ui/ImageNoteView;Ljava/lang/String;Z)V

    .line 528
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 483
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 489
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const-string v1, "LoadBackgroundImageTask > onPreExecute"

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$100(Lcom/sec/android/gallery3d/ui/ImageNoteView;Ljava/lang/String;Z)V

    .line 490
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 491
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->this$0:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const-string v1, "LoadBackgroundImageTask > onPreExecute"

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->access$100(Lcom/sec/android/gallery3d/ui/ImageNoteView;Ljava/lang/String;Z)V

    .line 492
    return-void
.end method
