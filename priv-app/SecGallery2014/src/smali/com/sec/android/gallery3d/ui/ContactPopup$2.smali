.class Lcom/sec/android/gallery3d/ui/ContactPopup$2;
.super Ljava/lang/Object;
.source "ContactPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/ContactPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 323
    const-string v2, "ContactPopup"

    const-string v3, "more button clicked!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$200(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 326
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$200(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    move-result-object v2

    const/16 v3, 0x10

    invoke-interface {v2, v3}, Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;->onClicked(I)V

    .line 329
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v2

    if-nez v2, :cond_1

    .line 388
    :goto_0
    return-void

    .line 333
    :cond_1
    const/4 v0, 0x0

    .line 334
    .local v0, "id":I
    const-string v2, "profile"

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$300(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->checkWritableAccountWithLookupkey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 335
    :cond_2
    const v0, 0x7f090038

    .line 345
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/samsung/gallery/access/contact/ContactData;->contactName:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->splitDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346
    .local v1, "title":Ljava/lang/String;
    const-string v2, "Me"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 347
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 350
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/gallery3d/ui/ContactPopup$2$1;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$2$1;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup$2;)V

    invoke-virtual {v3, v0, v4}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    # setter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$402(Lcom/sec/android/gallery3d/ui/ContactPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 357
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$400(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/ui/ContactPopup$2$2;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$2$2;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup$2;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 365
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$400(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/ui/ContactPopup$2$3;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$2$3;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup$2;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 375
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$400(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    const/4 v3, -0x2

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0046

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/gallery3d/ui/ContactPopup$2$4;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$2$4;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup$2;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 387
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$400(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 337
    .end local v1    # "title":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;->this$0:Lcom/sec/android/gallery3d/ui/ContactPopup;

    # getter for: Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPhone(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 338
    const v0, 0x7f090037

    goto/16 :goto_1

    .line 340
    :cond_5
    const v0, 0x7f090036

    goto/16 :goto_1
.end method
