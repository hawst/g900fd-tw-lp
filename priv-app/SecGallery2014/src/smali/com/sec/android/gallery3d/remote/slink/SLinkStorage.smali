.class public Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "SLinkStorage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;
    }
.end annotation


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isNeedMediaItemReQuery:Z

.field private mBaseUri:Landroid/net/Uri;

.field private mCachedCount:I

.field private mCachedName:Ljava/lang/String;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private final mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

.field private mReloadTimer:Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;

.field private mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

.field private mSortByType:I

.field private mStorageId:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;

    .line 188
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(_id)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->COUNT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;ILcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "storageId"    # I
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "sLinkClient"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1, p3}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedCount:I

    .line 55
    iput v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mSortByType:I

    .line 57
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->isNeedMediaItemReQuery:Z

    .line 63
    iput p2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mStorageId:I

    .line 64
    invoke-interface {p3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 65
    invoke-interface {p3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mContentResolver:Landroid/content/ContentResolver;

    .line 66
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .line 67
    int-to-long v0, p2

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mBaseUri:Landroid/net/Uri;

    .line 69
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    new-instance v1, Landroid/os/Handler;

    invoke-interface {p3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    .line 71
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mReloadTimer:Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;

    .line 72
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private loadOrUpdateMediaItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Landroid/database/Cursor;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 157
    invoke-virtual {p2, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 158
    .local v0, "image":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    if-nez v0, :cond_0

    .line 159
    const/4 v0, 0x0

    .line 161
    .end local v0    # "image":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :goto_0
    return-object v0

    .line 160
    .restart local v0    # "image":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :cond_0
    invoke-virtual {v0, p3}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->updateContent(Landroid/database/Cursor;)Z

    goto :goto_0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 17
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    sget-object v2, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMediaItem "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ~ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "limit"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 131
    .local v3, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mSortByType:I

    if-nez v2, :cond_1

    const-string v15, " DESC"

    .line 133
    .local v15, "sort":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "datetaken"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 136
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 139
    .local v14, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_2

    .line 140
    :cond_0
    invoke-super/range {p0 .. p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 150
    .end local v14    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 153
    :goto_1
    return-object v14

    .line 131
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v15    # "sort":Ljava/lang/String;
    :cond_1
    const-string v15, " ASC"

    goto :goto_0

    .line 141
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v15    # "sort":Ljava/lang/String;
    :cond_2
    :try_start_1
    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 143
    .local v12, "idIndex":I
    :cond_3
    invoke-interface {v8, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 144
    .local v10, "id":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v2, v10, v11}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v8}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->loadOrUpdateMediaItem(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Landroid/database/Cursor;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v13

    .line 145
    .local v13, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 150
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 147
    .end local v10    # "id":J
    .end local v12    # "idIndex":I
    .end local v13    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v9

    .line 148
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 150
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public getMediaItemCount()I
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 105
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    monitor-enter v2

    .line 106
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->isNeedMediaItemReQuery:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedCount:I

    if-ne v1, v3, :cond_2

    .line 107
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->isNeedMediaItemReQuery:Z

    .line 109
    iget v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mStorageId:I

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->queryStorageItemCount(I)I

    move-result v0

    .line 110
    .local v0, "queryCount":I
    if-le v0, v3, :cond_1

    .line 111
    iput v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedCount:I

    .line 113
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mDataVersion:J

    .line 114
    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMediaItemCount() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    .end local v0    # "queryCount":I
    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedCount:I

    monitor-exit v2

    return v1

    .line 117
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    iget v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mStorageId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStorageIndex(I)I

    move-result v0

    .line 84
    .local v0, "subsetIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 85
    const-string v1, ""

    .line 91
    .end local v0    # "subsetIndex":I
    :goto_0
    return-object v1

    .line 88
    .restart local v0    # "subsetIndex":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getDeviceName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedName:Ljava/lang/String;

    .line 91
    .end local v0    # "subsetIndex":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getStorageId()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mStorageId:I

    return v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 184
    const-wide/16 v0, 0x0

    .line 185
    .local v0, "supportedOperations":J
    return-wide v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 288
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public isConnect()Z
    .locals 3

    .prologue
    .line 95
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mSLinkClient:Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    iget v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mStorageId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStorageIndex(I)I

    move-result v0

    .line 96
    .local v0, "subsetIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 97
    const/4 v1, 0x0

    .line 99
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public notifyContentChanged()V
    .locals 2

    .prologue
    .line 206
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;

    const-string v1, "notifyContentChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mReloadTimer:Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->getToken()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mReloadTimer:Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkReloadTimer;->setTimeout()V

    .line 216
    :goto_0
    return-void

    .line 213
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->isNeedMediaItemReQuery:Z

    .line 215
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->notifyContentChanged()V

    goto :goto_0
.end method

.method public queryStorageItemCount(I)I
    .locals 8
    .param p1, "storageId"    # I

    .prologue
    const/4 v3, 0x0

    .line 193
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mContentResolver:Landroid/content/ContentResolver;

    int-to-long v4, p1

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->COUNT_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 196
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 197
    .local v6, "count":I
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 200
    :cond_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 201
    return v6
.end method

.method public registerContentObserver()V
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    if-eqz v0, :cond_0

    .line 220
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mBaseUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, p0, v2, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->registerContentObserver(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Landroid/content/Context;)V

    .line 222
    :cond_0
    return-void
.end method

.method public reload()J
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    monitor-enter v1

    .line 168
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->TAG:Ljava/lang/String;

    const-string v2, "reload"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mCachedCount:I

    .line 171
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    invoke-static {}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mDataVersion:J

    .line 174
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mDataVersion:J

    return-wide v0

    .line 171
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setSortByType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mSortByType:I

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->notifyDirty()V

    .line 78
    return-void
.end method

.method public unregisterContentObserver()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    if-eqz v0, :cond_0

    .line 226
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mNotifier:Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage$SLinkChangeNotifier;->unregisterContentObserver(Landroid/content/Context;)V

    .line 228
    :cond_0
    return-void
.end method
