.class public Lcom/sec/android/gallery3d/settings/AccountSettingActivity;
.super Landroid/app/Activity;
.source "AccountSettingActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountSetting"


# instance fields
.field private mKnox:Z

.field private mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mKnox:Z

    return-void
.end method

.method private checkSLinkAccount()Z
    .locals 4

    .prologue
    .line 117
    const/4 v2, 0x0

    .line 118
    .local v2, "samsungAccountExists":Z
    const/4 v1, 0x0

    .line 120
    .local v1, "isSignedIn":Z
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->samsungAccountExists()Z

    move-result v2

    .line 121
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 122
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 123
    const/4 v3, 0x1

    .line 127
    :goto_0
    return v3

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v11, 0x400

    const v10, 0x7f0f0006

    const/4 v7, 0x1

    const v9, 0x7f0f0007

    const/4 v8, 0x0

    .line 42
    const-string v5, "AccountSetting"

    const-string v6, "onCreate!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v5, :cond_0

    .line 44
    const v5, 0x7f110039

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->setTheme(I)V

    .line 46
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "action":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    const v6, 0x106000d

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setIcon(I)V

    .line 57
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 58
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v11, v11}, Landroid/view/Window;->setFlags(II)V

    .line 61
    :cond_1
    const/high16 v5, 0x7f030000

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->setContentView(I)V

    .line 62
    new-instance v5, Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    .line 63
    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getApplication()Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->setApplication(Landroid/app/Application;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 65
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 66
    invoke-virtual {p0, v9}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f020526

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 68
    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0b002d

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 70
    invoke-virtual {p0, v10}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 71
    const v5, 0x7f0f0009

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/app/FragmentBreadCrumbs;

    .line 72
    .local v2, "fragmentBreadCrumbs":Landroid/app/FragmentBreadCrumbs;
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Landroid/app/FragmentBreadCrumbs;->setMaxVisible(I)V

    .line 73
    invoke-virtual {v2, p0}, Landroid/app/FragmentBreadCrumbs;->setActivity(Landroid/app/Activity;)V

    .line 76
    const/4 v3, 0x0

    .line 77
    .local v3, "previouslySelectedPreference":I
    if-eqz p1, :cond_2

    .line 78
    const-string v5, "PREVIOUSLY_SELECTED_ITEM"

    invoke-virtual {p1, v5, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 84
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->setSelectedPreferenceItem(I)V

    .line 86
    new-instance v5, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;

    iget-object v6, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-direct {v5, v6, v2}, Lcom/sec/android/gallery3d/settings/AccountSettingMainFragment;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;Landroid/app/FragmentBreadCrumbs;)V

    invoke-virtual {v4, v10, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 101
    .end local v2    # "fragmentBreadCrumbs":Landroid/app/FragmentBreadCrumbs;
    .end local v3    # "previouslySelectedPreference":I
    :goto_1
    invoke-virtual {v4, v8}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 102
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    .line 103
    return-void

    .line 52
    .end local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_3
    if-eqz v0, :cond_4

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHomeAsUpFromSetting:Z

    if-eqz v5, :cond_4

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto/16 :goto_0

    .line 55
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto/16 :goto_0

    .line 88
    .restart local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_5
    const v5, 0x7f0f0008

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 89
    .local v1, "detailFragmentHeader":Landroid/widget/LinearLayout;
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "sec_container_1.com.sec.android.gallery3d"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 91
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mKnox:Z

    .line 92
    :cond_6
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mKnox:Z

    if-eqz v5, :cond_7

    .line 93
    new-instance v5, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    iget-object v6, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    const v7, 0x7f070005

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;I)V

    invoke-virtual {v4, v9, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1

    .line 95
    :cond_7
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v5, :cond_9

    .line 96
    new-instance v6, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_8

    const v5, 0x7f070006

    :goto_2
    invoke-direct {v6, v7, v5}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;I)V

    invoke-virtual {v4, v9, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1

    :cond_8
    const v5, 0x7f070004

    goto :goto_2

    .line 98
    :cond_9
    new-instance v6, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;

    iget-object v7, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_a

    const v5, 0x7f070006

    :goto_3
    invoke-direct {v6, v7, v5}, Lcom/sec/android/gallery3d/settings/AccountSettingDetailFragment;-><init>(Lcom/sec/android/gallery3d/settings/AccountSettingManager;I)V

    invoke-virtual {v4, v9, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto/16 :goto_1

    :cond_a
    const v5, 0x7f070003

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 108
    .local v0, "inflater":Landroid/view/MenuInflater;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 109
    const/high16 v1, 0x7f120000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 113
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x1

    .line 132
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 158
    :goto_0
    return v6

    .line 134
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->finish()V

    goto :goto_0

    .line 139
    :sswitch_1
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 140
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "account_types"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "com.osp.app.signin"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "com.sec.android.app.sns3.facebook"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "com.dropbox.android.account"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "com.google"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "com.skp.tcloud"

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 145
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 147
    const v2, 0x7f0e026c

    invoke-static {p0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 132
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0250 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 164
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->onPause()V

    .line 165
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->onResume()V

    .line 170
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 171
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 36
    const-string v0, "PREVIOUSLY_SELECTED_ITEM"

    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;->mSettingManager:Lcom/sec/android/gallery3d/settings/AccountSettingManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/settings/AccountSettingManager;->getSelectedPreferenceItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 38
    return-void
.end method
