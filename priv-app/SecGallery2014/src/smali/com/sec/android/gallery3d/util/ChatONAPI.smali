.class public Lcom/sec/android/gallery3d/util/ChatONAPI;
.super Ljava/lang/Object;
.source "ChatONAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;
    }
.end annotation


# static fields
.field public static final ACCESS_TOKEN_PROVIDER_URI:Landroid/net/Uri;

.field public static final ACTION_FILE:Ljava/lang/String; = "file"

.field public static final BUDDY_NAME:Ljava/lang/String; = "buddy_name"

.field public static final BUDDY_NO:Ljava/lang/String; = "buddy_no"

.field public static final BUDDY_RAW_CONTACT_ID:Ljava/lang/String; = "buddy_raw_contact_id"

.field public static final BUDDY_STATUS_MESSAGE:Ljava/lang/String; = "buddy_status_message"

.field public static final BUDDY_URI:Landroid/net/Uri;

.field private static final CHATON_ACTION_ADD_BUDDY:Ljava/lang/String; = "com.sec.chaton.action.ADD_BUDDY"

.field private static final DATA_PREFIX:Ljava/lang/String; = "chaton://"

.field private static final EXTRA_KEY_EXCEPT:Ljava/lang/String; = "except"

.field private static final EXTRA_KEY_ID:Ljava/lang/String; = "id"

.field private static final EXTRA_KEY_MAX_COUNT:Ljava/lang/String; = "max"

.field private static final EXTRA_KEY_RECEIVER:Ljava/lang/String; = "receiver"

.field private static final EXTRA_KEY_REQUIRE:Ljava/lang/String; = "require"

.field private static final EXTRA_KEY_SINGLE:Ljava/lang/String; = "single"

.field public static final ME_COUNTRY_ISO_CODE:Ljava/lang/String; = "country_iso_code"

.field public static final ME_COUNTRY_NUM:Ljava/lang/String; = "country_num"

.field public static final ME_ID:Ljava/lang/String; = "id"

.field public static final ME_NAME:Ljava/lang/String; = "name"

.field public static final ME_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179
    const-string v0, "content://com.sec.chaton.access_token.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI;->ACCESS_TOKEN_PROVIDER_URI:Landroid/net/Uri;

    .line 193
    const-string v0, "content://com.sec.chaton.provider/buddy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI;->BUDDY_URI:Landroid/net/Uri;

    .line 273
    const-string v0, "content://com.sec.chaton.provider/me"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI;->ME_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public static addBuddy(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 359
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.chaton.action.ADD_BUDDY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 363
    .local v0, "i":Landroid/content/Intent;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 365
    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 367
    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/util/ChatONAPI;->addPassword(Landroid/content/Context;Landroid/content/Intent;)V

    .line 373
    :cond_0
    return-object v0
.end method

.method private static addPassword(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 185
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/ChatONAPI;->ACCESS_TOKEN_PROVIDER_URI:Landroid/net/Uri;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 187
    .local v0, "returnUri":Landroid/net/Uri;
    const-string v1, "password"

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    return-void
.end method

.method private static getActivityIntent(Landroid/content/Context;Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mimeType"    # Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;
    .param p2, "extraText"    # Ljava/lang/String;
    .param p3, "extraStream"    # Landroid/net/Uri;
    .param p4, "receiver"    # Ljava/lang/String;
    .param p5, "action"    # Ljava/lang/String;

    .prologue
    .line 113
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 119
    .local v1, "i":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 121
    .local v0, "data":Landroid/net/Uri;
    if-nez p5, :cond_6

    .line 123
    const-string v2, "chaton://"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 129
    :goto_0
    if-eqz p1, :cond_7

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p3, :cond_7

    .line 131
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    :goto_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 143
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    :cond_1
    if-eqz p3, :cond_2

    .line 151
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 157
    :cond_2
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 159
    const-string v2, "receiver"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    :cond_3
    if-eqz p4, :cond_5

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz p3, :cond_5

    .line 165
    :cond_4
    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/util/ChatONAPI;->addPassword(Landroid/content/Context;Landroid/content/Intent;)V

    .line 169
    :cond_5
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 173
    return-object v1

    .line 127
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "chaton://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 135
    :cond_7
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public static getBuddyList(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 209
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/util/ChatONAPI;->BUDDY_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "buddy_no"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "buddy_name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "buddy_status_message"

    aput-object v4, v2, v3

    const-string v3, "BUDDY_RAW_CONTACT_ID = ?"

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getBuddyProfileImage(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "buddyNo"    # Ljava/lang/String;

    .prologue
    .line 223
    const/4 v0, 0x0

    .line 225
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 229
    .local v2, "pfdInput":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/android/gallery3d/util/ChatONAPI;->BUDDY_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buddy_no/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "r"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 231
    if-eqz v2, :cond_0

    .line 233
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 249
    :cond_0
    if-eqz v2, :cond_1

    .line 253
    :try_start_1
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 267
    :cond_1
    :goto_0
    return-object v0

    .line 255
    :catch_0
    move-exception v1

    .line 259
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 239
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 243
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 249
    if-eqz v2, :cond_1

    .line 253
    :try_start_3
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 255
    :catch_2
    move-exception v1

    .line 259
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 249
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_2

    .line 253
    :try_start_4
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 261
    :cond_2
    :goto_1
    throw v3

    .line 255
    :catch_3
    move-exception v1

    .line 259
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getMyProvider(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 289
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/util/ChatONAPI;->ME_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "name"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "country_num"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "country_iso_code"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static openMessageChatRoom(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "receiver"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 79
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, p1

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/util/ChatONAPI;->getActivityIntent(Landroid/content/Context;Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static openMessageChatRoomWithDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "receiver"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 87
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/util/ChatONAPI;->getActivityIntent(Landroid/content/Context;Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static pickBuddy(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "single"    # Z
    .param p2, "exceptBuddies"    # [Ljava/lang/String;
    .param p3, "require"    # Ljava/lang/String;
    .param p4, "maxCount"    # I

    .prologue
    .line 313
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    const-string v2, "chaton://"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 317
    .local v0, "i":Landroid/content/Intent;
    if-lez p4, :cond_0

    .line 319
    const-string v1, "max"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 325
    :cond_0
    const-string/jumbo v1, "single"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 329
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 331
    const-string v1, "require"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    :cond_1
    if-eqz p2, :cond_2

    array-length v1, p2

    if-lez v1, :cond_2

    .line 339
    const-string v1, "except"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    :cond_2
    return-object v0
.end method

.method public static sendMessageContent(Landroid/content/Context;Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mimeType"    # Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "stream"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    .line 95
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/util/ChatONAPI;->getActivityIntent(Landroid/content/Context;Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static sendMessageContent(Landroid/content/Context;Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mimeType"    # Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "stream"    # Landroid/net/Uri;
    .param p4, "receiver"    # Ljava/lang/String;

    .prologue
    .line 103
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/util/ChatONAPI;->getActivityIntent(Landroid/content/Context;Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
