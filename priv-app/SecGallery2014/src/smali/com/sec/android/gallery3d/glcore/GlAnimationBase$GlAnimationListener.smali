.class public interface abstract Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;
.super Ljava/lang/Object;
.source "GlAnimationBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GlAnimationListener"
.end annotation


# virtual methods
.method public abstract onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
.end method

.method public abstract onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
.end method

.method public abstract onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
.end method
