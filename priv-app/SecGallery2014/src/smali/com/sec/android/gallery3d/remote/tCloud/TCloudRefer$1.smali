.class Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;
.super Landroid/os/AsyncTask;
.source "TCloudRefer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->tCloudAlbumsSync(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 342
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 346
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    if-eqz v4, :cond_0

    .line 347
    const/4 v3, 0x0

    .line 349
    .local v3, "sync":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    :try_start_1
    new-array v2, v4, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v2, v4

    .line 350
    .local v2, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string/jumbo v5, "sync"

    invoke-virtual {v4, v5, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 356
    .end local v2    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    const/4 v4, 0x1

    :try_start_2
    new-array v1, v4, [Landroid/content/Context;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;->val$context:Landroid/content/Context;

    aput-object v5, v1, v4

    .line 357
    .local v1, "paramTypes":[Landroid/content/Context;
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer$1;->this$0:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    # getter for: Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mTcloudStoreAPI:Ljava/lang/Object;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;)Ljava/lang/Object;

    move-result-object v4

    check-cast v1, [Ljava/lang/Object;

    .end local v1    # "paramTypes":[Landroid/content/Context;
    invoke-virtual {v3, v4, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 370
    .end local v3    # "sync":Ljava/lang/reflect/Method;
    :cond_0
    :goto_1
    const/4 v4, 0x0

    return-object v4

    .line 351
    .restart local v3    # "sync":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 367
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v3    # "sync":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v0

    .line 368
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 359
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "sync":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v0

    .line 360
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 361
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 362
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 363
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 364
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1
.end method
