.class public Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HomeSyncDMRRequestReceiver.java"


# static fields
.field private static mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;


# instance fields
.field private final ACTION_IMAGE_CHANGED:Ljava/lang/String;

.field private final ACTION_PLAYSTATE_CHANGED:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 13
    const-class v0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->TAG:Ljava/lang/String;

    .line 14
    const-string v0, "com.sec.android.spc.dmr.IMAGE_CHANGED"

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->ACTION_IMAGE_CHANGED:Ljava/lang/String;

    .line 15
    const-string v0, "com.android.image.playstatechanged"

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->ACTION_PLAYSTATE_CHANGED:Ljava/lang/String;

    return-void
.end method

.method public static setDMRNewUriListener(Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    .prologue
    .line 52
    sput-object p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    .line 53
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 20
    const-string v4, "com.sec.android.spc.dmr.IMAGE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 21
    const-string v4, "MimeType"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "mimeType":Ljava/lang/String;
    const-string v4, "URI"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 23
    .local v2, "struri":Ljava/lang/String;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 25
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->TAG:Ljava/lang/String;

    const-string v5, "DMR image play request"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    sget-object v4, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    if-nez v4, :cond_1

    .line 28
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "start new activity"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 30
    .local v1, "myIntent":Landroid/content/Intent;
    const/high16 v4, 0x14000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 31
    new-instance v4, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 32
    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    const-string v4, "DMR"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 34
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 49
    .end local v0    # "mimeType":Ljava/lang/String;
    .end local v1    # "myIntent":Landroid/content/Intent;
    .end local v2    # "struri":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 36
    .restart local v0    # "mimeType":Ljava/lang/String;
    .restart local v2    # "struri":Ljava/lang/String;
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->TAG:Ljava/lang/String;

    const-string v5, "change image"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    sget-object v4, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    invoke-interface {v4, v3, v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;->onNewUri(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    .line 40
    .end local v0    # "mimeType":Ljava/lang/String;
    .end local v2    # "struri":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_2
    const-string v4, "com.android.image.playstatechanged"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 41
    const-string v4, "STOPPED"

    const-string v5, "Status"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 42
    sget-object v4, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    if-eqz v4, :cond_0

    .line 43
    iget-object v4, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->TAG:Ljava/lang/String;

    const-string v5, "DMR STOPPED, finish !!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    sget-object v4, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;->onStopPlayer()V

    .line 45
    const/4 v4, 0x0

    sput-object v4, Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestReceiver;->mListener:Lcom/sec/android/gallery3d/homesync/HomeSyncDMRRequestListener;

    goto :goto_0
.end method
