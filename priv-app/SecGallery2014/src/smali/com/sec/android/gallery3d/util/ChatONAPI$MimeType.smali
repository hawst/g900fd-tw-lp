.class public final enum Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;
.super Ljava/lang/Enum;
.source "ChatONAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/ChatONAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MimeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

.field public static final enum image:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

.field public static final enum text:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

.field public static final enum video:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    new-instance v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    const-string/jumbo v1, "text"

    const-string/jumbo v2, "text/*"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->text:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    .line 49
    new-instance v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    const-string v1, "image"

    const-string v2, "image/*"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->image:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    .line 51
    new-instance v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    const-string/jumbo v1, "video"

    const-string/jumbo v2, "video/*"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->video:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    sget-object v1, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->text:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->image:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->video:Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->$VALUES:[Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput-object p3, p0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->value:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->$VALUES:[Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/ChatONAPI$MimeType;->value:Ljava/lang/String;

    return-object v0
.end method
