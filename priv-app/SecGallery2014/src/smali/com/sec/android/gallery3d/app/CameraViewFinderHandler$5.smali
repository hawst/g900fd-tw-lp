.class Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$5;
.super Ljava/lang/Object;
.source "CameraViewFinderHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$5;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$5;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$600(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;)Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 374
    # getter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Presentation was dismissed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$5;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->mPresentation:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->access$602(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;)Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$CameraViewFinderPresentation;

    .line 377
    :cond_0
    return-void
.end method
