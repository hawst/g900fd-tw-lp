.class Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;
.super Ljava/lang/Object;
.source "GalleryMultiWindow.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->registerLayoutListener(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

.field final synthetic val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    iput-object p2, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 144
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$000(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    .line 145
    .local v2, "mViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$000(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 146
    .local v3, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-eqz v3, :cond_0

    .line 147
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v5

    # setter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isSelectionMode:Z
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$102(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Z)Z

    .line 149
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    # invokes: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isPickMode(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$200(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v4

    if-nez v4, :cond_2

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$300()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v4

    if-eqz v4, :cond_2

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$300()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mDropDownReloadNeeded:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$400(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v2, :cond_2

    instance-of v4, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-nez v4, :cond_2

    instance-of v4, v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-nez v4, :cond_2

    instance-of v4, v2, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-nez v4, :cond_2

    instance-of v4, v2, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isSelectionMode:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$100(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 157
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$000(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 158
    .local v1, "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 159
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 161
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # setter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mDropDownReloadNeeded:Z
    invoke-static {v4, v6}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$402(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Z)Z

    .line 165
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "activity":Landroid/app/Activity;
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$000(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->onMWLayoutChanged()V

    .line 166
    return-void

    .line 162
    :cond_2
    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$300()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v4

    if-eqz v4, :cond_1

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$300()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mDropDownReloadNeeded:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$400(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 163
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryMultiWindow$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->mDropDownReloadNeeded:Z
    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->access$402(Lcom/sec/android/gallery3d/app/GalleryMultiWindow;Z)Z

    goto :goto_0
.end method
