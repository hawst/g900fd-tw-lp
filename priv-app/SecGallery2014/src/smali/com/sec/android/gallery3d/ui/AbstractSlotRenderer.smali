.class public abstract Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;
.super Ljava/lang/Object;
.source "AbstractSlotRenderer.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/SlotView$SlotRenderer;


# static fields
.field private static FILMSTRIP_BORDER_MARGIN:I

.field private static FILMSTRIP_ICON_COLUMN:I

.field private static FILMSTRIP_ICON_GAP:I


# instance fields
.field private filmstripBorderMargin:I

.field private final m3DMpoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final m3DPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final m3DTourIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mBestFaceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mBestPhotoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mCameraShortcut:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mCheckBoxOffIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mCheckBoxOnIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mCinePicIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mCloudIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mDramaShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mEraserIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private final mFramePressed:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mFramePressedUp:Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

.field private final mFrameSelected:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private final mGenericFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private final mMagicShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mOutOfFocusIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mPhotoNoteIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mPicMotionIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mPrivateIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mSPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mSequenceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mSoundSceneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mVideoOverlay:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mVideoPlayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 39
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_BORDER_MARGIN:I

    .line 40
    sput v1, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_ICON_COLUMN:I

    .line 41
    sput v1, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_ICON_GAP:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x7f020183

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mBestPhotoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 56
    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mBestFaceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 57
    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mEraserIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 58
    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mDramaShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 59
    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPicMotionIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->filmstripBorderMargin:I

    .line 70
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020355

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mVideoOverlay:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 71
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0200f7

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mVideoPlayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 72
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0202f7

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 73
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    const v1, 0x7f0202c0

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressed:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 74
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-direct {v0, p1, v2}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFrameSelected:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 76
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-direct {v0, p1, v2}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 77
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    const v1, 0x7f02026e

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mGenericFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 78
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0200fa

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->m3DMpoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 79
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020111

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mSPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 80
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0200fb

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->m3DPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 81
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02011c

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mSoundSceneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 82
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02010e

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCinePicIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 83
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020093

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCloudIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 84
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->getCheckBoxOnResID()I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCheckBoxOnIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 85
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->getCheckBoxOffResID()I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCheckBoxOffIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 86
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02017f

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCameraShortcut:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 87
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0200fc

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->m3DTourIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 88
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020112

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPhotoNoteIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 89
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020118

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mMagicShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 90
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraShotAndMore:Z

    if-eqz v0, :cond_0

    .line 91
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0200ff

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mBestPhotoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 92
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0200fe

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mBestFaceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 93
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020109

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mEraserIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 94
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020107

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mDramaShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 95
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020105

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPicMotionIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 97
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02010b

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mOutOfFocusIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 98
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f020117

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mSequenceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 99
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f0200d7

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPrivateIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->filmstripBorderMargin:I

    .line 103
    return-void
.end method

.method private drawCheckBoxIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 4
    .param p1, "icon"    # Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "iconCount"    # I

    .prologue
    .line 290
    rem-int/lit8 v2, p5, 0x2

    if-nez v2, :cond_0

    sget v0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_BORDER_MARGIN:I

    .line 291
    .local v0, "x":I
    :goto_0
    div-int/lit8 v2, p5, 0x2

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    sget v1, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_BORDER_MARGIN:I

    .line 292
    .local v1, "y":I
    :goto_1
    invoke-virtual {p1, p2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 293
    return-void

    .line 290
    .end local v0    # "x":I
    .end local v1    # "y":I
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v2

    sget v3, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_BORDER_MARGIN:I

    add-int/2addr v2, v3

    sub-int v0, p3, v2

    goto :goto_0

    .line 291
    .restart local v0    # "x":I
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v2

    sget v3, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_BORDER_MARGIN:I

    add-int/2addr v2, v3

    sub-int v1, p4, v2

    goto :goto_1
.end method

.method protected static drawFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/Texture;IIII)V
    .locals 6
    .param p0, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p1, "padding"    # Landroid/graphics/Rect;
    .param p2, "frame"    # Lcom/sec/android/gallery3d/glrenderer/Texture;
    .param p3, "x"    # I
    .param p4, "y"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    .line 175
    iget v0, p1, Landroid/graphics/Rect;->left:I

    sub-int v2, p3, v0

    iget v0, p1, Landroid/graphics/Rect;->top:I

    sub-int v3, p4, v0

    iget v0, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, p5

    iget v1, p1, Landroid/graphics/Rect;->right:I

    add-int v4, v0, v1

    iget v0, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, p6

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    add-int v5, v0, v1

    move-object v0, p2

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/Texture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 177
    return-void
.end method

.method private getCheckBoxOffResID()I
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const v0, 0x7f020084

    .line 200
    :goto_0
    return v0

    .line 195
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_1

    .line 196
    const v0, 0x7f020121

    goto :goto_0

    .line 197
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    const v0, 0x7f0204a4

    goto :goto_0

    .line 200
    :cond_2
    const v0, 0x7f0204a6

    goto :goto_0
.end method

.method private getCheckBoxOnResID()I
    .locals 1

    .prologue
    .line 181
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    const v0, 0x7f020085

    .line 188
    :goto_0
    return v0

    .line 183
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_1

    .line 184
    const v0, 0x7f020122

    goto :goto_0

    .line 185
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    const v0, 0x7f0204ab

    goto :goto_0

    .line 188
    :cond_2
    const v0, 0x7f0204ad

    goto :goto_0
.end method


# virtual methods
.method protected draw3DMpoIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 213
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->m3DMpoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 214
    return-void
.end method

.method protected draw3DPanoramaIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 221
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->m3DPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 222
    return-void
.end method

.method protected draw3DTourIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 261
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->m3DTourIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 262
    return-void
.end method

.method protected drawBestFaceIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 245
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mBestFaceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 246
    return-void
.end method

.method protected drawBestPhotoIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 241
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mBestPhotoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 242
    return-void
.end method

.method protected drawCameraShortcut(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 296
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCameraShortcut:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v1, p1

    move v3, v2

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 297
    return-void
.end method

.method protected drawCheckBoxIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIZ)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "isChecked"    # Z

    .prologue
    .line 277
    if-eqz p4, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCheckBoxOnIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    :goto_0
    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawCheckBoxIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 278
    return-void

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCheckBoxOffIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_0
.end method

.method protected drawCinePicIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 229
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCinePicIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 230
    return-void
.end method

.method protected drawCloudIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 233
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mCloudIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 234
    return-void
.end method

.method protected drawContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/Texture;III)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "content"    # Lcom/sec/android/gallery3d/glrenderer/Texture;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "rotation"    # I

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 107
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 111
    invoke-static {p3, p4}, Ljava/lang/Math;->min(II)I

    move-result p4

    move p3, p4

    .line 112
    if-eqz p5, :cond_0

    .line 113
    int-to-float v1, p3

    div-float/2addr v1, v3

    int-to-float v2, p4

    div-float/2addr v2, v3

    invoke-interface {p1, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 114
    int-to-float v1, p5

    invoke-interface {p1, v1, v4, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->rotate(FFFF)V

    .line 115
    neg-int v1, p3

    int-to-float v1, v1

    div-float/2addr v1, v3

    neg-int v2, p4

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-interface {p1, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 119
    :cond_0
    int-to-float v1, p3

    invoke-interface {p2}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    int-to-float v2, p4

    invoke-interface {p2}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 122
    .local v0, "scale":F
    invoke-interface {p1, v0, v0, v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->scale(FFF)V

    .line 123
    invoke-interface {p2, p1, v6, v6}, Lcom/sec/android/gallery3d/glrenderer/Texture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 125
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 126
    return-void
.end method

.method protected drawDramaShotIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 253
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mDramaShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 254
    return-void
.end method

.method protected drawEraserIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 249
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mEraserIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 250
    return-void
.end method

.method protected drawFocusFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 205
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->getPaddings()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object v0, p1

    move v4, v3

    move v5, p2

    move v6, p3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/Texture;IIII)V

    .line 206
    return-void
.end method

.method protected drawGenericFocusFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 209
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mGenericFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->getPaddings()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mGenericFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object v0, p1

    move v4, v3

    move v5, p2

    move v6, p3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/Texture;IIII)V

    .line 210
    return-void
.end method

.method public drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 4
    .param p1, "icon"    # Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "iconCount"    # I

    .prologue
    .line 281
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    const/4 v2, 0x4

    sput v2, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_ICON_GAP:I

    .line 284
    :cond_0
    rem-int/lit8 v2, p5, 0x2

    if-nez v2, :cond_1

    sget v2, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_BORDER_MARGIN:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->filmstripBorderMargin:I

    add-int v0, v2, v3

    .line 285
    .local v0, "x":I
    :goto_0
    sget v2, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_ICON_COLUMN:I

    div-int v2, p5, v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    sub-int v2, p4, v2

    sget v3, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_BORDER_MARGIN:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->filmstripBorderMargin:I

    sub-int v1, v2, v3

    .line 286
    .local v1, "y":I
    invoke-virtual {p1, p2, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 287
    return-void

    .line 284
    .end local v0    # "x":I
    .end local v1    # "y":I
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v2

    sget v3, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->FILMSTRIP_ICON_GAP:I

    add-int v0, v2, v3

    goto :goto_0
.end method

.method protected drawMagicShotIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 237
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mMagicShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 238
    return-void
.end method

.method protected drawOutOfFocusIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 265
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mOutOfFocusIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 266
    return-void
.end method

.method protected drawPanoramaIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 142
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v4, v0, 0x6

    .line 143
    .local v4, "iconSize":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    sub-int v1, p2, v4

    div-int/lit8 v2, v1, 0x2

    sub-int v1, p3, v4

    div-int/lit8 v3, v1, 0x2

    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 145
    return-void
.end method

.method protected drawPhotoNoteIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 269
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPhotoNoteIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 270
    return-void
.end method

.method protected drawPicMotionIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 257
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPicMotionIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 258
    return-void
.end method

.method protected drawPressedFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 166
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressed:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->getPaddings()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressed:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object v0, p1

    move v4, v3

    move v5, p2

    move v6, p3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/Texture;IIII)V

    .line 167
    return-void
.end method

.method protected drawPressedUpFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 159
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressedUp:Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressed:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;-><init>(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressedUp:Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressed:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->getPaddings()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressedUp:Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

    move-object v0, p1

    move v4, v3

    move v5, p2

    move v6, p3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/Texture;IIII)V

    .line 163
    return-void
.end method

.method protected drawPrivateIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 300
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mPrivateIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 301
    return-void
.end method

.method protected drawSPanoramaIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 217
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mSPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 218
    return-void
.end method

.method protected drawSelectedFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 170
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFrameSelected:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->getPaddings()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFrameSelected:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object v0, p1

    move v4, v3

    move v5, p2

    move v6, p3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawFrame(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;Lcom/sec/android/gallery3d/glrenderer/Texture;IIII)V

    .line 171
    return-void
.end method

.method protected drawSequenceIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 273
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mSequenceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 274
    return-void
.end method

.method protected drawSoundSceneIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "iconCount"    # I

    .prologue
    .line 225
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mSoundSceneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->drawIcon(Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 226
    return-void
.end method

.method protected drawVideoOverlay(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 14
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 131
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mVideoOverlay:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 132
    .local v1, "v":Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    move/from16 v0, p3

    int-to-float v2, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    .line 133
    .local v13, "scale":F
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v13

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 134
    .local v5, "w":I
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v13

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 135
    .local v6, "h":I
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 137
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->min(II)I

    move-result v2

    div-int/lit8 v11, v2, 0x1

    .line 138
    .local v11, "s":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mVideoPlayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    sub-int v2, p2, v11

    div-int/lit8 v9, v2, 0x2

    sub-int v2, p3, v11

    div-int/lit8 v10, v2, 0x2

    move-object v8, p1

    move v12, v11

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 139
    return-void
.end method

.method protected isPressedUpFrameFinished()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressedUp:Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressedUp:Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    .line 152
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractSlotRenderer;->mFramePressedUp:Lcom/sec/android/gallery3d/glrenderer/FadeOutTexture;

    .line 155
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
