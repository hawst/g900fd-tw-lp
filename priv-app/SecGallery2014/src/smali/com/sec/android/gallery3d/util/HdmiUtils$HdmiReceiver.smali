.class public Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HdmiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/HdmiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HdmiReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x6

    .line 116
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTvOut:Z

    if-nez v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 119
    :cond_0
    const-string/jumbo v0, "state"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    .line 123
    const/4 v0, -0x1

    # setter for: Lcom/sec/android/gallery3d/util/HdmiUtils;->mIsSupport3D:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/HdmiUtils;->access$102(I)I

    .line 124
    # getter for: Lcom/sec/android/gallery3d/util/HdmiUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received plugged = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/gallery3d/util/HdmiUtils;->sIsPlugged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    # getter for: Lcom/sec/android/gallery3d/util/HdmiUtils;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->access$300()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 127
    # getter for: Lcom/sec/android/gallery3d/util/HdmiUtils;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->access$300()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
