.class public Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;
.super Landroid/preference/PreferenceFragment;
.source "MagazineWidgetPreferenceActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MagazinePrefsFragment"
.end annotation


# static fields
.field private static final KEY_CHANGE_PICTURE:Ljava/lang/String; = "preference_change_picture"

.field private static final KEY_INTERVAL:Ljava/lang/String; = "preference_category_interval"

.field private static final REQUEST_CHANGE_PICTURES:I = 0x3

.field private static mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$200()Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updateIntervalSummary()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updateCurrentWidget()V

    return-void
.end method

.method private updateCurrentWidget()V
    .locals 3

    .prologue
    .line 285
    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v2, v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 286
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v2, v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v1, v2, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    .line 287
    return-void
.end method

.method private updateIntervalSummary()V
    .locals 7

    .prologue
    const v6, 0x7f0e0138

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 265
    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 267
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v1, 0x0

    .line 269
    .local v1, "summary":Ljava/lang/String;
    iget v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    packed-switch v2, :pswitch_data_0

    .line 281
    :goto_0
    const-string v2, "preference_category_interval"

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 282
    return-void

    .line 271
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 272
    goto :goto_0

    .line 274
    :pswitch_1
    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 275
    goto :goto_0

    .line 277
    :pswitch_2
    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updatePictureSummary()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    .line 228
    const/4 v6, 0x0

    .line 229
    .local v6, "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    const/4 v1, 0x0

    .line 231
    .local v1, "count":I
    sget-object v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 232
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v9, v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v2

    .line 234
    .local v2, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v8, v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    packed-switch v8, :pswitch_data_0

    .line 258
    :goto_0
    if-ne v1, v10, :cond_1

    const v8, 0x7f0e0134

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 261
    .local v7, "summary":Ljava/lang/String;
    :goto_1
    const-string v8, "preference_change_picture"

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 262
    return-void

    .line 236
    .end local v7    # "summary":Ljava/lang/String;
    :pswitch_0
    new-instance v6, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v9, v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-direct {v6, v8, v9}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    .line 237
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-interface {v6}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v1

    .line 238
    goto :goto_0

    .line 240
    :pswitch_1
    iget-object v8, v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    .line 241
    .local v5, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 242
    .local v3, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 243
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v4, :cond_0

    new-instance v6, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-direct {v6}, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;-><init>()V

    .line 246
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    :goto_2
    invoke-interface {v6}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v1

    .line 247
    goto :goto_0

    .line 243
    :cond_0
    new-instance v6, Lcom/sec/android/gallery3d/gadget/MediaSetSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v3, v8}, Lcom/sec/android/gallery3d/gadget/MediaSetSource;-><init>(Lcom/sec/android/gallery3d/data/DataManager;Ljava/lang/String;)V

    goto :goto_2

    .line 249
    .end local v3    # "manager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "path":Lcom/sec/android/gallery3d/data/Path;
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    :pswitch_2
    new-instance v6, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    invoke-direct {v6, v8}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;-><init>(Landroid/content/Context;)V

    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    move-object v8, v6

    .line 250
    check-cast v8, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->shuffleAllSize()I

    move-result v1

    .line 251
    goto :goto_0

    .line 253
    :pswitch_3
    new-instance v6, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v9, v9, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-direct {v6, v8, v9}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    .line 254
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-interface {v6}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v1

    goto :goto_0

    .line 258
    :cond_1
    const v8, 0x7f0e0135

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v10, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 234
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 205
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 206
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updatePictureSummary()V

    .line 208
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updateCurrentWidget()V

    .line 211
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 143
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "created"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sput-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    .line 146
    const v0, 0x7f070013

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->addPreferencesFromResource(I)V

    .line 147
    const-string v0, "preference_change_picture"

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 148
    const-string v0, "preference_category_interval"

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updatePictureSummary()V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->updateIntervalSummary()V

    .line 154
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 215
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v0, v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    .line 216
    .local v0, "appWidgetId":I
    # getter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPreferenceChanged!! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    new-instance v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 218
    .local v2, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v1

    .line 219
    .local v1, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    .line 220
    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 221
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 223
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v4, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v4, v4, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v3, v4, v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    .line 224
    const/4 v3, 0x1

    return v3
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v6, 0x1

    .line 160
    const-string v2, "preference_change_picture"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 161
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    const-class v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "appWidgetId"

    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    const-string v2, "SHOW_WIDGET_TYPE"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 164
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 200
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v6

    .line 165
    :cond_1
    const-string v2, "preference_category_interval"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 167
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    iget v3, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    # setter for: Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalSelected:I
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->access$102(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;I)I

    .line 168
    new-instance v2, Landroid/app/AlertDialog$Builder;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0e0137

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    new-instance v5, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$3;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e00db

    new-instance v4, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$2;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0046

    new-instance v4, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment$1;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetPreferenceActivity$MagazinePrefsFragment;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
