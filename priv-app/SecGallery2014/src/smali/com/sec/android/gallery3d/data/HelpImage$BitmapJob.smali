.class Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;
.super Ljava/lang/Object;
.source "HelpImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/HelpImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BitmapJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/HelpImage;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/data/HelpImage;I)V
    .locals 0
    .param p2, "type"    # I

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput p2, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->mType:I

    .line 233
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v7, 0x0

    .line 238
    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->mType:I

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/LocalImage;->getTargetSize(I)I

    move-result v4

    .line 241
    .local v4, "targetSize":I
    const/4 v0, 0x0

    .line 243
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v5, :cond_0

    .line 244
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    # invokes: Lcom/sec/android/gallery3d/data/HelpImage;->getResourceName()Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/gallery3d/data/HelpImage;->access$600(Lcom/sec/android/gallery3d/data/HelpImage;)Ljava/lang/String;

    move-result-object v3

    .line 245
    .local v3, "resourceName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    # getter for: Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v5}, Lcom/sec/android/gallery3d/data/HelpImage;->access$200(Lcom/sec/android/gallery3d/data/HelpImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 255
    .end local v3    # "resourceName":Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_1

    .line 256
    const/4 v5, 0x0

    .line 270
    :goto_1
    return-object v5

    .line 247
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    # invokes: Lcom/sec/android/gallery3d/data/HelpImage;->getResourceId()I
    invoke-static {v5}, Lcom/sec/android/gallery3d/data/HelpImage;->access$700(Lcom/sec/android/gallery3d/data/HelpImage;)I

    move-result v2

    .line 248
    .local v2, "resourceId":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    # getter for: Lcom/sec/android/gallery3d/data/HelpImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v6}, Lcom/sec/android/gallery3d/data/HelpImage;->access$200(Lcom/sec/android/gallery3d/data/HelpImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 251
    .end local v2    # "resourceId":I
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 259
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->mType:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_2

    iget v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->mType:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_4

    .line 260
    :cond_2
    invoke-static {v0, v4, v7}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 265
    :goto_2
    if-eqz v0, :cond_3

    .line 266
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    # setter for: Lcom/sec/android/gallery3d/data/HelpImage;->mWidth:I
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/data/HelpImage;->access$402(Lcom/sec/android/gallery3d/data/HelpImage;I)I

    .line 267
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/HelpImage;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    # setter for: Lcom/sec/android/gallery3d/data/HelpImage;->mHeight:I
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/data/HelpImage;->access$502(Lcom/sec/android/gallery3d/data/HelpImage;I)I

    :cond_3
    move-object v5, v0

    .line 270
    goto :goto_1

    .line 262
    :cond_4
    invoke-static {v0, v4, v7}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 228
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/HelpImage$BitmapJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
