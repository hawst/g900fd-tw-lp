.class public Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;
.super Landroid/preference/PreferenceFragment;
.source "WidgetPreferenceActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrefsFragment"
.end annotation


# static fields
.field private static final KEY_CHANGE_PICTURE:Ljava/lang/String; = "preference_change_picture"

.field public static final KEY_FRAME_TYPE:Ljava/lang/String; = "preference_list_frame_type"

.field private static final KEY_INTERVAL:Ljava/lang/String; = "preference_category_interval"

.field private static final REQUEST_CHANGE_PICTURES:I = 0x3

.field private static mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$500()Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;
    .locals 1

    .prologue
    .line 303
    sget-object v0, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->updateIntervalSummary()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->updateCurrentWidget()V

    return-void
.end method

.method private updateCurrentWidget()V
    .locals 3

    .prologue
    .line 458
    sget-object v1, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v2, v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 459
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    sget-object v1, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v2, v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v1, v2, v0}, Lcom/sec/android/gallery3d/gadget/PhotoAppWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    .line 460
    return-void
.end method

.method private updateIntervalSummary()V
    .locals 7

    .prologue
    const v6, 0x7f0e0138

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 438
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 440
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v1, 0x0

    .line 442
    .local v1, "summary":Ljava/lang/String;
    iget v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    packed-switch v2, :pswitch_data_0

    .line 454
    :goto_0
    const-string v2, "preference_category_interval"

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 455
    return-void

    .line 444
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 445
    goto :goto_0

    .line 447
    :pswitch_1
    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 448
    goto :goto_0

    .line 450
    :pswitch_2
    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 442
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updatePictureSummary()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    .line 401
    const/4 v6, 0x0

    .line 402
    .local v6, "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    const/4 v1, 0x0

    .line 404
    .local v1, "count":I
    sget-object v8, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 405
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v9, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v9, v9, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v2

    .line 407
    .local v2, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v8, v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    packed-switch v8, :pswitch_data_0

    .line 431
    :goto_0
    if-ne v1, v10, :cond_1

    const v8, 0x7f0e0134

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 434
    .local v7, "summary":Ljava/lang/String;
    :goto_1
    const-string v8, "preference_change_picture"

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 435
    return-void

    .line 409
    .end local v7    # "summary":Ljava/lang/String;
    :pswitch_0
    new-instance v6, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v9, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v9, v9, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-direct {v6, v8, v9}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    .line 410
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-interface {v6}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v1

    .line 411
    goto :goto_0

    .line 413
    :pswitch_1
    iget-object v8, v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    .line 414
    .local v5, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 415
    .local v3, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 416
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v4, :cond_0

    new-instance v6, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-direct {v6}, Lcom/sec/android/gallery3d/gadget/WidgetService$EmptySource;-><init>()V

    .line 419
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    :goto_2
    invoke-interface {v6}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v1

    .line 420
    goto :goto_0

    .line 416
    :cond_0
    new-instance v6, Lcom/sec/android/gallery3d/gadget/MediaSetSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    iget-object v8, v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    invoke-direct {v6, v3, v8}, Lcom/sec/android/gallery3d/gadget/MediaSetSource;-><init>(Lcom/sec/android/gallery3d/data/DataManager;Ljava/lang/String;)V

    goto :goto_2

    .line 422
    .end local v3    # "manager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "path":Lcom/sec/android/gallery3d/data/Path;
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    :pswitch_2
    new-instance v6, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-direct {v6, v8}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;-><init>(Landroid/content/Context;)V

    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    move-object v8, v6

    .line 423
    check-cast v8, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/gadget/ShuffleAllSource;->shuffleAllSize()I

    move-result v1

    .line 424
    goto :goto_0

    .line 426
    :pswitch_3
    new-instance v6, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;

    .end local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    sget-object v8, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v9, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v9, v9, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-direct {v6, v8, v9}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;-><init>(Landroid/content/Context;I)V

    .line 427
    .restart local v6    # "source":Lcom/sec/android/gallery3d/gadget/WidgetSource;
    invoke-interface {v6}, Lcom/sec/android/gallery3d/gadget/WidgetSource;->size()I

    move-result v1

    goto :goto_0

    .line 431
    :cond_1
    const v8, 0x7f0e0135

    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v10, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 378
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 379
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 380
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->updatePictureSummary()V

    .line 381
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->updateCurrentWidget()V

    .line 384
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 313
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 314
    # getter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "created"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sput-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    .line 317
    const v2, 0x7f07001b

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->addPreferencesFromResource(I)V

    .line 318
    const-string v2, "preference_change_picture"

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 319
    const-string v2, "preference_list_frame_type"

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;

    .line 320
    .local v1, "framePrefCategory":Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;
    const-string v2, "preference_list_frame_type"

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 321
    const-string v2, "preference_category_interval"

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 323
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 324
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;->setFrameType(I)V

    .line 325
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->updatePictureSummary()V

    .line 326
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->updateIntervalSummary()V

    .line 328
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->setLayoutCategory(Lcom/sec/android/gallery3d/gadget/FramePreferenceCategory;)V

    .line 329
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 388
    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v0, v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    .line 389
    .local v0, "appWidgetId":I
    # getter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPreferenceChanged!! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    new-instance v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 391
    .local v2, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v1

    .line 392
    .local v1, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->frameType:I

    .line 393
    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 394
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 396
    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v4, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v4, v4, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v3, v4, v1}, Lcom/sec/android/gallery3d/gadget/PhotoAppWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    .line 397
    const/4 v3, 0x1

    return v3
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v6, 0x1

    .line 333
    const-string v2, "preference_change_picture"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 334
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    const-class v3, Lcom/sec/android/gallery3d/gadget/WidgetConfigure;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 335
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "appWidgetId"

    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 336
    const-string v2, "SHOW_WIDGET_TYPE"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 337
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 373
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v6

    .line 338
    :cond_1
    const-string v2, "preference_category_interval"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v3, v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mAppWidgetId:I

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(Landroid/content/Context;I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 340
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    sget-object v2, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    iget v3, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    # setter for: Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalSelected:I
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->access$402(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;I)I

    .line 341
    new-instance v2, Landroid/app/AlertDialog$Builder;

    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;->mPrefActivity:Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0e0137

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity;->mIntervalItems:[Ljava/lang/CharSequence;

    iget v4, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->showInterval:I

    new-instance v5, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment$3;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e00db

    new-instance v4, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment$2;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment$2;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0046

    new-instance v4, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment$1;-><init>(Lcom/sec/android/gallery3d/gadget/WidgetPreferenceActivity$PrefsFragment;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
