.class public Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;
.super Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;
.source "SLinkItemEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry$SLinkItemColumns;
    }
.end annotation


# static fields
.field private static mFields:[Ljava/lang/reflect/Field;


# instance fields
.field public device_id:I

.field public device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->mFields:[Ljava/lang/reflect/Field;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;-><init>()V

    .line 36
    return-void
.end method

.method private setPropertyFromCursor(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 74
    :try_start_0
    const-string v1, "_display_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->title:Ljava/lang/String;

    .line 75
    const-string v1, "_data"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_url:Ljava/lang/String;

    .line 76
    const-string/jumbo v1, "width"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->width:I

    .line 77
    const-string v1, "height"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->height:I

    .line 78
    const-string v1, "mime_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    .line 79
    const-string v1, "orientation"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->rotation:I

    .line 80
    const-string v1, "datetaken"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->date_taken:J

    .line 81
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->getDevicePhysicalType(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->clear()V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_id:I

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 46
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 50
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    if-nez v2, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 52
    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    .line 53
    .local v0, "p":Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/remote/RemotePhotoEntry;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_id:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_id:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getFields()[Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->mFields:[Ljava/lang/reflect/Field;

    return-object v0
.end method

.method public setProperty(Ljava/lang/Object;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 65
    instance-of v0, p1, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 66
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "source":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->setPropertyFromCursor(Landroid/database/Cursor;)V

    .line 70
    return-void

    .line 68
    .restart local p1    # "source":Ljava/lang/Object;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Source is wrong"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
