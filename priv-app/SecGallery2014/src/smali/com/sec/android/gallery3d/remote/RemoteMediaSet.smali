.class public abstract Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "RemoteMediaSet.java"


# instance fields
.field protected final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 29
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 30
    return-void
.end method
