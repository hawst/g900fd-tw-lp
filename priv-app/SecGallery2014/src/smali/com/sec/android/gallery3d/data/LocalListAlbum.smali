.class public Lcom/sec/android/gallery3d/data/LocalListAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "LocalListAlbum.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final mWatchUris:[Landroid/net/Uri;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBucketName:Ljava/lang/String;

.field private mHashKey:Ljava/lang/String;

.field private mMediaItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/gallery3d/data/LocalListAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->TAG:Ljava/lang/String;

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mWatchUris:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "hashKey"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalListAlbum;->nextVersionNumber()J

    move-result-wide v2

    invoke-direct {p0, p1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 45
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 46
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mHashKey:Ljava/lang/String;

    .line 47
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalListAlbum;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mDataVersion:J

    .line 49
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mHashKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getLocalItemList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 50
    .local v0, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez v0, :cond_0

    .line 51
    sget-object v1, Lcom/sec/android/gallery3d/data/LocalListAlbum;->TAG:Ljava/lang/String;

    const-string v2, "input media item list is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .restart local v0    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    .line 56
    new-instance v1, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v2, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mWatchUris:[Landroid/net/Uri;

    invoke-direct {v1, p0, v2, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 58
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 3
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v0, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move v1, p1

    .local v1, "ix":I
    :goto_0
    add-int v2, p1, p2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 79
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    :cond_0
    return-object v0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mBucketName:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalMediaItemCount()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public reload()J
    .locals 6

    .prologue
    .line 97
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mHashKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getLocalItemList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 100
    .local v1, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez v1, :cond_0

    .line 101
    sget-object v3, Lcom/sec/android/gallery3d/data/LocalListAlbum;->TAG:Ljava/lang/String;

    const-string v4, "input media item list is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .restart local v1    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 107
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalListAlbum;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mDataVersion:J

    .line 108
    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 109
    .local v2, "mediaItemList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "ix":I
    :goto_0
    if-ltz v0, :cond_4

    .line 110
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_2

    .line 111
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->isDeleted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 113
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    .line 122
    :cond_1
    :goto_1
    const/4 v1, 0x0

    .line 123
    move-object v1, v2

    .line 109
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 115
    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v3, :cond_3

    .line 116
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/LocalVideo;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalVideo;->isDeleted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 117
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 120
    :cond_3
    sget-object v4, Lcom/sec/android/gallery3d/data/LocalListAlbum;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 126
    .end local v0    # "ix":I
    .end local v2    # "mediaItemList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_4
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    .line 127
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mHashKey:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mMediaItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->setLocalItemList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 129
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mDataVersion:J

    return-wide v4
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalListAlbum;->mBucketName:Ljava/lang/String;

    .line 67
    return-void
.end method
