.class public Lcom/sec/android/gallery3d/data/FaceClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "FaceClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;,
        Lcom/sec/android/gallery3d/data/FaceClustering$Value;,
        Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;,
        Lcom/sec/android/gallery3d/data/FaceClustering$FaceClusterComparator;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FaceClustering"


# instance fields
.field private mClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mFilterConfirmedRecommendCluster:Z

.field private mGroupIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsCategory:Z

.field private mKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNameIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mNameId:Ljava/lang/String;

.field private mNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOnlyFilterConfirmedCluster:Z

.field private mOnlyFilterNamedCluster:Z

.field private mPersonIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameIDs:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mKeys:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mPersonIds:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mGroupIds:Ljava/util/ArrayList;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onlyFilterNamedCluster"    # Z

    .prologue
    const/4 v0, 0x0

    .line 570
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/FaceClustering;-><init>(Landroid/content/Context;)V

    .line 571
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterNamedCluster:Z

    .line 572
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterConfirmedCluster:Z

    .line 573
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mFilterConfirmedRecommendCluster:Z

    .line 574
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onlyFilterNamedCluster"    # Z
    .param p3, "isCategory"    # Z

    .prologue
    const/4 v1, 0x0

    .line 584
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/FaceClustering;-><init>(Landroid/content/Context;)V

    .line 585
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterNamedCluster:Z

    .line 586
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterConfirmedCluster:Z

    .line 587
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mFilterConfirmedRecommendCluster:Z

    .line 588
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 589
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    .line 592
    :goto_0
    return-void

    .line 591
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ZZZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onlyFilterNamedCluster"    # Z
    .param p3, "onlyFilterConfirmedCluster"    # Z
    .param p4, "filterConfirmedRecommendCluster"    # Z

    .prologue
    .line 576
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameIDs:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mKeys:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mPersonIds:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mGroupIds:Ljava/util/ArrayList;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    .line 577
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    .line 578
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterNamedCluster:Z

    .line 579
    iput-boolean p3, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterConfirmedCluster:Z

    .line 580
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mFilterConfirmedRecommendCluster:Z

    .line 581
    return-void
.end method

.method private getPath(I)Lcom/sec/android/gallery3d/data/Path;
    .locals 12
    .param p1, "imageId"    # I

    .prologue
    const/4 v2, 0x0

    .line 511
    const/4 v9, -0x1

    move-object v0, p0

    move v1, p1

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v10, v2

    move v11, v2

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/gallery3d/data/FaceClustering;->getPath(IIIIIIIIIII)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    return-object v0
.end method

.method private getPath(IIIIIIIIIII)Lcom/sec/android/gallery3d/data/Path;
    .locals 4
    .param p1, "imageId"    # I
    .param p2, "faceId"    # I
    .param p3, "faceData"    # I
    .param p4, "left"    # I
    .param p5, "top"    # I
    .param p6, "right"    # I
    .param p7, "bottom"    # I
    .param p8, "recommendedId"    # I
    .param p9, "personId"    # I
    .param p10, "groupId"    # I
    .param p11, "autoGroup"    # I

    .prologue
    .line 516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 517
    .local v0, "buf":Ljava/lang/StringBuilder;
    const-string v2, "/face/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "people"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 521
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 522
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    return-object v1
.end method


# virtual methods
.method public convertPath(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 17
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 595
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v14

    .line 596
    .local v14, "values":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 597
    .local v3, "buf":Ljava/lang/StringBuilder;
    const/4 v15, 0x2

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 598
    .local v5, "faceId":I
    const/4 v10, 0x1

    .line 599
    .local v10, "personId":I
    const/4 v11, 0x1

    .line 600
    .local v11, "recommendedId":I
    const/4 v6, 0x0

    .line 601
    .local v6, "groupId":I
    const/4 v15, 0x3

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 602
    .local v4, "faceData":I
    const/16 v15, 0xc

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 603
    .local v1, "autoGroup":I
    const/4 v15, 0x4

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 604
    .local v8, "left":I
    const/4 v15, 0x5

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 605
    .local v13, "top":I
    const/4 v15, 0x6

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 606
    .local v12, "right":I
    const/4 v15, 0x7

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 607
    .local v2, "bottom":I
    const/4 v15, 0x1

    aget-object v15, v14, v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 609
    .local v7, "imageId":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    invoke-static {v15, v5}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknown(Landroid/content/Context;I)V

    .line 611
    const-string v15, "/face/"

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "people"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 617
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 618
    .local v9, "p":Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object p1

    .line 620
    return-object p1
.end method

.method public getCluster(I)Ljava/util/ArrayList;
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 542
    :goto_0
    return-object v0

    .line 541
    :cond_0
    const-string v0, "FaceClustering"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCluster index("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") error. size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getClusterCover(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 564
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mKeys:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 555
    :goto_0
    return-object v0

    .line 554
    :cond_0
    const-string v0, "FaceClustering"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getClusterName index("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") error. size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getClusterNameID(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameIDs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getGroupId(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 530
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getPersonId(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mPersonIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method getPersonName(Landroid/content/ContentResolver;III)Ljava/lang/String;
    .locals 9
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "personId"    # I
    .param p3, "groupId"    # I
    .param p4, "index"    # I

    .prologue
    const/4 v0, 0x1

    .line 674
    const-string v8, ""

    .line 675
    .local v8, "personName":Ljava/lang/String;
    if-le p2, v0, :cond_1

    .line 676
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    .line 677
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v2, p2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 678
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 680
    .local v6, "c":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "name"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 681
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 683
    const-string v0, "profile/Me"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "profile/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00c0

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 690
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 702
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :goto_0
    return-object v8

    .line 687
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 688
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 690
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 693
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    if-lez p3, :cond_2

    .line 694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00c2

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2131624130_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    .line 696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 698
    :cond_2
    const-string v0, "2131624040"

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    .line 699
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0068

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 74
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 79
    const-string v6, "FaceClustering"

    const-string v9, "FaceClustering start!!"

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    move/from16 v48, v0

    .line 83
    .local v48, "isSortNeeded":Z
    const/16 v47, 0x0

    .line 84
    .local v47, "isByGroup":Z
    const/16 v46, 0x0

    .line 86
    .local v46, "imgIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v28, "clusters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;>;"
    const/16 v23, 0x0

    .line 93
    .local v23, "categoryIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/16 v56, 0x0

    .line 94
    .local v56, "onlyCategoryIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    if-eqz v6, :cond_2

    .line 95
    new-instance v23, Ljava/util/HashMap;

    .end local v23    # "categoryIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    .line 96
    .restart local v23    # "categoryIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v56, Ljava/util/HashMap;

    .end local v56    # "onlyCategoryIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {v56 .. v56}, Ljava/util/HashMap;-><init>()V

    .line 97
    .restart local v56    # "onlyCategoryIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v30, 0x0

    .line 98
    .local v30, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 100
    .local v4, "resolver":Landroid/content/ContentResolver;
    :try_start_0
    sget-object v5, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_IMAGE_TABLE_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string/jumbo v13, "uri"

    aput-object v13, v6, v9

    const/4 v9, 0x1

    const-string v13, "date"

    aput-object v13, v6, v9

    const-string v7, "scene_type =? "

    const/4 v9, 0x1

    new-array v8, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v13, "People"

    aput-object v13, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v30

    .line 102
    if-eqz v30, :cond_1

    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 104
    :cond_0
    const/4 v6, 0x0

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v73

    .line 105
    .local v73, "uri":Ljava/lang/String;
    const/4 v6, 0x1

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v29

    .line 106
    .local v29, "creationDate":Ljava/lang/Long;
    const-string v6, "/"

    move-object/from16 v0, v73

    invoke-virtual {v0, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, v73

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v44

    .line 107
    .local v44, "imageId":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v44

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    move-object/from16 v0, v56

    move-object/from16 v1, v44

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_0

    .line 112
    .end local v29    # "creationDate":Ljava/lang/Long;
    .end local v44    # "imageId":Ljava/lang/String;
    .end local v73    # "uri":Ljava/lang/String;
    :cond_1
    invoke-static/range {v30 .. v30}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 116
    .end local v4    # "resolver":Landroid/content/ContentResolver;
    .end local v30    # "cursor":Landroid/database/Cursor;
    :cond_2
    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v6, :cond_3

    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v6, :cond_5

    .line 117
    :cond_3
    const/16 v47, 0x1

    .line 118
    new-instance v46, Ljava/util/HashMap;

    .end local v46    # "imgIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct/range {v46 .. v46}, Ljava/util/HashMap;-><init>()V

    .line 119
    .restart local v46    # "imgIDs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v45, 0x0

    .line 120
    .local v45, "imgID":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v50

    .line 122
    .local v50, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual/range {v50 .. v50}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 123
    const/4 v6, 0x0

    move-object/from16 v0, v50

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v9, "/face"

    invoke-virtual {v6, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 124
    invoke-virtual/range {v50 .. v50}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v39

    .local v39, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 125
    .local v49, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {v49 .. v49}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v41

    .line 126
    .local v41, "idNames":[Ljava/lang/String;
    const/4 v6, 0x1

    aget-object v45, v41, v6

    .line 127
    move-object/from16 v0, v46

    move-object/from16 v1, v45

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 112
    .end local v39    # "i$":Ljava/util/Iterator;
    .end local v41    # "idNames":[Ljava/lang/String;
    .end local v45    # "imgID":Ljava/lang/String;
    .end local v49    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v50    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v4    # "resolver":Landroid/content/ContentResolver;
    .restart local v30    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v6

    invoke-static/range {v30 .. v30}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v6

    .line 130
    .end local v4    # "resolver":Landroid/content/ContentResolver;
    .end local v30    # "cursor":Landroid/database/Cursor;
    .restart local v45    # "imgID":Ljava/lang/String;
    .restart local v50    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_4
    invoke-virtual/range {v50 .. v50}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v39

    .restart local v39    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 131
    .restart local v49    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {v49 .. v49}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v45

    .line 132
    move-object/from16 v0, v46

    move-object/from16 v1, v45

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 138
    .end local v39    # "i$":Ljava/util/Iterator;
    .end local v45    # "imgID":Ljava/lang/String;
    .end local v49    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v50    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_5
    new-instance v72, Ljava/util/HashMap;

    invoke-direct/range {v72 .. v72}, Ljava/util/HashMap;-><init>()V

    .line 139
    .local v72, "unnamedMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$Value;>;"
    new-instance v53, Ljava/util/HashMap;

    invoke-direct/range {v53 .. v53}, Ljava/util/HashMap;-><init>()V

    .line 140
    .local v53, "namedMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$Value;>;"
    const/16 v71, 0x0

    .line 142
    .local v71, "ungroupedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    new-instance v43, Ljava/util/HashMap;

    invoke-direct/range {v43 .. v43}, Ljava/util/HashMap;-><init>()V

    .line 143
    .local v43, "imageHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;>;"
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .local v22, "buf":Ljava/lang/StringBuilder;
    const/16 v35, 0x0

    .local v35, "faceId":I
    const/16 v61, 0x0

    .local v61, "personId":I
    const/16 v64, 0x0

    .local v64, "recommendedId":I
    const/16 v37, 0x0

    .local v37, "groupId":I
    const/16 v34, 0x0

    .local v34, "faceData":I
    const/16 v20, 0x0

    .local v20, "autoGroup":I
    const/16 v44, 0x0

    .line 145
    .local v44, "imageId":I
    const/16 v66, 0x0

    .line 147
    .local v66, "similarity":I
    const/16 v52, 0x0

    .local v52, "left":I
    const/16 v69, 0x0

    .local v69, "top":I
    const/16 v65, 0x0

    .local v65, "right":I
    const/16 v21, 0x0

    .line 148
    .local v21, "bottom":I
    const/16 v58, 0x0

    .line 149
    .local v58, "path":Lcom/sec/android/gallery3d/data/Path;
    const/16 v57, 0x0

    .line 150
    .local v57, "p":Ljava/lang/String;
    const/16 v51, 0x0

    .line 151
    .local v51, "key":Ljava/lang/String;
    const/16 v62, 0x0

    .line 152
    .local v62, "personName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 153
    .restart local v4    # "resolver":Landroid/content/ContentResolver;
    const/16 v33, 0x0

    .line 154
    .local v33, "faceCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCloudIds(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v26

    .line 156
    .local v26, "cloudIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v60, Ljava/util/HashMap;

    invoke-direct/range {v60 .. v60}, Ljava/util/HashMap;-><init>()V

    .line 159
    .local v60, "pathTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;>;"
    :try_start_1
    sget-object v5, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/16 v6, 0xc

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v13, "_id"

    aput-object v13, v6, v9

    const/4 v9, 0x1

    const-string v13, "person_id"

    aput-object v13, v6, v9

    const/4 v9, 0x2

    const-string v13, "recommended_id"

    aput-object v13, v6, v9

    const/4 v9, 0x3

    const-string v13, "group_id"

    aput-object v13, v6, v9

    const/4 v9, 0x4

    const-string v13, "face_data"

    aput-object v13, v6, v9

    const/4 v9, 0x5

    const-string v13, "auto_group"

    aput-object v13, v6, v9

    const/4 v9, 0x6

    const-string v13, "pos_left"

    aput-object v13, v6, v9

    const/4 v9, 0x7

    const-string v13, "pos_top"

    aput-object v13, v6, v9

    const/16 v9, 0x8

    const-string v13, "pos_right"

    aput-object v13, v6, v9

    const/16 v9, 0x9

    const-string v13, "pos_bottom"

    aput-object v13, v6, v9

    const/16 v9, 0xa

    const-string v13, "image_id"

    aput-object v13, v6, v9

    const/16 v9, 0xb

    const-string/jumbo v13, "similarity"

    aput-object v13, v6, v9

    const-string/jumbo v7, "usable=?"

    const/4 v9, 0x1

    new-array v8, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v8, v9

    const-string v9, "recommended_id desc,group_id desc"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v33

    .line 169
    if-eqz v33, :cond_2f

    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v6

    if-eqz v6, :cond_2f

    move/from16 v8, v37

    .end local v37    # "groupId":I
    .local v8, "groupId":I
    move/from16 v7, v64

    .line 171
    .end local v64    # "recommendedId":I
    .local v7, "recommendedId":I
    :cond_6
    const/4 v6, 0x0

    :try_start_2
    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v35

    .line 172
    const/4 v6, 0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v61

    .line 173
    const/4 v6, 0x2

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 174
    const/4 v6, 0x3

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 175
    const/4 v6, 0x4

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v34

    .line 176
    const/4 v6, 0x5

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 177
    const/4 v6, 0x6

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v52

    .line 178
    const/4 v6, 0x7

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v69

    .line 179
    const/16 v6, 0x8

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v65

    .line 180
    const/16 v6, 0x9

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 181
    const/16 v6, 0xa

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v44

    .line 182
    const/16 v6, 0xb

    move-object/from16 v0, v33

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v66

    .line 185
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 333
    :cond_7
    :goto_2
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v6

    if-nez v6, :cond_6

    .line 337
    :goto_3
    invoke-static/range {v33 .. v33}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 340
    invoke-virtual/range {v43 .. v43}, Ljava/util/HashMap;->clear()V

    .line 342
    invoke-virtual/range {v53 .. v53}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v39

    .restart local v39    # "i$":Ljava/util/Iterator;
    :cond_8
    :goto_4
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1f

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/util/Map$Entry;

    .line 343
    .local v31, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$Value;>;"
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterConfirmedCluster:Z

    if-eqz v6, :cond_9

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mConfirmedList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    .line 346
    :cond_9
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v51

    .end local v51    # "key":Ljava/lang/String;
    check-cast v51, Ljava/lang/String;

    .line 347
    .restart local v51    # "key":Ljava/lang/String;
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v9, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v9, v6, v13}, Lcom/sec/android/gallery3d/data/FaceClustering;->getPersonName(Landroid/content/ContentResolver;III)Ljava/lang/String;

    move-result-object v62

    .line 348
    if-eqz v62, :cond_a

    invoke-virtual/range {v62 .. v62}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 349
    const-string v9, "FaceClustering"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "entry.getValue().mPersonId = "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v13, ", entry.getValue().mGroupId = "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mKeys:Ljava/util/ArrayList;

    move-object/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    move-object/from16 v0, v62

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameIDs:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget-object v9, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mConfirmedList:Ljava/util/ArrayList;

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 355
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget-object v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mConfirmedList:Ljava/util/ArrayList;

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mPersonIds:Ljava/util/ArrayList;

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mGroupIds:Ljava/util/ArrayList;

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 189
    .end local v31    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$Value;>;"
    .end local v39    # "i$":Ljava/util/Iterator;
    :cond_b
    if-eqz v47, :cond_c

    .line 190
    if-eqz v46, :cond_c

    :try_start_3
    invoke-static/range {v44 .. v44}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v46

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 194
    :cond_c
    invoke-static/range {v44 .. v44}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v40

    .line 195
    .local v40, "id":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    if-eqz v6, :cond_d

    .line 196
    if-eqz v23, :cond_7

    if-eqz v56, :cond_7

    .line 198
    move-object/from16 v0, v23

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 199
    move-object/from16 v0, v56

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_d
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v51

    .line 205
    const/4 v6, 0x0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    move-object/from16 v0, v22

    invoke-virtual {v0, v6, v9}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 206
    const-string v6, "/face/"

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v44

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v35

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v34

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v52

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v69

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v65

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v61

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "people"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    .line 212
    invoke-static/range {v57 .. v57}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v58

    .line 214
    if-eqz v48, :cond_e

    if-eqz v23, :cond_e

    .line 215
    new-instance v9, Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;

    move-object/from16 v0, v23

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move/from16 v0, v61

    invoke-direct {v9, v14, v15, v7, v0}, Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;-><init>(JII)V

    move-object/from16 v0, v60

    move-object/from16 v1, v58

    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_e
    const/4 v6, 0x1

    if-le v7, v6, :cond_1d

    .line 219
    move-object/from16 v0, v53

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .line 220
    .local v5, "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    if-nez v5, :cond_f

    .line 221
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v10, "confirmedlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .end local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    const-string v9, "named"

    move-object/from16 v6, p0

    invoke-direct/range {v5 .. v11}, Lcom/sec/android/gallery3d/data/FaceClustering$Value;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 224
    .restart local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, v53

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    .end local v10    # "confirmedlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_f
    move/from16 v0, v61

    if-ne v7, v0, :cond_15

    .line 230
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mConfirmedList:Ljava/util/ArrayList;

    move-object/from16 v0, v58

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 232
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v63

    check-cast v63, Ljava/util/HashMap;

    .line 233
    .local v63, "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 234
    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;

    .line 235
    .local v42, "imageFaceValue":Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;
    move-object/from16 v0, v42

    iget v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mMaxSimilarity:I

    const v9, 0x7fffffff

    if-eq v6, v9, :cond_11

    .line 236
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v42

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 237
    move-object/from16 v0, v42

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v67

    .line 238
    .local v67, "tempPath":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    move-object/from16 v0, v42

    iget v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mFaceId:I

    invoke-static {v6, v9}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknownIgnoreAutoGroup(Landroid/content/Context;I)V

    .line 239
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v9, 0xa

    aget-object v9, v67, v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    .line 240
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .line 241
    .local v12, "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    if-nez v12, :cond_10

    .line 242
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 243
    .restart local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .end local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    const/4 v14, 0x1

    const/16 v6, 0xa

    aget-object v6, v67, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v9, 0x7f0e00c2

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    move-object/from16 v13, p0

    move-object/from16 v18, v11

    invoke-direct/range {v12 .. v18}, Lcom/sec/android/gallery3d/data/FaceClustering$Value;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 245
    .restart local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    .end local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_10
    const/16 v6, 0x8

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v67, v6

    .line 248
    iget-object v6, v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    const-string v9, "/"

    move-object/from16 v0, v67

    invoke-static {v0, v9}, Lcom/sec/android/gallery3d/data/Path;->fromString([Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    move/from16 v0, v35

    move-object/from16 v1, v42

    iput v0, v1, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mFaceId:I

    .line 251
    const v6, 0x7fffffff

    move-object/from16 v0, v42

    iput v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mMaxSimilarity:I

    .line 252
    move-object/from16 v0, v58

    move-object/from16 v1, v42

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 330
    .end local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .end local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .end local v42    # "imageFaceValue":Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;
    .end local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    .end local v67    # "tempPath":[Ljava/lang/String;
    :cond_11
    :goto_5
    const/4 v6, 0x1

    if-le v7, v6, :cond_12

    move/from16 v0, v61

    if-eq v7, v0, :cond_7

    :cond_12
    const/4 v6, -0x1

    move/from16 v0, v20

    if-ne v0, v6, :cond_7

    .line 331
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    move/from16 v0, v35

    invoke-static {v6, v0}, Lcom/sec/samsung/gallery/access/face/FaceList;->remove(Landroid/content/Context;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_2

    .line 337
    .end local v40    # "id":Ljava/lang/String;
    :catchall_1
    move-exception v6

    :goto_6
    invoke-static/range {v33 .. v33}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v6

    .line 255
    .restart local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .restart local v40    # "id":Ljava/lang/String;
    .restart local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    :cond_13
    :try_start_4
    new-instance v6, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;

    const v9, 0x7fffffff

    move-object/from16 v0, p0

    move/from16 v1, v35

    move-object/from16 v2, v58

    invoke-direct {v6, v0, v1, v9, v2}, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILcom/sec/android/gallery3d/data/Path;)V

    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    move-object/from16 v1, v63

    invoke-virtual {v0, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 259
    .end local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    :cond_14
    new-instance v63, Ljava/util/HashMap;

    invoke-direct/range {v63 .. v63}, Ljava/util/HashMap;-><init>()V

    .line 260
    .restart local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    new-instance v6, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;

    const v9, 0x7fffffff

    move-object/from16 v0, p0

    move/from16 v1, v35

    move-object/from16 v2, v58

    invoke-direct {v6, v0, v1, v9, v2}, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILcom/sec/android/gallery3d/data/Path;)V

    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    move-object/from16 v1, v63

    invoke-virtual {v0, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 263
    .end local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterConfirmedCluster:Z

    if-eqz v6, :cond_16

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mFilterConfirmedRecommendCluster:Z

    if-eqz v6, :cond_11

    .line 264
    :cond_16
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v58

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 266
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v63

    check-cast v63, Ljava/util/HashMap;

    .line 267
    .restart local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 268
    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;

    .line 269
    .restart local v42    # "imageFaceValue":Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;
    move-object/from16 v0, v42

    iget v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mMaxSimilarity:I

    move/from16 v0, v66

    if-ge v6, v0, :cond_18

    .line 270
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v42

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 271
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    move-object/from16 v0, v42

    iget v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mFaceId:I

    invoke-static {v6, v9}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknownIgnoreAutoGroup(Landroid/content/Context;I)V

    .line 272
    move-object/from16 v0, v42

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v67

    .line 273
    .restart local v67    # "tempPath":[Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v9, 0xa

    aget-object v9, v67, v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    .line 274
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .line 275
    .restart local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    if-nez v12, :cond_17

    .line 276
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .restart local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .end local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    const/4 v14, 0x1

    const/16 v6, 0xa

    aget-object v6, v67, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v9, 0x7f0e00c2

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    move-object/from16 v13, p0

    move-object/from16 v18, v11

    invoke-direct/range {v12 .. v18}, Lcom/sec/android/gallery3d/data/FaceClustering$Value;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 279
    .restart local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    .end local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_17
    const/16 v6, 0x8

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v67, v6

    .line 283
    iget-object v6, v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    const-string v9, "/"

    move-object/from16 v0, v67

    invoke-static {v0, v9}, Lcom/sec/android/gallery3d/data/Path;->fromString([Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    move/from16 v0, v35

    move-object/from16 v1, v42

    iput v0, v1, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mFaceId:I

    .line 286
    move/from16 v0, v66

    move-object/from16 v1, v42

    iput v0, v1, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mMaxSimilarity:I

    .line 287
    move-object/from16 v0, v58

    move-object/from16 v1, v42

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;->mPath:Lcom/sec/android/gallery3d/data/Path;

    goto/16 :goto_5

    .line 289
    .end local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .end local v67    # "tempPath":[Ljava/lang/String;
    :cond_18
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ltz v6, :cond_19

    .line 290
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    iget-object v9, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 292
    :cond_19
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    move/from16 v0, v35

    invoke-static {v6, v0}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknownIgnoreAutoGroup(Landroid/content/Context;I)V

    .line 293
    invoke-virtual/range {v58 .. v58}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v67

    .line 294
    .restart local v67    # "tempPath":[Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    .line 295
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .line 296
    .restart local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    if-nez v12, :cond_1a

    .line 297
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .restart local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .end local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v9, 0x7f0e00c2

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    move-object/from16 v13, p0

    move v15, v8

    move-object/from16 v18, v11

    invoke-direct/range {v12 .. v18}, Lcom/sec/android/gallery3d/data/FaceClustering$Value;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 299
    .restart local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    .end local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_1a
    const/16 v6, 0x8

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v67, v6

    .line 302
    iget-object v6, v12, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    const-string v9, "/"

    move-object/from16 v0, v67

    invoke-static {v0, v9}, Lcom/sec/android/gallery3d/data/Path;->fromString([Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 305
    .end local v12    # "unnameedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .end local v42    # "imageFaceValue":Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;
    .end local v67    # "tempPath":[Ljava/lang/String;
    :cond_1b
    new-instance v6, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;

    move-object/from16 v0, p0

    move/from16 v1, v35

    move/from16 v2, v66

    move-object/from16 v3, v58

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILcom/sec/android/gallery3d/data/Path;)V

    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    move-object/from16 v1, v63

    invoke-virtual {v0, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 310
    .end local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    :cond_1c
    new-instance v63, Ljava/util/HashMap;

    invoke-direct/range {v63 .. v63}, Ljava/util/HashMap;-><init>()V

    .line 311
    .restart local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    new-instance v6, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;

    move-object/from16 v0, p0

    move/from16 v1, v35

    move/from16 v2, v66

    move-object/from16 v3, v58

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILcom/sec/android/gallery3d/data/Path;)V

    move-object/from16 v0, v63

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v43

    move-object/from16 v1, v63

    invoke-virtual {v0, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 316
    .end local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .end local v63    # "recommendedHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$ImageFaceValue;>;"
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterNamedCluster:Z

    if-nez v6, :cond_11

    const/4 v6, -0x1

    move/from16 v0, v20

    if-eq v0, v6, :cond_11

    .line 317
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    .line 318
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .line 319
    .restart local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    if-nez v5, :cond_1e

    .line 320
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 321
    .restart local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .end local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v9, 0x7f0e00c2

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    move-object v13, v5

    move-object/from16 v14, p0

    move v15, v7

    move/from16 v16, v8

    move-object/from16 v19, v11

    invoke-direct/range {v13 .. v19}, Lcom/sec/android/gallery3d/data/FaceClustering$Value;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 322
    .restart local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    if-nez v8, :cond_1e

    .line 324
    move-object/from16 v71, v5

    .line 327
    .end local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_1e
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v58

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_5

    .line 360
    .end local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .end local v40    # "id":Ljava/lang/String;
    .restart local v39    # "i$":Ljava/util/Iterator;
    :cond_1f
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mIsCategory:Z

    if-eqz v6, :cond_24

    .line 361
    const/4 v7, 0x1

    .line 362
    const/4 v8, 0x0

    .line 363
    invoke-virtual/range {v56 .. v56}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v39

    :goto_7
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_22

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/util/Map$Entry;

    .line 364
    .local v32, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v32 .. v32}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 365
    .local v24, "category_id":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/gallery3d/data/FaceClustering;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v25

    .line 366
    .local v25, "category_path":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v48, :cond_20

    if-eqz v23, :cond_20

    .line 367
    new-instance v9, Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;

    invoke-virtual/range {v23 .. v24}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/4 v6, -0x1

    invoke-direct {v9, v14, v15, v7, v6}, Lcom/sec/android/gallery3d/data/FaceClustering$OrderingData;-><init>(JII)V

    move-object/from16 v0, v60

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    :cond_20
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    .line 370
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .line 371
    .restart local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    if-nez v5, :cond_21

    .line 372
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 373
    .restart local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .end local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    const v9, 0x7f0e00c2

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    move-object v13, v5

    move-object/from16 v14, p0

    move v15, v7

    move/from16 v16, v8

    move-object/from16 v19, v11

    invoke-direct/range {v13 .. v19}, Lcom/sec/android/gallery3d/data/FaceClustering$Value;-><init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 374
    .restart local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    move-object/from16 v0, v72

    move-object/from16 v1, v51

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    .end local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_21
    iget-object v6, v5, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 378
    .end local v5    # "value":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    .end local v24    # "category_id":Ljava/lang/String;
    .end local v25    # "category_path":Lcom/sec/android/gallery3d/data/Path;
    .end local v32    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_22
    if-eqz v23, :cond_23

    .line 379
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashMap;->clear()V

    .line 381
    :cond_23
    invoke-virtual/range {v56 .. v56}, Ljava/util/HashMap;->clear()V

    .line 384
    :cond_24
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mOnlyFilterNamedCluster:Z

    if-nez v6, :cond_2b

    .line 385
    const/16 v38, 0x1

    .line 386
    .local v38, "i":I
    const/16 v70, 0x0

    .line 387
    .local v70, "ungroupedKey":Ljava/lang/String;
    const/16 v68, 0x0

    .line 388
    .local v68, "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual/range {v72 .. v72}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v39

    :goto_8
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2a

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/util/Map$Entry;

    .line 389
    .restart local v31    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$Value;>;"
    if-eqz v71, :cond_25

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    move-object/from16 v0, v71

    if-ne v0, v6, :cond_25

    .line 390
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v70

    .end local v70    # "ungroupedKey":Ljava/lang/String;
    check-cast v70, Ljava/lang/String;

    .line 391
    .restart local v70    # "ungroupedKey":Ljava/lang/String;
    goto :goto_8

    .line 393
    :cond_25
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v51

    .end local v51    # "key":Ljava/lang/String;
    check-cast v51, Ljava/lang/String;

    .line 394
    .restart local v51    # "key":Ljava/lang/String;
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget-object v0, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v68, v0

    .line 395
    invoke-virtual/range {v68 .. v68}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v9, 0x1

    if-ne v6, v9, :cond_28

    .line 397
    const/4 v6, 0x0

    move-object/from16 v0, v68

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v55

    check-cast v55, Lcom/sec/android/gallery3d/data/Path;

    .line 398
    .local v55, "oldPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual/range {v55 .. v55}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v59

    .line 399
    .local v59, "pathStrings":[Ljava/lang/String;
    const/16 v6, 0xa

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v59, v6

    .line 400
    const-string v6, "/"

    move-object/from16 v0, v59

    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/data/Path;->fromString([Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v54

    .line 401
    .local v54, "newPath":Lcom/sec/android/gallery3d/data/Path;
    const/4 v6, 0x0

    move-object/from16 v0, v68

    move-object/from16 v1, v54

    invoke-virtual {v0, v6, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 402
    if-eqz v48, :cond_26

    .line 403
    move-object/from16 v0, v60

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object/from16 v0, v60

    move-object/from16 v1, v54

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    :cond_26
    if-nez v71, :cond_27

    .line 407
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    const/4 v9, 0x0

    iput v9, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    .line 408
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v71

    .end local v71    # "ungroupedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    check-cast v71, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    .line 409
    .restart local v71    # "ungroupedValue":Lcom/sec/android/gallery3d/data/FaceClustering$Value;
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v70

    .end local v70    # "ungroupedKey":Ljava/lang/String;
    check-cast v70, Ljava/lang/String;

    .restart local v70    # "ungroupedKey":Ljava/lang/String;
    goto/16 :goto_8

    .line 411
    :cond_27
    move-object/from16 v0, v71

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v0, v68

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .line 415
    .end local v54    # "newPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v55    # "oldPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v59    # "pathStrings":[Ljava/lang/String;
    :cond_28
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v9, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v4, v9, v6, v1}, Lcom/sec/android/gallery3d/data/FaceClustering;->getPersonName(Landroid/content/ContentResolver;III)Ljava/lang/String;

    move-result-object v62

    .line 416
    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    if-lez v6, :cond_29

    .line 417
    add-int/lit8 v38, v38, 0x1

    .line 418
    :cond_29
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mKeys:Ljava/util/ArrayList;

    move-object/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    move-object/from16 v0, v62

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameIDs:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    move-object/from16 v0, v28

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mPersonIds:Ljava/util/ArrayList;

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mGroupIds:Ljava/util/ArrayList;

    invoke-interface/range {v31 .. v31}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;

    iget v6, v6, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 426
    .end local v31    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/sec/android/gallery3d/data/FaceClustering$Value;>;"
    :cond_2a
    if-eqz v71, :cond_2b

    if-eqz v70, :cond_2b

    .line 427
    move-object/from16 v51, v70

    .line 428
    move-object/from16 v0, v71

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    move-object/from16 v68, v0

    .line 429
    move-object/from16 v0, v71

    iget v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    move-object/from16 v0, v71

    iget v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v4, v6, v9, v1}, Lcom/sec/android/gallery3d/data/FaceClustering;->getPersonName(Landroid/content/ContentResolver;III)Ljava/lang/String;

    move-result-object v62

    .line 430
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mKeys:Ljava/util/ArrayList;

    move-object/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNames:Ljava/util/ArrayList;

    move-object/from16 v0, v62

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameIDs:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mNameId:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    move-object/from16 v0, v28

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mPersonIds:Ljava/util/ArrayList;

    move-object/from16 v0, v71

    iget v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mGroupIds:Ljava/util/ArrayList;

    move-object/from16 v0, v71

    iget v9, v0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    .end local v38    # "i":I
    .end local v68    # "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v70    # "ungroupedKey":Ljava/lang/String;
    :cond_2b
    if-eqz v48, :cond_2d

    .line 440
    const-string v6, "FaceClustering"

    const-string v9, "FaceClustering sort start!!"

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    new-instance v36, Lcom/sec/android/gallery3d/data/FaceClustering$FaceClusterComparator;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mContext:Landroid/content/Context;

    move-object/from16 v0, v36

    move-object/from16 v1, v60

    invoke-direct {v0, v6, v1}, Lcom/sec/android/gallery3d/data/FaceClustering$FaceClusterComparator;-><init>(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 442
    .local v36, "fcc":Lcom/sec/android/gallery3d/data/FaceClustering$FaceClusterComparator;
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v39

    :goto_9
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2c

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/util/ArrayList;

    .line 443
    .local v27, "cluster":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    move-object/from16 v0, v27

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_9

    .line 445
    .end local v27    # "cluster":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_2c
    const-string v6, "FaceClustering"

    const-string v9, "FaceClustering sort end!!"

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    .end local v36    # "fcc":Lcom/sec/android/gallery3d/data/FaceClustering$FaceClusterComparator;
    :cond_2d
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    if-eqz v6, :cond_2e

    .line 449
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 450
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    .line 452
    :cond_2e
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/data/FaceClustering;->mClusters:Ljava/util/ArrayList;

    .line 454
    invoke-virtual/range {v53 .. v53}, Ljava/util/HashMap;->clear()V

    .line 455
    invoke-virtual/range {v72 .. v72}, Ljava/util/HashMap;->clear()V

    .line 456
    const-string v6, "FaceClustering"

    const-string v9, "FaceClustering end!!"

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    return-void

    .line 337
    .end local v7    # "recommendedId":I
    .end local v8    # "groupId":I
    .end local v39    # "i$":Ljava/util/Iterator;
    .restart local v37    # "groupId":I
    .restart local v64    # "recommendedId":I
    :catchall_2
    move-exception v6

    move/from16 v8, v37

    .end local v37    # "groupId":I
    .restart local v8    # "groupId":I
    move/from16 v7, v64

    .end local v64    # "recommendedId":I
    .restart local v7    # "recommendedId":I
    goto/16 :goto_6

    .end local v7    # "recommendedId":I
    .end local v8    # "groupId":I
    .restart local v37    # "groupId":I
    .restart local v64    # "recommendedId":I
    :cond_2f
    move/from16 v8, v37

    .end local v37    # "groupId":I
    .restart local v8    # "groupId":I
    move/from16 v7, v64

    .end local v64    # "recommendedId":I
    .restart local v7    # "recommendedId":I
    goto/16 :goto_3
.end method
