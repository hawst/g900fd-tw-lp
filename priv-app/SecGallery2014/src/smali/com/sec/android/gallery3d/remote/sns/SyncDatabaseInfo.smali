.class public Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo;
.super Ljava/lang/Object;
.source "SyncDatabaseInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo$SYNCDB_PHOTO_COLUMS;,
        Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo$SYNCDB_ALBUM_COLUMS;
    }
.end annotation


# static fields
.field public static final ALBUM_COUNT:I = 0x9

.field public static final ALBUM_COVER_PHOTO:I = 0x7

.field public static final ALBUM_CREATED_TIME:I = 0xa

.field public static final ALBUM_DESCRIPTION:I = 0x4

.field public static final ALBUM_FROM_ID:I = 0x2

.field public static final ALBUM_FROM_NAME:I = 0x1

.field public static final ALBUM_ID:I = 0x0

.field public static final ALBUM_LINK:I = 0x6

.field public static final ALBUM_LOCATION:I = 0x5

.field public static final ALBUM_NAME:I = 0x3

.field public static final ALBUM_PRIVACY:I = 0x8

.field public static final ALBUM_TYPE:I = 0xc

.field public static final ALBUM_UPDATED_TIME:I = 0xb

.field public static final PHOTO_CREATED_TIME:I = 0xc

.field public static final PHOTO_FROM_ID:I = 0x3

.field public static final PHOTO_FROM_NAME:I = 0x2

.field public static final PHOTO_HEIGHT:I = 0x7

.field public static final PHOTO_ICON:I = 0xa

.field public static final PHOTO_ID:I = 0x0

.field public static final PHOTO_IMAGES_HEIGHT:I = 0xf

.field public static final PHOTO_IMAGES_URL:I = 0x10

.field public static final PHOTO_IMAGES_WIDTH:I = 0xe

.field public static final PHOTO_LINK:I = 0x9

.field public static final PHOTO_NAME:I = 0x4

.field public static final PHOTO_PARENT_ID:I = 0x1

.field public static final PHOTO_POSITION:I = 0xb

.field public static final PHOTO_SCREENNAIL_URL:I = 0x6

.field public static final PHOTO_THUMBNAIL_URL:I = 0x5

.field public static final PHOTO_UPDATED_TIME:I = 0xd

.field public static final PHOTO_WIDTH:I = 0x8

.field public static final SNS_ALBUM_PROJECTION:[Ljava/lang/String;

.field public static final SNS_PHOTO_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "id"

    aput-object v1, v0, v3

    const-string v1, "from_name"

    aput-object v1, v0, v4

    const-string v1, "from_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "description"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "location"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "link"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "cover_photo"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "privacy"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "count"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "created_time"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "updated_time"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo;->SNS_ALBUM_PROJECTION:[Ljava/lang/String;

    .line 109
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "target_id"

    aput-object v1, v0, v4

    const-string v1, "from_name"

    aput-object v1, v0, v5

    const-string v1, "from_id"

    aput-object v1, v0, v6

    const-string v1, "name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "picture"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "source"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "link"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "icon"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "position"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "created_time"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "updated_time"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "image_width"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "image_height"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo;->SNS_PHOTO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    return-void
.end method
