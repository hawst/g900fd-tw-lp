.class Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "PhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0

    .prologue
    .line 3795
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PhotoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/PhotoView$1;

    .prologue
    .line 3795
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 3821
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->onScale(Landroid/view/ScaleGestureDetector;F)Z

    move-result v0

    return v0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;F)Z
    .locals 7
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;
    .param p2, "fScale"    # F

    .prologue
    const/4 v6, 0x1

    .line 3797
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->isHelpViewingScrollMode()Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4800(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3816
    :cond_0
    :goto_0
    return v6

    .line 3799
    :cond_1
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 3800
    .local v0, "scale":F
    :goto_1
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v1

    if-eqz v1, :cond_2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->isAlmostEquals(FF)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5000(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isVideoPlayIcon()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 3806
    :cond_4
    if-eqz p1, :cond_5

    .line 3807
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8102(F)F

    .line 3808
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8202(F)F

    .line 3811
    :cond_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOnScale:Z
    invoke-static {v1, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8302(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 3812
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIgnoreUpEvent:Z
    invoke-static {v1, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8402(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 3814
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v3

    if-eqz p1, :cond_7

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8100()F

    move-result v1

    move v2, v1

    :goto_2
    if-eqz p1, :cond_8

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8200()F

    move-result v1

    :goto_3
    invoke-virtual {v3, v0, v2, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->scaleBy(FFF)I

    .line 3815
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onScale(F)V

    goto/16 :goto_0

    .end local v0    # "scale":F
    :cond_6
    move v0, p2

    .line 3799
    goto/16 :goto_1

    .line 3814
    .restart local v0    # "scale":F
    :cond_7
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterX:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8500(Lcom/sec/android/gallery3d/ui/PhotoView;)F

    move-result v1

    move v2, v1

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterY:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8600(Lcom/sec/android/gallery3d/ui/PhotoView;)F

    move-result v1

    goto :goto_3
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 8
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3826
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "onScaleBegin"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3827
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->isHelpViewingScrollMode()Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4800(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3866
    :goto_0
    return v3

    .line 3830
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5000(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1900(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    if-eq v5, v3, :cond_2

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingState:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1900(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_2

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isVideoPlayIcon()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mLoadingProgressState:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->LOADING_PROGRESS_RUNNING:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3400()I

    move-result v6

    if-ne v5, v6, :cond_3

    :cond_2
    move v3, v4

    .line 3832
    goto :goto_0

    .line 3833
    :cond_3
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v5

    if-eqz v5, :cond_4

    move v3, v4

    .line 3834
    goto :goto_0

    .line 3836
    :cond_4
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "onScaleBegin pass"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3838
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const-wide/16 v6, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleEndStartTime:J
    invoke-static {v4, v6, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6202(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    .line 3840
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v4, :cond_5

    .line 3841
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 3842
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFaceIndicatorView:Lcom/sec/android/gallery3d/ui/FaceIndicatorView;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/FaceIndicatorView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/FaceIndicatorView;->hidePopupMenu()V

    .line 3846
    :cond_5
    if-eqz p1, :cond_6

    .line 3847
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v4

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8102(F)F

    .line 3848
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8202(F)F

    .line 3851
    :cond_6
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    .line 3852
    .local v1, "focusX":F
    :goto_1
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    .line 3854
    .local v2, "focusY":F
    :goto_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 3855
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v4

    sget-object v5, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 3856
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector;->updateFocusPoint(FF)V

    .line 3859
    :cond_7
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->beginScale(FF)V

    .line 3862
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onScaleBegin(F)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3863
    :catch_0
    move-exception v0

    .line 3864
    .local v0, "Ne":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 3851
    .end local v0    # "Ne":Ljava/lang/NullPointerException;
    .end local v1    # "focusX":F
    .end local v2    # "focusY":F
    :cond_8
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterX:F
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8500(Lcom/sec/android/gallery3d/ui/PhotoView;)F

    move-result v1

    goto :goto_1

    .line 3852
    .restart local v1    # "focusX":F
    :cond_9
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCenterY:F
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8600(Lcom/sec/android/gallery3d/ui/PhotoView;)F

    move-result v2

    goto :goto_2
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v2, 0x0

    .line 3871
    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onScaleEnd"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3872
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->isHelpViewingScrollMode()Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$4800(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3885
    :goto_0
    return-void

    .line 3874
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->endScale()V

    .line 3876
    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8102(F)F

    .line 3877
    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8202(F)F

    .line 3878
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/MotionDetector;->checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3879
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v0

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 3881
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onScaleEnd()V

    .line 3883
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mScaleEndStartTime:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6202(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    .line 3884
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyScaleListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionScaleBegin:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8702(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    goto :goto_0
.end method
