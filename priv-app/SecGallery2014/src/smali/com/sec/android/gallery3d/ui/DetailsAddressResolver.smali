.class public Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;
.super Ljava/lang/Object;
.source "DetailsAddressResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;,
        Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressLookupJob;
    }
.end annotation


# instance fields
.field private mAddressLookupJob:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mContext:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mHandler:Landroid/os/Handler;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mContext:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mAddressLookupJob:Lcom/sec/android/gallery3d/util/Future;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;Landroid/location/Address;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;
    .param p1, "x1"    # Landroid/location/Address;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->updateLocation(Landroid/location/Address;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private updateLocation(Landroid/location/Address;)V
    .locals 6
    .param p1, "address"    # Landroid/location/Address;

    .prologue
    .line 96
    if-eqz p1, :cond_8

    .line 97
    const-string v4, "Location"

    const-string v5, "address is not null"

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mContext:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    .line 99
    .local v1, "context":Landroid/content/Context;
    const-string v0, ""

    .line 100
    .local v0, "addressText":Ljava/lang/String;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v4, :cond_2

    .line 101
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v4

    if-gt v2, v4, :cond_1

    .line 102
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 103
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1, v2}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 107
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 108
    invoke-virtual {p1}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v0

    .line 112
    .end local v2    # "i":I
    :cond_2
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v4, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 113
    :cond_3
    const/16 v4, 0x9

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/location/Address;->getSubAdminArea()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {p1}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {p1}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-virtual {p1}, Landroid/location/Address;->getSubThoroughfare()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    invoke-virtual {p1}, Landroid/location/Address;->getPremises()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    invoke-virtual {p1}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x8

    invoke-virtual {p1}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 124
    .local v3, "parts":[Ljava/lang/String;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_7

    .line 125
    aget-object v4, v3, v2

    if-eqz v4, :cond_4

    aget-object v4, v3, v2

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 124
    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 126
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 127
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 129
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 132
    .end local v2    # "i":I
    .end local v3    # "parts":[Ljava/lang/String;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mListener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;->onAddressAvailable(Ljava/lang/String;)V

    .line 134
    .end local v0    # "addressText":Ljava/lang/String;
    .end local v1    # "context":Landroid/content/Context;
    :cond_8
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mAddressLookupJob:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mAddressLookupJob:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mAddressLookupJob:Lcom/sec/android/gallery3d/util/Future;

    .line 141
    :cond_0
    return-void
.end method

.method public resolveAddress([DLcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;
    .locals 8
    .param p1, "latlng"    # [D
    .param p2, "listener"    # Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    .prologue
    .line 65
    const/4 v1, 0x0

    .line 67
    .local v1, "result":Ljava/lang/String;
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mListener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    .line 68
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mContext:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressLookupJob;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressLookupJob;-><init>(Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;[D)V

    new-instance v4, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$1;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$1;-><init>(Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/DetailsAddressResolver;->mAddressLookupJob:Lcom/sec/android/gallery3d/util/Future;

    .line 87
    :try_start_0
    const-string v2, "(%f,%f)"

    const/4 v3, 0x0

    aget-wide v4, p1, v3

    const/4 v3, 0x1

    aget-wide v6, p1, v3

    invoke-static {v2, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatLatitudeLongitude(Ljava/lang/String;DD)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 92
    :goto_0
    return-object v1

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
