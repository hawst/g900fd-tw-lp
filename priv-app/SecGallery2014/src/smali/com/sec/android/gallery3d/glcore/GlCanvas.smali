.class public Lcom/sec/android/gallery3d/glcore/GlCanvas;
.super Lcom/sec/android/gallery3d/glcore/TextureBase;
.source "GlCanvas.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlCanvas$1;,
        Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;,
        Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;,
        Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    }
.end annotation


# static fields
.field public static ASYNC_RENDER_STATE_APPLIED:I = 0x0

.field public static ASYNC_RENDER_STATE_NONE:I = 0x0

.field public static ASYNC_RENDER_STATE_POSTPONED:I = 0x0

.field public static ASYNC_RENDER_STATE_PROCESSED:I = 0x0

.field public static ASYNC_RENDER_STATE_RUNNING:I = 0x0

.field public static ASYNC_RENDER_STATE_SUSPENDED:I = 0x0

.field private static final BITMAP_COUNT_POOL:I = 0x20

.field private static final BITMAP_COUNT_TYPE:I = 0x30

.field private static final TAG:Ljava/lang/String;

.field private static final mConfig:Landroid/graphics/Bitmap$Config;


# instance fields
.field public mASyncRenderState:I

.field protected mCanvas:Landroid/graphics/Canvas;

.field private mContentBitmap:Landroid/graphics/Bitmap;

.field private mItemIndex:I

.field public mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

.field public mManualRecycle:Z

.field public mReferCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_NONE:I

    .line 19
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_RUNNING:I

    .line 20
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_PROCESSED:I

    .line 21
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_APPLIED:I

    .line 22
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_SUSPENDED:I

    .line 23
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_POSTPONED:I

    .line 28
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mConfig:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V
    .locals 3
    .param p1, "root"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "textureWidth"    # I
    .param p3, "textureHeight"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/TextureBase;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    .line 29
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 30
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    .line 31
    sget v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_NONE:I

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mItemIndex:I

    .line 33
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    .line 34
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mManualRecycle:Z

    .line 35
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    .line 39
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setSize(II)V

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "root"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/TextureBase;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    .line 29
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 30
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    .line 31
    sget v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_NONE:I

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mItemIndex:I

    .line 33
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    .line 34
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mManualRecycle:Z

    .line 35
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mReferCount:I

    .line 44
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 45
    return-void
.end method

.method public static createUploadBitmapCache()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;-><init>(I)V

    return-object v0
.end method

.method public static destroyUploadBitmapCache(Ljava/lang/Object;)V
    .locals 7
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 204
    move-object v0, p0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .line 206
    .local v0, "buffer":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$100(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 207
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$200(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    move-result-object v5

    aget-object v3, v5, v1

    .line 208
    .local v3, "itemSet":[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$300(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I

    move-result-object v5

    aget v5, v5, v1

    if-ge v4, v5, :cond_1

    .line 209
    aget-object v2, v3, v4

    .line 210
    .local v2, "item":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$700(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 211
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$700(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 212
    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$702(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 214
    :cond_0
    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$602(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 215
    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-static {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$402(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;Lcom/sec/android/gallery3d/glcore/GlCanvas;)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 208
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 206
    .end local v2    # "item":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    .end local v3    # "itemSet":[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    .end local v4    # "j":I
    :cond_2
    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mKeySet:[I
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$002(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;[I)[I

    move-object v5, v6

    .line 219
    check-cast v5, [[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$202(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    .line 220
    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$302(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;[I)[I

    .line 221
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I
    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$102(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;I)I

    .line 222
    return-void
.end method

.method public static updateUploadBitmapCache(Ljava/lang/Object;Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 8
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 184
    move-object v0, p0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .line 186
    .local v0, "canvasSet":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$100(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 187
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$200(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    move-result-object v6

    aget-object v3, v6, v2

    .line 188
    .local v3, "itemSet":[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$300(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I

    move-result-object v6

    aget v6, v6, v2

    if-ge v4, v6, :cond_2

    .line 189
    aget-object v5, v3, v4

    .line 190
    .local v5, "mItem":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    if-nez v5, :cond_0

    .line 188
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 193
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-static {v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$400(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v1

    .line 194
    .local v1, "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    if-eqz v1, :cond_1

    iget v6, v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    sget v7, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_PROCESSED:I

    if-ne v6, v7, :cond_1

    .line 195
    sget v6, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_APPLIED:I

    iput v6, v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 196
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setTexture(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 198
    :cond_1
    const/4 v6, 0x0

    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$402(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;Lcom/sec/android/gallery3d/glcore/GlCanvas;)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    goto :goto_2

    .line 186
    .end local v1    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .end local v5    # "mItem":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 201
    .end local v3    # "itemSet":[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    .end local v4    # "j":I
    :cond_3
    return-void
.end method


# virtual methods
.method public getContentBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onClearBitmap()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public onFreeBitmap()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 157
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mItemIndex:I

    .line 158
    .local v1, "index":I
    if-ne v1, v6, :cond_0

    .line 166
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v4, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUploadBmCache:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .line 161
    .local v0, "canvasSet":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$200(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    move-result-object v4

    shr-int/lit8 v5, v1, 0x10

    aget-object v3, v4, v5

    .line 162
    .local v3, "itemSet":[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    const v4, 0xffff

    and-int/2addr v4, v1

    aget-object v2, v3, v4

    .line 164
    .local v2, "item":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    const/4 v4, 0x0

    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-static {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$402(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;Lcom/sec/android/gallery3d/glcore/GlCanvas;)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 165
    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mItemIndex:I

    goto :goto_0
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 15

    .prologue
    const/16 v14, 0x20

    const/4 v13, 0x0

    const/4 v10, 0x0

    .line 81
    iget-object v11, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    if-eqz v11, :cond_0

    .line 82
    iget-object v10, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 147
    :goto_0
    return-object v10

    .line 84
    :cond_0
    iget-object v11, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, v11, Lcom/sec/android/gallery3d/glcore/GlRootView;->mUploadBmCache:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;

    .line 89
    .local v2, "canvasSet":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;
    if-nez v2, :cond_1

    .line 90
    sget-object v11, Lcom/sec/android/gallery3d/glcore/GlCanvas;->TAG:Ljava/lang/String;

    const-string v12, "onGetBitmap return null reason canvasSet == null"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    :cond_1
    monitor-enter v2

    .line 94
    :try_start_0
    iget v11, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    if-lez v11, :cond_2

    iget v11, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    if-gtz v11, :cond_3

    .line 95
    :cond_2
    sget-object v11, Lcom/sec/android/gallery3d/glcore/GlCanvas;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onGetBitmap return null reason size : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " x "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    monitor-exit v2

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v10

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 98
    :cond_3
    :try_start_1
    iget v11, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    iget v12, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    shl-int/lit8 v12, v12, 0xe

    or-int v6, v11, v12

    .line 99
    .local v6, "key":I
    const/4 v3, 0x0

    .line 100
    .local v3, "index":I
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mKeySet:[I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$000(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I

    move-result-object v7

    .line 101
    .local v7, "keySet":[I
    if-nez v7, :cond_4

    .line 102
    sget-object v11, Lcom/sec/android/gallery3d/glcore/GlCanvas;->TAG:Ljava/lang/String;

    const-string v12, "onGetBitmap return null reason keySet == null"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    monitor-exit v2

    goto :goto_0

    .line 105
    :cond_4
    :goto_1
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$100(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I

    move-result v11

    if-ge v3, v11, :cond_5

    aget v11, v7, v3

    if-eq v6, v11, :cond_5

    .line 106
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 108
    :cond_5
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$100(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I

    move-result v11

    if-ne v3, v11, :cond_7

    .line 109
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$100(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I

    move-result v11

    const/16 v12, 0x30

    if-ne v11, v12, :cond_6

    .line 110
    sget-object v11, Lcom/sec/android/gallery3d/glcore/GlCanvas;->TAG:Ljava/lang/String;

    const-string v12, "Reached max bitmap type count : current = 48"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    monitor-exit v2

    goto :goto_0

    .line 113
    :cond_6
    aput v6, v7, v3

    .line 114
    # ++operator for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mTypeCount:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$104(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)I

    .line 115
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$200(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    move-result-object v11

    const/16 v12, 0x20

    new-array v12, v12, [Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    aput-object v12, v11, v3

    .line 116
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$300(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I

    move-result-object v11

    const/4 v12, 0x0

    aput v12, v11, v3

    .line 118
    :cond_7
    const/4 v9, 0x0

    .line 119
    .local v9, "subIndex":I
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$300(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I

    move-result-object v11

    aget v8, v11, v3

    .line 120
    .local v8, "subCount":I
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mItem:[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$200(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    move-result-object v11

    aget-object v5, v11, v3

    .line 121
    .local v5, "itemSet":[Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    :goto_2
    if-ge v9, v8, :cond_8

    aget-object v11, v5, v9

    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-static {v11}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$400(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v11

    if-eqz v11, :cond_8

    .line 122
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 124
    :cond_8
    if-ne v9, v8, :cond_b

    .line 125
    if-ne v8, v14, :cond_9

    .line 126
    sget-object v11, Lcom/sec/android/gallery3d/glcore/GlCanvas;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Queue is full!"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", size : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " x "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    sget v11, Lcom/sec/android/gallery3d/glcore/GlCanvas;->ASYNC_RENDER_STATE_POSTPONED:I

    iput v11, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mASyncRenderState:I

    .line 128
    monitor-exit v2

    goto/16 :goto_0

    .line 130
    :cond_9
    iget v10, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    iget v11, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    sget-object v12, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 131
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_a

    .line 132
    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unable to alloc bitmap buffer (onGetBitmap) size : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mWidth:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " x "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mHeight:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 134
    :cond_a
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->mCount:[I
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;->access$300(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasSetInfo;)[I

    move-result-object v10

    aget v11, v10, v3

    add-int/lit8 v11, v11, 0x1

    aput v11, v10, v3

    .line 135
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 136
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;

    const/4 v10, 0x0

    invoke-direct {v4, v0, v1, p0, v10}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;Lcom/sec/android/gallery3d/glcore/GlCanvas;Lcom/sec/android/gallery3d/glcore/GlCanvas$1;)V

    .line 137
    .local v4, "item":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    aput-object v4, v5, v9

    .line 143
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mCanvas:Landroid/graphics/Canvas;

    .line 145
    shl-int/lit8 v10, v3, 0x10

    or-int/2addr v10, v9

    iput v10, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mItemIndex:I

    .line 146
    sget-object v10, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v13, v10}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 147
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$700(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)Landroid/graphics/Bitmap;

    move-result-object v10

    goto/16 :goto_0

    .line 139
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "item":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    :cond_b
    :try_start_2
    aget-object v4, v5, v9

    .line 140
    .restart local v4    # "item":Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;
    # getter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$600(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;)Landroid/graphics/Canvas;

    move-result-object v1

    .line 141
    .restart local v1    # "canvas":Landroid/graphics/Canvas;
    # setter for: Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-static {v4, p0}, Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;->access$402(Lcom/sec/android/gallery3d/glcore/GlCanvas$CanvasItem;Lcom/sec/android/gallery3d/glcore/GlCanvas;)Lcom/sec/android/gallery3d/glcore/GlCanvas;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/TextureBase;->recycle()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 171
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mState:I

    .line 175
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mBitmap:Landroid/graphics/Bitmap;

    .line 176
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 177
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    .line 48
    if-nez p1, :cond_1

    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 58
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setSize(II)V

    .line 59
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mState:I

    .line 60
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mContentValid:Z

    goto :goto_0
.end method

.method public setListener(Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mListener:Lcom/sec/android/gallery3d/glcore/GlCanvas$GlCanvasListener;

    .line 73
    return-void
.end method

.method public setManualRecycle(Z)V
    .locals 0
    .param p1, "isManualRecycle"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mManualRecycle:Z

    .line 69
    return-void
.end method
