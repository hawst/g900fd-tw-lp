.class public Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "SearchClusterAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchClusterAlbumSet"


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

.field private mKind:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "kind"    # I

    .prologue
    .line 32
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .line 33
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 34
    iput p4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mKind:I

    .line 35
    invoke-virtual {p3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 36
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->initClusterAlbumSet()V

    .line 37
    return-void
.end method

.method private initClusterAlbumSet()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 40
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "basePath":Ljava/lang/String;
    const/16 v4, 0x1000

    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 42
    .local v3, "timeClusterPath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aput-object v4, v5, v6

    .line 43
    const/4 v4, 0x4

    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "locationClusterPath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v6, 0x1

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aput-object v4, v5, v6

    .line 45
    const/16 v4, 0x20

    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "faceClusterPath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aput-object v4, v5, v7

    .line 47
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v4, v4, v7

    if-eqz v4, :cond_0

    .line 48
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v4, v4, v7

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->setSearchArcMode()V

    .line 50
    :cond_0
    return-void
.end method


# virtual methods
.method public getAlbums()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/ClusterAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/ClusterAlbum;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getAlbums()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    return-object v1
.end method

.method public getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method public getClusterKind()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mKind:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, "SearchClusterAlbumSet"

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->getSubMediaSetCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->getAlbums()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getSubMediaSetCount()I

    move-result v2

    add-int/2addr v0, v2

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    :cond_0
    return v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->notifyContentChanged()V

    .line 94
    return-void
.end method

.method public reload()J
    .locals 6

    .prologue
    .line 81
    const/4 v1, 0x0

    .line 82
    .local v1, "isDirty":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 83
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getDataVersion()J

    move-result-wide v2

    .line 84
    .local v2, "oldVersion":J
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->reload()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-lez v4, :cond_0

    .line 85
    const/4 v1, 0x1

    .line 82
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    .end local v2    # "oldVersion":J
    :cond_1
    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mDataVersion:J

    :goto_1
    return-wide v4

    :cond_2
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->mDataVersion:J

    goto :goto_1
.end method
