.class Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;
.super Lcom/sec/android/gallery3d/ui/PositionController$Animatable;
.source "PositionController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PositionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FilmRatio"
.end annotation


# instance fields
.field public mCurrentRatio:F

.field public mFromRatio:F

.field public mToRatio:F

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PositionController;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/PositionController;)V
    .locals 1

    .prologue
    .line 2006
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;-><init>(Lcom/sec/android/gallery3d/ui/PositionController$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/PositionController;Lcom/sec/android/gallery3d/ui/PositionController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/PositionController$1;

    .prologue
    .line 2006
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;-><init>(Lcom/sec/android/gallery3d/ui/PositionController;)V

    return-void
.end method

.method private doAnimation(FI)Z
    .locals 2
    .param p1, "targetRatio"    # F
    .param p2, "kind"    # I

    .prologue
    .line 2025
    iput p2, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mAnimationKind:I

    .line 2026
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mCurrentRatio:F

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mFromRatio:F

    .line 2027
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mToRatio:F

    .line 2028
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->startTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mAnimationStartTime:J

    .line 2029
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mAnimationDuration:I

    .line 2030
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->advanceAnimation()Z

    .line 2031
    const/4 v0, 0x1

    return v0

    .line 2029
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->ANIM_TIME:[I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2100()[I

    move-result-object v1

    aget v1, v1, p2

    invoke-virtual {v0, p2, v1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->getAnimationDuration(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected interpolate(F)Z
    .locals 4
    .param p1, "progress"    # F

    .prologue
    const/4 v0, 0x1

    .line 2036
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_1

    .line 2037
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mToRatio:F

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mCurrentRatio:F

    .line 2041
    :cond_0
    :goto_0
    return v0

    .line 2040
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mFromRatio:F

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mToRatio:F

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mFromRatio:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mCurrentRatio:F

    .line 2041
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mCurrentRatio:F

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mToRatio:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startSnapback()Z
    .locals 1

    .prologue
    .line 2020
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->startSnapback(I)Z

    move-result v0

    return v0
.end method

.method public startSnapback(I)Z
    .locals 2
    .param p1, "kind"    # I

    .prologue
    .line 2013
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1100(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2014
    .local v0, "target":F
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->mToRatio:F

    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 2015
    :goto_1
    return v1

    .line 2013
    .end local v0    # "target":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2015
    .restart local v0    # "target":F
    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/PositionController$FilmRatio;->doAnimation(FI)Z

    move-result v1

    goto :goto_1
.end method
