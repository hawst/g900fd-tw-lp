.class final enum Lcom/sec/android/gallery3d/data/MediaItem$TagType;
.super Ljava/lang/Enum;
.source "MediaItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/MediaItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TagType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaItem$TagType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/data/MediaItem$TagType;

.field public static final enum AUTO_FACE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

.field public static final enum FACE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

.field public static final enum GROUP:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

.field public static final enum NONE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117
    new-instance v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->NONE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    new-instance v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    const-string v1, "FACE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/data/MediaItem$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->FACE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    new-instance v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/data/MediaItem$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->GROUP:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    new-instance v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    const-string v1, "AUTO_FACE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/gallery3d/data/MediaItem$TagType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->AUTO_FACE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    .line 116
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    sget-object v1, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->NONE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->FACE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->GROUP:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->AUTO_FACE:Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->$VALUES:[Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaItem$TagType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 116
    const-class v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/data/MediaItem$TagType;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/gallery3d/data/MediaItem$TagType;->$VALUES:[Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/data/MediaItem$TagType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/data/MediaItem$TagType;

    return-object v0
.end method
