.class public Lcom/sec/android/gallery3d/data/CategoryAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "CategoryAlbumSet.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CategoryAlbumSet"

.field public static final TOP_PATH:Ljava/lang/String; = "/local/categoryalbumset"


# instance fields
.field private mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mBaseUri:Landroid/net/Uri;

.field private final mCateogryType:Ljava/lang/String;

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mWatchUri:[Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cateogryType"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {}, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 30
    sget-object v0, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_IMAGE_TABLE_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mBaseUri:Landroid/net/Uri;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mBaseUri:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/gallery3d/data/CategoryAlbum;->UPDATE_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mWatchUri:[Landroid/net/Uri;

    .line 39
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 40
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mCateogryType:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/local/categoryalbum/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mCateogryType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 42
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mWatchUri:[Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;[Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 43
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalMediaItemCount()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mAlbum:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 69
    invoke-static {}, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mDataVersion:J

    .line 71
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/CategoryAlbumSet;->mDataVersion:J

    return-wide v0
.end method
