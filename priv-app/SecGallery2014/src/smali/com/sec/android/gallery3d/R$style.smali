.class public final Lcom/sec/android/gallery3d/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AccountSettingSectionItem:I = 0x7f110045

.field public static final AccountSettingSectionList:I = 0x7f110044

.field public static final ActionBarTwoLineItem:I = 0x7f110008

.field public static final ActionBarTwoLinePrimary:I = 0x7f110006

.field public static final ActionBarTwoLineSecondary:I = 0x7f110007

.field public static final ActionButtonStyle:I = 0x7f110032

.field public static final Animation_OnScreenHint:I = 0x7f11000d

.field public static final BestPhotoActionBar_ActionButton:I = 0x7f110058

.field public static final BestPhotoActionBar_MenuTextStyle:I = 0x7f110057

.field public static final BestPhotoCustomActionButton:I = 0x7f11005b

.field public static final CameraControls:I = 0x7f110025

.field public static final CustomActionButton:I = 0x7f11004f

.field public static final CustomActionButtonImage:I = 0x7f110050

.field public static final CustomActionButtonText:I = 0x7f110051

.field public static final CustomActionButtonTextNew:I = 0x7f110052

.field public static final DetailAdvacneTextItem:I = 0x7f110043

.field public static final DetailsTextItem:I = 0x7f110042

.field public static final DialogTranscluent:I = 0x7f11003a

.field public static final DialogTransparent:I = 0x7f110047

.field public static final EffectSettingGrid:I = 0x7f110016

.field public static final EffectSettingItem:I = 0x7f110017

.field public static final EffectSettingItemTitle:I = 0x7f110018

.field public static final EffectSettingTypeTitle:I = 0x7f110019

.field public static final EffectTitleSeparator:I = 0x7f11001b

.field public static final EffectTypeSeparator:I = 0x7f11001a

.field public static final Gallery2013Spinner:I = 0x7f110030

.field public static final Gallery2013SpinnerBlack:I = 0x7f110031

.field public static final GalleryCustomPopupMenu:I = 0x7f11005a

.field public static final GalleryMenuTextApearance:I = 0x7f110059

.field public static final Gz_Holo_ActionButton_Overflow:I = 0x7f110041

.field public static final HideActionModeClose:I = 0x7f110038

.field public static final Holo_ActionBar:I = 0x7f110035

.field public static final Holo_ActionBarItem:I = 0x7f110037

.field public static final Holo_ActionBar_SmallProgressBar:I = 0x7f110036

.field public static final Holo_WidgetActionBar:I = 0x7f110055

.field public static final ImageNote_SeekBar:I = 0x7f11003d

.field public static final MediaButton_Play:I = 0x7f110004

.field public static final MenuIndicator:I = 0x7f110024

.field public static final MenuTextStyles:I = 0x7f11005c

.field public static final MoreInfo_details_button_text_style:I = 0x7f11004c

.field public static final MoreInfo_details_header_text_item:I = 0x7f11004e

.field public static final MoreInfo_details_text_item:I = 0x7f11004d

.field public static final MoreInfo_item_content_text_style:I = 0x7f110049

.field public static final MoreInfo_item_expandable_text_style:I = 0x7f11004b

.field public static final MoreInfo_item_locaition_text_style:I = 0x7f11004a

.field public static final MoreInfo_item_title_text_style:I = 0x7f110048

.field public static final MyCheckBox:I = 0x7f110046

.field public static final OnScreenHintTextAppearance:I = 0x7f11000b

.field public static final OnScreenHintTextAppearance_Small:I = 0x7f11000c

.field public static final OnViewfinderLabel:I = 0x7f110014

.field public static final PanoCustomDialogText:I = 0x7f110015

.field public static final PanoCustomDialogText_xlarge:I = 0x7f110021

.field public static final PopupTitleSeparator:I = 0x7f11000f

.field public static final PopupTitleText_xlarge:I = 0x7f110020

.field public static final QuickEffectStyle:I = 0x7f11003b

.field public static final ReviewControlText_xlarge:I = 0x7f11001f

.field public static final ReviewPlayIcon:I = 0x7f11000e

.field public static final SecurityPreferenceButton:I = 0x7f110040

.field public static final SecurityPreferenceButtonContainer:I = 0x7f11003f

.field public static final SettingItemList:I = 0x7f110010

.field public static final SettingItemText:I = 0x7f110012

.field public static final SettingItemTitle:I = 0x7f110011

.field public static final SettingRow:I = 0x7f110013

.field public static final SwitcherButton:I = 0x7f110023

.field public static final TextAppearance_DialogWindowTitle:I = 0x7f11001c

.field public static final TextAppearance_Medium:I = 0x7f11001d

.field public static final Theme_BestPhoto:I = 0x7f110056

.field public static final Theme_Camera:I = 0x7f110009

.field public static final Theme_CameraBase:I = 0x7f11000a

.field public static final Theme_Crop:I = 0x7f110003

.field public static final Theme_CustomDivider:I = 0x7f110053

.field public static final Theme_DeviceDefault:I = 0x7f110039

.field public static final Theme_Gallery:I = 0x7f110002

.field public static final Theme_GalleryAlbumTime:I = 0x7f11002a

.field public static final Theme_GalleryBase:I = 0x7f110000

.field public static final Theme_GalleryBlackStyle:I = 0x7f11002b

.field public static final Theme_GalleryCrop:I = 0x7f11002f

.field public static final Theme_GalleryPhone:I = 0x7f11002c

.field public static final Theme_GalleryPhonePicker:I = 0x7f11002d

.field public static final Theme_GalleryPhoneWithOutTitle:I = 0x7f110033

.field public static final Theme_GalleryPhoneWithTitle:I = 0x7f110034

.field public static final Theme_GalleryPhone_Gz:I = 0x7f11002e

.field public static final Theme_Gallery_Dialog:I = 0x7f110001

.field public static final Theme_ImageNote:I = 0x7f11003c

.field public static final Theme_ProxyLauncher:I = 0x7f110005

.field public static final Theme_WidgetPreference:I = 0x7f110054

.field public static final UndoBar:I = 0x7f110026

.field public static final UndoBarSeparator:I = 0x7f110028

.field public static final UndoBarTextAppearance:I = 0x7f110027

.field public static final UndoButton:I = 0x7f110029

.field public static final ViewfinderLabelLayout_xlarge:I = 0x7f110022

.field public static final Widget_Button_Borderless:I = 0x7f11001e

.field public static final textview_list_section_divider:I = 0x7f11003e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
