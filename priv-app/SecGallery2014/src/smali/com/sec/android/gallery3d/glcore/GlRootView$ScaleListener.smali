.class Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "GlRootView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlRootView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0

    .prologue
    .line 1538
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlRootView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/glcore/GlRootView$1;

    .prologue
    .line 1538
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v0, :cond_0

    .line 1544
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchScale(Landroid/view/ScaleGestureDetector;)V

    .line 1546
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 1551
    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onScaleBegin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1552
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$900(Lcom/sec/android/gallery3d/glcore/GlRootView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1553
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    .line 1555
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 1560
    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onScaleEnd"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$900(Lcom/sec/android/gallery3d/glcore/GlRootView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1562
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$ScaleListener;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->dispatchScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 1564
    :cond_0
    return-void
.end method
