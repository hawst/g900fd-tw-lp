.class public Lcom/sec/android/gallery3d/remote/sns/SNSImage;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaItem;
.source "SNSImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/sns/SNSImage$1;,
        Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSLargeImageRequest;,
        Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;,
        Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;
    }
.end annotation


# static fields
.field public static final DOWNLOAD_DIR:Ljava/io/File;

.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final TAG:Ljava/lang/String; = "SNSImage"


# instance fields
.field private mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

.field private mIsVideo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "download"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->DOWNLOAD_DIR:Ljava/io/File;

    .line 63
    const-string v0, "/sns/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/sns/PhotoTable;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "entry"    # Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 71
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .line 73
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/remote/sns/SNSImage;I)Ljava/net/URL;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    .param p1, "x1"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getPhotoUrl(I)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/remote/sns/SNSImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;J)Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    .locals 2
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "id"    # J

    .prologue
    .line 76
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p2, p3}, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;->getPhotoEntry(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    move-result-object v0

    .line 77
    .local v0, "entry":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 78
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-direct {v1, p0, p1, v0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/sns/PhotoTable;)V

    goto :goto_0
.end method

.method private getPhotoUrl(I)Ljava/net/URL;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 175
    const/4 v1, 0x0

    .line 177
    .local v1, "url":Ljava/net/URL;
    packed-switch p1, :pswitch_data_0

    .line 186
    :try_start_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    .end local v1    # "url":Ljava/net/URL;
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/net/MalformedURLException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 180
    .end local v0    # "e":Ljava/net/MalformedURLException;
    .restart local v1    # "url":Ljava/net/URL;
    :pswitch_0
    :try_start_1
    new-instance v1, Ljava/net/URL;

    .end local v1    # "url":Ljava/net/URL;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->thumbnail_url:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 191
    .restart local v1    # "url":Ljava/net/URL;
    :goto_0
    return-object v1

    .line 183
    :pswitch_1
    new-instance v1, Ljava/net/URL;

    .end local v1    # "url":Ljava/net/URL;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    .line 184
    .restart local v1    # "url":Ljava/net/URL;
    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static isScreenNailisFull(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "c"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 297
    :try_start_0
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "split_s":[Ljava/lang/String;
    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 299
    .local v0, "split_c":[Ljava/lang/String;
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 300
    const/4 v2, 0x1

    .line 305
    .end local v0    # "split_c":[Ljava/lang/String;
    .end local v1    # "split_s":[Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 303
    :catch_0
    move-exception v3

    goto :goto_0
.end method


# virtual methods
.method public download(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 398
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 399
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/app/DownloadManager$Request;

    invoke-direct {v2, v1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    move-result-object v2

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    move-result-object v0

    .line 401
    .local v0, "request":Landroid/app/DownloadManager$Request;
    invoke-static {p1, v0}, Lcom/sec/samsung/gallery/util/DownloadUtil;->download(Landroid/content/Context;Landroid/app/DownloadManager$Request;)V

    .line 402
    return-void
.end method

.method public getAlbumId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->album_id:Ljava/lang/String;

    return-object v0
.end method

.method public getCommentsCount()I
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->comments_count:I

    return v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 280
    sget-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDateInMs()J
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->date_taken:J

    return-wide v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 318
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 319
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->title:Ljava/lang/String;

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 321
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v1

    .line 322
    .local v1, "formater":Ljava/text/DateFormat;
    const/4 v2, 0x4

    new-instance v3, Ljava/util/Date;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->date_taken:J

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 324
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->width:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 325
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 326
    const/16 v2, 0xf

    const-string v3, "%dx%d"

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v5, v5, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->width:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v5, v5, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->height:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v10, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 331
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->latitude:D

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->longitude:D

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 332
    const/16 v2, 0x9

    new-array v3, v9, [D

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->latitude:D

    aput-wide v4, v3, v8

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v4, v4, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->longitude:D

    aput-wide v4, v3, v6

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 333
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->latitude:D

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 334
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->longitude:D

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 341
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_status:I

    if-ne v2, v10, :cond_0

    .line 343
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->extractExifInfo(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)V

    .line 345
    :cond_0
    return-object v0

    .line 336
    :cond_1
    const/16 v2, 0x9

    invoke-virtual {v0, v2, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 337
    const/16 v2, 0xa

    invoke-virtual {v0, v2, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 338
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v7}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 393
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->height:I

    return v0
.end method

.method public getLatLong([D)V
    .locals 4
    .param p1, "latLong"    # [D

    .prologue
    .line 239
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v2, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->latitude:D

    aput-wide v2, p1, v0

    .line 240
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v2, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->longitude:D

    aput-wide v2, p1, v0

    .line 241
    return-void
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->latitude:D

    return-wide v0
.end method

.method public getLikesCount()I
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->likes_count:I

    return v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->longitude:D

    return-wide v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mIsVideo:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_type:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoTable()Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    return-object v0
.end method

.method public getPlayUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_status:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 289
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 291
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->rotation:I

    return v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->size:J

    .line 358
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->source_id:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 266
    const-wide v0, 0xc00890000464L

    .line 269
    .local v0, "operation":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_type:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 270
    const-wide/16 v2, -0x41

    and-long/2addr v0, v2

    .line 272
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mIsVideo:Z

    if-eqz v2, :cond_1

    .line 273
    const-wide v2, -0x800000001L

    and-long/2addr v0, v2

    .line 275
    :cond_1
    return-wide v0
.end method

.method public getTags()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->width:I

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 8
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    .line 88
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 89
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;Lcom/sec/android/gallery3d/data/Path;I)V

    .line 103
    :goto_0
    return-object v1

    .line 91
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 92
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 93
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;Lcom/sec/android/gallery3d/data/Path;I)V

    goto :goto_0

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget v1, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_status:I

    if-ne v1, v2, :cond_2

    .line 97
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getDateInMs()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v7, v6, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    move v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_2
    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    if-ne p1, v2, :cond_4

    .line 100
    :cond_3
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getDateInMs()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v7, v6, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    move v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_4
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/remote/sns/SNSImage$DirectDecodeRequest;-><init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;Lcom/sec/android/gallery3d/remote/sns/SNSImage$1;)V

    goto :goto_0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 220
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->isScreenNailisFull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 222
    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSLargeImageRequest;

    new-instance v3, Ljava/net/URL;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v4, v4, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, p0, v3}, Lcom/sec/android/gallery3d/remote/sns/SNSImage$SNSLargeImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/sns/SNSImage;Ljava/net/URL;)V

    .line 228
    :goto_0
    return-object v1

    .line 224
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4}, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "SNSImage"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    .line 228
    goto :goto_0
.end method

.method protected updateContent(Lcom/sec/android/gallery3d/remote/sns/PhotoTable;)V
    .locals 2
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 250
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .line 251
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mDataVersion:J

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mData:Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    .line 255
    invoke-static {}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->mDataVersion:J

    goto :goto_0
.end method
