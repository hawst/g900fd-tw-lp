.class public Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;
.super Lcom/sec/android/gallery3d/data/ImageCacheRequest;
.source "LocalImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/LocalImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocalImageRequest"
.end annotation


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mIsGolf:Z

.field private mLocalFilePath:Ljava/lang/String;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mPath:Lcom/sec/android/gallery3d/data/Path;

.field private mType:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;ILjava/lang/String;)V
    .locals 8
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "type"    # I
    .param p4, "localFilePath"    # Ljava/lang/String;

    .prologue
    .line 337
    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V

    .line 339
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;)V
    .locals 9
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "timeModified"    # J
    .param p5, "type"    # I
    .param p6, "localFilePath"    # Ljava/lang/String;

    .prologue
    .line 344
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;Z)V

    .line 345
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JILjava/lang/String;Z)V
    .locals 11
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "timeModified"    # J
    .param p5, "type"    # I
    .param p6, "localFilePath"    # Ljava/lang/String;
    .param p7, "isGolf"    # Z

    .prologue
    .line 349
    invoke-static/range {p5 .. p5}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v9

    const/4 v10, 0x1

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-wide v6, p3

    move/from16 v8, p5

    invoke-direct/range {v3 .. v10}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JIIZ)V

    .line 350
    move/from16 v0, p5

    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mType:I

    .line 351
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    .line 352
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 353
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 354
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mIsGolf:Z

    .line 355
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->getObject()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 356
    return-void
.end method


# virtual methods
.method public onDecodeOriginal(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "type"    # I

    .prologue
    const/4 v7, 0x0

    .line 418
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mIsGolf:Z

    if-eqz v8, :cond_1

    .line 419
    new-instance v8, Lcom/sec/android/gallery3d/golf/GolfMgr;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/golf/GolfMgr;-><init>()V

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/golf/GolfMgr;->CreateThumbnails(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 420
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_0

    move-object v0, v7

    .line 469
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object v0

    .line 425
    :cond_1
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 426
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v8, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 427
    invoke-static {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v5

    .line 430
    .local v5, "targetSize":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    if-nez v8, :cond_2

    .line 431
    # getter for: Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->access$100()Ljava/lang/String;

    move-result-object v8

    const-string v9, "mLocalFilePath is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 432
    goto :goto_0

    .line 435
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-boolean v7, v7, Lcom/sec/android/gallery3d/data/MediaItem;->isdrm:Z

    if-eqz v7, :cond_3

    .line 436
    const/4 v7, 0x1

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->isPreview:Z

    .line 437
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-static {p1, v7, v3}, Lcom/sec/android/gallery3d/data/DecodeUtils;->requestDecode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 442
    :cond_3
    const/4 v7, 0x2

    if-eq p2, v7, :cond_4

    const/4 v7, 0x3

    if-ne p2, v7, :cond_6

    .line 443
    :cond_4
    new-instance v2, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 444
    .local v2, "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    const/4 v6, 0x0

    .line 446
    .local v6, "thumbData":[B
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lcom/sec/android/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V

    .line 447
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getThumbnail()[B
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v6

    .line 455
    :goto_1
    if-eqz v6, :cond_5

    .line 456
    invoke-static {p1, v6, v3, v5}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeIfBigEnough(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BLandroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 458
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 469
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v6    # "thumbData":[B
    :cond_5
    :goto_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-static {p1, v7, v3, v5, p2}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 448
    .restart local v2    # "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .restart local v6    # "thumbData":[B
    :catch_0
    move-exception v1

    .line 449
    .local v1, "e":Ljava/io/FileNotFoundException;
    # getter for: Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->access$100()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to find file to read thumbnail: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 450
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 451
    .local v1, "e":Ljava/io/IOException;
    # getter for: Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->access$100()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get thumbnail from: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 452
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 453
    .local v4, "t":Ljava/lang/Throwable;
    # getter for: Lcom/sec/android/gallery3d/data/LocalImage;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalImage;->access$100()Ljava/lang/String;

    move-result-object v7

    const-string v8, "fail to get exif thumb"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 461
    .end local v2    # "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v4    # "t":Ljava/lang/Throwable;
    .end local v6    # "thumbData":[B
    :cond_6
    const/4 v7, 0x5

    if-ne p2, v7, :cond_5

    .line 463
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-static {p1, v7, v3, v5, p2}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodePanoramaThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    goto/16 :goto_0

    .line 464
    :catch_3
    move-exception v1

    .line 465
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_2
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const v5, 0x7f0200a7

    const/4 v4, 0x1

    .line 360
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/data/MediaItem;->isdrm:Z

    if-eqz v2, :cond_2

    .line 361
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isValidRights(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 362
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->setBroken(Z)V

    .line 363
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mType:I

    if-ne v2, v4, :cond_1

    .line 364
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 389
    :cond_0
    :goto_0
    return-object v0

    .line 366
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 369
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/data/ImageCacheRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 370
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_3

    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 371
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_0

    .line 372
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mType:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    .line 373
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->setBroken(Z)V

    .line 374
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    .line 375
    .local v1, "supported":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v4, 0x4

    invoke-virtual {v2, v4, v5, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->setAttribute(JZ)V

    .line 376
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v2, v2, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_5

    if-nez v1, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSignatureFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 378
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->loadRectOfAllFaces()V

    .line 380
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->updatePendingAttribute()V

    goto :goto_0

    .line 384
    .end local v1    # "supported":Z
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_7

    .line 385
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->setBroken(Z)V

    .line 386
    :cond_7
    iget v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mType:I

    if-ne v2, v4, :cond_8

    .line 387
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 389
    :cond_8
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 322
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public updatePendingAttribute()V
    .locals 14

    .prologue
    const-wide/32 v12, 0x80000

    const-wide/16 v10, 0x20

    const-wide/16 v8, 0x8

    const-wide/16 v6, 0x200

    const/4 v4, 0x0

    .line 395
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasPendingAttribute(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "group_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "selection":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_0

    .line 398
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/LocalImage;->isBurstShotImage(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v2, v6, v7, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->setAttribute(JZ)V

    .line 399
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v6, v7, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->setPendingAttribute(JZ)V

    .line 403
    .end local v0    # "selection":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v8, v9}, Lcom/sec/android/gallery3d/data/MediaItem;->hasPendingAttribute(J)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v12, v13}, Lcom/sec/android/gallery3d/data/MediaItem;->hasPendingAttribute(J)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v10, v11}, Lcom/sec/android/gallery3d/data/MediaItem;->hasPendingAttribute(J)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 406
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_3

    .line 407
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    # invokes: Lcom/sec/android/gallery3d/data/LocalImage;->getLocalImageAttributes()Lcom/sec/android/gallery3d/data/LocalImageAttributes;
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->access$000(Lcom/sec/android/gallery3d/data/LocalImage;)Lcom/sec/android/gallery3d/data/LocalImageAttributes;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImageAttributes;->updateFileAccessAttribute()V

    .line 408
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v8, v9, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->setPendingAttribute(JZ)V

    .line 409
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v12, v13, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->setPendingAttribute(JZ)V

    .line 410
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalImageRequest;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v10, v11, v4}, Lcom/sec/android/gallery3d/data/MediaItem;->setPendingAttribute(JZ)V

    .line 412
    :cond_4
    return-void
.end method
