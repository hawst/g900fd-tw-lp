.class Lcom/sec/android/gallery3d/ui/PositionController$Platform;
.super Lcom/sec/android/gallery3d/ui/PositionController$Animatable;
.source "PositionController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PositionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Platform"
.end annotation


# instance fields
.field public mCurrentX:I

.field public mCurrentY:I

.field public mDefaultX:I

.field public mDefaultY:I

.field public mFlingOffset:I

.field public mFromX:I

.field public mFromY:I

.field public mToX:I

.field public mToY:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PositionController;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/PositionController;)V
    .locals 1

    .prologue
    .line 1632
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController$Animatable;-><init>(Lcom/sec/android/gallery3d/ui/PositionController$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/PositionController;Lcom/sec/android/gallery3d/ui/PositionController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/PositionController$1;

    .prologue
    .line 1632
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;-><init>(Lcom/sec/android/gallery3d/ui/PositionController;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/PositionController$Platform;III)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PositionController$Platform;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 1632
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->doAnimation(III)Z

    move-result v0

    return v0
.end method

.method private doAnimation(III)Z
    .locals 4
    .param p1, "targetX"    # I
    .param p2, "targetY"    # I
    .param p3, "kind"    # I

    .prologue
    const/4 v1, 0x0

    .line 1706
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    if-ne v0, p2, :cond_0

    .line 1716
    :goto_0
    return v1

    .line 1707
    :cond_0
    iput p3, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    .line 1708
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromX:I

    .line 1709
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromY:I

    .line 1710
    iput p1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    .line 1711
    iput p2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToY:I

    .line 1712
    invoke-static {}, Lcom/sec/android/gallery3d/ui/AnimationTime;->startTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    .line 1713
    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationDuration:I

    .line 1714
    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFlingOffset:I

    .line 1715
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->advanceAnimation()Z

    .line 1716
    const/4 v1, 0x1

    goto :goto_0

    .line 1713
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->ANIM_TIME:[I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2100()[I

    move-result-object v2

    aget v2, v2, p3

    invoke-virtual {v0, p3, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->getAnimationDuration(II)I

    move-result v0

    goto :goto_1
.end method

.method private interpolateFlingFilm(F)Z
    .locals 3
    .param p1, "progress"    # F

    .prologue
    .line 1731
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2200(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/common/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/common/Scroller;->computeScrollOffset()Z

    .line 1732
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2200(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/common/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/common/Scroller;->getCurrX()I

    move-result v1

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFlingOffset:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1734
    const/4 v0, -0x1

    .line 1735
    .local v0, "dir":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    if-ge v1, v2, :cond_2

    .line 1736
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mHasNext:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2300(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1737
    const/4 v0, 0x3

    .line 1744
    :cond_0
    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 1748
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2200(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/common/Scroller;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/common/Scroller;->forceFinished(Z)V

    .line 1749
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1751
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFilmScroller:Lcom/sec/android/gallery3d/common/Scroller;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2200(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/common/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/common/Scroller;->isFinished()Z

    move-result v1

    return v1

    .line 1739
    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    if-le v1, v2, :cond_0

    .line 1740
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mHasPrev:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2400(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1741
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private interpolateFlingPage(F)Z
    .locals 8
    .param p1, "progress"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    .line 1755
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2500(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/FlingScroller;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/sec/android/gallery3d/ui/FlingScroller;->computeScrollOffset(F)V

    .line 1756
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$900(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/util/RangeArray;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1757
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    iget v6, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(F)V
    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2600(Lcom/sec/android/gallery3d/ui/PositionController;F)V

    .line 1759
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1760
    .local v1, "oldX":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2500(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/FlingScroller;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/FlingScroller;->getCurrX()I

    move-result v5

    iput v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1763
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1500(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v5

    if-le v1, v5, :cond_1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1500(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v6

    if-ne v5, v6, :cond_1

    .line 1764
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2500(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/FlingScroller;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/FlingScroller;->getCurrVelocityX()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    add-float/2addr v5, v7

    float-to-int v2, v5

    .line 1765
    .local v2, "v":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$700(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    move-result-object v5

    const/4 v6, 0x3

    invoke-interface {v5, v2, v6}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onAbsorb(II)V

    .line 1771
    .end local v2    # "v":I
    :cond_0
    :goto_0
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, p1, v5

    if-ltz v5, :cond_2

    :goto_1
    return v3

    .line 1766
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1600(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v5

    if-ge v1, v5, :cond_0

    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1600(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 1767
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mPageScroller:Lcom/sec/android/gallery3d/ui/FlingScroller;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2500(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/FlingScroller;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/FlingScroller;->getCurrVelocityX()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v5, v7

    float-to-int v2, v5

    .line 1768
    .restart local v2    # "v":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PositionController;->access$700(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    move-result-object v5

    invoke-interface {v5, v2, v3}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->onAbsorb(II)V

    goto :goto_0

    .end local v2    # "v":I
    :cond_2
    move v3, v4

    .line 1771
    goto :goto_1
.end method

.method private interpolateLinear(F)Z
    .locals 6
    .param p1, "progress"    # F

    .prologue
    const/16 v5, 0xa

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1776
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_1

    .line 1777
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1778
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToY:I

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    .line 1789
    :cond_0
    :goto_0
    return v0

    .line 1781
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    if-ne v2, v5, :cond_2

    .line 1782
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/CaptureAnimation;->calculateSlide(F)F

    move-result p1

    .line 1784
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromX:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromX:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1785
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromY:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToY:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mFromY:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    .line 1786
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    if-ne v2, v5, :cond_3

    move v0, v1

    .line 1787
    goto :goto_0

    .line 1789
    :cond_3
    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToX:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mToY:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected interpolate(F)Z
    .locals 2
    .param p1, "progress"    # F

    .prologue
    .line 1721
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 1722
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->interpolateFlingPage(F)Z

    move-result v0

    .line 1726
    :goto_0
    return v0

    .line 1723
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1724
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->interpolateFlingFilm(F)Z

    move-result v0

    goto :goto_0

    .line 1726
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->interpolateLinear(F)Z

    move-result v0

    goto :goto_0
.end method

.method public startSnapback()Z
    .locals 1

    .prologue
    .line 1683
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->startSnapback(I)Z

    move-result v0

    return v0
.end method

.method public startSnapback(I)Z
    .locals 12
    .param p1, "kind"    # I

    .prologue
    const/4 v7, 0x0

    .line 1639
    iget-wide v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationStartTime:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-eqz v8, :cond_1

    .line 1678
    :cond_0
    :goto_0
    return v7

    .line 1640
    :cond_1
    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mListener:Lcom/sec/android/gallery3d/ui/PositionController$Listener;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$700(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/ui/PositionController$Listener;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/gallery3d/ui/PositionController$Listener;->isHoldingDown()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1642
    :cond_2
    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mAnimationKind:I

    const/16 v9, 0xc

    if-ne v8, v9, :cond_3

    .line 1643
    iget-object v7, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullView()V

    .line 1644
    const/4 v7, 0x1

    goto :goto_0

    .line 1646
    :cond_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mInScale:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$800(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1648
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoxes:Lcom/sec/android/gallery3d/util/RangeArray;
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$900(Lcom/sec/android/gallery3d/ui/PositionController;)Lcom/sec/android/gallery3d/util/RangeArray;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/util/RangeArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;

    .line 1649
    .local v0, "b":Lcom/sec/android/gallery3d/ui/PositionController$Box;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mExtraScalingRange:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1000(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    const v9, 0x3f7fbe77    # 0.999f

    mul-float v4, v8, v9

    .line 1651
    .local v4, "scaleMin":F
    :goto_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mExtraScalingRange:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1000(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMax:F

    const/high16 v9, 0x3f800000    # 1.0f

    mul-float v3, v8, v9

    .line 1653
    .local v3, "scaleMax":F
    :goto_2
    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    invoke-static {v8, v4, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v1

    .line 1654
    .local v1, "scale":F
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    .line 1655
    .local v5, "x":I
    iget v6, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultY:I

    .line 1656
    .local v6, "y":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1100(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1657
    iget v5, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    .line 1675
    :goto_3
    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentX:I

    if-ne v8, v5, :cond_4

    iget v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mCurrentY:I

    if-eq v8, v6, :cond_0

    .line 1676
    :cond_4
    invoke-direct {p0, v5, v6, p1}, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->doAnimation(III)Z

    move-result v7

    goto :goto_0

    .line 1649
    .end local v1    # "scale":F
    .end local v3    # "scaleMax":F
    .end local v4    # "scaleMin":F
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_5
    iget v4, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMin:F

    goto :goto_1

    .line 1651
    .restart local v4    # "scaleMin":F
    :cond_6
    iget v3, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mScaleMax:F

    goto :goto_2

    .line 1659
    .restart local v1    # "scale":F
    .restart local v3    # "scaleMax":F
    .restart local v5    # "x":I
    .restart local v6    # "y":I
    :cond_7
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController;->calculateStableBound(FI)V
    invoke-static {v8, v1, v7}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1200(Lcom/sec/android/gallery3d/ui/PositionController;FI)V

    .line 1669
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # invokes: Lcom/sec/android/gallery3d/ui/PositionController;->viewWiderThanScaledImage(F)Z
    invoke-static {v8, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1300(Lcom/sec/android/gallery3d/ui/PositionController;F)Z

    move-result v8

    if-nez v8, :cond_8

    .line 1670
    iget v8, v0, Lcom/sec/android/gallery3d/ui/PositionController$Box;->mCurrentScale:F

    sub-float v2, v8, v1

    .line 1671
    .local v2, "scaleDiff":F
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFocusX:F
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1400(Lcom/sec/android/gallery3d/ui/PositionController;)F

    move-result v8

    mul-float/2addr v8, v2

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v8, v9

    float-to-int v8, v8

    add-int/2addr v5, v8

    .line 1673
    .end local v2    # "scaleDiff":F
    :cond_8
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoundLeft:I
    invoke-static {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1500(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mBoundRight:I
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1600(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v9

    invoke-static {v5, v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v5

    goto :goto_3
.end method

.method public updateDefaultXY()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1694
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mConstrained:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1700(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1800(Lcom/sec/android/gallery3d/ui/PositionController;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1695
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1800(Lcom/sec/android/gallery3d/ui/PositionController;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mViewW:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1900(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    .line 1696
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mFilmMode:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1100(Lcom/sec/android/gallery3d/ui/PositionController;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultY:I

    .line 1702
    :goto_1
    return-void

    .line 1696
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mConstrainedFrame:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->access$1800(Lcom/sec/android/gallery3d/ui/PositionController;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->this$0:Lcom/sec/android/gallery3d/ui/PositionController;

    # getter for: Lcom/sec/android/gallery3d/ui/PositionController;->mViewH:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->access$2000(Lcom/sec/android/gallery3d/ui/PositionController;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    goto :goto_0

    .line 1699
    :cond_1
    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultX:I

    .line 1700
    iput v0, p0, Lcom/sec/android/gallery3d/ui/PositionController$Platform;->mDefaultY:I

    goto :goto_1
.end method
