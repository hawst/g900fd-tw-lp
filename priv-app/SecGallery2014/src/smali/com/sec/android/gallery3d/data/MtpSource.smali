.class public Lcom/sec/android/gallery3d/data/MtpSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "MtpSource.java"


# static fields
.field private static final MTP_DEVICE:I = 0x1

.field private static final MTP_DEVICESET:I = 0x0

.field private static final MTP_ITEM:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MtpSource"


# instance fields
.field mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 34
    const-string v0, "mtp"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 36
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/mtp"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/mtp/*"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/mtp/item/*/*"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 40
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpContext;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/MtpContext;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    .line 41
    return-void
.end method

.method public static isMtpPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 78
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->getPrefix()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mtp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v2, 0x0

    .line 45
    const-string v0, "MtpSource"

    const-string v1, "Start"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 63
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :pswitch_0
    const-string v0, "MtpSource"

    const-string v1, "DEVICESET"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpDeviceSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/gallery3d/data/MtpDeviceSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MtpContext;)V

    .line 60
    :goto_0
    return-object v0

    .line 52
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    .line 53
    .local v3, "deviceId":I
    const-string v0, "MtpSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "device id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpDevice;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-direct {v0, p1, v1, v3, v2}, Lcom/sec/android/gallery3d/data/MtpDevice;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;ILcom/sec/android/gallery3d/data/MtpContext;)V

    goto :goto_0

    .line 57
    .end local v3    # "deviceId":I
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v3

    .line 58
    .restart local v3    # "deviceId":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v4

    .line 59
    .local v4, "objectId":I
    const-string v0, "MtpSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "device id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " object"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpImage;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/MtpImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IILcom/sec/android/gallery3d/data/MtpContext;)V

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MtpContext;->pause()V

    .line 70
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpSource;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MtpContext;->resume()V

    .line 75
    return-void
.end method
