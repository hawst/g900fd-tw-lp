.class Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;
.super Ljava/lang/Object;
.source "PicasaContentProvider.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/picasa/GDataParser$EntryHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncUserAlbums(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Lcom/sec/android/gallery3d/remote/picasa/UserEntry;Landroid/content/SyncResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

.field final synthetic val$albumSchema:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

.field final synthetic val$context:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

.field final synthetic val$db:Landroid/database/sqlite/SQLiteDatabase;

.field final synthetic val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

.field final synthetic val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

.field final synthetic val$syncResult:Landroid/content/SyncResult;

.field final synthetic val$user:Lcom/sec/android/gallery3d/remote/picasa/UserEntry;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;Lcom/sec/android/gallery3d/remote/picasa/UserEntry;Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;Landroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 536
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$user:Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$albumSchema:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    iput-object p6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p7, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$context:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    iput-object p8, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$syncResult:Landroid/content/SyncResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleEntry(Lcom/sec/android/gallery3d/remote/picasa/Entry;)V
    .locals 11
    .param p1, "entry"    # Lcom/sec/android/gallery3d/remote/picasa/Entry;

    .prologue
    const/4 v10, 0x1

    .line 538
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;

    .line 539
    .local v0, "album":Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;
    iget-wide v2, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->id:J

    .line 540
    .local v2, "albumId":J
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iput-wide v2, v5, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->id:J

    .line 541
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$key:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    invoke-static {v5, v6}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 542
    .local v1, "index":I
    if-ltz v1, :cond_4

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$local:[Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;

    aget-object v4, v5, v1

    .line 543
    .local v4, "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    :goto_0
    if-eqz v4, :cond_0

    iget-wide v6, v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->dateEdited:J

    iget-wide v8, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->dateEdited:J

    cmp-long v5, v6, v8

    if-gez v5, :cond_2

    .line 546
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$user:Lcom/sec/android/gallery3d/remote/picasa/UserEntry;

    iget-object v5, v5, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->account:Ljava/lang/String;

    iput-object v5, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->syncAccount:Ljava/lang/String;

    .line 547
    iput-boolean v10, v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->photosDirty:Z

    .line 548
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$albumSchema:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5, v6, v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/sec/android/gallery3d/remote/picasa/Entry;)J

    .line 549
    if-nez v4, :cond_1

    .line 550
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$context:Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;

    iget-object v5, v5, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$SyncContext;->albumsAdded:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$1;->val$syncResult:Landroid/content/SyncResult;

    iget-object v5, v5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v5, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v5, Landroid/content/SyncStats;->numUpdates:J

    .line 561
    :cond_2
    if-eqz v4, :cond_3

    .line 562
    iput-boolean v10, v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;->survived:Z

    .line 564
    :cond_3
    return-void

    .line 542
    .end local v4    # "metadata":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$EntryMetadata;
    :cond_4
    const/4 v4, 0x0

    goto :goto_0
.end method
