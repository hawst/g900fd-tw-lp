.class public Lcom/sec/android/gallery3d/util/KeyBoardManager;
.super Ljava/lang/Object;
.source "KeyBoardManager.java"


# static fields
.field public static final ENTER_SELECTION_MODE:I = 0x1

.field public static final LEAVE_SELECTION_MODE:I = 0x2

.field public static final MAKE_SLIDE_WIDGET:I = 0x4

.field public static final SELECT_ALL_MODE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "KeyBoardManager"


# instance fields
.field private mAlbumPageIndex:I

.field private mAlbumSetIndex:I

.field private mContext:Landroid/content/Context;

.field private mFocuspath:Lcom/sec/android/gallery3d/data/Path;

.field private mInSelectionMode:Z

.field private mIsAlbumSet:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryContext;Z)V
    .locals 1
    .param p1, "galleryContext"    # Lcom/sec/android/gallery3d/app/GalleryContext;
    .param p2, "isAlbumSet"    # Z

    .prologue
    const/4 v0, -0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mAlbumSetIndex:I

    .line 38
    iput v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mAlbumPageIndex:I

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mFocuspath:Lcom/sec/android/gallery3d/data/Path;

    .line 42
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryContext;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mContext:Landroid/content/Context;

    .line 43
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mIsAlbumSet:Z

    .line 44
    return-void
.end method


# virtual methods
.method public getFocusIndex()I
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mIsAlbumSet:Z

    if-eqz v0, :cond_0

    .line 76
    iget v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mAlbumSetIndex:I

    .line 78
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mAlbumPageIndex:I

    goto :goto_0
.end method

.method public getFocusPath()Lcom/sec/android/gallery3d/data/Path;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mFocuspath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method public inFocusMode()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mInSelectionMode:Z

    return v0
.end method

.method public isInTouchMode()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isInTouchMode()Z

    move-result v0

    return v0
.end method

.method public isItemSelected(Lcom/sec/android/gallery3d/data/Path;)Z
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v0, 0x0

    .line 83
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mFocuspath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mFocuspath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDown()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public setFocusIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mInSelectionMode:Z

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 67
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mIsAlbumSet:Z

    if-eqz v0, :cond_1

    .line 68
    iput p1, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mAlbumSetIndex:I

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mAlbumPageIndex:I

    goto :goto_0
.end method

.method public setFocusMode(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mInSelectionMode:Z

    .line 52
    return-void
.end method

.method public setFocusPath(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/KeyBoardManager;->mFocuspath:Lcom/sec/android/gallery3d/data/Path;

    .line 92
    return-void
.end method
