.class public Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "CloudMergeSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field public static CLOUD_MERGESET_BUCKETID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CloudMergeSet"


# instance fields
.field private final mName:Ljava/lang/String;

.field private mRootSet:Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "/cloud/mergedall"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->CLOUD_MERGESET_BUCKETID:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 45
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->getBucketId()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    .line 46
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 47
    const-string v0, "DropBox"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mName:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public static find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    return-object v0
.end method


# virtual methods
.method public getBucketId()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    .line 103
    const/4 v0, -0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 61
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->getSubMediaSetCount(Z)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getSupportedOperations()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalMediaItems(I)[Landroid/database/Cursor;
    .locals 8
    .param p1, "filterType"    # I

    .prologue
    const/4 v0, 0x2

    .line 113
    new-array v6, v0, [Landroid/database/Cursor;

    .line 115
    .local v6, "cursors":[Landroid/database/Cursor;
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 116
    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "latitude"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "date_modified"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "date_modified DESC"

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v6, v7

    .line 125
    :cond_0
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_1

    .line 126
    const/4 v7, 0x1

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Videos;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "latitude"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "date_modified"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "date_modified DESC"

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v6, v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_1
    :goto_0
    return-object v6

    .line 134
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->notifyContentChanged()V

    .line 110
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mRootSet:Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;->reload()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mDataVersion:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 77
    invoke-static {}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mDataVersion:J

    .line 79
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeSet;->mDataVersion:J

    return-wide v0
.end method
