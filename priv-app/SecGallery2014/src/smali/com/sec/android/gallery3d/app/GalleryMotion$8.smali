.class Lcom/sec/android/gallery3d/app/GalleryMotion$8;
.super Ljava/lang/Object;
.source "GalleryMotion.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/app/GalleryMotion;->motionDialogInitialOff(Lcom/sec/android/gallery3d/util/MotionDetector;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryMotion;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;

.field final synthetic val$motionDefaultHeight:I

.field final synthetic val$motionDefaultWidth:I

.field final synthetic val$motionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryMotion;Landroid/widget/CheckBox;Lcom/sec/android/gallery3d/util/MotionDetector;II)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->this$0:Lcom/sec/android/gallery3d/app/GalleryMotion;

    iput-object p2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$checkBox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    iput p4, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDefaultWidth:I

    iput p5, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDefaultHeight:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v1, 0x1

    .line 398
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->this$0:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionDialogOff(Z)V

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->this$0:Lcom/sec/android/gallery3d/app/GalleryMotion;

    # invokes: Lcom/sec/android/gallery3d/app/GalleryMotion;->stopAnimation()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/GalleryMotion;->access$100(Lcom/sec/android/gallery3d/app/GalleryMotion;)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->this$0:Lcom/sec/android/gallery3d/app/GalleryMotion;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionEngineEnable(Z)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    sget-object v1, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_NONE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/MotionDetector;->setState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;

    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDefaultWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/gallery3d/app/GalleryMotion$8;->val$motionDefaultHeight:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/MotionDetector;->setDefaultMotionCenterPosition(FF)V

    .line 407
    :cond_1
    return-void
.end method
