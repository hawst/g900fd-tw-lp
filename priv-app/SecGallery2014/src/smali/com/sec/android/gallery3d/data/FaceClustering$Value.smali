.class public Lcom/sec/android/gallery3d/data/FaceClustering$Value;
.super Ljava/lang/Object;
.source "FaceClustering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/FaceClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Value"
.end annotation


# instance fields
.field mConfirmedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field public mGroupId:I

.field mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field mName:Ljava/lang/String;

.field public mPersonId:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/FaceClustering;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/FaceClustering;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "personId"    # I
    .param p3, "groupId"    # I
    .param p4, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 646
    .local p5, "confirmedlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local p6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->this$0:Lcom/sec/android/gallery3d/data/FaceClustering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 647
    iput p2, p0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mPersonId:I

    .line 648
    iput p3, p0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mGroupId:I

    .line 649
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mName:Ljava/lang/String;

    .line 650
    iput-object p5, p0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mConfirmedList:Ljava/util/ArrayList;

    .line 651
    iput-object p6, p0, Lcom/sec/android/gallery3d/data/FaceClustering$Value;->mList:Ljava/util/ArrayList;

    .line 652
    return-void
.end method
