.class public abstract Lcom/sec/android/gallery3d/homesync/HomeSyncItem;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "HomeSyncItem.java"


# static fields
.field private static final INDEX_CONTENT_INDEX:I = 0xd

.field public static final INDEX_DATA_URL:I = 0x1

.field private static final INDEX_DATETAKEN:I = 0xa

.field private static final INDEX_DATE_ADDED:I = 0x4

.field private static final INDEX_DATE_MODIFIED:I = 0x5

.field private static final INDEX_DISPLAY_NAME:I = 0x3

.field private static final INDEX_HEIGHT:I = 0xc

.field public static final INDEX_ID:I = 0x0

.field private static final INDEX_MIME_TYPE:I = 0x7

.field private static final INDEX_NEARBY_SEED:I = 0xe

.field private static final INDEX_ORIENTATION:I = 0x9

.field private static final INDEX_SIZE:I = 0x6

.field private static final INDEX_THUMBNAIL:I = 0x8

.field private static final INDEX_TITLE:I = 0x2

.field private static final INDEX_WIDTH:I = 0xb

.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field protected mContentIndex:I

.field protected mDataUri:Ljava/lang/String;

.field private mDateAdded:Ljava/lang/String;

.field protected mDateModified:Ljava/lang/String;

.field protected mDateTaken:Ljava/lang/String;

.field public mDisplayName:Ljava/lang/String;

.field protected mHeight:I

.field protected mId:I

.field protected mMimeType:Ljava/lang/String;

.field protected mNearbySeed:Ljava/lang/String;

.field protected mOrientation:I

.field protected mSize:J

.field protected mThumbnailUri:Ljava/lang/String;

.field protected mTitle:Ljava/lang/String;

.field protected mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->TAG:Ljava/lang/String;

    .line 40
    const-string v0, "/homesync/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 116
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 98
    iput v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mWidth:I

    .line 100
    iput v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mHeight:I

    .line 118
    iput-object p2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 120
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->loadFromCursor(Landroid/database/Cursor;)V

    .line 122
    return-void
.end method

.method private loadFromCursor(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 130
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mId:I

    .line 134
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "utf-8"

    invoke-static {v2, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataUri:Ljava/lang/String;

    .line 136
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataUri:Ljava/lang/String;

    const-string v3, "file://"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataUri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mTitle:Ljava/lang/String;

    .line 146
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDisplayName:Ljava/lang/String;

    .line 150
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "dbValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 154
    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateAdded:Ljava/lang/String;

    .line 156
    :cond_0
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 160
    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateModified:Ljava/lang/String;

    .line 162
    :cond_1
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mSize:J

    .line 164
    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mMimeType:Ljava/lang/String;

    .line 166
    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mThumbnailUri:Ljava/lang/String;

    .line 168
    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 172
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mOrientation:I

    .line 174
    :cond_2
    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 178
    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateTaken:Ljava/lang/String;

    .line 182
    :cond_3
    const/16 v2, 0xb

    :try_start_1
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 186
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mWidth:I

    .line 188
    :cond_4
    const/16 v2, 0xc

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 192
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mHeight:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 202
    :cond_5
    :goto_1
    const/16 v2, 0xe

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mNearbySeed:Ljava/lang/String;

    .line 204
    return-void

    .line 138
    .end local v0    # "dbValue":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 140
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v2, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported utf-8 encoding exception for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 194
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v0    # "dbValue":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 196
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size wrong : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iput v5, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mHeight:I

    iput v5, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mWidth:I

    goto :goto_1
.end method


# virtual methods
.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected getDateModifiedLong()J
    .locals 3

    .prologue
    .line 410
    const-wide/16 v0, 0x0

    .line 414
    .local v0, "dateModified":J
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateModified:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 420
    :goto_0
    return-wide v0

    .line 416
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getNearbySeed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mNearbySeed:Ljava/lang/String;

    return-object v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mOrientation:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public updateContent(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 398
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->updateFromCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataVersion:J

    .line 404
    :cond_0
    return-void
.end method

.method public updateFromCursor(Landroid/database/Cursor;)Z
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 308
    new-instance v2, Lcom/sec/android/gallery3d/util/UpdateHelper;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;-><init>()V

    .line 310
    .local v2, "uh":Lcom/sec/android/gallery3d/util/UpdateHelper;
    iget v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mId:I

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mId:I

    .line 314
    const/4 v3, 0x1

    :try_start_0
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "utf-8"

    invoke-static {v3, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataUri:Ljava/lang/String;

    .line 316
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataUri:Ljava/lang/String;

    const-string v4, "file://"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDataUri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mTitle:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mTitle:Ljava/lang/String;

    .line 326
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDisplayName:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDisplayName:Ljava/lang/String;

    .line 330
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 332
    .local v0, "dbValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 334
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateAdded:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateAdded:Ljava/lang/String;

    .line 336
    :cond_0
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 340
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateModified:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateModified:Ljava/lang/String;

    .line 342
    :cond_1
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 344
    if-eqz v0, :cond_2

    .line 346
    iget-wide v4, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mSize:J

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-long v6, v3

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mSize:J

    .line 348
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mMimeType:Ljava/lang/String;

    const/4 v4, 0x7

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mMimeType:Ljava/lang/String;

    .line 350
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mThumbnailUri:Ljava/lang/String;

    const/16 v4, 0x8

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mThumbnailUri:Ljava/lang/String;

    .line 352
    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 354
    if-eqz v0, :cond_3

    .line 356
    iget v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mOrientation:I

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mOrientation:I

    .line 358
    :cond_3
    const/16 v3, 0xa

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 362
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateTaken:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mDateTaken:Ljava/lang/String;

    .line 366
    :cond_4
    const/16 v3, 0xb

    :try_start_1
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 368
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 370
    iget v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mWidth:I

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mWidth:I

    .line 372
    :cond_5
    const/16 v3, 0xc

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 374
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 376
    iget v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mHeight:I

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mHeight:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 388
    :cond_6
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mNearbySeed:Ljava/lang/String;

    const/16 v4, 0xe

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mNearbySeed:Ljava/lang/String;

    .line 390
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v3

    return v3

    .line 318
    .end local v0    # "dbValue":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 320
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v3, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported utf-8 encoding exception for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 378
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v0    # "dbValue":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 380
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "size wrong : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    iput v8, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mHeight:I

    iput v8, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->mWidth:I

    goto :goto_1
.end method
