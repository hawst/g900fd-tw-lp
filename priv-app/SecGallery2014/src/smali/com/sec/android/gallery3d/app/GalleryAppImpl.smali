.class public Lcom/sec/android/gallery3d/app/GalleryAppImpl;
.super Landroid/app/Application;
.source "GalleryAppImpl.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/GalleryApp;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/GalleryAppImpl$GalleryUncaughtExceptionHandler;,
        Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;
    }
.end annotation


# static fields
.field private static final DOWNLOAD_CAPACITY:J

.field private static final DOWNLOAD_FOLDER:Ljava/lang/String; = "download"

.field private static KEY_CLUSTER_TYPE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static alreadtInitFaceTag:Z

.field private static context:Landroid/content/Context;


# instance fields
.field private bNeedSync:Z

.field private bNowSync:Z

.field private volatile isTaskComplete:Z

.field private mActivityLifecycleCallback:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private mActivityStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;",
            ">;"
        }
    .end annotation
.end field

.field public mAppCreateTime:J

.field private mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

.field private mClusterType:I

.field private mContactContentObserver:Landroid/database/ContentObserver;

.field private mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

.field private mContactsChangeListener:Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field private mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;

.field private mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

.field mHandlerSyncContact:Landroid/os/Handler;

.field private mImageCacheService:Lcom/sec/android/gallery3d/data/ImageCacheService;

.field private mImplFestivalDataCount:I

.field private mImplFestivalEndTime:J

.field private mImplFestivalStartTime:J

.field private mIntent:Landroid/content/Intent;

.field private mIsChanged:Z

.field private mIsContactsChanged:Z

.field private mIsFestivalMode:Z

.field private mIsPhotoPage:Z

.field private mListener:Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;

.field private mListeners:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mLock:Ljava/lang/Object;

.field private mNeedCleanCache:Z

.field private mNeedDocumentUpdate:Z

.field private mNeedRefresh:Z

.field private mNewAlbumSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mOperation:I

.field private mPersonGroups:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[",
            "Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSaveAlbumPath:Ljava/lang/String;

.field private mSlideShowMode:Z

.field private mSlideshowSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSortByFilterType:I

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field public mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const-class v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->TAG:Ljava/lang/String;

    .line 71
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x20000000

    :goto_0
    sput-wide v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->DOWNLOAD_CAPACITY:J

    .line 87
    const-string v0, "cluster-type"

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->KEY_CLUSTER_TYPE:Ljava/lang/String;

    .line 117
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->alreadtInitFaceTag:Z

    return-void

    .line 71
    :cond_0
    const-wide/32 v0, 0x4000000

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mLock:Ljava/lang/Object;

    .line 83
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedCleanCache:Z

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedRefresh:Z

    .line 85
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isTaskComplete:Z

    .line 86
    iput v3, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    .line 88
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mPersonGroups:Landroid/util/SparseArray;

    .line 89
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    .line 90
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactContentObserver:Landroid/database/ContentObserver;

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsContactsChanged:Z

    .line 92
    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mOperation:I

    .line 94
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListeners:Ljava/util/WeakHashMap;

    .line 96
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mActivityStack:Ljava/util/Stack;

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsChanged:Z

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedDocumentUpdate:Z

    .line 104
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsPhotoPage:Z

    .line 108
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsFestivalMode:Z

    .line 114
    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSortByFilterType:I

    .line 115
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    .line 123
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactsChangeListener:Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;

    .line 133
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mActivityLifecycleCallback:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 320
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNeedSync:Z

    .line 321
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNowSync:Z

    .line 455
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSaveAlbumPath:Ljava/lang/String;

    .line 668
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->syncContactsOnThread()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNeedSync:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNeedSync:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsContactsChanged:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactsChangeListener:Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Ljava/util/WeakHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListeners:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsChanged:Z

    return p1
.end method

.method static synthetic access$702(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNowSync:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method

.method public static getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 256
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->context:Landroid/content/Context;

    return-object v0
.end method

.method private initLocalDataBase()V
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 203
    return-void
.end method

.method private initUncaughtExceptionHandler()V
    .locals 2

    .prologue
    .line 662
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 664
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$GalleryUncaughtExceptionHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl$GalleryUncaughtExceptionHandler;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 666
    :cond_0
    return-void
.end method

.method private initializeAsyncTask()V
    .locals 1

    .prologue
    .line 314
    :try_start_0
    const-class v0, Landroid/os/AsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :goto_0
    return-void

    .line 315
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private syncContactsOnThread()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 323
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNowSync:Z

    if-eqz v0, :cond_0

    .line 324
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNeedSync:Z

    .line 348
    :goto_0
    return-void

    .line 327
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->bNowSync:Z

    .line 328
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl$3;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)V

    const-string v2, "SyncContactThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method


# virtual methods
.method public checkFaceInCurrentGroup(ILjava/lang/String;)Z
    .locals 8
    .param p1, "personId"    # I
    .param p2, "curMediaSetKey"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 375
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mPersonGroups:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 394
    :cond_0
    :goto_0
    return v1

    .line 378
    :cond_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mPersonGroups:Landroid/util/SparseArray;

    invoke-virtual {v7, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;

    .line 380
    .local v2, "groups":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    if-eqz v2, :cond_0

    .line 383
    const/4 v1, 0x0

    .line 384
    .local v1, "b":Z
    move-object v0, v2

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v6, :cond_0

    aget-object v4, v0, v3

    .line 386
    .local v4, "item":Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;
    iget v7, v4, Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;->mId:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 387
    .local v5, "key":Ljava/lang/String;
    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 389
    const/4 v1, 0x1

    .line 390
    goto :goto_0

    .line 384
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public checkInGroupCluster()Z
    .locals 2

    .prologue
    .line 370
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 437
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 439
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactContentObserver:Landroid/database/ContentObserver;

    .line 441
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->stopNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 442
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 443
    return-void
.end method

.method public getAlbumPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSaveAlbumPath:Ljava/lang/String;

    return-object v0
.end method

.method public getAndroidContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 252
    return-object p0
.end method

.method public getAppIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getCacheInterface()Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    return-object v0
.end method

.method public getContactProvider()Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    return-object v0
.end method

.method public getCurrentClusterType()I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    return v0
.end method

.method public declared-synchronized getDataManager()Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    if-nez v0, :cond_0

    .line 263
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->init(Landroid/content/Context;)V

    .line 265
    new-instance v0, Lcom/sec/android/gallery3d/data/DataManager;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/DataManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 266
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->initializeSourceMap()V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    return-object v0
.end method

.method public getDocumentUpdateStatus()Z
    .locals 1

    .prologue
    .line 546
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedDocumentUpdate:Z

    return v0
.end method

.method public declared-synchronized getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;
    .locals 4

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;

    if-nez v1, :cond_2

    .line 296
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExternalCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "download"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 299
    .local v0, "cacheDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 301
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 302
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to create: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    .end local v0    # "cacheDir":Ljava/io/File;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 305
    .restart local v0    # "cacheDir":Ljava/io/File;
    :cond_1
    :try_start_1
    new-instance v1, Lcom/sec/android/gallery3d/data/DownloadCache;

    sget-wide v2, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->DOWNLOAD_CAPACITY:J

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/sec/android/gallery3d/data/DownloadCache;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/io/File;J)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;

    .line 307
    .end local v0    # "cacheDir":Ljava/io/File;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDownloadCache:Lcom/sec/android/gallery3d/data/DownloadCache;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    .locals 2

    .prologue
    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    if-nez v0, :cond_0

    .line 352
    new-instance v0, Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFestivalCount()I
    .locals 1

    .prologue
    .line 575
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImplFestivalDataCount:I

    return v0
.end method

.method public getFestivalEndTime()J
    .locals 2

    .prologue
    .line 583
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImplFestivalEndTime:J

    return-wide v0
.end method

.method public getFestivalStartTime()J
    .locals 2

    .prologue
    .line 579
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImplFestivalStartTime:J

    return-wide v0
.end method

.method public getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;
    .locals 3

    .prologue
    .line 275
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 276
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImageCacheService:Lcom/sec/android/gallery3d/data/ImageCacheService;

    if-nez v0, :cond_0

    .line 277
    new-instance v0, Lcom/sec/android/gallery3d/data/ImageCacheService;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/data/ImageCacheService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImageCacheService:Lcom/sec/android/gallery3d/data/ImageCacheService;

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImageCacheService:Lcom/sec/android/gallery3d/data/ImageCacheService;

    monitor-exit v1

    return-object v0

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getRefreshOperation()I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mOperation:I

    return v0
.end method

.method public getRefreshState()Z
    .locals 1

    .prologue
    .line 414
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedRefresh:Z

    return v0
.end method

.method public getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 404
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSlideshowSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v0, :cond_0

    .line 616
    new-instance v0, Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSlideshowSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSlideshowSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method public getSortByType()I
    .locals 1

    .prologue
    .line 594
    iget v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSortByFilterType:I

    return v0
.end method

.method public declared-synchronized getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;
    .locals 1

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/sec/android/gallery3d/util/ThreadPool;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/ThreadPool;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTopActivity()Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mActivityStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    const/4 v0, 0x0

    .line 496
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mActivityStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    goto :goto_0
.end method

.method public initializeFaceTag()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 206
    sget-boolean v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->alreadtInitFaceTag:Z

    if-ne v1, v4, :cond_0

    .line 207
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->TAG:Ljava/lang/String;

    const-string v2, "already initialized faceTag. Skip init."

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :goto_0
    return-void

    .line 210
    :cond_0
    sput-boolean v4, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->alreadtInitFaceTag:Z

    .line 212
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "sync contacts"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 213
    .local v0, "threadSyncContact":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 215
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mHandlerSyncContact:Landroid/os/Handler;

    .line 216
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mHandlerSyncContact:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 225
    new-instance v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl$2;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mHandlerSyncContact:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl$2;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactContentObserver:Landroid/database/ContentObserver;

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public isArcMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 501
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-nez v1, :cond_1

    .line 504
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x20

    iget v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    if-eq v1, v2, :cond_2

    const/16 v1, 0x80

    iget v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    if-eq v1, v2, :cond_2

    const/16 v1, 0x4000

    iget v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    if-eq v1, v2, :cond_2

    const v1, 0x8000

    iget v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    if-eq v1, v2, :cond_2

    const/high16 v1, 0x10000

    iget v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isContactsChanged()Z
    .locals 1

    .prologue
    .line 432
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsContactsChanged:Z

    return v0
.end method

.method public isDocMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 538
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDocumentClassifier:Z

    if-nez v1, :cond_1

    .line 541
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x400

    iget v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isFestivalMode()Z
    .locals 2

    .prologue
    .line 558
    const/4 v0, 0x0

    .line 559
    .local v0, "result":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFestival:Z

    if-eqz v1, :cond_0

    .line 560
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsFestivalMode:Z

    .line 562
    :cond_0
    return v0
.end method

.method public isPhotoPage()Z
    .locals 1

    .prologue
    .line 513
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-nez v0, :cond_0

    .line 514
    const/4 v0, 0x0

    .line 516
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsPhotoPage:Z

    goto :goto_0
.end method

.method public isSlideShowMode()Z
    .locals 1

    .prologue
    .line 639
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSlideShowMode:Z

    return v0
.end method

.method public isTaskComplete()Z
    .locals 1

    .prologue
    .line 526
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isTaskComplete:Z

    return v0
.end method

.method public needCleanCache()Z
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedCleanCache:Z

    return v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 161
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mAppCreateTime:J

    .line 162
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initUncaughtExceptionHandler()V

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->context:Landroid/content/Context;

    .line 166
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 167
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initializeAsyncTask()V

    .line 168
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->initialize(Landroid/content/Context;)V

    .line 169
    invoke-static {p0}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->initialize(Landroid/content/Context;)V

    .line 171
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/UsageStatistics;->initialize(Landroid/content/Context;)V

    .line 174
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->init(Landroid/content/Context;)V

    .line 177
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_1

    .line 178
    new-instance v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    .line 179
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    if-ne v0, v2, :cond_3

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "TagBuddyPermissionAlertDialogOff"

    invoke-static {v0, v1, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "FaceTagPermissionAlertDialogOff"

    invoke-static {v0, v1, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 183
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initializeFaceTag()V

    .line 190
    :cond_1
    :goto_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVideoWall:Z

    if-eqz v0, :cond_2

    .line 191
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    .line 193
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->init()V

    .line 194
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initLocalDataBase()V

    .line 195
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mActivityLifecycleCallback:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 197
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/HdmiUtils;->initHdmiConnected(Landroid/content/Context;)V

    .line 199
    return-void

    .line 186
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initializeFaceTag()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 469
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->KEY_CLUSTER_TYPE:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 470
    return-void
.end method

.method public registerContactObserver(Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;)V
    .locals 2
    .param p1, "observer"    # Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;

    .prologue
    .line 478
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsChanged:Z

    if-eqz v0, :cond_0

    .line 479
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;->onChange(Z)V

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListeners:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 486
    :goto_0
    return-void

    .line 485
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListeners:Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public restoreFromState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 473
    sget-object v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->KEY_CLUSTER_TYPE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    .line 474
    return-void
.end method

.method public sendGalleryEvent(Lcom/sec/android/gallery3d/app/GalleryEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/sec/android/gallery3d/app/GalleryEvent;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListener:Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListener:Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;->onGalleryEventReceived(Lcom/sec/android/gallery3d/app/GalleryEvent;)V

    .line 155
    :cond_0
    return-void
.end method

.method public setAlbumPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 460
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSaveAlbumPath:Ljava/lang/String;

    .line 461
    return-void
.end method

.method public setAppIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 629
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIntent:Landroid/content/Intent;

    .line 630
    return-void
.end method

.method public setCacheInterface(Lcom/sec/samsung/gallery/decoder/CacheInterface;)V
    .locals 0
    .param p1, "cacheInterface"    # Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .prologue
    .line 652
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 653
    return-void
.end method

.method public setCleanCacheStatus(Z)V
    .locals 0
    .param p1, "isNeeded"    # Z

    .prologue
    .line 419
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedCleanCache:Z

    .line 420
    return-void
.end method

.method public setContactsChangeListener(Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactsChangeListener:Lcom/sec/android/gallery3d/app/GalleryAppImpl$IContactsChangeListener;

    .line 127
    return-void
.end method

.method public setCurrentClusterType(I)V
    .locals 0
    .param p1, "clusterType"    # I

    .prologue
    .line 360
    iput p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mClusterType:I

    .line 361
    return-void
.end method

.method public setDecoderInterface(Lcom/sec/samsung/gallery/decoder/DecoderInterface;)V
    .locals 0
    .param p1, "decoderInterface"    # Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .prologue
    .line 643
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 644
    return-void
.end method

.method public setDocumentUpdateStatus(Z)V
    .locals 0
    .param p1, "isNeeded"    # Z

    .prologue
    .line 551
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedDocumentUpdate:Z

    .line 552
    return-void
.end method

.method public setGalleryEventListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListener:Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;

    .line 149
    return-void
.end method

.method public setImplFestivalData(ZIJJ)V
    .locals 1
    .param p1, "mode"    # Z
    .param p2, "count"    # I
    .param p3, "start"    # J
    .param p5, "end"    # J

    .prologue
    .line 566
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsFestivalMode:Z

    .line 567
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 568
    iput p2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImplFestivalDataCount:I

    .line 569
    iput-wide p3, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImplFestivalStartTime:J

    .line 570
    iput-wide p5, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mImplFestivalEndTime:J

    .line 572
    :cond_0
    return-void
.end method

.method public setIsPhotoPage(Z)V
    .locals 0
    .param p1, "isPhotoPage"    # Z

    .prologue
    .line 521
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mIsPhotoPage:Z

    .line 522
    return-void
.end method

.method public setPersonGroupsToGlobal(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<[",
            "Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 399
    .local p1, "groups":Landroid/util/SparseArray;, "Landroid/util/SparseArray<[Lcom/sec/android/gallery3d/data/GroupClustering$GroupItem;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mPersonGroups:Landroid/util/SparseArray;

    .line 400
    return-void
.end method

.method public setRefreshOperation(I)V
    .locals 0
    .param p1, "operation"    # I

    .prologue
    .line 447
    iput p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mOperation:I

    .line 448
    return-void
.end method

.method public setRefreshState(Z)V
    .locals 0
    .param p1, "needRefresh"    # Z

    .prologue
    .line 409
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mNeedRefresh:Z

    .line 410
    return-void
.end method

.method public setSlideShowMode(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 635
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSlideShowMode:Z

    .line 636
    return-void
.end method

.method public setSortByType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 589
    iput p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mSortByFilterType:I

    .line 590
    return-void
.end method

.method public setTaskStatus(Z)V
    .locals 0
    .param p1, "isFinished"    # Z

    .prologue
    .line 531
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isTaskComplete:Z

    .line 532
    return-void
.end method

.method public unregisterContactObserver(Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mListeners:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    return-void
.end method
