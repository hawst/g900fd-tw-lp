.class public Lcom/sec/android/gallery3d/data/TimeAlbumClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "TimeAlbumClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/TimeAlbumClustering$1;,
        Lcom/sec/android/gallery3d/data/TimeAlbumClustering$DateComparator;
    }
.end annotation


# static fields
.field public static final DAY_MODE:I = 0x1

.field private static final DAY_UINT:J = 0x5265c00L

.field public static final MONTH_MODE:I = 0x3

.field private static final MONTH_UNIT:J = 0x3L

.field private static final TAG:Ljava/lang/String;

.field public static final WEEK_MODE:I = 0x2

.field private static final WEEK_UINT:J = 0x7L

.field public static final YEAR_MODE:I = 0x4


# instance fields
.field private currentDate:Ljava/util/Calendar;

.field private isToday:Z

.field private mClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/TimeCluster;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

.field private mCurrentTime:J

.field private mNames:[Ljava/lang/String;

.field private mReverseOrder:Z

.field private previousDay:I

.field private previousDayValuse:I

.field private previousMonth:I

.field private previousWeek:I

.field private previousWeekValuse:I

.field private previousYear:I

.field private final sDateComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation
.end field

.field private todayDay:I

.field private todayMonth:I

.field private todayWeek:I

.field private todayYear:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sortByType"    # I

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 70
    new-instance v0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering$DateComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering$DateComparator;-><init>(Lcom/sec/android/gallery3d/data/TimeAlbumClustering$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->sDateComparator:Ljava/util/Comparator;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    .line 83
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 86
    iput p2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mSortByType:I

    .line 87
    return-void
.end method

.method private computeForAlbumTime(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V
    .locals 12
    .param p1, "currentItem"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 335
    if-eqz p1, :cond_1b

    .line 336
    iget-wide v6, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 337
    .local v6, "time":J
    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    cmp-long v8, v8, v6

    if-gez v8, :cond_0

    .line 338
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    .line 340
    :cond_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v8, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 341
    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    sub-long/2addr v8, v6

    const-wide/32 v10, 0x5265c00

    div-long/2addr v8, v10

    long-to-int v4, v8

    .line 342
    .local v4, "dayDiff":I
    int-to-long v8, v4

    const-wide/16 v10, 0x7

    div-long/2addr v8, v10

    long-to-int v5, v8

    .line 344
    .local v5, "weekDiff":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v9, 0x6

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 345
    .local v0, "currentDay":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 346
    .local v2, "currentWeek":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 347
    .local v1, "currentMonth":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 348
    .local v3, "currentYear":I
    const/4 v8, 0x1

    if-ge v5, v8, :cond_6

    .line 349
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDayValuse:I

    if-ne v8, v4, :cond_2

    .line 350
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 501
    .end local v0    # "currentDay":I
    .end local v1    # "currentMonth":I
    .end local v2    # "currentWeek":I
    .end local v3    # "currentYear":I
    .end local v4    # "dayDiff":I
    .end local v5    # "weekDiff":I
    .end local v6    # "time":J
    :cond_1
    :goto_0
    return-void

    .line 352
    .restart local v0    # "currentDay":I
    .restart local v1    # "currentMonth":I
    .restart local v2    # "currentWeek":I
    .restart local v3    # "currentYear":I
    .restart local v4    # "dayDiff":I
    .restart local v5    # "weekDiff":I
    .restart local v6    # "time":J
    :cond_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 353
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v8, v9, :cond_5

    .line 356
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 357
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v8, :cond_3

    .line 358
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v10, 0x7f0e023a

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 364
    :cond_3
    :goto_1
    new-instance v8, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 366
    :cond_4
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    .line 367
    iput v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDayValuse:I

    .line 368
    iput v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    .line 369
    iput v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    .line 370
    iput v3, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    .line 371
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 362
    :cond_5
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_1

    .line 373
    :cond_6
    const/4 v8, 0x4

    if-ge v5, v8, :cond_c

    .line 374
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    if-ne v8, v5, :cond_7

    .line 375
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 377
    :cond_7
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_9

    .line 378
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    if-nez v8, :cond_b

    .line 380
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v8, v9, :cond_a

    .line 382
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 383
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v8, :cond_8

    .line 384
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v10, 0x7f0e023a

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 395
    :cond_8
    :goto_2
    new-instance v8, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 397
    :cond_9
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    .line 398
    iput v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    .line 399
    iput v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    .line 400
    iput v3, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    .line 401
    iput v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    .line 402
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_0

    .line 388
    :cond_a
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_2

    .line 392
    :cond_b
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_2

    .line 404
    :cond_c
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-ne v8, v3, :cond_13

    .line 405
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    if-ne v1, v8, :cond_d

    .line 406
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_0

    .line 408
    :cond_d
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_f

    .line 409
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    if-ne v8, v9, :cond_12

    .line 412
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    if-ne v8, v9, :cond_11

    .line 413
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v8, v9, :cond_10

    .line 415
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 416
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v8, :cond_e

    .line 417
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v10, 0x7f0e023a

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 432
    :cond_e
    :goto_3
    new-instance v8, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 434
    :cond_f
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    .line 435
    iput v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    .line 436
    iput v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    .line 437
    iput v3, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    .line 438
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_0

    .line 421
    :cond_10
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_3

    .line 425
    :cond_11
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_3

    .line 429
    :cond_12
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x3

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_3

    .line 441
    :cond_13
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    if-ne v3, v8, :cond_14

    .line 442
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_0

    .line 444
    :cond_14
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_16

    .line 445
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-ne v8, v9, :cond_1a

    .line 447
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    if-ne v8, v9, :cond_19

    .line 448
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    if-ne v8, v9, :cond_18

    .line 449
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v8, v9, :cond_17

    .line 451
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 452
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v8, :cond_15

    .line 453
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v10, 0x7f0e023a

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 472
    :cond_15
    :goto_4
    new-instance v8, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 474
    :cond_16
    iput v3, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    .line 475
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_0

    .line 457
    :cond_17
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_4

    .line 461
    :cond_18
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_4

    .line 465
    :cond_19
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x3

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_4

    .line 469
    :cond_1a
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x4

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_4

    .line 480
    .end local v0    # "currentDay":I
    .end local v1    # "currentMonth":I
    .end local v2    # "currentWeek":I
    .end local v3    # "currentYear":I
    .end local v4    # "dayDiff":I
    .end local v5    # "weekDiff":I
    .end local v6    # "time":J
    :cond_1b
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 481
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    sget-object v8, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->TAG:Ljava/lang/String;

    const-string v9, "================add NULL========================"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-eq v8, v9, :cond_1d

    .line 484
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x4

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 485
    new-instance v8, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 498
    :cond_1c
    :goto_5
    new-instance v8, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    goto/16 :goto_0

    .line 486
    :cond_1d
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    if-eq v8, v9, :cond_1e

    .line 487
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x3

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_5

    .line 488
    :cond_1e
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    if-eq v8, v9, :cond_1f

    .line 489
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_5

    .line 490
    :cond_1f
    iget v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-eq v8, v9, :cond_20

    .line 491
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_5

    .line 493
    :cond_20
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 494
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v8, :cond_1c

    .line 495
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v10, 0x7f0e023a

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    goto :goto_5
.end method

.method private computeForAlbumTimeWithTimeTable(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v6, v10, :cond_23

    .line 132
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .line 133
    .local v1, "currentItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    if-eqz v1, :cond_1

    .line 134
    iget-wide v8, v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 135
    .local v8, "time":J
    iget-wide v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    cmp-long v10, v10, v8

    if-gez v10, :cond_0

    .line 136
    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    .line 138
    :cond_0
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v10, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 139
    iget-wide v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    sub-long/2addr v10, v8

    const-wide/32 v12, 0x5265c00

    div-long/2addr v10, v12

    long-to-int v5, v10

    .line 140
    .local v5, "dayDiff":I
    int-to-long v10, v5

    const-wide/16 v12, 0x7

    div-long/2addr v10, v12

    long-to-int v7, v10

    .line 142
    .local v7, "weekDiff":I
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v11, 0x6

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 143
    .local v0, "currentDay":I
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v11, 0x3

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 144
    .local v3, "currentWeek":I
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 145
    .local v2, "currentMonth":I
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 146
    .local v4, "currentYear":I
    const/4 v10, 0x1

    if-ge v7, v10, :cond_8

    .line 147
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDayValuse:I

    if-ne v10, v5, :cond_2

    .line 148
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 131
    .end local v0    # "currentDay":I
    .end local v2    # "currentMonth":I
    .end local v3    # "currentWeek":I
    .end local v4    # "currentYear":I
    .end local v5    # "dayDiff":I
    .end local v7    # "weekDiff":I
    .end local v8    # "time":J
    :cond_1
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 150
    .restart local v0    # "currentDay":I
    .restart local v2    # "currentMonth":I
    .restart local v3    # "currentWeek":I
    .restart local v4    # "currentYear":I
    .restart local v5    # "dayDiff":I
    .restart local v7    # "weekDiff":I
    .restart local v8    # "time":J
    :cond_2
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    .line 151
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    if-eqz v10, :cond_5

    .line 152
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 155
    :goto_2
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v10, v11, :cond_6

    .line 157
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 158
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_3

    .line 159
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023a

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 168
    :cond_3
    :goto_3
    new-instance v10, Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-boolean v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    invoke-direct {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>(Z)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 170
    :cond_4
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    .line 171
    iput v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDayValuse:I

    .line 172
    iput v3, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    .line 173
    iput v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    .line 174
    iput v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    .line 175
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_1

    .line 154
    :cond_5
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 163
    :cond_6
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_7

    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    sub-int/2addr v10, v11

    const/4 v11, 0x1

    if-ne v10, v11, :cond_7

    .line 164
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023c

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    goto :goto_3

    .line 166
    :cond_7
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_3

    .line 177
    :cond_8
    const/4 v10, 0x4

    if-gt v7, v10, :cond_10

    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    if-ne v10, v2, :cond_10

    .line 178
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    if-ne v10, v7, :cond_9

    .line 179
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_1

    .line 181
    :cond_9
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_b

    .line 182
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    if-eqz v10, :cond_c

    .line 183
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 187
    :goto_4
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    if-nez v10, :cond_f

    .line 188
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v10, v11, :cond_d

    .line 190
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 191
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_a

    .line 192
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023a

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 206
    :cond_a
    :goto_5
    new-instance v10, Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-boolean v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    invoke-direct {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>(Z)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 208
    :cond_b
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    .line 209
    iput v3, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    .line 210
    iput v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    .line 211
    iput v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    .line 212
    iput v7, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    .line 213
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_1

    .line 185
    :cond_c
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 196
    :cond_d
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_e

    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    sub-int/2addr v10, v11

    const/4 v11, 0x1

    if-ne v10, v11, :cond_e

    .line 197
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023c

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    goto :goto_5

    .line 199
    :cond_e
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_5

    .line 203
    :cond_f
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x2

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_5

    .line 215
    :cond_10
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-ne v10, v4, :cond_19

    .line 216
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    if-ne v2, v10, :cond_11

    .line 217
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_1

    .line 219
    :cond_11
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_13

    .line 220
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    if-eqz v10, :cond_14

    .line 221
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 225
    :goto_6
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    const/4 v11, 0x4

    if-ge v10, v11, :cond_18

    .line 226
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    if-ne v10, v11, :cond_17

    .line 227
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v10, v11, :cond_15

    .line 229
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 230
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_12

    .line 231
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023a

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 249
    :cond_12
    :goto_7
    new-instance v10, Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-boolean v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    invoke-direct {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>(Z)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 251
    :cond_13
    iput v7, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    .line 252
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    .line 253
    iput v3, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    .line 254
    iput v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    .line 255
    iput v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    .line 256
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_1

    .line 223
    :cond_14
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 235
    :cond_15
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_16

    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    sub-int/2addr v10, v11

    const/4 v11, 0x1

    if-ne v10, v11, :cond_16

    .line 236
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023c

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    goto :goto_7

    .line 238
    :cond_16
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_7

    .line 242
    :cond_17
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x2

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_7

    .line 246
    :cond_18
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x3

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_7

    .line 259
    :cond_19
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    if-ne v4, v10, :cond_1a

    .line 260
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_1

    .line 262
    :cond_1a
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1c

    .line 263
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    if-eqz v10, :cond_1d

    .line 264
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 268
    :goto_8
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-ne v10, v11, :cond_22

    .line 269
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    if-ne v10, v11, :cond_21

    .line 270
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    if-ne v10, v11, :cond_20

    .line 271
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-ne v10, v11, :cond_1e

    .line 273
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 274
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_1b

    .line 275
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023a

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    .line 296
    :cond_1b
    :goto_9
    new-instance v10, Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-boolean v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    invoke-direct {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>(Z)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 298
    :cond_1c
    iput v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    .line 299
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto/16 :goto_1

    .line 266
    :cond_1d
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 279
    :cond_1e
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_1f

    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    sub-int/2addr v10, v11

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1f

    .line 280
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023c

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    goto :goto_9

    .line 282
    :cond_1f
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_9

    .line 286
    :cond_20
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x2

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_9

    .line 290
    :cond_21
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x3

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_9

    .line 294
    :cond_22
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x4

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_9

    .line 305
    .end local v0    # "currentDay":I
    .end local v1    # "currentItem":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .end local v2    # "currentMonth":I
    .end local v3    # "currentWeek":I
    .end local v4    # "currentYear":I
    .end local v5    # "dayDiff":I
    .end local v7    # "weekDiff":I
    .end local v8    # "time":J
    :cond_23
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_25

    .line 306
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    if-eqz v10, :cond_26

    .line 307
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11, v12}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 312
    :goto_a
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-eq v10, v11, :cond_27

    .line 313
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x4

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 329
    :cond_24
    :goto_b
    new-instance v10, Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-boolean v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    invoke-direct {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>(Z)V

    iput-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 331
    :cond_25
    return-void

    .line 309
    :cond_26
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 314
    :cond_27
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    if-eq v10, v11, :cond_28

    .line 315
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x3

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_b

    .line 316
    :cond_28
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    if-eq v10, v11, :cond_29

    .line 317
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x2

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_b

    .line 318
    :cond_29
    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    if-eq v10, v11, :cond_2b

    .line 319
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_2a

    iget v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    iget v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    sub-int/2addr v10, v11

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2a

    .line 320
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023c

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    goto :goto_b

    .line 322
    :cond_2a
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_b

    .line 324
    :cond_2b
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 325
    iget-boolean v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    if-eqz v10, :cond_24

    .line 326
    iget-object v10, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    const v12, 0x7f0e023a

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/data/TimeCluster;->setName(Ljava/lang/String;)V

    goto :goto_b
.end method

.method private doClustering(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const/4 v2, 0x0

    .line 598
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->getZoomMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 616
    :goto_0
    return-void

    .line 601
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->computeForAlbumTimeWithTimeTable(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 604
    :pswitch_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 605
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->sortToMonth(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 604
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 607
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->sortToMonth(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 610
    .end local v0    # "i":I
    :pswitch_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 611
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->sortToYear(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 610
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 613
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->sortToYear(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 598
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sortToMonth(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V
    .locals 8
    .param p1, "currentItem"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    .line 504
    if-eqz p1, :cond_7

    .line 505
    iget-wide v2, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 506
    .local v2, "time":J
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    cmp-long v4, v4, v2

    if-gez v4, :cond_0

    .line 507
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    .line 509
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v4, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 511
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 512
    .local v0, "currentMonth":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 514
    .local v1, "currentYear":I
    iget v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-ne v4, v1, :cond_4

    .line 515
    iget v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    if-ne v0, v4, :cond_2

    .line 516
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 557
    .end local v0    # "currentMonth":I
    .end local v1    # "currentYear":I
    .end local v2    # "time":J
    :cond_1
    :goto_0
    return-void

    .line 518
    .restart local v0    # "currentMonth":I
    .restart local v1    # "currentYear":I
    .restart local v2    # "time":J
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 519
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 522
    new-instance v4, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 524
    :cond_3
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    .line 525
    iput v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    .line 526
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 529
    :cond_4
    iget v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    if-ne v1, v4, :cond_5

    .line 530
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 532
    :cond_5
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 533
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 535
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 537
    new-instance v4, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 539
    :cond_6
    iput v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    .line 540
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 545
    .end local v0    # "currentMonth":I
    .end local v1    # "currentYear":I
    .end local v2    # "time":J
    :cond_7
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 546
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    iget v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    iget v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-eq v4, v5, :cond_8

    .line 549
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 550
    new-instance v4, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 554
    :goto_1
    new-instance v4, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    goto/16 :goto_0

    .line 552
    :cond_8
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private sortToYear(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V
    .locals 7
    .param p1, "currentItem"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    const/4 v6, 0x4

    .line 560
    if-eqz p1, :cond_5

    .line 561
    iget-wide v2, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 562
    .local v2, "time":J
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    cmp-long v1, v4, v2

    if-gez v1, :cond_0

    .line 563
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    .line 565
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 567
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 569
    .local v0, "currentYear":I
    iget v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    if-ne v1, v0, :cond_2

    .line 570
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 586
    :goto_0
    sget-object v1, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "currentYear("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    .end local v0    # "currentYear":I
    .end local v2    # "time":J
    :cond_1
    :goto_1
    return-void

    .line 572
    .restart local v0    # "currentYear":I
    .restart local v2    # "time":J
    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    if-ne v0, v1, :cond_3

    .line 573
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 575
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 576
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4, v6}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 580
    new-instance v1, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    .line 582
    :cond_4
    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    .line 583
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/TimeCluster;->addItem(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    goto :goto_0

    .line 588
    .end local v0    # "currentYear":I
    .end local v2    # "time":J
    :cond_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 589
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4, v6}, Lcom/sec/android/gallery3d/data/TimeCluster;->generateCaption(Landroid/content/Context;I)V

    .line 592
    new-instance v1, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/data/TimeCluster;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    goto :goto_1
.end method


# virtual methods
.method public getCluster(I)Ljava/util/ArrayList;
    .locals 7
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 661
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt p1, v4, :cond_1

    .line 662
    sget-object v4, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCluster index("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") error. size:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    const/4 v3, 0x0

    .line 670
    :cond_0
    return-object v3

    .line 665
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/TimeCluster;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 666
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 667
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 668
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 682
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mNames:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 676
    const-string v0, ""

    .line 677
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method initTime(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V
    .locals 13
    .param p1, "currentItem"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .prologue
    .line 90
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrCluster:Lcom/sec/android/gallery3d/data/TimeCluster;

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/TimeCluster;->setAddToFirst(Z)V

    .line 99
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 100
    .local v7, "today":Ljava/util/Calendar;
    new-instance v0, Ljava/util/Date;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Ljava/util/Date;-><init>(IIIIII)V

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 101
    iget v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    iget v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v7, v0, v1, v2}, Ljava/util/Calendar;->set(III)V

    .line 102
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    .line 103
    iget-wide v8, p1, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 104
    .local v8, "latestTime":J
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    sub-long/2addr v0, v8

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 105
    move-wide v10, v8

    .line 106
    .local v10, "time":J
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDay:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayDay:I

    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeek:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayWeek:I

    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousMonth:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousYear:I

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    .line 112
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 113
    new-instance v0, Ljava/util/Date;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Ljava/util/Date;-><init>(IIIIII)V

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 114
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 115
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->currentDate:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v12

    .line 116
    .local v12, "todayDayOfMonth":I
    iget v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayYear:I

    iget v1, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->todayMonth:I

    invoke-virtual {v7, v0, v1, v12}, Ljava/util/Calendar;->set(III)V

    .line 117
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    .line 122
    .end local v10    # "time":J
    .end local v12    # "todayDayOfMonth":I
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousDayValuse:I

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->previousWeekValuse:I

    .line 125
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mCurrentTime:J

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryTimeUtils;->updateTimeTableWithLatestDay(J)V

    .line 126
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->isToday:Z

    goto :goto_0
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 9
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 621
    sget-object v7, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->TAG:Ljava/lang/String;

    const-string v8, "run start"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v4

    .line 623
    .local v4, "total":I
    if-gtz v4, :cond_0

    .line 652
    :goto_0
    return-void

    .line 626
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 629
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const/4 v7, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v2, v4, v7}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V

    .line 631
    iget v7, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mSortByType:I

    if-ne v7, v5, :cond_1

    :goto_1
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mReverseOrder:Z

    .line 634
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->sDateComparator:Ljava/util/Comparator;

    invoke-static {v2, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 637
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->initTime(Lcom/sec/android/gallery3d/data/Clustering$SmallItem;)V

    .line 640
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->doClustering(Ljava/util/ArrayList;)V

    .line 643
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 644
    .local v3, "m":I
    new-array v5, v3, [Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mNames:[Ljava/lang/String;

    .line 645
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v3, :cond_2

    .line 646
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mNames:[Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->mClusters:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/TimeCluster;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/TimeCluster;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v1

    .line 645
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v1    # "i":I
    .end local v3    # "m":I
    :cond_1
    move v5, v6

    .line 631
    goto :goto_1

    .line 648
    .restart local v1    # "i":I
    .restart local v3    # "m":I
    :cond_2
    sget-object v5, Lcom/sec/android/gallery3d/data/TimeAlbumClustering;->TAG:Ljava/lang/String;

    const-string v6, "run end"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 649
    .end local v1    # "i":I
    .end local v3    # "m":I
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
