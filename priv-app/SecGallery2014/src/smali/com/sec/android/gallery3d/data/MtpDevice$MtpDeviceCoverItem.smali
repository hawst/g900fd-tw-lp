.class Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "MtpDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/MtpDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MtpDeviceCoverItem"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/MtpDevice;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/MtpDevice;Lcom/sec/android/gallery3d/data/Path;J)V
    .locals 3
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "version"    # J

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;->this$0:Lcom/sec/android/gallery3d/data/MtpDevice;

    .line 243
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 244
    const-string v0, "MtpDevice"

    const-string v1, "MtpDeviceCoverItem created"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    return-void
.end method


# virtual methods
.method public getGroupId()J
    .locals 2

    .prologue
    .line 291
    const-string v0, "MtpDevice"

    const-string v1, "MtpDeviceCoverItem getGroupId"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 285
    const-string v0, "MtpDevice"

    const-string v1, "MtpDeviceCoverItem getHeight"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const/16 v0, 0x15e

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 273
    const-string v0, "MtpDevice"

    const-string v1, "MtpDeviceCoverItem getMimeType"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 279
    const-string v0, "MtpDevice"

    const-string v1, "MtpDeviceCoverItem getWidth"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const/16 v0, 0x15e

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 2
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    const-string v0, "MtpDevice"

    const-string v1, "MtpDeviceCoverItem requestImage"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem$1;-><init>(Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    const-string v0, "MtpDevice"

    const-string v1, "MtpDeviceCoverItem requestLargeImage"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem$2;-><init>(Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 234
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MtpDeviceCoverItem version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;->mDataVersion:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "string":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;->mPath:Lcom/sec/android/gallery3d/data/Path;

    if-eqz v1, :cond_0

    .line 236
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpDevice$MtpDeviceCoverItem;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 239
    :goto_0
    return-object v0

    .line 238
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", path : null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
