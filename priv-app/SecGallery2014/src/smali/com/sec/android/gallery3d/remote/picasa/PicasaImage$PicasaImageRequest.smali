.class Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;
.super Ljava/lang/Object;
.source "PicasaImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PicasaImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 0
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "type"    # I

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput p3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    .line 152
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 18
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 156
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$200(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v2

    .line 157
    .local v2, "cacheService":Lcom/sec/android/gallery3d/data/ImageCacheService;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->get()Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;

    move-result-object v7

    .line 158
    .local v7, "buffer":Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    const/16 v17, 0x0

    .line 160
    .local v17, "requestDownload":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$300(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getDateInMs()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    const/4 v8, 0x3

    if-ne v6, v8, :cond_1

    const/4 v6, 0x2

    :goto_0
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageData(Lcom/sec/android/gallery3d/data/Path;JILcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)Z

    move-result v15

    .line 162
    .local v15, "found":Z
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    const/4 v14, 0x0

    .line 187
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 219
    :cond_0
    :goto_1
    return-object v14

    .line 160
    .end local v15    # "found":Z
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    goto :goto_0

    .line 163
    .restart local v15    # "found":Z
    :cond_2
    if-eqz v15, :cond_8

    .line 164
    new-instance v16, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 165
    .local v16, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v16

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 166
    iget-object v3, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->data:[B

    iget v4, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->offset:I

    iget v5, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v3, v4, v5, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decodeUsingPool(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 167
    .local v14, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mIsVideo:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$400(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 168
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    iget v5, v7, Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;->length:I

    move-object/from16 v0, p1

    # invokes: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->refreshVideoThumbnail(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    invoke-static {v3, v0, v14, v4, v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$500(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 169
    if-nez v14, :cond_4

    const/16 v17, 0x1

    .line 172
    :cond_3
    :goto_2
    if-nez v17, :cond_8

    .line 173
    if-nez v14, :cond_5

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_5

    .line 174
    const-string v3, "PicasaImage"

    const-string v4, "decode cached failed "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    const/4 v14, 0x0

    .line 187
    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto :goto_1

    .line 169
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    const/16 v17, 0x0

    goto :goto_2

    .line 177
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    .line 178
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 181
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    if-eqz v3, :cond_7

    .line 182
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    neg-int v3, v3

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v14

    .line 187
    :cond_7
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    goto/16 :goto_1

    .end local v14    # "bitmap":Landroid/graphics/Bitmap;
    .end local v16    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_8
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    .line 190
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v3

    if-nez v3, :cond_9

    .line 191
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 187
    .end local v15    # "found":Z
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->recycle(Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;)V

    throw v3

    .line 192
    .restart local v15    # "found":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$600(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 193
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 196
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    # invokes: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPhotoUrl(I)Ljava/net/URL;
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$700(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;I)Ljava/net/URL;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/data/DownloadUtils;->requestDownload(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/net/URL;)[B

    move-result-object v13

    .line 197
    .local v13, "array":[B
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->setMode(I)Z

    move-result v3

    if-nez v3, :cond_b

    .line 198
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 200
    :cond_b
    if-nez v13, :cond_c

    .line 201
    const-string v3, "PicasaImage"

    const-string v4, "download failed "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 205
    :cond_c
    new-instance v16, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 206
    .restart local v16    # "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, v16

    iput-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 207
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v13, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 208
    .restart local v14    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_d

    .line 209
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 212
    :cond_d
    if-eqz v14, :cond_e

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 213
    :cond_e
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 215
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$800(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getDateInMs()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_10

    const/4 v12, 0x2

    :goto_3
    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/gallery3d/data/ImageCacheService;->putImageData(Lcom/sec/android/gallery3d/data/Path;JI[B)V

    .line 217
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    if-eqz v3, :cond_0

    .line 218
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->this$0:Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    # getter for: Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->mData:Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->access$100(Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;)Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    neg-int v3, v3

    const/4 v4, 0x1

    invoke-static {v14, v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->rotateBitmap(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v14

    goto/16 :goto_1

    .line 215
    :cond_10
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->mType:I

    goto :goto_3
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage$PicasaImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
