.class public abstract Lcom/sec/android/gallery3d/remote/RemoteMediaItem;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "RemoteMediaItem.java"


# instance fields
.field protected final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 29
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 30
    return-void
.end method


# virtual methods
.method public abstract getSourceId()Ljava/lang/String;
.end method

.method public abstract getSourceUrl()Ljava/lang/String;
.end method

.method public abstract getThumbPath()Ljava/lang/String;
.end method

.method public abstract getThumbUrl()Ljava/lang/String;
.end method
