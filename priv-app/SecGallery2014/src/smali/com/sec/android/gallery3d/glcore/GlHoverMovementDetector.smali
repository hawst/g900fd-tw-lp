.class public Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;
.super Ljava/lang/Object;
.source "GlHoverMovementDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;
    }
.end annotation


# static fields
.field private static final HOVER_SCROLL_DETECT_AREA:I = 0x50

.field public static final HOVER_SCROLL_DOWN:I = 0x2

.field public static final HOVER_SCROLL_LEFT:I = 0x3

.field public static final HOVER_SCROLL_NONE:I = 0x0

.field public static final HOVER_SCROLL_RIGHT:I = 0x4

.field public static final HOVER_SCROLL_UP:I = 0x1

.field public static final LEFTNRIGHT:I = 0x1

.field private static final LIST_SCROLL_INTERVAL:J = 0x12cL

.field private static final LIST_SCROLL_RECOGNITION_EVT:I = 0x1

.field private static final LIST_SCROLL_TICK_EVT:I = 0x2

.field private static final LIST_SCROLL_TICK_INTERVAL:I = 0xa

.field private static final MIN_FLING_VELOCITY:F = 500.0f

.field private static final MOVE_STEP:I = 0x28

.field private static final PAST_COUNT:I = 0x8


# instance fields
.field private mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

.field private mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mIsListScroll:Z

.field private mLMs:[J

.field private mLX:[I

.field private mLY:[I

.field private mListScrollActive:Z

.field private mLxIndex:I

.field private mMoveEvent:Landroid/view/MotionEvent;

.field private mMoveX:I

.field private mMoveY:I

.field private mPressMs:J

.field private mPressX:I

.field private mPressY:I

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mScrollDirection:I

.field private mXFlexibleTopScrollMargin:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;)V
    .locals 3
    .param p1, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "listener"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 36
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 37
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    .line 41
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLX:[I

    .line 42
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLY:[I

    .line 43
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLMs:[J

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mIsListScroll:Z

    .line 50
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    .line 51
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    .line 52
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    .line 59
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mXFlexibleTopScrollMargin:I

    .line 62
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 63
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$1;-><init>(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 80
    iput-object p2, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    .prologue
    .line 9
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;
    .param p1, "x1"    # Z

    .prologue
    .line 9
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Lcom/sec/android/gallery3d/glcore/GlHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->updateMovement(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;Landroid/view/MotionEvent;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Z

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->calcMovement(Landroid/view/MotionEvent;Z)Z

    move-result v0

    return v0
.end method

.method private calcMovement(Landroid/view/MotionEvent;Z)Z
    .locals 22
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "stop"    # Z

    .prologue
    .line 91
    const/16 v17, 0x0

    .line 93
    .local v17, "wasConsumed":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v18

    move/from16 v0, v18

    float-to-int v11, v0

    .line 94
    .local v11, "nx":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v18

    move/from16 v0, v18

    float-to-int v14, v0

    .line 95
    .local v14, "ny":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v12

    .line 97
    .local v12, "ms":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLX:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    move/from16 v19, v0

    aput v11, v18, v19

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLY:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    move/from16 v19, v0

    aput v14, v18, v19

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLMs:[J

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    move/from16 v19, v0

    aput-wide v12, v18, v19

    .line 100
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    .line 101
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    move/from16 v18, v0

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 102
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    .line 104
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressX:I

    move/from16 v18, v0

    sub-int v9, v11, v18

    .line 105
    .local v9, "movX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressY:I

    move/from16 v18, v0

    sub-int v10, v14, v18

    .line 107
    .local v10, "movY":I
    if-eqz p2, :cond_4

    .line 108
    const/4 v8, 0x0

    .line 109
    .local v8, "fIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLX:[I

    move-object/from16 v18, v0

    aget v18, v18, v8

    sub-int v6, v11, v18

    .line 110
    .local v6, "dx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLY:[I

    move-object/from16 v18, v0

    aget v18, v18, v8

    sub-int v7, v14, v18

    .line 111
    .local v7, "dy":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLMs:[J

    move-object/from16 v18, v0

    aget-wide v18, v18, v8

    sub-long v4, v12, v18

    .line 112
    .local v4, "duration":J
    int-to-float v0, v6

    move/from16 v18, v0

    const/high16 v19, 0x44fa0000    # 2000.0f

    mul-float v18, v18, v19

    long-to-float v0, v4

    move/from16 v19, v0

    div-float v15, v18, v19

    .line 113
    .local v15, "velocityX":F
    int-to-float v0, v7

    move/from16 v18, v0

    const/high16 v19, 0x44fa0000    # 2000.0f

    mul-float v18, v18, v19

    long-to-float v0, v4

    move/from16 v19, v0

    div-float v16, v18, v19

    .line 114
    .local v16, "velocityY":F
    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x43fa0000    # 500.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_1

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v18

    const/high16 v19, 0x43fa0000    # 500.0f

    cmpg-float v18, v18, v19

    if-gez v18, :cond_1

    .line 115
    const/4 v15, 0x0

    .line 116
    const/16 v16, 0x0

    .line 118
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 119
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-interface {v0, v1, v10, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;->onRelease(IIII)Z

    move-result v18

    or-int v17, v17, v18

    .line 125
    .end local v4    # "duration":J
    .end local v6    # "dx":I
    .end local v7    # "dy":I
    .end local v8    # "fIndex":I
    .end local v15    # "velocityX":F
    .end local v16    # "velocityY":F
    :goto_0
    return v17

    .line 121
    .restart local v4    # "duration":J
    .restart local v6    # "dx":I
    .restart local v7    # "dy":I
    .restart local v8    # "fIndex":I
    .restart local v15    # "velocityX":F
    .restart local v16    # "velocityY":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-interface {v0, v9, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;->onRelease(IIII)Z

    move-result v18

    or-int v17, v17, v18

    goto :goto_0

    .line 123
    .end local v4    # "duration":J
    .end local v6    # "dx":I
    .end local v7    # "dy":I
    .end local v8    # "fIndex":I
    .end local v15    # "velocityX":F
    .end local v16    # "velocityY":F
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v0, v9, v10, v1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;->onMove(III)Z

    move-result v18

    or-int v17, v17, v18

    goto :goto_0
.end method

.method private checkPoint(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 236
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->getDirection(II)I

    move-result v0

    if-nez v0, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 239
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private processListScrollEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x7

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 162
    const/4 v10, 0x0

    .line 164
    .local v10, "wasConsumed":Z
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mIsListScroll:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 165
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 168
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 170
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v11, v1

    .line 171
    .local v11, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v12, v1

    .line 173
    .local v12, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    .line 175
    .local v8, "ms":J
    if-ne v0, v5, :cond_2

    .line 177
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressX:I

    .line 178
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressY:I

    .line 179
    iput-wide v8, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressMs:J

    .line 180
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLX:[I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressX:I

    aput v4, v1, v3

    .line 181
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLY:[I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressY:I

    aput v4, v1, v3

    .line 182
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLMs:[J

    iget-wide v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressMs:J

    aput-wide v4, v1, v3

    .line 183
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mLxIndex:I

    .line 184
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressX:I

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    .line 185
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressY:I

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    .line 186
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressX:I

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressY:I

    invoke-interface {v1, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;->onPress(II)Z

    move-result v10

    .line 187
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const-wide/16 v6, 0x12c

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    .line 188
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveEvent:Landroid/view/MotionEvent;

    .line 189
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z

    .line 190
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mIsListScroll:Z

    .line 211
    :cond_1
    :goto_0
    return v10

    .line 191
    :cond_2
    if-ne v0, v4, :cond_4

    .line 193
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mIsListScroll:Z

    if-eqz v1, :cond_3

    .line 194
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    if-nez v1, :cond_3

    .line 195
    invoke-virtual {p0, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->getDirection(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    .line 197
    :cond_3
    const/4 v10, 0x1

    goto :goto_0

    .line 198
    :cond_4
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 200
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z

    if-nez v1, :cond_5

    .line 201
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 205
    :goto_1
    iput v3, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    .line 206
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mListScrollActive:Z

    .line 207
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mIsListScroll:Z

    .line 208
    const/4 v10, 0x1

    goto :goto_0

    .line 203
    :cond_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveEvent:Landroid/view/MotionEvent;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->calcMovement(Landroid/view/MotionEvent;Z)Z

    goto :goto_1
.end method

.method private updateMovement(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 216
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    add-int/lit8 v0, v0, 0x28

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    .line 217
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 228
    :cond_0
    :goto_0
    return-object p1

    .line 218
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 219
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    add-int/lit8 v0, v0, -0x28

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    .line 220
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveY:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_0

    .line 221
    :cond_2
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 222
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    add-int/lit8 v0, v0, 0x28

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    .line 223
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressY:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_0

    .line 224
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mScrollDirection:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 225
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    add-int/lit8 v0, v0, -0x28

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    .line 226
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mMoveX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mPressY:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_0
.end method


# virtual methods
.method public getDirection(II)I
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/high16 v9, 0x42a00000    # 80.0f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 246
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v3

    .line 247
    .local v3, "point":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    mul-float v1, v7, v8

    .line 248
    .local v1, "height":F
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    mul-float v6, v7, v8

    .line 249
    .local v6, "width":F
    iget v7, v3, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    div-float/2addr v7, v6

    mul-float/2addr v7, v9

    float-to-int v2, v7

    .line 250
    .local v2, "horizontalArea":I
    iget v7, v3, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    div-float/2addr v7, v1

    mul-float/2addr v7, v9

    float-to-int v5, v7

    .line 251
    .local v5, "verticalArea":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 252
    .local v0, "actionbarHeight":I
    const/4 v4, 0x0

    .line 254
    .local v4, "scrollType":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;->getListScrollMode()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 255
    if-lez p1, :cond_1

    if-ge p1, v2, :cond_1

    .line 256
    const/4 v4, 0x3

    .line 269
    :cond_0
    :goto_0
    return v4

    .line 257
    :cond_1
    iget v7, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v2

    if-le p1, v7, :cond_0

    .line 258
    const/4 v4, 0x4

    goto :goto_0

    .line 261
    :cond_2
    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mXFlexibleTopScrollMargin:I

    add-int/2addr v7, v0

    if-le p2, v7, :cond_3

    iget v7, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mXFlexibleTopScrollMargin:I

    add-int/2addr v7, v0

    add-int/2addr v7, v5

    if-ge p2, v7, :cond_3

    .line 262
    const/4 v4, 0x1

    goto :goto_0

    .line 263
    :cond_3
    iget v7, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v7, v5

    if-le p2, v7, :cond_0

    .line 264
    const/4 v4, 0x2

    goto :goto_0
.end method

.method public isSelectedAt(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 232
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->checkPoint(II)Z

    move-result v0

    return v0
.end method

.method public onHover(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xa

    .line 130
    const/4 v1, 0x0

    .line 132
    .local v1, "wasConsumed":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 133
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 135
    .local v3, "y":I
    invoke-direct {p0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->checkPoint(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 136
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 137
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mIsListScroll:Z

    if-eqz v4, :cond_0

    .line 138
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 139
    .local v0, "cancel":Landroid/view/MotionEvent;
    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 140
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->processListScrollEvent(Landroid/view/MotionEvent;)Z

    .line 141
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 143
    .end local v0    # "cancel":Landroid/view/MotionEvent;
    :cond_0
    const/4 v4, 0x0

    .line 158
    :goto_0
    return v4

    .line 146
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->processListScrollEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_1
    move v4, v1

    .line 158
    goto :goto_0

    .line 149
    :cond_2
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mIsListScroll:Z

    if-eqz v4, :cond_3

    .line 150
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 151
    .restart local v0    # "cancel":Landroid/view/MotionEvent;
    invoke-virtual {v0, v6}, Landroid/view/MotionEvent;->setAction(I)V

    .line 152
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->processListScrollEvent(Landroid/view/MotionEvent;)Z

    .line 153
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 155
    .end local v0    # "cancel":Landroid/view/MotionEvent;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setHoverScrollFlexibleHeightMargin(I)V
    .locals 0
    .param p1, "topMargin"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mXFlexibleTopScrollMargin:I

    .line 244
    return-void
.end method

.method public setMoveDetectorListener(Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlHoverMovementDetector$GlHoverMoveDetectorListener;

    .line 275
    return-void
.end method
