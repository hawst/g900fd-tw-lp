.class Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;
.super Ljava/lang/Thread;
.source "AlbumReloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/AlbumReloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReloadTask"
.end annotation


# instance fields
.field private volatile mActive:Z

.field private volatile mDirty:Z

.field private mIsLoading:Z

.field private volatile mWait:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AlbumReloader;Ljava/lang/String;)V
    .locals 2
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 168
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    .line 169
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 163
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mActive:Z

    .line 164
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mDirty:Z

    .line 165
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mIsLoading:Z

    .line 166
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mWait:Z

    .line 170
    return-void
.end method

.method private updateLoading(Z)V
    .locals 6
    .param p1, "loading"    # Z

    .prologue
    .line 173
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mIsLoading:Z

    if-ne v4, p1, :cond_1

    .line 182
    :cond_0
    return-void

    .line 174
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mIsLoading:Z

    .line 175
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    if-eqz p1, :cond_3

    const/4 v4, 0x0

    :goto_0
    # setter for: Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadState:I
    invoke-static {v5, v4}, Lcom/sec/android/gallery3d/app/AlbumReloader;->access$502(Lcom/sec/android/gallery3d/app/AlbumReloader;I)I

    .line 176
    if-nez p1, :cond_0

    .line 177
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/AlbumReloader;->access$600(Lcom/sec/android/gallery3d/app/AlbumReloader;)[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 178
    .local v3, "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    if-eqz v3, :cond_2

    .line 179
    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;->reload()V

    .line 177
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 175
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private waitLoading()Z
    .locals 5

    .prologue
    .line 185
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumReloader;->mReloadListeners:[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    invoke-static {v4}, Lcom/sec/android/gallery3d/app/AlbumReloader;->access$600(Lcom/sec/android/gallery3d/app/AlbumReloader;)[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 186
    .local v3, "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;->isLoading()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 187
    const/4 v4, 0x1

    .line 190
    .end local v3    # "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    :goto_1
    return v4

    .line 185
    .restart local v3    # "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 190
    .end local v3    # "listener":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized notifyDirty(Z)V
    .locals 1
    .param p1, "fromChild"    # Z

    .prologue
    .line 234
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mWait:Z

    if-eqz v0, :cond_1

    .line 235
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mDirty:Z

    .line 236
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 238
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mWait:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    monitor-exit p0

    return-void

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 195
    const/4 v2, 0x0

    .line 196
    .local v2, "updateComplete":Z
    :cond_0
    :goto_0
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mActive:Z

    if-eqz v7, :cond_7

    .line 197
    monitor-enter p0

    .line 198
    :try_start_0
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mActive:Z

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mDirty:Z

    if-nez v7, :cond_1

    if-nez v2, :cond_2

    :cond_1
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mWait:Z

    if-eqz v7, :cond_3

    .line 199
    :cond_2
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mDirty:Z

    .line 200
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/AlbumReloader;->mWaitWhenResume:Z
    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/AlbumReloader;->access$702(Lcom/sec/android/gallery3d/app/AlbumReloader;Z)Z

    .line 201
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->updateLoading(Z)V

    .line 202
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 203
    monitor-exit p0

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 205
    :cond_3
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/AlbumReloader;->mWaitWhenResume:Z
    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/AlbumReloader;->access$702(Lcom/sec/android/gallery3d/app/AlbumReloader;Z)Z

    .line 206
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->waitLoading()Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mWait:Z

    .line 207
    iget-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mWait:Z

    if-eqz v7, :cond_4

    monitor-exit p0

    goto :goto_0

    .line 208
    :cond_4
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mDirty:Z

    .line 209
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->updateLoading(Z)V

    .line 211
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/AlbumReloader;->access$800(Lcom/sec/android/gallery3d/app/AlbumReloader;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v4

    .line 212
    .local v4, "version":J
    const/4 v1, 0x0

    .line 214
    .local v1, "info":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    :try_start_2
    new-instance v7, Lcom/sec/android/gallery3d/app/AlbumReloader$GetUpdateInfo;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-direct {v7, v8, v4, v5}, Lcom/sec/android/gallery3d/app/AlbumReloader$GetUpdateInfo;-><init>(Lcom/sec/android/gallery3d/app/AlbumReloader;J)V

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AlbumReloader$GetUpdateInfo;->call()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    .line 218
    :goto_1
    if-nez v1, :cond_6

    move v2, v3

    .line 219
    :goto_2
    if-nez v2, :cond_0

    .line 220
    iget-wide v8, v1, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    cmp-long v7, v8, v4

    if-eqz v7, :cond_5

    .line 221
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    # getter for: Lcom/sec/android/gallery3d/app/AlbumReloader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/AlbumReloader;->access$800(Lcom/sec/android/gallery3d/app/AlbumReloader;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    iput v7, v1, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->size:I

    .line 222
    iput-wide v4, v1, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    .line 225
    :cond_5
    :try_start_3
    new-instance v7, Lcom/sec/android/gallery3d/app/AlbumReloader$UpdateContent;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-direct {v7, v8, v1}, Lcom/sec/android/gallery3d/app/AlbumReloader$UpdateContent;-><init>(Lcom/sec/android/gallery3d/app/AlbumReloader;Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;)V

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AlbumReloader$UpdateContent;->call()Ljava/lang/Void;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 215
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 216
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_6
    move v2, v6

    .line 218
    goto :goto_2

    .line 230
    .end local v1    # "info":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    .end local v4    # "version":J
    :cond_7
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->updateLoading(Z)V

    .line 231
    return-void
.end method

.method public declared-synchronized terminate()V
    .locals 1

    .prologue
    .line 242
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadTask;->mActive:Z

    .line 243
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    monitor-exit p0

    return-void

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
