.class public Lcom/sec/android/gallery3d/ui/ContextualTagView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "ContextualTagView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;,
        Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;
    }
.end annotation


# static fields
.field private static final ACTION_BAR_HEIGHT:I = 0x41

.field public static final CITY_ID:Ljava/lang/String; = "city_ID"

.field public static final CONTEXTUAL_URI:Landroid/net/Uri;

.field public static final FILES_URI:Landroid/net/Uri;

.field private static final HIDE_ANIMATION_DURATION:I = 0x12c

.field private static final MAX_BOTTOM_MARGIN:I = 0x244

.field private static final MAX_LEFT_MARGIN:I = 0x230

.field private static final MSG_HIDE_CONTEXTUAL_TAG:I = 0x1

.field private static final MSG_SHOW_CONTEXTUAL_TAG_HELP:I = 0x3

.field private static final TABLET_DEFAULT_HELP_HEIGHT:I = 0xfa

.field private static final TABLET_DEFAULT_HELP_WIDTH:I = 0x2bc

.field private static final TAG:Ljava/lang/String;

.field private static final TEXT_COLOR:I

.field public static final WEATHER_ID:Ljava/lang/String; = "weather_ID"

.field static mCategory:Ljava/lang/String;

.field static mDate:Ljava/lang/String;

.field static mFace:Ljava/lang/String;

.field public static mFilePath:Ljava/lang/String;

.field static mLocation:Ljava/lang/String;

.field static mUserTags:Ljava/lang/String;

.field static mWeather:Ljava/lang/String;


# instance fields
.field private final SPERATOR:Ljava/lang/String;

.field private mAccWeatherLogo:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mAccWeatherPositionX:I

.field private mAccWeatherPositionY:I

.field mActionBarShown:Z

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private final mCAHandler:Landroid/os/Handler;

.field private mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mCategoryTagPositionX:I

.field private mCategoryTagPositionY:I

.field private mCityID:J

.field private mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

.field private mContext:Landroid/content/Context;

.field mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field mContextualTagFace:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field private mContextualTagHelpDialog:Landroid/app/Dialog;

.field mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field private mContextualTagPositionX:I

.field private mContextualTagTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field mContextualTagWeather:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

.field private mCurrentMultiZone:I

.field private mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mDateTagPositionX:I

.field private mDateTagPositionY:I

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field mFaces:[Lcom/sec/android/gallery3d/data/Face;

.field mHandler:Landroid/os/Handler;

.field private mHelpDialogDisabled:Z

.field private mIsNotShowChecked:Z

.field mIsVertical:Z

.field private mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mLocationTagPositionX:I

.field private mLocationTagPositionY:I

.field private mMaxTagArea:I

.field mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

.field private mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

.field private mTextFaceSize:I

.field private mTextHeight:I

.field private mTextLocationSize:I

.field private mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mUserTagsTagPositionX:I

.field private mUserTagsTagPositionY:I

.field private mWeatherTag:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xf0

    .line 82
    const-class v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    .line 136
    const-string v0, "content://media/external/contextural_tags"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->CONTEXTUAL_URI:Landroid/net/Uri;

    .line 137
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->FILES_URI:Landroid/net/Uri;

    .line 150
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TEXT_COLOR:I

    .line 158
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFilePath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 177
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 97
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    .line 99
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagWeather:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 100
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 101
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagFace:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 102
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 103
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 104
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 114
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mActionBarShown:Z

    .line 115
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mIsVertical:Z

    .line 118
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextFaceSize:I

    .line 119
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    .line 120
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextLocationSize:I

    .line 121
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mAccWeatherPositionX:I

    .line 122
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mAccWeatherPositionY:I

    .line 123
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCityID:J

    .line 126
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionX:I

    .line 127
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I

    .line 128
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionX:I

    .line 129
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionY:I

    .line 130
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionX:I

    .line 131
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionY:I

    .line 132
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagPositionX:I

    .line 133
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagPositionY:I

    .line 134
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    .line 154
    const-string v0, ","

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->SPERATOR:Ljava/lang/String;

    .line 156
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 163
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z

    .line 165
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mIsNotShowChecked:Z

    .line 168
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCurrentMultiZone:I

    .line 169
    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagTask:Landroid/os/AsyncTask;

    .line 178
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    .line 179
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 181
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 182
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_BIG:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextFaceSize:I

    .line 183
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_BIG:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextLocationSize:I

    .line 184
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->initDisplaySize()V

    .line 188
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setIcon(Landroid/content/Context;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .line 191
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    .line 192
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCurrentMultiZone:I

    .line 194
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/ui/ContextualTagView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$1;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCAHandler:Landroid/os/Handler;

    .line 214
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/ContextualTagView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->hide(Z)V

    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/ui/ContextualTagView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setCategory()V

    return-void
.end method

.method static synthetic access$1202(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/ui/ContextualTagView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setUserTags(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1402(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/ui/ContextualTagView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mIsNotShowChecked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/ContextualTagView;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/ui/ContextualTagView;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/ContextualTagView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setDate()V

    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;)Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setFace()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContextualTagView;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setLocation()V

    return-void
.end method

.method private computeAddressCA(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Landroid/location/Address;
    .locals 20
    .param p1, "set"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    .param p2, "geocoder"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    .prologue
    .line 793
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 794
    .local v4, "setMinLatitude":D
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 795
    .local v6, "setMinLongitude":D
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 796
    .local v10, "setMaxLatitude":D
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 797
    .local v12, "setMaxLongitude":D
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    move-wide/from16 v16, v0

    sub-double v8, v8, v16

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    cmpg-double v3, v8, v16

    if-gez v3, :cond_0

    .line 799
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 800
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 801
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 802
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 804
    :cond_0
    const/4 v8, 0x1

    move-object/from16 v3, p2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v2

    .line 805
    .local v2, "addr1":Landroid/location/Address;
    const/4 v14, 0x1

    move-object/from16 v9, p2

    invoke-virtual/range {v9 .. v14}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v15

    .line 806
    .local v15, "addr2":Landroid/location/Address;
    if-nez v2, :cond_2

    move-object v2, v15

    .line 811
    .end local v2    # "addr1":Landroid/location/Address;
    :cond_1
    :goto_0
    return-object v2

    .line 808
    .restart local v2    # "addr1":Landroid/location/Address;
    :cond_2
    if-nez v15, :cond_1

    goto :goto_0
.end method

.method private generateName(DDLcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;
    .locals 13
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "geocoder"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    .prologue
    .line 736
    new-instance v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;-><init>()V

    .line 737
    .local v8, "set":Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    const/4 v3, 0x0

    .line 739
    .local v3, "name":Ljava/lang/String;
    move-wide v4, p1

    .line 740
    .local v4, "itemLatitude":D
    move-wide/from16 v6, p3

    .line 742
    .local v6, "itemLongitude":D
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    cmpl-double v9, v10, v4

    if-lez v9, :cond_0

    .line 743
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 744
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 746
    :cond_0
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    cmpg-double v9, v10, v4

    if-gez v9, :cond_1

    .line 747
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 748
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 750
    :cond_1
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    cmpl-double v9, v10, v6

    if-lez v9, :cond_2

    .line 751
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 752
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 754
    :cond_2
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    cmpg-double v9, v10, v6

    if-gez v9, :cond_3

    .line 755
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 756
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 759
    :cond_3
    move-object/from16 v0, p5

    invoke-direct {p0, v8, v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->computeAddressCA(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Landroid/location/Address;

    move-result-object v2

    .line 761
    .local v2, "addr":Landroid/location/Address;
    if-eqz v2, :cond_5

    .line 762
    invoke-virtual {v2}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 763
    invoke-virtual {v2}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v3

    .line 764
    if-eqz v3, :cond_4

    .line 765
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0221

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    .line 779
    :cond_4
    :goto_0
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v9, :cond_5

    invoke-virtual {v2}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_5

    .line 780
    invoke-virtual {v2}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 781
    invoke-virtual {v2}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v3

    .line 782
    if-eqz v3, :cond_5

    .line 783
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0221

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    .line 789
    :cond_5
    return-object v3

    .line 767
    :cond_6
    invoke-virtual {v2}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 768
    invoke-virtual {v2}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v3

    .line 769
    if-eqz v3, :cond_4

    .line 770
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0221

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    goto :goto_0

    .line 773
    :cond_7
    invoke-virtual {v2}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v3

    .line 774
    if-eqz v3, :cond_4

    .line 775
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0221

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    goto :goto_0
.end method

.method private getBaseUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 712
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->FILES_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    return-object v0
.end method

.method private getDisplaySize(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1103
    new-instance v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;Lcom/sec/android/gallery3d/ui/ContextualTagView$1;)V

    .line 1104
    .local v1, "screenSize":Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    .line 1105
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    .line 1107
    iput v4, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    .line 1109
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1110
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 1112
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 1114
    .local v0, "p":Landroid/graphics/Point;
    iget v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    iget v3, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    if-le v2, v3, :cond_2

    .line 1115
    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    .line 1120
    :goto_0
    iget v2, v0, Landroid/graphics/Point;->x:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    .line 1121
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    .line 1123
    iget v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    int-to-float v2, v2

    iget v3, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    sub-float v3, v4, v3

    mul-float/2addr v2, v3

    invoke-interface {p1, v5, v2, v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    .line 1124
    iget v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    iget v3, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    invoke-interface {p1, v2, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->scale(FFF)V

    .line 1127
    .end local v0    # "p":Landroid/graphics/Point;
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1128
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 1129
    .restart local v0    # "p":Landroid/graphics/Point;
    iget v2, v0, Landroid/graphics/Point;->x:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    .line 1130
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    .line 1132
    .end local v0    # "p":Landroid/graphics/Point;
    :cond_1
    return-object v1

    .line 1117
    .restart local v0    # "p":Landroid/graphics/Point;
    :cond_2
    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget v3, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    add-int/lit16 v3, v3, -0x82

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    goto :goto_0
.end method

.method public static getFace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    return-object v0
.end method

.method public static getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 545
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public static getWeather()Ljava/lang/String;
    .locals 1

    .prologue
    .line 549
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mWeather:Ljava/lang/String;

    return-object v0
.end method

.method private hide(Z)V
    .locals 6
    .param p1, "useAnimation"    # Z

    .prologue
    const/4 v5, 0x1

    .line 516
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getVisibility()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 519
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    if-nez v2, :cond_0

    .line 523
    if-eqz p1, :cond_2

    .line 525
    :try_start_0
    new-instance v0, Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;-><init>(FF)V

    .line 526
    .local v0, "animation":Lcom/sec/android/gallery3d/anim/AlphaAnimation;
    const/16 v2, 0x12c

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;->setDuration(I)V

    .line 527
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    .end local v0    # "animation":Lcom/sec/android/gallery3d/anim/AlphaAnimation;
    :cond_2
    :goto_1
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setVisibility(I)V

    goto :goto_0

    .line 528
    :catch_0
    move-exception v1

    .line 529
    .local v1, "e":Ljava/lang/IllegalStateException;
    sget-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalStateException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private initDisplaySize()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDisplayWidth:I

    .line 218
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDisplayHeight:I

    .line 219
    return-void
.end method

.method private isVideoFilter(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 553
    if-nez p1, :cond_1

    .line 556
    :cond_0
    :goto_0
    return v0

    .line 555
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 556
    const-string/jumbo v1, "video/mp4"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "video/3gpp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setCategory()V
    .locals 8

    .prologue
    .line 622
    const/4 v6, 0x0

    sput-object v6, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    .line 624
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v6, :cond_3

    .line 625
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 626
    .local v3, "strBuilder":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getBaseUri()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getCategoryType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 627
    .local v0, "categoryType":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 628
    const/4 v1, 0x0

    .line 629
    .local v1, "count":I
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, ","

    invoke-direct {v4, v0, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    .local v4, "token":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 631
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 632
    .local v5, "type":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 633
    const-string v6, ", "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635
    :cond_0
    sget-object v6, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 636
    .local v2, "resId":I
    if-eqz v2, :cond_1

    .line 637
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 640
    goto :goto_0

    .line 641
    .end local v2    # "resId":I
    .end local v5    # "type":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    .line 644
    .end local v0    # "categoryType":Ljava/lang/String;
    .end local v1    # "count":I
    .end local v3    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "token":Ljava/util/StringTokenizer;
    :cond_3
    return-void
.end method

.method private setDate()V
    .locals 8

    .prologue
    .line 560
    const/4 v2, 0x0

    .line 562
    .local v2, "dateText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 563
    new-instance v0, Ljava/util/Date;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 564
    .local v0, "date":Ljava/util/Date;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 565
    .local v1, "dateFormat":Ljava/text/DateFormat;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 568
    .end local v0    # "date":Ljava/util/Date;
    .end local v1    # "dateFormat":Ljava/text/DateFormat;
    :cond_0
    sput-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    .line 569
    return-void
.end method

.method private setFace()V
    .locals 14

    .prologue
    const v13, 0x7f0e00c0

    const/4 v12, 0x1

    .line 572
    const/4 v4, 0x0

    .line 573
    .local v4, "faces":[Lcom/sec/android/gallery3d/data/Face;
    const-string v2, ""

    .line 575
    .local v2, "faceName":Ljava/lang/String;
    const/4 v10, 0x0

    sput-object v10, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    .line 577
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v10, v10, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v10, :cond_0

    .line 578
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v10, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaces()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v4

    .line 581
    :cond_0
    iput-object v4, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    .line 582
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 584
    .local v8, "res":Landroid/content/res/Resources;
    if-eqz v4, :cond_8

    .line 585
    const/4 v3, 0x0

    .line 586
    .local v3, "faceNum":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 587
    .local v9, "sb":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 588
    .local v7, "personId":Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/Face;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_7

    aget-object v1, v0, v5

    .line 589
    .local v1, "face":Lcom/sec/android/gallery3d/data/Face;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v7

    .line 590
    if-eqz v7, :cond_1

    .line 591
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 593
    :cond_1
    sget-object v10, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    if-eqz v10, :cond_3

    if-eqz v2, :cond_3

    sget-object v10, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    const-string v11, "/"

    invoke-virtual {v2, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 588
    :cond_2
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 597
    :cond_3
    if-eqz v2, :cond_2

    .line 598
    const-string v10, "Me"

    const-string v11, "/"

    invoke-virtual {v2, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 599
    add-int/lit8 v3, v3, 0x1

    .line 600
    if-ne v3, v12, :cond_4

    .line 601
    const-string v10, "/"

    invoke-virtual {v2, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 603
    :cond_4
    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v2, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    .line 607
    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 608
    if-ne v3, v12, :cond_6

    .line 609
    iget-object v10, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 611
    :cond_6
    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    .line 617
    .end local v1    # "face":Lcom/sec/android/gallery3d/data/Face;
    :cond_7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    .line 619
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/Face;
    .end local v3    # "faceNum":I
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "personId":Ljava/lang/String;
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    :cond_8
    return-void
.end method

.method private setLocation()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 716
    new-instance v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-direct {v6, v1}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    .line 717
    .local v6, "geocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    const/4 v0, 0x0

    .line 719
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 722
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->generateName(DDLcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;

    move-result-object v0

    .line 723
    if-nez v0, :cond_2

    .line 724
    sget-object v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    const-string v2, "Location name(generateName) is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 730
    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    .line 732
    :cond_1
    return-void

    .line 726
    :cond_2
    sget-object v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    const-string v2, "Location name(generateName) is not null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setUserTags(Ljava/lang/String;)V
    .locals 15
    .param p1, "face"    # Ljava/lang/String;

    .prologue
    .line 647
    const/4 v13, 0x0

    sput-object v13, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    .line 649
    sget-boolean v13, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v13, :cond_9

    .line 650
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 651
    .local v4, "sb":Ljava/lang/StringBuffer;
    iget-object v13, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getBaseUri()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTag(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 652
    .local v12, "userTagType":Ljava/lang/String;
    iget-object v13, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getBaseUri()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getSubCategoryType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 653
    .local v7, "subCategoryType":Ljava/lang/String;
    const/4 v2, 0x0

    .line 655
    .local v2, "personNamesType":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 656
    move-object/from16 v2, p1

    .line 658
    :cond_0
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_1

    .line 659
    invoke-virtual {v4, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 660
    const/4 v12, 0x0

    .line 663
    :cond_1
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_6

    .line 664
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-lez v13, :cond_2

    .line 665
    const-string v13, ","

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 667
    :cond_2
    const/4 v0, 0x0

    .line 668
    .local v0, "count":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 669
    .local v6, "strBuilder":Ljava/lang/StringBuilder;
    new-instance v9, Ljava/util/StringTokenizer;

    const-string v13, ","

    invoke-direct {v9, v7, v13}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    .local v9, "token":Ljava/util/StringTokenizer;
    :goto_0
    :try_start_0
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 672
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    .line 673
    .local v10, "type":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 674
    const-string v13, ","

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 676
    :cond_3
    sget-object v13, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->SUB_CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v13, v10}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 677
    .local v3, "resId":I
    if-eqz v3, :cond_4

    .line 678
    iget-object v13, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 680
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 681
    goto :goto_0

    .line 682
    .end local v3    # "resId":I
    .end local v10    # "type":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 683
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 685
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 686
    const/4 v7, 0x0

    .line 688
    .end local v0    # "count":I
    .end local v6    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v9    # "token":Ljava/util/StringTokenizer;
    :cond_6
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_8

    .line 689
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-lez v13, :cond_7

    .line 690
    const-string v13, ","

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 692
    :cond_7
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 693
    const/4 v2, 0x0

    .line 697
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_9

    .line 698
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    .line 699
    .local v11, "userTag":Ljava/lang/String;
    const-string v13, ","

    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 700
    .local v8, "tags":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 701
    .local v5, "sb2":Ljava/lang/StringBuilder;
    array-length v13, v8

    const/4 v14, 0x2

    if-lt v13, v14, :cond_a

    .line 702
    const/4 v13, 0x0

    aget-object v13, v8, v13

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v8, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 706
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    .line 709
    .end local v2    # "personNamesType":Ljava/lang/String;
    .end local v4    # "sb":Ljava/lang/StringBuffer;
    .end local v5    # "sb2":Ljava/lang/StringBuilder;
    .end local v7    # "subCategoryType":Ljava/lang/String;
    .end local v8    # "tags":[Ljava/lang/String;
    .end local v11    # "userTag":Ljava/lang/String;
    .end local v12    # "userTagType":Ljava/lang/String;
    :cond_9
    return-void

    .line 704
    .restart local v2    # "personNamesType":Ljava/lang/String;
    .restart local v4    # "sb":Ljava/lang/StringBuffer;
    .restart local v5    # "sb2":Ljava/lang/StringBuilder;
    .restart local v7    # "subCategoryType":Ljava/lang/String;
    .restart local v8    # "tags":[Ljava/lang/String;
    .restart local v11    # "userTag":Ljava/lang/String;
    .restart local v12    # "userTagType":Ljava/lang/String;
    :cond_a
    const/4 v13, 0x0

    aget-object v13, v8, v13

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private startTTSTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "weather"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;
    .param p3, "date"    # Ljava/lang/String;
    .param p4, "face"    # Ljava/lang/String;
    .param p5, "category"    # Ljava/lang/String;
    .param p6, "userTags"    # Ljava/lang/String;

    .prologue
    .line 1211
    const-string v0, ""

    .line 1213
    .local v0, "TTSText":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0223

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1215
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0074

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1217
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1218
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0103

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1219
    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0222

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1221
    :cond_3
    if-eqz p5, :cond_4

    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e00a9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1223
    :cond_4
    if-eqz p6, :cond_5

    invoke-virtual {p6}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0235

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1226
    :cond_5
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    .line 1227
    return-void
.end method

.method private updateHelpDialog()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 829
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 830
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z

    .line 832
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->hideContextualTagHelpView(Z)V

    .line 833
    return-void
.end method


# virtual methods
.method public disableHelpDialog()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 236
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z

    .line 237
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->hideContextualTagHelpView(Z)V

    .line 238
    return-void
.end method

.method public hideContextualTagHelpView(Z)V
    .locals 2
    .param p1, "isClear"    # Z

    .prologue
    .line 416
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 417
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 418
    if-eqz p1, :cond_0

    .line 419
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public isContextualTagHelpOff()Z
    .locals 3

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const-string v1, "contextualTagHelpOff"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 821
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->initDisplaySize()V

    .line 823
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDisplayWidth:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_RIGHT_MARGIN:I

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    .line 824
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->onContextualTag()V

    .line 825
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->updateHelpDialog()V

    .line 826
    return-void
.end method

.method public onContextualTag()V
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1136
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v0, v0

    sget v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TEXT_COLOR:I

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v7

    .line 1137
    .local v7, "fm":Landroid/graphics/Paint$FontMetrics;
    iget v0, v7, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v1, v7, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    .line 1138
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    if-eqz v0, :cond_5

    .line 1139
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1140
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    sget v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TEXT_COLOR:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1143
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1144
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_FOR_DATE:I

    int-to-float v1, v1

    sget v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TEXT_COLOR:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1147
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1148
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    sget v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TEXT_COLOR:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v6

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1151
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1152
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    sget v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TEXT_COLOR:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v6

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1173
    :cond_3
    :goto_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoReader:Z

    if-nez v0, :cond_4

    .line 1174
    sget-object v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mWeather:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    sget-object v5, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->startTTSTag(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1175
    :cond_4
    return-void

    .line 1156
    :cond_5
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1157
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v6

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1160
    :cond_6
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1161
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v6

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1164
    :cond_7
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1165
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v6

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1168
    :cond_8
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1169
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    int-to-float v3, v3

    move v5, v6

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    goto :goto_0
.end method

.method public onItemPressed(FF)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 436
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getVisibility()I

    move-result v5

    if-ne v5, v10, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 439
    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 440
    .local v2, "contextualDateTagRect":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 441
    .local v3, "contextualLocationTagRect":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 442
    .local v1, "contextualCategoryTagRect":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 444
    .local v4, "contextualUserTagsRect":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v0

    .line 446
    .local v0, "checkedItem":I
    sget-object v5, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-eqz v5, :cond_2

    and-int/lit16 v5, v0, 0x1000

    const/16 v6, 0x1000

    if-ne v5, v6, :cond_2

    .line 447
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "contextualDateTagRect":Landroid/graphics/Rect;
    iget v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 451
    .restart local v2    # "contextualDateTagRect":Landroid/graphics/Rect;
    :cond_2
    sget-object v5, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-eqz v5, :cond_3

    and-int/lit16 v5, v0, 0x100

    const/16 v6, 0x100

    if-ne v5, v6, :cond_3

    .line 452
    new-instance v3, Landroid/graphics/Rect;

    .end local v3    # "contextualLocationTagRect":Landroid/graphics/Rect;
    iget v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionY:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionY:I

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v3, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 457
    .restart local v3    # "contextualLocationTagRect":Landroid/graphics/Rect;
    :cond_3
    sget-object v5, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-eqz v5, :cond_4

    and-int/lit8 v5, v0, 0x10

    const/16 v6, 0x10

    if-ne v5, v6, :cond_4

    .line 458
    new-instance v1, Landroid/graphics/Rect;

    .end local v1    # "contextualCategoryTagRect":Landroid/graphics/Rect;
    iget v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionY:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionY:I

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v1, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 462
    .restart local v1    # "contextualCategoryTagRect":Landroid/graphics/Rect;
    :cond_4
    sget-object v5, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    if-eqz v5, :cond_5

    and-int/lit8 v5, v0, 0x1

    if-ne v5, v10, :cond_5

    .line 463
    new-instance v4, Landroid/graphics/Rect;

    .end local v4    # "contextualUserTagsRect":Landroid/graphics/Rect;
    iget v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagPositionY:I

    iget v7, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagPositionY:I

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 468
    .restart local v4    # "contextualUserTagsRect":Landroid/graphics/Rect;
    :cond_5
    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-nez v5, :cond_6

    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-nez v5, :cond_6

    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-nez v5, :cond_6

    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 472
    :cond_6
    iget-boolean v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mActionBarShown:Z

    if-nez v5, :cond_0

    .line 473
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 475
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "MOREINFO_EVENT"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 842
    invoke-super/range {p0 .. p5}, Lcom/sec/android/gallery3d/ui/GLView;->onLayout(ZIIII)V

    .line 843
    return-void
.end method

.method public onMWLayoutChanged()V
    .locals 2

    .prologue
    .line 1202
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-nez v1, :cond_1

    .line 1208
    :cond_0
    :goto_0
    return-void

    .line 1204
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    .line 1205
    .local v0, "curZone":I
    :goto_1
    iget v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCurrentMultiZone:I

    if-eq v1, v0, :cond_0

    .line 1206
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->invalidate()V

    goto :goto_0

    .line 1204
    .end local v0    # "curZone":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->getZone()I

    move-result v0

    goto :goto_1
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 836
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->updateHelpDialog()V

    .line 837
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 29
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 847
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v25, v0

    if-nez v25, :cond_0

    .line 1100
    :goto_0
    return-void

    .line 851
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/ui/PositionController;->inSlidingAnimation()Z

    move-result v25

    if-nez v25, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/ui/PositionController;->isScrolling()Z

    move-result v25

    if-nez v25, :cond_1a

    .line 854
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getDisplaySize(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;

    move-result-object v7

    .line 856
    .local v7, "ds":Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v16

    .line 857
    .local v16, "locale":Ljava/util/Locale;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1b

    const/4 v13, 0x1

    .line 859
    .local v13, "isLayoutRtl":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ICON_LEFT_MARGIN:I

    move/from16 v17, v0

    .line 860
    .local v17, "positionX":I
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_VERTICAL_BOTTOM_MARGIN:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    move/from16 v26, v0

    sub-int v18, v25, v26

    .line 862
    .local v18, "positionY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v5

    .line 864
    .local v5, "bounds":Landroid/graphics/Rect;
    iget v14, v5, Landroid/graphics/Rect;->left:I

    .line 865
    .local v14, "left":I
    iget v0, v5, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    .line 868
    .local v20, "right":I
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    move/from16 v25, v0

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_1

    .line 869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v17, v0

    .line 870
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_BOTTOM_MARGIN:I

    move/from16 v26, v0

    sub-int v18, v25, v26

    .line 871
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->resizeContextualTagString(I)V

    .line 878
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v25

    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 879
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0d010f

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    sub-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v18, v0

    .line 881
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-nez v25, :cond_3

    .line 882
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_VERTICAL_LEFT_MARGIN:I

    move/from16 v17, v0

    .line 883
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v25

    move-object/from16 v0, v25

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1d

    .line 884
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v26, v0

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_LANDSCAPE_MAX_NEEDED:I

    move/from16 v27, v0

    move/from16 v0, v25

    move/from16 v1, v27

    if-le v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_LANDSCAPE:I

    move/from16 v25, v0

    :goto_2
    sub-int v18, v26, v25

    .line 893
    :cond_3
    :goto_3
    const/16 v22, 0x0

    .line 895
    .local v22, "textWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v25

    if-eqz v25, :cond_2b

    .line 897
    const/4 v11, 0x0

    .line 898
    .local v11, "i":I
    const/4 v10, 0x0

    .line 900
    .local v10, "faces":[Lcom/sec/android/gallery3d/data/Face;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    move/from16 v25, v0

    if-eqz v25, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v25, v0

    if-eqz v25, :cond_4

    .line 901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaces()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v10

    .line 904
    :cond_4
    if-eqz v10, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v25, v0

    if-eqz v25, :cond_20

    .line 905
    move-object v4, v10

    .local v4, "arr$":[Lcom/sec/android/gallery3d/data/Face;
    array-length v15, v4

    .local v15, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_4
    if-ge v12, v15, :cond_6

    aget-object v9, v4, v12

    .line 906
    .local v9, "face":Lcom/sec/android/gallery3d/data/Face;
    array-length v0, v10

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_5

    if-eqz v9, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v25, v0

    aget-object v25, v25, v11

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Lcom/sec/android/gallery3d/data/Face;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_1f

    .line 907
    :cond_5
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagFace:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 908
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setFace()V

    .line 909
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setUserTags(Ljava/lang/String;)V

    .line 910
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->onContextualTag()V

    .line 924
    .end local v4    # "arr$":[Lcom/sec/android/gallery3d/data/Face;
    .end local v9    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v12    # "i$":I
    .end local v15    # "len$":I
    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v6

    .line 925
    .local v6, "checkedItem":I
    sget-boolean v25, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v25, :cond_7

    .line 926
    or-int/lit16 v6, v6, 0x1000

    .line 929
    :cond_7
    :try_start_0
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    if-eqz v25, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_b

    and-int/lit8 v25, v6, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_b

    .line 930
    const/16 v23, 0x0

    .line 931
    .local v23, "userTagHeight":I
    const/16 v24, 0x0

    .line 932
    .local v24, "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_8

    .line 933
    if-eqz v13, :cond_21

    .line 934
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    move/from16 v25, v0

    sub-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v26

    sub-int v21, v25, v26

    .line 935
    .local v21, "rtlx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 936
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v23

    .line 937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v25, v0

    sub-int v25, v21, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v26

    sub-int v24, v25, v26

    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v26, v0

    sub-int v26, v23, v26

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 946
    .end local v21    # "rtlx":I
    :cond_8
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v22

    .line 947
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 948
    .local v19, "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_9

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_9
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagPositionY:I

    .line 950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_22

    and-int/lit8 v25, v6, 0x10

    const/16 v26, 0x10

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_22

    .line 951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v18, v18, v25

    .line 952
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 953
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_a

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_a
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionY:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 969
    .end local v23    # "userTagHeight":I
    .end local v24    # "x":I
    :cond_b
    :goto_7
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_f

    and-int/lit8 v25, v6, 0x10

    const/16 v26, 0x10

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_f

    .line 970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_c

    .line 971
    if-eqz v13, :cond_26

    .line 972
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    move/from16 v25, v0

    sub-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v26

    sub-int v21, v25, v26

    .line 973
    .restart local v21    # "rtlx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 974
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v25, v0

    sub-int v25, v21, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v26

    sub-int v24, v25, v26

    .line 975
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 983
    .end local v21    # "rtlx":I
    .end local v24    # "x":I
    :cond_c
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 984
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 985
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_d

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_d
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionY:I

    .line 987
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_27

    and-int/lit16 v0, v6, 0x100

    move/from16 v25, v0

    const/16 v26, 0x100

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_27

    .line 988
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v18, v18, v25

    .line 989
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 990
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_e

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_e
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionY:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1002
    :cond_f
    :goto_9
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_13

    and-int/lit16 v0, v6, 0x100

    move/from16 v25, v0

    const/16 v26, 0x100

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_13

    .line 1003
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_10

    .line 1004
    if-eqz v13, :cond_29

    .line 1005
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    move/from16 v25, v0

    sub-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v26

    sub-int v21, v25, v26

    .line 1006
    .restart local v21    # "rtlx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1007
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v25, v0

    sub-int v25, v21, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v26

    sub-int v24, v25, v26

    .line 1008
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1018
    .end local v21    # "rtlx":I
    .end local v24    # "x":I
    :cond_10
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 1019
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 1021
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_11

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_11
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionY:I

    .line 1024
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_13

    and-int/lit16 v0, v6, 0x1000

    move/from16 v25, v0

    const/16 v26, 0x1000

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_13

    .line 1025
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v18, v18, v25

    .line 1027
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 1029
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_12

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_12
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1039
    :cond_13
    :goto_b
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_16

    and-int/lit16 v0, v6, 0x1000

    move/from16 v25, v0

    const/16 v26, 0x1000

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_16

    .line 1040
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_14

    .line 1041
    if-eqz v13, :cond_2a

    .line 1042
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    move/from16 v25, v0

    sub-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v26

    sub-int v21, v25, v26

    .line 1043
    .restart local v21    # "rtlx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v25, v0

    sub-int v25, v21, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v26

    sub-int v24, v25, v26

    .line 1045
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1055
    .end local v21    # "rtlx":I
    .end local v24    # "x":I
    :cond_14
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 1056
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 1058
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_15

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_15
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 1065
    :cond_16
    :goto_d
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagPositionX:I

    .line 1091
    .end local v6    # "checkedItem":I
    .end local v10    # "faces":[Lcom/sec/android/gallery3d/data/Face;
    .end local v11    # "i":I
    :cond_17
    :goto_e
    if-lez v22, :cond_19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z

    move/from16 v25, v0

    if-nez v25, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    move/from16 v25, v0

    if-nez v25, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalVideo;

    move/from16 v25, v0

    if-nez v25, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move/from16 v25, v0

    if-eqz v25, :cond_19

    .line 1092
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0d014b

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    add-int v18, v18, v25

    .line 1093
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCAHandler:Landroid/os/Handler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCAHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    add-int v28, v17, v22

    add-int/lit8 v28, v28, 0x30

    move-object/from16 v0, v26

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1095
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-nez v25, :cond_1a

    .line 1096
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 1099
    .end local v5    # "bounds":Landroid/graphics/Rect;
    .end local v7    # "ds":Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;
    .end local v13    # "isLayoutRtl":Z
    .end local v14    # "left":I
    .end local v16    # "locale":Ljava/util/Locale;
    .end local v17    # "positionX":I
    .end local v18    # "positionY":I
    .end local v20    # "right":I
    .end local v22    # "textWidth":I
    :cond_1a
    invoke-super/range {p0 .. p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    goto/16 :goto_0

    .line 857
    .restart local v7    # "ds":Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;
    .restart local v16    # "locale":Ljava/util/Locale;
    :cond_1b
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 884
    .restart local v5    # "bounds":Landroid/graphics/Rect;
    .restart local v13    # "isLayoutRtl":Z
    .restart local v14    # "left":I
    .restart local v17    # "positionX":I
    .restart local v18    # "positionY":I
    .restart local v20    # "right":I
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_LANDSCAPE_MAX:I

    move/from16 v25, v0

    goto/16 :goto_2

    .line 888
    :cond_1d
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v26, v0

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_PORTRAIT_MAX_NEEDED:I

    move/from16 v27, v0

    move/from16 v0, v25

    move/from16 v1, v27

    if-le v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_PORTRAIT:I

    move/from16 v25, v0

    :goto_f
    sub-int v18, v26, v25

    goto/16 :goto_3

    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->MW_BOTTOM_MARGIN_PORTRAIT_MAX:I

    move/from16 v25, v0

    goto :goto_f

    .line 913
    .restart local v4    # "arr$":[Lcom/sec/android/gallery3d/data/Face;
    .restart local v9    # "face":Lcom/sec/android/gallery3d/data/Face;
    .restart local v10    # "faces":[Lcom/sec/android/gallery3d/data/Face;
    .restart local v11    # "i":I
    .restart local v12    # "i$":I
    .restart local v15    # "len$":I
    .restart local v22    # "textWidth":I
    :cond_1f
    add-int/lit8 v11, v11, 0x1

    .line 905
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    .line 915
    .end local v4    # "arr$":[Lcom/sec/android/gallery3d/data/Face;
    .end local v9    # "face":Lcom/sec/android/gallery3d/data/Face;
    .end local v12    # "i$":I
    .end local v15    # "len$":I
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    move-object/from16 v25, v0

    if-eqz v25, :cond_6

    if-nez v10, :cond_6

    .line 916
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagFace:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 917
    const/16 v25, 0x0

    sput-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    .line 918
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFaces:[Lcom/sec/android/gallery3d/data/Face;

    .line 919
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setUserTags(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 940
    .restart local v6    # "checkedItem":I
    .restart local v23    # "userTagHeight":I
    .restart local v24    # "x":I
    :cond_21
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v23

    .line 942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v25

    add-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v26, v0

    add-int v24, v25, v26

    .line 943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v26, v0

    sub-int v26, v23, v26

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_6

    .line 964
    .end local v23    # "userTagHeight":I
    .end local v24    # "x":I
    :catch_0
    move-exception v8

    .line 965
    .local v8, "e":Ljava/lang/Exception;
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 954
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v23    # "userTagHeight":I
    .restart local v24    # "x":I
    :cond_22
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_24

    and-int/lit16 v0, v6, 0x100

    move/from16 v25, v0

    const/16 v26, 0x100

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_24

    .line 955
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v18, v18, v25

    .line 956
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 957
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_23

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_23
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionY:I

    goto/16 :goto_7

    .line 958
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_b

    and-int/lit16 v0, v6, 0x1000

    move/from16 v25, v0

    const/16 v26, 0x1000

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_b

    .line 959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v18, v18, v25

    .line 960
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 961
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_25

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_25
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_7

    .line 978
    .end local v23    # "userTagHeight":I
    .end local v24    # "x":I
    :cond_26
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 979
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v25

    add-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v26, v0

    add-int v24, v25, v26

    .line 980
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_8

    .line 997
    .end local v24    # "x":I
    :catch_1
    move-exception v8

    .line 998
    .restart local v8    # "e":Ljava/lang/Exception;
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    .line 991
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_27
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_f

    and-int/lit16 v0, v6, 0x1000

    move/from16 v25, v0

    const/16 v26, 0x1000

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_f

    .line 992
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_ITEM_HEIGHT:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_BG_VERTICAL_MARGIN:I

    move/from16 v26, v0

    add-int v25, v25, v26

    sub-int v18, v18, v25

    .line 993
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->h:I

    move/from16 v25, v0

    sub-int v25, v25, v18

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f800000    # 1.0f

    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->screenRatio:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    add-int v19, v18, v25

    .line 994
    .restart local v19    # "positionY_temp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v25

    if-eqz v25, :cond_28

    move/from16 v19, v18

    .end local v19    # "positionY_temp":I
    :cond_28
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_9

    .line 1011
    :cond_29
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1012
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v25

    add-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v26, v0

    add-int v24, v25, v26

    .line 1014
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_a

    .line 1034
    .end local v24    # "x":I
    :catch_2
    move-exception v8

    .line 1035
    .restart local v8    # "e":Ljava/lang/Exception;
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    .line 1048
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2a
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1049
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v25

    add-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v26, v0

    add-int v24, v25, v26

    .line 1051
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_c

    .line 1061
    .end local v24    # "x":I
    :catch_3
    move-exception v8

    .line 1062
    .restart local v8    # "e":Ljava/lang/Exception;
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_d

    .line 1067
    .end local v6    # "checkedItem":I
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "faces":[Lcom/sec/android/gallery3d/data/Face;
    .end local v11    # "i":I
    :cond_2b
    sget-boolean v25, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v25, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagEnable(Landroid/content/Context;)Z

    move-result v25

    if-nez v25, :cond_17

    .line 1070
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    if-eqz v25, :cond_17

    .line 1071
    if-eqz v13, :cond_2c

    .line 1072
    iget v0, v7, Lcom/sec/android/gallery3d/ui/ContextualTagView$DisplaySize;->w:I

    move/from16 v25, v0

    sub-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v26

    sub-int v21, v25, v26

    .line 1073
    .restart local v21    # "rtlx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1074
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v25, v0

    sub-int v25, v21, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v26

    sub-int v24, v25, v26

    .line 1075
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1084
    .end local v21    # "rtlx":I
    :goto_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->getWidth()I

    move-result v22

    .line 1085
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4

    goto/16 :goto_e

    .line 1087
    .end local v24    # "x":I
    :catch_4
    move-exception v8

    .line 1088
    .restart local v8    # "e":Ljava/lang/Exception;
    sget-object v25, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_e

    .line 1078
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2c
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 1079
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v25

    add-int v25, v25, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_HORIZONTAL_LEFT_MARGIN:I

    move/from16 v26, v0

    add-int v24, v25, v26

    .line 1081
    .restart local v24    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mTextHeight:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    div-int/lit8 v26, v26, 0x2

    add-int v26, v26, v18

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_10
.end method

.method public resizeContextualTagString(I)V
    .locals 7
    .param p1, "positionX"    # I

    .prologue
    const/4 v2, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v6, 0xf0

    .line 1178
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 1179
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    if-eqz v0, :cond_2

    .line 1180
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1181
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE_FOR_DATE:I

    int-to-float v1, v1

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    sub-int/2addr v3, p1

    int-to-float v3, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1184
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1185
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    sub-int/2addr v3, p1

    int-to-float v3, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1198
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 1199
    return-void

    .line 1189
    :cond_2
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1190
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    sub-int/2addr v3, p1

    int-to-float v3, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 1193
    :cond_3
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1194
    sget-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_SIZE:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    sub-int/2addr v3, p1

    int-to-float v3, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/StringTexture;->newInstance(Ljava/lang/String;FIFZI)Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    goto :goto_0
.end method

.method public setContextualTagHelpOff(Z)V
    .locals 2
    .param p1, "contextualTagHelpOff"    # Z

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const-string v1, "contextualTagHelpOff"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 433
    return-void
.end method

.method public setContextualTagView(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "actionBarShown"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 241
    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->hideContextualTagHelpView(Z)V

    .line 243
    iput-boolean p2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mActionBarShown:Z

    .line 244
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 245
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFilePath:Ljava/lang/String;

    .line 247
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mAccWeatherPositionX:I

    .line 248
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mAccWeatherPositionY:I

    .line 249
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionX:I

    .line 250
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagPositionY:I

    .line 251
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionX:I

    .line 252
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationTagPositionY:I

    .line 253
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionX:I

    .line 254
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagPositionY:I

    .line 255
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagPositionX:I

    .line 256
    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagPositionY:I

    .line 258
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagWeather:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 259
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagDate:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 260
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagFace:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 261
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagLocation:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 262
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagCategory:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 263
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagUserTags:Lcom/sec/android/gallery3d/glrenderer/StringTexture;

    .line 265
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDisplayHeight:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDisplayWidth:I

    if-le v0, v1, :cond_0

    .line 266
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDisplayWidth:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_RIGHT_MARGIN:I

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    .line 267
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mIsVertical:Z

    .line 273
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->isContextualTagHelpOff()Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setIcon(Landroid/content/Context;)V

    .line 276
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mWeather:Ljava/lang/String;

    .line 277
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDate:Ljava/lang/String;

    .line 278
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocation:Ljava/lang/String;

    .line 279
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mFace:Ljava/lang/String;

    .line 280
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategory:Ljava/lang/String;

    .line 281
    const-string v0, ""

    sput-object v0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTags:Ljava/lang/String;

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->onContextualTag()V

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->show()V

    .line 295
    :goto_1
    return-void

    .line 269
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDisplayWidth:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->CA_TEXT_RIGHT_MARGIN:I

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMaxTagArea:I

    .line 270
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mIsVertical:Z

    goto :goto_0

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagTask:Landroid/os/AsyncTask;

    if-nez v0, :cond_2

    .line 286
    new-instance v0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagTask:Landroid/os/AsyncTask;

    .line 287
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagTask:Landroid/os/AsyncTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 293
    :goto_2
    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setVisibility(I)V

    goto :goto_1

    .line 289
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 290
    new-instance v0, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$ContextualTagDataTask;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagTask:Landroid/os/AsyncTask;

    .line 291
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagTask:Landroid/os/AsyncTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2
.end method

.method public setIcon(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 222
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mHelpDialogDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->isContextualTagHelpOff()Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 224
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 225
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 226
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 233
    :goto_0
    return-void

    .line 228
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02013b

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mDateTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 229
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02013a

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCategoryTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 230
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02013d

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mUserTagsTagIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 231
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    const v1, 0x7f02013c

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mLocationIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_0
.end method

.method public setPositionController(Lcom/sec/android/gallery3d/ui/PositionController;)V
    .locals 0
    .param p1, "positionController"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 817
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 818
    return-void
.end method

.method public setShowBarState(Z)V
    .locals 1
    .param p1, "showBar"    # Z

    .prologue
    .line 485
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mActionBarShown:Z

    .line 486
    if-eqz p1, :cond_0

    .line 487
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->hide(Z)V

    .line 491
    :goto_0
    return-void

    .line 489
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->show()V

    goto :goto_0
.end method

.method public show()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 513
    :goto_0
    return-void

    .line 498
    :cond_0
    :try_start_0
    new-instance v0, Lcom/sec/android/gallery3d/anim/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;-><init>(FF)V

    .line 499
    .local v0, "animation":Lcom/sec/android/gallery3d/anim/AlphaAnimation;
    const/16 v2, 0x12c

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/anim/AlphaAnimation;->setDuration(I)V

    .line 500
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->startAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    .end local v0    # "animation":Lcom/sec/android/gallery3d/anim/AlphaAnimation;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCAHandler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    .line 506
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCAHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 507
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCAHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 509
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mCAHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x9c4

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 512
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setVisibility(I)V

    goto :goto_0

    .line 501
    :catch_0
    move-exception v1

    .line 502
    .local v1, "e":Ljava/lang/IllegalStateException;
    sget-object v2, Lcom/sec/android/gallery3d/ui/ContextualTagView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalStateException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public showContextualTagHelpView(II)V
    .locals 14
    .param p1, "positionX"    # I
    .param p2, "positionY"    # I

    .prologue
    .line 298
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->isShowing()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 302
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    const-string v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 305
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const/4 v6, 0x0

    .line 307
    .local v6, "layout":Landroid/view/View;
    const v9, 0x7f03003d

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 309
    const v9, 0x7f0f00a7

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 311
    .local v0, "ContextualTagGuide":Landroid/widget/TextView;
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0265

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0e021f

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    const v9, 0x7f0f00a6

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 323
    .local v8, "userTagsTextView":Landroid/widget/TextView;
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, -0x1000000

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 324
    const v9, 0x7f0f00a4

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 325
    .local v7, "locationTextView":Landroid/widget/TextView;
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, -0x1000000

    invoke-virtual {v7, v9, v10, v11, v12}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 326
    const v9, 0x7f0f00a5

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 327
    .local v2, "categoryTextView":Landroid/widget/TextView;
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, -0x1000000

    invoke-virtual {v2, v9, v10, v11, v12}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 328
    const v9, 0x7f0f00a3

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 329
    .local v4, "dateTextView":Landroid/widget/TextView;
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    if-eqz v9, :cond_3

    .line 330
    const v9, -0xf0f10

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 337
    :goto_1
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 338
    const v9, 0x7f0e021f

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 339
    const v9, 0x7f0f001d

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 341
    .local v3, "checkBox":Landroid/widget/CheckBox;
    iget-boolean v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mIsNotShowChecked:Z

    if-nez v9, :cond_4

    .line 342
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 347
    :goto_2
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 348
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 349
    new-instance v9, Lcom/sec/android/gallery3d/ui/ContextualTagView$2;

    invoke-direct {v9, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$2;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 355
    new-instance v9, Lcom/sec/android/gallery3d/ui/ContextualTagView$3;

    invoke-direct {v9, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$3;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 362
    const v9, 0x7f0e00db

    new-instance v10, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;

    invoke-direct {v10, p0, v3}, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 374
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v10, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;

    invoke-direct {v10, p0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView$5;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {v9, v10}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 397
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->isFinishing()Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    if-eqz v9, :cond_2

    .line 398
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->show()V

    .line 401
    :cond_2
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    if-eqz v9, :cond_0

    .line 402
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 403
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;

    new-instance v10, Lcom/sec/android/gallery3d/ui/ContextualTagView$6;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/ui/ContextualTagView$6;-><init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0

    .line 335
    .end local v3    # "checkBox":Landroid/widget/CheckBox;
    :cond_3
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, -0x1000000

    invoke-virtual {v4, v9, v10, v11, v12}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto :goto_1

    .line 344
    .restart local v3    # "checkBox":Landroid/widget/CheckBox;
    :cond_4
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2
.end method
