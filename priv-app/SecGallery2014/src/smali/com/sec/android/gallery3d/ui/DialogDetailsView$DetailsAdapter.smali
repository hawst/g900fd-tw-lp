.class Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;
.super Landroid/widget/BaseAdapter;
.source "DialogDetailsView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;
.implements Lcom/sec/android/gallery3d/ui/DetailsHelper$ResolutionResolvingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/DialogDetailsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetailsAdapter"
.end annotation


# instance fields
.field private final mDecimalFormat:Ljava/text/DecimalFormat;

.field private final mDefaultLocale:Ljava/util/Locale;

.field private mHeightIndex:I

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationIndex:I

.field private mWidthIndex:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/DialogDetailsView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/DialogDetailsView;Lcom/sec/android/gallery3d/data/MediaDetails;)V
    .locals 4
    .param p2, "details"    # Lcom/sec/android/gallery3d/data/MediaDetails;

    .prologue
    const/4 v3, -0x1

    .line 129
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->this$0:Lcom/sec/android/gallery3d/ui/DialogDetailsView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 124
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDefaultLocale:Ljava/util/Locale;

    .line 125
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, ".####"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDecimalFormat:Ljava/text/DecimalFormat;

    .line 126
    iput v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mWidthIndex:I

    .line 127
    iput v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mHeightIndex:I

    .line 130
    # getter for: Lcom/sec/android/gallery3d/ui/DialogDetailsView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView;->access$200(Lcom/sec/android/gallery3d/ui/DialogDetailsView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    .line 131
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaDetails;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    .line 132
    iput v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mLocationIndex:I

    .line 133
    invoke-direct {p0, v0, p2}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->setDetails(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V

    .line 134
    return-void
.end method

.method private setDetails(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "details"    # Lcom/sec/android/gallery3d/data/MediaDetails;

    .prologue
    .line 137
    const/4 v13, 0x1

    .line 138
    .local v13, "resolutionIsValid":Z
    const/4 v12, 0x0

    .line 139
    .local v12, "path":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaDetails;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 140
    .local v4, "detail":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const/16 v16, 0x0

    .line 141
    .local v16, "value":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    sparse-switch v18, :sswitch_data_0

    .line 231
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    .line 233
    .local v17, "valueObj":Ljava/lang/Object;
    if-nez v17, :cond_7

    .line 237
    const-string v16, ""

    .line 244
    .end local v17    # "valueObj":Ljava/lang/Object;
    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 245
    .local v10, "key":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/MediaDetails;->hasUnit(I)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 246
    const-string v18, "%s: %s %s"

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->getDetailsName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v16, v19, v20

    const/16 v20, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/MediaDetails;->getUnit(I)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 252
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    .end local v10    # "key":I
    :sswitch_0
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [D

    move-object/from16 v11, v18

    check-cast v11, [D

    .line 144
    .local v11, "latlng":[D
    if-nez v11, :cond_1

    .line 145
    const-string v18, "DialogDetailsView"

    const-string v19, "setDetails() : latlng is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 148
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mLocationIndex:I

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->this$0:Lcom/sec/android/gallery3d/ui/DialogDetailsView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/DialogDetailsView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/DialogDetailsView;->access$200(Lcom/sec/android/gallery3d/ui/DialogDetailsView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-static {v0, v11, v1}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->resolveAddress(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;[DLcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;

    move-result-object v16

    .line 150
    goto/16 :goto_1

    .line 153
    .end local v11    # "latlng":[D
    :sswitch_1
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v16

    .line 155
    goto/16 :goto_1

    .line 158
    :sswitch_2
    const-string v18, "1"

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    const v18, 0x7f0e0083

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 161
    :goto_3
    goto/16 :goto_1

    .line 158
    :cond_2
    const v18, 0x7f0e0090

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto :goto_3

    .line 164
    :sswitch_3
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;

    .line 168
    .local v5, "flash":Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;->isFlashFired()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 169
    const v18, 0x7f0e0091

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_1

    .line 171
    :cond_3
    const v18, 0x7f0e0092

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 173
    goto/16 :goto_1

    .line 176
    .end local v5    # "flash":Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;
    :sswitch_4
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "value":Ljava/lang/String;
    check-cast v16, Ljava/lang/String;

    .line 177
    .restart local v16    # "value":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    .line 178
    .local v14, "time":D
    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    cmpg-double v18, v14, v18

    if-gez v18, :cond_4

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDefaultLocale:Ljava/util/Locale;

    move-object/from16 v18, v0

    const-string v19, "%d/%d"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-wide/high16 v22, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    div-double v24, v24, v14

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v18 .. v20}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_1

    .line 182
    :cond_4
    double-to-int v9, v14

    .line 183
    .local v9, "integer":I
    int-to-double v0, v9

    move-wide/from16 v18, v0

    sub-double v14, v14, v18

    .line 184
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\'\'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 185
    const-wide v18, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpl-double v18, v14, v18

    if-lez v18, :cond_0

    .line 186
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDefaultLocale:Ljava/util/Locale;

    move-object/from16 v19, v0

    const-string v20, " %d/%d"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-wide/high16 v24, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v26, v26, v14

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v19 .. v21}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_1

    .line 193
    .end local v9    # "integer":I
    .end local v14    # "time":D
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mWidthIndex:I

    .line 194
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "0"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 195
    const v18, 0x7f0e00ff

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 196
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 198
    :cond_5
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->toLocalInteger(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 200
    goto/16 :goto_1

    .line 202
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mHeightIndex:I

    .line 203
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "0"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 204
    const v18, 0x7f0e00ff

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 205
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 207
    :cond_6
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->toLocalInteger(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 209
    goto/16 :goto_1

    .line 217
    :sswitch_7
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 218
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 219
    goto/16 :goto_1

    .line 221
    :sswitch_8
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->toLocalNumber(I)Ljava/lang/String;

    move-result-object v16

    .line 222
    goto/16 :goto_1

    .line 224
    :sswitch_9
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 225
    .local v6, "focalLength":D
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->toLocalNumber(D)Ljava/lang/String;

    move-result-object v16

    .line 226
    goto/16 :goto_1

    .line 228
    .end local v6    # "focalLength":D
    :sswitch_a
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->toLocalInteger(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 229
    goto/16 :goto_1

    .line 241
    .restart local v17    # "valueObj":Ljava/lang/Object;
    :cond_7
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_1

    .line 249
    .end local v17    # "valueObj":Ljava/lang/Object;
    .restart local v10    # "key":I
    :cond_8
    const-string v18, "%s: %s"

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->getDetailsName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v16, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    .line 254
    .end local v4    # "detail":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    .end local v10    # "key":I
    .end local v16    # "value":Ljava/lang/String;
    :cond_9
    if-nez v13, :cond_a

    .line 255
    move-object/from16 v0, p0

    invoke-static {v12, v0}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->resolveResolution(Ljava/lang/String;Lcom/sec/android/gallery3d/ui/DetailsHelper$ResolutionResolvingListener;)V

    .line 257
    :cond_a
    return-void

    .line 141
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xc -> :sswitch_5
        0xd -> :sswitch_6
        0xe -> :sswitch_1
        0x10 -> :sswitch_a
        0x66 -> :sswitch_3
        0x67 -> :sswitch_9
        0x68 -> :sswitch_2
        0x6b -> :sswitch_4
        0x6c -> :sswitch_8
        0xc8 -> :sswitch_7
    .end sparse-switch
.end method

.method private toLocalInteger(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p1, "valueObj"    # Ljava/lang/Object;

    .prologue
    .line 324
    instance-of v1, p1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 325
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "valueObj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->toLocalNumber(I)Ljava/lang/String;

    move-result-object v0

    .line 334
    :goto_0
    return-object v0

    .line 327
    .restart local p1    # "valueObj":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->toLocalNumber(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 330
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private toLocalNumber(D)Ljava/lang/String;
    .locals 1
    .param p1, "n"    # D

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDecimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toLocalNumber(I)Ljava/lang/String;
    .locals 5
    .param p1, "n"    # I

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDefaultLocale:Ljava/util/Locale;

    const-string v1, "%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->this$0:Lcom/sec/android/gallery3d/ui/DialogDetailsView;

    # getter for: Lcom/sec/android/gallery3d/ui/DialogDetailsView;->mDetails:Lcom/sec/android/gallery3d/data/MediaDetails;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/DialogDetailsView;->access$300(Lcom/sec/android/gallery3d/ui/DialogDetailsView;)Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 281
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 287
    if-nez p2, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->this$0:Lcom/sec/android/gallery3d/ui/DialogDetailsView;

    # getter for: Lcom/sec/android/gallery3d/ui/DialogDetailsView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/DialogDetailsView;->access$200(Lcom/sec/android/gallery3d/ui/DialogDetailsView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03004a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 293
    .local v0, "tv":Landroid/widget/TextView;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    return-object v0

    .end local v0    # "tv":Landroid/widget/TextView;
    :cond_0
    move-object v0, p2

    .line 291
    check-cast v0, Landroid/widget/TextView;

    .restart local v0    # "tv":Landroid/widget/TextView;
    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method

.method public onAddressAvailable(Ljava/lang/String;)V
    .locals 2
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mLocationIndex:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->notifyDataSetChanged()V

    .line 301
    return-void
.end method

.method public onResolutionAvailable(II)V
    .locals 10
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 305
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->this$0:Lcom/sec/android/gallery3d/ui/DialogDetailsView;

    # getter for: Lcom/sec/android/gallery3d/ui/DialogDetailsView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/DialogDetailsView;->access$200(Lcom/sec/android/gallery3d/ui/DialogDetailsView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    .line 308
    .local v0, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDefaultLocale:Ljava/util/Locale;

    const-string v4, "%s: %d"

    new-array v5, v9, [Ljava/lang/Object;

    const/16 v6, 0xc

    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->getDetailsName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 311
    .local v2, "widthString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mDefaultLocale:Ljava/util/Locale;

    const-string v4, "%s: %d"

    new-array v5, v9, [Ljava/lang/Object;

    const/16 v6, 0xd

    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->getDetailsName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 314
    .local v1, "heightString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mWidthIndex:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 315
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mItems:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->mHeightIndex:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/DialogDetailsView$DetailsAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
