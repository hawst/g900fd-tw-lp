.class public Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
.super Ljava/lang/Object;
.source "NearbyClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$7;,
        Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;,
        Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;,
        Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;
    }
.end annotation


# static fields
.field private static final CACHE_QUEUE_MAX:I = 0x1e

.field private static final FULL_HD_HEIGHT:I = 0x438

.field private static final FULL_HD_WIDTH:I = 0x780

.field public static final NEARBY_CACHE_PREFIX:Ljava/lang/String; = "nearby_cache"

.field private static final NOT_PLAYED_YET:I

.field private static final TAG:Ljava/lang/String;

.field private static mCachedFileList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mPendedPlayerId:Ljava/lang/String;


# instance fields
.field private mCacheFilePrefix:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

.field private mDeviceListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mDisplayItemHeight:I

.field private mDisplayItemWidth:I

.field private mIsViewerShowRequested:Z

.field private mLastPlayedActivityId:I

.field private mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

.field private mPendedMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mPendedMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayerStateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

.field private mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mPrepareMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

.field private mUseAsf:Z

.field private mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

.field private mViewerOnPlaying:Z

.field private mWeakActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    .line 86
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCachedFileList:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nearbyContext"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCacheFilePrefix:Ljava/lang/String;

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    .line 114
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerStateListeners:Ljava/util/ArrayList;

    .line 130
    iput v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mLastPlayedActivityId:I

    .line 134
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mUseAsf:Z

    .line 174
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;

    .line 218
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 636
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 178
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;

    .line 179
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    .line 180
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->setCacheFilePrefix()V

    .line 181
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/ImageViewer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/ImageViewer;)Lcom/samsung/android/allshare/media/ImageViewer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/ImageViewer;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mIsViewerShowRequested:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/samsung/android/allshare/Item;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->prepareItem(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/samsung/android/allshare/Item;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->updateDisplayItemInfo(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mNearbyContext:Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mUseAsf:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mUseAsf:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshDeviceList()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerIcon()V

    return-void
.end method

.method private copyExifOrientation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "origFilePath"    # Ljava/lang/String;
    .param p2, "destFilePath"    # Ljava/lang/String;

    .prologue
    .line 1018
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1019
    .local v2, "origExif":Landroid/media/ExifInterface;
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, p2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1020
    .local v0, "destExif":Landroid/media/ExifInterface;
    const-string v3, "Orientation"

    const-string v4, "Orientation"

    invoke-virtual {v2, v4}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    invoke-virtual {v0}, Landroid/media/ExifInterface;->saveAttributes()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1026
    .end local v0    # "destExif":Landroid/media/ExifInterface;
    .end local v2    # "origExif":Landroid/media/ExifInterface;
    :goto_0
    return-void

    .line 1023
    :catch_0
    move-exception v1

    .line 1024
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private isOnlyOriginalImageSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 848
    const-wide/16 v0, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    return v0
.end method

.method private playImage(Lcom/samsung/android/allshare/media/ImageViewer;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "viewer"    # Lcom/samsung/android/allshare/media/ImageViewer;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 653
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayInfoQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;

    invoke-direct {v1, p1, p2, p3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayInfo;-><init>(Lcom/samsung/android/allshare/media/ImageViewer;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 654
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 655
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v1, "new player thread, run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    .line 657
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;->start()V

    .line 668
    :cond_1
    :goto_0
    return-void

    .line 660
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mIsViewerShowRequested:Z

    if-nez v0, :cond_1

    .line 664
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    monitor-enter v1

    .line 665
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerThread:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$PlayerThread;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 666
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private prepareItem(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/samsung/android/allshare/Item;
    .locals 12
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v9, 0x0

    .line 773
    if-nez p1, :cond_1

    move-object v4, v9

    .line 843
    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return-object v4

    .line 776
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 777
    .local v2, "filePath":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v5

    .line 778
    .local v5, "mimeType":Ljava/lang/String;
    const/4 v4, 0x0

    .line 780
    .local v4, "itemToShow":Lcom/samsung/android/allshare/Item;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 781
    instance-of v10, p1, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-eqz v10, :cond_2

    .line 782
    check-cast p1, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getItemToShow()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    goto :goto_0

    .line 783
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    instance-of v10, p1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v10, :cond_5

    .line 784
    check-cast p1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getSourceUrl()Ljava/lang/String;

    move-result-object v7

    .line 785
    .local v7, "sourceUrl":Ljava/lang/String;
    if-nez v7, :cond_3

    move-object v8, v9

    .line 786
    .local v8, "uri":Landroid/net/Uri;
    :goto_1
    if-nez v8, :cond_4

    .line 787
    sget-object v10, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v11, "SNS Image has no url for this image!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v9

    .line 788
    goto :goto_0

    .line 785
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    goto :goto_1

    .line 790
    .restart local v8    # "uri":Landroid/net/Uri;
    :cond_4
    new-instance v9, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    invoke-direct {v9, v8, v5}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    .line 791
    goto :goto_0

    .end local v7    # "sourceUrl":Ljava/lang/String;
    .end local v8    # "uri":Landroid/net/Uri;
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    instance-of v10, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v10, :cond_6

    .line 792
    check-cast p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getPhotoEntry()Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    move-result-object v1

    .line 793
    .local v1, "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    new-instance v9, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    iget-object v10, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    iget-object v11, v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    .line 795
    goto :goto_0

    .end local v1    # "entry":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    instance-of v10, p1, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v10, :cond_7

    .line 796
    check-cast p1, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getNearbyItem()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    goto :goto_0

    .line 798
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v8

    .line 799
    .restart local v8    # "uri":Landroid/net/Uri;
    if-nez v8, :cond_8

    move-object v4, v9

    .line 800
    goto :goto_0

    .line 802
    :cond_8
    const-string v9, "content"

    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 803
    new-instance v9, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v5}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    goto/16 :goto_0

    .line 804
    :cond_9
    const-string v9, "http"

    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 805
    new-instance v9, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    invoke-direct {v9, v8, v5}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    goto/16 :goto_0

    .line 809
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_a
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->isOnlyOriginalImageSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 810
    new-instance v9, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    goto/16 :goto_0

    .line 814
    :cond_b
    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCacheFilePrefix:Ljava/lang/String;

    if-nez v9, :cond_c

    .line 815
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->setCacheFilePrefix()V

    .line 817
    :cond_c
    const/4 v0, 0x0

    .line 819
    .local v0, "cachePath":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCacheFilePrefix:Ljava/lang/String;

    if-eqz v9, :cond_d

    .line 820
    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCacheFilePrefix:Ljava/lang/String;

    invoke-static {v9, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->createCacheFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 823
    :cond_d
    const/16 v9, 0x780

    const/16 v10, 0x438

    invoke-virtual {p0, v2, v0, v9, v10}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->compressForDmr(Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v3

    .line 825
    .local v3, "isCompressed":Z
    if-eqz v3, :cond_f

    move-object v6, v0

    .line 827
    .local v6, "selectedFilePath":Ljava/lang/String;
    :goto_2
    if-eqz v3, :cond_e

    const-string v9, "image/jpeg"

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 828
    invoke-direct {p0, v2, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->copyExifOrientation(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    :cond_e
    new-instance v9, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-direct {v9, v6, v5}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v4

    .line 834
    if-eqz v3, :cond_0

    .line 835
    sget-object v9, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCachedFileList:Ljava/util/LinkedList;

    invoke-virtual {v9, v0}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    .line 836
    sget-object v9, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCachedFileList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v9

    const/16 v10, 0x1e

    if-le v9, v10, :cond_0

    .line 837
    sget-object v9, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCachedFileList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->deletePreviousCache(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v6    # "selectedFilePath":Ljava/lang/String;
    :cond_f
    move-object v6, v2

    .line 825
    goto :goto_2
.end method

.method private refreshChangePlayerIcon()V
    .locals 4

    .prologue
    .line 502
    sget-object v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v3, "refresh change player icon"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 505
    sget-object v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v3, "no listener to update"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    :cond_0
    return-void

    .line 509
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 510
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;->onPlayerStateChanged()V

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private refreshDeviceList()V
    .locals 9

    .prologue
    .line 347
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v7, "refresh device list"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-nez v6, :cond_0

    .line 350
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v7, "ignore refresh for the device list"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :goto_0
    return-void

    .line 354
    :cond_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->refresh()V

    .line 356
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 357
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v7, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    sget-object v8, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceDomain;Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v2

    .line 359
    .local v2, "deviceProviderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 361
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v7, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    sget-object v8, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceDomain;Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    .line 363
    .local v1, "deviceImageViewerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 365
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 366
    .local v3, "gallery":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    if-nez v3, :cond_1

    .line 367
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v7, "AbstractGalleryActivity instance is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 370
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 371
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/nearby/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 373
    .local v5, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    if-eqz v5, :cond_2

    .line 374
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Update device in NearbyDevice instance. device id= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    invoke-virtual {v5, v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->update(Lcom/samsung/android/allshare/Device;)V

    goto :goto_1

    .line 379
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v5    # "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    :cond_3
    sget-object v6, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "device count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Provider : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ImageViewer : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private resizeDownTo(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "dstWidth"    # I
    .param p3, "dstHeight"    # I

    .prologue
    const/4 v11, 0x0

    .line 989
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 990
    .local v6, "srcWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 992
    .local v5, "srcHeight":I
    int-to-float v9, p2

    int-to-float v10, v6

    div-float v8, v9, v10

    .line 993
    .local v8, "widthScale":F
    int-to-float v9, p3

    int-to-float v10, v5

    div-float v2, v9, v10

    .line 995
    .local v2, "heightScale":F
    invoke-static {v8, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 997
    .local v4, "scale":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    .line 999
    .local v1, "config":Landroid/graphics/Bitmap$Config;
    if-nez v1, :cond_0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1001
    :cond_0
    int-to-float v9, v6

    mul-float/2addr v9, v4

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    int-to-float v10, v5

    mul-float/2addr v10, v4

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    invoke-static {v9, v10, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1003
    .local v7, "target":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1004
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v4, v4}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1005
    new-instance v3, Landroid/graphics/Paint;

    const/4 v9, 0x2

    invoke-direct {v3, v9}, Landroid/graphics/Paint;-><init>(I)V

    .line 1006
    .local v3, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p1, v11, v11, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1007
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1009
    return-object v7
.end method

.method private setCacheFilePrefix()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "nearby_cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mCacheFilePrefix:Ljava/lang/String;

    goto :goto_0
.end method

.method public static setPendedPlayer(Landroid/content/Intent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 137
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v2, "setPendedPlayer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 139
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    const-string v1, "DMRUDN"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedPlayerId:Ljava/lang/String;

    .line 144
    invoke-static {p0}, Lcom/sec/android/gallery3d/sconnect/SConnectUtil;->isSConnectIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    invoke-static {p0}, Lcom/sec/android/gallery3d/sconnect/SConnectUtil;->getDeviceId(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedPlayerId:Ljava/lang/String;

    goto :goto_0
.end method

.method private stopPlayer()V
    .locals 3

    .prologue
    .line 881
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "stop player called"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    if-nez v1, :cond_1

    .line 889
    :cond_0
    :goto_0
    return-void

    .line 885
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/ImageViewer;->getViewerState()Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    move-result-object v0

    .line 886
    .local v0, "imageViewerState":Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;
    sget-object v1, Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/samsung/android/allshare/media/ImageViewer$ImageViewerState;

    if-eq v0, v1, :cond_0

    .line 887
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/ImageViewer;->stop()V

    goto :goto_0
.end method

.method private updateDisplayItemInfo(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 852
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    .line 853
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDisplayItemWidth:I

    .line 854
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDisplayItemHeight:I

    .line 856
    iget v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDisplayItemWidth:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDisplayItemHeight:I

    if-nez v4, :cond_2

    .line 857
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 859
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/16 v4, 0xc

    :try_start_0
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 860
    .local v3, "w":Ljava/lang/String;
    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 861
    .local v2, "h":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 862
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDisplayItemWidth:I

    .line 863
    :cond_1
    if-eqz v2, :cond_2

    .line 864
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDisplayItemHeight:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 871
    .end local v0    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .end local v2    # "h":Ljava/lang/String;
    .end local v3    # "w":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 865
    .restart local v0    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    :catch_0
    move-exception v1

    .line 866
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "unsupported file"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addPlayerStateListener(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;

    .prologue
    .line 917
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 918
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 920
    :cond_0
    return-void
.end method

.method public changePlayer(ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 8
    .param p1, "activityId"    # I
    .param p2, "imageViewerID"    # Ljava/lang/String;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 517
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v5, "changePlayer"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    if-nez p2, :cond_0

    .line 520
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v5, ""

    new-instance v6, Ljava/lang/Exception;

    const-string v7, "cannot play image through image viewer!"

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 632
    :goto_0
    return-void

    .line 525
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/media/ImageViewer;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;

    const v5, 0x7f0e012f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 527
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->disconnectWithPlayDevice()V

    goto :goto_0

    .line 531
    :cond_3
    sget-object v4, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p0, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    .line 532
    .local v1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-nez v1, :cond_4

    .line 533
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v5, "no player to change"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 537
    :cond_4
    const/4 v3, 0x0

    .line 539
    .local v3, "viewer":Lcom/samsung/android/allshare/media/ImageViewer;
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 540
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object v3, v0

    .line 541
    check-cast v3, Lcom/samsung/android/allshare/media/ImageViewer;

    .line 546
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :cond_6
    if-nez v3, :cond_7

    .line 547
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No such device : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 551
    :cond_7
    new-instance v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$5;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$5;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/media/ImageViewer;->setEventListener(Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerEventListener;)V

    .line 574
    new-instance v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;

    invoke-direct {v4, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$6;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/media/ImageViewer;->setResponseListener(Lcom/samsung/android/allshare/media/ImageViewer$IImageViewerResponseListener;)V

    .line 630
    iput p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mLastPlayedActivityId:I

    .line 631
    invoke-direct {p0, v3, p4, p3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playImage(Lcom/samsung/android/allshare/media/ImageViewer;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0
.end method

.method public compressForDmr(Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 11
    .param p1, "origFilePath"    # Ljava/lang/String;
    .param p2, "destFilePath"    # Ljava/lang/String;
    .param p3, "destWidth"    # I
    .param p4, "destHeight"    # I

    .prologue
    .line 937
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 938
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 939
    const/4 v6, 0x1

    .line 985
    :goto_0
    return v6

    .line 942
    :cond_0
    const/4 v6, 0x0

    .line 945
    .local v6, "result":Z
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 946
    .local v3, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v7, 0x1

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 947
    invoke-static {p1, v3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 948
    const/4 v7, 0x0

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 949
    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v8, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v9, 0x780

    invoke-static {v7, v8, v9}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(III)I

    move-result v7

    iput v7, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 953
    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lt p3, v7, :cond_1

    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-lt p4, v7, :cond_1

    .line 955
    const/4 v6, 0x0

    goto :goto_0

    .line 958
    :cond_1
    const/4 v0, 0x0

    .line 960
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {p1, v3}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 965
    :goto_1
    if-nez v0, :cond_2

    .line 966
    sget-object v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v8, "cannnot decode file for compress"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    const/4 v6, 0x0

    goto :goto_0

    .line 961
    :catch_0
    move-exception v1

    .line 962
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 963
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_1

    .line 970
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :cond_2
    invoke-direct {p0, v0, p3, p4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->resizeDownTo(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 973
    const/4 v4, 0x0

    .line 975
    .local v4, "out":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 976
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 977
    .end local v4    # "out":Ljava/io/OutputStream;
    .local v5, "out":Ljava/io/OutputStream;
    :try_start_2
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x5a

    invoke-virtual {v0, v7, v8, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v6

    .line 978
    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 982
    sget-object v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "resized to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " x "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v4, v5

    .line 984
    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    goto/16 :goto_0

    .line 979
    :catch_1
    move-exception v1

    .line 980
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 982
    sget-object v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "resized to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " x "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 982
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    :goto_3
    sget-object v8, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "resized to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " x "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v7

    .line 982
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    goto :goto_3

    .line 979
    .end local v4    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    :catch_2
    move-exception v1

    move-object v4, v5

    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/OutputStream;
    goto :goto_2
.end method

.method public disconnectWithPlayDevice()V
    .locals 1

    .prologue
    .line 874
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->stopPlayer()V

    .line 875
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    .line 876
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    .line 877
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerIcon()V

    .line 878
    return-void
.end method

.method public download(Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "serverName"    # Ljava/lang/String;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1029
    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v2, :cond_0

    .line 1038
    :goto_0
    return-void

    :cond_0
    move-object v1, p2

    .line 1032
    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .line 1034
    .local v1, "nearbyItem":Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1035
    .local v0, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getItemToShow()Lcom/samsung/android/allshare/Item;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1037
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDownloader()Lcom/samsung/android/allshare/extension/SECDownloader;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Lcom/samsung/android/allshare/extension/SECDownloader;->Download(Ljava/lang/String;Ljava/util/ArrayList;)Z

    goto :goto_0
.end method

.method public download(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "serverName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1041
    .local p2, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1043
    .local v1, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 1044
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1045
    .local v2, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v5, :cond_0

    .line 1043
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v4, v2

    .line 1048
    check-cast v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    .line 1049
    .local v4, "nearbyImage":Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;->getItemToShow()Lcom/samsung/android/allshare/Item;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1052
    .end local v2    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v4    # "nearbyImage":Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDownloader()Lcom/samsung/android/allshare/extension/SECDownloader;

    move-result-object v5

    invoke-virtual {v5, p1, v1}, Lcom/samsung/android/allshare/extension/SECDownloader;->Download(Ljava/lang/String;Ljava/util/ArrayList;)Z

    .line 1053
    return-void
.end method

.method public getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "type"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/Device$DeviceType;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 401
    .local v2, "typedDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 402
    const/4 v0, 0x0

    .line 403
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 404
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 405
    .restart local v0    # "device":Lcom/samsung/android/allshare/Device;
    if-nez v0, :cond_1

    .line 403
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 408
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 409
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 413
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v1    # "i":I
    :cond_2
    invoke-static {v2}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    return-object v3
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 195
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 196
    sget-object v4, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v5, "getDevice ID arg is null"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 206
    :goto_0
    return-object v0

    .line 199
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    monitor-enter v4

    .line 200
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 201
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 202
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 203
    monitor-exit v4

    goto :goto_0

    .line 205
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v2    # "n":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 200
    .restart local v0    # "device":Lcom/samsung/android/allshare/Device;
    .restart local v2    # "n":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 205
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :cond_2
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v3

    .line 206
    goto :goto_0
.end method

.method public getDevices()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDevices:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDisplayDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "nic"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 419
    .local v6, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;>;"
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v8

    .line 420
    .local v8, "imageViewerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-nez v8, :cond_1

    .line 438
    :cond_0
    return-object v6

    .line 424
    :cond_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/Device;

    .line 425
    .local v5, "device":Lcom/samsung/android/allshare/Device;
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;-><init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Object;)V

    .line 427
    .local v0, "data":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/ImageViewer;->getID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/ImageViewer;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 430
    :cond_3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 431
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ignore because not in the same NIC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 435
    :cond_4
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getDisplayDeviceListCount()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 442
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-nez v2, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v1

    .line 445
    :cond_1
    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 446
    .local v0, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_0
.end method

.method public getOnPlayingPlayer()Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    .locals 6

    .prologue
    .line 909
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    if-nez v0, :cond_1

    .line 910
    :cond_0
    const/4 v0, 0x0

    .line 912
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/ImageViewer;->getIcon()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/ImageViewer;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/media/ImageViewer;->getID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;-><init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public isOnPlaying()Z
    .locals 1

    .prologue
    .line 905
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    return v0
.end method

.method public playImageContinuously(ILcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "activityId"    # I
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 640
    iget v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mLastPlayedActivityId:I

    if-eq v0, p1, :cond_1

    .line 641
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->isOnPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->disconnectWithPlayDevice()V

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 646
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-direct {p0, v0, p2, p3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playImage(Lcom/samsung/android/allshare/media/ImageViewer;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method

.method public playPendedPlayer()V
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v0, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mLastPlayedActivityId:I

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playPendedPlayer(ILcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method

.method public playPendedPlayer(ILcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "activityId"    # I
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 150
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v1, "playPendedPlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedPlayerId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-nez v0, :cond_1

    .line 155
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v1, "playing is pended. service provider null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 157
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 158
    iput p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mLastPlayedActivityId:I

    goto :goto_0

    .line 160
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v1, "play pended item"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedPlayerId:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, p3, p2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->changePlayer(ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 162
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPendedPlayerId:Ljava/lang/String;

    goto :goto_0
.end method

.method public prepareNextImage(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 690
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 691
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 692
    return-void
.end method

.method public prepareNextImage(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 677
    .local p2, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->prepareNextImage(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-nez v0, :cond_0

    .line 893
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->startAllShareFrameworkService()V

    .line 898
    :goto_0
    return-void

    .line 897
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshDeviceList()V

    goto :goto_0
.end method

.method public refreshChangePlayerDialogRef()V
    .locals 2

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refreshChangePlayerIcon()V

    .line 212
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;

    .line 213
    .local v0, "listener":Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;
    if-eqz v0, :cond_0

    .line 214
    invoke-interface {v0}, Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;->onDeviceChanged()V

    .line 216
    :cond_0
    return-void
.end method

.method public removePlayerStateListener(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;

    .prologue
    .line 923
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mPlayerStateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 924
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 901
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;

    .line 902
    return-void
.end method

.method public setOnDeviceChangedListener(Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;

    .prologue
    .line 191
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;

    .line 192
    return-void
.end method

.method public showDialogDisconnected(Ljava/lang/String;)V
    .locals 4
    .param p1, "deviceID"    # Ljava/lang/String;

    .prologue
    .line 481
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "showDialogDisconnected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "    viewer was playing? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/ImageViewer;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewer:Lcom/samsung/android/allshare/media/ImageViewer;

    .line 485
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mViewerOnPlaying:Z

    .line 487
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 488
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 489
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e013a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e012a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$4;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$4;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 499
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    return-void
.end method

.method public showDmsDisconnectedDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "deviceID"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 450
    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v2

    .line 451
    .local v2, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-eqz p1, :cond_0

    if-nez v2, :cond_1

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    const/4 v4, 0x1

    .line 455
    .local v4, "isDeviceDisconnected":Z
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Device;

    .line 456
    .local v1, "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 457
    const/4 v4, 0x0

    .line 462
    .end local v1    # "device":Lcom/samsung/android/allshare/Device;
    :cond_3
    if-eqz v4, :cond_0

    .line 463
    sget-object v5, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "showDmsDisconnected!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mWeakActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 465
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_0

    .line 466
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x1010355

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0e013a

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0129

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0e00db

    new-instance v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$3;

    invoke-direct {v7, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$3;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public startAllShareFrameworkService()V
    .locals 4

    .prologue
    .line 291
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v2, "create service provider : "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$2;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;)V

    const-string v3, "com.samsung.android.allshare.media"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 344
    :goto_0
    return-void

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->TAG:Ljava/lang/String;

    const-string v2, "No AllShare Class found! ignore allshare"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 341
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 342
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    goto :goto_0
.end method

.method public stopAllShareFrameworkService()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 384
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 387
    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-eqz v0, :cond_1

    .line 390
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mUseAsf:Z

    .line 391
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 392
    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 394
    :cond_1
    return-void
.end method
