.class Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;
.super Ljava/lang/Object;
.source "GalleryAppImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initializeFaceTag()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 219
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 220
    .local v0, "t":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    invoke-static {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->access$000(Lcom/sec/android/gallery3d/app/GalleryAppImpl;)Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/GalleryAppImpl$1;->this$0:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->syncAllContactInfoList(Landroid/content/Context;)V

    .line 221
    const-string/jumbo v2, "syncContacts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sync time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    return-void
.end method
