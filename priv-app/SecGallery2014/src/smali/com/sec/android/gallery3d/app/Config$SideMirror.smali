.class public Lcom/sec/android/gallery3d/app/Config$SideMirror;
.super Ljava/lang/Object;
.source "Config.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SideMirror"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/android/gallery3d/app/Config$SideMirror;


# instance fields
.field public final sidemirrorBarSize:I

.field public final sidemirrorBgWidth:I

.field public final sidemirrorBgWidthLand:I

.field public final sidemirrorBottomMargin:I

.field public final sidemirrorContentSize:I

.field public final sidemirrorGripSize:I

.field public final sidemirrorGripWidth:I

.field public final sidemirrorMidMargin:I

.field public final sidemirrorThumbHeight:I

.field public final sidemirrorThumbWidth:I

.field public final sidemirrorTopMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 95
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d0117

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorTopMargin:I

    .line 96
    const v1, 0x7f0d010a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorMidMargin:I

    .line 97
    const v1, 0x7f0d010b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBottomMargin:I

    .line 98
    const v1, 0x7f0d0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorThumbWidth:I

    .line 99
    const v1, 0x7f0d011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorThumbHeight:I

    .line 100
    const v1, 0x7f0d010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorContentSize:I

    .line 101
    const v1, 0x7f0d0110

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorGripSize:I

    .line 102
    const v1, 0x7f0d0111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBarSize:I

    .line 103
    const v1, 0x7f0d0112

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorGripWidth:I

    .line 104
    const v1, 0x7f0d011b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBgWidth:I

    .line 105
    const v1, 0x7f0d011c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBgWidthLand:I

    .line 106
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$SideMirror;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const-class v1, Lcom/sec/android/gallery3d/app/Config$SideMirror;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sInstance:Lcom/sec/android/gallery3d/app/Config$SideMirror;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/sec/android/gallery3d/app/Config$SideMirror;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/Config$SideMirror;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sInstance:Lcom/sec/android/gallery3d/app/Config$SideMirror;

    .line 90
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sInstance:Lcom/sec/android/gallery3d/app/Config$SideMirror;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
