.class public Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;
.super Ljava/lang/Object;
.source "DecodeTempBufferPool.java"


# static fields
.field private static mBuffer1:[B

.field private static mBuffer2:[B

.field private static mBuffer3:[B

.field private static mBuffer4:[B


# instance fields
.field private mAllocList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mFreeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x4000

    .line 24
    new-array v0, v1, [B

    sput-object v0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer1:[B

    .line 25
    new-array v0, v1, [B

    sput-object v0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer2:[B

    .line 26
    new-array v0, v1, [B

    sput-object v0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer3:[B

    .line 27
    new-array v0, v1, [B

    sput-object v0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer4:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mFreeList:Ljava/util/LinkedList;

    .line 30
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mAllocList:Ljava/util/LinkedList;

    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mFreeList:Ljava/util/LinkedList;

    sget-object v1, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer1:[B

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mFreeList:Ljava/util/LinkedList;

    sget-object v1, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer2:[B

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 35
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mFreeList:Ljava/util/LinkedList;

    sget-object v1, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer3:[B

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mFreeList:Ljava/util/LinkedList;

    sget-object v1, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mBuffer4:[B

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method


# virtual methods
.method public freeBuffer([B)V
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mAllocList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mAllocList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 49
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mAllocList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 50
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mFreeList:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_0
    return-void

    .line 47
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getBuffer()[B
    .locals 2

    .prologue
    .line 40
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mFreeList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 41
    .local v0, "buffer":[B
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/DecodeTempBufferPool;->mAllocList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 43
    return-object v0
.end method
