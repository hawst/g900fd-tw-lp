.class public abstract Lcom/sec/android/gallery3d/data/LocalMediaItem;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "LocalMediaItem.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LocalMediaItem"


# instance fields
.field public bucketId:I

.field public caption:Ljava/lang/String;

.field public dateAddedInSec:J

.field public dateModifiedInSec:J

.field public dateTakenInMs:J

.field public faces:[Lcom/sec/android/gallery3d/data/Face;

.field public facesRect:Landroid/graphics/RectF;

.field public filePath:Ljava/lang/String;

.field public fileSize:J

.field public groupId:J

.field public height:I

.field public id:I

.field public latitude:D

.field public longitude:D

.field protected final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mCameraItem:Z

.field private mCounters:I

.field protected mHiddenSourcePath:Ljava/lang/String;

.field private mIsDeleted:Z

.field public mIsGroupItem:Z

.field protected mIsSdCardFile:Z

.field private mSrcHiddenFileName:Ljava/lang/String;

.field public mimeType:Ljava/lang/String;

.field public resolution:Ljava/lang/String;

.field public sphericalMosaic:I

.field public width:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;JLcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "version"    # J
    .param p4, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 51
    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    .line 52
    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    .line 64
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->facesRect:Landroid/graphics/RectF;

    .line 72
    iput v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCounters:I

    .line 74
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCameraItem:Z

    .line 75
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsSdCardFile:Z

    .line 77
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mHiddenSourcePath:Ljava/lang/String;

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsDeleted:Z

    .line 84
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 85
    return-void
.end method

.method private checkSrcNameExistance(Ljava/lang/String;I)V
    .locals 9
    .param p1, "fileFullPath"    # Ljava/lang/String;
    .param p2, "count"    # I

    .prologue
    .line 414
    iput p2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCounters:I

    .line 415
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 417
    .local v6, "srcItemFile":Ljava/io/File;
    const/4 v0, 0x0

    .line 418
    .local v0, "filePath":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 419
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 420
    .local v4, "orgFileName":Ljava/lang/String;
    const/4 v7, 0x0

    const-string v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 421
    .local v5, "orgFileNameWitoutExt":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCounters:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 422
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 425
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "newFileFullPath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 428
    .local v2, "newFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 429
    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 430
    iget v7, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCounters:I

    add-int/lit8 v1, v7, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCounters:I

    .line 431
    .local v1, "incrCount":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getHiddenSourcePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7, v1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->checkSrcNameExistance(Ljava/lang/String;I)V

    .line 437
    .end local v1    # "incrCount":I
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mSrcHiddenFileName:Ljava/lang/String;

    goto :goto_0
.end method

.method private moveProcess(Ljava/io/File;Ljava/io/File;)Z
    .locals 4
    .param p1, "srcAlbum"    # Ljava/io/File;
    .param p2, "srcItem"    # Ljava/io/File;

    .prologue
    const/4 v2, 0x0

    .line 440
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 441
    .local v0, "dstItem":Ljava/io/File;
    invoke-virtual {v0, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->delete()V

    .line 445
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 446
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 447
    .local v1, "fileName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 448
    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;

    .line 451
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->copyTo(Ljava/lang/String;)V

    .line 452
    const/4 v2, 0x1

    .line 454
    .end local v1    # "fileName":Ljava/lang/String;
    :cond_1
    return v2
.end method


# virtual methods
.method protected addHiddenInfo(Landroid/content/ContentValues;)Z
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 284
    const/4 v0, 0x0

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 219
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-static {v3, v4, p1, p2}, Lcom/sec/samsung/gallery/util/FileUtil;->copyFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "newFilePath":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 221
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 222
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    .line 223
    if-eqz p2, :cond_0

    .line 224
    const/4 v3, 0x0

    const-string v4, "."

    invoke-virtual {p2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->copyTo(Ljava/lang/String;)V

    .line 230
    const/4 v2, 0x1

    .end local v1    # "newFilePath":Ljava/lang/String;
    :goto_0
    return v2

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method protected copyTo(Ljava/lang/String;)V
    .locals 1
    .param p1, "dstFolderPath"    # Ljava/lang/String;

    .prologue
    .line 259
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected deleteHiddenItem(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/lang/String;)V
    .locals 10
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "table"    # Ljava/lang/String;

    .prologue
    .line 459
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mHiddenSourcePath:Ljava/lang/String;

    if-nez v6, :cond_1

    .line 491
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    .line 465
    .local v0, "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mHiddenSourcePath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    .line 466
    .local v4, "sourceAlbumPath":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    .line 468
    .local v2, "hiddenAlbumPath":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 469
    const-string v6, "_id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget v9, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->id:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v0, p2, v6, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 473
    iget v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->bucketId:I

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbum(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 475
    iget v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->bucketId:I

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHiddenAlbumInfo(I)Z

    .line 477
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 478
    .local v5, "sourceFolder":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 479
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 481
    .end local v5    # "sourceFolder":Ljava/io/File;
    :cond_2
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mHiddenSourcePath:Ljava/lang/String;

    .line 485
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 486
    .local v3, "hiddenFolder":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 487
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 488
    .end local v0    # "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .end local v2    # "hiddenAlbumPath":Ljava/lang/String;
    .end local v3    # "hiddenFolder":Ljava/io/File;
    .end local v4    # "sourceAlbumPath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 489
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->bucketId:I

    return v0
.end method

.method protected getContentValues()Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 280
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    return-object v0
.end method

.method public getDateInMs()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->dateTakenInMs:J

    return-wide v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 12

    .prologue
    .line 127
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v2

    .line 128
    .local v2, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/16 v5, 0xc8

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 129
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 130
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v4

    .line 131
    .local v4, "formater":Ljava/text/DateFormat;
    const/4 v5, 0x4

    new-instance v6, Ljava/util/Date;

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->dateModifiedInSec:J

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 133
    const/16 v5, 0xc

    iget v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->width:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 134
    const/16 v5, 0xd

    iget v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->height:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 136
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 137
    const/16 v5, 0x9

    const/4 v6, 0x2

    new-array v6, v6, [D

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    aput-wide v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    aput-wide v8, v6, v7

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 139
    const/16 v5, 0xa

    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 140
    const/16 v5, 0xb

    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 147
    :goto_0
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCIMFolderMerge:Z

    if-eqz v5, :cond_3

    .line 148
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->DCIM_PATH:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 149
    const/4 v5, 0x5

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDCIMName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 163
    :cond_0
    :goto_1
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->fileSize:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    const/16 v5, 0xe

    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->fileSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 164
    :cond_1
    return-object v2

    .line 142
    :cond_2
    const/16 v5, 0x9

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 143
    const/16 v5, 0xa

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 144
    const/16 v5, 0xb

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_0

    .line 152
    :cond_3
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "bucketPath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    const-string v6, "/"

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "albumName":Ljava/lang/String;
    const/4 v5, 0x5

    invoke-virtual {v2, v5, v0}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 155
    .end local v0    # "albumName":Ljava/lang/String;
    .end local v1    # "bucketPath":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 156
    .local v3, "e":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-virtual {v3}, Ljava/lang/StringIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1

    .line 157
    .end local v3    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :catch_1
    move-exception v3

    .line 158
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 213
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->groupId:J

    return-wide v0
.end method

.method public getHiddenSourcePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mHiddenSourcePath:Ljava/lang/String;

    return-object v0
.end method

.method public getItemType()I
    .locals 1

    .prologue
    .line 197
    const/4 v0, -0x1

    return v0
.end method

.method public getLatLong([D)V
    .locals 4
    .param p1, "latLong"    # [D

    .prologue
    .line 99
    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    aput-wide v2, p1, v0

    .line 100
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    aput-wide v2, p1, v0

    .line 101
    return-void
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getModifiedDateInSec()J
    .locals 2

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->dateModifiedInSec:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->fileSize:J

    return-wide v0
.end method

.method public final hide(Ljava/lang/String;)Z
    .locals 12
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 289
    if-nez p1, :cond_0

    .line 290
    sget-object p1, Lcom/sec/android/gallery3d/data/HiddenSource;->ISLAND_HIDE_PATH:Ljava/lang/String;

    .line 293
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 294
    .local v1, "dstAlbum":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    .line 295
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 298
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    .line 299
    .local v6, "srcFilePath":Ljava/lang/String;
    const-string v9, "."

    invoke-virtual {v6, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_2

    .line 300
    const-string v9, "LocalMediaItem"

    const-string v10, "hide() failed because: srcFilePath.lastIndexOf(\".\") < 0"

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :goto_0
    return v8

    .line 303
    :cond_2
    const-string v9, "."

    invoke-virtual {v6, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 304
    .local v3, "ext":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 305
    .local v5, "srcFile":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v1, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 306
    .local v2, "dstFile":Ljava/io/File;
    const/4 v0, 0x1

    .line 307
    .local v0, "count":I
    :goto_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 308
    new-instance v2, Ljava/io/File;

    .end local v2    # "dstFile":Ljava/io/File;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v0}, Lcom/sec/android/gallery3d/common/Utils;->addPostfix(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v1, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 309
    .restart local v2    # "dstFile":Ljava/io/File;
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 311
    :cond_3
    invoke-virtual {v5, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->delete()V

    .line 313
    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    .line 314
    .local v4, "srcAlbum":Ljava/io/File;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getContentValues()Landroid/content/ContentValues;

    move-result-object v7

    .line 315
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "_data"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    if-eqz v4, :cond_4

    .line 317
    const-string v8, "album_name"

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_4
    const-string/jumbo v8, "source_album"

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v8, "hidden_album"

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->addHiddenInfo(Landroid/content/ContentValues;)Z

    move-result v8

    goto/16 :goto_0

    .line 323
    .end local v4    # "srcAlbum":Ljava/io/File;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_5
    const-string v9, "LocalMediaItem"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "hide() is faild because: renameTo() faild; srcFile="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "; dstFile="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public isCameraItem()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCameraItem:Z

    return v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsDeleted:Z

    return v0
.end method

.method public isHiddenItem()Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mHiddenSourcePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHideBlockedItem()Z
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    const/4 v0, 0x1

    .line 276
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isItemRenamed(Lcom/sec/android/gallery3d/util/UpdateHelper;Landroid/database/Cursor;Z)Z
    .locals 6
    .param p1, "uh"    # Lcom/sec/android/gallery3d/util/UpdateHelper;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isVideo"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x1

    .line 494
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v1

    .line 495
    .local v1, "updated":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;

    if-eqz p3, :cond_0

    :cond_0
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;

    .line 497
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v0

    .line 498
    .local v0, "captionUpdated":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    if-eqz p3, :cond_1

    :cond_1
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 500
    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 504
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isSdCardFile()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsSdCardFile:Z

    return v0
.end method

.method public final move(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 235
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 237
    .local v2, "srcFileDirectory":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 255
    :goto_0
    return v3

    .line 240
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-static {v4, v5, p1, p2}, Lcom/sec/samsung/gallery/util/FileUtil;->copyFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "newFilePath":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 242
    iput-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 243
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    .line 244
    if-eqz p2, :cond_1

    .line 245
    const/4 v4, 0x0

    const-string v5, "."

    invoke-virtual {p2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->copyTo(Ljava/lang/String;)V

    .line 252
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->delete()V

    .line 255
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 247
    .end local v1    # "newFilePath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public final moveToSecretbox(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 17
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 330
    if-nez p1, :cond_0

    .line 331
    sget-object v13, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    if-eqz v13, :cond_2

    .line 332
    sget-object p1, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    .line 338
    :cond_0
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 339
    .local v3, "dstAlbum":Ljava/io/File;
    const/4 v9, 0x0

    .line 340
    .local v9, "oldFilePath":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_1

    .line 341
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 343
    :cond_1
    if-nez p2, :cond_4

    .line 344
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v12

    .line 345
    .local v12, "srcFilePath":Ljava/lang/String;
    const-string v13, "."

    invoke-virtual {v12, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 346
    .local v7, "ext":Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 347
    .local v10, "srcFile":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 348
    .local v5, "dstFileName":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v3, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 349
    .local v4, "dstFile":Ljava/io/File;
    const/4 v2, 0x1

    .line 350
    .local v2, "count":I
    :goto_0
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 351
    new-instance v4, Ljava/io/File;

    .end local v4    # "dstFile":Ljava/io/File;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14, v2}, Lcom/sec/android/gallery3d/common/Utils;->addNamePostfix(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v3, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 352
    .restart local v4    # "dstFile":Ljava/io/File;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 334
    .end local v2    # "count":I
    .end local v3    # "dstAlbum":Ljava/io/File;
    .end local v4    # "dstFile":Ljava/io/File;
    .end local v5    # "dstFileName":Ljava/lang/String;
    .end local v7    # "ext":Ljava/lang/String;
    .end local v9    # "oldFilePath":Ljava/lang/String;
    .end local v10    # "srcFile":Ljava/io/File;
    .end local v12    # "srcFilePath":Ljava/lang/String;
    :cond_2
    const/4 v13, 0x0

    .line 383
    :goto_1
    return v13

    .line 354
    .restart local v2    # "count":I
    .restart local v3    # "dstAlbum":Ljava/io/File;
    .restart local v4    # "dstFile":Ljava/io/File;
    .restart local v5    # "dstFileName":Ljava/lang/String;
    .restart local v7    # "ext":Ljava/lang/String;
    .restart local v9    # "oldFilePath":Ljava/lang/String;
    .restart local v10    # "srcFile":Ljava/io/File;
    .restart local v12    # "srcFilePath":Ljava/lang/String;
    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    .line 357
    .end local v2    # "count":I
    .end local v4    # "dstFile":Ljava/io/File;
    .end local v5    # "dstFileName":Ljava/lang/String;
    .end local v7    # "ext":Ljava/lang/String;
    .end local v10    # "srcFile":Ljava/io/File;
    .end local v12    # "srcFilePath":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v15

    add-int/lit8 v15, v15, 0x1

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 360
    .local v11, "srcFileDirectory":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 361
    const/4 v13, 0x0

    goto :goto_1

    .line 363
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v13}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v13, v14, v0, v1}, Lcom/sec/samsung/gallery/util/FileUtil;->moveFileForPrivate(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 364
    .local v8, "newFilePath":Ljava/lang/String;
    if-eqz v8, :cond_6

    .line 365
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 366
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 367
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    .line 368
    if-eqz p2, :cond_6

    .line 369
    const/4 v13, 0x0

    const-string v14, "."

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    :cond_6
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 376
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->copyTo(Ljava/lang/String;)V

    .line 377
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 380
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_8

    .line 381
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->delete()V

    .line 383
    :cond_8
    const/4 v13, 0x1

    goto :goto_1

    .line 371
    .end local v8    # "newFilePath":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 372
    .local v6, "e":Ljava/io/IOException;
    const/4 v13, 0x0

    goto :goto_1
.end method

.method public setCameraItem(Z)V
    .locals 0
    .param p1, "isCameraItem"    # Z

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCameraItem:Z

    .line 265
    return-void
.end method

.method public setHiddenSourcePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mHiddenSourcePath:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public setLatLong([D)V
    .locals 2
    .param p1, "latLong"    # [D

    .prologue
    .line 105
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->latitude:D

    .line 106
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->longitude:D

    .line 107
    return-void
.end method

.method public final show()Z
    .locals 6

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isHiddenItem()Z

    move-result v4

    if-nez v4, :cond_0

    .line 389
    const/4 v4, 0x1

    .line 409
    :goto_0
    return v4

    .line 392
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 393
    .local v0, "dstItem":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getHiddenSourcePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 394
    .local v3, "srcItem":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 395
    .local v2, "srcAlbum":Ljava/io/File;
    if-eqz v2, :cond_2

    .line 396
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 397
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 403
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getHiddenSourcePath()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCounters:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mCounters:I

    invoke-direct {p0, v4, v5}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->checkSrcNameExistance(Ljava/lang/String;I)V

    .line 405
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mSrcHiddenFileName:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 406
    .local v1, "modifiedSrcItemName":Ljava/io/File;
    invoke-direct {p0, v2, v1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->moveProcess(Ljava/io/File;Ljava/io/File;)Z

    move-result v4

    goto :goto_0

    .line 400
    .end local v1    # "modifiedSrcItemName":Ljava/io/File;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 409
    :cond_3
    invoke-direct {p0, v2, v3}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->moveProcess(Ljava/io/File;Ljava/io/File;)Z

    move-result v4

    goto :goto_0
.end method

.method public updateContent(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateFromCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mDataVersion:J

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->updateFaceDataVersion()V

    .line 120
    :cond_0
    return-void
.end method

.method protected updateFaceDataVersion()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method protected abstract updateFromCursor(Landroid/database/Cursor;)Z
.end method
