.class public Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaItem;
.source "SLinkImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;
    }
.end annotation


# static fields
.field private static final PROXY_WAIT_RETRY:I = 0x2

.field private static final PROXY_WAIT_TIME:J = 0x7d0L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;IJLcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "deviceId"    # I
    .param p3, "objectId"    # J
    .param p5, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 44
    invoke-direct {p0, p1, p5}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 46
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iput p2, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_id:I

    .line 48
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->source_id:Ljava/lang/String;

    .line 50
    invoke-interface {p5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mContentResolver:Landroid/content/ContentResolver;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;)Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    return-object v0
.end method


# virtual methods
.method public delete()V
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->deleteFile(Landroid/content/Context;J)V

    .line 62
    return-void
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_url:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDateInMs()J
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->date_taken:J

    return-wide v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 6

    .prologue
    .line 234
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 235
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->date_taken:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 238
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->width:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 239
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 240
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 241
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 244
    return-object v0
.end method

.method public getDeviceId()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_id:I

    return v0
.end method

.method public getDeviceType()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 96
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->height:I

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x2

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    return-object v0
.end method

.method public getNearbyItem()Lcom/samsung/android/allshare/Item;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 149
    sget-object v7, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    const-string v8, "getNearbyUri()"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media;->getImageUriBatch(Landroid/content/ContentResolver;J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;

    move-result-object v5

    .line 154
    .local v5, "uris":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;
    if-nez v5, :cond_1

    .line 155
    sget-object v7, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    const-string v8, "Image Uri Batch is Null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_0
    :goto_0
    return-object v6

    .line 159
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->getLocalUri()Landroid/net/Uri;

    move-result-object v3

    .line 160
    .local v3, "localUri":Landroid/net/Uri;
    if-eqz v3, :cond_2

    .line 161
    sget-object v6, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "localUri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    new-instance v6, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v8, v8, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v6

    goto :goto_0

    .line 165
    :cond_2
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->getSameAccessPointUri()Landroid/net/Uri;

    move-result-object v4

    .line 166
    .local v4, "sameAccessPointUri":Landroid/net/Uri;
    if-eqz v4, :cond_3

    .line 167
    sget-object v6, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sameAccessPointUri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    new-instance v6, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v7, v7, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    invoke-direct {v6, v4, v7}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v6

    goto :goto_0

    .line 171
    :cond_3
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v7, 0x2

    if-ge v2, v7, :cond_0

    .line 172
    sget-object v7, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "try to get proxy uri"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Media$ImagesUriBatch;->getHttpProxyUri()Landroid/net/Uri;

    move-result-object v1

    .line 174
    .local v1, "httpProxyUri":Landroid/net/Uri;
    if-eqz v1, :cond_4

    .line 175
    sget-object v6, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "httpProxyUri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    new-instance v6, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v7, v7, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    invoke-direct {v6, v1, v7}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v6

    goto/16 :goto_0

    .line 179
    :cond_4
    :try_start_0
    sget-object v7, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "wait until S Link proxy is initialized: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const-wide/16 v8, 0x7d0

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->rotation:I

    return v0
.end method

.method public getSlinkObjectId()J
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->source_id:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->source_id:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_url:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 143
    const-wide v0, 0x200100008044062cL

    return-wide v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Thumbnails;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->width:I

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 8
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getDateInMs()J

    move-result-wide v4

    invoke-static {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v7

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage$SLinkImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/slink/SLinkImage;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;JII)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateContent(Landroid/database/Cursor;)Z
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 114
    if-nez p1, :cond_0

    .line 115
    const/4 v2, 0x0

    .line 130
    :goto_0
    return v2

    .line 118
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;-><init>()V

    .line 119
    .local v0, "newEntry":Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->setProperty(Ljava/lang/Object;)V

    .line 121
    new-instance v1, Lcom/sec/android/gallery3d/util/UpdateHelper;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/util/UpdateHelper;-><init>()V

    .line 122
    .local v1, "uh":Lcom/sec/android/gallery3d/util/UpdateHelper;
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->title:Ljava/lang/String;

    .line 123
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_url:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_url:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_url:Ljava/lang/String;

    .line 124
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->width:I

    iget v4, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->width:I

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->width:I

    .line 125
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->height:I

    iget v4, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->height:I

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->height:I

    .line 126
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->content_type:Ljava/lang/String;

    .line 127
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->rotation:I

    iget v4, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->rotation:I

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->rotation:I

    .line 128
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->date_taken:J

    iget-wide v6, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->date_taken:J

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->date_taken:J

    .line 129
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->mSlinkEntry:Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    iput-object v3, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkItemEntry;->device_type:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 130
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v2

    goto :goto_0
.end method
