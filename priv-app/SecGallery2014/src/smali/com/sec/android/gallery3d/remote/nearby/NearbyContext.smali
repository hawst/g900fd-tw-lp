.class public Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;
.super Ljava/lang/Object;
.source "NearbyContext.java"


# static fields
.field public static final KEY_DMRUDN:Ljava/lang/String; = "DMRUDN"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

.field private mContext:Landroid/content/Context;

.field private mIsActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->TAG:Ljava/lang/String;

    const-string v1, "Nearby Context Create!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mContext:Landroid/content/Context;

    .line 27
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-direct {v0, p1, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mIsActive:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    return-object v0
.end method

.method public getProviderDeviceCount()I
    .locals 3

    .prologue
    .line 80
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 81
    .local v0, "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    :cond_0
    const/4 v1, 0x0

    .line 83
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_0
.end method

.method public notifyDirty()V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "nearby://nearby"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 33
    return-void
.end method

.method public notifyDirty(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nearby://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 38
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mIsActive:Z

    .line 46
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mIsActive:Z

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->notifyDirty()V

    .line 51
    return-void
.end method

.method public showToastForScanningPlayerDevices()V
    .locals 6

    .prologue
    const-wide/16 v2, 0xbb8

    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->mContext:Landroid/content/Context;

    const v1, 0x7f0e012c

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 55
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext$1;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;JJ)V

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext$1;->start()Landroid/os/CountDownTimer;

    .line 69
    return-void
.end method
