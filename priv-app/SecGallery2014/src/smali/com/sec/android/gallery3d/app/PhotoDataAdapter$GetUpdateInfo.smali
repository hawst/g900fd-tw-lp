.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetUpdateInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private mInfo:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;)V
    .locals 0
    .param p2, "info"    # Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    .prologue
    .line 1427
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1428
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->mInfo:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    .line 1429
    return-void
.end method

.method private needContentReload()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 1432
    const/4 v0, 0x0

    .line 1434
    .local v0, "current":Lcom/sec/android/gallery3d/data/MediaItem;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v2

    .local v2, "i":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 1435
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    rem-int/lit8 v6, v2, 0x28

    aget-object v5, v5, v6

    if-nez v5, :cond_1

    .line 1442
    .end local v2    # "i":I
    .end local v3    # "n":I
    :cond_0
    :goto_1
    return v4

    .line 1434
    .restart local v2    # "i":I
    .restart local v3    # "n":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1438
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v6

    rem-int/lit8 v6, v6, 0x28

    aget-object v0, v5, v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1442
    .end local v2    # "i":I
    .end local v3    # "n":I
    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    if-ne v5, v6, :cond_0

    const/4 v4, 0x0

    goto :goto_1

    .line 1439
    :catch_0
    move-exception v1

    .line 1440
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public call()Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1448
    new-instance v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$1;)V

    .line 1449
    .local v0, "info":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFirstLoading:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 1450
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->mInfo:Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    iget v2, v2, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->size:I

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1802(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I

    .line 1451
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Z)V

    .line 1453
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2000(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->version:J

    .line 1454
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->needContentReload()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->reloadContent:Z

    .line 1455
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->target:Lcom/sec/android/gallery3d/data/Path;

    .line 1456
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    .line 1457
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    .line 1458
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentEnd:I

    .line 1459
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->size:I

    .line 1460
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->curIndex:I

    .line 1461
    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1424
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;->call()Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

    move-result-object v0

    return-object v0
.end method
