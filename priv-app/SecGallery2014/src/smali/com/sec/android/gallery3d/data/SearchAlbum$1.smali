.class Lcom/sec/android/gallery3d/data/SearchAlbum$1;
.super Ljava/lang/Object;
.source "SearchAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/SearchAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/SearchAlbum;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/SearchAlbum;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum$1;->this$0:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v3, 0x1

    .line 67
    instance-of v2, p2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v2, :cond_1

    move-object v2, p2

    .line 68
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v0

    .line 69
    .local v0, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->isDeleted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 70
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->getAllFaceImagePaths()Ljava/util/Set;

    move-result-object v1

    .line 71
    .local v1, "pathSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz v1, :cond_0

    .line 72
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum$1;->this$0:Lcom/sec/android/gallery3d/data/SearchAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/SearchAlbum;->mPaths:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/SearchAlbum;->access$000(Lcom/sec/android/gallery3d/data/SearchAlbum;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    .line 77
    .end local v0    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v1    # "pathSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_0
    :goto_0
    return v3

    .line 76
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchAlbum$1;->this$0:Lcom/sec/android/gallery3d/data/SearchAlbum;

    # getter for: Lcom/sec/android/gallery3d/data/SearchAlbum;->mTempMediaItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/SearchAlbum;->access$100(Lcom/sec/android/gallery3d/data/SearchAlbum;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
