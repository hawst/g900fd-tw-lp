.class Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;
.super Ljava/lang/Object;
.source "FacebookService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/FacebookService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserData"
.end annotation


# instance fields
.field private mCreatedTime:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mFromId:Ljava/lang/String;

.field private mFromName:Ljava/lang/String;

.field private mLookupKey:Ljava/lang/String;

.field private mMessage:Ljava/lang/String;

.field private mPostID:Ljava/lang/String;

.field private mStory:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/remote/sns/FacebookService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "story"    # Ljava/lang/String;
    .param p5, "fromName"    # Ljava/lang/String;
    .param p6, "fromId"    # Ljava/lang/String;
    .param p7, "postID"    # Ljava/lang/String;
    .param p8, "createdTime"    # Ljava/lang/String;
    .param p9, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->this$0:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mMessage:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mFromName:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mFromId:Ljava/lang/String;

    .line 48
    iput-object p7, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mPostID:Ljava/lang/String;

    .line 49
    iput-object p8, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mCreatedTime:Ljava/lang/String;

    .line 50
    iput-object p9, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mLookupKey:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mDescription:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mStory:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method public info()Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    .locals 11

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mPostID:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mFromName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mFromId:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mLookupKey:Ljava/lang/String;

    const-string v5, "facebook"

    const-string v6, "com.facebook.katana"

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mMessage:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mDescription:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mStory:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/sns/FacebookService$UserData;->mCreatedTime:Ljava/lang/String;

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
