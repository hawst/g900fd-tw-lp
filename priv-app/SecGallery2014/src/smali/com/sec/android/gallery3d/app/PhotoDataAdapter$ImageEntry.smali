.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageEntry"
.end annotation


# instance fields
.field public decodedAvailable:Z

.field public decodedTiles:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/TileImageView$Tile;",
            ">;"
        }
    .end annotation
.end field

.field public faceVersion:J

.field public failToLoad:Z

.field public fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

.field public fullImageTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field public isBrokenImage:Z

.field mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

.field microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

.field public microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/android/gallery3d/ui/ScreenNail;",
            ">;"
        }
    .end annotation
.end field

.field public requestedFullImage:J

.field public requestedScreenNail:J

.field public rotation:I

.field public screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

.field public screenNailTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/android/gallery3d/ui/ScreenNail;",
            ">;"
        }
    .end annotation
.end field

.field public tileLevel:I


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 1369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374
    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    .line 1375
    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    .line 1376
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    .line 1378
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->tileLevel:I

    .line 1379
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedAvailable:Z

    .line 1380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    .line 1385
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isBrokenImage:Z

    .line 1386
    iput-wide v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->faceVersion:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$1;

    .prologue
    .line 1369
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;-><init>()V

    return-void
.end method
