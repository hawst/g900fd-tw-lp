.class public Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;
.super Landroid/app/Activity;
.source "MagazineWidgetConfigure.java"


# static fields
.field private static final REQUEST_CHOOSE_ALBUM:I = 0x1

.field private static final REQUEST_CHOOSE_PERSON:I = 0x3

.field private static final REQUEST_GET_PHOTOS:I = 0x0

.field private static final REQUEST_SHUFFLE_ALL:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAppWidgetId:I

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mMenuList:[Ljava/lang/String;

.field private mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    new-instance v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$1;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    .line 77
    new-instance v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$2;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setWidgetType(I)V

    return-void
.end method

.method private setChoosenAlbum(Landroid/content/Intent;)V
    .locals 4
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 249
    .local v0, "albumPath":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 250
    const-string v3, "ALBUM_PATH"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    :cond_0
    new-instance v2, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 253
    .local v2, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v1

    .line 254
    .local v1, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v3, 0x2

    iput v3, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 255
    iput-object v0, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    .line 257
    :try_start_0
    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 258
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 262
    return-void

    .line 260
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v3
.end method

.method private setChoosenPerson(Landroid/content/Intent;)V
    .locals 13
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 265
    const/4 v7, 0x0

    .line 266
    .local v7, "personIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 267
    .local v9, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .local v2, "filePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v8, 0x0

    .line 269
    .local v8, "personIdPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v10}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    .line 271
    .local v5, "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    if-eqz p1, :cond_1

    .line 272
    const-string v10, "personIds"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 274
    if-nez v7, :cond_2

    .line 275
    :try_start_0
    sget-object v10, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    const-string v11, "personIdList is null! setChoosenPerson()"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    :cond_0
    :goto_0
    iget v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-static {v5, v2}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-static {p0, v10, v11}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 294
    new-instance v3, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 295
    .local v3, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v3, v10}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v1

    .line 296
    .local v1, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v10, 0x4

    iput v10, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 297
    iput-object v8, v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->albumPath:Ljava/lang/String;

    .line 300
    :try_start_1
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 301
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 303
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 306
    .end local v1    # "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .end local v3    # "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    :cond_1
    return-void

    .line 277
    :cond_2
    :try_start_2
    invoke-static {v9, v7}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->getPersonImagePath(Landroid/content/ContentResolver;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 279
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 280
    .local v6, "personId":I
    if-nez v8, :cond_3

    .line 281
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 283
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v8

    goto :goto_1

    .line 287
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "personId":I
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/lang/Exception;
    sget-object v10, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "showWidgetTypeDialog() exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 303
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .restart local v3    # "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    :catchall_0
    move-exception v10

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v10
.end method

.method private setChoosenPhotos(Landroid/content/Intent;)V
    .locals 11
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 222
    if-eqz p1, :cond_1

    .line 223
    const-string v7, "selectedItems"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 224
    .local v6, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v4, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 226
    .local v3, "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 227
    .local v5, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    invoke-virtual {v3, v5, v7}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->convertPathToFilePath(Lcom/sec/android/gallery3d/data/DataManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {p0, v7, v8}, Lcom/sec/android/gallery3d/gadget/LocalImagesSource;->saveWidgetPaths(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 233
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .end local v6    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 234
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 235
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v7, 0x3

    iput v7, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 237
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 238
    iget v7, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 243
    const/4 v7, -0x1

    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const-string v9, "appWidgetId"

    iget v10, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->finish()V

    .line 245
    return-void

    .line 240
    :catchall_0
    move-exception v7

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v7
.end method

.method private setDefaultWidget()V
    .locals 6

    .prologue
    .line 202
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 203
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 204
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    const/4 v2, 0x0

    iput v2, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 206
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 207
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    .line 212
    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "appWidgetId"

    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->finish()V

    .line 214
    return-void

    .line 209
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v2
.end method

.method private setDynamicFrame(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 354
    if-eqz p1, :cond_0

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mMenuList:[Ljava/lang/String;

    .line 359
    :goto_0
    return-void

    .line 357
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mMenuList:[Ljava/lang/String;

    goto :goto_0
.end method

.method private setWidgetType(I)V
    .locals 8
    .param p1, "widgetType"    # I

    .prologue
    const/4 v7, 0x1

    .line 310
    packed-switch p1, :pswitch_data_0

    .line 351
    :goto_0
    return-void

    .line 312
    :pswitch_0
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 313
    .local v3, "request":Landroid/content/Intent;
    const-string v5, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 314
    const-string v5, "image/*"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 315
    const-string v5, "photo-pick"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 316
    const-string v5, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 317
    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 320
    .end local v3    # "request":Landroid/content/Intent;
    :pswitch_1
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 321
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.GET_CONTENT"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    const-string v5, "image/*"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 323
    const-string v5, "album-pick"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 324
    const-string v5, "photo-pick"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 325
    const-string v5, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 326
    invoke-virtual {p0, v2, v7}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 329
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_2
    new-instance v1, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 330
    .local v1, "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->getEntry(I)Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    move-result-object v0

    .line 331
    .local v0, "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    iget v5, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    iput v5, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->widgetId:I

    .line 332
    iput v7, v0, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;->type:I

    .line 334
    :try_start_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->setWidget(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)Z

    .line 335
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;->close()V

    throw v5

    .line 341
    .end local v0    # "entry":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;
    .end local v1    # "helper":Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper;
    :pswitch_3
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 342
    .local v4, "requestPerson":Landroid/content/Intent;
    const-string v5, "android.intent.action.PERSON_PICK"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string v5, "image/*"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v5, "person-pick"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 345
    const-string v5, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 346
    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 310
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateWidgetAndFinish(Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V
    .locals 4
    .param p1, "entry"    # Lcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-static {v0, v1, p1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->buildAndUpdateWidget(Landroid/content/Context;ILcom/sec/android/gallery3d/gadget/WidgetDatabaseHelper$Entry;)V

    .line 171
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "appWidgetId"

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->finish()V

    .line 173
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 177
    if-nez p2, :cond_0

    .line 179
    const-wide/16 v2, 0x15e

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :cond_0
    :goto_0
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    .line 185
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "appWidgetId"

    iget v3, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->finish()V

    .line 199
    :goto_1
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 190
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    if-nez p1, :cond_2

    .line 191
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setChoosenPhotos(Landroid/content/Intent;)V

    goto :goto_1

    .line 192
    :cond_2
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    .line 193
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setChoosenAlbum(Landroid/content/Intent;)V

    goto :goto_1

    .line 194
    :cond_3
    const/4 v1, 0x3

    if-ne p1, v1, :cond_4

    .line 195
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setChoosenPerson(Landroid/content/Intent;)V

    goto :goto_1

    .line 197
    :cond_4
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "appWidgetId"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    .line 96
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 97
    .local v0, "deleteFilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 100
    iget v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mAppWidgetId:I

    if-nez v2, :cond_0

    .line 101
    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    const-string v3, "AppWidgetId is invalid. cancelled"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setResult(I)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->finish()V

    .line 122
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SHOW_WIDGET_TYPE"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 110
    .local v1, "needShowWidgetTypeDialog":Z
    if-nez v1, :cond_1

    .line 111
    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    const-string v3, "set default widget of S Camera"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-direct {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setDefaultWidget()V

    goto :goto_0

    .line 118
    .end local v1    # "needShowWidgetTypeDialog":Z
    :cond_1
    sget-object v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "show widget type dialog"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDynamicFrame:Z

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->setDynamicFrame(Z)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->showWidgetTypeDialog()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mWidgetDeletedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 140
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 141
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUMS:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "sys.usb.config"

    const-string v1, "none"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mass_storage"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e016e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->finish()V

    .line 134
    :cond_0
    return-void
.end method

.method public showWidgetTypeDialog()V
    .locals 4

    .prologue
    .line 145
    :try_start_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e009f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->mMenuList:[Ljava/lang/String;

    new-instance v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$5;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$5;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0046

    new-instance v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$4;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$4;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$3;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure$3;-><init>(Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/gallery3d/gadget/MagazineWidgetConfigure;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "showWidgetTypeDialog() exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
