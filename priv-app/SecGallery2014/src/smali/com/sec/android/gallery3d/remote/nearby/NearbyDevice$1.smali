.class Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;
.super Ljava/lang/Object;
.source "NearbyDevice.java"

# interfaces
.implements Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 139
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FlatBrowse cancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z

    .line 141
    return-void
.end method

.method public onError(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 133
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FlatBrowse error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z

    .line 135
    return-void
.end method

.method public onFinish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FlatBrowse onFinish"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatBrowseCount:I
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$102(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;I)I

    .line 124
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z

    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$302(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z

    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$400(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->notifyContentChanged()V

    .line 129
    :cond_0
    return-void
.end method

.method public onProgress(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FlatBrowse onProgress : index = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    # ++operator for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatBrowseCount:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$104(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    if-nez p1, :cond_0

    .line 102
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "FlatBrowse onProgress - item is null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_0
    return-void

    .line 106
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "n":I
    move v1, v0

    .end local v0    # "i":I
    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 107
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item;

    .line 109
    .local v2, "imageItem":Lcom/samsung/android/allshare/Item;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_3

    .line 110
    :cond_1
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 111
    add-int/lit8 v3, v3, -0x1

    .line 106
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 115
    .end local v2    # "imageItem":Lcom/samsung/android/allshare/Item;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mJpegChildren:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$400(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 116
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->nextVersionNumber()J

    move-result-wide v6

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mDataVersion:J
    invoke-static {v4, v6, v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$502(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;J)J

    .line 117
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->notifyContentChanged()V

    goto :goto_0

    .restart local v2    # "imageItem":Lcom/samsung/android/allshare/Item;
    :cond_3
    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_2
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 91
    # getter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FlatBrowse onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mFlatBrowseCount:I
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$102(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;I)I

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$202(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z

    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice$1;->this$0:Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    # setter for: Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->mIsFlatBrowseFinished:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->access$302(Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;Z)Z

    .line 95
    return-void
.end method
