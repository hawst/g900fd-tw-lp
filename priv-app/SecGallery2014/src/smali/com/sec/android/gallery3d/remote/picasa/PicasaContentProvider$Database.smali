.class public final Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$Database;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PicasaContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Database"
.end annotation


# static fields
.field public static final DATABASE_NAME:Ljava/lang/String; = "picasa.db"

.field public static final DATABASE_VERSION:I = 0x5f


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 115
    const/4 v0, 0x0

    const/16 v1, 0x5f

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 116
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 120
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 121
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 122
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 123
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 127
    const/16 v0, 0x5f

    if-ge p2, v0, :cond_0

    .line 128
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->upgradeTable(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 135
    :goto_0
    return-void

    .line 130
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 131
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 132
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/UserEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 133
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider$Database;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method
