.class Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;
.super Ljava/lang/Thread;
.source "FeatureManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/FeatureManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrepareFeatureTask"
.end annotation


# instance fields
.field mFilePath:Ljava/lang/String;

.field mOrientation:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/util/FeatureManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/util/FeatureManager;Ljava/lang/String;)V
    .locals 0
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;->this$0:Lcom/sec/android/gallery3d/util/FeatureManager;

    .line 65
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 66
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 70
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;->this$0:Lcom/sec/android/gallery3d/util/FeatureManager;

    # getter for: Lcom/sec/android/gallery3d/util/FeatureManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/util/FeatureManager;->access$000(Lcom/sec/android/gallery3d/util/FeatureManager;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 71
    .local v0, "activity":Lcom/sec/android/gallery3d/app/GalleryActivity;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 72
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;->this$0:Lcom/sec/android/gallery3d/util/FeatureManager;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/gallery3d/util/FeatureManager;->setPrepareScreenNailResult(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/FeatureManager;->access$100(Lcom/sec/android/gallery3d/util/FeatureManager;Z)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/gallery3d/util/FeatureManager$PrepareFeatureTask;->this$0:Lcom/sec/android/gallery3d/util/FeatureManager;

    # getter for: Lcom/sec/android/gallery3d/util/FeatureManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/util/FeatureManager;->access$000(Lcom/sec/android/gallery3d/util/FeatureManager;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e025c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setDCIMName(Ljava/lang/String;)V

    .line 74
    return-void
.end method
