.class public Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSet;
.source "PicasaAlbumSet.java"


# static fields
.field private static final ALBUM_ID_WHERE_CLAUSE:Ljava/lang/String; = "album_id=?"

.field private static final SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

.field private static final SUM_SIZE_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "PicasaAlbumSet"


# instance fields
.field private mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "sum(size)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->SUM_SIZE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 60
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mType:I

    .line 61
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0096

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mName:Ljava/lang/String;

    .line 63
    return-void
.end method

.method private static getAlbumCacheSize(Landroid/content/ContentResolver;J)J
    .locals 7
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "albumId"    # J

    .prologue
    const/4 v5, 0x0

    .line 183
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->SUM_SIZE_PROJECTION:[Ljava/lang/String;

    const-string v3, "album_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 190
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 191
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 192
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 198
    if-eqz v6, :cond_0

    .line 199
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-wide v0

    .line 195
    :cond_1
    const-wide/16 v0, 0x0

    .line 198
    if-eqz v6, :cond_0

    .line 199
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 199
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getAlbumData(Landroid/content/ContentResolver;J)Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    .locals 9
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J

    .prologue
    const/4 v5, 0x0

    .line 158
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 165
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    new-instance v7, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    invoke-direct {v7}, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;-><init>()V

    .line 167
    .local v7, "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    if-eqz v6, :cond_0

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    :cond_0
    :goto_0
    return-object v7

    .line 173
    :cond_1
    if-eqz v6, :cond_2

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v7, v5

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private static getTypeFromPath(Lcom/sec/android/gallery3d/data/Path;)I
    .locals 5
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->split()[Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "name":[Ljava/lang/String;
    array-length v2, v0

    if-ge v2, v1, :cond_0

    .line 68
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 70
    :cond_0
    const-string v2, "all"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "mergedall"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 71
    :cond_1
    const/4 v1, 0x6

    .line 75
    :cond_2
    :goto_0
    return v1

    .line 72
    :cond_3
    const-string v2, "image"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mergedimage"

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 74
    const-string/jumbo v1, "video"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "mergedvideo"

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 75
    :cond_4
    const/4 v1, 0x4

    goto :goto_0

    .line 76
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private loadSubMediaSets()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 106
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->ALBUMS_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v4, "type"

    iget v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mType:I

    invoke-static {v5}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->getTypeString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 110
    .local v1, "uri":Landroid/net/Uri;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v12, "loadBuffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/picasa/AlbumData;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 112
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 114
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 115
    const-string v2, "PicasaAlbumSet"

    const-string v3, "cannot open picasa database"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 154
    :cond_0
    return-object v14

    .line 119
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 120
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    new-instance v3, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;-><init>()V

    invoke-virtual {v2, v8, v3}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/sec/android/gallery3d/remote/picasa/Entry;)Lcom/sec/android/gallery3d/remote/picasa/Entry;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    .line 121
    .local v10, "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    iget v2, v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->valid:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 122
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 131
    .end local v10    # "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    .line 123
    .restart local v10    # "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    :cond_2
    :try_start_1
    iget v2, v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->valid:I

    if-nez v2, :cond_1

    .line 124
    iget-wide v2, v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->id:J

    invoke-static {v0, v2, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->getAlbumCacheSize(Landroid/content/ContentResolver;J)J

    move-result-wide v2

    iput-wide v2, v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->cacheSize:J

    .line 125
    iget-wide v2, v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->cacheSize:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 126
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 131
    .end local v10    # "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 134
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v14, "newAlbums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v9

    .line 137
    .local v9, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v11, 0x0

    .local v11, "i":I
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    .local v13, "n":I
    :goto_1
    if-ge v11, v13, :cond_5

    .line 138
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;

    .line 139
    .restart local v10    # "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    iget-wide v4, v10, Lcom/sec/android/gallery3d/remote/picasa/AlbumData;->id:J

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    .line 140
    .local v7, "childPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v9, v7}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    .line 142
    .local v6, "album":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;
    if-nez v6, :cond_4

    .line 143
    new-instance v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    .end local v6    # "album":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mType:I

    invoke-direct {v6, v7, v2, v10, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/remote/picasa/AlbumData;I)V

    .line 147
    .restart local v6    # "album":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;
    :goto_2
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 145
    :cond_4
    invoke-virtual {v6, v10}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->updateContent(Lcom/sec/android/gallery3d/remote/picasa/AlbumData;)V

    goto :goto_2

    .line 149
    .end local v6    # "album":Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;
    .end local v7    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "entry":Lcom/sec/android/gallery3d/remote/picasa/AlbumData;
    :cond_5
    sget-object v2, Lcom/sec/android/gallery3d/util/MediaSetUtils;->NAME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v14, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 151
    const/4 v11, 0x0

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v13

    :goto_3
    if-ge v11, v13, :cond_0

    .line 152
    invoke-virtual {v14, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->reload()J

    .line 151
    add-int/lit8 v11, v11, 0x1

    goto :goto_3
.end method


# virtual methods
.method public getBucketId()I
    .locals 2

    .prologue
    .line 263
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 264
    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getBucketId()I

    move-result v0

    .line 266
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->getBucketId()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 5
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v4, :cond_3

    .line 234
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v1, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .line 236
    .local v3, "totalItemCount":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 237
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    .line 238
    if-lt p1, v3, :cond_0

    .line 239
    sub-int/2addr p1, v3

    .line 240
    goto :goto_0

    .line 242
    :cond_0
    sub-int v4, v3, p1

    if-ge v4, p2, :cond_1

    .line 243
    sub-int v4, v3, p1

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 245
    sub-int v4, v3, p1

    sub-int/2addr p2, v4

    .line 246
    const/4 p1, 0x0

    goto :goto_0

    .line 248
    :cond_1
    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 254
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v3    # "totalItemCount":I
    :cond_2
    :goto_1
    return-object v1

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1
.end method

.method public getMediaItemCount()I
    .locals 4

    .prologue
    .line 220
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v3, :cond_0

    .line 221
    const/4 v2, 0x0

    .line 222
    .local v2, "totalItemCount":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 223
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 224
    goto :goto_0

    .line 227
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "totalItemCount":I
    :cond_0
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/RemoteMediaSet;->getMediaItemCount()I

    move-result v2

    :cond_1
    return v2
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 86
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 88
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getSupportedOperations()J
    .locals 6

    .prologue
    .line 98
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_0

    const-wide/16 v2, 0x0

    .line 99
    .local v2, "supported":J
    :goto_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 100
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v2, v4

    .line 101
    goto :goto_1

    .line 98
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "supported":J
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 102
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "supported":J
    :cond_1
    return-wide v2
.end method

.method public isAlbumSetEmpty()Z
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 273
    const/4 v0, 0x0

    .line 274
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->loadSubMediaSets()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mAlbums:Ljava/util/ArrayList;

    .line 208
    invoke-static {}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mDataVersion:J

    .line 210
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;->mDataVersion:J

    return-wide v0
.end method
