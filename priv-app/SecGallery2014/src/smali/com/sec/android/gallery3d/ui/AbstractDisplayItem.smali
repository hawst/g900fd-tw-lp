.class public abstract Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;
.super Lcom/sec/android/gallery3d/ui/DisplayItem;
.source "AbstractDisplayItem.java"


# static fields
.field private static final STATE_CANCELING:I = 0x8

.field private static final STATE_ERROR:I = 0x10

.field private static final STATE_INVALID:I = 0x1

.field private static final STATE_REFRESHING:I = 0x20

.field private static final STATE_UPDATING:I = 0x4

.field private static final STATE_VALID:I = 0x2


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mImageRequested:Z

.field public final mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mRecycling:Z

.field private mRefreshRequested:Z

.field private mState:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/DisplayItem;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRefreshRequested:Z

    .line 37
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRecycling:Z

    .line 44
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 45
    if-nez p1, :cond_0

    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 46
    :cond_0
    return-void
.end method

.method private inState(I)Z
    .locals 1
    .param p1, "states"    # I

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancelImageRequest()V
    .locals 2

    .prologue
    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    .line 132
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 133
    :cond_0
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->cancelLoadBitmap()V

    .line 136
    :cond_1
    return-void
.end method

.method protected abstract cancelLoadBitmap()V
.end method

.method public getIdentity()J
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRequestInProgress()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->inState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract onBitmapAvailable(Landroid/graphics/Bitmap;)V
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 143
    const/16 v0, 0x2c

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->inState(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 148
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 151
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRecycling:Z

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->cancelImageRequest()V

    goto :goto_0
.end method

.method protected abstract recycleBitmap(Landroid/graphics/Bitmap;)V
.end method

.method public requestImage()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 92
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRefreshRequested:Z

    .line 94
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    if-ne v0, v1, :cond_0

    .line 95
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->startLoadBitmap()V

    .line 98
    :cond_0
    return-void
.end method

.method public requestImageLatency()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x4

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRefreshRequested:Z

    .line 103
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    if-ne v0, v1, :cond_1

    .line 104
    iput v2, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->startLoadBitmap()V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 107
    :cond_2
    iput v2, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->startLoadBitmapLatency()V

    goto :goto_0
.end method

.method public requestNewImage()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRefreshRequested:Z

    .line 115
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->startLoadBitmap()V

    .line 117
    return-void
.end method

.method public requestRefresh()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 120
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    .line 121
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRefreshRequested:Z

    .line 122
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 123
    const/16 v1, 0x20

    iput v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->startRefreshBitmap()V

    .line 127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract startLoadBitmap()V
.end method

.method protected startLoadBitmapLatency()V
    .locals 0

    .prologue
    .line 168
    return-void
.end method

.method public startRefreshBitmap()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method protected updateImage(Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "isCancelled"    # Z

    .prologue
    const/4 v0, 0x2

    .line 50
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mRecycling:Z

    if-eqz v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    if-eqz p2, :cond_2

    if-nez p1, :cond_2

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    .line 56
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mImageRequested:Z

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->requestImage()V

    goto :goto_0

    .line 62
    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    if-ne v1, v0, :cond_5

    .line 64
    if-eqz p1, :cond_4

    .line 65
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 69
    :cond_3
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 75
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->onBitmapAvailable(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 72
    :cond_5
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 73
    if-nez p1, :cond_6

    const/16 v0, 0x10

    :cond_6
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;->mState:I

    goto :goto_1
.end method
