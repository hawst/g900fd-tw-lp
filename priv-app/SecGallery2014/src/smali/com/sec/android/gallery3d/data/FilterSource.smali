.class public Lcom/sec/android/gallery3d/data/FilterSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "FilterSource.java"


# static fields
.field private static final FILTER_BY_CAMERA_SHORTCUT:I = 0x4

.field private static final FILTER_BY_CAMERA_SHORTCUT_ITEM:I = 0x5

.field private static final FILTER_BY_DELETE:I = 0x1

.field private static final FILTER_BY_EMPTY:I = 0x2

.field private static final FILTER_BY_EMPTY_ITEM:I = 0x3

.field private static final FILTER_BY_FOLDER:I = 0x64

.field private static final FILTER_BY_MEDIATYPE:I = 0x0

.field public static final FILTER_CAMERA_SHORTCUT:Ljava/lang/String; = "/filter/camera_shortcut"

.field private static final FILTER_CAMERA_SHORTCUT_ITEM:Ljava/lang/String; = "/filter/camera_shortcut_item"

.field public static final FILTER_EMPTY_ITEM:Ljava/lang/String; = "/filter/empty_prompt"

.field private static final TAG:Ljava/lang/String; = "FilterSource"


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mCameraShortcutItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mEmptyItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 44
    const-string v0, "filter"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 46
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/filter/mediatype/*/*"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/filter/delete/*"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/filter/empty/*"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/filter/empty_prompt"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/filter/camera_shortcut"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/filter/camera_shortcut_item"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/filter/folder/*/*"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 57
    new-instance v0, Lcom/sec/android/gallery3d/data/EmptyAlbumImage;

    const-string v1, "/filter/empty_prompt"

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/EmptyAlbumImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mEmptyItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 59
    new-instance v0, Lcom/sec/android/gallery3d/data/CameraShortcutImage;

    const-string v1, "/filter/camera_shortcut_item"

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/data/CameraShortcutImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mCameraShortcutItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 61
    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 13
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v3, 0x0

    .line 69
    const/4 v10, 0x0

    .line 70
    .local v10, "split":[Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 71
    aget-object v0, v10, v3

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object p1

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v7

    .line 74
    .local v7, "matchType":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    .line 75
    .local v2, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sparse-switch v7, :sswitch_data_0

    .line 120
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bad path: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v4

    .line 78
    .local v4, "mediaType":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v9

    .line 79
    .local v9, "setsName":Ljava/lang/String;
    invoke-virtual {v2, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSetsFromString(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 81
    .local v8, "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v5, 0x0

    .line 82
    .local v5, "filterMimeType":Ljava/lang/String;
    array-length v0, v10

    if-ne v0, v12, :cond_0

    .line 83
    aget-object v0, v10, v11

    const-string v1, "all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    aget-object v5, v10, v11

    .line 88
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/data/FilterTypeSet;

    aget-object v3, v8, v3

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/FilterTypeSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;ILjava/lang/String;)V

    .line 117
    .end local v4    # "mediaType":I
    .end local v5    # "filterMimeType":Ljava/lang/String;
    .end local v8    # "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v9    # "setsName":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 91
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v9

    .line 92
    .restart local v9    # "setsName":Ljava/lang/String;
    invoke-virtual {v2, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSetsFromString(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 93
    .restart local v8    # "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v0, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    aget-object v1, v8, v3

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 96
    .end local v8    # "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v9    # "setsName":Ljava/lang/String;
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v9

    .line 97
    .restart local v9    # "setsName":Ljava/lang/String;
    invoke-virtual {v2, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSetsFromString(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 98
    .restart local v8    # "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v0, Lcom/sec/android/gallery3d/data/FilterEmptyPromptSet;

    aget-object v1, v8, v3

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mEmptyItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v0, p1, v1, v3}, Lcom/sec/android/gallery3d/data/FilterEmptyPromptSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0

    .line 101
    .end local v8    # "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v9    # "setsName":Ljava/lang/String;
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mEmptyItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 104
    :sswitch_4
    new-instance v0, Lcom/sec/android/gallery3d/data/SingleItemAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mCameraShortcutItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/data/SingleItemAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0

    .line 107
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mCameraShortcutItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 111
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v9

    .line 112
    .restart local v9    # "setsName":Ljava/lang/String;
    invoke-virtual {v2, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSetsFromString(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 113
    .restart local v8    # "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v6, 0x0

    .line 114
    .local v6, "folder":Ljava/lang/String;
    array-length v0, v10

    if-ne v0, v12, :cond_1

    .line 115
    aget-object v6, v10, v11

    .line 117
    :cond_1
    new-instance v0, Lcom/sec/android/gallery3d/data/FilterFolderSet;

    aget-object v1, v8, v3

    invoke-direct {v0, p1, v2, v1, v6}, Lcom/sec/android/gallery3d/data/FilterFolderSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x64 -> :sswitch_6
    .end sparse-switch
.end method
