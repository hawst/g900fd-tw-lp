.class public Lcom/sec/android/gallery3d/data/ImageDiskCache$ImageData;
.super Ljava/lang/Object;
.source "ImageDiskCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/ImageDiskCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageData"
.end annotation


# instance fields
.field public mData:[B

.field public mMeta:Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;

.field public mOffset:I


# direct methods
.method public constructor <init>([BILcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;)V
    .locals 0
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "meta"    # Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache$ImageData;->mData:[B

    .line 178
    iput p2, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache$ImageData;->mOffset:I

    .line 179
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/ImageDiskCache$ImageData;->mMeta:Lcom/sec/android/gallery3d/data/ImageDiskCache$MetaData;

    .line 180
    return-void
.end method
