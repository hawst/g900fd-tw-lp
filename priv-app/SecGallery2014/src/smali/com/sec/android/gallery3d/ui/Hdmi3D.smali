.class public Lcom/sec/android/gallery3d/ui/Hdmi3D;
.super Ljava/lang/Object;
.source "Hdmi3D.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;
    }
.end annotation


# static fields
.field public static final COMMAND_STOP:I = 0x9

.field public static final KEY_COMMAND:Ljava/lang/String; = "key_method"

.field public static final KEY_DISPLAY_ID:Ljava/lang/String; = "key_display_id"

.field public static final NOTI_ID:I = 0x270f

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mBmpLeftSrc:Landroid/graphics/Bitmap;

.field private mBmpRightSrc:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mDisplay:Landroid/view/Display;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mDisplaySet:[Landroid/view/Display;

.field private mIgnoreBackkeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mLayout:Landroid/view/View;

.field private mLeftView:Landroid/widget/ImageView;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mPresentation:Landroid/app/Presentation;

.field private mPresentationFocuser:Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;

.field private mRightView:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/gallery3d/ui/Hdmi3D;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mBmpLeftSrc:Landroid/graphics/Bitmap;

    .line 50
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mBmpRightSrc:Landroid/graphics/Bitmap;

    .line 210
    new-instance v0, Lcom/sec/android/gallery3d/ui/Hdmi3D$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/Hdmi3D$1;-><init>(Lcom/sec/android/gallery3d/ui/Hdmi3D;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 221
    new-instance v0, Lcom/sec/android/gallery3d/ui/Hdmi3D$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/Hdmi3D$2;-><init>(Lcom/sec/android/gallery3d/ui/Hdmi3D;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mIgnoreBackkeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 232
    new-instance v0, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;-><init>(Lcom/sec/android/gallery3d/ui/Hdmi3D;Lcom/sec/android/gallery3d/ui/Hdmi3D$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentationFocuser:Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;

    .line 53
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/Hdmi3D;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/Hdmi3D;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/Hdmi3D;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->clearPresentation(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/Hdmi3D;)Landroid/app/Presentation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    return-object v0
.end method

.method private clearPresentation(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 208
    return-void
.end method

.method private createLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 155
    const/16 v3, 0x7d2

    .line 157
    .local v3, "windowType":I
    :try_start_0
    const-class v4, Landroid/view/WindowManager$LayoutParams;

    const-string v5, "TYPE_FAKE_APPLICATION"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 159
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 160
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 165
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    const v4, 0x40160

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 173
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 174
    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 176
    const/16 v4, 0x33

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 177
    const/16 v4, 0x20

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 178
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 180
    return-object v2

    .line 161
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getBitmapScalingFactor(Landroid/graphics/Bitmap;)F
    .locals 10
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 185
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    .line 186
    .local v6, "point":Landroid/graphics/Point;
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplay:Landroid/view/Display;

    invoke-virtual {v8, v6}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 187
    iget v8, v6, Landroid/graphics/Point;->x:I

    div-int/lit8 v5, v8, 0x2

    .line 188
    .local v5, "displayWidth":I
    iget v4, v6, Landroid/graphics/Point;->y:I

    .line 190
    .local v4, "displayHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 191
    .local v2, "bmWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    mul-int/lit8 v0, v8, 0x2

    .line 192
    .local v0, "bmHeight":I
    int-to-float v8, v4

    int-to-float v9, v5

    div-float v3, v8, v9

    .line 193
    .local v3, "disRatio":F
    int-to-float v8, v0

    int-to-float v9, v2

    div-float v1, v8, v9

    .line 196
    .local v1, "bmRatio":F
    cmpl-float v8, v3, v1

    if-ltz v8, :cond_0

    .line 197
    int-to-float v8, v5

    int-to-float v9, v2

    div-float v7, v8, v9

    .line 201
    .local v7, "ratio":F
    :goto_0
    return v7

    .line 199
    .end local v7    # "ratio":F
    :cond_0
    int-to-float v8, v4

    int-to-float v9, v0

    div-float v7, v8, v9

    .restart local v7    # "ratio":F
    goto :goto_0
.end method

.method private show(I)V
    .locals 6
    .param p1, "displayId"    # I

    .prologue
    .line 123
    sget-object v3, Lcom/sec/android/gallery3d/ui/Hdmi3D;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "show presentation"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->createLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 128
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    new-instance v3, Landroid/app/Presentation;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplay:Landroid/view/Display;

    invoke-direct {v3, v4, v5}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    .line 129
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mIgnoreBackkeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/app/Presentation;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    invoke-virtual {v3}, Landroid/app/Presentation;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 131
    .local v2, "window":Landroid/view/Window;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 132
    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 133
    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 135
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 138
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v3, 0x7f0300c0

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mLayout:Landroid/view/View;

    .line 139
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/app/Presentation;->setContentView(Landroid/view/View;)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v3, v4}, Landroid/app/Presentation;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 142
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mLayout:Landroid/view/View;

    const v4, 0x7f0f0200

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mLeftView:Landroid/widget/ImageView;

    .line 143
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mLayout:Landroid/view/View;

    const v4, 0x7f0f0201

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mRightView:Landroid/widget/ImageView;

    .line 145
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentationFocuser:Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->sendBroadcast(Landroid/content/Context;)V

    .line 146
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentationFocuser:Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->registerReceiver(Landroid/content/Context;)V

    .line 147
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    invoke-virtual {v3}, Landroid/app/Presentation;->show()V

    .line 148
    return-void
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    invoke-virtual {v0}, Landroid/app/Presentation;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImage(Landroid/graphics/Bitmap;Z)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "isLeft"    # Z

    .prologue
    .line 102
    if-eqz p2, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mLeftView:Landroid/widget/ImageView;

    .line 103
    .local v3, "view":Landroid/widget/ImageView;
    :goto_0
    if-nez v3, :cond_2

    .line 120
    :cond_0
    :goto_1
    return-void

    .line 102
    .end local v3    # "view":Landroid/widget/ImageView;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mRightView:Landroid/widget/ImageView;

    goto :goto_0

    .line 106
    .restart local v3    # "view":Landroid/widget/ImageView;
    :cond_2
    if-eqz p2, :cond_3

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mBmpLeftSrc:Landroid/graphics/Bitmap;

    .line 107
    .local v2, "srcBmp":Landroid/graphics/Bitmap;
    :goto_2
    if-eq v2, p1, :cond_0

    .line 110
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->getBitmapScalingFactor(Landroid/graphics/Bitmap;)F

    move-result v1

    .line 111
    .local v1, "scalingFactor":F
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v1

    invoke-static {p1, v1, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->scaleBitmap(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 114
    .local v0, "output":Landroid/graphics/Bitmap;
    if-nez v0, :cond_4

    .line 115
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 106
    .end local v0    # "output":Landroid/graphics/Bitmap;
    .end local v1    # "scalingFactor":F
    .end local v2    # "srcBmp":Landroid/graphics/Bitmap;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mBmpRightSrc:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 118
    .restart local v0    # "output":Landroid/graphics/Bitmap;
    .restart local v1    # "scalingFactor":F
    .restart local v2    # "srcBmp":Landroid/graphics/Bitmap;
    :cond_4
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public startPresentation()V
    .locals 3

    .prologue
    .line 63
    sget-object v1, Lcom/sec/android/gallery3d/ui/Hdmi3D;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startPresentation()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 67
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const-string v2, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplaySet:[Landroid/view/Display;

    .line 69
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplaySet:[Landroid/view/Display;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplaySet:[Landroid/view/Display;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplay:Landroid/view/Display;

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplay:Landroid/view/Display;

    if-nez v1, :cond_1

    .line 83
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    if-eqz v1, :cond_2

    .line 76
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Presentation;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 77
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentationFocuser:Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->unRegisterReceiver(Landroid/content/Context;)V

    .line 78
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    invoke-virtual {v1}, Landroid/app/Presentation;->dismiss()V

    .line 81
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    .line 82
    .local v0, "displayId":I
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->show(I)V

    goto :goto_0
.end method

.method public stopPresentation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    sget-object v0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopPresentation()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    invoke-virtual {v0, v2}, Landroid/app/Presentation;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentationFocuser:Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/Hdmi3D$PresentationFocuser;->unRegisterReceiver(Landroid/content/Context;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    invoke-virtual {v0}, Landroid/app/Presentation;->dismiss()V

    .line 91
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mPresentation:Landroid/app/Presentation;

    .line 93
    :cond_0
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mBmpLeftSrc:Landroid/graphics/Bitmap;

    .line 94
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mBmpRightSrc:Landroid/graphics/Bitmap;

    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/Hdmi3D;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/Hdmi3D;->clearPresentation(Landroid/content/Context;)V

    .line 96
    return-void
.end method
