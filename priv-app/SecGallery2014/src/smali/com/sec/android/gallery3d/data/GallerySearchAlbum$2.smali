.class final Lcom/sec/android/gallery3d/data/GallerySearchAlbum$2;
.super Ljava/lang/Object;
.source "GallerySearchAlbum.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/GallerySearchAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 6
    .param p1, "object1"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "object2"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 147
    const-wide/16 v0, 0x0

    .line 148
    .local v0, "obj1_Time":J
    const-wide/16 v2, 0x0

    .line 149
    .local v2, "obj2_Time":J
    const/4 v4, -0x1

    .line 150
    .local v4, "result":I
    # getter for: Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->mSortByOldest:Z
    invoke-static {}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->access$100()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 151
    const/4 v4, 0x1

    .line 154
    :cond_0
    if-nez p1, :cond_2

    .line 155
    neg-int v4, v4

    .line 162
    .end local v4    # "result":I
    :cond_1
    :goto_0
    return v4

    .line 156
    .restart local v4    # "result":I
    :cond_2
    if-eqz p2, :cond_1

    .line 159
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v0

    .line 160
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    .line 162
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Long;->compare(JJ)I

    move-result v4

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 143
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum$2;->compare(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    return v0
.end method
