.class Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;
.super Ljava/lang/Thread;
.source "CameraViewFinderHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GLRenderTask"
.end annotation


# static fields
.field private static final RENDER_INTERVAL_TIME:I = 0x3c

.field private static final TOTAL_RENDER_TIME:I = 0xbb8

.field private static final TOTAL_RENDER_TIME_COUNT:I = 0x32


# instance fields
.field private final mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mIsCanceled:Z

.field private mRenderTimeCount:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 1
    .param p2, "glRootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    const/4 v0, 0x0

    .line 388
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->this$0:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 385
    iput v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mRenderTimeCount:I

    .line 386
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mIsCanceled:Z

    .line 389
    iput-object p2, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 390
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p3, "x2"    # Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$1;

    .prologue
    .line 380
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;-><init>(Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mIsCanceled:Z

    .line 406
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 394
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mIsCanceled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mRenderTimeCount:I

    const/16 v1, 0x32

    if-gt v0, v1, :cond_0

    .line 395
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 396
    iget v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mRenderTimeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler$GLRenderTask;->mRenderTimeCount:I

    .line 398
    const-wide/16 v0, 0x3c

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 399
    :catch_0
    move-exception v0

    goto :goto_0

    .line 402
    :cond_0
    return-void
.end method
