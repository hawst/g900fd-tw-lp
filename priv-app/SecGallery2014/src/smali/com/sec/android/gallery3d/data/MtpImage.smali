.class public Lcom/sec/android/gallery3d/data/MtpImage;
.super Lcom/sec/android/gallery3d/data/MediaItem;
.source "MtpImage.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MtpImage"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDateTaken:J

.field private final mDeviceId:I

.field private mFileName:Ljava/lang/String;

.field private final mImageHeight:I

.field private final mImageWidth:I

.field private final mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

.field private final mObjInfo:Landroid/mtp/MtpObjectInfo;

.field private mObjectId:I

.field private mObjectSize:I


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;IILcom/sec/android/gallery3d/data/MtpContext;)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "deviceId"    # I
    .param p4, "objectId"    # I
    .param p5, "mtpContext"    # Lcom/sec/android/gallery3d/data/MtpContext;

    .prologue
    .line 75
    invoke-static {p5, p3, p4}, Lcom/sec/android/gallery3d/data/MtpDevice;->getObjectInfo(Lcom/sec/android/gallery3d/data/MtpContext;II)Landroid/mtp/MtpObjectInfo;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/MtpImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;ILandroid/mtp/MtpObjectInfo;Lcom/sec/android/gallery3d/data/MtpContext;)V

    .line 77
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;ILandroid/mtp/MtpObjectInfo;Lcom/sec/android/gallery3d/data/MtpContext;)V
    .locals 3
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "deviceId"    # I
    .param p4, "objInfo"    # Landroid/mtp/MtpObjectInfo;
    .param p5, "mtpContext"    # Lcom/sec/android/gallery3d/data/MtpContext;

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-static {}, Lcom/sec/android/gallery3d/data/MtpImage;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 54
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mContext:Landroid/content/Context;

    .line 55
    iput p3, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDeviceId:I

    .line 56
    iput-object p5, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    .line 57
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjInfo:Landroid/mtp/MtpObjectInfo;

    .line 59
    if-nez p4, :cond_0

    .line 60
    const-string v0, "MtpImage"

    const-string v1, "object info is null, ignore a MTP image"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iput v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageWidth:I

    .line 62
    iput v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageHeight:I

    .line 72
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p4}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectId:I

    .line 67
    invoke-virtual {p4}, Landroid/mtp/MtpObjectInfo;->getCompressedSize()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectSize:I

    .line 68
    invoke-virtual {p4}, Landroid/mtp/MtpObjectInfo;->getDateCreated()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDateTaken:J

    .line 69
    invoke-virtual {p4}, Landroid/mtp/MtpObjectInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mFileName:Ljava/lang/String;

    .line 70
    invoke-virtual {p4}, Landroid/mtp/MtpObjectInfo;->getImagePixWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageWidth:I

    .line 71
    invoke-virtual {p4}, Landroid/mtp/MtpObjectInfo;->getImagePixHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageHeight:I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/MtpImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MtpImage;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDeviceId:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/MtpImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MtpImage;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectId:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/MtpImage;)Lcom/sec/android/gallery3d/data/MtpContext;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MtpImage;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/data/MtpImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MtpImage;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectSize:I

    return v0
.end method


# virtual methods
.method public Import()Z
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    iget v1, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDeviceId:I

    invoke-static {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjInfo:Landroid/mtp/MtpObjectInfo;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MtpContext;->copyFile(Ljava/lang/String;Landroid/mtp/MtpObjectInfo;)Z

    move-result v0

    return v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->getUriFor(Landroid/content/Context;Lcom/sec/android/gallery3d/data/Path;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDateInMs()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDateTaken:J

    return-wide v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 178
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 180
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 183
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MtpImage;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 184
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDateTaken:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 187
    const/16 v1, 0xc

    iget v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageWidth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 188
    const/16 v1, 0xd

    iget v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageHeight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 190
    const/16 v1, 0xf

    const-string v2, "%dx%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    iget v4, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 192
    const/16 v1, 0xe

    iget v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectSize:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 193
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGroupId()J
    .locals 2

    .prologue
    .line 214
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageHeight:I

    return v0
.end method

.method public getImageData()[B
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MtpContext;->getMtpClient()Lcom/sec/android/gallery3d/data/MtpClient;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDeviceId:I

    invoke-static {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectId:I

    iget v3, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectSize:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/data/MtpClient;->getObject(Ljava/lang/String;II)[B

    move-result-object v0

    return-object v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x2

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    const-string v0, "image/jpeg"

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectSize:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 139
    const-wide v0, 0x20000000440L

    return-wide v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mImageWidth:I

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpImage$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/MtpImage$1;-><init>(Lcom/sec/android/gallery3d/data/MtpImage;)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/sec/android/gallery3d/data/MtpImage$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/MtpImage$2;-><init>(Lcom/sec/android/gallery3d/data/MtpImage;)V

    return-object v0
.end method

.method public updateContent(Landroid/mtp/MtpObjectInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/mtp/MtpObjectInfo;

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectId:I

    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDateTaken:J

    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getDateCreated()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 149
    :cond_0
    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mObjectId:I

    .line 150
    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getDateCreated()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDateTaken:J

    .line 151
    invoke-static {}, Lcom/sec/android/gallery3d/data/MtpImage;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MtpImage;->mDataVersion:J

    .line 153
    :cond_1
    return-void
.end method
