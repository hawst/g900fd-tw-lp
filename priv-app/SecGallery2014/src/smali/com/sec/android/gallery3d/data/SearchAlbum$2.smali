.class Lcom/sec/android/gallery3d/data/SearchAlbum$2;
.super Ljava/lang/Object;
.source "SearchAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/SearchAlbum;->delete()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/SearchAlbum;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/SearchAlbum;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/SearchAlbum$2;->this$0:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 384
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 385
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->delete()V

    .line 386
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
