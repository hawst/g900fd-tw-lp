.class Lcom/sec/android/gallery3d/ui/FilmStripView$1;
.super Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.source "FilmStripView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/FilmStripView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/util/KeyBoardManager;IZLcom/sec/android/gallery3d/app/AlbumReloader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/FilmStripView;Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$1;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 139
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 150
    new-instance v0, Ljava/lang/AssertionError;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 141
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$1;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$000(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/ScrollBarView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$1;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mSlotView:Lcom/sec/android/gallery3d/ui/SlotView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$100(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/SlotView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$1;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    # getter for: Lcom/sec/android/gallery3d/ui/FilmStripView;->mScrollBarView:Lcom/sec/android/gallery3d/ui/ScrollBarView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->access$000(Lcom/sec/android/gallery3d/ui/FilmStripView;)Lcom/sec/android/gallery3d/ui/ScrollBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ScrollBarView;->hideScrollBar()V

    .line 144
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/FilmStripView$1;->this$0:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->invalidate()V

    .line 152
    :cond_0
    return-void

    .line 139
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
