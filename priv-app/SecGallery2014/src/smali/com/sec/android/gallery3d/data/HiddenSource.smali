.class public Lcom/sec/android/gallery3d/data/HiddenSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "HiddenSource.java"


# static fields
.field private static final HIDDEN_ALL_ALBUM:I = 0x7

.field private static final HIDDEN_ALL_ALBUMSET:I = 0x6

.field private static final HIDDEN_IMAGE_ALBUM:I = 0x2

.field private static final HIDDEN_VIDEO_ALBUM:I = 0x3

.field public static final HIDE_PATH:Ljava/lang/String;

.field public static final ISLAND_HIDE_PATH:Ljava/lang/String;

.field private static sContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.hide"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenSource;->HIDE_PATH:Ljava/lang/String;

    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/gallery3d/data/HiddenSource;->HIDE_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/island"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenSource;->ISLAND_HIDE_PATH:Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/sec/android/gallery3d/data/HiddenSource;->sContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 30
    const-string v0, "hidden"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 32
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/hidden/all"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/hidden/image/*"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/hidden/video/*"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/hidden/all/*"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 39
    return-void
.end method

.method public static hasHiddenMedias(Lcom/sec/android/gallery3d/app/GalleryApp;)Z
    .locals 1
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 72
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/data/HiddenSource;->hasHiddenMediasForType(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z

    move-result v0

    goto :goto_0
.end method

.method public static hasHiddenMedias(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z
    .locals 1
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 79
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/data/HiddenSource;->hasHiddenMediasForType(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z

    move-result v0

    goto :goto_0
.end method

.method private static hasHiddenMediasForType(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z
    .locals 4
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v3, :cond_1

    move v1, v2

    .line 93
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    .line 90
    .local v0, "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    if-eqz p1, :cond_2

    .line 91
    invoke-virtual {v0, v2, p1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbum(ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 93
    :cond_2
    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbum(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public static declared-synchronized syncHiddenFiles(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 4
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 104
    const-class v2, Lcom/sec/android/gallery3d/data/HiddenSource;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/data/HiddenSource;->sContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    .line 106
    .local v0, "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    const-string v1, "images_hidden_album"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->updateDataBase(Ljava/lang/String;)Z

    .line 107
    const-string/jumbo v1, "video_hidden_album"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->updateDataBase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    .end local v0    # "dbMgr":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    :cond_0
    monitor-exit v2

    return-void

    .line 104
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 12
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v5, 0x1

    const/4 v11, 0x0

    .line 43
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 67
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :pswitch_1
    new-instance v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 62
    :goto_0
    return-object v0

    .line 47
    :pswitch_2
    new-instance v0, Lcom/sec/android/gallery3d/data/HiddenAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v3, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v4

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/HiddenAlbum;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/data/Path;IZ)V

    goto :goto_0

    .line 51
    :pswitch_3
    new-instance v0, Lcom/sec/android/gallery3d/data/HiddenAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v3, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v4

    move-object v3, p1

    move v5, v11

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/HiddenAlbum;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Lcom/sec/android/gallery3d/data/Path;IZ)V

    goto :goto_0

    .line 55
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/data/PathMatcher;->getIntVar(I)I

    move-result v6

    .line 56
    .local v6, "bucketId":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 57
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 59
    .local v9, "imageSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v0, Lcom/sec/android/gallery3d/data/HiddenAlbumSet;->PATH_VIDEO:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 61
    .local v10, "videoSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v7, Lcom/sec/android/gallery3d/data/DataManager;->sDateTakenComparator:Ljava/util/Comparator;

    .line 62
    .local v7, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/HiddenSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/MediaSet;

    aput-object v9, v2, v11

    aput-object v10, v2, v5

    invoke-direct {v0, p1, v7, v1, v2}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/gallery3d/data/HiddenSource;->sContentDirty:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 100
    invoke-super {p0}, Lcom/sec/android/gallery3d/data/MediaSource;->pause()V

    .line 101
    return-void
.end method
