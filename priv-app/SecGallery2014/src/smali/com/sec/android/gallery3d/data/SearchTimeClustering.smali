.class public Lcom/sec/android/gallery3d/data/SearchTimeClustering;
.super Lcom/sec/android/gallery3d/data/Clustering;
.source "SearchTimeClustering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/SearchTimeClustering$1;,
        Lcom/sec/android/gallery3d/data/SearchTimeClustering$DateComparator;
    }
.end annotation


# static fields
.field private static final CATEGORY:[I

.field private static final sDateComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCalendar:[Ljava/util/Calendar;

.field private final mContext:Landroid/content/Context;

.field private mItemsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    .line 39
    new-instance v0, Lcom/sec/android/gallery3d/data/SearchTimeClustering$DateComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering$DateComparator;-><init>(Lcom/sec/android/gallery3d/data/SearchTimeClustering$1;)V

    sput-object v0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->sDateComparator:Ljava/util/Comparator;

    return-void

    .line 33
    :array_0
    .array-data 4
        0x7f0e023a
        0x7f0e023c
        0x7f0e023d
        0x7f0e023e
        0x7f0e023f
        0x7f0e0240
        0x7f0e0241
        0x7f0e0242
        0x7f0e0243
        0x7f0e0244
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x7

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/Clustering;-><init>()V

    .line 49
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/util/Calendar;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mNames:Ljava/util/List;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mItemsList:Ljava/util/List;

    .line 54
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mContext:Landroid/content/Context;

    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    aput-object v1, v0, v3

    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v4

    if-nez v0, :cond_1

    .line 66
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v4

    .line 67
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v4

    const/4 v1, -0x1

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->add(II)V

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    if-nez v0, :cond_2

    .line 72
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v2

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/4 v1, -0x3

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->add(II)V

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v7

    if-nez v0, :cond_3

    .line 78
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v7

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v7

    invoke-virtual {v0, v6, v4}, Ljava/util/Calendar;->set(II)V

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    if-nez v0, :cond_4

    .line 84
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v2

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const/4 v1, -0x1

    invoke-virtual {v0, v7, v1}, Ljava/util/Calendar;->add(II)V

    .line 89
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v5

    if-nez v0, :cond_5

    .line 90
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v5

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v5

    invoke-virtual {v0, v5, v4}, Ljava/util/Calendar;->set(II)V

    .line 95
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    if-nez v0, :cond_6

    .line 96
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v2

    .line 97
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 101
    :cond_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v6

    if-nez v0, :cond_7

    .line 102
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v6

    .line 103
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v6

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 107
    :cond_7
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    if-nez v0, :cond_8

    .line 108
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v2

    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    const/4 v1, -0x1

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->add(II)V

    .line 113
    :cond_8
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    if-nez v0, :cond_9

    .line 114
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v2, 0x9

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    aput-object v0, v1, v2

    .line 115
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    const/4 v1, -0x2

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->add(II)V

    .line 117
    :cond_9
    return-void
.end method

.method private addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V
    .locals 3
    .param p1, "category"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Clustering$SmallItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 199
    .local p3, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "strCategory":Ljava/lang/String;
    invoke-virtual {p3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 201
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    if-nez v0, :cond_0

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .restart local v0    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    invoke-virtual {p3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public getCluster(I)Ljava/util/ArrayList;
    .locals 5
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mItemsList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 218
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 219
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 220
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    iget-object v4, v4, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_0
    return-object v3
.end method

.method public getClusterKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 233
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClusterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfClusters()I
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public run(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 14
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 121
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v10

    .line 122
    .local v10, "total":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .local v4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;"
    const/4 v11, 0x0

    invoke-virtual {p0, p1, v4, v10, v11}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;IZ)V

    .line 124
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->sDateComparator:Ljava/util/Comparator;

    invoke-static {v4, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 126
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 127
    .local v6, "n":I
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 129
    .local v1, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Clustering$SmallItem;>;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v6, :cond_b

    .line 130
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;

    .line 131
    .local v3, "item":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    iget-wide v8, v3, Lcom/sec/android/gallery3d/data/Clustering$SmallItem;->dateInMs:J

    .line 132
    .local v8, "t":J
    const-wide/16 v12, 0x0

    cmp-long v11, v8, v12

    if-nez v11, :cond_1

    .line 129
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 134
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 135
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 138
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x0

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 139
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x0

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 143
    :cond_2
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x1

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x0

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 144
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x1

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 148
    :cond_3
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x2

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 149
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x2

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 153
    :cond_4
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x3

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 154
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x3

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 158
    :cond_5
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x4

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x3

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 159
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x4

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 163
    :cond_6
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x5

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 164
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x5

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 168
    :cond_7
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x6

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x5

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 169
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x6

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 173
    :cond_8
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x7

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 174
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/4 v12, 0x7

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 178
    :cond_9
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v12, 0x8

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/4 v12, 0x7

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 179
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/16 v12, 0x8

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    .line 183
    :cond_a
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mCalendar:[Ljava/util/Calendar;

    const/16 v12, 0x9

    aget-object v11, v11, v12

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 184
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    const/16 v12, 0x9

    aget v11, v11, v12

    invoke-direct {p0, v11, v3, v1}, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->addItem(ILcom/sec/android/gallery3d/data/Clustering$SmallItem;Ljava/util/HashMap;)V

    goto/16 :goto_1

    .line 188
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/Clustering$SmallItem;
    .end local v8    # "t":J
    :cond_b
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 189
    .local v5, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x0

    :goto_2
    sget-object v11, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    array-length v11, v11

    if-ge v2, v11, :cond_d

    .line 190
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget-object v12, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->CATEGORY:[I

    aget v12, v12, v2

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 191
    .local v7, "strCategory":Ljava/lang/String;
    invoke-interface {v5, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 192
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mNames:Ljava/util/List;

    invoke-interface {v11, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v11, p0, Lcom/sec/android/gallery3d/data/SearchTimeClustering;->mItemsList:Ljava/util/List;

    invoke-virtual {v1, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 196
    .end local v7    # "strCategory":Ljava/lang/String;
    :cond_d
    return-void
.end method
