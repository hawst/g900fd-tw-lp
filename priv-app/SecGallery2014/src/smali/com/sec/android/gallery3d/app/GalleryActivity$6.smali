.class Lcom/sec/android/gallery3d/app/GalleryActivity$6;
.super Landroid/os/Handler;
.source "GalleryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/GalleryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/GalleryActivity;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 1227
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 1230
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1232
    .local v0, "what":I
    packed-switch v0, :pswitch_data_0

    .line 1259
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1260
    return-void

    .line 1234
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsActive:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$500(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1235
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->onResume()V

    .line 1236
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # setter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z
    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$602(Lcom/sec/android/gallery3d/app/GalleryActivity;Z)Z

    .line 1237
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v4, v2, v3}, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1241
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$600(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1242
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->onPause()V

    goto :goto_0

    .line 1246
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mUpdateBackscreenInPauseState:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$600(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1247
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$700(Lcom/sec/android/gallery3d/app/GalleryActivity;)Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1248
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$700(Lcom/sec/android/gallery3d/app/GalleryActivity;)Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->pause()V

    goto :goto_0

    .line 1250
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->mFinishAtSecrureLock:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$800(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1251
    # getter for: Lcom/sec/android/gallery3d/app/GalleryActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Gallery is finished because GalleryUtils.isCameraQuickViewOnLockscreen() returns true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1252
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivity$6;->this$0:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->finish()V

    goto :goto_0

    .line 1232
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
