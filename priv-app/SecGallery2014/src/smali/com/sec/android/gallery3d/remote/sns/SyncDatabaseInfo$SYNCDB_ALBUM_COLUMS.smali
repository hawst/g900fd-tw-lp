.class public interface abstract Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo$SYNCDB_ALBUM_COLUMS;
.super Ljava/lang/Object;
.source "SyncDatabaseInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/SyncDatabaseInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SYNCDB_ALBUM_COLUMS"
.end annotation


# static fields
.field public static final album_count:Ljava/lang/String; = "count"

.field public static final album_cover_photo:Ljava/lang/String; = "cover_photo"

.field public static final album_created_time:Ljava/lang/String; = "created_time"

.field public static final album_description:Ljava/lang/String; = "description"

.field public static final album_from_id:Ljava/lang/String; = "from_id"

.field public static final album_from_name:Ljava/lang/String; = "from_name"

.field public static final album_id:Ljava/lang/String; = "id"

.field public static final album_link:Ljava/lang/String; = "link"

.field public static final album_location:Ljava/lang/String; = "location"

.field public static final album_name:Ljava/lang/String; = "name"

.field public static final album_privacy:Ljava/lang/String; = "privacy"

.field public static final album_type:Ljava/lang/String; = "type"

.field public static final album_updated_time:Ljava/lang/String; = "updated_time"
