.class Lcom/sec/android/gallery3d/data/ClusterAlbum$4;
.super Ljava/lang/Object;
.source "ClusterAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/ClusterAlbum;->getLocalMediaItemFromPath(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

.field final synthetic val$buf:[Lcom/sec/android/gallery3d/data/MediaItem;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/ClusterAlbum;[Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0

    .prologue
    .line 654
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$4;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    iput-object p2, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$4;->val$buf:[Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 2
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 656
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$4;->val$buf:[Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object p2, v0, p1

    .line 657
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeAllSelectMode:Z

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$4;->val$buf:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/ClusterAlbum$4;->this$0:Lcom/sec/android/gallery3d/data/ClusterAlbum;

    iget-object v1, v1, Lcom/sec/android/gallery3d/data/ClusterAlbum;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->setParentSetPath(Lcom/sec/android/gallery3d/data/Path;)V

    .line 659
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
