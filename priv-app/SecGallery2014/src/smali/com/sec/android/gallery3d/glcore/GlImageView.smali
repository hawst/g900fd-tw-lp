.class public Lcom/sec/android/gallery3d/glcore/GlImageView;
.super Lcom/sec/android/gallery3d/glcore/GlView;
.source "GlImageView.java"


# static fields
.field public static final FIT_VIEW:I = 0x2

.field public static final FULL_VIEW:I = 0x0

.field public static final KEEP_RATIO:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBm:Landroid/graphics/Bitmap;

.field private mCropH:I

.field private mCropRect:Landroid/graphics/Rect;

.field private mCropW:I

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field mFitMode:I

.field private mOutRect:Landroid/graphics/Rect;

.field private mPadding:Landroid/graphics/Rect;

.field private mPaint:Landroid/graphics/Paint;

.field private mResourceID:I

.field protected mResources:Landroid/content/res/Resources;

.field private mRotation:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/glcore/GlImageView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlView;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResources:Landroid/content/res/Resources;

    .line 27
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 28
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    .line 29
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 30
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    .line 31
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropRect:Landroid/graphics/Rect;

    .line 32
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPaint:Landroid/graphics/Paint;

    .line 33
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    .line 34
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mRotation:I

    .line 37
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mFitMode:I

    .line 42
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResources:Landroid/content/res/Resources;

    .line 43
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 44
    return-void
.end method

.method private getRectForKeepRatio(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "outRect"    # Landroid/graphics/Rect;

    .prologue
    .line 200
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    int-to-float v11, v12

    .line 201
    .local v11, "width":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    int-to-float v5, v12

    .line 202
    .local v5, "height":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v12

    int-to-float v2, v12

    .line 203
    .local v2, "canvasW":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getHeight()I

    move-result v12

    int-to-float v1, v12

    .line 209
    .local v1, "canvasH":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getRoot()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/glcore/GlView;->getWidth()I

    move-result v12

    int-to-float v7, v12

    .line 210
    .local v7, "rw":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getRoot()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/glcore/GlView;->getHeight()I

    move-result v12

    int-to-float v6, v12

    .line 212
    .local v6, "rh":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v12, :cond_0

    .line 238
    :goto_0
    return-void

    .line 214
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v12, v12, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v13, v13, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    div-float/2addr v12, v13

    div-float v13, v7, v6

    mul-float v10, v12, v13

    .line 215
    .local v10, "targetRatio":F
    const/high16 v12, 0x3f800000    # 1.0f

    cmpl-float v12, v10, v12

    if-ltz v12, :cond_3

    .line 216
    move v11, v2

    .line 217
    div-float v12, v1, v10

    float-to-int v12, v12

    int-to-float v5, v12

    .line 223
    :goto_1
    sub-float v4, v7, v11

    .line 224
    .local v4, "delW":F
    sub-float v3, v6, v5

    .line 225
    .local v3, "delH":F
    const/4 v12, 0x0

    cmpg-float v12, v4, v12

    if-ltz v12, :cond_1

    const/4 v12, 0x0

    cmpg-float v12, v3, v12

    if-gez v12, :cond_2

    .line 226
    :cond_1
    cmpg-float v12, v3, v4

    if-gez v12, :cond_4

    .line 227
    div-float v12, v6, v5

    mul-float/2addr v11, v12

    .line 228
    move v5, v6

    .line 235
    :cond_2
    :goto_2
    sub-float v12, v2, v11

    const/high16 v13, 0x40000000    # 2.0f

    div-float v8, v12, v13

    .line 236
    .local v8, "sx":F
    sub-float v12, v1, v5

    const/high16 v13, 0x40000000    # 2.0f

    div-float v9, v12, v13

    .line 237
    .local v9, "sy":F
    float-to-int v12, v8

    float-to-int v13, v9

    add-float v14, v8, v11

    float-to-int v14, v14

    add-float v15, v9, v5

    float-to-int v15, v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 219
    .end local v3    # "delH":F
    .end local v4    # "delW":F
    .end local v8    # "sx":F
    .end local v9    # "sy":F
    :cond_3
    move v5, v1

    .line 220
    mul-float v12, v2, v10

    float-to-int v12, v12

    int-to-float v11, v12

    goto :goto_1

    .line 230
    .restart local v3    # "delH":F
    .restart local v4    # "delW":F
    :cond_4
    div-float v12, v7, v11

    mul-float/2addr v5, v12

    .line 231
    move v11, v7

    goto :goto_2
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 271
    const/4 v1, 0x0

    .line 272
    .local v1, "handled":Z
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getChildCount()I

    move-result v0

    .line 274
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 275
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 274
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 277
    :cond_0
    return v1
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCropRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 245
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 248
    if-nez p1, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 252
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mFitMode:I

    if-nez v0, :cond_2

    .line 253
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 257
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getRectForKeepRatio(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    goto :goto_1

    .line 262
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mOutRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method protected onLayout(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 242
    return-void
.end method

.method public setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V
    .locals 10
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "rotationDegree"    # I
    .param p5, "faceRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v9, 0x0

    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mRotation:I

    if-ne v0, p4, :cond_0

    .line 126
    :goto_0
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mRotation:I

    .line 127
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropW:I

    .line 128
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropH:I

    .line 129
    iput v9, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 131
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropW:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropH:I

    invoke-static {v0, v1, v2, p5}, Lcom/sec/android/gallery3d/common/BitmapUtils;->calcCropRect(Landroid/graphics/Bitmap;IILandroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropRect:Landroid/graphics/Rect;

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    .line 133
    :goto_1
    return-void

    .line 108
    :cond_0
    if-nez p4, :cond_1

    .line 109
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 111
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 112
    .local v3, "w":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 113
    .local v4, "h":I
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 114
    .local v5, "mtx":Landroid/graphics/Matrix;
    int-to-float v0, p4

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 116
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    move-object v0, p1

    :try_start_0
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 118
    :catch_0
    move-exception v7

    .line 119
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_1

    .line 121
    .end local v7    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v8

    .line 122
    .local v8, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1
.end method

.method public setCroppedBitmapAlbumTime(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V
    .locals 8
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "rotationDegree"    # I
    .param p5, "faceRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 136
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    if-ne v4, p1, :cond_0

    .line 158
    :goto_0
    return-void

    .line 139
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 141
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-eq p2, v4, :cond_1

    .line 142
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 143
    .local v2, "nb":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 144
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v4, v5, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 145
    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 146
    .local v3, "src":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v1, v6, v6, p2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 147
    .local v1, "dst":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v4, v3, v1, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 148
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 151
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "dst":Landroid/graphics/Rect;
    .end local v2    # "nb":Landroid/graphics/Bitmap;
    .end local v3    # "src":Landroid/graphics/Rect;
    :cond_1
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mRotation:I

    .line 152
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropW:I

    .line 153
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropH:I

    .line 154
    iput v6, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    .line 155
    iput-object v7, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 156
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mCropRect:Landroid/graphics/Rect;

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    goto :goto_0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 162
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    .line 163
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    .line 165
    return-void
.end method

.method public setFitMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 180
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mFitMode:I

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    .line 182
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    if-ne v0, p1, :cond_0

    .line 78
    :goto_0
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 75
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 76
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResources:Landroid/content/res/Resources;

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;I)V
    .locals 7
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "rotationDegree"    # I

    .prologue
    const/4 v1, 0x0

    .line 82
    if-nez p1, :cond_0

    .line 83
    sget-object v0, Lcom/sec/android/gallery3d/glcore/GlImageView;->TAG:Ljava/lang/String;

    const-string v1, "bm is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :goto_0
    return-void

    .line 87
    :cond_0
    if-nez p2, :cond_1

    .line 88
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 91
    :cond_1
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 93
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 94
    .local v3, "w":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 95
    .local v4, "h":I
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 96
    .local v5, "mtx":Landroid/graphics/Matrix;
    int-to-float v0, p2

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 97
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 98
    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 99
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    goto :goto_0
.end method

.method public setImageResource(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    const/4 v1, 0x0

    .line 47
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    if-ne v0, p1, :cond_0

    .line 54
    :goto_0
    return-void

    .line 49
    :cond_0
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 50
    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 51
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    .line 52
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResourceID:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    goto :goto_0
.end method

.method public setImageResource(II)V
    .locals 7
    .param p1, "resId"    # I
    .param p2, "rotationDegree"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 58
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 59
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2, v6, p1}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 60
    .local v0, "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 61
    .local v3, "w":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 62
    .local v4, "h":I
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 63
    .local v5, "mtx":Landroid/graphics/Matrix;
    int-to-float v2, p2

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 64
    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    .line 65
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mResources:Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mBm:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->invalidate()V

    .line 68
    return-void
.end method

.method public setPaddings(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlImageView;->mPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 186
    return-void
.end method
