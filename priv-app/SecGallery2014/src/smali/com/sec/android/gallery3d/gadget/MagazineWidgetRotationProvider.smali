.class public Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;
.super Landroid/content/ContentProvider;
.source "MagazineWidgetRotationProvider.java"


# static fields
.field private static EXTRA_SPAN_X:Ljava/lang/String;

.field private static EXTRA_SPAN_Y:Ljava/lang/String;

.field private static EXTRA_WIDGET_ID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "extra_widget_id"

    sput-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;->EXTRA_WIDGET_ID:Ljava/lang/String;

    .line 16
    const-string v0, "extra_span_x"

    sput-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;->EXTRA_SPAN_X:Ljava/lang/String;

    .line 17
    const-string v0, "extra_span_y"

    sput-object v0, Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;->EXTRA_SPAN_Y:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private magazineWidgetRotation(III)Landroid/os/Bundle;
    .locals 7
    .param p1, "widgetId"    # I
    .param p2, "spanX"    # I
    .param p3, "spanY"    # I

    .prologue
    .line 32
    const/4 v3, 0x0

    .line 33
    .local v3, "isRotationSuccess":Z
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 34
    .local v4, "returnBundle":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 35
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_0

    .line 36
    const/4 v4, 0x0

    .line 48
    .end local v4    # "returnBundle":Landroid/os/Bundle;
    :goto_0
    return-object v4

    .line 37
    .restart local v4    # "returnBundle":Landroid/os/Bundle;
    :cond_0
    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 38
    .local v5, "widgetMgr":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v5, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v2

    .line 39
    .local v2, "info":Landroid/appwidget/AppWidgetProviderInfo;
    const-string v0, ""

    .line 40
    .local v0, "classname":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 41
    iget-object v6, v2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 44
    :cond_1
    const-class v6, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 45
    invoke-static {v1, p1, p2, p3}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetProvider;->magazineWidgetRotation(Landroid/content/Context;III)Z

    .line 47
    :cond_2
    const-string v6, "return magazineWidgetRotation()"

    invoke-virtual {v4, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 5
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, -0x1

    .line 21
    const-string v3, "magazineWidgetRotation()"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 22
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;->EXTRA_SPAN_X:Ljava/lang/String;

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 23
    .local v0, "spanX":I
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;->EXTRA_SPAN_Y:Ljava/lang/String;

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 24
    .local v1, "spanY":I
    sget-object v3, Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;->EXTRA_WIDGET_ID:Ljava/lang/String;

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 26
    .local v2, "widgetId":I
    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/gallery3d/gadget/MagazineWidgetRotationProvider;->magazineWidgetRotation(III)Landroid/os/Bundle;

    move-result-object v3

    .line 28
    .end local v0    # "spanX":I
    .end local v1    # "spanY":I
    .end local v2    # "widgetId":I
    :goto_0
    return-object v3

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v3

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # [Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method
