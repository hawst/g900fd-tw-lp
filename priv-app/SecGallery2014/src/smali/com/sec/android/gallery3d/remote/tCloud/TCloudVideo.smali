.class public Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;
.super Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;
.source "TCloudVideo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;
    }
.end annotation


# static fields
.field private static final INDEX_AVAILABLE_THUMB:I = 0xb

.field private static final INDEX_BUCKET_ID:I = 0x6

.field private static final INDEX_BUCKET_NAME:I = 0x7

.field private static final INDEX_CACHED:I = 0x9

.field private static final INDEX_CACHE_PATH:I = 0x8

.field private static final INDEX_CAPTION:I = 0x3

.field private static final INDEX_DATA:I = 0x1

.field private static final INDEX_DATE_MODIFIED:I = 0x5

.field private static final INDEX_ID:I = 0x0

.field private static final INDEX_MIME_TYPE:I = 0x4

.field private static final INDEX_SIZE:I = 0x2

.field private static final INDEX_THUMBPATH:I = 0xa

.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private final mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->TAG:Ljava/lang/String;

    .line 49
    const-string v0, "/tCloud/video/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V
    .locals 5
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "id"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 76
    new-instance v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 77
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 78
    .local v1, "resolver":Landroid/content/ContentResolver;
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v2, v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseFileUri:Landroid/net/Uri;

    .line 79
    .local v2, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 81
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->getTCloudVideoProjection()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, p3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;->getItemCursor(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    .line 82
    if-nez v0, :cond_0

    .line 83
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "cannot get cursor for: "

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 86
    :cond_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-gtz v3, :cond_1

    .line 87
    sget-object v3, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->TAG:Ljava/lang/String;

    const-string v4, "Cursor getCount is zero"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 90
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->loadFromCursor(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 97
    return-void

    .line 92
    :cond_2
    :try_start_2
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "cannot find data for: "

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 69
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    .line 70
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->loadFromCursor(Landroid/database/Cursor;)V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method private getVideoURL()Ljava/lang/String;
    .locals 8

    .prologue
    .line 211
    new-instance v3, Ljava/util/concurrent/FutureTask;

    new-instance v5, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$1;-><init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;)V

    invoke-direct {v3, v5}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 221
    .local v3, "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/Thread;

    const-string v6, "CloudVideoUrl"

    invoke-direct {v5, v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 223
    const/4 v4, 0x0

    .line 226
    .local v4, "url":Ljava/lang/String;
    const-wide/16 v6, 0x12

    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v6, v7, v5}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 234
    :goto_0
    return-object v4

    .line 227
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 229
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v2

    .line 230
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    goto :goto_0

    .line 231
    .end local v2    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v2

    .line 232
    .local v2, "e":Ljava/util/concurrent/TimeoutException;
    invoke-virtual {v2}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto :goto_0
.end method

.method private loadFromCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    .line 101
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->data:Ljava/lang/String;

    .line 102
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->fileSize:J

    .line 103
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->caption:Ljava/lang/String;

    .line 104
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mimeType:Ljava/lang/String;

    .line 105
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->dateModifiedInSec:J

    .line 106
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->bucketId:I

    .line 107
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->bucketName:Ljava/lang/String;

    .line 108
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->cachedPath:Ljava/lang/String;

    .line 109
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->isCached:I

    .line 110
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->thumbPath:Ljava/lang/String;

    .line 111
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->availablethumb:I

    .line 112
    return-void
.end method


# virtual methods
.method public delete()V
    .locals 4

    .prologue
    .line 250
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 251
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->Videos_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "where":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseVideoUri:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 253
    return-void
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mTCloudRefer:Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->mBaseFileUri:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 6

    .prologue
    .line 193
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 194
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->dateModifiedInSec:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 196
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x4

    return v0
.end method

.method public getPlayUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->getVideoURL()Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "url":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 207
    :goto_0
    return-object v0

    .line 205
    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 206
    .local v0, "uri":Landroid/net/Uri;
    const-string v2, "application/vnd.apple.mpegurl"

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    iget-wide v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 244
    const-wide/16 v0, 0x480

    .line 245
    .local v0, "operation":J
    return-wide v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->thumbPath:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo$ThumbDecodeRequest;-><init>(Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;I)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot regquest a large image to a local video!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected updateFromCursor(Landroid/database/Cursor;)Z
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 116
    new-instance v0, Lcom/sec/android/gallery3d/util/UpdateHelper;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;-><init>()V

    .line 117
    .local v0, "uh":Lcom/sec/android/gallery3d/util/UpdateHelper;
    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->id:J

    .line 118
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->data:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->data:Ljava/lang/String;

    .line 119
    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->fileSize:J

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->fileSize:J

    .line 120
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->caption:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->caption:Ljava/lang/String;

    .line 121
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mimeType:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->mimeType:Ljava/lang/String;

    .line 122
    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->dateModifiedInSec:J

    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->dateModifiedInSec:J

    .line 123
    iget v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->bucketId:I

    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->bucketId:I

    .line 124
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->bucketName:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->bucketName:Ljava/lang/String;

    .line 125
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->cachedPath:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->cachedPath:Ljava/lang/String;

    .line 126
    iget v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->isCached:I

    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->isCached:I

    .line 127
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->thumbPath:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->thumbPath:Ljava/lang/String;

    .line 128
    iget v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->availablethumb:I

    const/16 v2, 0xb

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;->availablethumb:I

    .line 129
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v1

    return v1
.end method
