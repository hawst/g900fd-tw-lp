.class public interface abstract Lcom/sec/android/gallery3d/app/GalleryContext;
.super Ljava/lang/Object;
.source "GalleryContext.java"


# virtual methods
.method public abstract getAndroidContext()Landroid/content/Context;
.end method

.method public abstract getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;
.end method

.method public abstract getAudioManager()Landroid/media/AudioManager;
.end method

.method public abstract getCacheInterface()Lcom/sec/samsung/gallery/decoder/CacheInterface;
.end method

.method public abstract getContentResolver()Landroid/content/ContentResolver;
.end method

.method public abstract getDataManager()Lcom/sec/android/gallery3d/data/DataManager;
.end method

.method public abstract getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;
.end method

.method public abstract getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;
.end method

.method public abstract getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;
.end method

.method public abstract getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;
.end method

.method public abstract getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;
.end method

.method public abstract getGalleryId()I
.end method

.method public abstract getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;
.end method

.method public abstract getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;
.end method

.method public abstract getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;
.end method

.method public abstract getMainLooper()Landroid/os/Looper;
.end method

.method public abstract getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
.end method

.method public abstract getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;
.end method

.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;
.end method

.method public abstract getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;
.end method

.method public abstract isContactsChanged()Z
.end method

.method public abstract isInTouchMode()Z
.end method

.method public abstract registerFaceRecommendationObserver(Landroid/net/Uri;Z)V
.end method
