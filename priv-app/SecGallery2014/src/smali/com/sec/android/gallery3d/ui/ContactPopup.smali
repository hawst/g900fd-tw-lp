.class public Lcom/sec/android/gallery3d/ui/ContactPopup;
.super Landroid/widget/LinearLayout;
.source "ContactPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;
    }
.end annotation


# static fields
.field private static final CANCEL_KEY:I = 0x20

.field public static final CHATON_PACKAGE_NAME:Ljava/lang/String; = "com.sec.chaton"

.field private static final FACEBOOK:Ljava/lang/String; = "Facebook"

.field public static final FACEBOOK_PACKAGE_NAME:Ljava/lang/String; = "com.facebook.katana"

.field public static final FACEBOOK_PROFILE_MIMETYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.facebook.profile"

.field private static final GOOGLEPLUS:Ljava/lang/String; = "Google+"

.field public static final GOOGLEPLUS_PACKAGE_NAME:Ljava/lang/String; = "com.google.android.apps.plus"

.field private static final GOOGLEPLUS_PROFILE_MIMETYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.googleplus.profile"

.field private static final MORE_CLICK_KEY:I = 0x10

.field private static final SettingDialog_EditTag:I = 0x0

.field private static final SettingDialog_RemoveTag:I = 0x2

.field private static final SettingDialog_SetAsCallerId:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ContactPopup"

.field private static final TWITTER:Ljava/lang/String; = "Twitter"

.field public static final TWITTER_PACKAGE_NAME:Ljava/lang/String; = "com.twitter.android"

.field private static final TWITTER_PROFILE_MIMETYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.twitter.profile"


# instance fields
.field private mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

.field private mCall:Landroid/widget/ImageButton;

.field private mContactButtonCount:I

.field private mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

.field private mContactName:Landroid/widget/TextView;

.field private final mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

.field private final mContext:Landroid/content/Context;

.field private mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mEmail:Landroid/widget/ImageButton;

.field private mHead:Landroid/widget/ImageView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsClicked:Z

.field private mIsMe:Z

.field private mLayout:Landroid/widget/LinearLayout;

.field private mMessage:Landroid/widget/ImageButton;

.field private mMore:Landroid/widget/ImageButton;

.field private mMoreDialog:Landroid/app/AlertDialog;

.field private mRectScale:Landroid/graphics/RectF;

.field private final mRes:Landroid/content/res/Resources;

.field private mSnsIcon:Landroid/widget/ImageView;

.field private mSnsMsg:Landroid/widget/TextView;

.field private mSnsTimeStamp:Landroid/widget/TextView;

.field private mSocial:Landroid/widget/ImageButton;

.field private mWithSNS:Z

.field private moreButtonListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 282
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/ui/ContactPopup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 283
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/gallery3d/ui/ContactPopup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 290
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    .line 93
    iput v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    .line 316
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mHead:Landroid/widget/ImageView;

    .line 317
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z

    .line 319
    new-instance v0, Lcom/sec/android/gallery3d/ui/ContactPopup$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$2;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->moreButtonListener:Landroid/view/View$OnClickListener;

    .line 822
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsMe:Z

    .line 291
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    .line 292
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRes:Landroid/content/res/Resources;

    .line 293
    new-instance v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-direct {v0, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    .line 294
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->init()V

    .line 295
    return-void
.end method

.method private IsInstallPackage(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 902
    const/4 v4, 0x0

    .line 905
    .local v4, "result":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    const/4 v6, 0x0

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 906
    .local v3, "mainIntent":Landroid/content/Intent;
    const-string v5, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 907
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 910
    .local v2, "mApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 911
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 912
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 916
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    :cond_1
    return-object v4
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/ui/ContactPopup;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->gotoSnsWallEx(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/ui/ContactPopup;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->gotoSnsWall(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/ContactPopup;)Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/gallery3d/ui/ContactPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/ContactPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->selectItemSettingDialog(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/ContactPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/gallery3d/ui/ContactPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/ContactPopup;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->resetButtonsClickable(IZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/ContactPopup;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ContactPopup;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 267
    const/4 v1, 0x0

    .line 268
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 270
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/16 v4, 0x80

    :try_start_0
    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 272
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 278
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return-object v1

    .line 274
    :catch_0
    move-exception v2

    .line 275
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private gotoSnsWall(Ljava/lang/String;Ljava/lang/String;)V
    .locals 18
    .param p1, "lookupKey"    # Ljava/lang/String;
    .param p2, "app"    # Ljava/lang/String;

    .prologue
    .line 743
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 803
    :cond_0
    :goto_0
    return-void

    .line 747
    :cond_1
    sget-object v7, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v15, "entities"

    invoke-virtual {v7, v15}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 753
    .local v3, "uri":Landroid/net/Uri;
    const-string v7, "Google+"

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 754
    const-string v5, "mimetype = ? and data4 == 30"

    .line 758
    .local v5, "where":Ljava/lang/String;
    :goto_1
    const/4 v7, 0x4

    new-array v4, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v15, "contact_id"

    aput-object v15, v4, v7

    const/4 v7, 0x1

    const-string v15, "data_id"

    aput-object v15, v4, v7

    const/4 v7, 0x2

    const-string v15, "mimetype"

    aput-object v15, v4, v7

    const/4 v7, 0x3

    const-string v15, "data1"

    aput-object v15, v4, v7

    .line 762
    .local v4, "QUREY_PRO":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 763
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 764
    .local v6, "selections":[Ljava/lang/String;
    const-string v7, "Facebook"

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 765
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    .end local v6    # "selections":[Ljava/lang/String;
    const/4 v7, 0x0

    const-string/jumbo v15, "vnd.android.cursor.item/vnd.facebook.profile"

    aput-object v15, v6, v7

    .line 772
    .restart local v6    # "selections":[Ljava/lang/String;
    :cond_2
    :goto_2
    const/4 v8, 0x0

    .line 773
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 774
    .local v10, "dataID":I
    const/4 v14, 0x0

    .line 777
    .local v14, "mimeType":Ljava/lang/String;
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 779
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 780
    const/4 v7, 0x1

    invoke-interface {v8, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 781
    const/4 v7, 0x2

    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 786
    :cond_3
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 789
    :goto_3
    if-lez v10, :cond_0

    if-eqz v14, :cond_0

    .line 790
    sget-object v11, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 791
    .local v11, "dataUri":Landroid/net/Uri;
    int-to-long v0, v10

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v11, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    .line 792
    .local v9, "data":Landroid/net/Uri;
    new-instance v13, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v13, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 793
    .local v13, "intent":Landroid/content/Intent;
    invoke-virtual {v13, v9, v14}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 796
    :try_start_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v13}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 797
    :catch_0
    move-exception v12

    .line 798
    .local v12, "e":Landroid/content/ActivityNotFoundException;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    const v15, 0x7f0e0062

    invoke-static {v7, v15}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 799
    invoke-virtual {v12}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 756
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v4    # "QUREY_PRO":[Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    .end local v6    # "selections":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v9    # "data":Landroid/net/Uri;
    .end local v10    # "dataID":I
    .end local v11    # "dataUri":Landroid/net/Uri;
    .end local v12    # "e":Landroid/content/ActivityNotFoundException;
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v14    # "mimeType":Ljava/lang/String;
    :cond_4
    const-string v5, "mimetype = ?"

    .restart local v5    # "where":Ljava/lang/String;
    goto/16 :goto_1

    .line 766
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v4    # "QUREY_PRO":[Ljava/lang/String;
    .restart local v6    # "selections":[Ljava/lang/String;
    :cond_5
    const-string v7, "Google+"

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 767
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    .end local v6    # "selections":[Ljava/lang/String;
    const/4 v7, 0x0

    const-string/jumbo v15, "vnd.android.cursor.item/vnd.googleplus.profile"

    aput-object v15, v6, v7

    .restart local v6    # "selections":[Ljava/lang/String;
    goto :goto_2

    .line 768
    :cond_6
    const-string v7, "Twitter"

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 769
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    .end local v6    # "selections":[Ljava/lang/String;
    const/4 v7, 0x0

    const-string/jumbo v15, "vnd.android.cursor.item/vnd.twitter.profile"

    aput-object v15, v6, v7

    .restart local v6    # "selections":[Ljava/lang/String;
    goto :goto_2

    .line 783
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "dataID":I
    .restart local v14    # "mimeType":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 784
    .local v12, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 786
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_3

    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v7
.end method

.method private gotoSnsWallEx(Ljava/lang/String;)V
    .locals 12
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 622
    iget-object v8, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getAccountTypeNdataSet(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 624
    .local v0, "accounts":[Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 625
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 626
    .local v6, "snsAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 629
    array-length v4, v0

    .line 630
    .local v4, "length":I
    if-lez v4, :cond_5

    .line 631
    aget-object v3, v0, v10

    .line 632
    .local v3, "item":Ljava/lang/String;
    const-string v8, "com.google/plus"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.facebook.auth.login"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 634
    :cond_0
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 636
    :cond_1
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_5

    .line 637
    aget-object v8, v0, v2

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 636
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 639
    :cond_3
    const-string v8, "com.google/plus"

    aget-object v9, v0, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "com.facebook.auth.login"

    aget-object v9, v0, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 641
    :cond_4
    aget-object v3, v0, v2

    .line 642
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 647
    .end local v2    # "i":I
    .end local v3    # "item":Ljava/lang/String;
    :cond_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_9

    .line 648
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 649
    .local v5, "size":I
    new-array v7, v5, [Ljava/lang/String;

    .line 650
    .local v7, "snsApps":[Ljava/lang/String;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    if-ge v2, v5, :cond_8

    .line 651
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v9, "com.google/plus"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 652
    const-string v8, "Google+"

    aput-object v8, v7, v2

    .line 650
    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 653
    :cond_7
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v9, "com.facebook.auth.login"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 654
    const-string v8, "Facebook"

    aput-object v8, v7, v2

    goto :goto_3

    .line 658
    :cond_8
    array-length v8, v7

    if-ne v8, v11, :cond_a

    .line 659
    aget-object v8, v7, v10

    invoke-direct {p0, p1, v8}, Lcom/sec/android/gallery3d/ui/ContactPopup;->gotoSnsWall(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    .end local v2    # "i":I
    .end local v4    # "length":I
    .end local v5    # "size":I
    .end local v6    # "snsAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "snsApps":[Ljava/lang/String;
    :cond_9
    :goto_4
    const-string v8, "ContactPopup"

    const-string v9, "acctount"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    return-void

    .line 660
    .restart local v2    # "i":I
    .restart local v4    # "length":I
    .restart local v5    # "size":I
    .restart local v6    # "snsAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "snsApps":[Ljava/lang/String;
    :cond_a
    array-length v8, v7

    if-le v8, v11, :cond_9

    .line 663
    new-instance v8, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0e00bb

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    new-instance v9, Lcom/sec/android/gallery3d/ui/ContactPopup$8;

    invoke-direct {v9, p0, p1, v7}, Lcom/sec/android/gallery3d/ui/ContactPopup$8;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v8, v7, v9}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f0e0046

    new-instance v10, Lcom/sec/android/gallery3d/ui/ContactPopup$7;

    invoke-direct {v10, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$7;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 681
    .local v1, "dlg":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_4
.end method

.method private init()V
    .locals 2

    .prologue
    .line 866
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mInflater:Landroid/view/LayoutInflater;

    .line 868
    return-void
.end method

.method private reloadWidget()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const v4, 0x7f020099

    .line 463
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPhone(Landroid/content/Context;)Z

    move-result v0

    .line 464
    .local v0, "hasCall":Z
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSmsIntentRegistered(Landroid/content/Context;)Z

    move-result v1

    .line 466
    .local v1, "hasSms":Z
    const/4 v2, 0x4

    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    .line 468
    const v2, 0x7f0f0091

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    .line 469
    const v2, 0x7f0f0092

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    .line 470
    const v2, 0x7f0f0093

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    .line 471
    const v2, 0x7f0f0094

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    .line 472
    const v2, 0x7f0f008f

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMore:Landroid/widget/ImageButton;

    .line 473
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMore:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->moreButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    const v2, 0x7f0f008e

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactName:Landroid/widget/TextView;

    .line 475
    const v2, 0x7f0f008c

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mHead:Landroid/widget/ImageView;

    .line 477
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    if-eqz v2, :cond_0

    .line 478
    const v2, 0x7f0f0097

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsIcon:Landroid/widget/ImageView;

    .line 479
    const v2, 0x7f0f0096

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsMsg:Landroid/widget/TextView;

    .line 480
    const v2, 0x7f0f0098

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsTimeStamp:Landroid/widget/TextView;

    .line 483
    :cond_0
    if-nez v0, :cond_1

    .line 484
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 485
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    .line 487
    :cond_1
    if-nez v1, :cond_2

    .line 488
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 489
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    .line 492
    :cond_2
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_8

    .line 493
    if-nez v0, :cond_3

    .line 494
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 496
    :cond_3
    if-nez v1, :cond_4

    .line 497
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 503
    :cond_4
    :goto_0
    if-eqz v0, :cond_5

    .line 504
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/gallery3d/ui/ContactPopup$3;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$3;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 532
    :cond_5
    if-eqz v1, :cond_9

    .line 533
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/gallery3d/ui/ContactPopup$4;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$4;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 562
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    if-eqz v2, :cond_6

    .line 563
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/gallery3d/ui/ContactPopup$5;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$5;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    if-eqz v2, :cond_7

    .line 591
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/gallery3d/ui/ContactPopup$6;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$6;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 608
    :cond_7
    return-void

    .line 499
    :cond_8
    iget v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactButtonCount:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 500
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0

    .line 559
    :cond_9
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1
.end method

.method private removeFace(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 988
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    .line 989
    invoke-static {p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceList;->remove(Landroid/content/Context;I)V

    .line 990
    return-void
.end method

.method private resetButtonsClickable(IZ)V
    .locals 1
    .param p1, "buttonId"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 971
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getId()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 972
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 973
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getId()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 974
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 975
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getId()I

    move-result v0

    if-eq p1, v0, :cond_2

    .line 976
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 977
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getId()I

    move-result v0

    if-eq p1, v0, :cond_3

    .line 978
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 979
    :cond_3
    return-void
.end method

.method private selectItemSettingDialog(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, -0x1

    .line 392
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z

    if-eqz v0, :cond_1

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z

    .line 397
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 399
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    if-eqz v0, :cond_2

    .line 402
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;->onClicked(I)V

    .line 404
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->callContactList()V

    goto :goto_0

    .line 408
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    if-eqz v0, :cond_0

    .line 409
    const-string v0, "profile"

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v1, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v1, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->checkWritableAccountWithLookupkey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 410
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v0, v0, Lcom/sec/samsung/gallery/access/contact/ContactData;->mAutoGroup:I

    if-ne v0, v2, :cond_5

    .line 411
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v1, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->removeFace(Landroid/content/Context;I)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    if-eqz v0, :cond_4

    .line 413
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;->cancelCurIndex()V

    .line 419
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;->onClicked(I)V

    goto :goto_0

    .line 416
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v1, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setFaceUnknown(Landroid/content/Context;I)V

    goto :goto_1

    .line 423
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->startCropActivity()V

    .line 425
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    const/16 v1, 0x20

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;->onClicked(I)V

    goto :goto_0

    .line 433
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v0, v0, Lcom/sec/samsung/gallery/access/contact/ContactData;->mAutoGroup:I

    if-ne v0, v2, :cond_8

    .line 435
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v1, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->removeFace(Landroid/content/Context;I)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    if-eqz v0, :cond_7

    .line 437
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;->cancelCurIndex()V

    .line 443
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;->onClicked(I)V

    goto/16 :goto_0

    .line 440
    :cond_8
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget v1, v1, Lcom/sec/samsung/gallery/access/contact/ContactData;->mFaceId:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setFaceUnknown(Landroid/content/Context;I)V

    goto :goto_2

    .line 397
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setButtonEnabled(IZ)V
    .locals 1
    .param p1, "buttonId"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 806
    packed-switch p1, :pswitch_data_0

    .line 820
    :cond_0
    :goto_0
    return-void

    .line 808
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 812
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 816
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 806
    :pswitch_data_0
    .packed-switch 0x7f0f0091
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setFaceUnknown(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I

    .prologue
    .line 993
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    .line 994
    invoke-static {p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceList;->setFaceUnknown(Landroid/content/Context;I)V

    .line 995
    return-void
.end method

.method private setPersonId(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "faceId"    # I
    .param p3, "personId"    # I

    .prologue
    .line 983
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V

    .line 984
    invoke-static {p1, p2, p3}, Lcom/sec/samsung/gallery/access/face/FaceList;->setPersonId(Landroid/content/Context;II)V

    .line 985
    return-void
.end method

.method private startCropActivity()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 998
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.android.camera.action.CROP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 999
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1000
    const-string v3, "is-caller-id"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1001
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRectScale:Landroid/graphics/RectF;

    if-eqz v3, :cond_0

    .line 1002
    const/4 v3, 0x4

    new-array v2, v3, [F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRectScale:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    aput v4, v2, v3

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRectScale:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    aput v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRectScale:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    aput v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRectScale:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    aput v4, v2, v3

    .line 1003
    .local v2, "values":[F
    const-string v3, "face-position"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;

    .line 1005
    .end local v2    # "values":[F
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 1006
    .local v0, "data":Landroid/net/Uri;
    const-string v3, "aspectX"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1007
    const-string v3, "aspectY"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1008
    const-string v3, "outputX"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCropSizeforSetCallerID(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1009
    const-string v3, "outputY"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCropSizeforSetCallerID(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1010
    const-string v3, "return-data"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1011
    const-string v3, "output"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/app/CropImage;->getCallerIdSavePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1012
    const-string v3, "lookupKeyFromContactPopup"

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v4, v4, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1014
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1015
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    const/16 v4, 0x305

    invoke-virtual {v3, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1016
    return-void
.end method

.method private trasnTimeString(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "snsTimeStamp"    # Ljava/lang/String;

    .prologue
    .line 251
    const/4 v7, 0x0

    .line 252
    .local v7, "timestampDisplayValue":Ljava/lang/CharSequence;
    if-eqz p1, :cond_0

    .line 253
    const-wide/16 v2, 0xc

    invoke-static {p1, v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->parseDate(Ljava/lang/String;J)J

    move-result-wide v0

    .line 257
    .local v0, "statusTimestamp":J
    const/high16 v6, 0x40000

    .line 258
    .local v6, "flags":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v7

    .line 263
    .end local v0    # "statusTimestamp":J
    .end local v6    # "flags":I
    :cond_0
    return-object v7
.end method


# virtual methods
.method public callContactList()V
    .locals 3

    .prologue
    .line 454
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    aput-object v2, v0, v1

    .line 457
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "START_CONTACT_PICK"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 458
    return-void
.end method

.method public checkSocialButton()V
    .locals 11

    .prologue
    .line 690
    const/4 v5, 0x0

    .line 691
    .local v5, "result":Z
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v4, v9, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    .line 692
    .local v4, "lookupKey":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    invoke-virtual {v9, v4}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getAccountTypeNdataSet(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 694
    .local v0, "accounts":[Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    if-nez v9, :cond_0

    .line 740
    :goto_0
    return-void

    .line 697
    :cond_0
    if-eqz v0, :cond_3

    .line 698
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 699
    .local v7, "snsAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 702
    array-length v3, v0

    .line 703
    .local v3, "length":I
    if-lez v3, :cond_2

    .line 704
    const/4 v9, 0x0

    aget-object v2, v0, v9

    .line 705
    .local v2, "item":Ljava/lang/String;
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 706
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 707
    aget-object v9, v0, v1

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 706
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 710
    :cond_1
    aget-object v2, v0, v1

    .line 711
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 716
    .end local v1    # "i":I
    .end local v2    # "item":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 717
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 718
    .local v6, "size":I
    new-array v8, v6, [Ljava/lang/String;

    .line 719
    .local v8, "snsApps":[Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    if-ge v1, v6, :cond_3

    .line 720
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v10, "com.google/plus"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 722
    const-string v9, "Google+"

    aput-object v9, v8, v1

    .line 723
    const/4 v5, 0x1

    .line 739
    .end local v1    # "i":I
    .end local v3    # "length":I
    .end local v6    # "size":I
    .end local v7    # "snsAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "snsApps":[Ljava/lang/String;
    :cond_3
    :goto_4
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    invoke-virtual {v9, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 725
    .restart local v1    # "i":I
    .restart local v3    # "length":I
    .restart local v6    # "size":I
    .restart local v7    # "snsAccounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8    # "snsApps":[Ljava/lang/String;
    :cond_4
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v10, "com.facebook.auth.login"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 727
    const-string v9, "Facebook"

    aput-object v9, v8, v1

    .line 728
    const/4 v5, 0x1

    .line 729
    goto :goto_4

    .line 719
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public contains(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, 0x0

    .line 950
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getLeft()I

    move-result v2

    if-ge p1, v2, :cond_1

    .line 963
    :cond_0
    :goto_0
    return v1

    .line 953
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getRight()I

    move-result v2

    if-gt p1, v2, :cond_0

    .line 956
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    .line 957
    .local v0, "headHeight":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getTop()I

    move-result v2

    add-int/2addr v2, v0

    if-lt p2, v2, :cond_0

    .line 960
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getBottom()I

    move-result v2

    if-gt p2, v2, :cond_0

    .line 963
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 891
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 892
    :cond_0
    return-void
.end method

.method protected getButtonState()Z
    .locals 1

    .prologue
    .line 615
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z

    return v0
.end method

.method public isMoreDialogShow()Z
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    const/4 v0, 0x1

    .line 898
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBtnClickedListener(Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mBtnClickListener:Lcom/sec/android/gallery3d/ui/ContactPopup$onBtnClickedListener;

    .line 124
    return-void
.end method

.method public setButtonState(Z)V
    .locals 0
    .param p1, "isclicked"    # Z

    .prologue
    .line 611
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsClicked:Z

    .line 612
    return-void
.end method

.method public setContactData(Lcom/sec/samsung/gallery/access/contact/ContactData;)V
    .locals 13
    .param p1, "data"    # Lcom/sec/samsung/gallery/access/contact/ContactData;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    .line 128
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    if-eqz v11, :cond_c

    .line 129
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    if-eqz v11, :cond_1

    .line 130
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v11, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->contactName:Ljava/lang/String;

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->splitDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 131
    .local v9, "title":Ljava/lang/String;
    if-eqz v9, :cond_1

    .line 132
    const-string v11, "Me"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 133
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e00c0

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 135
    :cond_0
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v11, v9}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 139
    .end local v9    # "title":Ljava/lang/String;
    :cond_1
    const-string v11, "profile"

    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v12, v12, Lcom/sec/samsung/gallery/access/contact/ContactData;->lookupKey:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    iput-boolean v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsMe:Z

    .line 141
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v2, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->contactName:Ljava/lang/String;

    .line 144
    .local v2, "displayName":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 145
    const-string v11, "/"

    invoke-virtual {v2, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 146
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 147
    .local v10, "vs":[Ljava/lang/String;
    const/4 v11, 0x1

    aget-object v2, v10, v11

    .line 153
    const-string v11, "Me"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 154
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e00c0

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 156
    .end local v10    # "vs":[Ljava/lang/String;
    :cond_2
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactName:Landroid/widget/TextView;

    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    :cond_3
    iget-boolean v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mIsMe:Z

    if-eqz v11, :cond_d

    .line 160
    const v11, 0x7f0f0090

    invoke-virtual {p0, v11}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 161
    .local v4, "layout":Landroid/widget/LinearLayout;
    const/16 v11, 0x8

    invoke-virtual {v4, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 163
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    if-eqz v11, :cond_4

    .line 164
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 166
    :cond_4
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    if-eqz v11, :cond_5

    .line 167
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 169
    :cond_5
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    if-eqz v11, :cond_6

    .line 170
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 172
    :cond_6
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    if-eqz v11, :cond_7

    .line 173
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 194
    .end local v4    # "layout":Landroid/widget/LinearLayout;
    :cond_7
    :goto_0
    iget-boolean v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    if-eqz v11, :cond_b

    .line 195
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v11, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsMsg:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 196
    .local v5, "message":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v11, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->description:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "description":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v11, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->story:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 199
    .local v7, "story":Ljava/lang/String;
    const/4 v6, 0x0

    .line 200
    .local v6, "sns":Ljava/lang/String;
    if-eqz v5, :cond_11

    const-string v11, ""

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_11

    .line 201
    move-object v6, v5

    .line 208
    :cond_8
    :goto_1
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsMsg:Landroid/widget/TextView;

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsMsg:Landroid/widget/TextView;

    new-instance v12, Lcom/sec/android/gallery3d/ui/ContactPopup$1;

    invoke-direct {v12, p0}, Lcom/sec/android/gallery3d/ui/ContactPopup$1;-><init>(Lcom/sec/android/gallery3d/ui/ContactPopup;)V

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    const/4 v8, 0x0

    .line 224
    .local v8, "timeAgo":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v11, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsTimeStamp:Ljava/lang/String;

    invoke-direct {p0, v11}, Lcom/sec/android/gallery3d/ui/ContactPopup;->trasnTimeString(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 225
    .local v0, "charSeq":Ljava/lang/CharSequence;
    if-eqz v0, :cond_9

    .line 226
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 228
    :cond_9
    if-eqz v8, :cond_a

    .line 229
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsTimeStamp:Landroid/widget/TextView;

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    :cond_a
    iget-object v11, p1, Lcom/sec/samsung/gallery/access/contact/ContactData;->snsPackage:Ljava/lang/String;

    invoke-direct {p0, v11}, Lcom/sec/android/gallery3d/ui/ContactPopup;->getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 232
    .local v3, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_b

    .line 233
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsIcon:Landroid/widget/ImageView;

    invoke-virtual {v11, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 246
    .end local v0    # "charSeq":Ljava/lang/CharSequence;
    .end local v1    # "description":Ljava/lang/String;
    .end local v3    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v5    # "message":Ljava/lang/String;
    .end local v6    # "sns":Ljava/lang/String;
    .end local v7    # "story":Ljava/lang/String;
    .end local v8    # "timeAgo":Ljava/lang/String;
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->checkSocialButton()V

    .line 248
    .end local v2    # "displayName":Ljava/lang/String;
    :cond_c
    return-void

    .line 175
    .restart local v2    # "displayName":Ljava/lang/String;
    :cond_d
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v11, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->phoneNum:Ljava/lang/String;

    if-eqz v11, :cond_e

    .line 176
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    invoke-virtual {v11}, Landroid/widget/ImageButton;->getId()I

    move-result v11

    const/4 v12, 0x1

    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonEnabled(IZ)V

    .line 177
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    invoke-virtual {v11}, Landroid/widget/ImageButton;->getId()I

    move-result v11

    const/4 v12, 0x1

    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonEnabled(IZ)V

    .line 183
    :goto_2
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContactData:Lcom/sec/samsung/gallery/access/contact/ContactData;

    iget-object v11, v11, Lcom/sec/samsung/gallery/access/contact/ContactData;->email:Ljava/lang/String;

    if-eqz v11, :cond_10

    .line 184
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v11, :cond_f

    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmailAvailable(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_f

    .line 185
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    invoke-virtual {v11}, Landroid/widget/ImageButton;->getId()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonEnabled(IZ)V

    goto/16 :goto_0

    .line 179
    :cond_e
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCall:Landroid/widget/ImageButton;

    invoke-virtual {v11}, Landroid/widget/ImageButton;->getId()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonEnabled(IZ)V

    .line 180
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMessage:Landroid/widget/ImageButton;

    invoke-virtual {v11}, Landroid/widget/ImageButton;->getId()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonEnabled(IZ)V

    goto :goto_2

    .line 187
    :cond_f
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    invoke-virtual {v11}, Landroid/widget/ImageButton;->getId()I

    move-result v11

    const/4 v12, 0x1

    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonEnabled(IZ)V

    goto/16 :goto_0

    .line 190
    :cond_10
    iget-object v11, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mEmail:Landroid/widget/ImageButton;

    invoke-virtual {v11}, Landroid/widget/ImageButton;->getId()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setButtonEnabled(IZ)V

    goto/16 :goto_0

    .line 202
    .restart local v1    # "description":Ljava/lang/String;
    .restart local v5    # "message":Ljava/lang/String;
    .restart local v6    # "sns":Ljava/lang/String;
    .restart local v7    # "story":Ljava/lang/String;
    :cond_11
    if-eqz v1, :cond_12

    const-string v11, ""

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_12

    .line 203
    move-object v6, v1

    goto/16 :goto_1

    .line 204
    :cond_12
    if-eqz v7, :cond_8

    const-string v11, ""

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 205
    move-object v6, v7

    goto/16 :goto_1
.end method

.method public setCurFaceInfo(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Rect;)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 943
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 944
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcWidth()I

    move-result v1

    .line 945
    .local v1, "w":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mCurMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getArcHeight()I

    move-result v0

    .line 946
    .local v0, "h":I
    new-instance v2, Landroid/graphics/RectF;

    iget v3, p2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    int-to-float v4, v1

    div-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    int-to-float v5, v0

    div-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    int-to-float v6, v1

    div-float/2addr v5, v6

    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    int-to-float v7, v0

    div-float/2addr v6, v7

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRectScale:Landroid/graphics/RectF;

    .line 947
    return-void
.end method

.method public setHeadPosition(III)V
    .locals 4
    .param p1, "snsLeft"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 298
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mHead:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 299
    sub-int/2addr p2, p1

    .line 300
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 301
    .local v1, "popup_w":I
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v2, :cond_0

    .line 302
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 303
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f0d03d8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 304
    .local v0, "head_w":I
    div-int/lit8 v2, v0, 0x2

    if-ge p2, v2, :cond_3

    .line 305
    div-int/lit8 p2, v0, 0x2

    .line 311
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mHead:Landroid/widget/ImageView;

    int-to-float v3, p2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 314
    .end local v0    # "head_w":I
    .end local v1    # "popup_w":I
    :cond_2
    return-void

    .line 306
    .restart local v0    # "head_w":I
    .restart local v1    # "popup_w":I
    :cond_3
    mul-int/lit8 v2, v0, 0x3

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v1, v2

    if-le p2, v2, :cond_1

    .line 307
    mul-int/lit8 v2, v0, 0x3

    div-int/lit8 v2, v2, 0x2

    sub-int p2, v1, v2

    goto :goto_0
.end method

.method public setPopupType(Z)V
    .locals 6
    .param p1, "withSNS"    # Z

    .prologue
    const v4, 0x7f030039

    const/4 v3, 0x0

    .line 825
    if-eqz p1, :cond_3

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    .line 826
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    .line 827
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 829
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v2, :cond_5

    .line 830
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mInflater:Landroid/view/LayoutInflater;

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    if-eqz v2, :cond_4

    move v2, v4

    :goto_1
    invoke-virtual {v5, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mLayout:Landroid/widget/LinearLayout;

    .line 839
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->reloadWidget()V

    .line 840
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    if-eqz v2, :cond_7

    move v1, v3

    .line 842
    .local v1, "visible":I
    :goto_3
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    .line 843
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 846
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    if-eqz v2, :cond_8

    .line 847
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsMsg:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 848
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 849
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSnsTimeStamp:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 850
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    .line 851
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 861
    :cond_2
    :goto_4
    return-void

    .end local v1    # "visible":I
    :cond_3
    move v2, v3

    .line 825
    goto :goto_0

    .line 830
    :cond_4
    const v2, 0x7f030035

    goto :goto_1

    .line 834
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mInflater:Landroid/view/LayoutInflater;

    iget-boolean v5, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    if-eqz v5, :cond_6

    :goto_5
    invoke-virtual {v2, v4, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mLayout:Landroid/widget/LinearLayout;

    goto :goto_2

    :cond_6
    const v4, 0x7f030034

    goto :goto_5

    .line 840
    :cond_7
    const/16 v1, 0x8

    goto :goto_3

    .line 854
    .restart local v1    # "visible":I
    :cond_8
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag:Z

    if-nez v2, :cond_2

    .line 855
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mSocial:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 856
    const v2, 0x7f0f0093

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ContactPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 857
    .local v0, "emailBtn":Landroid/widget/ImageButton;
    if-eqz v0, :cond_2

    .line 858
    const v2, 0x7f02009b

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_4
.end method

.method public show(Z)V
    .locals 1
    .param p1, "vis"    # Z

    .prologue
    .line 871
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/ContactPopup;->setVisibility(I)V

    .line 872
    return-void

    .line 871
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public showMoreDialog(Z)V
    .locals 1
    .param p1, "vis"    # Z

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-ne p1, v0, :cond_1

    .line 887
    :cond_0
    :goto_0
    return-void

    .line 879
    :cond_1
    if-eqz p1, :cond_2

    .line 880
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 882
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mMoreDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method

.method public startSocialhub(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 921
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/ContactPopup;->IsInstallPackage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 923
    .local v0, "className":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 924
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 925
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, p1, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 926
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 927
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 929
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 939
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 930
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 931
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "ContactPopup"

    const-string v4, "Activity Not Found"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 937
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v3, "ContactPopup"

    const-string v4, "not install"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public withSNS()Z
    .locals 1

    .prologue
    .line 967
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ContactPopup;->mWithSNS:Z

    return v0
.end method
