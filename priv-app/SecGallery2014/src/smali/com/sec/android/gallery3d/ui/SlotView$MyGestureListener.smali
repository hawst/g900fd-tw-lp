.class Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;
.super Ljava/lang/Object;
.source "SlotView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/SlotView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyGestureListener"
.end annotation


# instance fields
.field private isDown:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/SlotView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/SlotView;)V
    .locals 0

    .prologue
    .line 683
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/SlotView;Lcom/sec/android/gallery3d/ui/SlotView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/SlotView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/SlotView$1;

    .prologue
    .line 683
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;-><init>(Lcom/sec/android/gallery3d/ui/SlotView;)V

    return-void
.end method

.method private cancelDown(Z)V
    .locals 1
    .param p1, "byLongPress"    # Z

    .prologue
    .line 707
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->isDown:Z

    if-nez v0, :cond_0

    .line 710
    :goto_0
    return-void

    .line 708
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->isDown:Z

    .line 709
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/SlotView;->access$900(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/gallery3d/ui/SlotView$Listener;->onUp(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 714
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v2, 0x0

    .line 720
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->cancelDown(Z)V

    .line 721
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/SlotView;->access$800(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getScrollLimit()I

    move-result v0

    .line 722
    .local v0, "scrollLimit":I
    if-nez v0, :cond_0

    .line 727
    :goto_0
    return v2

    .line 723
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v3, v3, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v3, :cond_2

    move v1, p3

    .line 724
    .local v1, "velocity":F
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1000(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    move-result-object v3

    neg-float v4, v1

    float-to-int v4, v4

    invoke-virtual {v3, v4, v2, v0}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->fling(III)V

    .line 725
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1100(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mUIListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1100(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/UserInteractionListener;->onUserInteractionBegin()V

    .line 726
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 727
    const/4 v2, 0x1

    goto :goto_0

    .end local v1    # "velocity":F
    :cond_2
    move v1, p4

    .line 723
    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 766
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->cancelDown(Z)V

    .line 767
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mDownInScrolling:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1400(Lcom/sec/android/gallery3d/ui/SlotView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 775
    :goto_0
    return-void

    .line 768
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->lockRendering()V

    .line 770
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->access$800(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotIndexByPosition(FF)I

    move-result v0

    .line 771
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->access$900(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/gallery3d/ui/SlotView$Listener;->onLongTap(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 773
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->unlockRendering()V

    goto :goto_0

    .end local v0    # "index":I
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->unlockRendering()V

    throw v1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v5, 0x0

    .line 733
    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->cancelDown(Z)V

    .line 734
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/ui/SlotView;->WIDE:Z

    if-eqz v2, :cond_2

    move v0, p3

    .line 735
    .local v0, "distance":F
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mScroller:Lcom/sec/android/gallery3d/ui/ScrollerHelper;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1000(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/ScrollerHelper;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/SlotView;->access$800(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getScrollLimit()I

    move-result v4

    invoke-virtual {v2, v3, v5, v4}, Lcom/sec/android/gallery3d/ui/ScrollerHelper;->startScroll(III)I

    move-result v1

    .line 737
    .local v1, "overDistance":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mOverscrollEffect:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1200(Lcom/sec/android/gallery3d/ui/SlotView;)I

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 738
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mPaper:Lcom/sec/android/gallery3d/ui/Paper;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1300(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/Paper;

    move-result-object v2

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/Paper;->overScroll(F)V

    .line 741
    :cond_0
    if-eqz v1, :cond_1

    .line 742
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$900(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/android/gallery3d/ui/SlotView$Listener;->onScrollOverDistance(I)V

    .line 751
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->invalidate()V

    .line 752
    const/4 v2, 0x1

    return v2

    .end local v0    # "distance":F
    .end local v1    # "overDistance":I
    :cond_2
    move v0, p4

    .line 734
    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 690
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    .line 691
    .local v1, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    if-eqz v1, :cond_0

    .line 692
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 694
    :cond_0
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->isDown:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    .line 701
    if-eqz v1, :cond_1

    .line 702
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    .line 704
    :cond_1
    :goto_0
    return-void

    .line 695
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$800(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotIndexByPosition(FF)I

    move-result v0

    .line 696
    .local v0, "index":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 697
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->isDown:Z

    .line 698
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/SlotView;->access$900(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/sec/android/gallery3d/ui/SlotView$Listener;->onDown(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 701
    :cond_3
    if-eqz v1, :cond_1

    .line 702
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    goto :goto_0

    .line 701
    .end local v0    # "index":I
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_4

    .line 702
    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    :cond_4
    throw v2
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 757
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->cancelDown(Z)V

    .line 758
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mDownInScrolling:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->access$1400(Lcom/sec/android/gallery3d/ui/SlotView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 761
    :cond_0
    :goto_0
    return v4

    .line 759
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mLayout:Lcom/sec/android/gallery3d/ui/SlotView$Layout;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->access$800(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Layout;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SlotView$Layout;->getSlotIndexByPosition(FF)I

    move-result v0

    .line 760
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/SlotView$MyGestureListener;->this$0:Lcom/sec/android/gallery3d/ui/SlotView;

    # getter for: Lcom/sec/android/gallery3d/ui/SlotView;->mListener:Lcom/sec/android/gallery3d/ui/SlotView$Listener;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/SlotView;->access$900(Lcom/sec/android/gallery3d/ui/SlotView;)Lcom/sec/android/gallery3d/ui/SlotView$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/gallery3d/ui/SlotView$Listener;->onSingleTapUp(I)V

    goto :goto_0
.end method
