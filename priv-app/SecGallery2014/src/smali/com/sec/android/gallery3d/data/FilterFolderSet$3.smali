.class Lcom/sec/android/gallery3d/data/FilterFolderSet$3;
.super Ljava/lang/Object;
.source "FilterFolderSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/FilterFolderSet;->delete()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/FilterFolderSet;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/FilterFolderSet;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FilterFolderSet$3;->this$0:Lcom/sec/android/gallery3d/data/FilterFolderSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 256
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->delete()V

    .line 259
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
