.class public Lcom/sec/android/gallery3d/data/FilterTypeSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "FilterTypeSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "FilterTypeSet"


# instance fields
.field private final mAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private final mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private final mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mFilterMimeType:Ljava/lang/String;

.field private mFilterMimeTypeMode:Z

.field private mIsLoading:Z

.field private final mMediaType:I

.field private final mPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mSefFileType:Ljava/lang/Integer;

.field private mSupportShare:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;ILjava/lang/String;)V
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "mediaType"    # I
    .param p5, "filterMimeType"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 47
    const-wide/16 v2, -0x1

    invoke-direct {p0, p1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mAlbums:Ljava/util/ArrayList;

    .line 36
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSupportShare:Z

    .line 37
    iput-object v6, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    .line 38
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeTypeMode:Z

    .line 39
    iput-object v6, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSefFileType:Ljava/lang/Integer;

    .line 48
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 49
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 50
    iput p4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mMediaType:I

    .line 51
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 53
    iput-object p5, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    .line 54
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeTypeMode:Z

    .line 56
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "mimeInfo":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 59
    const/4 v1, 0x1

    :try_start_0
    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSefFileType:Ljava/lang/Integer;

    .line 60
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .end local v0    # "mimeInfo":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 61
    .restart local v0    # "mimeInfo":[Ljava/lang/String;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/data/FilterTypeSet;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/FilterTypeSet;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mMediaType:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/data/FilterTypeSet;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/FilterTypeSet;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->isFilteredItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/data/FilterTypeSet;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/FilterTypeSet;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addExtraPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "filteredPath"    # Ljava/lang/String;

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeTypeMode:Z

    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "##"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 210
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSefFileType:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSefFileType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 214
    :cond_0
    return-object p1
.end method

.method private buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p3, "isImage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 370
    .local p2, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p1, :cond_3

    .line 371
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 372
    const/4 v5, 0x0

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 373
    .local v1, "id":I
    if-eqz p3, :cond_1

    sget-object v3, Lcom/sec/android/gallery3d/data/LocalImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 374
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    const/4 v5, 0x4

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 375
    .local v2, "mimeType":Ljava/lang/String;
    const/4 v5, 0x5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "filePath":Ljava/lang/String;
    if-eqz p3, :cond_2

    const/4 v5, 0x6

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 377
    .local v4, "sefFileType":I
    :goto_2
    invoke-direct {p0, v2, v0, v4}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->isFilteredItem(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 378
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 373
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v2    # "mimeType":Ljava/lang/String;
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "sefFileType":I
    :cond_1
    sget-object v3, Lcom/sec/android/gallery3d/data/LocalVideo;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_1

    .line 376
    .restart local v0    # "filePath":Ljava/lang/String;
    .restart local v2    # "mimeType":Ljava/lang/String;
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_2
    const/4 v4, -0x1

    goto :goto_2

    .line 382
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "id":I
    .end local v2    # "mimeType":Ljava/lang/String;
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_3
    return-void
.end method

.method private isFilteredItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 246
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    iget v0, v1, Lcom/sec/android/gallery3d/data/LocalImage;->seffiletype:I

    .line 247
    .local v0, "sefFileType":I
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->isFilteredItem(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    return v1

    .line 246
    .end local v0    # "sefFileType":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private isFilteredItem(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4
    .param p1, "itemMimeType"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "sefFileType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 253
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeTypeMode:Z

    if-nez v2, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v0

    .line 255
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSefFileType:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSefFileType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p3, :cond_2

    move v0, v1

    .line 256
    goto :goto_0

    .line 258
    :cond_2
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 259
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 263
    const-string v2, "image/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    const-string v3, "image/*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    const-string/jumbo v2, "video/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    const-string/jumbo v3, "video/*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 269
    :cond_4
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    const-string v3, "image/jpeg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "image/jpg"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 272
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    const-string/jumbo v3, "video/3gp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 273
    if-nez p2, :cond_6

    move v0, v1

    .line 274
    goto :goto_0

    .line 276
    :cond_6
    const-string v2, ".3gp"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v2, "video/3gp"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "video/3gpp"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "video/3gpp2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "video/mp4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    move v0, v1

    .line 286
    goto/16 :goto_0
.end method

.method private updateData()V
    .locals 10

    .prologue
    .line 145
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 147
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-eqz v8, :cond_2

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->updateDataForComboAndLocalSet()V

    .line 205
    :cond_1
    return-void

    .line 152
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/filter/mediatype/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mMediaType:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "basePath":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    .local v5, "n":I
    :goto_0
    if-ge v4, v5, :cond_5

    .line 155
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 156
    .local v6, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/{"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "}"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "filteredPath":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->addExtraPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 160
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 161
    .local v3, "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v3, :cond_4

    .line 162
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 163
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    if-gtz v8, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v8

    if-lez v8, :cond_4

    .line 165
    :cond_3
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 171
    .end local v2    # "filteredPath":Ljava/lang/String;
    .end local v3    # "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 172
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    .line 174
    .local v7, "total":I
    if-lez v7, :cond_1

    .line 178
    new-array v1, v7, [Lcom/sec/android/gallery3d/data/Path;

    .line 180
    .local v1, "buf":[Lcom/sec/android/gallery3d/data/Path;
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    new-instance v9, Lcom/sec/android/gallery3d/data/FilterTypeSet$1;

    invoke-direct {v9, p0, v7, v1}, Lcom/sec/android/gallery3d/data/FilterTypeSet$1;-><init>(Lcom/sec/android/gallery3d/data/FilterTypeSet;I[Lcom/sec/android/gallery3d/data/Path;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 197
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v7, :cond_1

    .line 198
    aget-object v8, v1, v4

    if-eqz v8, :cond_6

    .line 199
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    aget-object v9, v1, v4

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 200
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    aget-object v9, v1, v4

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private updateDataForComboAndLocalSet()V
    .locals 11

    .prologue
    .line 290
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/filter/mediatype/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mMediaType:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "basePath":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    .local v5, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 292
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 293
    .local v6, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/{"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "}"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "filteredPath":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->addExtraPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 295
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v9, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 296
    .local v2, "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_1

    .line 297
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 298
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v9

    if-gtz v9, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v9

    if-lez v9, :cond_1

    .line 299
    :cond_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 304
    .end local v1    # "filteredPath":Ljava/lang/String;
    .end local v2    # "filteredSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v6    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 305
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v9, v9, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v9, v9, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-eqz v9, :cond_5

    .line 306
    :cond_3
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v10, 0x0

    invoke-direct {p0, v9, v10}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->updateMediaItemData(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 316
    :cond_4
    return-void

    .line 309
    :cond_5
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v8

    .line 310
    .local v8, "total":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v9, Lcom/sec/android/gallery3d/data/ComboAlbumSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/ComboAlbumSet;->getMediaSets()[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    .line 311
    .local v7, "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v3, 0x0

    :goto_1
    array-length v9, v7

    if-ge v3, v9, :cond_4

    .line 312
    aget-object v4, v7, v3

    .line 313
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-direct {p0, v4, v8}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->updateMediaItemData(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 311
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private updateMediaItemData(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 9
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "total"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 319
    if-nez p1, :cond_0

    .line 320
    const-string v4, "FilterTypeSet"

    const-string v5, "mediaSet is null"

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    .end local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return-void

    .line 324
    .restart local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    instance-of v6, p1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-eqz v6, :cond_3

    .line 325
    const/4 v0, 0x0

    .line 327
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    check-cast p1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    .end local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget v6, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mMediaType:I

    const/4 v7, 0x1

    invoke-virtual {p1, v6, v7}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getTotalMediaItems(IZ)Landroid/database/Cursor;

    move-result-object v0

    .line 328
    if-eqz v0, :cond_1

    .line 329
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    iget v7, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mMediaType:I

    if-ne v7, v8, :cond_2

    :goto_1
    invoke-direct {p0, v0, v6, v4}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    :cond_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_2
    move v4, v5

    .line 329
    goto :goto_1

    .line 332
    :catchall_0
    move-exception v4

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4

    .line 334
    .end local v0    # "cursor":Landroid/database/Cursor;
    .restart local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    if-eqz v4, :cond_5

    .line 335
    const/4 v2, 0x0

    .line 336
    .local v2, "imageCursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 338
    .local v3, "videoCursor":Landroid/database/Cursor;
    :try_start_1
    check-cast p1, Lcom/sec/android/gallery3d/data/LocalAlbumSet;

    .end local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget v4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mMediaType:I

    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->getTotalMediaItems(I)[Landroid/database/Cursor;

    move-result-object v1

    .line 339
    .local v1, "cursors":[Landroid/database/Cursor;
    if-eqz v1, :cond_4

    array-length v4, v1

    if-ne v4, v8, :cond_4

    .line 340
    const/4 v4, 0x0

    aget-object v2, v1, v4

    .line 341
    const/4 v4, 0x1

    aget-object v3, v1, v4

    .line 342
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {p0, v2, v4, v5}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V

    .line 343
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->buildCursorToPath(Landroid/database/Cursor;Ljava/util/ArrayList;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 346
    :cond_4
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 347
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 346
    .end local v1    # "cursors":[Landroid/database/Cursor;
    :catchall_1
    move-exception v4

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 347
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v4

    .line 350
    .end local v2    # "imageCursor":Landroid/database/Cursor;
    .end local v3    # "videoCursor":Landroid/database/Cursor;
    .restart local p1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    new-instance v4, Lcom/sec/android/gallery3d/data/FilterTypeSet$3;

    invoke-direct {v4, p0, p2}, Lcom/sec/android/gallery3d/data/FilterTypeSet$3;-><init>(Lcom/sec/android/gallery3d/data/FilterTypeSet;I)V

    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    goto :goto_0
.end method


# virtual methods
.method public delete()V
    .locals 4

    .prologue
    .line 227
    new-instance v0, Lcom/sec/android/gallery3d/data/FilterTypeSet$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/data/FilterTypeSet$2;-><init>(Lcom/sec/android/gallery3d/data/FilterTypeSet;)V

    .line 236
    .local v0, "consumer":Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;I)V

    .line 237
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, -0x1

    .line 407
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 8
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-static {v3, p1, p2, v4}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItemFromPath(Ljava/util/ArrayList;IILcom/sec/android/gallery3d/data/DataManager;)Ljava/util/ArrayList;

    move-result-object v2

    .line 112
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 113
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 115
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSupportShare:Z

    .line 119
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    return-object v2
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPath()Lcom/sec/android/gallery3d/data/Path;
    .locals 3

    .prologue
    .line 86
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "oriPath":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->addExtraPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "extraPath":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 91
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/sec/android/gallery3d/data/Path;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/data/Path;-><init>()V

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    goto :goto_0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 76
    const/4 v2, 0x0

    .line 78
    .local v2, "result":Lcom/sec/android/gallery3d/data/MediaSet;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    return-object v2

    .line 79
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v3, "FilterTypeSet"

    const-string v4, "IndexOutOfBoundsException at getSubMediaSet"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 220
    const-wide/16 v0, 0x5

    .line 221
    .local v0, "operation":J
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mSupportShare:Z

    if-nez v2, :cond_0

    const-wide/16 v2, -0x5

    and-long/2addr v0, v2

    .line 222
    :cond_0
    return-wide v0
.end method

.method public isCameraAlbum()Z
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraAlbum()Z

    move-result v0

    return v0
.end method

.method public isCameraRoll()Z
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized isLoading()Z
    .locals 1

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mIsLoading:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->notifyContentChanged()V

    .line 141
    return-void
.end method

.method public reload()J
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataVersion:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->isLoading()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mIsLoading:Z

    .line 132
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->updateData()V

    .line 133
    invoke-static {}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataVersion:J

    .line 135
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mDataVersion:J

    return-wide v0
.end method

.method public setFilterMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "filterMimeType"    # Ljava/lang/String;

    .prologue
    .line 385
    if-eqz p1, :cond_0

    .line 386
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeType:Ljava/lang/String;

    .line 387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/FilterTypeSet;->mFilterMimeTypeMode:Z

    .line 389
    :cond_0
    return-void
.end method
