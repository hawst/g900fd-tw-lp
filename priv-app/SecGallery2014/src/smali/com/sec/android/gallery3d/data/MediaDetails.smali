.class public Lcom/sec/android/gallery3d/data/MediaDetails;
.super Ljava/lang/Object;
.source "MediaDetails.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/MediaDetails$1;,
        Lcom/sec/android/gallery3d/data/MediaDetails$DetailIndexComparator;,
        Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final EXPOSURE_BIAS_VALUE:Ljava/lang/String; = "ExposureBiasValue"

.field private static final EXPOSURE_TIME_EX:Ljava/lang/String; = "ExposureTimeEx"

.field public static final INDEX_ALBUM_NAME:I = 0x5

.field public static final INDEX_APERTURE:I = 0x6a

.field public static final INDEX_AVAILABLE_USE:I = 0x12d

.field public static final INDEX_DATEMODIFIEDTIME:I = 0x12

.field public static final INDEX_DATETIME:I = 0x4

.field public static final INDEX_DESCRIPTION:I = 0x2

.field public static final INDEX_DRM_DES:I = 0x12c

.field public static final INDEX_DURATION:I = 0x11

.field public static final INDEX_EXPOSURE_TIME:I = 0x6b

.field public static final INDEX_EXPOSURE_VALUE:I = 0x6d

.field public static final INDEX_FLASH:I = 0x66

.field public static final INDEX_FOCAL_LENGTH:I = 0x67

.field public static final INDEX_FORWARD_LOCK:I = 0x135

.field public static final INDEX_HEIGHT:I = 0xd

.field public static final INDEX_ISO:I = 0x6c

.field public static final INDEX_LATITUDE:I = 0xa

.field public static final INDEX_LICENSE_AVAILABLE_TIME:I = 0x12f

.field public static final INDEX_LICENSE_EXPIRY_TIME:I = 0x130

.field public static final INDEX_LICENSE_ORIGINAL_INTERVAL:I = 0x132

.field public static final INDEX_LICENSE_START_TIME:I = 0x131

.field public static final INDEX_LICENSE_TYPE:I = 0x12e

.field public static final INDEX_LOCATION:I = 0x9

.field public static final INDEX_LONGITUDE:I = 0xb

.field public static final INDEX_MAKE:I = 0x64

.field public static final INDEX_MAX_REPEAT_COUNT:I = 0x133

.field public static final INDEX_MIMETYPE:I = 0x3

.field public static final INDEX_MODEL:I = 0x65

.field public static final INDEX_ORIENTATION:I = 0x10

.field public static final INDEX_PATH:I = 0xc8

.field public static final INDEX_REMAINING_REPEAT_COUNT:I = 0x134

.field public static final INDEX_RESOLUTION:I = 0xf

.field public static final INDEX_SHUTTER_SPEED:I = 0x69

.field public static final INDEX_SIZE:I = 0xe

.field public static final INDEX_TITLE:I = 0x1

.field public static final INDEX_WHITE_BALANCE:I = 0x68

.field public static final INDEX_WIDTH:I = 0xc

.field private static final TAG:Ljava/lang/String; = "MediaDetails"


# instance fields
.field private mDetails:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mUnits:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/TreeMap;

    new-instance v1, Lcom/sec/android/gallery3d/data/MediaDetails$DetailIndexComparator;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails$DetailIndexComparator;-><init>(Lcom/sec/android/gallery3d/data/MediaDetails$1;)V

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mUnits:Ljava/util/HashMap;

    .line 266
    return-void
.end method

.method public static extractExifInfo(Lcom/sec/android/gallery3d/data/MediaDetails;Ljava/lang/String;)V
    .locals 10
    .param p0, "details"    # Lcom/sec/android/gallery3d/data/MediaDetails;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x67

    const/16 v8, 0x6a

    .line 206
    new-instance v2, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 208
    .local v2, "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/exif/ExifInterface;->readExif(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 215
    :goto_0
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_FLASH:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0x66

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 217
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_WIDTH:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0xc

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 219
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_IMAGE_LENGTH:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0xd

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 221
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_MAKE:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0x64

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 223
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_MODEL:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0x65

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 226
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_F_NUMBER:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    invoke-static {p0, v5, v8}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 228
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_ISO_SPEED_RATINGS:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0x6c

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 230
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_WHITE_BALANCE:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0x68

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 232
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_EXPOSURE_TIME:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    const/16 v6, 0x6b

    invoke-static {p0, v5, v6}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 235
    const/4 v4, 0x0

    .line 236
    .local v4, "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 237
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 238
    .local v0, "apeture":Ljava/lang/String;
    invoke-virtual {p0, v8}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "apeture":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 239
    .restart local v0    # "apeture":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 240
    const-string v5, "--"

    invoke-virtual {p0, v8, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 243
    .end local v0    # "apeture":Ljava/lang/String;
    :cond_1
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_EXPOSURE_BIAS_VALUE:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v4

    .line 244
    if-eqz v4, :cond_2

    .line 245
    const/16 v5, 0x6d

    invoke-static {p0, v4, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V

    .line 248
    :cond_2
    sget v5, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_FOCAL_LENGTH:I

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getTag(I)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v3

    .line 249
    .local v3, "focalTag":Lcom/sec/android/gallery3d/exif/ExifTag;
    if-eqz v3, :cond_3

    .line 250
    const-wide/16 v6, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/gallery3d/exif/ExifTag;->getValueAsRational(J)Lcom/sec/android/gallery3d/exif/Rational;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/exif/Rational;->toDouble()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {p0, v9, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 252
    const v5, 0x7f0e0082

    invoke-virtual {p0, v9, v5}, Lcom/sec/android/gallery3d/data/MediaDetails;->setUnit(II)V

    .line 254
    :cond_3
    return-void

    .line 209
    .end local v3    # "focalTag":Lcom/sec/android/gallery3d/exif/ExifTag;
    .end local v4    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v5, "MediaDetails"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not find file to read exif: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 211
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 212
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "MediaDetails"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not read exif from file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private static setExifData(Lcom/sec/android/gallery3d/data/MediaDetails;Lcom/sec/android/gallery3d/exif/ExifTag;I)V
    .locals 10
    .param p0, "details"    # Lcom/sec/android/gallery3d/data/MediaDetails;
    .param p1, "tag"    # Lcom/sec/android/gallery3d/exif/ExifTag;
    .param p2, "key"    # I

    .prologue
    const-wide/16 v8, 0x0

    .line 175
    if-eqz p1, :cond_2

    .line 176
    const/4 v4, 0x0

    .line 177
    .local v4, "value":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/exif/ExifTag;->getDataType()S

    move-result v1

    .line 178
    .local v1, "type":I
    const/4 v5, 0x5

    if-eq v1, v5, :cond_0

    const/16 v5, 0xa

    if-ne v1, v5, :cond_4

    .line 179
    :cond_0
    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/exif/ExifTag;->getValueAsRational(J)Lcom/sec/android/gallery3d/exif/Rational;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/exif/Rational;->toDouble()D

    move-result-wide v2

    .line 180
    .local v2, "v":D
    const/16 v5, 0x6b

    if-ne p2, v5, :cond_3

    const-wide v6, 0x3fd6666666666666L    # 0.35

    cmpg-double v5, v2, v6

    if-gtz v5, :cond_3

    .line 181
    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/exif/ExifTag;->getValueAsRational(J)Lcom/sec/android/gallery3d/exif/Rational;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 192
    .end local v2    # "v":D
    :cond_1
    :goto_0
    const/16 v5, 0x66

    if-ne p2, v5, :cond_6

    .line 193
    if-eqz v4, :cond_2

    .line 194
    new-instance v0, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;-><init>(I)V

    .line 196
    .local v0, "state":Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;
    invoke-virtual {p0, p2, v0}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 202
    .end local v0    # "state":Lcom/sec/android/gallery3d/data/MediaDetails$FlashState;
    .end local v1    # "type":I
    .end local v4    # "value":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 183
    .restart local v1    # "type":I
    .restart local v2    # "v":D
    .restart local v4    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/exif/ExifTag;->getValueAsRational(J)Lcom/sec/android/gallery3d/exif/Rational;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/exif/Rational;->toDouble()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    .line 184
    const-string v5, "NaN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    .line 187
    .end local v2    # "v":D
    :cond_4
    const/4 v5, 0x2

    if-ne v1, v5, :cond_5

    .line 188
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/exif/ExifTag;->getValueAsString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 190
    :cond_5
    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/exif/ExifTag;->forceGetValueAsLong(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 199
    :cond_6
    invoke-virtual {p0, p2, v4}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public addDetail(ILjava/lang/Object;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method

.method public getDetail(I)Ljava/lang/Object;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getUnit(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mUnits:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public hasUnit(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mUnits:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public removeDetail(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    return-void
.end method

.method public replaceDetail(ILjava/lang/Object;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    return-void
.end method

.method public setUnit(II)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "unit"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mUnits:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaDetails;->mDetails:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    return v0
.end method
