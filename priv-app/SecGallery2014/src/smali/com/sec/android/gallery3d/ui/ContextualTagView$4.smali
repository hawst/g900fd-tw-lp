.class Lcom/sec/android/gallery3d/ui/ContextualTagView$4;
.super Ljava/lang/Object;
.source "ContextualTagView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContextualTagView;->showContextualTagHelpView(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v2, 0x1

    .line 365
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setContextualTagHelpOff(Z)V

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->hideContextualTagHelpView(Z)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->hide(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$000(Lcom/sec/android/gallery3d/ui/ContextualTagView;Z)V

    .line 370
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$4;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v1, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setContextualTagView(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 371
    return-void
.end method
