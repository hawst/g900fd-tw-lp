.class public Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "HomeSyncMediaSet.java"


# static fields
.field private static final COUNT_PROJECTION:[Ljava/lang/String;

.field private static final INVALID_COUNT:I = -0x1

.field public static final SET_PATH:Ljava/lang/String; = "/homesync/mediaset"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mCachedCount:I

.field private final mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "count(*)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->COUNT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 48
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mCachedCount:I

    .line 49
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    const-string v1, "new HomeSyncMediaSet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iput-object p2, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 52
    invoke-interface {p2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mResolver:Landroid/content/ContentResolver;

    .line 54
    new-instance v0, Lcom/sec/android/gallery3d/data/ChangeNotifier;

    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->BASE_URI:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/gallery3d/data/ChangeNotifier;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Landroid/net/Uri;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    .line 55
    return-void
.end method

.method private static loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 4
    .param p0, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "dataManager"    # Lcom/sec/android/gallery3d/data/DataManager;
    .param p3, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 87
    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load or update item : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/data/DataManager;->peekMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;

    .line 89
    .local v0, "item":Lcom/sec/android/gallery3d/homesync/HomeSyncItem;
    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;

    .end local v0    # "item":Lcom/sec/android/gallery3d/homesync/HomeSyncItem;
    invoke-direct {v0, p0, p3, p1}, Lcom/sec/android/gallery3d/homesync/HomeSyncLocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    .line 94
    .restart local v0    # "item":Lcom/sec/android/gallery3d/homesync/HomeSyncItem;
    :goto_0
    return-object v0

    .line 92
    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->updateContent(Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 12
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMediaItem "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int v3, p1, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    .line 61
    .local v8, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 62
    .local v1, "uri":Landroid/net/Uri;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 65
    const/4 v7, 0x0

    .line 67
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 68
    if-nez v7, :cond_0

    .line 69
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    const-string v2, "query fail : "

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 83
    :goto_0
    return-object v11

    .line 72
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cursor count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_1
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 75
    .local v9, "id":I
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncItem;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 76
    .local v6, "childPath":Lcom/sec/android/gallery3d/data/Path;
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v6, v7, v8, v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->loadOrUpdateItem(Lcom/sec/android/gallery3d/data/Path;Landroid/database/Cursor;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    .line 77
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v10, :cond_1

    .line 78
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 81
    .end local v6    # "childPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v9    # "id":I
    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public getMediaItemCount()I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 99
    iget v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mCachedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 100
    const/4 v6, 0x0

    .line 102
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/homesync/HomeSyncSource;->BASE_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->COUNT_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 103
    if-nez v6, :cond_0

    .line 104
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    const-string v1, "query fail"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v7

    .line 114
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v0

    .line 107
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 108
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DB mediaset count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mCachedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 114
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mCachedCount:I

    goto :goto_0

    .line 111
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string v0, "HomeSync Native Viewer"

    return-object v0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method

.method public reload()J
    .locals 2

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    const-string v1, "reload!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mNotifier:Lcom/sec/android/gallery3d/data/ChangeNotifier;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ChangeNotifier;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    sget-object v0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->TAG:Ljava/lang/String;

    const-string v1, "reload dirty!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->nextVersionNumber()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mDataVersion:J

    .line 128
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mCachedCount:I

    .line 130
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncMediaSet;->mDataVersion:J

    return-wide v0
.end method
