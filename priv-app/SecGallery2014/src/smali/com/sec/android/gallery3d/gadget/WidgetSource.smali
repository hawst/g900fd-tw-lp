.class public interface abstract Lcom/sec/android/gallery3d/gadget/WidgetSource;
.super Ljava/lang/Object;
.source "WidgetSource.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract getContentUri(I)Landroid/net/Uri;
.end method

.method public abstract getImage(I)Landroid/graphics/Bitmap;
.end method

.method public abstract getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
.end method

.method public abstract isDrm(I)Z
.end method

.method public abstract reload()V
.end method

.method public abstract setContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
.end method

.method public abstract size()I
.end method
