.class public Lcom/sec/android/gallery3d/app/MediaScannerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaScannerReceiver.java"


# static fields
.field private static final INTERNAL_DIR:Ljava/lang/String;

.field private static final MESSAGE_MEDIA_SCANNER_FINISHED:I = 0x1

.field public static final TAG:Ljava/lang/String; = "GalleryCacheReady"


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/media"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/MediaScannerReceiver;->INTERNAL_DIR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/MediaScannerReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/MediaScannerReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/MediaScannerReceiver;->handleMeidaScanFinish(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private handleMeidaScanFinish(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 83
    const-string v6, "GalleryCacheReady"

    const-string v7, "handleMeidaScanFinish()"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v6, "CleanBuffer"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 86
    .local v4, "restoreMMC":Z
    const-string v6, "BackupTable"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 88
    .local v3, "removeMMC":Z
    const/4 v5, 0x3

    .line 89
    .local v5, "scanner_finish_type":I
    if-eqz v4, :cond_2

    .line 90
    const-string v6, "GalleryCacheReady"

    const-string v7, "Receive action.ACTION_MEDIA_SCANNER_FINISHED restoreMMC"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const/4 v5, 0x1

    .line 97
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 99
    .local v1, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v1, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 100
    const-string v6, "GalleryCacheReady"

    const-string v7, "check storage - not run service"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_1
    :goto_1
    return-void

    .line 92
    .end local v1    # "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    :cond_2
    if-eqz v3, :cond_0

    .line 93
    const-string v6, "GalleryCacheReady"

    const-string v7, "Receive action.ACTION_MEDIA_SCANNER_FINISHED removeMMC"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const/4 v5, 0x2

    goto :goto_0

    .line 104
    .restart local v1    # "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    :cond_3
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v6, :cond_4

    .line 105
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 106
    .local v2, "data":Landroid/net/Uri;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/android/gallery3d/app/MediaScannerReceiver;->INTERNAL_DIR:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 107
    const/4 v6, 0x0

    invoke-static {p1, v6, v9, v5}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->requestFaceScan(Landroid/content/Context;Ljava/util/List;ZI)V

    .line 113
    .end local v2    # "data":Landroid/net/Uri;
    :cond_4
    :goto_2
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v6, :cond_1

    .line 114
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v0

    .line 115
    .local v0, "EventAlbumMgr":Lcom/sec/android/gallery3d/data/EventAlbumManager;
    const-string v6, "images_event_album"

    invoke-virtual {v0, v9, v6}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    .line 116
    const-string/jumbo v6, "video_event_album"

    invoke-virtual {v0, v8, v6}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    goto :goto_1

    .line 109
    .end local v0    # "EventAlbumMgr":Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .restart local v2    # "data":Landroid/net/Uri;
    :cond_5
    const-string v6, "GalleryCacheReady"

    const-string v7, "SKIP requestFaceScan"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 47
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 49
    const-string v3, "GalleryCacheReady"

    const-string v4, "Receive action.ACTION_MEDIA_SCANNER_FINISHED"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    new-instance v1, Landroid/os/HandlerThread;

    const-string v3, "MediaScannerReceiver"

    invoke-direct {v1, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 52
    .local v1, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 54
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    .line 56
    .local v2, "looper":Landroid/os/Looper;
    new-instance v3, Lcom/sec/android/gallery3d/app/MediaScannerReceiver$1;

    invoke-direct {v3, p0, v2, p1, v2}, Lcom/sec/android/gallery3d/app/MediaScannerReceiver$1;-><init>(Lcom/sec/android/gallery3d/app/MediaScannerReceiver;Landroid/os/Looper;Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/app/MediaScannerReceiver;->mHandler:Landroid/os/Handler;

    .line 72
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/MediaScannerReceiver;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/MediaScannerReceiver;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 80
    .end local v1    # "handlerThread":Landroid/os/HandlerThread;
    .end local v2    # "looper":Landroid/os/Looper;
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 76
    const-string v3, "GalleryCacheReady"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Receive : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 77
    :cond_2
    const-string v3, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 78
    const-string v3, "GalleryCacheReady"

    const-string v4, "Receive : home resume"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
