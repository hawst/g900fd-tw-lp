.class Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/MotionDetector$MotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyMotionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0

    .prologue
    .line 3902
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/ui/PhotoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/ui/PhotoView$1;

    .prologue
    .line 3902
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;-><init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 24
    .param p1, "event"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 3905
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v18

    sparse-switch v18, :sswitch_data_0

    .line 4117
    :cond_0
    :goto_0
    return-void

    .line 3907
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    sget-object v19, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_MOVE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/gallery3d/util/MotionDetector;->checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 3910
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5100(Lcom/sec/android/gallery3d/ui/PhotoView;)J

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_1

    .line 3911
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J
    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5102(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    goto :goto_0

    .line 3914
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPanningStartTime:J
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5100(Lcom/sec/android/gallery3d/ui/PhotoView;)J

    move-result-wide v20

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->DOUBLE_TAP_TIMEOUT:I
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8800()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    const-wide/16 v22, 0x64

    add-long v20, v20, v22

    cmp-long v18, v18, v20

    if-ltz v18, :cond_0

    .line 3918
    sget-object v14, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    .line 3919
    .local v14, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v18

    if-eqz v18, :cond_2

    .line 3920
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v14

    .line 3922
    :cond_2
    sget-object v18, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v18

    if-eq v14, v0, :cond_0

    sget-object v18, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v18

    if-eq v14, v0, :cond_0

    sget-object v18, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v18

    if-eq v14, v0, :cond_0

    .line 3925
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5000(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    if-nez v18, :cond_0

    .line 3928
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 3929
    .local v15, "res":Landroid/content/res/Resources;
    const v18, 0x7f0c0045

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 3930
    .local v9, "kx":I
    const v18, 0x7f0c0046

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    .line 3932
    .local v10, "ky":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/motion/MREvent;->getPanningDxImage()I

    move-result v18

    mul-int v4, v18, v9

    .line 3933
    .local v4, "dX":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/motion/MREvent;->getPanningDyImage()I

    move-result v18

    mul-int v5, v18, v10

    .line 3934
    .local v5, "dY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PositionController;->isFullScreen()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 3935
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v18

    const/16 v19, 0x28

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_0

    .line 3936
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9000(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionPeekUse()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 3938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPeekStartTime:J
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6600(Lcom/sec/android/gallery3d/ui/PhotoView;)J

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_3

    .line 3939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPeekStartTime:J
    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6602(Lcom/sec/android/gallery3d/ui/PhotoView;J)J

    goto/16 :goto_0

    .line 3942
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPeekStartTime:J
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6600(Lcom/sec/android/gallery3d/ui/PhotoView;)J

    move-result-wide v20

    const-wide/16 v22, 0x64

    add-long v20, v20, v22

    cmp-long v18, v18, v20

    if-ltz v18, :cond_0

    .line 3946
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9000(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    move-result-object v18

    const/16 v19, 0x1

    invoke-interface/range {v18 .. v19}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;->onShow(Z)V

    .line 3948
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 3949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01c7

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 3950
    .local v16, "text":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01cc

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 3951
    .local v17, "text_complete":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePeek:Z
    invoke-static/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9102(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 3952
    if-eqz v16, :cond_4

    if-eqz v17, :cond_4

    .line 3953
    const/16 v18, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3954
    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3956
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01c8

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 3957
    .local v8, "head":Landroid/widget/ImageView;
    if-eqz v8, :cond_0

    .line 3958
    const/16 v18, 0x4

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 3965
    .end local v8    # "head":Landroid/widget/ImageView;
    .end local v16    # "text":Landroid/widget/TextView;
    .end local v17    # "text_complete":Landroid/widget/TextView;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionPanningUse()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 3969
    const/4 v6, 0x0

    .line 3970
    .local v6, "deltaX":I
    const/4 v7, 0x0

    .line 3971
    .local v7, "deltaY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mDisplay:Landroid/view/Display;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9200(Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/view/Display;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/Display;->getRotation()I

    move-result v18

    packed-switch v18, :pswitch_data_0

    .line 3990
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # += operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I
    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9312(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 3991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # += operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I
    invoke-static {v0, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9412(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 3993
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x3

    cmp-long v18, v18, v20

    if-gez v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x3

    cmp-long v18, v18, v20

    if-lez v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9400(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x3

    cmp-long v18, v18, v20

    if-gez v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9400(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x3

    cmp-long v18, v18, v20

    if-gtz v18, :cond_0

    .line 3997
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 3998
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01bf

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 3999
    .restart local v16    # "text":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01c6

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 4000
    .restart local v17    # "text_complete":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mIsOperatePanning:Z
    invoke-static/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9502(Lcom/sec/android/gallery3d/ui/PhotoView;Z)Z

    .line 4001
    if-eqz v16, :cond_7

    if-eqz v17, :cond_7

    .line 4002
    const/16 v18, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4003
    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4005
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01c0

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 4006
    .restart local v8    # "head":Landroid/widget/ImageView;
    if-eqz v8, :cond_8

    .line 4007
    const/16 v18, 0x4

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4010
    .end local v8    # "head":Landroid/widget/ImageView;
    .end local v16    # "text":Landroid/widget/TextView;
    .end local v17    # "text_complete":Landroid/widget/TextView;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v6

    .line 4011
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9400(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v7

    .line 4013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    sget-object v19, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_MOVE:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/gallery3d/util/MotionDetector;->checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4018
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v18

    const/16 v19, 0xb

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v6, v7, v1}, Lcom/sec/android/gallery3d/ui/PositionController;->scrollPage(III)V

    .line 4020
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningXSum:I
    invoke-static/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9302(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 4021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionPanningYSum:I
    invoke-static/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9402(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    goto/16 :goto_0

    .line 3973
    :pswitch_0
    move v6, v4

    .line 3974
    neg-int v7, v5

    .line 3975
    goto/16 :goto_1

    .line 3977
    :pswitch_1
    neg-int v6, v4

    .line 3978
    move v7, v5

    .line 3979
    goto/16 :goto_1

    .line 3981
    :pswitch_2
    neg-int v6, v5

    .line 3982
    neg-int v7, v4

    .line 3983
    goto/16 :goto_1

    .line 3985
    :pswitch_3
    move v6, v5

    .line 3986
    move v7, v4

    goto/16 :goto_1

    .line 4026
    .end local v4    # "dX":I
    .end local v5    # "dY":I
    .end local v6    # "deltaX":I
    .end local v7    # "deltaY":I
    .end local v9    # "kx":I
    .end local v10    # "ky":I
    .end local v14    # "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    .end local v15    # "res":Landroid/content/res/Resources;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    sget-object v19, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/gallery3d/util/MotionDetector;->checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4029
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v14

    .line 4030
    .restart local v14    # "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v18, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v18

    if-eq v14, v0, :cond_0

    sget-object v18, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v18

    if-eq v14, v0, :cond_0

    sget-object v18, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v18

    if-eq v14, v0, :cond_0

    .line 4033
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGalleryMotion:Lcom/sec/android/gallery3d/app/GalleryMotion;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryMotion;->isMotionTiltUse()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 4039
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01ce

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 4040
    .restart local v16    # "text":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9600(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    if-nez v18, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9700(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    if-nez v18, :cond_a

    if-eqz v16, :cond_a

    .line 4041
    const v18, 0x7f0e00e7

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 4045
    .end local v16    # "text":Landroid/widget/TextView;
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/motion/MREvent;->getTilt()I

    move-result v13

    .line 4046
    .local v13, "motionTiltValue":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # += operator for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I
    invoke-static {v0, v13}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9812(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 4050
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x3

    cmp-long v18, v18, v20

    if-gez v18, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x3

    cmp-long v18, v18, v20

    if-gtz v18, :cond_0

    .line 4053
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9800(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v13

    .line 4055
    const/high16 v11, 0x3f800000    # 1.0f

    .line 4056
    .local v11, "mMotionScale":F
    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const v19, 0x3b449ba6    # 0.003f

    mul-float v12, v18, v19

    .line 4057
    .local v12, "motionRatio":F
    if-gez v13, :cond_f

    .line 4058
    const v18, 0x3f7d70a4    # 0.99f

    sub-float v11, v18, v12

    .line 4063
    :goto_2
    invoke-static {v11}, Ljava/lang/Float;->isNaN(F)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v11}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v18

    if-nez v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTransitionMode:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$5000(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    if-nez v18, :cond_0

    .line 4069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    if-eqz v18, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionDetector:Lcom/sec/android/gallery3d/util/MotionDetector;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$6500(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/util/MotionDetector;

    move-result-object v18

    sget-object v19, Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;->STATE_TILT:Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/gallery3d/util/MotionDetector;->checkState(Lcom/sec/android/gallery3d/util/MotionDetector$MotionStateType;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4072
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 4074
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v18

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusX:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8100()F

    move-result v19

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mGetPrevFocusY:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$8200()F

    move-result v20

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v11, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->scaleBy(FFF)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4076
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    .line 4078
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mMotionTiltSum:I
    invoke-static/range {v18 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9802(Lcom/sec/android/gallery3d/ui/PhotoView;I)I

    .line 4080
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->onScale(F)V

    .line 4082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4083
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01ce

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 4084
    .restart local v16    # "text":Landroid/widget/TextView;
    if-eqz v16, :cond_0

    .line 4085
    const v18, 0x3f7d70a4    # 0.99f

    cmpg-float v18, v11, v18

    if-gez v18, :cond_d

    .line 4086
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # operator++ for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9608(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    .line 4087
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9600(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0xf

    cmp-long v18, v18, v20

    if-ltz v18, :cond_d

    .line 4088
    const v18, 0x7f0e00e7

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 4091
    :cond_d
    const v18, 0x3f8147ae    # 1.01f

    cmpl-float v18, v11, v18

    if-lez v18, :cond_e

    .line 4092
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # operator++ for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9708(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    .line 4093
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9700(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0xf

    cmp-long v18, v18, v20

    if-ltz v18, :cond_e

    .line 4094
    const v18, 0x7f0e00e8

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 4097
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomIn:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9600(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0xf

    cmp-long v18, v18, v20

    if-ltz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mTiltZoomOut:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9700(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    const-wide/16 v20, 0xf

    cmp-long v18, v18, v20

    if-ltz v18, :cond_0

    .line 4098
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$100(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    const v19, 0x7f0f01d0

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 4099
    .restart local v17    # "text_complete":Landroid/widget/TextView;
    const/16 v18, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4100
    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFinishHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$9900(Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x64

    const-wide/16 v20, 0x7d0

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 4060
    .end local v16    # "text":Landroid/widget/TextView;
    .end local v17    # "text_complete":Landroid/widget/TextView;
    :cond_f
    const v18, 0x3f8147ae    # 1.01f

    add-float v11, v18, v12

    goto/16 :goto_2

    .line 4076
    :catchall_0
    move-exception v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoView$MyMotionListener;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/PhotoView;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v18

    .line 3905
    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_0
        0x48 -> :sswitch_1
    .end sparse-switch

    .line 3971
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
