.class public Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;
.super Ljava/lang/Object;
.source "GLES11Canvas.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glrenderer/GLCanvas;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$1;,
        Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;,
        Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;
    }
.end annotation


# static fields
.field private static final BOX_COORDINATES:[F

.field private static CIRCLE_COORDINATES:[F = null

.field private static final CIRCLE_VERTEX_COUNT:I = 0x90

.field private static final MSCALE_X:I = 0x0

.field private static final MSCALE_Y:I = 0x5

.field private static final MSKEW_X:I = 0x4

.field private static final MSKEW_Y:I = 0x1

.field private static final OFFSET_CIRCLE:I = 0x0

.field private static final OFFSET_DRAW_LINE:I = 0x4

.field private static final OFFSET_DRAW_RECT:I = 0x6

.field private static final OFFSET_FILL_RECT:I = 0x0

.field private static final OPAQUE_ALPHA:F = 0.95f

.field private static final TAG:Ljava/lang/String; = "GLCanvasImp"


# instance fields
.field private mAlpha:F

.field private mAnimationTime:J

.field private mBlendEnabled:Z

.field private mBoxCoords:I

.field private mCircleCoords:I

.field private final mClipRect:Landroid/graphics/Rect;

.field mCountDrawCircle:I

.field mCountDrawLine:I

.field mCountDrawMesh:I

.field mCountFillRect:I

.field mCountTextureOES:I

.field mCountTextureRect:I

.field private final mDeleteBuffers:Lcom/sec/android/gallery3d/util/IntArray;

.field private final mDrawTextureSourceRect:Landroid/graphics/RectF;

.field private final mDrawTextureTargetRect:Landroid/graphics/RectF;

.field private mFrameBuffer:[I

.field private mGL:Ljavax/microedition/khronos/opengles/GL11;

.field private mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

.field private mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

.field private mHeight:I

.field private final mMapPointsBuffer:[F

.field private final mMatrixValues:[F

.field private mRecycledRestoreAction:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

.field private final mRestoreStack:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;",
            ">;"
        }
    .end annotation
.end field

.field private final mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private final mTargetStack:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glrenderer/RawTexture;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetTexture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

.field private final mTempMatrix:[F

.field private final mTextureColor:[F

.field private mTextureId:[I

.field private final mTextureMatrixValues:[F

.field private final mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

.field private mUploadedCount:I

.field private sCropRect:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->BOX_COORDINATES:[F

    .line 1411
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->CIRCLE_COORDINATES:[F

    return-void

    .line 52
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 1
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;-><init>(Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Ljavax/microedition/khronos/opengles/GL11;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 9
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p2, "rootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    const/4 v4, 0x4

    const/16 v8, 0x1406

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/16 v3, 0x10

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    .line 60
    const/16 v3, 0x10

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    .line 67
    const/16 v3, 0xa

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMapPointsBuffer:[F

    .line 70
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureColor:[F

    .line 75
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetStack:Ljava/util/ArrayList;

    .line 78
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRestoreStack:Ljava/util/ArrayList;

    .line 81
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureSourceRect:Landroid/graphics/RectF;

    .line 82
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureTargetRect:Landroid/graphics/RectF;

    .line 83
    const/16 v3, 0x20

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTempMatrix:[F

    .line 84
    new-instance v3, Lcom/sec/android/gallery3d/util/IntArray;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/util/IntArray;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    .line 85
    new-instance v3, Lcom/sec/android/gallery3d/util/IntArray;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/util/IntArray;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDeleteBuffers:Lcom/sec/android/gallery3d/util/IntArray;

    .line 88
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    .line 89
    new-array v3, v5, [I

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mFrameBuffer:[I

    .line 90
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->sCropRect:[F

    .line 102
    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/GLES11IdImpl;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11IdImpl;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    .line 106
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mClipRect:Landroid/graphics/Rect;

    .line 108
    new-array v3, v5, [I

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureId:[I

    .line 1412
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCircleCoords:I

    .line 144
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 145
    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-direct {v3, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;-><init>(Ljavax/microedition/khronos/opengles/GL11;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    .line 146
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCircleFace:Z

    if-eqz v3, :cond_0

    .line 147
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->initCircle(Ljavax/microedition/khronos/opengles/GL11;)V

    .line 149
    :cond_0
    sget-object v3, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->BOX_COORDINATES:[F

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x20

    div-int/lit8 v1, v3, 0x8

    .line 150
    .local v1, "size":I
    invoke-static {v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->allocateDirectNativeOrderBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v2

    .line 151
    .local v2, "xyBuffer":Ljava/nio/FloatBuffer;
    sget-object v3, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->BOX_COORDINATES:[F

    sget-object v4, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->BOX_COORDINATES:[F

    array-length v4, v4

    invoke-virtual {v2, v3, v6, v4}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 153
    new-array v0, v5, [I

    .line 154
    .local v0, "name":[I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    invoke-interface {v3, v5, v0, v6}, Lcom/sec/android/gallery3d/glrenderer/GLId;->glGenBuffers(I[II)V

    .line 155
    aget v3, v0, v6

    iput v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBoxCoords:I

    .line 157
    const v3, 0x8892

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBoxCoords:I

    invoke-interface {p1, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 158
    const v3, 0x8892

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v4

    mul-int/lit8 v4, v4, 0x4

    const v5, 0x88e4

    invoke-interface {p1, v3, v4, v2, v5}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 161
    invoke-interface {p1, v7, v8, v6, v6}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    .line 162
    invoke-interface {p1, v7, v8, v6, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    .line 165
    const v3, 0x84c1

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glClientActiveTexture(I)V

    .line 166
    invoke-interface {p1, v7, v8, v6, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    .line 167
    const v3, 0x84c0

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glClientActiveTexture(I)V

    .line 168
    const v3, 0x8078

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glEnableClientState(I)V

    .line 171
    const/16 v3, 0xb71

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 174
    iput-object p2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 175
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mClipRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;)Ljavax/microedition/khronos/opengles/GL11;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    return-object v0
.end method

.method private static allocateDirectNativeOrderBuffer(I)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0, "size"    # I

    .prologue
    .line 229
    invoke-static {p0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method private bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z
    .locals 3
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .prologue
    .line 539
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->onBind(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 543
    :goto_0
    return v1

    .line 540
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTarget()I

    move-result v0

    .line 541
    .local v0, "target":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureTarget(I)V

    .line 542
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getId()I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 543
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static checkFramebufferStatus(Ljavax/microedition/khronos/opengles/GL11ExtensionPack;)V
    .locals 5
    .param p0, "gl11ep"    # Ljavax/microedition/khronos/opengles/GL11ExtensionPack;

    .prologue
    .line 1023
    const v2, 0x8d40

    invoke-interface {p0, v2}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glCheckFramebufferStatusOES(I)I

    move-result v1

    .line 1024
    .local v1, "status":I
    const v2, 0x8cd5

    if-eq v1, v2, :cond_0

    .line 1025
    const-string v0, ""

    .line 1026
    .local v0, "msg":Ljava/lang/String;
    packed-switch v1, :pswitch_data_0

    .line 1049
    :goto_0
    :pswitch_0
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1028
    :pswitch_1
    const-string v0, "FRAMEBUFFER_FORMATS"

    .line 1029
    goto :goto_0

    .line 1031
    :pswitch_2
    const-string v0, "FRAMEBUFFER_ATTACHMENT"

    .line 1032
    goto :goto_0

    .line 1034
    :pswitch_3
    const-string v0, "FRAMEBUFFER_MISSING_ATTACHMENT"

    .line 1035
    goto :goto_0

    .line 1037
    :pswitch_4
    const-string v0, "FRAMEBUFFER_DRAW_BUFFER"

    .line 1038
    goto :goto_0

    .line 1040
    :pswitch_5
    const-string v0, "FRAMEBUFFER_READ_BUFFER"

    .line 1041
    goto :goto_0

    .line 1043
    :pswitch_6
    const-string v0, "FRAMEBUFFER_UNSUPPORTED"

    .line 1044
    goto :goto_0

    .line 1046
    :pswitch_7
    const-string v0, "FRAMEBUFFER_INCOMPLETE_DIMENSIONS"

    goto :goto_0

    .line 1051
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    return-void

    .line 1026
    :pswitch_data_0
    .packed-switch 0x8cd6
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static convertCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V
    .locals 9
    .param p0, "source"    # Landroid/graphics/RectF;
    .param p1, "target"    # Landroid/graphics/RectF;
    .param p2, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .prologue
    .line 507
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v3

    .line 508
    .local v3, "width":I
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v0

    .line 509
    .local v0, "height":I
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureWidth()I

    move-result v2

    .line 510
    .local v2, "texWidth":I
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureHeight()I

    move-result v1

    .line 512
    .local v1, "texHeight":I
    iget v6, p0, Landroid/graphics/RectF;->left:F

    int-to-float v7, v2

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->left:F

    .line 513
    iget v6, p0, Landroid/graphics/RectF;->right:F

    int-to-float v7, v2

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->right:F

    .line 514
    iget v6, p0, Landroid/graphics/RectF;->top:F

    int-to-float v7, v1

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->top:F

    .line 515
    iget v6, p0, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, v1

    div-float/2addr v6, v7

    iput v6, p0, Landroid/graphics/RectF;->bottom:F

    .line 518
    int-to-float v6, v3

    int-to-float v7, v2

    div-float v4, v6, v7

    .line 519
    .local v4, "xBound":F
    iget v6, p0, Landroid/graphics/RectF;->right:F

    cmpl-float v6, v6, v4

    if-lez v6, :cond_0

    .line 520
    iget v6, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v7

    iget v8, p0, Landroid/graphics/RectF;->left:F

    sub-float v8, v4, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v8

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p1, Landroid/graphics/RectF;->right:F

    .line 522
    iput v4, p0, Landroid/graphics/RectF;->right:F

    .line 524
    :cond_0
    int-to-float v6, v0

    int-to-float v7, v1

    div-float v5, v6, v7

    .line 525
    .local v5, "yBound":F
    iget v6, p0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v6, v6, v5

    if-lez v6, :cond_1

    .line 526
    iget v6, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v7

    iget v8, p0, Landroid/graphics/RectF;->top:F

    sub-float v8, v5, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v8

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, p1, Landroid/graphics/RectF;->bottom:F

    .line 528
    iput v5, p0, Landroid/graphics/RectF;->bottom:F

    .line 530
    :cond_1
    return-void
.end method

.method private drawBoundTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIII)V
    .locals 9
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f000000    # 0.5f

    .line 424
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->isMatrixRotatedOrFlipped([F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 425
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->hasBorder()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v4, v0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v4, v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureCoords(FFFF)V

    .line 436
    :goto_0
    int-to-float v0, p2

    int-to-float v1, p3

    int-to-float v2, p4

    int-to-float v3, p5

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->textureRect(FFFF)V

    .line 450
    :cond_0
    :goto_1
    return-void

    .line 432
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-direct {p0, v3, v3, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureCoords(FFFF)V

    goto :goto_0

    .line 439
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    add-int v3, p3, p5

    add-int v4, p2, p4

    move-object v0, p0

    move v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mapPoints([FIIII)[F

    move-result-object v6

    .line 441
    .local v6, "points":[F
    aget v0, v6, v8

    add-float/2addr v0, v7

    float-to-int p2, v0

    .line 442
    const/4 v0, 0x1

    aget v0, v6, v0

    add-float/2addr v0, v7

    float-to-int p3, v0

    .line 443
    const/4 v0, 0x2

    aget v0, v6, v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    sub-int p4, v0, p2

    .line 444
    const/4 v0, 0x3

    aget v0, v6, v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    sub-int p5, v0, p3

    .line 445
    if-lez p4, :cond_0

    if-lez p5, :cond_0

    .line 446
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11Ext;

    move v1, p2

    move v2, p3

    move v3, v8

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/opengles/GL11Ext;->glDrawTexiOES(IIIII)V

    .line 447
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureOES:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureOES:I

    goto :goto_1
.end method

.method private drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IFIIIIF)V
    .locals 9
    .param p1, "from"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "toColor"    # I
    .param p3, "ratio"    # F
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "alpha"    # F

    .prologue
    .line 633
    const v1, 0x3c23d70a    # 0.01f

    cmpg-float v1, p3, v1

    if-gtz v1, :cond_1

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    .line 634
    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIIIF)V

    .line 653
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p3, v1

    if-ltz v1, :cond_2

    .line 637
    int-to-float v2, p4

    int-to-float v3, p5

    int-to-float v4, p6

    move/from16 v0, p7

    int-to-float v5, v0

    move-object v1, p0

    move v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->fillRect(FFFFI)V

    goto :goto_0

    .line 641
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->isOpaque(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x3f733333    # 0.95f

    cmpg-float v1, p8, v1

    if-gez v1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 644
    iget-object v8, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 645
    .local v8, "gl":Ljavax/microedition/khronos/opengles/GL11;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 648
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    const v2, 0x8570

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTexEnvMode(I)V

    .line 649
    move/from16 v0, p8

    invoke-direct {p0, p2, p3, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setMixedColor(IFF)V

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    .line 651
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawBoundTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIII)V

    .line 652
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    const/16 v2, 0x1e01

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTexEnvMode(I)V

    goto :goto_0

    .line 641
    .end local v8    # "gl":Ljavax/microedition/khronos/opengles/GL11;
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;FIIIIF)V
    .locals 9
    .param p1, "from"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "to"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p3, "ratio"    # F
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "alpha"    # F

    .prologue
    .line 1178
    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move v3, p5

    move v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    .line 1179
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIIIF)V

    .line 1251
    :cond_0
    :goto_0
    return-void

    .line 1181
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_2

    move-object v0, p0

    move-object v1, p2

    move v2, p4

    move v3, p5

    move v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    .line 1182
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIIIF)V

    goto :goto_0

    .line 1188
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v0

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 1191
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x3f733333    # 0.95f

    cmpg-float v0, p8, v0

    if-gez v0, :cond_5

    :cond_3
    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 1194
    iget-object v8, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 1195
    .local v8, "gl":Ljavax/microedition/khronos/opengles/GL11;
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1206
    const/high16 v0, 0x3f800000    # 1.0f

    mul-float v1, p8, p3

    sub-float v7, v0, v1

    .line 1209
    .local v7, "comboRatio":F
    const v0, 0x3f733333    # 0.95f

    cmpg-float v0, p8, v0

    if-gez v0, :cond_6

    .line 1210
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p3

    mul-float v1, v1, p8

    div-float/2addr v1, v7

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureAlpha(F)V

    .line 1215
    :goto_3
    const v0, 0x84c1

    invoke-interface {v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 1216
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1218
    const/16 v0, 0xde1

    invoke-interface {v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 1220
    const v0, 0x84c0

    invoke-interface {v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    goto :goto_0

    .line 1188
    .end local v7    # "comboRatio":F
    .end local v8    # "gl":Ljavax/microedition/khronos/opengles/GL11;
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1191
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 1212
    .restart local v7    # "comboRatio":F
    .restart local v8    # "gl":Ljavax/microedition/khronos/opengles/GL11;
    :cond_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureAlpha(F)V

    goto :goto_3

    .line 1223
    :cond_7
    const/16 v0, 0xde1

    invoke-interface {v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 1226
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    const v1, 0x8570

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTexEnvMode(I)V

    .line 1227
    const/16 v0, 0x2300

    const v1, 0x8571

    const v2, 0x47057500    # 34165.0f

    invoke-interface {v8, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 1228
    const/16 v0, 0x2300

    const v1, 0x8572

    const v2, 0x47057500    # 34165.0f

    invoke-interface {v8, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 1233
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v7}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureColor(FFFF)V

    .line 1234
    const/16 v0, 0x2300

    const/16 v1, 0x2201

    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureColor:[F

    const/4 v3, 0x0

    invoke-interface {v8, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvfv(II[FI)V

    .line 1237
    const/16 v0, 0x2300

    const v1, 0x8582

    const v2, 0x47057600    # 34166.0f

    invoke-interface {v8, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 1238
    const/16 v0, 0x2300

    const v1, 0x8592

    const v2, 0x44408000    # 770.0f

    invoke-interface {v8, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 1241
    const/16 v0, 0x2300

    const v1, 0x858a

    const v2, 0x47057600    # 34166.0f

    invoke-interface {v8, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 1242
    const/16 v0, 0x2300

    const v1, 0x859a

    const v2, 0x44408000    # 770.0f

    invoke-interface {v8, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    move-object v0, p0

    move-object v1, p2

    move v2, p4

    move v3, p5

    move v4, p6

    move/from16 v5, p7

    .line 1245
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawBoundTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIII)V

    .line 1248
    const/16 v0, 0xde1

    invoke-interface {v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 1250
    const v0, 0x84c0

    invoke-interface {v8, v0}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    goto/16 :goto_0
.end method

.method private freeRestoreConfig(Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;)V
    .locals 1
    .param p1, "action"    # Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    .prologue
    .line 918
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRecycledRestoreAction:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    iput-object v0, p1, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mNextFree:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    .line 919
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRecycledRestoreAction:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    .line 920
    return-void
.end method

.method private initCircle(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 8
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const v7, 0x8892

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1450
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->initCircleCoordinates()V

    .line 1451
    sget-object v3, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->CIRCLE_COORDINATES:[F

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x20

    div-int/lit8 v1, v3, 0x8

    .line 1452
    .local v1, "size":I
    invoke-static {v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->allocateDirectNativeOrderBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v2

    .line 1453
    .local v2, "xyBuffer":Ljava/nio/FloatBuffer;
    sget-object v3, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->CIRCLE_COORDINATES:[F

    sget-object v4, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->CIRCLE_COORDINATES:[F

    array-length v4, v4

    invoke-virtual {v2, v3, v5, v4}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 1455
    new-array v0, v6, [I

    .line 1456
    .local v0, "name":[I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    invoke-interface {v3, v6, v0, v5}, Lcom/sec/android/gallery3d/glrenderer/GLId;->glGenBuffers(I[II)V

    .line 1457
    aget v3, v0, v5

    iput v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCircleCoords:I

    .line 1459
    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCircleCoords:I

    invoke-interface {p1, v7, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 1460
    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v3

    mul-int/lit8 v3, v3, 0x4

    const v4, 0x88e4

    invoke-interface {p1, v7, v3, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 1462
    return-void
.end method

.method private declared-synchronized initCircleCoordinates()V
    .locals 1

    .prologue
    .line 1445
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->CIRCLE_COORDINATES:[F

    if-nez v0, :cond_0

    .line 1446
    invoke-static {}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->makeCircleCoords()[F

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->CIRCLE_COORDINATES:[F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1447
    :cond_0
    monitor-exit p0

    return-void

    .line 1445
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static isMatrixRotatedOrFlipped([F)Z
    .locals 6
    .param p0, "matrix"    # [F

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const v5, 0x3727c5ac    # 1.0E-5f

    .line 662
    const v0, 0x3727c5ac    # 1.0E-5f

    .line 663
    .local v0, "eps":F
    const/4 v3, 0x4

    aget v3, p0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_0

    aget v3, p0, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_0

    aget v3, p0, v1

    const v4, -0x48d83a54    # -1.0E-5f

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    const/4 v3, 0x5

    aget v3, p0, v3

    cmpl-float v3, v3, v5

    if-lez v3, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    return v1
.end method

.method private static makeCircleCoords()[F
    .locals 20

    .prologue
    .line 1415
    const/high16 v13, 0x3f800000    # 1.0f

    .line 1416
    .local v13, "radius":F
    const/4 v3, 0x0

    .line 1417
    .local v3, "center_x":F
    const/4 v4, 0x0

    .line 1420
    .local v4, "center_y":F
    const/16 v14, 0x120

    new-array v2, v14, [F

    .line 1421
    .local v2, "buffer":[F
    const/4 v6, 0x0

    .line 1424
    .local v6, "idx":I
    const/16 v8, 0x8f

    .line 1426
    .local v8, "outerVertexCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    move v7, v6

    .end local v6    # "idx":I
    .local v7, "idx":I
    :goto_0
    if-ge v5, v8, :cond_0

    .line 1427
    int-to-float v14, v5

    int-to-float v15, v8

    div-float v11, v14, v15

    .line 1428
    .local v11, "percent":F
    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v14, v11

    float-to-double v14, v14

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    mul-double v14, v14, v16

    double-to-float v12, v14

    .line 1431
    .local v12, "rad":F
    float-to-double v14, v3

    float-to-double v0, v13

    move-wide/from16 v16, v0

    float-to-double v0, v12

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v9, v14

    .line 1432
    .local v9, "outer_x":F
    float-to-double v14, v4

    float-to-double v0, v13

    move-wide/from16 v16, v0

    float-to-double v0, v12

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v10, v14

    .line 1434
    .local v10, "outer_y":F
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "idx":I
    .restart local v6    # "idx":I
    aput v9, v2, v7

    .line 1435
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "idx":I
    .restart local v7    # "idx":I
    aput v10, v2, v6

    .line 1426
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1438
    .end local v9    # "outer_x":F
    .end local v10    # "outer_y":F
    .end local v11    # "percent":F
    .end local v12    # "rad":F
    :cond_0
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "idx":I
    .restart local v6    # "idx":I
    const/4 v14, 0x0

    aget v14, v2, v14

    aput v14, v2, v7

    .line 1439
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "idx":I
    .restart local v7    # "idx":I
    const/4 v14, 0x1

    aget v14, v2, v14

    aput v14, v2, v6

    .line 1441
    return-object v2
.end method

.method private mapPoints([FIIII)[F
    .locals 10
    .param p1, "m"    # [F
    .param p2, "x1"    # I
    .param p3, "y1"    # I
    .param p4, "x2"    # I
    .param p5, "y2"    # I

    .prologue
    const/4 v9, 0x7

    const/4 v8, 0x5

    const/4 v3, 0x0

    const/4 v7, 0x3

    .line 382
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMapPointsBuffer:[F

    .line 398
    .local v0, "r":[F
    const/4 v5, 0x6

    .line 399
    .local v5, "srcOffset":I
    int-to-float v2, p2

    aput v2, v0, v5

    .line 400
    int-to-float v2, p3

    aput v2, v0, v9

    .line 401
    const/16 v2, 0x8

    const/4 v4, 0x0

    aput v4, v0, v2

    .line 402
    const/16 v2, 0x9

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v0, v2

    .line 404
    const/4 v1, 0x0

    .local v1, "resultOffset":I
    move-object v2, p1

    move-object v4, v0

    .line 405
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 406
    aget v2, v0, v1

    aget v4, v0, v7

    div-float/2addr v2, v4

    aput v2, v0, v1

    .line 407
    const/4 v2, 0x1

    aget v4, v0, v2

    aget v6, v0, v7

    div-float/2addr v4, v6

    aput v4, v0, v2

    .line 410
    int-to-float v2, p4

    aput v2, v0, v5

    .line 411
    int-to-float v2, p5

    aput v2, v0, v9

    .line 412
    const/4 v1, 0x2

    move-object v2, p1

    move-object v4, v0

    .line 413
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 414
    aget v2, v0, v1

    aget v3, v0, v8

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 415
    aget v2, v0, v7

    aget v3, v0, v8

    div-float/2addr v2, v3

    aput v2, v0, v7

    .line 417
    return-object v0
.end method

.method private obtainRestoreConfig()Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;
    .locals 2

    .prologue
    .line 923
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRecycledRestoreAction:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    if-eqz v1, :cond_0

    .line 924
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRecycledRestoreAction:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    .line 925
    .local v0, "result":Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;
    iget-object v1, v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mNextFree:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    iput-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRecycledRestoreAction:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    .line 928
    .end local v0    # "result":Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;-><init>(Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$1;)V

    goto :goto_0
.end method

.method private restoreTransform()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 975
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTempMatrix:[F

    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    const/16 v2, 0x10

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 976
    return-void
.end method

.method private saveTransform()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 971
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTempMatrix:[F

    const/16 v2, 0x10

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 972
    return-void
.end method

.method private setMixedColor(IFF)V
    .locals 11
    .param p1, "toColor"    # I
    .param p2, "ratio"    # F
    .param p3, "alpha"    # F

    .prologue
    const v10, 0x47057500    # 34165.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const v9, 0x44408000    # 770.0f

    const v8, 0x47057600    # 34166.0f

    const/16 v7, 0x2300

    .line 565
    sub-float v4, v5, p2

    mul-float v1, p3, v4

    .line 566
    .local v1, "combo":F
    mul-float v4, p3, p2

    sub-float/2addr v5, v1

    div-float v3, v4, v5

    .line 571
    .local v3, "scale":F
    ushr-int/lit8 v4, p1, 0x18

    int-to-float v4, v4

    mul-float/2addr v4, v3

    const v5, 0x477e0100    # 65025.0f

    div-float v0, v4, v5

    .line 572
    .local v0, "colorScale":F
    ushr-int/lit8 v4, p1, 0x10

    and-int/lit16 v4, v4, 0xff

    int-to-float v4, v4

    mul-float/2addr v4, v0

    ushr-int/lit8 v5, p1, 0x8

    and-int/lit16 v5, v5, 0xff

    int-to-float v5, v5

    mul-float/2addr v5, v0

    and-int/lit16 v6, p1, 0xff

    int-to-float v6, v6

    mul-float/2addr v6, v0

    invoke-direct {p0, v4, v5, v6, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureColor(FFFF)V

    .line 575
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 576
    .local v2, "gl":Ljavax/microedition/khronos/opengles/GL11;
    const/16 v4, 0x2201

    iget-object v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureColor:[F

    const/4 v6, 0x0

    invoke-interface {v2, v7, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvfv(II[FI)V

    .line 578
    const v4, 0x8571

    invoke-interface {v2, v7, v4, v10}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 579
    const v4, 0x8572

    invoke-interface {v2, v7, v4, v10}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 580
    const v4, 0x8581

    invoke-interface {v2, v7, v4, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 581
    const v4, 0x8591

    const/high16 v5, 0x44400000    # 768.0f

    invoke-interface {v2, v7, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 582
    const v4, 0x8589

    invoke-interface {v2, v7, v4, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 583
    const v4, 0x8599

    invoke-interface {v2, v7, v4, v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 586
    const v4, 0x8582

    invoke-interface {v2, v7, v4, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 587
    const v4, 0x8592

    invoke-interface {v2, v7, v4, v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 590
    const v4, 0x858a

    invoke-interface {v2, v7, v4, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 591
    const v4, 0x859a

    invoke-interface {v2, v7, v4, v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexEnvf(IIF)V

    .line 593
    return-void
.end method

.method private setRenderTarget(Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V
    .locals 6
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    .prologue
    const/4 v4, 0x1

    const v1, 0x8d40

    const/4 v5, 0x0

    .line 979
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;

    .line 981
    .local v0, "gl11ep":Ljavax/microedition/khronos/opengles/GL11ExtensionPack;
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetTexture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 982
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mFrameBuffer:[I

    invoke-interface {v2, v4, v3, v5}, Lcom/sec/android/gallery3d/glrenderer/GLId;->glGenBuffers(I[II)V

    .line 983
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mFrameBuffer:[I

    aget v2, v2, v5

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glBindFramebufferOES(II)V

    .line 986
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetTexture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    if-eqz v2, :cond_1

    if-nez p1, :cond_1

    .line 987
    invoke-interface {v0, v1, v5}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glBindFramebufferOES(II)V

    .line 988
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mFrameBuffer:[I

    invoke-interface {v0, v4, v2, v5}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glDeleteFramebuffersOES(I[II)V

    .line 991
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetTexture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    .line 992
    if-nez p1, :cond_2

    .line 993
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mScreenWidth:I

    iget v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mScreenHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setSize(II)V

    .line 1006
    :goto_0
    return-void

    .line 995
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->getHeight()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setSize(II)V

    .line 997
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->isLoaded()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->prepare(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 999
    :cond_3
    const v2, 0x8ce0

    const/16 v3, 0xde1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->getId()I

    move-result v4

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glFramebufferTexture2DOES(IIIII)V

    .line 1004
    invoke-static {v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->checkFramebufferStatus(Ljavax/microedition/khronos/opengles/GL11ExtensionPack;)V

    goto :goto_0
.end method

.method private setTextureColor(FFFF)V
    .locals 2
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "alpha"    # F

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureColor:[F

    .line 548
    .local v0, "color":[F
    const/4 v1, 0x0

    aput p1, v0, v1

    .line 549
    const/4 v1, 0x1

    aput p2, v0, v1

    .line 550
    const/4 v1, 0x2

    aput p3, v0, v1

    .line 551
    const/4 v1, 0x3

    aput p4, v0, v1

    .line 552
    return-void
.end method

.method private setTextureCoords(FFFF)V
    .locals 5
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 823
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 824
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    sub-float v1, p3, p1

    aput v1, v0, v4

    .line 825
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    const/4 v1, 0x5

    sub-float v2, p4, p2

    aput v2, v0, v1

    .line 826
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    const/16 v1, 0xa

    aput v3, v0, v1

    .line 827
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    const/16 v1, 0xc

    aput p1, v0, v1

    .line 828
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    const/16 v1, 0xd

    aput p2, v0, v1

    .line 829
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    const/16 v1, 0xf

    aput v3, v0, v1

    .line 830
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureMatrixValues:[F

    invoke-interface {v0, v1, v4}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 831
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 832
    return-void
.end method

.method private setTextureCoords(Landroid/graphics/RectF;)V
    .locals 4
    .param p1, "source"    # Landroid/graphics/RectF;

    .prologue
    .line 818
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->right:F

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureCoords(FFFF)V

    .line 819
    return-void
.end method

.method private setTextureCoords([F)V
    .locals 2
    .param p1, "mTextureTransform"    # [F

    .prologue
    .line 835
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 836
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 837
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 838
    return-void
.end method

.method private textureRect(FFFF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    const/4 v3, 0x0

    .line 329
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 331
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->saveTransform()V

    .line 332
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->translate(FF)V

    .line 333
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p3, p4, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->scale(FFF)V

    .line 335
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    invoke-interface {v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 336
    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v3, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 338
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->restoreTransform()V

    .line 339
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureRect:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureRect:I

    .line 340
    return-void
.end method

.method private uploadBuffer(Ljava/nio/Buffer;I)I
    .locals 6
    .param p1, "buf"    # Ljava/nio/Buffer;
    .param p2, "elementSize"    # I

    .prologue
    const v5, 0x8892

    const/4 v4, 0x0

    .line 1110
    const/4 v2, 0x1

    new-array v1, v2, [I

    .line 1111
    .local v1, "bufferIds":[I
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    array-length v3, v1

    invoke-interface {v2, v3, v1, v4}, Lcom/sec/android/gallery3d/glrenderer/GLId;->glGenBuffers(I[II)V

    .line 1112
    aget v0, v1, v4

    .line 1113
    .local v0, "bufferId":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v2, v5, v0}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 1114
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Ljava/nio/Buffer;->capacity()I

    move-result v3

    mul-int/2addr v3, p2

    const v4, 0x88e4

    invoke-interface {v2, v5, v3, p1, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 1116
    return v0
.end method


# virtual methods
.method public beginRenderTarget(Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V
    .locals 2
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    .prologue
    .line 1017
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->save()V

    .line 1018
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetStack:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetTexture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1019
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setRenderTarget(Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V

    .line 1020
    return-void
.end method

.method public clearBuffer()V
    .locals 1

    .prologue
    .line 814
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->clearBuffer([F)V

    .line 815
    return-void
.end method

.method public clearBuffer([F)V
    .locals 5
    .param p1, "argb"    # [F

    .prologue
    const/4 v2, 0x0

    .line 803
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 804
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x1

    aget v1, p1, v1

    const/4 v2, 0x2

    aget v2, p1, v2

    const/4 v3, 0x3

    aget v3, p1, v3

    const/4 v4, 0x0

    aget v4, p1, v4

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glClearColor(FFFF)V

    .line 809
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x4000

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glClear(I)V

    .line 810
    return-void

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v2, v2, v2, v1}, Ljavax/microedition/khronos/opengles/GL11;->glClearColor(FFFF)V

    goto :goto_0
.end method

.method public clipRect(IIII)Z
    .locals 9
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1140
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mapPoints([FIIII)[F

    move-result-object v8

    .line 1144
    .local v8, "point":[F
    const/4 v0, 0x0

    aget v0, v8, v0

    const/4 v1, 0x2

    aget v1, v8, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1145
    const/4 v0, 0x2

    aget v0, v8, v0

    float-to-int p1, v0

    .line 1146
    const/4 v0, 0x0

    aget v0, v8, v0

    float-to-int p3, v0

    .line 1151
    :goto_0
    const/4 v0, 0x1

    aget v0, v8, v0

    const/4 v1, 0x3

    aget v1, v8, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 1152
    const/4 v0, 0x3

    aget v0, v8, v0

    float-to-int p2, v0

    .line 1153
    const/4 v0, 0x1

    aget v0, v8, v0

    float-to-int p4, v0

    .line 1158
    :goto_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mClipRect:Landroid/graphics/Rect;

    .line 1160
    .local v6, "clip":Landroid/graphics/Rect;
    invoke-virtual {v6, p1, p2, p3, p4}, Landroid/graphics/Rect;->intersect(IIII)Z

    move-result v7

    .line 1161
    .local v7, "intersect":Z
    if-nez v7, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1162
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v1, v6, Landroid/graphics/Rect;->left:I

    iget v2, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glScissor(IIII)V

    .line 1163
    return v7

    .line 1148
    .end local v6    # "clip":Landroid/graphics/Rect;
    .end local v7    # "intersect":Z
    :cond_1
    const/4 v0, 0x0

    aget v0, v8, v0

    float-to-int p1, v0

    .line 1149
    const/4 v0, 0x2

    aget v0, v8, v0

    float-to-int p3, v0

    goto :goto_0

    .line 1155
    :cond_2
    const/4 v0, 0x1

    aget v0, v8, v0

    float-to-int p2, v0

    .line 1156
    const/4 v0, 0x3

    aget v0, v8, v0

    float-to-int p4, v0

    goto :goto_1
.end method

.method public copyTexture(IIII)Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .locals 14
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 1255
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    invoke-static {v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->isMatrixRotatedOrFlipped([F)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1256
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "cannot support rotated matrix"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1258
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    add-int v5, p2, p4

    add-int v6, p1, p3

    move-object v2, p0

    move v4, p1

    move/from16 v7, p2

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mapPoints([FIIII)[F

    move-result-object v12

    .line 1259
    .local v12, "points":[F
    const/4 v3, 0x0

    aget v3, v12, v3

    float-to-int p1, v3

    .line 1260
    const/4 v3, 0x1

    aget v3, v12, v3

    float-to-int v0, v3

    move/from16 p2, v0

    .line 1261
    const/4 v3, 0x2

    aget v3, v12, v3

    float-to-int v3, v3

    sub-int p3, v3, p1

    .line 1262
    const/4 v3, 0x3

    aget v3, v12, v3

    float-to-int v3, v3

    sub-int p4, v3, p2

    .line 1264
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 1267
    .local v2, "gl":Ljavax/microedition/khronos/opengles/GL11;
    new-instance v13, Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    const/4 v3, 0x1

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-direct {v13, v0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;-><init>(IIZ)V

    .line 1271
    .local v13, "texture":Lcom/sec/android/gallery3d/glrenderer/RawTexture;
    const/16 v3, 0xde1

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->getId()I

    move-result v4

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 1272
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->setSize(II)V

    .line 1274
    const/4 v3, 0x4

    new-array v11, v3, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v11, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v11, v3

    const/4 v3, 0x2

    aput p3, v11, v3

    const/4 v3, 0x3

    aput p4, v11, v3

    .line 1275
    .local v11, "cropRect":[I
    const/16 v3, 0xde1

    const v4, 0x8b9d

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v11, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameteriv(II[II)V

    .line 1277
    const/16 v3, 0xde1

    const/16 v4, 0x2802

    const v5, 0x812f

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameteri(III)V

    .line 1279
    const/16 v3, 0xde1

    const/16 v4, 0x2803

    const v5, 0x812f

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameteri(III)V

    .line 1281
    const/16 v3, 0xde1

    const/16 v4, 0x2801

    const v5, 0x46180400    # 9729.0f

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 1283
    const/16 v3, 0xde1

    const/16 v4, 0x2800

    const v5, 0x46180400    # 9729.0f

    invoke-interface {v2, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 1285
    const/16 v3, 0xde1

    const/4 v4, 0x0

    const/16 v5, 0x1907

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->getTextureWidth()I

    move-result v8

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/glrenderer/RawTexture;->getTextureHeight()I

    move-result v9

    const/4 v10, 0x0

    move v6, p1

    move/from16 v7, p2

    invoke-interface/range {v2 .. v10}, Ljavax/microedition/khronos/opengles/GL11;->glCopyTexImage2D(IIIIIIII)V

    .line 1289
    return-object v13
.end method

.method public currentAnimationTimeMillis()J
    .locals 2

    .prologue
    .line 1136
    iget-wide v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAnimationTime:J

    return-wide v0
.end method

.method public deleteBuffer(I)V
    .locals 2
    .param p1, "bufferId"    # I

    .prologue
    .line 853
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    monitor-enter v1

    .line 854
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDeleteBuffers:Lcom/sec/android/gallery3d/util/IntArray;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/util/IntArray;->add(I)V

    .line 855
    monitor-exit v1

    .line 856
    return-void

    .line 855
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deleteRecycledResources()V
    .locals 7

    .prologue
    .line 860
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    monitor-enter v2

    .line 861
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    .line 862
    .local v0, "ids":Lcom/sec/android/gallery3d/util/IntArray;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 863
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->size()I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->getInternalArray()[I

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v1, v3, v4, v5, v6}, Lcom/sec/android/gallery3d/glrenderer/GLId;->glDeleteTextures(Ljavax/microedition/khronos/opengles/GL11;I[II)V

    .line 864
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->clear()V

    .line 867
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDeleteBuffers:Lcom/sec/android/gallery3d/util/IntArray;

    .line 868
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 869
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->size()I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->getInternalArray()[I

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v1, v3, v4, v5, v6}, Lcom/sec/android/gallery3d/glrenderer/GLId;->glDeleteBuffers(Ljavax/microedition/khronos/opengles/GL11;I[II)V

    .line 870
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/IntArray;->clear()V

    .line 872
    :cond_1
    monitor-exit v2

    .line 873
    return-void

    .line 872
    .end local v0    # "ids":Lcom/sec/android/gallery3d/util/IntArray;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public drawCircle(FFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V
    .locals 9
    .param p1, "cx"    # F
    .param p2, "cy"    # F
    .param p3, "radius"    # F
    .param p4, "paint"    # Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .prologue
    const/16 v8, 0x1406

    const/16 v7, 0x90

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1466
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 1468
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getColor()I

    move-result v2

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getAlpha()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setColorMode(IF)V

    .line 1469
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getLineWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineWidth(F)V

    .line 1470
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getAntiAlias()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineSmooth(Z)V

    .line 1472
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->saveTransform()V

    .line 1473
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->translate(FF)V

    .line 1474
    div-float v1, p3, v6

    div-float v2, p3, v6

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->scale(FFF)V

    .line 1476
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    invoke-interface {v0, v1, v4}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 1478
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v2, 0x8892

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCircleCoords:I

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 1479
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v5, v8, v4, v4}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    .line 1481
    invoke-virtual {p4}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getStyle()Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->FILL:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    if-ne v1, v2, :cond_0

    .line 1482
    const/4 v1, 0x6

    invoke-interface {v0, v1, v4, v7}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 1487
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v2, 0x8892

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBoxCoords:I

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 1488
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v5, v8, v4, v4}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    .line 1490
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->restoreTransform()V

    .line 1491
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawCircle:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawCircle:I

    .line 1492
    return-void

    .line 1484
    :cond_0
    invoke-interface {v0, v5, v4, v7}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    goto :goto_0
.end method

.method public drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V
    .locals 4
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F
    .param p5, "paint"    # Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 257
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getColor()I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setColorMode(IF)V

    .line 258
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getLineWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineWidth(F)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getAntiAlias()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineSmooth(Z)V

    .line 263
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->saveTransform()V

    .line 264
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->translate(FF)V

    .line 265
    sub-float v1, p3, p1

    sub-float v2, p4, p2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->scale(FFF)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 268
    const/4 v1, 0x3

    const/4 v2, 0x4

    const/4 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 270
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->restoreTransform()V

    .line 271
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    .line 272
    return-void
.end method

.method public drawMesh(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIIIII)V
    .locals 8
    .param p1, "tex"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "xyBuffer"    # I
    .param p5, "uvBuffer"    # I
    .param p6, "indexBuffer"    # I
    .param p7, "indexCount"    # I

    .prologue
    const/4 v4, 0x0

    const v7, 0x8892

    const/16 v6, 0x1406

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 345
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    .line 346
    .local v0, "alpha":F
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x3f733333    # 0.95f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 350
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureAlpha(F)V

    .line 354
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {p0, v4, v4, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureCoords(FFFF)V

    .line 356
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->saveTransform()V

    .line 357
    int-to-float v1, p2

    int-to-float v3, p3

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->translate(FF)V

    .line 359
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    invoke-interface {v1, v3, v2}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 361
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v7, p4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 362
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v5, v6, v2, v2}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    .line 364
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v7, p5}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 365
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v5, v6, v2, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    .line 367
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v3, 0x8893

    invoke-interface {v1, v3, p6}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 368
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v3, 0x5

    const/16 v4, 0x1401

    invoke-interface {v1, v3, p7, v4, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawElements(IIII)V

    .line 371
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBoxCoords:I

    invoke-interface {v1, v7, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 372
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v5, v6, v2, v2}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIII)V

    .line 373
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v1, v5, v6, v2, v2}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    .line 375
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->restoreTransform()V

    .line 376
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawMesh:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawMesh:I

    goto :goto_0

    :cond_2
    move v1, v2

    .line 348
    goto :goto_1
.end method

.method public drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IFIIII)V
    .locals 9
    .param p1, "from"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "toColor"    # I
    .param p3, "ratio"    # F
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "w"    # I
    .param p7, "h"    # I

    .prologue
    .line 535
    iget v8, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IFIIIIF)V

    .line 536
    return-void
.end method

.method public drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IFLandroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 7
    .param p1, "from"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "toColor"    # I
    .param p3, "ratio"    # F
    .param p4, "source"    # Landroid/graphics/RectF;
    .param p5, "target"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 598
    invoke-virtual {p5}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p5}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 628
    :cond_0
    :goto_0
    return-void

    .line 600
    :cond_1
    const v0, 0x3c23d70a    # 0.01f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_2

    .line 601
    invoke-virtual {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0

    .line 603
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_3

    .line 604
    iget v1, p5, Landroid/graphics/RectF;->left:F

    iget v2, p5, Landroid/graphics/RectF;->top:F

    invoke-virtual {p5}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {p5}, Landroid/graphics/RectF;->height()F

    move-result v4

    move-object v0, p0

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->fillRect(FFFFI)V

    goto :goto_0

    .line 608
    :cond_3
    iget v6, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    .line 611
    .local v6, "alpha":F
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureSourceRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureTargetRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 613
    iget-object p4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureSourceRect:Landroid/graphics/RectF;

    .line 614
    iget-object p5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureTargetRect:Landroid/graphics/RectF;

    .line 616
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->isOpaque(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x3f733333    # 0.95f

    cmpg-float v0, v6, v0

    if-gez v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 619
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    const v1, 0x8570

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTexEnvMode(I)V

    .line 623
    invoke-direct {p0, p2, p3, v6}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setMixedColor(IFF)V

    .line 624
    invoke-static {p4, p5, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->convertCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V

    .line 625
    invoke-direct {p0, p4}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureCoords(Landroid/graphics/RectF;)V

    .line 626
    iget v0, p5, Landroid/graphics/RectF;->left:F

    iget v1, p5, Landroid/graphics/RectF;->top:F

    invoke-virtual {p5}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p5}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->textureRect(FFFF)V

    .line 627
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    const/16 v1, 0x1e01

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTexEnvMode(I)V

    goto :goto_0

    .line 616
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;FIIII)V
    .locals 9
    .param p1, "from"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "to"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p3, "ratio"    # F
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "w"    # I
    .param p7, "h"    # I

    .prologue
    .line 1172
    iget v8, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawMixed(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;FIIIIF)V

    .line 1173
    return-void
.end method

.method public drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "paint"    # Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 236
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getColor()I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setColorMode(IF)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getLineWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineWidth(F)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getAntiAlias()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineSmooth(Z)V

    .line 242
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->saveTransform()V

    .line 243
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->translate(FF)V

    .line 244
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p3, p4, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->scale(FFF)V

    .line 246
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 247
    const/4 v1, 0x2

    const/4 v2, 0x6

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 249
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->restoreTransform()V

    .line 250
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    .line 251
    return-void
.end method

.method public drawRelativeRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "paint"    # Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 1306
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getColor()I

    move-result v2

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setColorMode(IF)V

    .line 1307
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getLineWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineWidth(F)V

    .line 1308
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->getAntiAlias()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setLineSmooth(Z)V

    .line 1310
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->translate(FFF)V

    .line 1311
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p3, p4, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->scale(FFF)V

    .line 1313
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 1314
    const/4 v1, 0x2

    const/4 v2, 0x6

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 1316
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    .line 1317
    return-void
.end method

.method public drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIII)V
    .locals 7
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 455
    iget v6, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIIIF)V

    .line 456
    return-void
.end method

.method public drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIIIF)V
    .locals 2
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "alpha"    # F

    .prologue
    .line 462
    if-lez p4, :cond_0

    if-gtz p5, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x3f733333    # 0.95f

    cmpg-float v0, p6, v0

    if-gez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 466
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    invoke-virtual {v0, p6}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureAlpha(F)V

    .line 468
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->drawBoundTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IIII)V

    goto :goto_0

    .line 464
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 4
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "source"    # Landroid/graphics/RectF;
    .param p3, "target"    # Landroid/graphics/RectF;

    .prologue
    const/4 v1, 0x0

    .line 473
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureSourceRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureTargetRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 478
    iget-object p2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureSourceRect:Landroid/graphics/RectF;

    .line 479
    iget-object p3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mDrawTextureTargetRect:Landroid/graphics/RectF;

    .line 481
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    const v2, 0x3f733333    # 0.95f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 483
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 484
    invoke-static {p2, p3, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->convertCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V

    .line 485
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureCoords(Landroid/graphics/RectF;)V

    .line 486
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureAlpha(F)V

    .line 487
    iget v0, p3, Landroid/graphics/RectF;->left:F

    iget v1, p3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->textureRect(FFFF)V

    goto :goto_0

    .line 481
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;[FIIII)V
    .locals 4
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "mTextureTransform"    # [F
    .param p3, "x"    # I
    .param p4, "y"    # I
    .param p5, "w"    # I
    .param p6, "h"    # I

    .prologue
    .line 493
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    const v2, 0x3f733333    # 0.95f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setBlendEnabled(Z)V

    .line 495
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->bindTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 499
    :goto_1
    return-void

    .line 493
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 496
    :cond_2
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setTextureCoords([F)V

    .line 497
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setTextureAlpha(F)V

    .line 498
    int-to-float v0, p3

    int-to-float v1, p4

    int-to-float v2, p5

    int-to-float v3, p6

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->textureRect(FFFF)V

    goto :goto_1
.end method

.method public dumpStatisticsAndClear()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 957
    const-string v1, "MESH:%d, TEX_OES:%d, TEX_RECT:%d, FILL_RECT:%d, LINE:%d, CIRCLE:%d"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawMesh:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureRect:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureOES:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountFillRect:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawCircle:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 961
    .local v0, "line":Ljava/lang/String;
    iput v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawMesh:I

    .line 962
    iput v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureRect:I

    .line 963
    iput v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountTextureOES:I

    .line 964
    iput v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountFillRect:I

    .line 965
    iput v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawLine:I

    .line 966
    iput v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountDrawCircle:I

    .line 967
    const-string v1, "GLCanvasImp"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    return-void
.end method

.method public endRenderTarget()V
    .locals 3

    .prologue
    .line 1010
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetStack:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetStack:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    .line 1011
    .local v0, "texture":Lcom/sec/android/gallery3d/glrenderer/RawTexture;
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->setRenderTarget(Lcom/sec/android/gallery3d/glrenderer/RawTexture;)V

    .line 1012
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->restore()V

    .line 1013
    return-void
.end method

.method public fillRect(FFFFI)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "color"    # I

    .prologue
    const/4 v3, 0x0

    .line 276
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLState:Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;

    iget v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    invoke-virtual {v1, p5, v2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$GLState;->setColorMode(IF)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 279
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->saveTransform()V

    .line 280
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->translate(FF)V

    .line 281
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p3, p4, v1}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->scale(FFF)V

    .line 283
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    invoke-interface {v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 284
    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {v0, v1, v3, v2}, Ljavax/microedition/khronos/opengles/GL11;->glDrawArrays(III)V

    .line 286
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->restoreTransform()V

    .line 287
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountFillRect:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mCountFillRect:I

    .line 288
    return-void
.end method

.method public getAllTextures()Ljava/util/WeakHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/sec/android/gallery3d/glrenderer/BasicTexture;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1336
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAllTextures()Ljava/util/WeakHashMap;

    move-result-object v0

    return-object v0
.end method

.method public getAlpha()F
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    return v0
.end method

.method public getBounds(Landroid/graphics/Rect;IIII)V
    .locals 0
    .param p1, "bounds"    # Landroid/graphics/Rect;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 1127
    return-void
.end method

.method public getGLId()Lcom/sec/android/gallery3d/glrenderer/GLId;
    .locals 1

    .prologue
    .line 1131
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGLId:Lcom/sec/android/gallery3d/glrenderer/GLId;

    return-object v0
.end method

.method public getTextureIdBuffer()[I
    .locals 1

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTextureId:[I

    return-object v0
.end method

.method public getUploadedCount()I
    .locals 1

    .prologue
    .line 1370
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUploadedCount:I

    return v0
.end method

.method public getViewMatrix()[F
    .locals 1

    .prologue
    .line 1320
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    return-object v0
.end method

.method public initializeTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILjava/nio/Buffer;II)V
    .locals 11
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "buffer"    # Ljava/nio/Buffer;
    .param p5, "format"    # I
    .param p6, "type"    # I

    .prologue
    .line 1386
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTarget()I

    move-result v10

    .line 1387
    .local v10, "target":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getId()I

    move-result v1

    invoke-interface {v0, v10, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 1388
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/4 v6, 0x0

    move/from16 v3, p5

    move v4, p2

    move v5, p3

    move/from16 v7, p5

    move/from16 v8, p6

    move-object v9, p4

    invoke-interface/range {v0 .. v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 1393
    return-void
.end method

.method public initializeTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 1086
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTarget()I

    move-result v0

    .line 1087
    .local v0, "target":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getId()I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 1088
    invoke-static {v0, v3, p2, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 1089
    return-void
.end method

.method public initializeTextureSize(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;II)V
    .locals 10
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "format"    # I
    .param p3, "type"    # I

    .prologue
    const/4 v2, 0x0

    .line 1077
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTarget()I

    move-result v1

    .line 1078
    .local v1, "target":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getId()I

    move-result v3

    invoke-interface {v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 1079
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureWidth()I

    move-result v4

    .line 1080
    .local v4, "width":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTextureHeight()I

    move-result v5

    .line 1081
    .local v5, "height":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v9, 0x0

    move v3, p2

    move v6, v2

    move v7, p2

    move v8, p3

    invoke-interface/range {v0 .. v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 1082
    return-void
.end method

.method public invalidateAllTextures()V
    .locals 4

    .prologue
    .line 1350
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->getAllTextures()Ljava/util/WeakHashMap;

    move-result-object v3

    monitor-enter v3

    .line 1351
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->getAllTextures()Ljava/util/WeakHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .line 1352
    .local v1, "t":Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    const/4 v2, 0x0

    iput v2, v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mState:I

    .line 1353
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->setAssociatedCanvas(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    goto :goto_0

    .line 1355
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "t":Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1356
    return-void
.end method

.method public multiplyAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 224
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 225
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    .line 226
    return-void

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public multiplyMatrix([FI)V
    .locals 6
    .param p1, "matrix"    # [F
    .param p2, "offset"    # I

    .prologue
    const/4 v1, 0x0

    .line 323
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTempMatrix:[F

    .line 324
    .local v0, "temp":[F
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    move v3, v1

    move-object v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 325
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    const/16 v3, 0x10

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 326
    return-void
.end method

.method public recoverFromLightCycle()V
    .locals 0

    .prologue
    .line 1122
    return-void
.end method

.method public resetUploadLimit()V
    .locals 1

    .prologue
    .line 1360
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUploadedCount:I

    .line 1361
    return-void
.end method

.method public restore()V
    .locals 3

    .prologue
    .line 909
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRestoreStack:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 915
    :goto_0
    return-void

    .line 912
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRestoreStack:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRestoreStack:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    .line 913
    .local v0, "config":Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->restore(Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;)V

    .line 914
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->freeRestoreConfig(Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;)V

    goto :goto_0
.end method

.method public rotate(FFFF)V
    .locals 9
    .param p1, "angle"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    const/16 v8, 0x10

    const/4 v1, 0x0

    .line 314
    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-nez v2, :cond_0

    .line 319
    :goto_0
    return-void

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTempMatrix:[F

    .local v0, "temp":[F
    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 316
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    .line 317
    iget-object v4, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    move-object v2, v0

    move v3, v8

    move v5, v1

    move-object v6, v0

    move v7, v1

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 318
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    invoke-static {v0, v8, v2, v1, v8}, Ljava/lang/System;->arraycopy([FI[FII)V

    goto :goto_0
.end method

.method public save()V
    .locals 1

    .prologue
    .line 877
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->save(I)V

    .line 878
    return-void
.end method

.method public save(I)V
    .locals 5
    .param p1, "saveFlags"    # I

    .prologue
    const/4 v4, 0x0

    .line 882
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->obtainRestoreConfig()Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;

    move-result-object v0

    .line 884
    .local v0, "config":Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_0

    .line 885
    iget v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    iput v1, v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mAlpha:F

    .line 891
    :goto_0
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_1

    .line 892
    iget-object v1, v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 898
    :goto_1
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_2

    .line 899
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    iget-object v2, v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mMatrix:[F

    const/16 v3, 0x10

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy([FI[FII)V

    .line 904
    :goto_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRestoreStack:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 905
    return-void

    .line 887
    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mAlpha:F

    goto :goto_0

    .line 894
    :cond_1
    iget-object v1, v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mRect:Landroid/graphics/Rect;

    const v2, 0x7fffffff

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_1

    .line 901
    :cond_2
    iget-object v1, v0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas$ConfigState;->mMatrix:[F

    const/high16 v2, -0x800000    # Float.NEGATIVE_INFINITY

    aput v2, v1, v4

    goto :goto_2
.end method

.method public scale(FFF)V
    .locals 2
    .param p1, "sx"    # F
    .param p2, "sy"    # F
    .param p3, "sz"    # F

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 310
    return-void
.end method

.method public setAllTextues(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V
    .locals 3
    .param p1, "b"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAllTextures()Ljava/util/WeakHashMap;

    move-result-object v1

    monitor-enter v1

    .line 1330
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAllTextures()Ljava/util/WeakHashMap;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331
    monitor-exit v1

    .line 1332
    return-void

    .line 1331
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 213
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 214
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    .line 215
    return-void

    .line 213
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBlendEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 1167
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mBlendEnabled:Z

    .line 1168
    return-void
.end method

.method public setCurrentAnimationTimeMillis(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 1293
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 1294
    iput-wide p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAnimationTime:J

    .line 1295
    return-void

    .line 1293
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSize(II)V
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 180
    if-ltz p1, :cond_2

    if-ltz p2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetTexture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    if-nez v2, :cond_0

    .line 183
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mScreenWidth:I

    .line 184
    iput p2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mScreenHeight:I

    .line 186
    :cond_0
    iput v6, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mAlpha:F

    .line 188
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    .line 189
    .local v0, "gl":Ljavax/microedition/khronos/opengles/GL11;
    invoke-interface {v0, v3, v3, p1, p2}, Ljavax/microedition/khronos/opengles/GL11;->glViewport(IIII)V

    .line 190
    const/16 v2, 0x1701

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 191
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 192
    int-to-float v2, p1

    int-to-float v4, p2

    invoke-static {v0, v5, v2, v5, v4}, Landroid/opengl/GLU;->gluOrtho2D(Ljavax/microedition/khronos/opengles/GL10;FFFF)V

    .line 194
    const/16 v2, 0x1700

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 195
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL11;->glLoadIdentity()V

    .line 197
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    .line 198
    .local v1, "matrix":[F
    invoke-static {v1, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 200
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mTargetTexture:Lcom/sec/android/gallery3d/glrenderer/RawTexture;

    if-nez v2, :cond_1

    .line 201
    int-to-float v2, p2

    invoke-static {v1, v3, v5, v2, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 202
    const/high16 v2, -0x40800000    # -1.0f

    invoke-static {v1, v3, v6, v2, v6}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 206
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v3, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 207
    invoke-interface {v0, v3, v3, p1, p2}, Ljavax/microedition/khronos/opengles/GL11;->glScissor(IIII)V

    .line 209
    return-void

    .end local v0    # "gl":Ljavax/microedition/khronos/opengles/GL11;
    .end local v1    # "matrix":[F
    :cond_2
    move v2, v3

    .line 180
    goto :goto_0
.end method

.method public setTextureParameters(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)V
    .locals 9
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .prologue
    const v8, 0x812f

    const/4 v7, 0x0

    const v6, 0x46180400    # 9729.0f

    .line 1055
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v2

    .line 1056
    .local v2, "width":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v0

    .line 1060
    .local v0, "height":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->sCropRect:[F

    const/4 v4, 0x0

    aput v4, v3, v7

    .line 1061
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->sCropRect:[F

    const/4 v4, 0x1

    int-to-float v5, v0

    aput v5, v3, v4

    .line 1062
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->sCropRect:[F

    const/4 v4, 0x2

    int-to-float v5, v2

    aput v5, v3, v4

    .line 1063
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->sCropRect:[F

    const/4 v4, 0x3

    neg-int v5, v0

    int-to-float v5, v5

    aput v5, v3, v4

    .line 1066
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTarget()I

    move-result v1

    .line 1067
    .local v1, "target":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getId()I

    move-result v4

    invoke-interface {v3, v1, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 1068
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const v4, 0x8b9d

    iget-object v5, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->sCropRect:[F

    invoke-interface {v3, v1, v4, v5, v7}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterfv(II[FI)V

    .line 1069
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2802

    invoke-interface {v3, v1, v4, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameteri(III)V

    .line 1070
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2803

    invoke-interface {v3, v1, v4, v8}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameteri(III)V

    .line 1071
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2801

    invoke-interface {v3, v1, v4, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 1072
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v4, 0x2800

    invoke-interface {v3, v1, v4, v6}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameterf(IIF)V

    .line 1073
    return-void
.end method

.method public setUploadedCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 1375
    iput p1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUploadedCount:I

    .line 1376
    return-void
.end method

.method public texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILandroid/graphics/Bitmap;II)V
    .locals 7
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "xOffset"    # I
    .param p3, "yOffset"    # I
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;
    .param p5, "format"    # I
    .param p6, "type"    # I

    .prologue
    .line 1094
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTarget()I

    move-result v0

    .line 1095
    .local v0, "target":I
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getId()I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 1096
    const/4 v1, 0x0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;II)V

    .line 1097
    return-void
.end method

.method public texSubImage2D(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;IILjava/nio/Buffer;II)V
    .locals 10
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "xOffset"    # I
    .param p3, "yOffset"    # I
    .param p4, "buffer"    # Ljava/nio/Buffer;
    .param p5, "format"    # I
    .param p6, "type"    # I

    .prologue
    .line 1401
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getTarget()I

    move-result v1

    .line 1402
    .local v1, "target":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11;->glBindTexture(II)V

    .line 1403
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mGL:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->getHeight()I

    move-result v6

    move v3, p2

    move v4, p3

    move v7, p5

    move/from16 v8, p6

    move-object v9, p4

    invoke-interface/range {v0 .. v9}, Ljavax/microedition/khronos/opengles/GL11;->glTexSubImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 1407
    return-void
.end method

.method public translate(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    .line 301
    .local v0, "m":[F
    const/16 v1, 0xc

    aget v2, v0, v1

    const/4 v3, 0x0

    aget v3, v0, v3

    mul-float/2addr v3, p1

    const/4 v4, 0x4

    aget v4, v0, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 302
    const/16 v1, 0xd

    aget v2, v0, v1

    const/4 v3, 0x1

    aget v3, v0, v3

    mul-float/2addr v3, p1

    const/4 v4, 0x5

    aget v4, v0, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 303
    const/16 v1, 0xe

    aget v2, v0, v1

    const/4 v3, 0x2

    aget v3, v0, v3

    mul-float/2addr v3, p1

    const/4 v4, 0x6

    aget v4, v0, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 304
    const/16 v1, 0xf

    aget v2, v0, v1

    const/4 v3, 0x3

    aget v3, v0, v3

    mul-float/2addr v3, p1

    const/4 v4, 0x7

    aget v4, v0, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 305
    return-void
.end method

.method public translate(FFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mMatrixValues:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 293
    return-void
.end method

.method public unloadTexture(I)V
    .locals 2
    .param p1, "textureID"    # I

    .prologue
    .line 1298
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    monitor-enter v1

    .line 1299
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/util/IntArray;->add(I)V

    .line 1300
    monitor-exit v1

    .line 1301
    return-void

    .line 1300
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unloadTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;)Z
    .locals 3
    .param p1, "t"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .prologue
    .line 844
    iget-object v1, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    monitor-enter v1

    .line 845
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    .line 847
    :goto_0
    return v0

    .line 846
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUnboundTextures:Lcom/sec/android/gallery3d/util/IntArray;

    iget v2, p1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->mId:I

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/util/IntArray;->add(I)V

    .line 847
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 848
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public uploadBuffer(Ljava/nio/ByteBuffer;)I
    .locals 1
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 1106
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->uploadBuffer(Ljava/nio/Buffer;I)I

    move-result v0

    return v0
.end method

.method public uploadBuffer(Ljava/nio/FloatBuffer;)I
    .locals 1
    .param p1, "buf"    # Ljava/nio/FloatBuffer;

    .prologue
    .line 1101
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->uploadBuffer(Ljava/nio/Buffer;I)I

    move-result v0

    return v0
.end method

.method public uploadLimitReached()Z
    .locals 2

    .prologue
    .line 1365
    iget v0, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mUploadedCount:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public yieldAllTextures()V
    .locals 4

    .prologue
    .line 1341
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAllTextures()Ljava/util/WeakHashMap;

    move-result-object v3

    monitor-enter v3

    .line 1342
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glrenderer/GLES11Canvas;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAllTextures()Ljava/util/WeakHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;

    .line 1343
    .local v1, "t":Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/BasicTexture;->yield()V

    goto :goto_0

    .line 1345
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "t":Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1346
    return-void
.end method
