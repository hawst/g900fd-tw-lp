.class public Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
.super Ljava/lang/Object;
.source "BucketHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/BucketHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BucketEntry"
.end annotation


# instance fields
.field public bucketId:I

.field public bucketName:Ljava/lang/String;

.field public bucketPath:Ljava/lang/String;

.field public dateTaken:I

.field public isCamera:Z

.field public isDcim:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    iput p1, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    .line 264
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->ensureNotNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketName:Ljava/lang/String;

    .line 265
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    iput p1, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    .line 270
    invoke-static {p3}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->getFolderpathFrom(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketPath:Ljava/lang/String;

    .line 271
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/BucketHelper;->isCameraPath(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->isCamera:Z

    .line 272
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCIMFolderMerge:Z

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/BucketHelper;->isDcimPath(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->isDcim:Z

    .line 277
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->isCamera:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->isDcim:Z

    if-eqz v0, :cond_2

    .line 278
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDCIMName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketName:Ljava/lang/String;

    .line 281
    :goto_1
    return-void

    .line 275
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->isCamera:Z

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->isDcim:Z

    goto :goto_0

    .line 280
    :cond_2
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->ensureNotNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketName:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 291
    instance-of v2, p1, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    if-nez v2, :cond_1

    .line 293
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 292
    check-cast v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;

    .line 293
    .local v0, "entry":Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;
    iget v2, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    iget v3, v0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/sec/android/gallery3d/data/BucketHelper$BucketEntry;->bucketId:I

    return v0
.end method
