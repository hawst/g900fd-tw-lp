.class Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;
.super Ljava/lang/Object;
.source "GlRootView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlRootView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IdleRunner"
.end annotation


# instance fields
.field private mActive:Z

.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 1

    .prologue
    .line 890
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 892
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->mActive:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlRootView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/glcore/GlRootView$1;

    .prologue
    .line 890
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public enable()V
    .locals 1

    .prologue
    .line 921
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->mActive:Z

    if-eqz v0, :cond_0

    .line 924
    :goto_0
    return-void

    .line 922
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->mActive:Z

    .line 923
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 897
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$500(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayDeque;

    move-result-object v3

    monitor-enter v3

    .line 898
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->mActive:Z

    .line 899
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$500(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayDeque;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    monitor-exit v3

    .line 917
    :goto_0
    return-void

    .line 900
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$500(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayDeque;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;

    .line 901
    .local v1, "listener":Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 902
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$600(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 903
    const/4 v0, 0x0

    .line 905
    .local v0, "keepInQueue":Z
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$700(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 906
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mCanvas:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$700(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v3, v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    invoke-interface {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;->onGLIdle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v0

    .line 911
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$600(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 913
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$500(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayDeque;

    move-result-object v3

    monitor-enter v3

    .line 914
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$500(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayDeque;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 915
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderRequested:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mIdleListeners:Ljava/util/ArrayDeque;
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$500(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/ArrayDeque;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->enable()V

    .line 916
    :cond_2
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 901
    .end local v0    # "keepInQueue":Z
    .end local v1    # "listener":Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 908
    .restart local v0    # "keepInQueue":Z
    .restart local v1    # "listener":Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;
    :cond_3
    :try_start_4
    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$800()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Invalid case : canvas is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 911
    :catchall_2
    move-exception v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlRootView$IdleRunner;->this$0:Lcom/sec/android/gallery3d/glcore/GlRootView;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlRootView;->mRenderLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->access$600(Lcom/sec/android/gallery3d/glcore/GlRootView;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2
.end method
