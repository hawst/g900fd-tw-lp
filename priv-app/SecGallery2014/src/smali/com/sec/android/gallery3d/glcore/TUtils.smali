.class public Lcom/sec/android/gallery3d/glcore/TUtils;
.super Ljava/lang/Object;
.source "TUtils.java"


# static fields
.field public static final M_PI:F = 3.1415927f


# instance fields
.field mBaseUsed:I

.field mCoordBuffer:Ljava/nio/FloatBuffer;

.field public mGl:Ljavax/microedition/khronos/opengles/GL11;

.field mIsTexture:[Z

.field final mMaxTexture:I

.field final mProjectNear:F

.field mProspectMatix:[F

.field mTextureIds:[I

.field public mUsedIndex:I

.field mVertexBuffer:Ljava/nio/FloatBuffer;

.field mVertexBuffer180:Ljava/nio/FloatBuffer;

.field mVertexBuffer270:Ljava/nio/FloatBuffer;

.field mVertexBuffer90:Ljava/nio/FloatBuffer;

.field public texCoords:[F

.field public vertices:[F

.field public vertices180:[F

.field public vertices270:[F

.field public vertices90:[F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0xc

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mProjectNear:F

    .line 13
    const/16 v0, 0x400

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mMaxTexture:I

    .line 15
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    .line 16
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    .line 17
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    .line 19
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    .line 21
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    .line 22
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices90:[F

    .line 23
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices180:[F

    .line 24
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices270:[F

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->texCoords:[F

    return-void

    .line 21
    nop

    :array_0
    .array-data 4
        -0x41000000    # -0.5f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        -0x41000000    # -0.5f
        0x0
        -0x41000000    # -0.5f
        -0x41000000    # -0.5f
        0x0
    .end array-data

    .line 22
    :array_1
    .array-data 4
        -0x41000000    # -0.5f
        -0x41000000    # -0.5f
        0x0
        -0x41000000    # -0.5f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        -0x41000000    # -0.5f
        0x0
    .end array-data

    .line 23
    :array_2
    .array-data 4
        0x3f000000    # 0.5f
        -0x41000000    # -0.5f
        0x0
        -0x41000000    # -0.5f
        -0x41000000    # -0.5f
        0x0
        -0x41000000    # -0.5f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x0
    .end array-data

    .line 24
    :array_3
    .array-data 4
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        -0x41000000    # -0.5f
        0x0
        -0x41000000    # -0.5f
        -0x41000000    # -0.5f
        0x0
        -0x41000000    # -0.5f
        0x3f000000    # 0.5f
        0x0
    .end array-data

    .line 25
    :array_4
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static getByteBufferFromByteArray([B)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0, "array"    # [B

    .prologue
    .line 45
    array-length v1, p0

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 46
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 47
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 48
    return-object v0
.end method

.method public static getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;
    .locals 3
    .param p0, "array"    # [F

    .prologue
    .line 52
    array-length v2, p0

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 53
    .local v1, "tempBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 54
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 55
    .local v0, "buffer":Ljava/nio/FloatBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 56
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 57
    return-object v0
.end method

.method public static getIntBufferFromIntArray([I)Ljava/nio/IntBuffer;
    .locals 3
    .param p0, "array"    # [I

    .prologue
    .line 61
    array-length v2, p0

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 62
    .local v1, "tempBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 63
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    .line 64
    .local v0, "buffer":Ljava/nio/IntBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 65
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 66
    return-object v0
.end method

.method public static glOrtho([FFFFF)V
    .locals 5
    .param p0, "matrix"    # [F
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "znear"    # F
    .param p4, "zfar"    # F

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 203
    sub-float v0, p4, p3

    .line 204
    .local v0, "depth":F
    const/4 v1, 0x0

    div-float v2, v4, p1

    aput v2, p0, v1

    .line 205
    const/4 v1, 0x1

    aput v3, p0, v1

    .line 206
    const/4 v1, 0x2

    aput v3, p0, v1

    .line 207
    const/4 v1, 0x3

    aput v3, p0, v1

    .line 209
    const/4 v1, 0x4

    aput v3, p0, v1

    .line 210
    const/4 v1, 0x5

    div-float v2, v4, p2

    aput v2, p0, v1

    .line 211
    const/4 v1, 0x6

    aput v3, p0, v1

    .line 212
    const/4 v1, 0x7

    aput v3, p0, v1

    .line 214
    const/16 v1, 0x8

    aput v3, p0, v1

    .line 215
    const/16 v1, 0x9

    aput v3, p0, v1

    .line 216
    const/16 v1, 0xa

    const/high16 v2, -0x40000000    # -2.0f

    div-float/2addr v2, v0

    aput v2, p0, v1

    .line 217
    const/16 v1, 0xb

    aput v3, p0, v1

    .line 219
    const/16 v1, 0xc

    aput v3, p0, v1

    .line 220
    const/16 v1, 0xd

    aput v3, p0, v1

    .line 221
    const/16 v1, 0xe

    add-float v2, p4, p3

    neg-float v2, v2

    div-float/2addr v2, v0

    aput v2, p0, v1

    .line 222
    const/16 v1, 0xf

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, p0, v1

    .line 224
    return-void
.end method

.method public static glhFrustumf2([FFFFFFF)V
    .locals 7
    .param p0, "matrix"    # [F
    .param p1, "left"    # F
    .param p2, "right"    # F
    .param p3, "bottom"    # F
    .param p4, "top"    # F
    .param p5, "znear"    # F
    .param p6, "zfar"    # F

    .prologue
    const/4 v6, 0x0

    .line 178
    const/high16 v4, 0x40000000    # 2.0f

    mul-float v0, v4, p5

    .line 179
    .local v0, "temp":F
    sub-float v1, p2, p1

    .line 180
    .local v1, "temp2":F
    sub-float v2, p4, p3

    .line 181
    .local v2, "temp3":F
    sub-float v3, p6, p5

    .line 182
    .local v3, "temp4":F
    const/4 v4, 0x0

    div-float v5, v0, v1

    aput v5, p0, v4

    .line 183
    const/4 v4, 0x1

    aput v6, p0, v4

    .line 184
    const/4 v4, 0x2

    aput v6, p0, v4

    .line 185
    const/4 v4, 0x3

    aput v6, p0, v4

    .line 186
    const/4 v4, 0x4

    aput v6, p0, v4

    .line 187
    const/4 v4, 0x5

    div-float v5, v0, v2

    aput v5, p0, v4

    .line 188
    const/4 v4, 0x6

    aput v6, p0, v4

    .line 189
    const/4 v4, 0x7

    aput v6, p0, v4

    .line 190
    const/16 v4, 0x8

    add-float v5, p2, p1

    div-float/2addr v5, v1

    aput v5, p0, v4

    .line 191
    const/16 v4, 0x9

    add-float v5, p4, p3

    div-float/2addr v5, v2

    aput v5, p0, v4

    .line 192
    const/16 v4, 0xa

    neg-float v5, p6

    sub-float/2addr v5, p5

    div-float/2addr v5, v3

    aput v5, p0, v4

    .line 193
    const/16 v4, 0xb

    const/high16 v5, -0x40800000    # -1.0f

    aput v5, p0, v4

    .line 194
    const/16 v4, 0xc

    aput v6, p0, v4

    .line 195
    const/16 v4, 0xd

    aput v6, p0, v4

    .line 196
    const/16 v4, 0xe

    neg-float v5, v0

    mul-float/2addr v5, p6

    div-float/2addr v5, v3

    aput v5, p0, v4

    .line 197
    const/16 v4, 0xf

    aput v6, p0, v4

    .line 198
    return-void
.end method

.method public static glhPerspectivef2([FFFFF)V
    .locals 8
    .param p0, "matrix"    # [F
    .param p1, "fovyInDegrees"    # F
    .param p2, "aspectRatio"    # F
    .param p3, "znear"    # F
    .param p4, "zfar"    # F

    .prologue
    .line 168
    const v0, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, p1

    float-to-double v0, v0

    const-wide v6, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float v4, p3, v0

    .line 171
    .local v4, "ymax":F
    mul-float v2, v4, p2

    .line 172
    .local v2, "xmax":F
    neg-float v1, v2

    neg-float v3, v4

    move-object v0, p0

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/glcore/TUtils;->glhFrustumf2([FFFFFFF)V

    .line 173
    return-void
.end method

.method public static nextPowerOf2(I)I
    .locals 3
    .param p0, "n"    # I

    .prologue
    .line 34
    if-lez p0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    if-le p0, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "n : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    add-int/lit8 p0, p0, -0x1

    .line 36
    shr-int/lit8 v0, p0, 0x10

    or-int/2addr p0, v0

    .line 37
    shr-int/lit8 v0, p0, 0x8

    or-int/2addr p0, v0

    .line 38
    shr-int/lit8 v0, p0, 0x4

    or-int/2addr p0, v0

    .line 39
    shr-int/lit8 v0, p0, 0x2

    or-int/2addr p0, v0

    .line 40
    shr-int/lit8 v0, p0, 0x1

    or-int/2addr p0, v0

    .line 41
    add-int/lit8 v0, p0, 0x1

    return v0
.end method


# virtual methods
.method public clearTexture()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    if-eqz v1, :cond_0

    .line 104
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    add-int/lit16 v1, v1, 0x400

    if-ge v0, v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    aput-boolean v2, v1, v0

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    .end local v0    # "i":I
    :cond_0
    iput v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    .line 108
    return-void
.end method

.method public finalizeTexture(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 1
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    if-eq v0, p1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    goto :goto_0
.end method

.method public freeId(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 131
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    if-nez v1, :cond_1

    .line 147
    :cond_0
    return-void

    .line 133
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_2

    .line 134
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_3

    .line 135
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 139
    :cond_2
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    add-int/2addr v2, v3

    if-ne v1, v2, :cond_0

    .line 140
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    add-int/2addr v1, v2

    add-int/lit8 v0, v1, -0x1

    :goto_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    if-lt v0, v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    aget-boolean v1, v1, v0

    if-nez v1, :cond_0

    .line 142
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    .line 140
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 133
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getAvailable()I
    .locals 4

    .prologue
    .line 113
    :try_start_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_1

    .line 114
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    aget-boolean v2, v2, v1

    if-nez v2, :cond_0

    .line 115
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    const/4 v3, 0x1

    aput-boolean v3, v2, v1

    .line 116
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    aget v2, v2, v1

    .line 125
    .end local v1    # "i":I
    :goto_1
    return v2

    .line 113
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :cond_1
    iget v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    .line 120
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    const/4 v3, 0x1

    aput-boolean v3, v2, v1

    .line 121
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    aget v2, v2, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 122
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 125
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public getBase(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 150
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    if-ge p1, v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    aget v0, v0, p1

    .line 153
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getProspectMatrix()[F
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mProspectMatix:[F

    return-object v0
.end method

.method public initTexture(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 5
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    const/4 v4, 0x0

    .line 72
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    array-length v2, v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    invoke-interface {v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteTextures(I[II)V

    .line 76
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    .line 78
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    .line 79
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    add-int/lit16 v1, v1, 0x400

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    .line 80
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    add-int/lit16 v1, v1, 0x400

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    .line 81
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    add-int/lit16 v1, v1, 0x400

    if-ge v0, v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mIsTexture:[Z

    aput-boolean v4, v1, v0

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mBaseUsed:I

    add-int/lit16 v1, v1, 0x400

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mTextureIds:[I

    invoke-interface {p1, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenTextures(I[II)V

    .line 84
    iput v4, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mUsedIndex:I

    .line 85
    return-void
.end method

.method public setObjectVertex()V
    .locals 8

    .prologue
    const/16 v7, 0x2601

    const/16 v6, 0xde1

    const/4 v5, 0x2

    const/16 v4, 0x1406

    const/4 v3, 0x0

    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices:[F

    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer:Ljava/nio/FloatBuffer;

    .line 228
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices90:[F

    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer90:Ljava/nio/FloatBuffer;

    .line 229
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices180:[F

    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer180:Ljava/nio/FloatBuffer;

    .line 230
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->vertices270:[F

    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer270:Ljava/nio/FloatBuffer;

    .line 231
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->texCoords:[F

    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/TUtils;->getFloatBufferFromFloatArray([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mCoordBuffer:Ljava/nio/FloatBuffer;

    .line 233
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {v0, v1, v4, v3, v2}, Ljavax/microedition/khronos/opengles/GL11;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mCoordBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {v0, v5, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const v1, 0x84c1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const v1, 0x84c1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glClientActiveTexture(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const v1, 0x8078

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glEnableClientState(I)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mCoordBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {v0, v5, v4, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x2800

    invoke-interface {v0, v6, v1, v7}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameteri(III)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v1, 0x2801

    invoke-interface {v0, v6, v1, v7}, Ljavax/microedition/khronos/opengles/GL11;->glTexParameteri(III)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const v1, 0x84c0

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glClientActiveTexture(I)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const v1, 0x84c0

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11;->glActiveTexture(I)V

    .line 247
    return-void
.end method

.method public setProspectMatrix([F)V
    .locals 0
    .param p1, "matrix"    # [F

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/TUtils;->mProspectMatix:[F

    .line 158
    return-void
.end method
