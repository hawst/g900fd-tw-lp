.class public Lcom/sec/android/gallery3d/data/GroupClustering$Value;
.super Ljava/lang/Object;
.source "GroupClustering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/GroupClustering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Value"
.end annotation


# instance fields
.field public mGroupId:I

.field mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field mName:Ljava/lang/String;

.field mNameId:I

.field mRecord:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/GroupClustering;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/GroupClustering;ILjava/lang/String;Ljava/util/ArrayList;I)V
    .locals 1
    .param p2, "groupId"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p5, "nameId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->this$0:Lcom/sec/android/gallery3d/data/GroupClustering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mRecord:Ljava/util/HashMap;

    .line 213
    iput p2, p0, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mGroupId:I

    .line 214
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mName:Ljava/lang/String;

    .line 215
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mList:Ljava/util/ArrayList;

    .line 216
    iput p5, p0, Lcom/sec/android/gallery3d/data/GroupClustering$Value;->mNameId:I

    .line 217
    return-void
.end method
