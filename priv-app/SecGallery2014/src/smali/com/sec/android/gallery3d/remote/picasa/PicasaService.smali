.class public final Lcom/sec/android/gallery3d/remote/picasa/PicasaService;
.super Landroid/app/Service;
.source "PicasaService.java"


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field public static final ACTION_PERIODIC_SYNC:Ljava/lang/String; = "com.sec.android.gallery3d.picasa.action.PERIODIC_SYNC"

.field public static final ACTION_SYNC:Ljava/lang/String; = "com.sec.android.gallery3d.picasa.action.SYNC"

.field public static final FEATURE_SERVICE_NAME:Ljava/lang/String; = "service_lh2"

.field public static final KEY_ID:Ljava/lang/String; = "com.sec.android.gallery3d.SYNC_ID"

.field public static final KEY_TYPE:Ljava/lang/String; = "com.sec.android.gallery3d.SYNC_TYPE"

.field public static final SERVICE_NAME:Ljava/lang/String; = "lh2"

.field private static final TAG:Ljava/lang/String; = "PicasaService"

.field public static final TYPE_ALBUM_PHOTOS:I = 0x2

.field public static final TYPE_USERS:I = 0x0

.field public static final TYPE_USERS_ALBUMS:I = 0x1

.field private static final sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private mSyncHandler:Landroid/os/Handler;

.field private mSyncThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 53
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PicasaSyncThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncThread:Landroid/os/HandlerThread;

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 77
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncHandler:Landroid/os/Handler;

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaService$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService$1;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 83
    return-void
.end method

.method public static getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 86
    .local v1, "cr":Landroid/content/ContentResolver;
    const/4 v0, 0x0

    .line 88
    .local v0, "client":Landroid/content/ContentProviderClient;
    :try_start_0
    const-string v3, "com.sec.android.gallery3d.picasa.contentprovider"

    invoke-virtual {v1, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_0
    :goto_0
    return-object v3

    .line 90
    :catch_0
    move-exception v2

    .line 91
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    const/4 v3, 0x0

    .line 94
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_0

    .line 94
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_1
    throw v3
.end method

.method private static getIsSyncable(Landroid/content/Context;Landroid/accounts/Account;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 222
    :try_start_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v9, "com.google"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "service_lh2"

    aput-object v12, v10, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v8

    invoke-interface {v8}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/accounts/Account;

    .line 224
    .local v5, "picasaAccounts":[Landroid/accounts/Account;
    move-object v0, v5

    .local v0, "arr$":[Landroid/accounts/Account;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 225
    .local v4, "picasaAccount":Landroid/accounts/Account;
    invoke-virtual {p1, v4}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_0

    .line 229
    .end local v4    # "picasaAccount":Landroid/accounts/Account;
    :goto_1
    return v6

    .line 224
    .restart local v4    # "picasaAccount":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v4    # "picasaAccount":Landroid/accounts/Account;
    :cond_1
    move v6, v7

    .line 229
    goto :goto_1

    .line 230
    .end local v0    # "arr$":[Landroid/accounts/Account;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "picasaAccounts":[Landroid/accounts/Account;
    :catch_0
    move-exception v1

    .line 231
    .local v1, "e":Landroid/accounts/AuthenticatorException;
    new-instance v6, Ljava/io/IOException;

    invoke-virtual {v1}, Landroid/accounts/AuthenticatorException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public static performSync(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 124
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 140
    :goto_0
    return v0

    .line 130
    :cond_0
    const-string v2, "PicasaService"

    const-string/jumbo v3, "start picasa sync"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->performSyncImpl(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V

    .line 134
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 135
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v2

    .line 136
    :try_start_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 137
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    const-string v0, "PicasaService"

    const-string v2, "end picasa sync"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 140
    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static performSyncImpl(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 159
    const-string v0, "com.sec.android.gallery3d.picasa.contentprovider"

    .line 160
    .local v0, "authority":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 161
    const-string v6, "PicasaService"

    const-string v7, "extras is null !!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    const-string v8, "initialize"

    invoke-virtual {p2, v8, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 166
    if-eqz p1, :cond_0

    invoke-static {p1, v0}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v8

    if-gez v8, :cond_0

    .line 168
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->getIsSyncable(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v8

    if-eqz v8, :cond_2

    :goto_1
    invoke-static {p1, v0, v6}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v1}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .end local v1    # "e":Landroid/accounts/OperationCanceledException;
    :cond_2
    move v6, v7

    .line 168
    goto :goto_1

    .line 171
    :catch_1
    move-exception v1

    .line 172
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 180
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    if-eqz p1, :cond_4

    invoke-static {p1, v0}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v8

    if-gez v8, :cond_4

    .line 181
    iget-object v6, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v6, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v6, Landroid/content/SyncStats;->numSkippedEntries:J

    goto :goto_0

    .line 187
    :cond_4
    const-string v8, "com.sec.android.gallery3d.SYNC_TYPE"

    invoke-virtual {p2, v8, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 188
    .local v5, "type":I
    const-string v8, "com.sec.android.gallery3d.SYNC_ID"

    const-wide/16 v10, -0x1

    invoke-virtual {p2, v8, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 192
    .local v2, "id":J
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->getContentProvider(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;

    move-result-object v4

    .line 194
    .local v4, "provider":Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;
    if-nez v4, :cond_5

    .line 195
    const-string v6, "PicasaService"

    const-string v7, "PicasaContentProvider is null !!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 198
    :cond_5
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->reloadAccounts()V

    .line 201
    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->setActiveSyncAccount(Landroid/accounts/Account;)V

    .line 203
    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->setStopCurrentSyncing(Z)V

    .line 205
    packed-switch v5, :pswitch_data_0

    .line 216
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v6

    .line 207
    :pswitch_0
    invoke-virtual {v4, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncUsers(Landroid/content/SyncResult;)V

    goto :goto_0

    .line 210
    :pswitch_1
    invoke-virtual {v4, v6, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncUsersAndAlbums(ZLandroid/content/SyncResult;)V

    goto :goto_0

    .line 213
    :pswitch_2
    invoke-virtual {v4, v2, v3, v6, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->syncAlbumPhotos(JZLandroid/content/SyncResult;)V

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static requestSync(Landroid/content/Context;IJ)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I
    .param p2, "id"    # J

    .prologue
    .line 60
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 72
    :cond_0
    return-void

    .line 64
    :cond_1
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v3, "extras":Landroid/os/Bundle;
    const-string v6, "com.sec.android.gallery3d.SYNC_TYPE"

    invoke-virtual {v3, v6, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    const-string v6, "com.sec.android.gallery3d.SYNC_ID"

    invoke-virtual {v3, v6, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 68
    invoke-static {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    .line 69
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v2, v4

    .line 70
    .local v0, "account":Landroid/accounts/Account;
    const-string v6, "com.sec.android.gallery3d.picasa.contentprovider"

    invoke-static {v0, v6, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 69
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static waitForPerformSync()V
    .locals 3

    .prologue
    .line 144
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v2

    .line 145
    :goto_0
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    :try_start_1
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->sSyncPending:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/InterruptedException;
    :cond_0
    :try_start_2
    monitor-exit v2

    .line 155
    return-void

    .line 154
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 112
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 118
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncThread:Landroid/os/HandlerThread;

    .line 119
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncHandler:Landroid/os/Handler;

    .line 120
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->mSyncHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaService$2;

    invoke-direct {v1, p0, p1, p3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService$2;-><init>(Lcom/sec/android/gallery3d/remote/picasa/PicasaService;Landroid/content/Intent;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 107
    const/4 v0, 0x2

    return v0
.end method
