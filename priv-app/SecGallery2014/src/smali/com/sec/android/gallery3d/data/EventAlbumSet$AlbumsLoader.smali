.class Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;
.super Ljava/lang/Object;
.source "EventAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/EventAlbumSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumsLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/EventAlbumSet;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/data/EventAlbumSet;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/data/EventAlbumSet;Lcom/sec/android/gallery3d/data/EventAlbumSet$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/data/EventAlbumSet;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/data/EventAlbumSet$1;

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;-><init>(Lcom/sec/android/gallery3d/data/EventAlbumSet;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/EventAlbumSet$AlbumsLoader;->this$0:Lcom/sec/android/gallery3d/data/EventAlbumSet;

    # getter for: Lcom/sec/android/gallery3d/data/EventAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->access$100(Lcom/sec/android/gallery3d/data/EventAlbumSet;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getEventAlbumMap()Ljava/util/ArrayList;

    move-result-object v1

    # invokes: Lcom/sec/android/gallery3d/data/EventAlbumSet;->loadSubMediaSets(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/EventAlbumSet;->access$200(Lcom/sec/android/gallery3d/data/EventAlbumSet;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
