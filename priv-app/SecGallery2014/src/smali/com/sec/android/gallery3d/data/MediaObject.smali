.class public abstract Lcom/sec/android/gallery3d/data/MediaObject;
.super Ljava/lang/Object;
.source "MediaObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;
    }
.end annotation


# static fields
.field public static final CACHE_FLAG_FULL:I = 0x2

.field public static final CACHE_FLAG_NO:I = 0x0

.field public static final CACHE_FLAG_SCREENNAIL:I = 0x1

.field public static final CACHE_STATUS_CACHED_FULL:I = 0x3

.field public static final CACHE_STATUS_CACHED_SCREENNAIL:I = 0x2

.field public static final CACHE_STATUS_CACHING:I = 0x1

.field public static final CACHE_STATUS_NOT_CACHED:I = 0x0

.field public static final INVALID_DATA_VERSION:J = -0x1L

.field public static final MEDIA_TYPE_ALL:I = 0x6

.field public static final MEDIA_TYPE_ALL_STRING:Ljava/lang/String; = "all"

.field public static final MEDIA_TYPE_IMAGE:I = 0x2

.field public static final MEDIA_TYPE_IMAGE_STRING:Ljava/lang/String; = "image"

.field public static final MEDIA_TYPE_UNKNOWN:I = 0x1

.field public static final MEDIA_TYPE_VIDEO:I = 0x4

.field public static final MEDIA_TYPE_VIDEO_STRING:Ljava/lang/String; = "video"

.field public static final SUPPORT_3D_PANORAMA:J = 0x4000000000L

.field public static final SUPPORT_ACTION:I = 0x4000

.field public static final SUPPORT_ADD_USER_TAG:J = 0x1000000000000000L

.field public static final SUPPORT_ALL:J = -0x1L

.field public static final SUPPORT_ASSIGN_NAME:J = 0x800000000000000L

.field public static final SUPPORT_BACK:I = 0x2000

.field public static final SUPPORT_BEST_VIDEO_SHOT:J = 0x40000000000L

.field public static final SUPPORT_BURST_SHOT_PLAY:J = 0x100000000000L

.field public static final SUPPORT_CACHE:I = 0x100

.field public static final SUPPORT_CAMERA_SHORTCUT:I = 0x8000

.field public static final SUPPORT_COLLAGE:J = 0x2000000000L

.field public static final SUPPORT_CONFIRM:J = 0x400000000000000L

.field public static final SUPPORT_COPY:J = 0x20000000000000L

.field public static final SUPPORT_COPY_TO_ALBUM:J = 0x400000L

.field public static final SUPPORT_COPY_TO_CLIPBOARD:J = 0x2000000000000000L

.field public static final SUPPORT_COPY_TO_EVENT:J = 0x800000L

.field public static final SUPPORT_CREATE_VIDEO_ALBUM:J = 0x10000000L

.field public static final SUPPORT_CROP:I = 0x8

.field public static final SUPPORT_DELETE:I = 0x1

.field public static final SUPPORT_DIRECT_SHARE:J = 0x10000000000L

.field public static final SUPPORT_DOWNLOAD_SLINK:J = 0x1000000000000L

.field public static final SUPPORT_DOWNLOAD_VIA_DOWNLOAD_MANAGER:J = 0x800000000000L

.field public static final SUPPORT_EDIT:I = 0x200

.field public static final SUPPORT_FACE_CLUSTERING:J = 0x80000000000000L

.field public static final SUPPORT_FACE_RECOGNITION_TAG:J = 0x40000000000000L

.field public static final SUPPORT_FLASH_ANNOTATE:I = 0x80000

.field public static final SUPPORT_FULL_IMAGE:I = 0x40

.field public static final SUPPORT_GET_TEXT:J = 0x200000000000L

.field public static final SUPPORT_HIDE_ALBUM:J = 0x4000000000000L

.field public static final SUPPORT_IMAGENOTE:J = 0x400000000L

.field public static final SUPPORT_IMPORT:J = 0x20000000000L

.field public static final SUPPORT_INFO:I = 0x400

.field public static final SUPPORT_LAST_SHARE:J = 0x80000000000L

.field public static final SUPPORT_LAST_SHARE_SHOW:J = 0x8000000L

.field public static final SUPPORT_MANUAL_DETECT:J = 0x4000000L

.field public static final SUPPORT_MMS_SAVE:J = 0x8000000000L

.field public static final SUPPORT_MOBILEPRINT:J = -0x8000000000000000L

.field public static final SUPPORT_MOTION_PICTURE:J = 0x200000000L

.field public static final SUPPORT_MOVE:J = 0x10000000000000L

.field public static final SUPPORT_MOVE_TO_ALBUM:J = 0x100000L

.field public static final SUPPORT_MOVE_TO_KNOX:J = 0x2000000L

.field public static final SUPPORT_MOVE_TO_SECRETBOX:J = 0x40000000L

.field public static final SUPPORT_MUTE:I = 0x10000

.field public static final SUPPORT_NEARBY_PLAY:J = 0x80000000L

.field public static final SUPPORT_NEW_ALBUM:J = 0x8000000000000L

.field public static final SUPPORT_PHOTO_SIGNATURE:J = 0x100000000L

.field public static final SUPPORT_PLAY:I = 0x80

.field public static final SUPPORT_PRINT:I = 0x20000

.field public static final SUPPORT_REMOVE_FROM_KNOX:J = 0x1000000L

.field public static final SUPPORT_REMOVE_FROM_SECRETBOX:J = 0x20000000L

.field public static final SUPPORT_REMOVE_TAG:J = 0x200000000000000L

.field public static final SUPPORT_RENAME:J = 0x4000000000000000L

.field public static final SUPPORT_ROTATE:I = 0x2

.field public static final SUPPORT_SEND_TO_OTHER_DEVICES:I = 0x40000

.field public static final SUPPORT_SETAS:I = 0x20

.field public static final SUPPORT_SHARE:I = 0x4

.field public static final SUPPORT_SHOW_ON_MAP:I = 0x10

.field public static final SUPPORT_SLIDESHOW:J = 0x400000000000L

.field public static final SUPPORT_STORYALBUM:J = 0x800000000L

.field public static final SUPPORT_S_STUDIO:J = 0x200000L

.field public static final SUPPORT_TRIM:I = 0x800

.field public static final SUPPORT_UNLOCK:I = 0x1000

.field public static final SUPPORT_UPLOAD:J = 0x2000000000000L

.field public static final SUPPORT_VIDEO_TRIM:J = 0x1000000000L

.field private static final TAG:Ljava/lang/String;

.field public static final UNSUPPORT_ALL:J

.field private static sArcVersionSerial:J

.field private static sVersionSerial:J


# instance fields
.field protected mArcDataVersion:J

.field protected mDataVersion:J

.field protected final mPath:Lcom/sec/android/gallery3d/data/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 22
    const-class v0, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/MediaObject;->TAG:Ljava/lang/String;

    .line 122
    sput-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sVersionSerial:J

    .line 124
    sput-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sArcVersionSerial:J

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;J)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "version"    # J

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaObject;->mArcDataVersion:J

    .line 139
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/data/Path;->setObject(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 140
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MediaObject;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 141
    iput-wide p2, p0, Lcom/sec/android/gallery3d/data/MediaObject;->mDataVersion:J

    .line 142
    return-void
.end method

.method public static declared-synchronized clearArcVersionNumber()V
    .locals 4

    .prologue
    .line 271
    const-class v0, Lcom/sec/android/gallery3d/data/MediaObject;

    monitor-enter v0

    const-wide/16 v2, 0x0

    :try_start_0
    sput-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sArcVersionSerial:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    monitor-exit v0

    return-void

    .line 271
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized getArcVersionNumber()J
    .locals 4

    .prologue
    .line 263
    const-class v0, Lcom/sec/android/gallery3d/data/MediaObject;

    monitor-enter v0

    :try_start_0
    sget-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sArcVersionSerial:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-wide v2

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getTypeFromString(Ljava/lang/String;)I
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 212
    const-string v0, "all"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    .line 214
    :goto_0
    return v0

    .line 213
    :cond_0
    const-string v0, "image"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 214
    :cond_1
    const-string/jumbo v0, "video"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    .line 215
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getTypeString(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 219
    packed-switch p0, :pswitch_data_0

    .line 224
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 220
    :pswitch_1
    const-string v0, "image"

    .line 222
    :goto_0
    return-object v0

    .line 221
    :pswitch_2
    const-string/jumbo v0, "video"

    goto :goto_0

    .line 222
    :pswitch_3
    const-string v0, "all"

    goto :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static declared-synchronized nextVersionNumber()J
    .locals 6

    .prologue
    .line 208
    const-class v1, Lcom/sec/android/gallery3d/data/MediaObject;

    monitor-enter v1

    :try_start_0
    sget-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sVersionSerial:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    sput-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sVersionSerial:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized setArcVersionNumber()V
    .locals 6

    .prologue
    .line 267
    const-class v1, Lcom/sec/android/gallery3d/data/MediaObject;

    monitor-enter v1

    :try_start_0
    sget-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sArcVersionSerial:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    sput-wide v2, Lcom/sec/android/gallery3d/data/MediaObject;->sArcVersionSerial:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    monitor-exit v1

    return-void

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized setVersion()V
    .locals 2

    .prologue
    .line 276
    const-class v0, Lcom/sec/android/gallery3d/data/MediaObject;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->setArcVersionNumber()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    monitor-exit v0

    return-void

    .line 276
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public cache(I)V
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 204
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public clearCachedPanoramaSupport()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public copy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 245
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public delete()V
    .locals 1

    .prologue
    .line 160
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public delete(Z)V
    .locals 1
    .param p1, "includeDir"    # Z

    .prologue
    .line 241
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getCacheFlag()I
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method public getCacheSize()J
    .locals 1

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getCacheStatus()I
    .locals 1

    .prologue
    .line 196
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 168
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "className":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/gallery3d/data/MediaObject;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "should implement getContentUri."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    sget-object v1, Lcom/sec/android/gallery3d/data/MediaObject;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The object was created from path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1
.end method

.method public getDataVersion()J
    .locals 2

    .prologue
    .line 188
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaObject;->mDataVersion:J

    return-wide v0
.end method

.method public getDataVersion(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/sec/android/gallery3d/data/MediaObject;->mDataVersion:J

    return-wide v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 1

    .prologue
    .line 183
    new-instance v0, Lcom/sec/android/gallery3d/data/MediaDetails;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/MediaDetails;-><init>()V

    .line 184
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    return-object v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x1

    return v0
.end method

.method public getPanoramaSupport(Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;

    .prologue
    const/4 v0, 0x0

    .line 153
    invoke-interface {p1, p0, v0, v0}, Lcom/sec/android/gallery3d/data/MediaObject$PanoramaSupportCallback;->panoramaInfoAvailable(Lcom/sec/android/gallery3d/data/MediaObject;ZZ)V

    .line 154
    return-void
.end method

.method public getPath()Lcom/sec/android/gallery3d/data/Path;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/MediaObject;->mPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method public getPlayUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 175
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 253
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getSupportedOperations()J
    .locals 2

    .prologue
    .line 149
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public hide(Ljava/lang/String;)Z
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 229
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public move(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 249
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public moveToSecretbox(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    .line 233
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public rotate(I)V
    .locals 1
    .param p1, "degrees"    # I

    .prologue
    .line 164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public show()Z
    .locals 1

    .prologue
    .line 237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
