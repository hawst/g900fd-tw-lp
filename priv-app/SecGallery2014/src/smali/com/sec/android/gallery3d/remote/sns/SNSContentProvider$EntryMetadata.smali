.class final Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;
.super Ljava/lang/Object;
.source "SNSContentProvider.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EntryMetadata"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field public cachePath:Ljava/lang/String;

.field public cacheStatus:I

.field public dateEdited:J

.field public displayIndex:I

.field public id:Ljava/lang/String;

.field public survived:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 721
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->survived:Z

    .line 724
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JILjava/lang/String;I)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "dateEdited"    # J
    .param p4, "displayIndex"    # I
    .param p5, "cachePath"    # Ljava/lang/String;
    .param p6, "cacheStatus"    # I

    .prologue
    .line 727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 721
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->survived:Z

    .line 728
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->id:Ljava/lang/String;

    .line 729
    iput-wide p2, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->dateEdited:J

    .line 730
    iput p4, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->displayIndex:I

    .line 731
    iput-object p5, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->cachePath:Ljava/lang/String;

    .line 732
    iput p6, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->cacheStatus:I

    .line 733
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;)I
    .locals 2
    .param p1, "other"    # Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    .prologue
    .line 736
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 715
    check-cast p1, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;->compareTo(Lcom/sec/android/gallery3d/remote/sns/SNSContentProvider$EntryMetadata;)I

    move-result v0

    return v0
.end method
