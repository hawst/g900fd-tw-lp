.class public Lcom/sec/android/gallery3d/glcore/GlRect;
.super Ljava/lang/Object;
.source "GlRect.java"


# instance fields
.field public mHeight:I

.field public mLeft:I

.field public mTop:I

.field public mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public Set(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 11
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    .line 12
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    .line 13
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    .line 14
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    .line 15
    return-void
.end method

.method public SetPos(II)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    .line 23
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    .line 24
    return-void
.end method

.method public SetSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    .line 28
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    .line 29
    return-void
.end method

.method public getAndroidRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 18
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    iget v2, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    iget v3, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    iget v5, p0, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method
