.class public Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;
.super Ljava/lang/Object;
.source "WifiDisplayAlertDialog.java"


# static fields
.field private static final HEADER_TEXT_COLOR:I

.field private static final MSG_SHOW_CHANGEPLAYER_DIALOG:I = 0x0

.field private static final PRODUCT_NAME_METROPCS:Ljava/lang/String; = "metropcs"

.field private static final PRODUCT_NAME_TMO:Ljava/lang/String; = "tmo"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mView:Landroid/view/View;

.field private mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    const v0, -0xcab9b1

    :goto_0
    sput v0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->HEADER_TEXT_COLOR:I

    return-void

    :cond_0
    const v0, -0xa0a0b

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialog"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
    .param p3, "wifiDisplayUtils"    # Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0e00db

    const/16 v4, 0x8

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v2, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mHandler:Landroid/os/Handler;

    .line 58
    if-nez p2, :cond_0

    .line 59
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "dialog must not be null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 62
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .line 64
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    .line 66
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030028

    invoke-virtual {v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mView:Landroid/view/View;

    .line 67
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mView:Landroid/view/View;

    const v3, 0x7f0f006a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 68
    .local v1, "textView":Landroid/widget/TextView;
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->getErrorStringId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 69
    sget v2, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->HEADER_TEXT_COLOR:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 70
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mView:Landroid/view/View;

    const v3, 0x7f0f001d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 71
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 72
    sget v2, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->HEADER_TEXT_COLOR:I

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 74
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getExceptionalCase()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 75
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 76
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0120

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$2;-><init>(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0046

    invoke-virtual {v2, v3, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 103
    :goto_0
    return-void

    .line 87
    :cond_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0132

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog$3;-><init>(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mAlertDialog:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mView:Landroid/view/View;

    return-object v0
.end method

.method private getErrorStringId()I
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getExceptionalCase()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 140
    :pswitch_0
    const v1, 0x7f0e013a

    :goto_0
    return v1

    .line 116
    :pswitch_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v1, :cond_0

    .line 117
    const v1, 0x7f0e014c

    goto :goto_0

    .line 119
    :cond_0
    const v1, 0x7f0e0147

    goto :goto_0

    .line 121
    :pswitch_2
    sget-object v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mProductName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "productNameLowered":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v1, :cond_1

    .line 123
    const v1, 0x7f0e014b

    goto :goto_0

    .line 124
    :cond_1
    const-string/jumbo v1, "tmo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 125
    const v1, 0x7f0e0146

    goto :goto_0

    .line 126
    :cond_2
    const-string v1, "metropcs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 127
    const v1, 0x7f0e0145

    goto :goto_0

    .line 129
    :cond_3
    const v1, 0x7f0e0144

    goto :goto_0

    .line 132
    .end local v0    # "productNameLowered":Ljava/lang/String;
    :pswitch_3
    const v1, 0x7f0e0149

    goto :goto_0

    .line 134
    :pswitch_4
    const v1, 0x7f0e0148

    goto :goto_0

    .line 136
    :pswitch_5
    const v1, 0x7f0e014a

    goto :goto_0

    .line 138
    :pswitch_6
    const v1, 0x7f0e0122

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    .line 111
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 107
    return-void
.end method
