.class public Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;
.super Ljava/lang/Object;
.source "AlbumSetSlidingWindow.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;,
        Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumCoverLoader;,
        Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$EntryUpdater;,
        Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;,
        Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;
    }
.end annotation


# static fields
.field private static final MSG_UPDATE_ALBUM_ENTRY:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AlbumSetSlidingWindow"


# instance fields
.field private mActiveEnd:I

.field private mActiveRequestCount:I

.field private mActiveStart:I

.field private mContentEnd:I

.field private mContentStart:I

.field private final mContentUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

.field private final mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

.field private final mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field private mIsActive:Z

.field private final mLabelMaker:Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;

.field private final mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

.field private mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

.field private mLoadingLabel:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

.field private final mLoadingText:Ljava/lang/String;

.field private mSize:I

.field private mSlotWidth:I

.field private final mSource:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

.field private final mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;I)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "source"    # Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;
    .param p3, "labelSpec"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;
    .param p4, "cacheSize"    # I

    .prologue
    const/4 v0, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    .line 53
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    .line 55
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    .line 56
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    .line 69
    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    .line 70
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    .line 100
    invoke-virtual {p2, p0}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->setModelListener(Lcom/sec/android/gallery3d/app/AlbumSetDataLoader$DataListener;)V

    .line 101
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    .line 102
    new-array v0, p4, [Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    .line 103
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    .line 104
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 106
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/AlbumSetSlotRenderer$LabelSpec;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelMaker:Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;

    .line 107
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0032

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLoadingText:Ljava/lang/String;

    .line 108
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    .line 109
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    .line 111
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$1;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 119
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .line 121
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelMaker:Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TextureUploader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/util/ThreadPool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    return v0
.end method

.method static synthetic access$706(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->requestNonactiveImages()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;)Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    return-object v0
.end method

.method private cancelImagesInSlot(I)V
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    .line 225
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    if-lt p1, v1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 227
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    if-eqz v1, :cond_2

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->cancelLoad()V

    .line 228
    :cond_2
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->cancelLoad()V

    goto :goto_0
.end method

.method private cancelNonactiveImages()V
    .locals 5

    .prologue
    .line 209
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 211
    .local v1, "range":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 212
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    add-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->cancelImagesInSlot(I)V

    .line 213
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->cancelImagesInSlot(I)V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    :cond_0
    return-void
.end method

.method private freeSlotContent(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    .line 238
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 239
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->recycle()V

    .line 240
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    if-eqz v1, :cond_1

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->recycle()V

    .line 241
    :cond_1
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->recycle()V

    .line 242
    :cond_2
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->recycle()V

    .line 243
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 244
    return-void
.end method

.method private static getDataVersion(Lcom/sec/android/gallery3d/data/MediaObject;)J
    .locals 2
    .param p0, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 232
    if-nez p0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaObject;->getDataVersion()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static identifyCacheFlag(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 4
    .param p0, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 467
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x100

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 469
    :cond_0
    const/4 v0, 0x0

    .line 472
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getCacheFlag()I

    move-result v0

    goto :goto_0
.end method

.method private static identifyCacheStatus(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 4
    .param p0, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 476
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x100

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 478
    :cond_0
    const/4 v0, 0x0

    .line 481
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaSet;->getCacheStatus()I

    move-result v0

    goto :goto_0
.end method

.method private isLabelChanged(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Ljava/lang/String;II)Z
    .locals 1
    .param p1, "entry"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "totalCount"    # I
    .param p4, "sourceType"    # I

    .prologue
    .line 248
    iget-object v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->title:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->totalCount:I

    if-ne v0, p3, :cond_0

    iget v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->sourceType:I

    if-eq v0, p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private prepareSlotContent(I)V
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    .line 298
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;-><init>()V

    .line 299
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateAlbumSetEntry(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;I)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aput-object v0, v1, v2

    .line 301
    return-void
.end method

.method private requestImagesInSlot(I)V
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    .line 218
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    if-lt p1, v1, :cond_1

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 220
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    if-eqz v1, :cond_2

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->startLoad()V

    .line 221
    :cond_2
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->startLoad()V

    goto :goto_0
.end method

.method private requestNonactiveImages()V
    .locals 5

    .prologue
    .line 200
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 202
    .local v1, "range":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 203
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    add-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->requestImagesInSlot(I)V

    .line 204
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->requestImagesInSlot(I)V

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    return-void
.end method

.method private setContentWindow(II)V
    .locals 3
    .param p1, "contentStart"    # I
    .param p2, "contentEnd"    # I

    .prologue
    .line 144
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    if-ne p1, v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    if-ne p2, v2, :cond_0

    .line 172
    :goto_0
    return-void

    .line 146
    :cond_0
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    if-ge p1, v2, :cond_1

    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    if-lt v2, p2, :cond_3

    .line 147
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 148
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->freeSlotContent(I)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 150
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->setActiveWindow(II)V

    .line 151
    move v0, p1

    :goto_2
    if-ge v0, p2, :cond_7

    .line 152
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->prepareSlotContent(I)V

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 155
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    .restart local v0    # "i":I
    :goto_3
    if-ge v0, p1, :cond_4

    .line 156
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->freeSlotContent(I)V

    .line 155
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 158
    :cond_4
    move v0, p2

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    .restart local v1    # "n":I
    :goto_4
    if-ge v0, v1, :cond_5

    .line 159
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->freeSlotContent(I)V

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 161
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->setActiveWindow(II)V

    .line 162
    move v0, p1

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    :goto_5
    if-ge v0, v1, :cond_6

    .line 163
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->prepareSlotContent(I)V

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 165
    :cond_6
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    :goto_6
    if-ge v0, p2, :cond_7

    .line 166
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->prepareSlotContent(I)V

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 170
    :cond_7
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    .line 171
    iput p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    goto :goto_0
.end method

.method private static startLoadBitmap(Lcom/sec/android/gallery3d/ui/BitmapLoader;)Z
    .locals 1
    .param p0, "loader"    # Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .prologue
    .line 304
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 306
    :goto_0
    return v0

    .line 305
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->startLoad()V

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->isRequestInProgress()Z

    move-result v0

    goto :goto_0
.end method

.method private updateAlbumSetEntry(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;I)V
    .locals 12
    .param p1, "entry"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    .param p2, "slotIndex"    # I

    .prologue
    const/4 v8, 0x0

    .line 254
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->getMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 255
    .local v6, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->getCoverItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v7

    .line 256
    .local v7, "cover":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSource:Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;

    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/app/AlbumSetDataLoader;->getTotalCount(I)I

    move-result v4

    .line 258
    .local v4, "totalCount":I
    iput-object v6, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->album:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 259
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->getDataVersion(Lcom/sec/android/gallery3d/data/MediaObject;)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->setDataVersion:J

    .line 260
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->identifyCacheFlag(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    iput v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->cacheFlag:I

    .line 261
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->identifyCacheStatus(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    iput v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->cacheStatus:I

    .line 262
    if-nez v6, :cond_4

    move-object v0, v8

    :goto_0
    iput-object v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->setPath:Lcom/sec/android/gallery3d/data/Path;

    .line 264
    if-nez v6, :cond_5

    const-string v3, ""

    .line 265
    .local v3, "title":Ljava/lang/String;
    :goto_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/data/DataSourceType;->identifySourceType(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v5

    .line 266
    .local v5, "sourceType":I
    invoke-direct {p0, p1, v3, v4, v5}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->isLabelChanged(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Ljava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    iput-object v3, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->title:Ljava/lang/String;

    .line 268
    iput v4, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->totalCount:I

    .line 269
    iput v5, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->sourceType:I

    .line 270
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 271
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->recycle()V

    .line 272
    # setter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1, v8}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$102(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .line 273
    iput-object v8, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 275
    :cond_0
    if-eqz v6, :cond_1

    .line 276
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;

    move-object v1, p0

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;ILjava/lang/String;II)V

    # setter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$102(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .line 281
    :cond_1
    iput-object v7, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 282
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->getDataVersion(Lcom/sec/android/gallery3d/data/MediaObject;)J

    move-result-wide v0

    iget-wide v10, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverDataVersion:J

    cmp-long v0, v0, v10

    if-eqz v0, :cond_3

    .line 283
    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->getDataVersion(Lcom/sec/android/gallery3d/data/MediaObject;)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverDataVersion:J

    .line 284
    if-nez v7, :cond_6

    const/4 v0, 0x0

    :goto_2
    iput v0, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->rotation:I

    .line 285
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 286
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->recycle()V

    .line 287
    # setter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1, v8}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$002(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .line 288
    iput-object v8, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    .line 289
    iput-object v8, p1, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->content:Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 291
    :cond_2
    if-eqz v7, :cond_3

    .line 292
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumCoverLoader;

    invoke-direct {v0, p0, p2, v7}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumCoverLoader;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;ILcom/sec/android/gallery3d/data/MediaItem;)V

    # setter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {p1, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$002(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .line 295
    :cond_3
    return-void

    .line 262
    .end local v3    # "title":Ljava/lang/String;
    .end local v5    # "sourceType":I
    :cond_4
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    goto :goto_0

    .line 264
    :cond_5
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->ensureNotNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 284
    .restart local v3    # "title":Ljava/lang/String;
    .restart local v5    # "sourceType":I
    :cond_6
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    goto :goto_2
.end method

.method private updateAllImageRequests()V
    .locals 5

    .prologue
    .line 346
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    .line 347
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    .local v1, "i":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 348
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v4, v4

    rem-int v4, v1, v4

    aget-object v0, v3, v4

    .line 349
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->coverLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$000(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->startLoadBitmap(Lcom/sec/android/gallery3d/ui/BitmapLoader;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    .line 350
    :cond_0
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->startLoadBitmap(Lcom/sec/android/gallery3d/ui/BitmapLoader;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    .line 347
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 352
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    :cond_2
    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveRequestCount:I

    if-nez v3, :cond_3

    .line 353
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->requestNonactiveImages()V

    .line 357
    :goto_1
    return-void

    .line 355
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->cancelNonactiveImages()V

    goto :goto_1
.end method

.method private updateTextureUploadQueue()V
    .locals 7

    .prologue
    .line 321
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    if-nez v4, :cond_1

    .line 343
    :cond_0
    return-void

    .line 322
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->clear()V

    .line 323
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;->clear()V

    .line 326
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    .local v1, "i":I
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_4

    .line 327
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v5, v5

    rem-int v5, v1, v5

    aget-object v0, v4, v5

    .line 328
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v4, :cond_2

    .line 329
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->addTexture(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    .line 331
    :cond_2
    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    if-eqz v4, :cond_3

    .line 332
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;->addFgTexture(Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;)V

    .line 326
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 337
    .end local v0    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    :cond_4
    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    iget v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    iget v6, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    sub-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 339
    .local v3, "range":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    .line 340
    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    add-int/2addr v4, v1

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->uploadBackgroundTextureInSlot(I)V

    .line 341
    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    sub-int/2addr v4, v1

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->uploadBackgroundTextureInSlot(I)V

    .line 339
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private uploadBackgroundTextureInSlot(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 310
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    if-lt p1, v1, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 312
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    if-eqz v1, :cond_2

    .line 313
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->bitmapTexture:Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->addTexture(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    .line 315
    :cond_2
    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    if-eqz v1, :cond_0

    .line 316
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;->addBgTexture(Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;)V

    goto :goto_0
.end method


# virtual methods
.method public get(I)Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->isActiveSlot(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    const-string v0, "invalid slot: %s outsides (%s, %s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->fail(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v1, v1

    rem-int v1, p1, v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getLoadingTexture()Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 394
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLoadingLabel:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    if-nez v1, :cond_0

    .line 395
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelMaker:Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLoadingText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;->requestLabel(Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 398
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLoadingLabel:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 399
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLoadingLabel:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->setOpaque(Z)V

    .line 401
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLoadingLabel:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    return-object v1
.end method

.method public isActiveSlot(I)Z
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 140
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContentChanged(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 371
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    if-nez v1, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 377
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    if-lt p1, v1, :cond_2

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    if-lt p1, v1, :cond_3

    .line 378
    :cond_2
    const-string v1, "AlbumSetSlidingWindow"

    const-string v2, "invalid update: %s is outside (%s, %s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 384
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 385
    .local v0, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateAlbumSetEntry(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;I)V

    .line 386
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateAllImageRequests()V

    .line 387
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateTextureUploadQueue()V

    .line 388
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->isActiveSlot(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 389
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;->onContentChanged()V

    goto :goto_0
.end method

.method public onSizeChanged(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    if-eq v0, p1, :cond_2

    .line 362
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    .line 363
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;->onSizeChanged(I)V

    .line 364
    :cond_0
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    .line 365
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    iput v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    .line 367
    :cond_2
    return-void
.end method

.method public onSlotSizeChanged(II)V
    .locals 9
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v8, 0x0

    .line 531
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSlotWidth:I

    if-ne v0, p1, :cond_1

    .line 553
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSlotWidth:I

    .line 534
    iput-object v8, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLoadingLabel:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 535
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelMaker:Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;

    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSlotWidth:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/AlbumLabelMaker;->setLabelWidth(I)V

    .line 537
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    if-eqz v0, :cond_0

    .line 539
    iget v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    .local v2, "i":I
    iget v7, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    .local v7, "n":I
    :goto_1
    if-ge v2, v7, :cond_4

    .line 540
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v1, v1

    rem-int v1, v2, v1

    aget-object v6, v0, v1

    .line 541
    .local v6, "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 542
    # getter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$100(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/BitmapLoader;->recycle()V

    .line 543
    # setter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$102(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .line 544
    iput-object v8, v6, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 546
    :cond_2
    iget-object v0, v6, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->album:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_3

    .line 547
    new-instance v0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;

    iget-object v3, v6, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->title:Ljava/lang/String;

    iget v4, v6, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->totalCount:I

    iget v5, v6, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->sourceType:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumLabelLoader;-><init>(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;ILjava/lang/String;II)V

    # setter for: Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->labelLoader:Lcom/sec/android/gallery3d/ui/BitmapLoader;
    invoke-static {v6, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;->access$102(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;Lcom/sec/android/gallery3d/ui/BitmapLoader;)Lcom/sec/android/gallery3d/ui/BitmapLoader;

    .line 539
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 551
    .end local v6    # "entry":Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateAllImageRequests()V

    .line 552
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateTextureUploadQueue()V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 405
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    .line 406
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mLabelUploader:Lcom/sec/android/gallery3d/glrenderer/TextureUploader;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glrenderer/TextureUploader;->clear()V

    .line 407
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->clear()V

    .line 408
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 409
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->freeSlotContent(I)V

    .line 408
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 411
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 414
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    .line 415
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mContentEnd:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 416
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->prepareSlotContent(I)V

    .line 415
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 418
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateAllImageRequests()V

    .line 419
    return-void
.end method

.method public setActiveWindow(II)V
    .locals 8
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v7, 0x0

    .line 175
    if-gt p1, p2, :cond_0

    sub-int v3, p2, p1

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v4, v4

    if-gt v3, v4, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    if-le p2, v3, :cond_1

    .line 176
    :cond_0
    const-string/jumbo v3, "start = %s, end = %s, length = %s, size = %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    array-length v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->fail(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mData:[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;

    .line 181
    .local v2, "data":[Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$AlbumSetEntry;
    iput p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveStart:I

    .line 182
    iput p2, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mActiveEnd:I

    .line 183
    add-int v3, p1, p2

    div-int/lit8 v3, v3, 0x2

    array-length v4, v2

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    array-length v5, v2

    sub-int/2addr v4, v5

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v7, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 185
    .local v1, "contentStart":I
    array-length v3, v2

    add-int/2addr v3, v1

    iget v4, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 186
    .local v0, "contentEnd":I
    invoke-direct {p0, v1, v0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->setContentWindow(II)V

    .line 188
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mIsActive:Z

    if-eqz v3, :cond_2

    .line 189
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateTextureUploadQueue()V

    .line 190
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->updateAllImageRequests()V

    .line 192
    :cond_2
    return-void
.end method

.method public setListener(Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mListener:Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow$Listener;

    .line 125
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/gallery3d/ui/AlbumSetSlidingWindow;->mSize:I

    return v0
.end method
