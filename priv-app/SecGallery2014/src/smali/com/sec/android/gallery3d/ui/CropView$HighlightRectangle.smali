.class Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "CropView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/CropView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HighlightRectangle"
.end annotation


# instance fields
.field private mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mArrowBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mEdgeIndicatorBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mHighlightRect:Landroid/graphics/RectF;

.field private mMovingEdges:I

.field private mReferenceX:F

.field private mReferenceY:F

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/CropView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/CropView;)V
    .locals 5

    .prologue
    const v4, 0x7f0203ff

    const v3, 0x7f0203fe

    const/high16 v2, 0x3f400000    # 0.75f

    const/high16 v1, 0x3e800000    # 0.25f

    .line 314
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 306
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    .line 310
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 315
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/CropView;->access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 316
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/CropView;->access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrowBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 317
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/CropView;->access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 318
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/CropView;->access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicatorBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 319
    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p1, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    .line 320
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/ui/CropView;->access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/TTSUtil;->init(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 321
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;)Landroid/graphics/RectF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    .param p1, "x1"    # Landroid/graphics/RectF;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;Landroid/graphics/RectF;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;
    .param p1, "x1"    # Landroid/graphics/RectF;

    .prologue
    .line 305
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setInitRectangle(Landroid/graphics/RectF;)V

    return-void
.end method

.method private drawHighlightRectangle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;)V
    .locals 30
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/RectF;

    .prologue
    .line 601
    const/high16 v3, 0x3f000000    # 0.5f

    .line 602
    .local v3, "offset":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioX:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1600(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioY:F
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1700(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsPhotoFrame:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1800(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 603
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1900(Lcom/sec/android/gallery3d/ui/CropView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v27

    .line 604
    .local v27, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    .line 605
    .local v24, "lcdWidth":I
    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v23, v0

    .line 607
    .local v23, "lcdHeight":I
    const/4 v2, 0x0

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->left:F

    move/from16 v0, v23

    int-to-float v5, v0

    const/high16 v6, 0x59000000

    move-object/from16 v1, p1

    invoke-interface/range {v1 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 608
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    add-float/2addr v2, v1

    move/from16 v0, v24

    int-to-float v1, v0

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v4, v5

    sub-float v4, v1, v4

    move/from16 v0, v23

    int-to-float v5, v0

    const/high16 v6, 0x59000000

    move-object/from16 v1, p1

    invoke-interface/range {v1 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 609
    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v4

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/RectF;->top:F

    const/high16 v6, 0x59000000

    move-object/from16 v1, p1

    invoke-interface/range {v1 .. v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 610
    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    add-float v6, v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v7

    move/from16 v0, v23

    int-to-float v1, v0

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v2, v4

    sub-float v8, v1, v2

    const/high16 v9, 0x59000000

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 612
    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    add-float v6, v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v8

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 613
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsPhotoFrame:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1800(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 614
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioX:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1600(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v2

    mul-float v7, v1, v2

    .line 615
    .local v7, "sx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioY:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1700(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v2

    mul-float v8, v1, v2

    .line 616
    .local v8, "sy":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v19

    .line 617
    .local v19, "cx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v20

    .line 619
    .local v20, "cy":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v5, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v6, v20, v1

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 620
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v5, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v6, v20, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v9

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 621
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float v10, v1, v3

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    add-float v11, v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    sub-float v12, v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float v13, v1, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 623
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v10, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v11, v20, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    move v12, v8

    move v13, v7

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 624
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v10, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v11, v20, v1

    const/4 v14, 0x0

    move-object/from16 v9, p1

    move v12, v8

    move v13, v7

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 636
    .end local v7    # "sx":F
    .end local v8    # "sy":F
    .end local v19    # "cx":F
    .end local v20    # "cy":F
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2200(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 637
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_8

    .line 639
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 640
    .restart local v7    # "sx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v8

    .line 641
    .restart local v8    # "sy":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v19

    .line 642
    .restart local v19    # "cx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v20

    .line 643
    .restart local v20    # "cy":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    add-float v26, v19, v1

    .line 644
    .local v26, "rLeft":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    add-float v1, v1, v19

    cmpg-float v1, v1, v26

    if-gez v1, :cond_4

    move/from16 v10, v26

    .line 645
    .local v10, "rRight":F
    :goto_1
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v1, v19, v1

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    .line 646
    .local v21, "lLeft":F
    :goto_2
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v22, v19, v1

    .line 647
    .local v22, "lRight":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    add-float v1, v1, v20

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v29, v0

    .line 648
    .local v29, "top":F
    :goto_3
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v1, v20, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_7

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v17, v0

    .line 650
    .local v17, "bottom":F
    :goto_4
    const/high16 v1, 0x3f800000    # 1.0f

    add-float v11, v17, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v13, v29, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    move v12, v10

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 651
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    move/from16 v11, v17

    move/from16 v12, v26

    move/from16 v13, v17

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 652
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    move/from16 v11, v29

    move/from16 v12, v26

    move/from16 v13, v29

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 654
    const/high16 v1, 0x3f800000    # 1.0f

    add-float v13, v17, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v15, v29, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move/from16 v12, v21

    move/from16 v14, v21

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 655
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move/from16 v12, v21

    move/from16 v13, v17

    move/from16 v14, v22

    move/from16 v15, v17

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 656
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move/from16 v12, v21

    move/from16 v13, v29

    move/from16 v14, v22

    move/from16 v15, v29

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 691
    .end local v7    # "sx":F
    .end local v8    # "sy":F
    .end local v10    # "rRight":F
    .end local v17    # "bottom":F
    .end local v19    # "cx":F
    .end local v20    # "cy":F
    .end local v21    # "lLeft":F
    .end local v22    # "lRight":F
    .end local v23    # "lcdHeight":I
    .end local v24    # "lcdWidth":I
    .end local v26    # "rLeft":F
    .end local v27    # "rect":Landroid/graphics/Rect;
    .end local v29    # "top":F
    :cond_1
    :goto_5
    return-void

    .line 626
    .restart local v23    # "lcdHeight":I
    .restart local v24    # "lcdWidth":I
    .restart local v27    # "rect":Landroid/graphics/Rect;
    :cond_2
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float v10, v1, v3

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    add-float v11, v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float v12, v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float v13, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 627
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsSetAsContactPhoto:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2100(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 628
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;->STROKE:Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glrenderer/GLPaint;->setStyle(Lcom/sec/android/gallery3d/glrenderer/GLPaint$Style;)V

    .line 629
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawCircle(FFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    goto/16 :goto_0

    .line 631
    :cond_3
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->left:F

    add-float v10, v1, v2

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    add-float v11, v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/high16 v2, 0x40400000    # 3.0f

    div-float v12, v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float v13, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 632
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float v10, v1, v3

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    add-float v11, v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float v12, v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x40400000    # 3.0f

    div-float v13, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v14

    move-object/from16 v9, p1

    invoke-interface/range {v9 .. v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    goto/16 :goto_0

    .line 644
    .restart local v7    # "sx":F
    .restart local v8    # "sy":F
    .restart local v19    # "cx":F
    .restart local v20    # "cy":F
    .restart local v26    # "rLeft":F
    :cond_4
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    add-float v10, v19, v1

    goto/16 :goto_1

    .line 645
    .restart local v10    # "rRight":F
    :cond_5
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v21, v19, v1

    goto/16 :goto_2

    .line 647
    .restart local v21    # "lLeft":F
    .restart local v22    # "lRight":F
    :cond_6
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    add-float v1, v1, v20

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v29, v1, v2

    goto/16 :goto_3

    .line 648
    .restart local v29    # "top":F
    :cond_7
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v1, v20, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v17, v1, v2

    goto/16 :goto_4

    .line 659
    .end local v7    # "sx":F
    .end local v8    # "sy":F
    .end local v10    # "rRight":F
    .end local v19    # "cx":F
    .end local v20    # "cy":F
    .end local v21    # "lLeft":F
    .end local v22    # "lRight":F
    .end local v26    # "rLeft":F
    .end local v29    # "top":F
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 660
    .restart local v7    # "sx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v8

    .line 661
    .restart local v8    # "sy":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v19

    .line 662
    .restart local v19    # "cx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v20

    .line 663
    .restart local v20    # "cy":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v25, v19, v1

    .line 664
    .local v25, "left":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    add-float v28, v19, v1

    .line 665
    .local v28, "right":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v29, v20, v1

    .line 666
    .restart local v29    # "top":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    add-float v17, v20, v1

    .line 667
    .restart local v17    # "bottom":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v8, v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v18, v1, v2

    .line 669
    .local v18, "coef":F
    add-float v12, v25, v18

    sub-float v13, v29, v18

    sub-float v14, v28, v18

    sub-float v15, v29, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 670
    sub-float v12, v28, v18

    sub-float v14, v28, v18

    sub-float v15, v29, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move/from16 v13, v29

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 671
    add-float v12, v25, v18

    add-float v14, v25, v18

    sub-float v15, v29, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move/from16 v13, v29

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 673
    add-float v12, v25, v18

    add-float v13, v17, v18

    sub-float v14, v28, v18

    add-float v15, v17, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 674
    sub-float v12, v28, v18

    sub-float v14, v28, v18

    add-float v15, v17, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move/from16 v13, v17

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 675
    add-float v12, v25, v18

    add-float v14, v25, v18

    add-float v15, v17, v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mDotPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move/from16 v13, v17

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawLine(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    goto/16 :goto_5

    .line 679
    .end local v7    # "sx":F
    .end local v8    # "sy":F
    .end local v17    # "bottom":F
    .end local v18    # "coef":F
    .end local v19    # "cx":F
    .end local v20    # "cy":F
    .end local v23    # "lcdHeight":I
    .end local v24    # "lcdWidth":I
    .end local v25    # "left":F
    .end local v27    # "rect":Landroid/graphics/Rect;
    .end local v28    # "right":F
    .end local v29    # "top":F
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioX:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1600(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v2

    mul-float v7, v1, v2

    .line 680
    .restart local v7    # "sx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mSpotlightRatioY:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1700(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v2

    mul-float v8, v1, v2

    .line 681
    .restart local v8    # "sy":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerX()F

    move-result v19

    .line 682
    .restart local v19    # "cx":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->centerY()F

    move-result v20

    .line 684
    .restart local v20    # "cy":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v5, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v6, v20, v1

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    .line 685
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v5, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v6, v20, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v9

    move-object/from16 v4, p1

    invoke-interface/range {v4 .. v9}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 686
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float v12, v1, v3

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    add-float v13, v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    sub-float v14, v1, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float v15, v1, v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 688
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v12, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v13, v20, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mPaint:Lcom/sec/android/gallery3d/glrenderer/GLPaint;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$2000(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/glrenderer/GLPaint;

    move-result-object v16

    move-object/from16 v11, p1

    move v14, v8

    move v15, v7

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawRect(FFFFLcom/sec/android/gallery3d/glrenderer/GLPaint;)V

    .line 689
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float v12, v19, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v7, v1

    sub-float v13, v20, v1

    const/16 v16, 0x0

    move-object/from16 v11, p1

    move v14, v8

    move v15, v7

    invoke-interface/range {v11 .. v16}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->fillRect(FFFFI)V

    goto/16 :goto_5
.end method

.method private moveEdges(Landroid/view/MotionEvent;)V
    .locals 22
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->getScale()F

    move-result v15

    .line 348
    .local v15, "scale":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 349
    .local v4, "convertX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 350
    .local v5, "convertY":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceX:F

    move/from16 v19, v0

    sub-float v19, v4, v19

    div-float v19, v19, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v6, v19, v20

    .line 351
    .local v6, "dx":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceY:F

    move/from16 v19, v0

    sub-float v19, v5, v19

    div-float v19, v19, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v7, v19, v20

    .line 352
    .local v7, "dy":F
    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceX:F

    .line 353
    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceY:F

    .line 354
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    .line 355
    .local v13, "r":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView;->getMaxIdentityRect()Landroid/graphics/RectF;

    move-result-object v10

    .line 357
    .local v10, "max":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x10

    if-eqz v19, :cond_2

    .line 358
    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v6, v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v6

    .line 359
    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v7, v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v7

    .line 361
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    add-float v19, v19, v7

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    cmpl-float v19, v19, v20

    if-ltz v19, :cond_0

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    add-float v19, v19, v7

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    cmpg-float v19, v19, v20

    if-gtz v19, :cond_0

    .line 362
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    add-float v19, v19, v7

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->top:F

    .line 363
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    add-float v19, v19, v7

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->bottom:F

    .line 365
    :cond_0
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    add-float v19, v19, v6

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    cmpl-float v19, v19, v20

    if-ltz v19, :cond_1

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    add-float v19, v19, v6

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    cmpg-float v19, v19, v20

    if-gtz v19, :cond_1

    .line 366
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    add-float v19, v19, v6

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->left:F

    .line 367
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    add-float v19, v19, v6

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->right:F

    .line 441
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    # invokes: Lcom/sec/android/gallery3d/ui/CropView;->check()V
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView;->access$1400(Lcom/sec/android/gallery3d/ui/CropView;)V

    .line 443
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->invalidate()V

    .line 444
    return-void

    .line 371
    :cond_2
    new-instance v12, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceX:F

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceY:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v12, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 372
    .local v12, "point":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->inverseMapPoint(Landroid/graphics/PointF;)V

    .line 374
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    const/high16 v20, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    add-float v9, v19, v20

    .line 375
    .local v9, "left":F
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    const/high16 v20, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    sub-float v14, v19, v20

    .line 376
    .local v14, "right":F
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    const/high16 v20, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    add-float v17, v19, v20

    .line 377
    .local v17, "top":F
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    const/high16 v20, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    sub-float v3, v19, v20

    .line 378
    .local v3, "bottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsManualFD:Z
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView;->access$1200(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v19

    if-eqz v19, :cond_7

    const v11, 0x3c4ccccd    # 0.0125f

    .line 379
    .local v11, "minRangeRatio":F
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x4

    if-eqz v19, :cond_3

    .line 380
    iget v0, v12, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    add-float v20, v9, v11

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v21, v0

    invoke-static/range {v19 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->right:F

    .line 382
    const/high16 v19, 0x3f800000    # 1.0f

    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const v20, 0x3cc8b440    # 0.024500012f

    cmpg-float v19, v19, v20

    if-gez v19, :cond_3

    .line 383
    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->right:F

    .line 386
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x1

    if-eqz v19, :cond_4

    .line 387
    iget v0, v12, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    sub-float v21, v14, v11

    invoke-static/range {v19 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->left:F

    .line 389
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    const v20, 0x3cc8b440    # 0.024500012f

    cmpg-float v19, v19, v20

    if-gez v19, :cond_4

    .line 390
    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->left:F

    .line 393
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x2

    if-eqz v19, :cond_5

    .line 394
    iget v0, v12, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v21, v3, v11

    invoke-static/range {v19 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->top:F

    .line 396
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    const v20, 0x3cc8b440    # 0.024500012f

    cmpg-float v19, v19, v20

    if-gez v19, :cond_5

    .line 397
    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->top:F

    .line 400
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x8

    if-eqz v19, :cond_6

    .line 401
    iget v0, v12, Landroid/graphics/PointF;->y:F

    move/from16 v19, v0

    add-float v20, v17, v11

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v21, v0

    invoke-static/range {v19 .. v21}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->bottom:F

    .line 403
    const/high16 v19, 0x3f800000    # 1.0f

    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v19, v19, v20

    const v20, 0x3cc8b440    # 0.024500012f

    cmpg-float v19, v19, v20

    if-gez v19, :cond_6

    .line 404
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->bottom:F

    .line 407
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v19

    const/high16 v20, -0x40800000    # -1.0f

    cmpl-float v19, v19, v20

    if-eqz v19, :cond_1

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v19, v19, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v16, v19, v20

    .line 409
    .local v16, "targetRatio":F
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v19

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v20

    div-float v19, v19, v20

    cmpl-float v19, v19, v16

    if-lez v19, :cond_9

    .line 410
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v19

    div-float v8, v19, v16

    .line 411
    .local v8, "height":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x8

    if-eqz v19, :cond_8

    .line 412
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    add-float v19, v19, v8

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->bottom:F

    .line 424
    .end local v8    # "height":F
    :goto_2
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v19

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v20

    div-float v19, v19, v20

    cmpl-float v19, v19, v16

    if-lez v19, :cond_c

    .line 425
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v19

    mul-float v18, v19, v16

    .line 426
    .local v18, "width":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x1

    if-eqz v19, :cond_b

    .line 427
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v18

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v14}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->left:F

    goto/16 :goto_0

    .line 378
    .end local v11    # "minRangeRatio":F
    .end local v16    # "targetRatio":F
    .end local v18    # "width":F
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 414
    .restart local v8    # "height":F
    .restart local v11    # "minRangeRatio":F
    .restart local v16    # "targetRatio":F
    :cond_8
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->top:F

    goto :goto_2

    .line 417
    .end local v8    # "height":F
    :cond_9
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v19

    mul-float v18, v19, v16

    .line 418
    .restart local v18    # "width":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x1

    if-eqz v19, :cond_a

    .line 419
    iget v0, v13, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v18

    iget v0, v10, Landroid/graphics/RectF;->left:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v14}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->left:F

    goto :goto_2

    .line 421
    :cond_a
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    add-float v19, v19, v18

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v9, v1}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->right:F

    goto/16 :goto_2

    .line 429
    :cond_b
    iget v0, v13, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    add-float v19, v19, v18

    iget v0, v10, Landroid/graphics/RectF;->right:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v9, v1}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->right:F

    goto/16 :goto_0

    .line 432
    .end local v18    # "width":F
    :cond_c
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v19

    div-float v8, v19, v16

    .line 433
    .restart local v8    # "height":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x8

    if-eqz v19, :cond_d

    .line 434
    iget v0, v13, Landroid/graphics/RectF;->top:F

    move/from16 v19, v0

    add-float v19, v19, v8

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 436
    :cond_d
    iget v0, v13, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    iget v0, v10, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v19

    move/from16 v0, v19

    iput v0, v13, Landroid/graphics/RectF;->top:F

    goto/16 :goto_0
.end method

.method private setInitRectangle(Landroid/graphics/RectF;)V
    .locals 10
    .param p1, "rect"    # Landroid/graphics/RectF;

    .prologue
    const/high16 v9, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x3f000000    # 0.5f

    .line 698
    if-eqz p1, :cond_0

    .line 699
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    invoke-virtual {v4, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 745
    :goto_0
    return-void

    .line 701
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v5

    cmpl-float v5, v5, v9

    if-nez v5, :cond_2

    move v2, v4

    .line 704
    .local v2, "targetRatio":F
    :goto_1
    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/CropView;->access$2400()F

    move-result v5

    div-float v3, v5, v7

    .line 705
    .local v3, "w":F
    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->SELECTION_RATIO:F
    invoke-static {}, Lcom/sec/android/gallery3d/ui/CropView;->access$2400()F

    move-result v5

    div-float v0, v5, v7

    .line 706
    .local v0, "h":F
    cmpl-float v5, v2, v4

    if-lez v5, :cond_3

    .line 707
    div-float v0, v3, v2

    .line 711
    :goto_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsWallpaper:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$2500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 712
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    cmpl-float v4, v5, v4

    if-lez v4, :cond_4

    .line 713
    const/high16 v0, 0x3f000000    # 0.5f

    .line 714
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    div-float v3, v4, v7

    .line 742
    :cond_1
    :goto_3
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    sub-float v5, v8, v3

    sub-float v6, v8, v0

    add-float v7, v8, v3

    add-float/2addr v8, v0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 743
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->mapRect(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    goto :goto_0

    .line 701
    .end local v0    # "h":F
    .end local v2    # "targetRatio":F
    .end local v3    # "w":F
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v6

    int-to-float v6, v6

    div-float v2, v5, v6

    goto :goto_1

    .line 709
    .restart local v0    # "h":F
    .restart local v2    # "targetRatio":F
    .restart local v3    # "w":F
    :cond_3
    mul-float v3, v0, v2

    goto :goto_2

    .line 716
    :cond_4
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    div-float v0, v4, v7

    .line 717
    const/high16 v3, 0x3f000000    # 0.5f

    goto :goto_3

    .line 719
    :cond_5
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsSNote:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$2600(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$2200(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsSView:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$2700(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 720
    :cond_6
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mIsSingleFrame:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$2200(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 721
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    cmpl-float v4, v5, v4

    if-lez v4, :cond_7

    .line 722
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    div-float v3, v4, v7

    .line 723
    div-float v0, v3, v2

    goto/16 :goto_3

    .line 725
    :cond_7
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    div-float v0, v4, v7

    .line 726
    mul-float v3, v0, v2

    goto/16 :goto_3

    .line 729
    :cond_8
    div-float v4, v8, v3

    div-float v5, v8, v0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_9

    div-float v1, v8, v0

    .line 730
    .local v1, "s":F
    :goto_4
    mul-float/2addr v0, v1

    .line 731
    mul-float/2addr v3, v1

    .line 732
    goto/16 :goto_3

    .line 729
    .end local v1    # "s":F
    :cond_9
    div-float v1, v8, v3

    goto :goto_4

    .line 733
    :cond_a
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v5

    cmpl-float v5, v5, v9

    if-nez v5, :cond_1

    .line 734
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    cmpl-float v4, v5, v4

    if-lez v4, :cond_b

    .line 735
    const/high16 v0, 0x3f000000    # 0.5f

    .line 736
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    div-float v3, v4, v7

    goto/16 :goto_3

    .line 738
    :cond_b
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    div-float v0, v4, v7

    .line 739
    const/high16 v3, 0x3f000000    # 0.5f

    goto/16 :goto_3
.end method

.method private setMovingEdges(Landroid/view/MotionEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 447
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v4, v9, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    .line 448
    .local v4, "r":Landroid/graphics/RectF;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    .line 449
    .local v7, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    .line 451
    .local v8, "y":F
    iget v9, v4, Landroid/graphics/RectF;->left:F

    const/high16 v10, 0x42480000    # 50.0f

    add-float/2addr v9, v10

    cmpl-float v9, v7, v9

    if-lez v9, :cond_1

    iget v9, v4, Landroid/graphics/RectF;->right:F

    const/high16 v10, 0x42480000    # 50.0f

    sub-float/2addr v9, v10

    cmpg-float v9, v7, v9

    if-gez v9, :cond_1

    iget v9, v4, Landroid/graphics/RectF;->top:F

    const/high16 v10, 0x42480000    # 50.0f

    add-float/2addr v9, v10

    cmpl-float v9, v8, v9

    if-lez v9, :cond_1

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    const/high16 v10, 0x42480000    # 50.0f

    sub-float/2addr v9, v10

    cmpg-float v9, v8, v9

    if-gez v9, :cond_1

    .line 453
    const/16 v9, 0x10

    iput v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    iget v9, v4, Landroid/graphics/RectF;->top:F

    const/high16 v10, 0x42480000    # 50.0f

    sub-float/2addr v9, v10

    cmpg-float v9, v9, v8

    if-gtz v9, :cond_9

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    const/high16 v10, 0x42480000    # 50.0f

    add-float/2addr v9, v10

    cmpg-float v9, v8, v9

    if-gtz v9, :cond_9

    const/4 v2, 0x1

    .line 459
    .local v2, "inVerticalRange":Z
    :goto_1
    iget v9, v4, Landroid/graphics/RectF;->left:F

    const/high16 v10, 0x42480000    # 50.0f

    sub-float/2addr v9, v10

    cmpg-float v9, v9, v7

    if-gtz v9, :cond_a

    iget v9, v4, Landroid/graphics/RectF;->right:F

    const/high16 v10, 0x42480000    # 50.0f

    add-float/2addr v9, v10

    cmpg-float v9, v7, v9

    if-gtz v9, :cond_a

    const/4 v1, 0x1

    .line 462
    .local v1, "inHorizontalRange":Z
    :goto_2
    if-eqz v2, :cond_5

    .line 463
    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v9, v7, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x42480000    # 50.0f

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_b

    const/4 v3, 0x1

    .line 464
    .local v3, "left":Z
    :goto_3
    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v9, v7, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x42480000    # 50.0f

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_c

    const/4 v5, 0x1

    .line 465
    .local v5, "right":Z
    :goto_4
    if-eqz v3, :cond_2

    if-eqz v5, :cond_2

    .line 466
    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v9, v7, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, v4, Landroid/graphics/RectF;->right:F

    sub-float v10, v7, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_d

    const/4 v3, 0x1

    .line 467
    :goto_5
    if-nez v3, :cond_e

    const/4 v5, 0x1

    .line 469
    :cond_2
    :goto_6
    if-eqz v3, :cond_3

    iget v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    or-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 470
    :cond_3
    if-eqz v5, :cond_4

    iget v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    or-int/lit8 v9, v9, 0x4

    iput v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 471
    :cond_4
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v9

    const/high16 v10, -0x40800000    # -1.0f

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_5

    if-eqz v1, :cond_5

    .line 472
    iget v10, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    iget v9, v4, Landroid/graphics/RectF;->top:F

    iget v11, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v9, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v9, v11

    cmpl-float v9, v8, v9

    if-lez v9, :cond_f

    const/16 v9, 0x8

    :goto_7
    or-int/2addr v9, v10

    iput v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 475
    .end local v3    # "left":Z
    .end local v5    # "right":Z
    :cond_5
    if-eqz v1, :cond_0

    .line 476
    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v9, v8, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x42480000    # 50.0f

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_10

    const/4 v6, 0x1

    .line 477
    .local v6, "top":Z
    :goto_8
    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v9, v8, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x42480000    # 50.0f

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_11

    const/4 v0, 0x1

    .line 478
    .local v0, "bottom":Z
    :goto_9
    if-eqz v6, :cond_6

    if-eqz v0, :cond_6

    .line 479
    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v9, v8, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v8, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_12

    const/4 v6, 0x1

    .line 480
    :goto_a
    if-nez v6, :cond_13

    const/4 v0, 0x1

    .line 482
    :cond_6
    :goto_b
    if-eqz v6, :cond_7

    iget v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    or-int/lit8 v9, v9, 0x2

    iput v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 483
    :cond_7
    if-eqz v0, :cond_8

    iget v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    or-int/lit8 v9, v9, 0x8

    iput v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 484
    :cond_8
    iget-object v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v9

    const/high16 v10, -0x40800000    # -1.0f

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_0

    if-eqz v2, :cond_0

    .line 485
    iget v10, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    iget v9, v4, Landroid/graphics/RectF;->left:F

    iget v11, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v9, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v9, v11

    cmpl-float v9, v7, v9

    if-lez v9, :cond_14

    const/4 v9, 0x4

    :goto_c
    or-int/2addr v9, v10

    iput v9, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    goto/16 :goto_0

    .line 457
    .end local v0    # "bottom":Z
    .end local v1    # "inHorizontalRange":Z
    .end local v2    # "inVerticalRange":Z
    .end local v6    # "top":Z
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 459
    .restart local v2    # "inVerticalRange":Z
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 463
    .restart local v1    # "inHorizontalRange":Z
    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 464
    .restart local v3    # "left":Z
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 466
    .restart local v5    # "right":Z
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 467
    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 472
    :cond_f
    const/4 v9, 0x2

    goto/16 :goto_7

    .line 476
    .end local v3    # "left":Z
    .end local v5    # "right":Z
    :cond_10
    const/4 v6, 0x0

    goto :goto_8

    .line 477
    .restart local v6    # "top":Z
    :cond_11
    const/4 v0, 0x0

    goto :goto_9

    .line 479
    .restart local v0    # "bottom":Z
    :cond_12
    const/4 v6, 0x0

    goto :goto_a

    .line 480
    :cond_13
    const/4 v0, 0x0

    goto :goto_b

    .line 485
    :cond_14
    const/4 v9, 0x1

    goto :goto_c
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 325
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    iget-object v1, v1, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mTouch:Z
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$800(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mTouch:Z
    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$802(Lcom/sec/android/gallery3d/ui/CropView;Z)Z

    .line 327
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$700(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "cropDescription":Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    .line 330
    .end local v0    # "cropDescription":Ljava/lang/String;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public isMoved()Z
    .locals 2

    .prologue
    .line 694
    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 492
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->isScaleMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    iput v2, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->invalidate()V

    .line 519
    :goto_0
    return v1

    .line 497
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 499
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceX:F

    .line 500
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mReferenceY:F

    .line 501
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->setMovingEdges(Landroid/view/MotionEvent;)V

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->invalidate()V

    .line 503
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mTouch:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$802(Lcom/sec/android/gallery3d/ui/CropView;Z)Z

    goto :goto_0

    .line 507
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1502(Lcom/sec/android/gallery3d/ui/CropView;Z)Z

    .line 508
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->moveEdges(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 512
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1502(Lcom/sec/android/gallery3d/ui/CropView;Z)Z

    .line 513
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # setter for: Lcom/sec/android/gallery3d/ui/CropView;->mTouch:Z
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$802(Lcom/sec/android/gallery3d/ui/CropView;Z)Z

    .line 514
    iput v2, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    .line 515
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->invalidate()V

    goto :goto_0

    .line 497
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 21
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView;->borderDisplayRect:Landroid/graphics/RectF;

    move-object/from16 v17, v0

    .line 525
    .local v17, "r":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->drawHighlightRectangle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;)V

    .line 527
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v19, v0

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v7, v18, v19

    .line 528
    .local v7, "centerY":F
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    add-float v18, v18, v19

    const/high16 v19, 0x40000000    # 2.0f

    div-float v6, v18, v19

    .line 529
    .local v6, "centerX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v4, v18, v19

    .line 530
    .local v4, "arrowCenterX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v5, v18, v19

    .line 531
    .local v5, "arrowCenterY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v8, v18, v19

    .line 532
    .local v8, "edgeCenterX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->getHeight()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v9, v18, v19

    .line 533
    .local v9, "edgeCenterY":F
    const/high16 v16, 0x40000000    # 2.0f

    .line 535
    .local v16, "offset":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v18, v0

    if-nez v18, :cond_8

    const/4 v15, 0x1

    .line 536
    .local v15, "notMoving":Z
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x2

    if-eqz v18, :cond_9

    const/4 v14, 0x1

    .line 537
    .local v14, "isMovingTop":Z
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x1

    if-eqz v18, :cond_a

    const/4 v12, 0x1

    .line 538
    .local v12, "isMovingLeft":Z
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x4

    if-eqz v18, :cond_b

    const/4 v13, 0x1

    .line 539
    .local v13, "isMovingRight":Z
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mMovingEdges:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x8

    if-eqz v18, :cond_c

    const/4 v11, 0x1

    .line 541
    .local v11, "isMovingBottom":Z
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mAspectRatio:F
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1300(Lcom/sec/android/gallery3d/ui/CropView;)F

    move-result v18

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_d

    const/16 v18, 0x1

    :goto_5
    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 542
    .local v10, "isDrawArrow":Ljava/lang/Boolean;
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    if-nez v18, :cond_3

    .line 544
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    sub-float v20, v7, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    sub-float v19, v19, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    sub-float v20, v7, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 546
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    sub-float v19, v6, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v20, v20, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 547
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrow:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    sub-float v19, v6, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v20, v20, v16

    sub-float v20, v20, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 549
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_0

    if-eqz v13, :cond_0

    if-nez v14, :cond_0

    if-nez v11, :cond_0

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrowBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    sub-float v20, v7, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 554
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_1

    if-eqz v12, :cond_1

    if-nez v14, :cond_1

    if-nez v11, :cond_1

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrowBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    sub-float v19, v19, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    sub-float v20, v7, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 559
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_2

    if-eqz v14, :cond_2

    if-nez v12, :cond_2

    if-nez v13, :cond_2

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrowBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    sub-float v19, v6, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v20, v20, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 564
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_3

    if-eqz v11, :cond_3

    if-nez v12, :cond_3

    if-nez v13, :cond_3

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mArrowBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    sub-float v19, v6, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v20, v20, v16

    sub-float v20, v20, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 571
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    add-int/lit8 v20, v20, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 573
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicator:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    add-int/lit8 v20, v20, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_4

    if-eqz v12, :cond_4

    if-eqz v14, :cond_4

    .line 577
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicatorBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 581
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_5

    if-eqz v12, :cond_5

    if-eqz v11, :cond_5

    .line 582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicatorBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    add-int/lit8 v20, v20, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 586
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_6

    if-eqz v13, :cond_6

    if-eqz v14, :cond_6

    .line 587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicatorBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 591
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mMove:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/CropView;->access$1500(Lcom/sec/android/gallery3d/ui/CropView;)Z

    move-result v18

    if-eqz v18, :cond_7

    if-eqz v13, :cond_7

    if-eqz v11, :cond_7

    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mEdgeIndicatorBlue:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v19, v0

    sub-float v19, v19, v8

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v19

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v20, v0

    sub-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v20

    add-int/lit8 v20, v20, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 596
    :cond_7
    return-void

    .line 535
    .end local v10    # "isDrawArrow":Ljava/lang/Boolean;
    .end local v11    # "isMovingBottom":Z
    .end local v12    # "isMovingLeft":Z
    .end local v13    # "isMovingRight":Z
    .end local v14    # "isMovingTop":Z
    .end local v15    # "notMoving":Z
    :cond_8
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 536
    .restart local v15    # "notMoving":Z
    :cond_9
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 537
    .restart local v14    # "isMovingTop":Z
    :cond_a
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 538
    .restart local v12    # "isMovingLeft":Z
    :cond_b
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 539
    .restart local v13    # "isMovingRight":Z
    :cond_c
    const/4 v11, 0x0

    goto/16 :goto_4

    .line 541
    .restart local v11    # "isMovingBottom":Z
    :cond_d
    const/16 v18, 0x0

    goto/16 :goto_5
.end method

.method public setFaceRectangle(Landroid/graphics/RectF;)V
    .locals 5
    .param p1, "faceRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 334
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v3

    if-gez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    .line 335
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 336
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v4, v0, v3, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 341
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mScaleListener:Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/CropView;->access$300(Lcom/sec/android/gallery3d/ui/CropView;)Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->mHighlightRect:Landroid/graphics/RectF;

    # invokes: Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->moveCenterPoint(Landroid/graphics/RectF;)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;->access$1100(Lcom/sec/android/gallery3d/ui/CropView$CropScaleListener;Landroid/graphics/RectF;)V

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->invalidate()V

    .line 344
    return-void

    .line 338
    :cond_2
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageHeight:I
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/CropView;->access$1000(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/CropView$HighlightRectangle;->this$0:Lcom/sec/android/gallery3d/ui/CropView;

    # getter for: Lcom/sec/android/gallery3d/ui/CropView;->mImageWidth:I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/CropView;->access$900(Lcom/sec/android/gallery3d/ui/CropView;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v4, v1, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method
