.class public Lcom/sec/android/gallery3d/remote/cloud/CloudImage;
.super Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;
.source "CloudImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/remote/cloud/CloudImage$CloudLargeImageRequest;,
        Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;
    }
.end annotation


# static fields
.field private static final CACHED_IMAGE_SIZE:I = 0x400

.field public static final ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final MAX_THREAD_COUNT:I = 0x5

.field static final PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "CloudImage"

.field private static mSyncIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mPath:Lcom/sec/android/gallery3d/data/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    const-string v0, "/cloud/image/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->ITEM_PATH:Lcom/sec/android/gallery3d/data/Path;

    .line 63
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "cloud_cached_path"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "cloud_thumb_path"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "cloud_is_cached"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "height"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->PROJECTION:[Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;I)V
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "id"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 84
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mHandler:Landroid/os/Handler;

    .line 96
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 97
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 98
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mBaseFileUri:Landroid/net/Uri;

    .line 100
    .local v3, "uri":Landroid/net/Uri;
    sget-object v4, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->PROJECTION:[Ljava/lang/String;

    invoke-static {v2, v3, v4, p3}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;->getItemCursor(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    .line 101
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 102
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "cannot get cursor for: "

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 106
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-gtz v4, :cond_1

    .line 107
    const-string v4, "CloudImage"

    const-string v5, "Cursor getCount is zero"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 111
    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->setProperty(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 120
    :goto_0
    return-void

    .line 113
    :cond_2
    :try_start_1
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "cannot find data for: "

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 118
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 84
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mHandler:Landroid/os/Handler;

    .line 89
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 90
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    invoke-virtual {v0, p3}, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->setProperty(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$600()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method


# virtual methods
.method public delete()V
    .locals 5

    .prologue
    .line 398
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 400
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 401
    .local v1, "where":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mBaseImageUri:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    .end local v1    # "where":Ljava/lang/String;
    :goto_0
    return-void

    .line 402
    :catch_0
    move-exception v0

    .line 403
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public download(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 425
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/android/cloudagent/CloudStore$API;->download(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    :goto_0
    return-void

    .line 426
    :catch_0
    move-exception v0

    .line 427
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 334
    sget-object v0, Lcom/sec/android/gallery3d/provider/GalleryProvider;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;
    .locals 6

    .prologue
    .line 322
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v0

    .line 323
    .local v0, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->date_edited:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 325
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->rotation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaDetails;->addDetail(ILjava/lang/Object;)V

    .line 326
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_url:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->height:I

    return v0
.end method

.method public getItemId()I
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->_id:J

    long-to-int v0, v0

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->longitude:D

    return-wide v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 301
    const/4 v0, 0x2

    return v0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return v0
.end method

.method public getSharedFilePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 345
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v1, Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/cloudagent/CloudStore$API;->prefetchWithBlocking(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    .line 346
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_url:Ljava/lang/String;

    .line 347
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :goto_0
    return-object v1

    .line 348
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 351
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_url:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 4

    .prologue
    .line 356
    const-wide v0, 0xc00880000400L

    .line 359
    .local v0, "operation":J
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_type:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->isSupportedByRegionDecoder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 360
    const-wide/16 v2, 0x40

    or-long/2addr v0, v2

    .line 362
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduCloudDragDrop:Z

    if-eqz v2, :cond_1

    .line 363
    const-wide/high16 v2, 0x20000000000000L

    or-long/2addr v0, v2

    .line 364
    const-wide/high16 v2, 0x10000000000000L

    or-long/2addr v0, v2

    .line 367
    :cond_1
    const-wide/16 v2, 0x4

    or-long/2addr v0, v2

    .line 368
    const-wide/16 v2, 0x20

    or-long/2addr v0, v2

    .line 369
    return-wide v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->thumb_pathname:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->thumbnail_url:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 340
    sget-object v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mBaseImageUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->width:I

    return v0
.end method

.method public requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$ThumbDecodeRequest;-><init>(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;I)V

    return-object v0
.end method

.method public requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 213
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v1, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_status:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v1, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 214
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->startSyncCloud()V

    move-object v1, v2

    .line 221
    :goto_0
    return-object v1

    .line 217
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$CloudLargeImageRequest;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$CloudLargeImageRequest;-><init>(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "CloudImage"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    .line 221
    goto :goto_0
.end method

.method public rotate(I)V
    .locals 8
    .param p1, "degrees"    # I

    .prologue
    .line 374
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->assertNotInRenderThread()V

    .line 375
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 376
    .local v1, "values":Landroid/content/ContentValues;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->rotation:I

    add-int/2addr v2, p1

    rem-int/lit16 v0, v2, 0x168

    .line 377
    .local v0, "orientation":I
    if-gez v0, :cond_0

    .line 378
    add-int/lit16 v0, v0, 0x168

    .line 379
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_type:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 380
    const-string v2, "_size"

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->size:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 381
    const-string v2, "orientation"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 382
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mBaseImageUri:Landroid/net/Uri;

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v7, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 394
    :goto_0
    return-void

    .line 387
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$2;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$2;-><init>(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public startSyncCloud()V
    .locals 3

    .prologue
    .line 248
    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 254
    :cond_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage$1;-><init>(Lcom/sec/android/gallery3d/remote/cloud/CloudImage;)V

    const-string v2, "SyncCloudImage"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 273
    .local v0, "syncCloud":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 274
    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 275
    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mSyncIdList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected updateFromCursor(Landroid/database/Cursor;)Z
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 124
    new-instance v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;-><init>()V

    .line 125
    .local v0, "newEntry":Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->setProperty(Ljava/lang/Object;)V

    .line 127
    new-instance v1, Lcom/sec/android/gallery3d/util/UpdateHelper;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/util/UpdateHelper;-><init>()V

    .line 128
    .local v1, "uh":Lcom/sec/android/gallery3d/util/UpdateHelper;
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->source_id:Ljava/lang/String;

    .line 129
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_url:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_url:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_url:Ljava/lang/String;

    .line 130
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->size:J

    iget-wide v6, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->size:J

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->size:J

    .line 131
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->title:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->title:Ljava/lang/String;

    .line 132
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_type:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_type:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->content_type:Ljava/lang/String;

    .line 133
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->date_edited:J

    iget-wide v6, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->date_edited:J

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(JJ)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->date_edited:J

    .line 134
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->album_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->album_id:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->album_id:Ljava/lang/String;

    .line 135
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_pathname:Ljava/lang/String;

    .line 136
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v2, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->thumb_pathname:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->thumb_pathname:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->thumb_pathname:Ljava/lang/String;

    .line 137
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->rotation:I

    iget v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->rotation:I

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->rotation:I

    .line 138
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_status:I

    iget v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_status:I

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->cache_status:I

    .line 139
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->latitude:D

    iget-wide v6, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->latitude:D

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(DD)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->latitude:D

    .line 140
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-wide v4, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->longitude:D

    iget-wide v6, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->longitude:D

    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(DD)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->longitude:D

    .line 141
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->width:I

    iget v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->width:I

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->width:I

    .line 142
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->mCloudEntry:Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;

    iget v3, v3, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->height:I

    iget v4, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->height:I

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/util/UpdateHelper;->update(II)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudItemEntry;->height:I

    .line 143
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/UpdateHelper;->isUpdated()Z

    move-result v2

    return v2
.end method
