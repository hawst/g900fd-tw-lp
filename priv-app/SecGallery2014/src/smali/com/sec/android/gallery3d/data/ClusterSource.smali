.class Lcom/sec/android/gallery3d/data/ClusterSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "ClusterSource.java"


# static fields
.field static final CLUSTER_ALBUMSET_DOCUMENT:I = 0x6

.field static final CLUSTER_ALBUMSET_FACE:I = 0x4

.field static final CLUSTER_ALBUMSET_FACE_CONFIRMED:I = 0xc

.field static final CLUSTER_ALBUMSET_FACE_CONFIRMED_RECOMMEND:I = 0xd

.field static final CLUSTER_ALBUMSET_FACE_NAMED:I = 0xb

.field static final CLUSTER_ALBUMSET_FESTIVAL:I = 0x10

.field static final CLUSTER_ALBUMSET_GALLERYSEARCH:I = 0xe

.field static final CLUSTER_ALBUMSET_GALLERYSEARCHALL:I = 0xf

.field static final CLUSTER_ALBUMSET_GROUP:I = 0x5

.field static final CLUSTER_ALBUMSET_LOCATION:I = 0x1

.field static final CLUSTER_ALBUMSET_OCR:I = 0x8

.field static final CLUSTER_ALBUMSET_SEARCH:I = 0x7

.field static final CLUSTER_ALBUMSET_SIZE:I = 0x3

.field static final CLUSTER_ALBUMSET_TAG:I = 0x2

.field static final CLUSTER_ALBUMSET_TIME:I = 0x0

.field static final CLUSTER_ALBUMSET_TIME_SEARCH:I = 0xa

.field static final CLUSTER_ALBUM_DOCUMENT:I = 0x107

.field static final CLUSTER_ALBUM_FACE:I = 0x104

.field static final CLUSTER_ALBUM_FACE_NAMED:I = 0x112

.field static final CLUSTER_ALBUM_FACE_RECOGNITION:I = 0x105

.field static final CLUSTER_ALBUM_FESTIVAL:I = 0x114

.field static final CLUSTER_ALBUM_GALLERYSEARCH:I = 0x113

.field static final CLUSTER_ALBUM_GROUP:I = 0x106

.field static final CLUSTER_ALBUM_LOCATION:I = 0x101

.field static final CLUSTER_ALBUM_OCR:I = 0x109

.field static final CLUSTER_ALBUM_SEARCH:I = 0x108

.field static final CLUSTER_ALBUM_SIZE:I = 0x103

.field static final CLUSTER_ALBUM_TAG:I = 0x102

.field static final CLUSTER_ALBUM_TIME:I = 0x100

.field static final CLUSTER_ALBUM_TIME_SEARCH:I = 0x111


# instance fields
.field mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 66
    const-string v0, "cluster"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 67
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 68
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/time"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/location"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/tag"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/size"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/face"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/gallerysearchall"

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/time/*"

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/location/*"

    const/16 v2, 0x101

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/tag/*"

    const/16 v2, 0x102

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/size/*"

    const/16 v2, 0x103

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/face/*"

    const/16 v2, 0x104

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/face_named"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/face_confirmed"

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/face_confirmed_recommend"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/group"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/document"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/festival"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/search"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/gallerysearch"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/ocr/*"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/time_search"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/face_named/*"

    const/16 v2, 0x112

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/group/*"

    const/16 v2, 0x106

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/document/*"

    const/16 v2, 0x107

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/search/children"

    const/16 v2, 0x108

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/gallerysearchall/children"

    const/16 v2, 0x113

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/festival/*"

    const/16 v2, 0x114

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/cluster/*/ocr/*/0"

    const/16 v2, 0x109

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 106
    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 8
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v7, 0x0

    .line 115
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v5, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v1

    .line 116
    .local v1, "matchType":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/data/PathMatcher;->getVar(I)Ljava/lang/String;

    move-result-object v4

    .line 117
    .local v4, "setsName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 118
    .local v0, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSetsFromString(Ljava/lang/String;)[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 119
    .local v3, "sets":[Lcom/sec/android/gallery3d/data/MediaSet;
    sparse-switch v1, :sswitch_data_0

    .line 179
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bad path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 125
    :sswitch_0
    new-instance v5, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    aget-object v7, v3, v7

    invoke-direct {v5, p1, v6, v7, v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 174
    :goto_0
    return-object v5

    .line 131
    :sswitch_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 134
    .local v2, "parent":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v5, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-direct {v5, p1, v0, v2, v1}, Lcom/sec/android/gallery3d/data/ClusterAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto :goto_0

    .line 148
    .end local v2    # "parent":Lcom/sec/android/gallery3d/data/MediaSet;
    :sswitch_2
    new-instance v5, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    aget-object v7, v3, v7

    invoke-direct {v5, p1, v6, v7, v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto :goto_0

    .line 150
    :sswitch_3
    new-instance v5, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    aget-object v7, v3, v7

    invoke-direct {v5, p1, v6, v7, v1}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto :goto_0

    .line 152
    :sswitch_4
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;

    .line 153
    .local v2, "parent":Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;
    new-instance v5, Lcom/sec/android/gallery3d/data/SearchAlbum;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v5, p1, v0, v6, v2}, Lcom/sec/android/gallery3d/data/SearchAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 156
    .end local v2    # "parent":Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;
    :sswitch_5
    new-instance v5, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    aget-object v7, v3, v7

    invoke-direct {v5, p1, v6, v7, v1}, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto :goto_0

    .line 158
    :sswitch_6
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .line 160
    .local v2, "parent":Lcom/sec/android/gallery3d/data/ClusterAlbumSet;
    new-instance v5, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/ClusterSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v5, p1, v0, v6, v2}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 170
    .end local v2    # "parent":Lcom/sec/android/gallery3d/data/ClusterAlbumSet;
    :sswitch_7
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/Path;->getParent()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 174
    .local v2, "parent":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v5, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-direct {v5, p1, v0, v2, v7}, Lcom/sec/android/gallery3d/data/ClusterAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x8 -> :sswitch_2
        0xa -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_5
        0xf -> :sswitch_2
        0x10 -> :sswitch_2
        0x100 -> :sswitch_1
        0x101 -> :sswitch_1
        0x102 -> :sswitch_1
        0x103 -> :sswitch_1
        0x104 -> :sswitch_1
        0x106 -> :sswitch_7
        0x107 -> :sswitch_7
        0x108 -> :sswitch_4
        0x109 -> :sswitch_7
        0x112 -> :sswitch_7
        0x113 -> :sswitch_6
        0x114 -> :sswitch_7
    .end sparse-switch
.end method
