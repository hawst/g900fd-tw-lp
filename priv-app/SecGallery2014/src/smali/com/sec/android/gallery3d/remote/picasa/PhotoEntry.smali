.class public final Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
.super Lcom/sec/android/gallery3d/remote/picasa/Entry;
.source "PhotoEntry.java"


# annotations
.annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Table;
    value = "photos"
.end annotation


# static fields
.field public static final SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

.field private static final TAG:Ljava/lang/String; = "PhotoEntry"


# instance fields
.field public albumId:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        indexed = true
        value = "album_id"
    .end annotation
.end field

.field public cachePathname:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        doNotMerge = true
        value = "cache_pathname"
    .end annotation
.end field

.field public cachestatus:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        doNotMerge = true
        extraSql = "DEFAULT 0"
        value = "cache_status"
    .end annotation
.end field

.field public commentCount:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "comment_count"
    .end annotation
.end field

.field public contentType:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "content_type"
    .end annotation
.end field

.field public contentUrl:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "content_url"
    .end annotation
.end field

.field public dateEdited:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "date_edited"
    .end annotation
.end field

.field public datePublished:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "date_published"
    .end annotation
.end field

.field public dateTaken:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "date_taken"
    .end annotation
.end field

.field public dateUpdated:J
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "date_updated"
    .end annotation
.end field

.field public displayIndex:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        indexed = true
        value = "display_index"
    .end annotation
.end field

.field public editUri:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "edit_uri"
    .end annotation
.end field

.field public height:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "height"
    .end annotation
.end field

.field public htmlPageUrl:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "html_page_url"
    .end annotation
.end field

.field public keywords:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "keywords"
    .end annotation
.end field

.field public latitude:D
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "latitude"
    .end annotation
.end field

.field public longitude:D
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "longitude"
    .end annotation
.end field

.field public rotation:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "rotation"
    .end annotation
.end field

.field public screennailUrl:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "screennail_url"
    .end annotation
.end field

.field public size:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "size"
    .end annotation
.end field

.field public summary:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "summary"
    .end annotation
.end field

.field public syncAccount:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "sync_account"
    .end annotation
.end field

.field public thumbnailUrl:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "thumbnail_url"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "title"
    .end annotation
.end field

.field public width:I
    .annotation runtime Lcom/sec/android/gallery3d/remote/picasa/Entry$Column;
        value = "width"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    const-class v1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->SCHEMA:Lcom/sec/android/gallery3d/remote/picasa/EntrySchema;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/picasa/Entry;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 193
    invoke-super {p0}, Lcom/sec/android/gallery3d/remote/picasa/Entry;->clear()V

    .line 194
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->syncAccount:Ljava/lang/String;

    .line 195
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    .line 196
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->editUri:Ljava/lang/String;

    .line 197
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->albumId:J

    .line 198
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->displayIndex:I

    .line 199
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    .line 200
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->summary:Ljava/lang/String;

    .line 201
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->datePublished:J

    .line 202
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateUpdated:J

    .line 203
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateEdited:J

    .line 204
    iput-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateTaken:J

    .line 205
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->commentCount:I

    .line 206
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->width:I

    .line 207
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->height:I

    .line 208
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    .line 209
    iput v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->size:I

    .line 210
    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    .line 211
    iput-wide v4, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    .line 212
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->thumbnailUrl:Ljava/lang/String;

    .line 213
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    .line 214
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    .line 215
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    .line 216
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->htmlPageUrl:Ljava/lang/String;

    .line 217
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->keywords:Ljava/lang/String;

    .line 218
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 222
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    if-nez v2, :cond_1

    .line 225
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 224
    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;

    .line 225
    .local v0, "p":Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->syncAccount:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->syncAccount:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->editUri:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->editUri:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->albumId:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->albumId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->displayIndex:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->displayIndex:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->summary:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->summary:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->datePublished:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->datePublished:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateUpdated:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateUpdated:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateEdited:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateEdited:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateTaken:J

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateTaken:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->commentCount:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->commentCount:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->width:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->width:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->height:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->height:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->size:I

    iget v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->size:I

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    iget-wide v4, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->thumbnailUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->thumbnailUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->htmlPageUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->htmlPageUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->keywords:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->keywords:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->cachePathname:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public setPropertyFromXml(Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 10
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;
    .param p4, "content"    # Ljava/lang/String;

    .prologue
    .line 255
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 256
    .local v2, "localNameChar":C
    const-string v7, "http://schemas.google.com/photos/2007"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 257
    sparse-switch v2, :sswitch_data_0

    .line 384
    .end local v2    # "localNameChar":C
    :cond_0
    :goto_0
    return-void

    .line 259
    .restart local v2    # "localNameChar":C
    :sswitch_0
    const-string v7, "id"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 260
    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->id:J

    goto :goto_0

    .line 381
    .end local v2    # "localNameChar":C
    :catch_0
    move-exception v0

    .line 382
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 264
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "localNameChar":C
    :sswitch_1
    const-string v7, "albumid"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 265
    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->albumId:J

    goto :goto_0

    .line 269
    :sswitch_2
    const-string/jumbo v7, "timestamp"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 270
    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateTaken:J

    goto :goto_0

    .line 274
    :sswitch_3
    const-string v7, "commentCount"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 275
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->commentCount:I

    goto :goto_0

    .line 279
    :sswitch_4
    const-string/jumbo v7, "width"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 280
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->width:I

    goto :goto_0

    .line 284
    :sswitch_5
    const-string v7, "height"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 285
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->height:I

    goto :goto_0

    .line 289
    :sswitch_6
    const-string v7, "rotation"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 290
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->rotation:I

    goto :goto_0

    .line 294
    :sswitch_7
    const-string/jumbo v7, "size"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 295
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->size:I

    goto :goto_0

    .line 299
    :sswitch_8
    const-string v7, "latitude"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 300
    invoke-static {p4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    goto/16 :goto_0

    .line 301
    :cond_1
    const-string v7, "longitude"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 302
    invoke-static {p4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D

    goto/16 :goto_0

    .line 310
    :cond_2
    const-string v7, "http://www.w3.org/2005/Atom"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 311
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 333
    :pswitch_1
    const-string v7, "link"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 334
    const-string v7, ""

    const-string v8, "rel"

    invoke-interface {p3, v7, v8}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 335
    .local v3, "rel":Ljava/lang/String;
    const-string v7, ""

    const-string v8, "href"

    invoke-interface {p3, v7, v8}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 336
    .local v1, "href":Ljava/lang/String;
    const-string v7, "alternate"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const-string/jumbo v7, "text/html"

    const-string v8, ""

    const-string/jumbo v9, "type"

    invoke-interface {p3, v8, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 338
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->htmlPageUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 313
    .end local v1    # "href":Ljava/lang/String;
    .end local v3    # "rel":Ljava/lang/String;
    :pswitch_2
    const-string/jumbo v7, "title"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 314
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 318
    :pswitch_3
    const-string/jumbo v7, "summary"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 319
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->summary:Ljava/lang/String;

    goto/16 :goto_0

    .line 323
    :pswitch_4
    const-string v7, "published"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 324
    invoke-static {p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->parseAtomTimestamp(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->datePublished:J

    goto/16 :goto_0

    .line 328
    :pswitch_5
    const-string/jumbo v7, "updated"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 329
    invoke-static {p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->parseAtomTimestamp(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateUpdated:J

    goto/16 :goto_0

    .line 339
    .restart local v1    # "href":Ljava/lang/String;
    .restart local v3    # "rel":Ljava/lang/String;
    :cond_3
    const-string v7, "edit"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 340
    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->editUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 347
    .end local v1    # "href":Ljava/lang/String;
    .end local v3    # "rel":Ljava/lang/String;
    :cond_4
    const-string v7, "http://www.w3.org/2007/app"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 348
    const-string v7, "edited"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 349
    invoke-static {p4}, Lcom/sec/android/gallery3d/remote/picasa/GDataParser;->parseAtomTimestamp(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->dateEdited:J

    goto/16 :goto_0

    .line 351
    :cond_5
    const-string v7, "http://search.yahoo.com/mrss/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 352
    const-string/jumbo v7, "thumbnail"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 353
    const-string v7, ""

    const-string/jumbo v8, "url"

    invoke-interface {p3, v7, v8}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 354
    .local v6, "url":Ljava/lang/String;
    const-string v7, "/s200-c/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 355
    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->thumbnailUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 356
    :cond_6
    const-string v7, "/s640/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 357
    iput-object v6, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->screennailUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 359
    :cond_7
    const-string v7, "PhotoEntry"

    const-string v8, "Unexpected format of the thumbnail urlreturned from Picasaweb: "

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 362
    .end local v6    # "url":Ljava/lang/String;
    :cond_8
    const-string v7, "content"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 364
    const-string v7, ""

    const-string/jumbo v8, "type"

    invoke-interface {p3, v7, v8}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 365
    .local v5, "type":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    if-eqz v7, :cond_9

    const-string/jumbo v7, "video/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 366
    :cond_9
    const-string v7, ""

    const-string/jumbo v8, "url"

    invoke-interface {p3, v7, v8}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentUrl:Ljava/lang/String;

    .line 367
    iput-object v5, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->contentType:Ljava/lang/String;

    goto/16 :goto_0

    .line 369
    .end local v5    # "type":Ljava/lang/String;
    :cond_a
    const-string v7, "keywords"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 370
    iput-object p4, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->keywords:Ljava/lang/String;

    goto/16 :goto_0

    .line 372
    :cond_b
    const-string v7, "http://www.opengis.net/gml"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 373
    const-string v7, "pos"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 374
    const/16 v7, 0x20

    invoke-virtual {p4, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 375
    .local v4, "spaceIndex":I
    const/4 v7, -0x1

    if-eq v4, v7, :cond_0

    .line 376
    const/4 v7, 0x0

    invoke-virtual {p4, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->latitude:D

    .line 377
    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/gallery3d/remote/picasa/PhotoEntry;->longitude:D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 257
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_1
        0x63 -> :sswitch_3
        0x68 -> :sswitch_5
        0x69 -> :sswitch_0
        0x6c -> :sswitch_8
        0x72 -> :sswitch_6
        0x73 -> :sswitch_7
        0x74 -> :sswitch_2
        0x77 -> :sswitch_4
    .end sparse-switch

    .line 311
    :pswitch_data_0
    .packed-switch 0x6c
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method
