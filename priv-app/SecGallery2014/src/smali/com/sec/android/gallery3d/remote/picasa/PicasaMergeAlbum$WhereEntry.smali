.class Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;
.super Ljava/lang/Object;
.source "PicasaMergeAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WhereEntry"
.end annotation


# instance fields
.field public args:[Ljava/lang/String;

.field public selection:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 304
    packed-switch p1, :pswitch_data_0

    .line 320
    :pswitch_0
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->selection:Ljava/lang/String;

    .line 321
    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->args:[Ljava/lang/String;

    .line 325
    :goto_0
    return-void

    .line 306
    :pswitch_1
    const-string v0, "content_type LIKE ?"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->selection:Ljava/lang/String;

    .line 307
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "image/%"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->args:[Ljava/lang/String;

    goto :goto_0

    .line 313
    :pswitch_2
    const-string v0, "content_type LIKE ?"

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->selection:Ljava/lang/String;

    .line 314
    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, "video/%"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum$WhereEntry;->args:[Ljava/lang/String;

    goto :goto_0

    .line 304
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
