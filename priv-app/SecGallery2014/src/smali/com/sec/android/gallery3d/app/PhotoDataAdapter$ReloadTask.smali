.class Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;
.super Lcom/sec/android/gallery3d/app/DetailReloadTask;
.source "PhotoDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReloadTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Ljava/lang/String;)V
    .locals 2
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    .line 1599
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 1600
    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;
    invoke-static {p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AlbumReloader;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lcom/sec/android/gallery3d/app/DetailReloadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/gallery3d/app/AlbumReloader;)V

    .line 1601
    return-void
.end method

.method private findCurrentMediaItem(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 5
    .param p1, "info"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

    .prologue
    const/4 v2, 0x0

    .line 1673
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 1674
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget v3, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    iget v4, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    sub-int v0, v3, v4

    .line 1676
    .local v0, "index":I
    if-nez v1, :cond_1

    .line 1677
    const-string v3, "PhotoDataAdapter"

    const-string v4, "findCurrentMediaItem : items is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1681
    :cond_0
    :goto_0
    return-object v2

    :cond_1
    if-ltz v0, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0
.end method

.method private findIndexOfPathInCache(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;Lcom/sec/android/gallery3d/data/Path;)I
    .locals 6
    .param p1, "info"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v4, -0x1

    .line 1722
    iget-object v2, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 1723
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez v2, :cond_1

    .line 1730
    :cond_0
    :goto_0
    return v4

    .line 1724
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "n":I
    :goto_1
    if-ge v0, v3, :cond_0

    .line 1725
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1726
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    if-ne v5, p2, :cond_2

    .line 1727
    iget v4, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    add-int/2addr v4, v0

    goto :goto_0

    .line 1724
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private findIndexOfTarget(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;)I
    .locals 8
    .param p1, "info"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

    .prologue
    .line 1685
    iget-object v5, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->target:Lcom/sec/android/gallery3d/data/Path;

    if-nez v5, :cond_0

    iget v5, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    .line 1718
    :goto_0
    return v5

    .line 1686
    :cond_0
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 1689
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v1, :cond_2

    .line 1695
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .local v4, "n":I
    :goto_1
    if-ge v0, v4, :cond_2

    .line 1696
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    iget-object v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->target:Lcom/sec/android/gallery3d/data/Path;

    if-ne v5, v6, :cond_1

    .line 1697
    iget v5, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    add-int/2addr v5, v0

    goto :goto_0

    .line 1695
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1702
    .end local v0    # "i":I
    .end local v4    # "n":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    iget-object v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->target:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    .line 1703
    .local v3, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v2, 0x0

    .line 1705
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v5, v3, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_3

    move-object v2, v3

    .line 1706
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1708
    :cond_3
    if-eqz v2, :cond_4

    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    new-instance v5, Ljava/io/File;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1710
    const/4 v5, -0x1

    goto :goto_0

    .line 1715
    :cond_4
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v5, :cond_5

    .line 1716
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    iget-object v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->target:Lcom/sec/android/gallery3d/data/Path;

    iget v7, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOfItemEx(Lcom/sec/android/gallery3d/data/Path;I)I

    move-result v5

    goto :goto_0

    .line 1718
    :cond_5
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    iget-object v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->target:Lcom/sec/android/gallery3d/data/Path;

    iget v7, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOfItem(Lcom/sec/android/gallery3d/data/Path;I)I

    move-result v5

    goto :goto_0
.end method


# virtual methods
.method protected onLoadData()Z
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/4 v6, 0x1

    .line 1610
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getReloadInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3000(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    move-result-object v5

    .line 1611
    .local v5, "reloadInfo":Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    new-instance v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {v8, v9, v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;)V

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;

    .line 1612
    .local v3, "info":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;
    if-nez v3, :cond_1

    const/4 v6, 0x0

    .line 1669
    :cond_0
    :goto_0
    return v6

    .line 1613
    :cond_1
    iget-wide v8, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->version:J

    iget-wide v10, v5, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->version:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_2

    .line 1614
    iput-boolean v6, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->reloadContent:Z

    .line 1615
    iget v7, v5, Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;->size:I

    iput v7, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->size:I

    .line 1617
    :cond_2
    iget-boolean v7, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->reloadContent:Z

    if-eqz v7, :cond_0

    .line 1621
    iget v7, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentEnd:I

    iget v8, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    sub-int v0, v7, v8

    .line 1622
    .local v0, "count":I
    if-gez v0, :cond_3

    const/4 v0, 0x0

    .line 1623
    :cond_3
    monitor-enter p0

    .line 1624
    :try_start_0
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v7, :cond_8

    .line 1625
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    iget v8, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    invoke-virtual {v7, v8, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 1629
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1630
    const/4 v2, -0x1

    .line 1633
    .local v2, "index":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 1634
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-direct {p0, v3, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->findIndexOfPathInCache(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;Lcom/sec/android/gallery3d/data/Path;)I

    move-result v2

    .line 1635
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;
    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3202(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    .line 1640
    :cond_4
    if-ne v2, v12, :cond_5

    .line 1641
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->findCurrentMediaItem(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 1642
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    iget-object v8, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->target:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1643
    iget v2, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    .line 1656
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    :goto_2
    if-ne v2, v12, :cond_7

    .line 1657
    iget v2, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->indexHint:I

    .line 1658
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v1

    .line 1659
    .local v1, "focusHintDirection":I
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    if-ne v2, v7, :cond_6

    .line 1660
    const/4 v1, 0x0

    .line 1662
    :cond_6
    if-ne v1, v6, :cond_7

    if-lez v2, :cond_7

    .line 1664
    add-int/lit8 v2, v2, -0x1

    .line 1668
    .end local v1    # "focusHintDirection":I
    :cond_7
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    new-instance v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-direct {v8, v9, v3, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;I)V

    # invokes: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$3100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1627
    .end local v2    # "index":I
    :cond_8
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    iget v8, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->contentStart:I

    invoke-virtual {v7, v8, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    goto :goto_1

    .line 1629
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 1645
    .restart local v2    # "index":I
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_9
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->findIndexOfTarget(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;)I

    move-result v2

    goto :goto_2
.end method

.method protected updateLoadingStatus(Z)V
    .locals 2
    .param p1, "loading"    # Z

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->this$0:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    # getter for: Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->access$700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1606
    return-void

    .line 1605
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
