.class public Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;
.super Landroid/preference/PreferenceFragment;
.source "FilterbySettingFragment.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDragSelectedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mIsSwitched:Z

.field private mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mIsSwitched:Z

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$2;-><init>(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mDragSelectedList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 84
    .local v0, "lv":Landroid/widget/ListView;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 86
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 31
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->mContext:Landroid/content/Context;

    .line 37
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCustomFilterBy:Z

    if-eqz v0, :cond_0

    .line 38
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->addPreferencesFromResource(I)V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    const v0, 0x7f07000f

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->addPreferencesFromResource(I)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDetach()V

    .line 47
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment$1;-><init>(Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/gallery3d/settings/FilterbySettingFragment;->init()V

    .line 80
    return-void
.end method
