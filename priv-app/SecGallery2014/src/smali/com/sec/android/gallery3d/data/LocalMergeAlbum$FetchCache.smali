.class Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;
.super Ljava/lang/Object;
.source "LocalMergeAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/LocalMergeAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FetchCache"
.end annotation


# instance fields
.field private final mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mLastetCacheRef:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOldestCacheRef:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private mStartPos:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 0
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 425
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 426
    return-void
.end method


# virtual methods
.method public getItem(ILjava/util/ArrayList;Z)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 11
    .param p1, "index"    # I
    .param p3, "showAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)",
            "Lcom/sec/android/gallery3d/data/MediaItem;"
        }
    .end annotation

    .prologue
    .local p2, "showGroupIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v9, 0x0

    .line 457
    const/4 v5, 0x1

    .line 458
    .local v5, "isSortByLatest":Z
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    if-eqz v8, :cond_0

    .line 459
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v8}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v8

    if-nez v8, :cond_5

    const/4 v5, 0x1

    .line 461
    :cond_0
    :goto_0
    if-eqz v5, :cond_6

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mLastetCacheRef:Ljava/lang/ref/SoftReference;

    .line 463
    .local v2, "cacheRef":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    :goto_1
    iget v7, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mStartPos:I

    .line 465
    .local v7, "startPos":I
    const/4 v6, 0x0

    .line 466
    .local v6, "needLoading":Z
    const/4 v1, 0x0

    .line 467
    .local v1, "cache":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v2, :cond_1

    if-lt p1, v7, :cond_1

    add-int/lit8 v8, v7, 0x10

    if-lt p1, v8, :cond_7

    .line 468
    :cond_1
    const/4 v6, 0x1

    .line 476
    :cond_2
    :goto_2
    if-eqz v6, :cond_3

    .line 477
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v8

    if-eqz v8, :cond_8

    if-eqz p2, :cond_8

    .line 478
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalAlbum;

    const/16 v10, 0x10

    invoke-virtual {v8, p1, v10, p2, p3}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v1

    .line 483
    :goto_3
    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 484
    .end local v2    # "cacheRef":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    .local v3, "cacheRef":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    move v7, p1

    move-object v2, v3

    .line 486
    .end local v3    # "cacheRef":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    :cond_3
    if-lt p1, v7, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    add-int/2addr v8, v7

    if-lt p1, v8, :cond_a

    .line 495
    :cond_4
    iput v7, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mStartPos:I

    .line 496
    if-eqz v5, :cond_9

    .line 497
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mLastetCacheRef:Ljava/lang/ref/SoftReference;

    :goto_4
    move-object v8, v9

    .line 499
    :goto_5
    return-object v8

    .line 459
    .end local v1    # "cache":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v6    # "needLoading":Z
    .end local v7    # "startPos":I
    :cond_5
    const/4 v5, 0x0

    goto :goto_0

    .line 461
    :cond_6
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mOldestCacheRef:Ljava/lang/ref/SoftReference;

    goto :goto_1

    .line 470
    .restart local v1    # "cache":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v2    # "cacheRef":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    .restart local v6    # "needLoading":Z
    .restart local v7    # "startPos":I
    :cond_7
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/util/ArrayList;

    move-object v1, v0

    .line 471
    if-nez v1, :cond_2

    .line 472
    const/4 v6, 0x1

    goto :goto_2

    .line 480
    :cond_8
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/16 v10, 0x10

    invoke-virtual {v8, p1, v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_3

    .line 499
    .end local v2    # "cacheRef":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    :cond_9
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mOldestCacheRef:Ljava/lang/ref/SoftReference;

    goto :goto_4

    .line 489
    :cond_a
    sub-int v8, p1, v7

    :try_start_2
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 495
    iput v7, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mStartPos:I

    .line 496
    if-eqz v5, :cond_b

    .line 497
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mLastetCacheRef:Ljava/lang/ref/SoftReference;

    goto :goto_5

    .line 499
    :cond_b
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mOldestCacheRef:Ljava/lang/ref/SoftReference;

    goto :goto_5

    .line 490
    :catch_0
    move-exception v4

    .line 495
    .local v4, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    iput v7, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mStartPos:I

    .line 496
    if-eqz v5, :cond_c

    .line 497
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mLastetCacheRef:Ljava/lang/ref/SoftReference;

    :goto_6
    move-object v8, v9

    .line 499
    goto :goto_5

    :cond_c
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mOldestCacheRef:Ljava/lang/ref/SoftReference;

    goto :goto_6

    .line 492
    .end local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v4

    .line 495
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    iput v7, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mStartPos:I

    .line 496
    if-eqz v5, :cond_d

    .line 497
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mLastetCacheRef:Ljava/lang/ref/SoftReference;

    :goto_7
    move-object v8, v9

    .line 499
    goto :goto_5

    :cond_d
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mOldestCacheRef:Ljava/lang/ref/SoftReference;

    goto :goto_7

    .line 495
    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catchall_0
    move-exception v8

    iput v7, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mStartPos:I

    .line 496
    if-eqz v5, :cond_e

    .line 497
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mLastetCacheRef:Ljava/lang/ref/SoftReference;

    .line 499
    :goto_8
    throw v8

    :cond_e
    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mOldestCacheRef:Ljava/lang/ref/SoftReference;

    goto :goto_8
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mOldestCacheRef:Ljava/lang/ref/SoftReference;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->mLastetCacheRef:Ljava/lang/ref/SoftReference;

    .line 430
    return-void
.end method
