.class public Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;
.super Lcom/sec/android/gallery3d/remote/RemoteMediaSource;
.source "PicasaSource.java"


# static fields
.field public static final ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

.field private static final IMAGE_MEDIA_ID:I = 0x1

.field private static final MAP_BATCH_COUNT:I = 0x64

.field private static final NO_MATCH:I = -0x1

.field private static final PICASA_ALBUM:I = 0x1

.field private static final PICASA_ALBUMSET:I = 0x0

.field private static final PICASA_IMAGE_ALBUM:I = 0x2

.field private static final PICASA_ITEM:I = 0x4

.field private static final PICASA_MERGE_ALL:I = 0x8

.field private static final PICASA_MERGE_ALLSET:I = 0x5

.field private static final PICASA_MERGE_IMAGE:I = 0x9

.field private static final PICASA_MERGE_IMAGESET:I = 0x6

.field private static final PICASA_MERGE_VIDEO:I = 0xa

.field private static final PICASA_MERGE_VIDEOSET:I = 0x7

.field private static final PICASA_VIDEO_ALBUM:I = 0x3

.field private static final TAG:Ljava/lang/String; = "PicasaSource"


# instance fields
.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-string v0, "/picasa/all"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 4
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    const-string v0, "picasa"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 67
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mUriMatcher:Landroid/content/UriMatcher;

    .line 75
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 76
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/all"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/image"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/video"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/mergedall"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/mergedimage"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/mergedvideo"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/all/*"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/video/*"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/image/*"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/mergedall/*"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/mergedvideo/*"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/mergedimage/*"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/picasa/item/*"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.gallery3d.picasa.contentprovider"

    const-string v2, "photos/#"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.gallery3d.provider"

    const-string v2, "picasa/item/#"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 94
    return-void
.end method

.method public static extractExifValues(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/exif/ExifInterface;)V
    .locals 0
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p1, "exif"    # Lcom/sec/android/gallery3d/exif/ExifInterface;

    .prologue
    .line 228
    return-void
.end method

.method public static isPicasaDataExist(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 303
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 304
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 307
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaContentProvider;->PHOTOS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 312
    if-eqz v6, :cond_2

    .line 313
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 317
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_3

    move v1, v8

    .line 322
    :goto_0
    if-eqz v6, :cond_0

    .line 323
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move v9, v1

    :cond_1
    :goto_1
    return v9

    .line 322
    :cond_2
    if-eqz v6, :cond_1

    .line 323
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_3
    move v1, v9

    .line 317
    goto :goto_0

    .line 318
    :catch_0
    move-exception v7

    .line 319
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "PicasaSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 322
    if-eqz v6, :cond_1

    .line 323
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 322
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_4

    .line 323
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method public static isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p0, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 161
    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 149
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :pswitch_0
    new-instance v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbumSet;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 146
    :goto_0
    return-object v0

    .line 126
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    const/4 v1, 0x6

    invoke-static {p1, v0, v2, v3, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    move-result-object v0

    goto :goto_0

    .line 130
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    const/4 v1, 0x2

    invoke-static {p1, v0, v2, v3, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    move-result-object v0

    goto :goto_0

    .line 134
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    const/4 v1, 0x4

    invoke-static {p1, v0, v2, v3, v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;JI)Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    move-result-object v0

    goto :goto_0

    .line 138
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->getLongVar(I)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;J)Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move-result-object v0

    goto :goto_0

    .line 142
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {p1, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeSet;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    goto :goto_0

    .line 146
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {p1, v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;->find(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    move-result-object v0

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method public findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 233
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 241
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 235
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v1, "PicasaSource"

    const-string/jumbo v2, "uri: "

    invoke-static {v1, v2, v0}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 6
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 246
    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 247
    .local v1, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v2, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 248
    check-cast v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    .line 249
    .local v0, "image":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    sget-object v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->ALBUM_PATH:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;->getAlbumId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/Path;->getChild(J)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 251
    .end local v0    # "image":Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V
    .locals 12
    .param p2, "consumer"    # Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSource$PathId;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 257
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSource$PathId;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/16 v11, 0x1f4

    if-ge v10, v11, :cond_1

    .line 258
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/remote/RemoteMediaSource;->mapMediaItems(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 290
    :cond_0
    return-void

    .line 263
    :cond_1
    sget-object v10, Lcom/sec/android/gallery3d/data/LocalSource;->sIdComparator:Ljava/util/Comparator;

    invoke-static {p1, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 265
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 266
    .local v8, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_0

    .line 268
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 269
    .local v4, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 271
    .local v0, "count":I
    move v6, v1

    .local v6, "j":I
    :goto_1
    if-ge v6, v8, :cond_2

    .line 272
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 273
    .local v9, "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget-object v10, v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->path:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->getSuffix()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 274
    .local v2, "curId":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    add-int/lit8 v0, v0, 0x1

    const/16 v10, 0x64

    if-ne v0, v10, :cond_3

    .line 280
    .end local v2    # "curId":J
    .end local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    :cond_2
    iget-object v10, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v10, v4}, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;->getMediaItemById(Lcom/sec/android/gallery3d/app/GalleryApp;Ljava/util/ArrayList;)[Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    .line 282
    .local v5, "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    move v7, v1

    .local v7, "k":I
    :goto_2
    if-ge v7, v6, :cond_4

    .line 283
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;

    .line 284
    .restart local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    iget v10, v9, Lcom/sec/android/gallery3d/data/MediaSource$PathId;->id:I

    sub-int v11, v7, v1

    aget-object v11, v5, v11

    invoke-interface {p2, v10, v11}, Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;->consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 282
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 271
    .end local v5    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v7    # "k":I
    .restart local v2    # "curId":J
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 288
    .end local v2    # "curId":J
    .end local v9    # "pid":Lcom/sec/android/gallery3d/data/MediaSource$PathId;
    .restart local v5    # "items":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v7    # "k":I
    :cond_4
    move v1, v6

    .line 289
    goto :goto_0
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 300
    return-void
.end method

.method public requestSyncAll()V
    .locals 4

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaService;->requestSync(Landroid/content/Context;IJ)V

    .line 332
    return-void
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 295
    return-void
.end method
