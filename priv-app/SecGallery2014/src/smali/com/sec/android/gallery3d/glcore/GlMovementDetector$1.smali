.class Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;
.super Lcom/sec/android/gallery3d/glcore/GlHandler;
.source "GlMovementDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/glcore/GlMovementDetector;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glcore/GlHandler;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public onMessage(ILjava/lang/Object;III)V
    .locals 2
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const/4 v1, 0x0

    .line 79
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->access$000(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->access$100(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;)Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    # getter for: Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mDetectListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->access$100(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;)Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    move-result-object v0

    invoke-interface {v0, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;->onLongClick(II)Z

    .line 82
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    # setter for: Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mClickActive:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->access$202(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;Z)Z

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlMovementDetector$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    # setter for: Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->mLongTmActive:Z
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->access$002(Lcom/sec/android/gallery3d/glcore/GlMovementDetector;Z)Z

    .line 86
    :cond_1
    return-void
.end method
