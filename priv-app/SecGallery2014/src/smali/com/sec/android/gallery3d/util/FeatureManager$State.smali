.class public final enum Lcom/sec/android/gallery3d/util/FeatureManager$State;
.super Ljava/lang/Enum;
.source "FeatureManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/util/FeatureManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/gallery3d/util/FeatureManager$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/gallery3d/util/FeatureManager$State;

.field public static final enum ACTIVE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

.field public static final enum DONE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

.field public static final enum FAILED:Lcom/sec/android/gallery3d/util/FeatureManager$State;

.field public static final enum IDLE:Lcom/sec/android/gallery3d/util/FeatureManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/util/FeatureManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->IDLE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    new-instance v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/util/FeatureManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->ACTIVE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    new-instance v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/gallery3d/util/FeatureManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->DONE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    new-instance v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/gallery3d/util/FeatureManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->FAILED:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/gallery3d/util/FeatureManager$State;

    sget-object v1, Lcom/sec/android/gallery3d/util/FeatureManager$State;->IDLE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/gallery3d/util/FeatureManager$State;->ACTIVE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/gallery3d/util/FeatureManager$State;->DONE:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/gallery3d/util/FeatureManager$State;->FAILED:Lcom/sec/android/gallery3d/util/FeatureManager$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->$VALUES:[Lcom/sec/android/gallery3d/util/FeatureManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/gallery3d/util/FeatureManager$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/gallery3d/util/FeatureManager$State;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/gallery3d/util/FeatureManager$State;->$VALUES:[Lcom/sec/android/gallery3d/util/FeatureManager$State;

    invoke-virtual {v0}, [Lcom/sec/android/gallery3d/util/FeatureManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/gallery3d/util/FeatureManager$State;

    return-object v0
.end method
