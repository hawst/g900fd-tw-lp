.class public Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;
.super Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;
.source "NinePatchTexture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NinePatchTexture"


# instance fields
.field private mChunk:Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

.field private mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    .line 41
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    .line 46
    return-void
.end method

.method private findInstance(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 108
    int-to-long v2, p2

    .line 109
    .local v2, "key":J
    const/16 v4, 0x20

    shl-long v4, v2, v4

    int-to-long v6, p3

    or-long v2, v4, v6

    .line 110
    iget-object v4, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;

    .line 112
    .local v0, "instance":Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    if-nez v0, :cond_0

    .line 113
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;

    .end local v0    # "instance":Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    invoke-direct {v0, p0, p2, p3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;-><init>(Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;II)V

    .line 114
    .restart local v0    # "instance":Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    iget-object v4, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v4, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;->getJustRemoved()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;

    .line 116
    .local v1, "removed":Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;->recycle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 121
    .end local v1    # "removed":Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "w"    # I
    .param p5, "h"    # I

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;->clear()V

    .line 130
    :cond_0
    if-eqz p4, :cond_1

    if-eqz p5, :cond_1

    .line 131
    invoke-direct {p0, p1, p4, p5}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->findInstance(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;

    move-result-object v0

    invoke-virtual {v0, p1, p0, p2, p3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;II)V

    .line 133
    :cond_1
    return-void
.end method

.method public getNinePatchChunk()Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mChunk:Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->onGetBitmap()Landroid/graphics/Bitmap;

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mChunk:Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

    return-object v0
.end method

.method public getPaddings()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mChunk:Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->onGetBitmap()Landroid/graphics/Bitmap;

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mChunk:Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;->mPaddings:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 50
    iget-object v4, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mBitmap:Landroid/graphics/Bitmap;

    .line 68
    :cond_0
    :goto_0
    return-object v3

    .line 52
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 53
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 54
    iget-object v4, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mResId:I

    invoke-static {v4, v5, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 56
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 59
    iput-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mBitmap:Landroid/graphics/Bitmap;

    .line 60
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->setSize(II)V

    .line 61
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v1

    .line 62
    .local v1, "chunkData":[B
    if-nez v1, :cond_2

    :goto_1
    iput-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mChunk:Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

    .line 65
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mChunk:Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

    if-nez v3, :cond_3

    .line 66
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid nine-patch image: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mResId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 62
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;->deserialize([B)Lcom/sec/android/gallery3d/glrenderer/NinePatchChunk;

    move-result-object v3

    goto :goto_1

    :cond_3
    move-object v3, v0

    .line 68
    goto :goto_0
.end method

.method public recycle()V
    .locals 4

    .prologue
    .line 137
    invoke-super {p0}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->recycle()V

    .line 138
    iget-object v0, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mCanvasRef:Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .line 139
    .local v0, "canvas":Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;

    .line 141
    .local v2, "instance":Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;->recycle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    goto :goto_1

    .line 143
    .end local v2    # "instance":Lcom/sec/android/gallery3d/glrenderer/NinePatchInstance;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->mInstanceCache:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture$MyCacheMap;->clear()V

    goto :goto_0
.end method
