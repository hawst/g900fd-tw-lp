.class public Lcom/sec/android/gallery3d/glcore/GlView;
.super Ljava/lang/Object;
.source "GlView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/glcore/GlView$1;,
        Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;
    }
.end annotation


# static fields
.field public static final ALIGN_BOTTOM:I = 0x3

.field public static final ALIGN_CENTER:I = 0x2

.field public static final ALIGN_LEFT:I = 0x1

.field public static final ALIGN_MIDDLE:I = 0x2

.field public static final ALIGN_NONE:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x3

.field public static final ALIGN_TOP:I = 0x1

.field private static final FLAG_INVISIBLE:I = 0x1

.field public static final INVALID:I = -0x1

.field public static final INVISIBLE:I = 0x1

.field public static final MATCH_PARENT:I = 0x1

.field public static final NOT_SPECIFIED:I = 0x0

.field public static final POS_SPECIFIED:I = 0x4

.field public static final SIZE_SPECIFIED:I = 0x2

.field public static final VISIBLE:I = 0x0

.field public static final WRAP_CONTENT:I = 0x8


# instance fields
.field public mAnimation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlAnimationBase;",
            ">;"
        }
    .end annotation
.end field

.field private mChild:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlView;",
            ">;"
        }
    .end annotation
.end field

.field public mCommand:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlCmd;",
            ">;"
        }
    .end annotation
.end field

.field public mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field public mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mHAlign:I

.field private mHFlag:I

.field public mID:I

.field private mMargineBottom:I

.field private mMargineLeft:I

.field private mMargineRight:I

.field private mMargineTop:I

.field private mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

.field protected mParent:Lcom/sec/android/gallery3d/glcore/GlView;

.field private mRatio:F

.field protected mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

.field private mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

.field private mTextChildCount:I

.field private mVAlign:I

.field private mVFlag:I

.field private mViewFlags:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 52
    iput-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 58
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    .line 59
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mHFlag:I

    .line 60
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mVFlag:I

    .line 61
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mHAlign:I

    .line 62
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mVAlign:I

    .line 63
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineLeft:I

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineRight:I

    .line 64
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineTop:I

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineBottom:I

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    .line 67
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    .line 68
    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    .line 70
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlRect;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlRect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    .line 71
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;-><init>(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlView$1;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    .line 563
    return-void
.end method

.method private removeOneChild(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 1
    .param p1, "child"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

    if-ne v0, p1, :cond_0

    .line 235
    :cond_0
    const/4 v0, -0x1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    .line 236
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mParent:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 237
    instance-of v0, p1, Lcom/sec/android/gallery3d/glcore/GlTextView;

    if-eqz v0, :cond_1

    .line 238
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    .line 239
    :cond_1
    return-void
.end method


# virtual methods
.method public addChild(Lcom/sec/android/gallery3d/glcore/GlView;)I
    .locals 2
    .param p1, "child"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    .line 172
    const/4 v0, 0x2

    .line 175
    .local v0, "id":I
    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mParent:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    .line 181
    :cond_1
    instance-of v1, p1, Lcom/sec/android/gallery3d/glcore/GlTextView;

    if-eqz v1, :cond_2

    .line 182
    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    .line 183
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 185
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mParent:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 186
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 187
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_3
    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    .line 191
    iget v1, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    return v1
.end method

.method public addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 3
    .param p1, "child"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "ID"    # I

    .prologue
    .line 153
    iget-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mParent:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    .line 159
    :cond_1
    instance-of v0, p1, Lcom/sec/android/gallery3d/glcore/GlTextView;

    if-eqz v0, :cond_2

    .line 160
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 163
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mParent:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 164
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 165
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addChild with ID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already used!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_3
    iput p2, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    .line 169
    return-void
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 582
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 459
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 460
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 461
    .local v3, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    .line 462
    .local v10, "action":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v0, :cond_0

    .line 463
    if-nez v10, :cond_2

    .line 464
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 465
    .local v1, "cancel":Landroid/view/MotionEvent;
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 466
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/glcore/GlView;Z)Z

    .line 467
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 477
    .end local v1    # "cancel":Landroid/view/MotionEvent;
    :cond_0
    if-nez v10, :cond_6

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->getChildCount()I

    move-result v0

    add-int/lit8 v11, v0, -0x1

    .local v11, "i":I
    :goto_0
    if-ltz v11, :cond_6

    .line 480
    invoke-virtual {p0, v11}, Lcom/sec/android/gallery3d/glcore/GlView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    .line 481
    .local v8, "child":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glcore/GlView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_5

    .line 479
    :cond_1
    add-int/lit8 v11, v11, -0x1

    goto :goto_0

    .line 469
    .end local v8    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v11    # "i":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

    const/4 v9, 0x0

    move-object v4, p0

    move-object v5, p1

    move v6, v2

    move v7, v3

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/glcore/GlView;Z)Z

    .line 470
    const/4 v0, 0x3

    if-eq v10, v0, :cond_3

    const/4 v0, 0x1

    if-ne v10, v0, :cond_4

    .line 472
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 474
    :cond_4
    const/4 v0, 0x1

    .line 488
    :goto_1
    return v0

    .line 482
    .restart local v8    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .restart local v11    # "i":I
    :cond_5
    const/4 v9, 0x1

    move-object v4, p0

    move-object v5, p1

    move v6, v2

    move v7, v3

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/glcore/GlView;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 483
    iput-object v8, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMotionTarget:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 484
    const/4 v0, 0x1

    goto :goto_1

    .line 488
    .end local v8    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v11    # "i":I
    :cond_6
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method protected dispatchTouchEvent(Landroid/view/MotionEvent;IILcom/sec/android/gallery3d/glcore/GlView;Z)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "child"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p5, "checkBounds"    # Z

    .prologue
    .line 441
    iget-object v0, p4, Lcom/sec/android/gallery3d/glcore/GlView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    .line 442
    .local v0, "childRect":Lcom/sec/android/gallery3d/glcore/GlRect;
    new-instance v2, Landroid/graphics/Rect;

    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    iget v5, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    iget v6, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    iget v7, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    iget v7, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    iget v8, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x1

    invoke-direct {v2, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 445
    .local v2, "rect":Landroid/graphics/Rect;
    iget v1, v2, Landroid/graphics/Rect;->left:I

    .line 446
    .local v1, "left":I
    iget v3, v2, Landroid/graphics/Rect;->top:I

    .line 447
    .local v3, "top":I
    if-eqz p5, :cond_0

    invoke-virtual {v2, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 448
    :cond_0
    neg-int v4, v1

    int-to-float v4, v4

    neg-int v5, v3

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 449
    invoke-virtual {p4, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 450
    int-to-float v4, v1

    int-to-float v5, v3

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 451
    const/4 v4, 0x1

    .line 455
    :goto_0
    return v4

    .line 453
    :cond_1
    int-to-float v4, v1

    int-to-float v5, v3

    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 455
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 7
    .param p1, "ID"    # I

    .prologue
    const/4 v5, 0x0

    .line 129
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    .line 130
    .local v0, "child":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlView;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    move-object v1, v5

    .line 144
    :cond_1
    :goto_0
    return-object v1

    .line 133
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 134
    .local v2, "childViewCnt":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v2, :cond_3

    .line 135
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlView;

    .line 136
    .local v1, "childView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget v6, v1, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    if-eq v6, p1, :cond_1

    .line 134
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 138
    .end local v1    # "childView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v2, :cond_5

    .line 139
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlView;

    .line 140
    .restart local v1    # "childView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    .line 141
    .local v3, "detectedView":Lcom/sec/android/gallery3d/glcore/GlView;
    if-eqz v3, :cond_4

    move-object v1, v3

    .line 142
    goto :goto_0

    .line 138
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .end local v1    # "childView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v3    # "detectedView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_5
    move-object v1, v5

    .line 144
    goto :goto_0
.end method

.method public getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlView;

    return-object v0
.end method

.method public getChildCount()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mH:I

    .line 422
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    return v0
.end method

.method protected getRoot()Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 2

    .prologue
    .line 570
    move-object v0, p0

    .line 571
    .local v0, "cur":Lcom/sec/android/gallery3d/glcore/GlView;
    :goto_0
    iget-object v1, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mParent:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v1, :cond_0

    .line 572
    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mParent:Lcom/sec/android/gallery3d/glcore/GlView;

    goto :goto_0

    .line 574
    :cond_0
    return-object v0
.end method

.method public getScaleRatio()F
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    return v0
.end method

.method public getVisibility()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mW:I

    .line 415
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    goto :goto_0
.end method

.method public hasTextChild()Z
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mTextChildCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v0, :cond_0

    .line 554
    :goto_0
    return-void

    .line 552
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedRender:Z

    .line 553
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->invalidate()V

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 578
    const/4 v0, 0x0

    return v0
.end method

.method public layout(II)V
    .locals 19
    .param p1, "parentWidth"    # I
    .param p2, "parentHeight"    # I

    .prologue
    .line 501
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    .line 502
    .local v4, "glReqAttr":Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    .line 503
    .local v3, "glRect":Lcom/sec/android/gallery3d/glcore/GlRect;
    monitor-enter v4

    .line 504
    :try_start_0
    iget-boolean v0, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 505
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    .line 506
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mHFlag:I

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x2

    if-lez v17, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    move/from16 v17, v0

    iget v0, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mW:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v16, v0

    .line 507
    .local v16, "w":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mVFlag:I

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x2

    if-lez v17, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    move/from16 v17, v0

    iget v0, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mH:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v5, v0

    .line 508
    .local v5, "h":I
    :goto_1
    move/from16 v0, v16

    invoke-virtual {v3, v0, v5}, Lcom/sec/android/gallery3d/glcore/GlRect;->SetSize(II)V

    .line 510
    .end local v5    # "h":I
    .end local v16    # "w":I
    :cond_0
    iget-boolean v0, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mPos:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 511
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mPos:Z

    .line 512
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineLeft:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v8, v0

    .line 513
    .local v8, "margineLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineRight:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v9, v0

    .line 514
    .local v9, "margineRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineTop:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v10, v0

    .line 515
    .local v10, "margineTop":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineBottom:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v7, v0

    .line 516
    .local v7, "margineBottom":I
    sub-int v17, p1, v8

    sub-int v15, v17, v9

    .line 517
    .local v15, "pWidth":I
    sub-int v17, p2, v10

    sub-int v14, v17, v7

    .line 519
    .local v14, "pHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mHAlign:I

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 520
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mHAlign:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 521
    add-int v17, v8, v15

    iget v0, v3, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    move/from16 v18, v0

    sub-int v12, v17, v18

    .line 528
    .local v12, "nx":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mVAlign:I

    move/from16 v17, v0

    if-eqz v17, :cond_a

    .line 529
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mVAlign:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    .line 530
    add-int v17, v10, v14

    iget v0, v3, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    move/from16 v18, v0

    sub-int v13, v17, v18

    .line 537
    .local v13, "ny":I
    :goto_3
    invoke-virtual {v3, v12, v13}, Lcom/sec/android/gallery3d/glcore/GlRect;->SetPos(II)V

    .line 540
    .end local v7    # "margineBottom":I
    .end local v8    # "margineLeft":I
    .end local v9    # "margineRight":I
    .end local v10    # "margineTop":I
    .end local v12    # "nx":I
    .end local v13    # "ny":I
    .end local v14    # "pHeight":I
    .end local v15    # "pWidth":I
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    const/4 v6, 0x0

    .local v6, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    if-nez v17, :cond_b

    const/4 v11, 0x0

    .local v11, "n":I
    :goto_4
    if-ge v6, v11, :cond_c

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlView;

    .line 543
    .local v2, "child":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlView;->hasTextChild()Z

    move-result v17

    if-nez v17, :cond_2

    .line 544
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v2, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    .line 545
    :cond_2
    iget v0, v3, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    move/from16 v17, v0

    iget v0, v3, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->layout(II)V

    .line 541
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .end local v2    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v6    # "i":I
    .end local v11    # "n":I
    :cond_3
    move/from16 v16, p1

    .line 506
    goto/16 :goto_0

    .restart local v16    # "w":I
    :cond_4
    move/from16 v5, p2

    .line 507
    goto/16 :goto_1

    .line 522
    .end local v16    # "w":I
    .restart local v7    # "margineBottom":I
    .restart local v8    # "margineLeft":I
    .restart local v9    # "margineRight":I
    .restart local v10    # "margineTop":I
    .restart local v14    # "pHeight":I
    .restart local v15    # "pWidth":I
    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mHAlign:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 523
    iget v0, v3, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    move/from16 v17, v0

    sub-int v17, v15, v17

    div-int/lit8 v17, v17, 0x2

    add-int v12, v8, v17

    .restart local v12    # "nx":I
    goto :goto_2

    .line 525
    .end local v12    # "nx":I
    :cond_6
    move v12, v8

    .restart local v12    # "nx":I
    goto :goto_2

    .line 527
    .end local v12    # "nx":I
    :cond_7
    move v12, v8

    .restart local v12    # "nx":I
    goto/16 :goto_2

    .line 531
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mVAlign:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 532
    iget v0, v3, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    move/from16 v17, v0

    sub-int v17, v14, v17

    div-int/lit8 v17, v17, 0x2

    add-int v13, v10, v17

    .restart local v13    # "ny":I
    goto :goto_3

    .line 534
    .end local v13    # "ny":I
    :cond_9
    move v13, v10

    .restart local v13    # "ny":I
    goto :goto_3

    .line 536
    .end local v13    # "ny":I
    :cond_a
    move v13, v10

    .restart local v13    # "ny":I
    goto/16 :goto_3

    .line 540
    .end local v7    # "margineBottom":I
    .end local v8    # "margineLeft":I
    .end local v9    # "margineRight":I
    .end local v10    # "margineTop":I
    .end local v12    # "nx":I
    .end local v13    # "ny":I
    .end local v14    # "pHeight":I
    .end local v15    # "pWidth":I
    :catchall_0
    move-exception v17

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v17

    .line 541
    .restart local v6    # "i":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v11

    goto/16 :goto_4

    .line 547
    .restart local v11    # "n":I
    :cond_c
    return-void
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 0
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 492
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 255
    return-void
.end method

.method protected onLayout(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 495
    return-void
.end method

.method public onSurfaceChanged(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 498
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 435
    const/4 v0, 0x0

    return v0
.end method

.method protected onVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 426
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->getChildCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 427
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 428
    .local v0, "child":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 429
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->onVisibilityChanged(I)V

    .line 426
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 432
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_1
    return-void
.end method

.method public removeAllChilds()V
    .locals 3

    .prologue
    .line 206
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 207
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->removeOneChild(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 210
    return-void
.end method

.method public removeAllExcept(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 4
    .param p1, "exceptView"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    .line 213
    if-nez p1, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeAllChilds()V

    .line 224
    :goto_0
    return-void

    .line 218
    :cond_0
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlView;->mID:I

    .line 219
    .local v0, "exId":I
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 220
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/glcore/GlView;->removeOneChild(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 222
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 223
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    goto :goto_0
.end method

.method public removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z
    .locals 2
    .param p1, "child"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    const/4 v0, 0x0

    .line 196
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 201
    :cond_0
    :goto_0
    return v0

    .line 197
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->removeOneChild(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 199
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeCommand(I)V
    .locals 3
    .param p1, "command"    # I

    .prologue
    const/4 v2, 0x0

    .line 405
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_0

    .line 409
    :goto_0
    return-void

    .line 406
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 407
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 408
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method protected render(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 242
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    .line 243
    .local v0, "glRect":Lcom/sec/android/gallery3d/glcore/GlRect;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 244
    iget v3, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    int-to-float v3, v3

    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 245
    iget v3, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    invoke-virtual {p1, v5, v5, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 247
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->onDraw(Landroid/graphics/Canvas;)V

    .line 248
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->getChildCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 249
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/gallery3d/glcore/GlView;->renderChild(Landroid/graphics/Canvas;Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 248
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 251
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 252
    return-void
.end method

.method protected renderChild(Landroid/graphics/Canvas;Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "child"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    .line 258
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glcore/GlView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-virtual {p2, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->render(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public requestLayout()V
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v0, :cond_0

    .line 561
    :goto_0
    return-void

    .line 559
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mNeedLayout:Z

    .line 560
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->invalidate()V

    goto :goto_0
.end method

.method public setAlign(II)V
    .locals 3
    .param p1, "hAlign"    # I
    .param p2, "vAlign"    # I

    .prologue
    .line 310
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    monitor-enter v1

    .line 311
    :try_start_0
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mHAlign:I

    .line 312
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mVAlign:I

    .line 313
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mPos:Z

    .line 314
    monitor-exit v1

    .line 315
    return-void

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 1
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 328
    iput-object p0, p1, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->mView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 329
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mAnimation:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mAnimation:Ljava/util/ArrayList;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mAnimation:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    :goto_0
    return-void

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method

.method public setChildVisibility(ILjava/util/ArrayList;)V
    .locals 5
    .param p1, "visibility"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p2, "exceptViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlView;>;"
    const/4 v3, 0x0

    .line 96
    .local v3, "needInvalidate":Z
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->getChildCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 97
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlView;

    .line 98
    .local v0, "child":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlView;->getVisibility()I

    move-result v4

    if-ne p1, v4, :cond_1

    .line 96
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    :cond_1
    if-nez p1, :cond_2

    .line 103
    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    and-int/lit8 v4, v4, -0x2

    iput v4, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    .line 107
    :goto_2
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->onVisibilityChanged(I)V

    .line 108
    const/4 v3, 0x1

    goto :goto_1

    .line 105
    :cond_2
    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    goto :goto_2

    .line 110
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_3
    if-eqz v3, :cond_4

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->invalidate()V

    .line 112
    :cond_4
    return-void
.end method

.method public setCommand(IIII)V
    .locals 2
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I

    .prologue
    .line 341
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIII)V

    .line 342
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_VIEW:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 343
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 344
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_1

    .line 345
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 346
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    :goto_0
    return-void

    .line 349
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setCommand(IIIILjava/lang/Object;)V
    .locals 6
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 356
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;)V

    .line 357
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_VIEW:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 358
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 359
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 361
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    .line 362
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setCommandDelayed(IIIIJ)V
    .locals 9
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "msecDelay"    # J

    .prologue
    .line 372
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p5

    .line 373
    .local v6, "expTime":J
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIIJ)V

    .line 374
    .local v1, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_VIEW:I

    iput v0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 375
    iput-object p0, v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 376
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 378
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    :goto_0
    return-void

    .line 381
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setCommandDelayed(IIIILjava/lang/Object;J)V
    .locals 8
    .param p1, "command"    # I
    .param p2, "parm1"    # I
    .param p3, "parm2"    # I
    .param p4, "parm3"    # I
    .param p5, "obj"    # Ljava/lang/Object;
    .param p6, "msecDelay"    # J

    .prologue
    .line 389
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v6, v2, p6

    .line 390
    .local v6, "expTime":J
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCmd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/glcore/GlCmd;-><init>(IIIILjava/lang/Object;J)V

    .line 391
    .local v0, "cmd":Lcom/sec/android/gallery3d/glcore/GlCmd;
    sget v1, Lcom/sec/android/gallery3d/glcore/GlCmd;->CMD_TYPE_VIEW:I

    iput v1, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mCmdType:I

    .line 392
    iput-object p0, v0, Lcom/sec/android/gallery3d/glcore/GlCmd;->mThis:Ljava/lang/Object;

    .line 393
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_1

    .line 394
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    .line 396
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mCommand:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    :goto_0
    return-void

    .line 398
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setCommand(Lcom/sec/android/gallery3d/glcore/GlCmd;)V

    goto :goto_0
.end method

.method public setMargine(IIII)V
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 318
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    monitor-enter v1

    .line 319
    :try_start_0
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineLeft:I

    .line 320
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineTop:I

    .line 321
    iput p3, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineRight:I

    .line 322
    iput p4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mMargineBottom:I

    .line 323
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mPos:Z

    .line 324
    monitor-exit v1

    .line 325
    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected setObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 5
    .param p1, "glObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v4, 0x1

    .line 266
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 267
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iput-boolean v4, v3, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mPos:Z

    .line 268
    iget-object v3, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iput-boolean v4, v3, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    .line 269
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->getChildCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 270
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 271
    .local v0, "childView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->setObject(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 269
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 273
    .end local v0    # "childView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    return-void
.end method

.method public setScaleRatio(F)V
    .locals 7
    .param p1, "ratio"    # F

    .prologue
    .line 280
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    monitor-enter v5

    .line 281
    :try_start_0
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    .line 282
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    const/4 v6, 0x1

    iput-boolean v6, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    .line 283
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    const/4 v6, 0x1

    iput-boolean v6, v4, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mPos:Z

    .line 284
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 287
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlView;

    .line 288
    .local v0, "child":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v3, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    .line 289
    .local v3, "req":Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;
    iget-object v5, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    monitor-enter v5

    .line 290
    :try_start_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlView;->hasTextChild()Z

    move-result v4

    if-nez v4, :cond_0

    .line 291
    iput p1, v0, Lcom/sec/android/gallery3d/glcore/GlView;->mRatio:F

    .line 293
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    .line 294
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mPos:Z

    .line 295
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 286
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 284
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v1    # "i":I
    .end local v2    # "n":I
    .end local v3    # "req":Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 286
    .restart local v1    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mChild:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto :goto_0

    .line 295
    .restart local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .restart local v2    # "n":I
    .restart local v3    # "req":Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;
    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    .line 297
    .end local v0    # "child":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v3    # "req":Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;
    :cond_2
    return-void
.end method

.method public setSize(II)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 300
    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    monitor-enter v1

    .line 301
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iput p1, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mW:I

    .line 302
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    iput p2, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mH:I

    .line 303
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mReq:Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/sec/android/gallery3d/glcore/GlView$GlReqAttr;->mSize:Z

    .line 304
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mVFlag:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mVFlag:I

    .line 305
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mHFlag:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mHFlag:I

    .line 306
    monitor-exit v1

    .line 307
    return-void

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->getVisibility()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 75
    :cond_0
    if-nez p1, :cond_1

    .line 76
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    .line 80
    :goto_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->onVisibilityChanged(I)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/glcore/GlView;->invalidate()V

    goto :goto_0

    .line 78
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlView;->mViewFlags:I

    goto :goto_1
.end method
