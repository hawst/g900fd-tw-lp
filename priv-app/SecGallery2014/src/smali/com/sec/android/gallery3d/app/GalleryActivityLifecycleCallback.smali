.class public Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;
.super Ljava/lang/Object;
.source "GalleryActivityLifecycleCallback.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivityCount:I

.field private mAppContext:Landroid/content/Context;

.field private mHdmiReceiver:Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mHdmiReceiver:Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;

    .line 28
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mAppContext:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 33
    iget v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    .line 34
    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "created : count "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    if-ne v4, v7, :cond_2

    .line 37
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    .line 38
    .local v2, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v1, 0x0

    .line 39
    .local v1, "calledNearbyFramework":Z
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mAppContext:Landroid/content/Context;

    const-string v5, "ChangePlayerWifiDataAlertDialogOff"

    invoke-static {v4, v5, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mAppContext:Landroid/content/Context;

    const-string v5, "ScanNearbyDeviceWifiDataAlertDialogOff"

    invoke-static {v4, v5, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_4

    .line 41
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 42
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 44
    const-string v4, "com.samsung.android.sconnect.action.IMAGE_DMR"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 45
    :cond_0
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 46
    const/4 v1, 0x1

    .line 54
    .end local v0    # "action":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    if-ne v1, v7, :cond_2

    .line 55
    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->TAG:Ljava/lang/String;

    const-string v5, "calledNearbyFramework = true"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    .end local v1    # "calledNearbyFramework":Z
    .end local v2    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_2
    iget v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    if-ne v4, v7, :cond_3

    .line 62
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mAppContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mHdmiReceiver:Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;

    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->getHdmiIntentFilter()Landroid/content/IntentFilter;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 64
    :cond_3
    return-void

    .line 50
    .restart local v1    # "calledNearbyFramework":Z
    .restart local v2    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_4
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 51
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 68
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    .line 69
    sget-object v1, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "destroyed : count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mActivityCount:I

    if-nez v1, :cond_0

    .line 73
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 74
    .local v0, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->stopNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 75
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->deletePreviousCaches(Landroid/content/Context;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivityLifecycleCallback;->mHdmiReceiver:Lcom/sec/android/gallery3d/util/HdmiUtils$HdmiReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 78
    .end local v0    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_0
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 82
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 86
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 94
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 98
    return-void
.end method
