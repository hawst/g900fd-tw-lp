.class public Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "GallerySearchClusterAlbumSet.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchClusterAlbumSet"

.field public static final mUseFaceAlbum:Z


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

.field private mKind:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p3, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "kind"    # I

    .prologue
    .line 33
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .line 34
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 35
    iput p4, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mKind:I

    .line 36
    invoke-virtual {p3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 37
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->initClusterAlbumSet()V

    .line 38
    return-void
.end method

.method private initClusterAlbumSet()V
    .locals 5

    .prologue
    .line 41
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "basePath":Ljava/lang/String;
    const/high16 v2, 0x80000

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aput-object v2, v3, v4

    .line 51
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    .line 53
    return-void
.end method


# virtual methods
.method public getAlbums()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/ClusterAlbum;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/ClusterAlbum;>;"
    const/4 v2, 0x0

    .line 114
    .local v2, "size":I
    const/4 v2, 0x1

    .line 116
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 117
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getAlbums()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    return-object v1
.end method

.method public getAllSet()Lcom/sec/android/gallery3d/data/ClusterAlbumSet;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getApplication()Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method public getClusterKind()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mKind:I

    return v0
.end method

.method public getFacesSet()Lcom/sec/android/gallery3d/data/ClusterAlbumSet;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const-string v0, "SearchClusterAlbumSet"

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->getSubMediaSetCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 70
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->getAlbums()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    goto :goto_0
.end method

.method public getSubMediaSetCount()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->notifyContentChanged()V

    .line 106
    return-void
.end method

.method public reload()J
    .locals 8

    .prologue
    .line 87
    const/4 v1, 0x0

    .line 88
    .local v1, "isDirty":Z
    const/4 v4, 0x0

    .line 92
    .local v4, "size":I
    const/4 v4, 0x1

    .line 94
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 95
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->getDataVersion()J

    move-result-wide v2

    .line 96
    .local v2, "oldVersion":J
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mClusterAlbumSets:[Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->reload()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-lez v5, :cond_0

    .line 97
    const/4 v1, 0x1

    .line 94
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    .end local v2    # "oldVersion":J
    :cond_1
    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->nextVersionNumber()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mDataVersion:J

    :goto_1
    return-wide v6

    :cond_2
    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/GallerySearchClusterAlbumSet;->mDataVersion:J

    goto :goto_1
.end method
