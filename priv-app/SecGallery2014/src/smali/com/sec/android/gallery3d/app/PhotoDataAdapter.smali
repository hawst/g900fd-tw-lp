.class public Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
.super Ljava/lang/Object;
.source "PhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;
.implements Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailListener;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateContent;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$GetUpdateInfo;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$UpdateInfo;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;,
        Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;
    }
.end annotation


# static fields
.field private static final BIT_FULL_IMAGE:I = 0x2

.field private static final BIT_SCREEN_NAIL:I = 0x1

.field private static final DATA_CACHE_SIZE:I = 0x28

.field private static final IMAGE_CACHE_SIZE:I = 0x7

.field private static final MIN_LOAD_COUNT:I = 0x10

.field private static final MSG_LOAD_CANCEL:I = 0x5

.field private static final MSG_LOAD_FINISH:I = 0x2

.field private static final MSG_LOAD_START:I = 0x1

.field private static final MSG_RUN_OBJECT:I = 0x3

.field private static final MSG_UPDATE_IMAGE_REQUESTS:I = 0x4

.field private static final SCREEN_NAIL_MAX:I = 0x3

.field private static final TAG:Ljava/lang/String; = "PhotoDataAdapter"

.field private static final VERSION_OUT_OF_RANGE:J


# instance fields
.field private mAGIF:Lcom/quramsoft/agif/QuramAGIF;

.field private mActiveEnd:I

.field private mActiveStart:I

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

.field private mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field private mBrokenImage:Landroid/graphics/Bitmap;

.field private mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

.field private mCameraIndex:I

.field private final mChanges:[J

.field private mContentEnd:I

.field private mContentStart:I

.field private mCurrentIndex:I

.field private final mData:[Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

.field public mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field private final mFacePaths:[Lcom/sec/android/gallery3d/data/Path;

.field private mFirstLoading:Z

.field private mFocusHintDirection:I

.field private mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;

.field private mImageCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

.field public mInitRotation:I

.field public mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

.field private mIsActive:Z

.field private mIsAlive:Z

.field private mIsPanorama:Z

.field private mIsRequestPostGalleryCmd:Z

.field private mIsStaticCamera:Z

.field private mItemPath:Lcom/sec/android/gallery3d/data/Path;

.field private mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

.field private final mMainHandler:Landroid/os/Handler;

.field private mNeedFullImage:Z

.field private final mPaths:[Lcom/sec/android/gallery3d/data/Path;

.field private final mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

.field private mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

.field private mRoadingState:I

.field private mSize:I

.field private mSource:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mSourceVersion:J

.field private final mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private final mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

.field private mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

.field private final mUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 105
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaObject;->nextVersionNumber()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->VERSION_OUT_OF_RANGE:J

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/Path;IIZZLcom/sec/android/gallery3d/app/AlbumReloader;)V
    .locals 4
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p3, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "itemPath"    # Lcom/sec/android/gallery3d/data/Path;
    .param p5, "indexHint"    # I
    .param p6, "cameraIndex"    # I
    .param p7, "isPanorama"    # Z
    .param p8, "isStaticCamera"    # Z
    .param p9, "albumReloader"    # Lcom/sec/android/gallery3d/app/AlbumReloader;

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    .line 165
    const/16 v0, 0x28

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    .line 166
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    .line 167
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFirstLoading:Z

    .line 175
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    .line 177
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    .line 178
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    .line 187
    const/4 v0, 0x7

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    .line 190
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/sec/android/gallery3d/data/Path;

    .line 191
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFacePaths:[Lcom/sec/android/gallery3d/data/Path;

    .line 202
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    .line 203
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    .line 211
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 215
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitRotation:I

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 242
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mRoadingState:I

    .line 244
    new-instance v0, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    .line 256
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 257
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->initImageFetchSeq()V

    .line 260
    invoke-static {p3}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 261
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PhotoView;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 262
    invoke-static {p4}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/Path;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 263
    iput p5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    .line 264
    iput p6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    .line 265
    iput-boolean p7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsPanorama:Z

    .line 266
    iput-boolean p8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsStaticCamera:Z

    .line 267
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFirstLoading:Z

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsAlive:Z

    .line 272
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 274
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    .line 276
    iput-object p9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    .line 277
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->setDataListener(Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadListener;)V

    .line 279
    new-instance v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$1;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    .line 319
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 320
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getCacheInterface()Lcom/sec/samsung/gallery/decoder/CacheInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 321
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 323
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    .line 324
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsAlive:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateScreenNail(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)[Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFirstLoading:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFirstLoading:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AlbumReloader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    return-wide v0
.end method

.method static synthetic access$2002(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 87
    iput-wide p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSourceVersion:J

    return-wide p1
.end method

.method static synthetic access$2100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isTemporaryItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getReloadInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Ljava/util/concurrent/Callable;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/data/Path;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateMicroThumbnail(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getTiledTextureResource()Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/util/Future;
    .param p3, "x3"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateFullImage(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;Lcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->showRootView()V

    return-void
.end method

.method private executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    const/4 v2, 0x0

    .line 1391
    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 1392
    .local v1, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<TT;>;"
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1394
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 1403
    :goto_0
    return-object v2

    .line 1395
    :catch_0
    move-exception v0

    .line 1396
    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_0

    .line 1397
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 1398
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    goto :goto_0

    .line 1401
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v0

    .line 1402
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private fireDataChange()V
    .locals 14

    .prologue
    .line 361
    const/4 v1, 0x0

    .line 362
    .local v1, "changed":Z
    const/4 v3, -0x3

    .local v3, "i":I
    :goto_0
    const/4 v10, 0x3

    if-gt v3, v10, :cond_1

    .line 363
    iget v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v10, v3

    invoke-direct {p0, v10}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getVersion(I)J

    move-result-wide v6

    .line 364
    .local v6, "newVersion":J
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v11, v3, 0x3

    aget-wide v10, v10, v11

    cmp-long v10, v10, v6

    if-eqz v10, :cond_0

    .line 365
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mChanges:[J

    add-int/lit8 v11, v3, 0x3

    aput-wide v6, v10, v11

    .line 366
    const/4 v1, 0x1

    .line 362
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 370
    .end local v6    # "newVersion":J
    :cond_1
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setScrolling(Z)V

    .line 373
    if-nez v1, :cond_2

    .line 433
    :goto_1
    return-void

    .line 378
    :cond_2
    const/4 v0, 0x7

    .line 379
    .local v0, "N":I
    const/4 v10, 0x7

    new-array v2, v10, [I

    .line 382
    .local v2, "fromIndex":[I
    const/4 v10, 0x7

    new-array v8, v10, [Lcom/sec/android/gallery3d/data/Path;

    .line 383
    .local v8, "oldPaths":[Lcom/sec/android/gallery3d/data/Path;
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/sec/android/gallery3d/data/Path;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x7

    invoke-static {v10, v11, v8, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 385
    const/4 v10, 0x7

    new-array v5, v10, [Lcom/sec/android/gallery3d/data/Path;

    .line 386
    .local v5, "oldFacePaths":[Lcom/sec/android/gallery3d/data/Path;
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFacePaths:[Lcom/sec/android/gallery3d/data/Path;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x7

    invoke-static {v10, v11, v5, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 389
    const/4 v3, 0x0

    :goto_2
    const/4 v10, 0x7

    if-ge v3, v10, :cond_3

    .line 390
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/sec/android/gallery3d/data/Path;

    iget v11, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v11, v3

    add-int/lit8 v11, v11, -0x3

    invoke-direct {p0, v11}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v11

    aput-object v11, v10, v3

    .line 391
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFacePaths:[Lcom/sec/android/gallery3d/data/Path;

    iget v11, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v11, v3

    add-int/lit8 v11, v11, -0x3

    invoke-direct {p0, v11}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getFacePath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v11

    aput-object v11, v10, v3

    .line 389
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 394
    :cond_3
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v10

    sget-object v11, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v10, v11, :cond_8

    .line 396
    const/4 v3, 0x0

    :goto_3
    const/4 v10, 0x7

    if-ge v3, v10, :cond_d

    .line 397
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFacePaths:[Lcom/sec/android/gallery3d/data/Path;

    aget-object v9, v10, v3

    .line 398
    .local v9, "p":Lcom/sec/android/gallery3d/data/Path;
    if-nez v9, :cond_4

    .line 399
    const v10, 0x7fffffff

    aput v10, v2, v3

    .line 396
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 405
    :cond_4
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_5
    const/4 v10, 0x7

    if-ge v4, v10, :cond_5

    .line 406
    aget-object v10, v5, v4

    if-ne v10, v9, :cond_6

    .line 410
    :cond_5
    const/4 v10, 0x7

    if-ge v4, v10, :cond_7

    add-int/lit8 v10, v4, -0x3

    :goto_6
    aput v10, v2, v3

    goto :goto_4

    .line 405
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 410
    :cond_7
    const v10, 0x7fffffff

    goto :goto_6

    .line 414
    .end local v4    # "j":I
    .end local v9    # "p":Lcom/sec/android/gallery3d/data/Path;
    :cond_8
    const/4 v3, 0x0

    :goto_7
    const/4 v10, 0x7

    if-ge v3, v10, :cond_d

    .line 415
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPaths:[Lcom/sec/android/gallery3d/data/Path;

    aget-object v9, v10, v3

    .line 416
    .restart local v9    # "p":Lcom/sec/android/gallery3d/data/Path;
    if-nez v9, :cond_9

    .line 417
    const v10, 0x7fffffff

    aput v10, v2, v3

    .line 414
    :goto_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 423
    :cond_9
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_9
    const/4 v10, 0x7

    if-ge v4, v10, :cond_a

    .line 424
    aget-object v10, v8, v4

    if-ne v10, v9, :cond_b

    .line 428
    :cond_a
    const/4 v10, 0x7

    if-ge v4, v10, :cond_c

    add-int/lit8 v10, v4, -0x3

    :goto_a
    aput v10, v2, v3

    goto :goto_8

    .line 423
    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 428
    :cond_c
    const v10, 0x7fffffff

    goto :goto_a

    .line 431
    .end local v4    # "j":I
    .end local v9    # "p":Lcom/sec/android/gallery3d/data/Path;
    :cond_d
    iget-object v10, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget v11, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    neg-int v11, v11

    iget v12, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v12, v12, -0x1

    iget v13, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int/2addr v12, v13

    invoke-virtual {v10, v2, v11, v12}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyDataChange([III)V

    goto/16 :goto_1
.end method

.method private getFacePath(I)Lcom/sec/android/gallery3d/data/Path;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2168
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 2169
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 2170
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFacePath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0
.end method

.method private getItemInternal(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 327
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-lt p1, v1, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-object v0

    .line 328
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v1, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v1, p1, 0x28

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method private getPath(I)Lcom/sec/android/gallery3d/data/Path;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItemInternal(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 355
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 356
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_0
.end method

.method private getReloadInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;
    .locals 1

    .prologue
    .line 1594
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->getLatestUpdateInfo()Lcom/sec/android/gallery3d/app/AlbumReloader$ReloadInfo;

    move-result-object v0

    return-object v0
.end method

.method private getTiledTextureResource()Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;
    .locals 1

    .prologue
    .line 2196
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    if-nez v0, :cond_0

    .line 2197
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    .line 2199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTiledTextureResources:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$TiledTextureResources;

    return-object v0
.end method

.method private getVersion(I)J
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 339
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-lt p1, v1, :cond_1

    .line 340
    :cond_0
    sget-wide v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->VERSION_OUT_OF_RANGE:J

    .line 349
    :goto_0
    return-wide v2

    .line 342
    :cond_1
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v1, :cond_2

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v1, :cond_2

    .line 343
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v2, p1, 0x28

    aget-object v0, v1, v2

    .line 345
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_2

    .line 346
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion(I)J

    move-result-wide v2

    goto :goto_0

    .line 349
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method private initImageFetchSeq()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 127
    const/4 v2, 0x0

    .line 128
    .local v2, "k":I
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 129
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->getMTNCachingSize()I

    move-result v0

    .line 130
    .local v0, "cacheSize":I
    add-int/lit8 v4, v0, -0x1

    mul-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    new-array v4, v4, [Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    iput-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    .line 132
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "k":I
    .local v3, "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v2

    .line 133
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 134
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "k":I
    .restart local v2    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v5, v1, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v3

    .line 135
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "k":I
    .restart local v3    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    neg-int v6, v1

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v2

    .line 133
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v2, v3

    .line 150
    .end local v0    # "cacheSize":I
    .end local v3    # "k":I
    .restart local v2    # "k":I
    :goto_1
    return-void

    .line 138
    .end local v1    # "i":I
    :cond_1
    const/16 v4, 0x10

    new-array v4, v4, [Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    iput-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    .line 139
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "k":I
    .restart local v3    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v2

    .line 141
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "k":I
    .restart local v2    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v5, v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v3

    .line 142
    const/4 v1, 0x1

    .restart local v1    # "i":I
    move v3, v2

    .end local v2    # "k":I
    .restart local v3    # "k":I
    :goto_2
    const/4 v4, 0x7

    if-ge v1, v4, :cond_2

    .line 143
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "k":I
    .restart local v2    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v5, v1, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v3

    .line 144
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "k":I
    .restart local v3    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    neg-int v6, v1

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v2

    .line 142
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 147
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "k":I
    .restart local v2    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v5, v7, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v3

    .line 148
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "k":I
    .restart local v3    # "k":I
    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    const/4 v6, -0x1

    invoke-direct {v5, v6, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v5, v4, v2

    move v2, v3

    .end local v3    # "k":I
    .restart local v2    # "k":I
    goto :goto_1
.end method

.method private isScreenNailAvailable(Lcom/sec/android/gallery3d/ui/ScreenNail;)Z
    .locals 3
    .param p1, "screenNail"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2203
    instance-of v2, p1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-eqz v2, :cond_2

    .line 2204
    check-cast p1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    .end local p1    # "screenNail":Lcom/sec/android/gallery3d/ui/ScreenNail;
    iget-object v2, p1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 2208
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2204
    goto :goto_0

    .line 2205
    .restart local p1    # "screenNail":Lcom/sec/android/gallery3d/ui/ScreenNail;
    :cond_2
    instance-of v2, p1, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v2, :cond_3

    .line 2206
    check-cast p1, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    .end local p1    # "screenNail":Lcom/sec/android/gallery3d/ui/ScreenNail;
    iget-object v2, p1, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->mTexture:Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .restart local p1    # "screenNail":Lcom/sec/android/gallery3d/ui/ScreenNail;
    :cond_3
    move v0, v1

    .line 2208
    goto :goto_0
.end method

.method private isTemporaryItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 6
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 1132
    iget v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-gez v2, :cond_1

    .line 1143
    :cond_0
    :goto_0
    return v1

    .line 1134
    :cond_1
    instance-of v2, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 1135
    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 1136
    .local v0, "item":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v2

    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-ne v2, v3, :cond_0

    .line 1138
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getSize()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1139
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1140
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1142
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getDateInMs()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 1143
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private newPlaceholderScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 5
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1150
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v1

    .line 1151
    .local v1, "width":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v0

    .line 1153
    .local v0, "height":I
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseBufferThumbnailMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1154
    new-instance v2, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    iget v4, v4, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BUFFER_TYPE_MTN:I

    invoke-direct {v2, v3, v1, v0, v4}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;-><init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;III)V

    .line 1156
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    invoke-direct {v2, v1, v0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;-><init>(II)V

    goto :goto_0
.end method

.method private showRootView()V
    .locals 3

    .prologue
    .line 2161
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW_WITH_DELAY:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    aput-object v2, v0, v1

    .line 2164
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "HIDE_ROOT_VIEW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2165
    return-void
.end method

.method private startTaskIfNeeded(II)Lcom/sec/android/gallery3d/util/Future;
    .locals 10
    .param p1, "index"    # I
    .param p2, "which"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/sec/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 1162
    iget v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p1, v5, :cond_0

    iget v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt p1, v5, :cond_1

    .line 1203
    :cond_0
    :goto_0
    return-object v4

    .line 1165
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1166
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 1169
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v6, p1, 0x28

    aget-object v1, v5, v6

    .line 1172
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_2

    .line 1173
    const-string v5, "PhotoDataAdapter"

    const-string v6, "item is null! at startTaskIfNeeded"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1177
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v2

    .line 1179
    .local v2, "version":J
    if-ne p2, v8, :cond_3

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v5, :cond_3

    iget-wide v6, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    cmp-long v5, v6, v2

    if-nez v5, :cond_3

    .line 1181
    iget-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0

    .line 1182
    :cond_3
    if-ne p2, v9, :cond_4

    iget-wide v6, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    cmp-long v5, v6, v2

    if-nez v5, :cond_4

    .line 1183
    iget-object v5, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v5, :cond_4

    .line 1184
    iget-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0

    .line 1188
    :cond_4
    if-ne p2, v8, :cond_6

    iget-wide v6, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    cmp-long v5, v6, v2

    if-eqz v5, :cond_6

    .line 1189
    iput-wide v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    .line 1190
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseBufferThumbnailMode()Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    invoke-direct {v4, p0, v6, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailBufferJob;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/CacheInterface;Lcom/sec/android/gallery3d/data/MediaItem;)V

    :goto_1
    new-instance v6, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailListener;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1194
    iget-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0

    .line 1190
    :cond_5
    new-instance v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ScreenNailJob;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_1

    .line 1196
    :cond_6
    if-ne p2, v9, :cond_0

    iget-wide v6, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    cmp-long v5, v6, v2

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v6

    const-wide/16 v8, 0x40

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 1197
    iput-wide v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    .line 1198
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;

    invoke-direct {v5, p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V

    new-instance v6, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1201
    iget-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    goto/16 :goto_0
.end method

.method private updateCurrentIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 626
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    if-eq v1, p1, :cond_0

    if-gez p1, :cond_1

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    .line 631
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    .line 633
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v2, p1, 0x28

    aget-object v0, v1, v2

    .line 634
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_3

    const/4 v1, 0x0

    :goto_1
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 636
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    .line 637
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    .line 639
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->resetBrokenImage()V

    .line 641
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    .line 643
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    if-eqz v1, :cond_2

    .line 644
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-interface {v1, p1, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;->onPhotoChanged(ILcom/sec/android/gallery3d/data/Path;)V

    .line 647
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    goto :goto_0

    .line 634
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_1
.end method

.method private updateCurrentIndexFastest(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 651
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    if-eq v1, p1, :cond_0

    if-gez p1, :cond_1

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    iput p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    .line 656
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    .line 657
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v2, p1, 0x28

    aget-object v0, v1, v2

    .line 658
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_2

    const/4 v1, 0x0

    :goto_1
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 660
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    .line 661
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    .line 662
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider()V

    .line 663
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    goto :goto_0

    .line 658
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    goto :goto_1
.end method

.method private updateFullImage(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 11
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    const/4 v10, 0x1

    .line 499
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 501
    .local v8, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-nez v8, :cond_1

    .line 503
    invoke-interface {p2}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 504
    .local v9, "fullImage":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->recycle()V

    .line 535
    .end local v9    # "fullImage":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    const/4 v0, 0x0

    iput-object v0, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    .line 509
    invoke-interface {p2}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    iput-object v0, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 510
    iget-object v0, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-eqz v0, :cond_4

    .line 512
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_2

    .line 513
    iget-object v0, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v0, :cond_2

    .line 514
    iget-object v0, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    if-nez v0, :cond_2

    .line 515
    new-instance v0, Lcom/sec/samsung/gallery/decoder/LargeImage;

    iget-object v1, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    move-object v3, p3

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/decoder/LargeImage;-><init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;)V

    iput-object v0, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    .line 519
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    if-eqz v0, :cond_3

    .line 520
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    invoke-interface {v0, p1, v10}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;->onPhotoAvailable(Lcom/sec/android/gallery3d/data/Path;Z)V

    .line 523
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    if-ne p1, v0, :cond_4

    .line 525
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v0, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateNearbyEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 526
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v0, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTCloudEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 528
    invoke-direct {p0, v8}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 531
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 534
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    goto :goto_0
.end method

.method private updateImageCache()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1207
    new-instance v6, Ljava/util/HashSet;

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1208
    .local v6, "toBeRemoved":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/android/gallery3d/data/Path;>;"
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    .local v1, "i":I
    :goto_0
    iget v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v7, :cond_c

    .line 1209
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v8, v1, 0x28

    aget-object v3, v7, v8

    .line 1210
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v3, :cond_1

    .line 1208
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1213
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    .line 1214
    .local v4, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1215
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    invoke-virtual {v6, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1216
    if-eqz v0, :cond_a

    .line 1217
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getVersion(I)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->faceVersion:J

    .line 1219
    iget v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v7, v13, :cond_5

    .line 1220
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v7, :cond_2

    .line 1221
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 1222
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1224
    :cond_2
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    if-eqz v7, :cond_3

    .line 1225
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    invoke-virtual {v7, v13}, Lcom/sec/samsung/gallery/decoder/LargeImage;->stopDecodeThread(Z)V

    .line 1227
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    .line 1229
    :cond_3
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-eqz v7, :cond_4

    .line 1230
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->requestCanceDecode()V

    .line 1231
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->recycle()V

    .line 1232
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 1234
    :cond_4
    const-wide/16 v8, -0x1

    iput-wide v8, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    .line 1238
    :cond_5
    iget v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    const/4 v8, 0x3

    if-le v7, v8, :cond_7

    .line 1239
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v7, :cond_6

    .line 1240
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 1241
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1243
    :cond_6
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_7

    .line 1244
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    .line 1245
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 1251
    :cond_7
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v7, :cond_8

    .line 1252
    iget v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    sub-int v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    const/4 v8, 0x2

    if-le v7, v8, :cond_8

    .line 1253
    iget-boolean v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedAvailable:Z

    if-eqz v7, :cond_8

    .line 1254
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v7}, Landroid/util/LongSparseArray;->clear()V

    .line 1255
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    .line 1256
    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedAvailable:Z

    .line 1262
    :cond_8
    iget-wide v8, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 1265
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v7, v7, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-eqz v7, :cond_9

    .line 1266
    iget-object v5, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v5, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    .line 1267
    .local v5, "s":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v7

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->updatePlaceholderSize(II)V

    goto/16 :goto_1

    .line 1269
    .end local v5    # "s":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    :cond_9
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v7, v7, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v7, :cond_0

    .line 1270
    iget-object v5, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v5, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    .line 1271
    .local v5, "s":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v7

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->updatePlaceholderSize(II)V

    goto/16 :goto_1

    .line 1277
    .end local v5    # "s":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    :cond_a
    new-instance v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .end local v0    # "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    invoke-direct {v0, v12}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$1;)V

    .line 1279
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v3, :cond_b

    .line 1280
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v7

    iput v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    .line 1281
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getVersion(I)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->faceVersion:J

    .line 1284
    :cond_b
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1289
    .end local v0    # "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_c
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_d
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/Path;

    .line 1290
    .restart local v4    # "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1292
    .restart local v0    # "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v7, :cond_e

    .line 1293
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 1295
    :cond_e
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    if-eqz v7, :cond_f

    .line 1296
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    invoke-virtual {v7, v13}, Lcom/sec/samsung/gallery/decoder/LargeImage;->stopDecodeThread(Z)V

    .line 1298
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    .line 1300
    :cond_f
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_10

    .line 1301
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    .line 1302
    iput-object v12, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 1306
    :cond_10
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v7, :cond_11

    .line 1307
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 1309
    :cond_11
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v7, :cond_d

    .line 1310
    iget-object v7, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    goto :goto_2

    .line 1314
    .end local v0    # "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    .end local v4    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_12
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateScreenNailUploadQueue()V

    .line 1315
    return-void
.end method

.method private updateMicroThumbnail(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 5
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/android/gallery3d/ui/ScreenNail;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2118
    .local p2, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Lcom/sec/android/gallery3d/ui/ScreenNail;>;"
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 2119
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    invoke-interface {p2}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 2121
    .local v2, "microThumbnail":Lcom/sec/android/gallery3d/ui/ScreenNail;
    if-nez v0, :cond_1

    .line 2122
    if-eqz v2, :cond_0

    .line 2123
    invoke-interface {v2}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    .line 2157
    :cond_0
    :goto_0
    return-void

    .line 2128
    :cond_1
    const/4 v4, 0x0

    iput-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    .line 2131
    iget-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v4, v4, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v4, :cond_2

    .line 2132
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v3, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    .line 2133
    .local v3, "original":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->combine(Lcom/sec/android/gallery3d/ui/ScreenNail;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    .line 2136
    .end local v3    # "original":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    :cond_2
    if-eqz v2, :cond_3

    .line 2137
    iput-object v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 2141
    :cond_3
    if-eqz v2, :cond_0

    .line 2142
    iput-object v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 2144
    const/4 v1, -0x3

    .local v1, "i":I
    :goto_1
    const/4 v4, 0x3

    if-gt v1, v4, :cond_0

    .line 2145
    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v4, v1

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    if-ne p1, v4, :cond_5

    .line 2146
    if-nez v1, :cond_4

    .line 2147
    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateNearbyEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 2148
    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTCloudEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 2149
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    .line 2151
    :cond_4
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    goto :goto_0

    .line 2144
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private updateNearbyEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    .locals 3
    .param p1, "index"    # I
    .param p2, "entry"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .prologue
    .line 1752
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1753
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-eqz v1, :cond_0

    .line 1754
    iget-object v1, p2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1756
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v1

    iput v1, p2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    .line 1759
    :cond_0
    return-object p2
.end method

.method private updateScreenNail(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Lcom/sec/android/gallery3d/ui/ScreenNail;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Lcom/sec/android/gallery3d/ui/ScreenNail;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 440
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 441
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    invoke-interface {p2}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 443
    .local v3, "screenNail":Lcom/sec/android/gallery3d/ui/ScreenNail;
    if-nez v0, :cond_1

    .line 444
    if-eqz v3, :cond_0

    .line 445
    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    const/4 v4, 0x0

    iput-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    .line 453
    iget-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v4, v4, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-eqz v4, :cond_6

    .line 454
    iget-object v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v2, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    .line 455
    .local v2, "original":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->combine(Lcom/sec/android/gallery3d/ui/ScreenNail;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    .line 464
    .end local v2    # "original":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    :cond_2
    :goto_1
    if-nez v3, :cond_7

    .line 465
    iput-boolean v6, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    .line 476
    :cond_3
    :goto_2
    const/4 v1, -0x3

    .local v1, "i":I
    :goto_3
    const/4 v4, 0x3

    if-gt v1, v4, :cond_5

    .line 477
    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v4, v1

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    if-ne p1, v4, :cond_8

    .line 478
    if-nez v1, :cond_4

    .line 480
    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateNearbyEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 481
    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTCloudEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 483
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    .line 485
    :cond_4
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 489
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    .line 490
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateScreenNailUploadQueue()V

    .line 492
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsRequestPostGalleryCmd:Z

    if-nez v4, :cond_0

    .line 493
    iput-boolean v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsRequestPostGalleryCmd:Z

    .line 494
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;->onPhotoRquestDone()V

    goto :goto_0

    .line 458
    .end local v1    # "i":I
    :cond_6
    iget-object v4, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v4, v4, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v4, :cond_2

    .line 459
    iget-object v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v2, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    .line 460
    .local v2, "original":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->combine(Lcom/sec/android/gallery3d/ui/ScreenNail;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    goto :goto_1

    .line 467
    .end local v2    # "original":Lcom/sec/samsung/gallery/decoder/TextureScreenNail;
    :cond_7
    iput-boolean v5, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    .line 468
    iput-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 470
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    if-eqz v4, :cond_3

    .line 471
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    invoke-interface {v4, p1, v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;->onPhotoAvailable(Lcom/sec/android/gallery3d/data/Path;Z)V

    goto :goto_2

    .line 476
    .restart local v1    # "i":I
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private updateScreenNailUploadQueue()V
    .locals 2

    .prologue
    .line 692
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->clear()V

    .line 693
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->uploadScreenNail(I)V

    .line 694
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 695
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->uploadScreenNail(I)V

    .line 696
    neg-int v1, v0

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->uploadScreenNail(I)V

    .line 694
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 698
    :cond_0
    return-void
.end method

.method private updateSlidingWindow()V
    .locals 1

    .prologue
    .line 972
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow(Z)V

    .line 973
    return-void
.end method

.method private updateSlidingWindow(Z)V
    .locals 6
    .param p1, "reload"    # Z

    .prologue
    const/4 v5, 0x0

    .line 977
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/lit8 v3, v3, -0x3

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v4, -0x7

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v5, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    .line 978
    .local v2, "start":I
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v2, 0x7

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 980
    .local v0, "end":I
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-ne v3, v2, :cond_1

    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ne v3, v0, :cond_1

    .line 1002
    :cond_0
    :goto_0
    return-void

    .line 984
    :cond_1
    iput v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    .line 985
    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    .line 988
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/lit8 v3, v3, -0x14

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v4, -0x28

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v5, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    .line 989
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    add-int/lit8 v4, v2, 0x28

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 990
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-gt v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    sub-int v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x10

    if-le v3, v4, :cond_0

    .line 991
    :cond_2
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    .local v1, "i":I
    :goto_1
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge v1, v3, :cond_5

    .line 992
    if-lt v1, v2, :cond_3

    if-lt v1, v0, :cond_4

    .line 993
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v4, v1, 0x28

    const/4 v5, 0x0

    aput-object v5, v3, v4

    .line 991
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 996
    :cond_5
    iput v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    .line 997
    iput v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    .line 998
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v3, :cond_0

    .line 999
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method private updateTCloudEntryInfo(ILcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    .locals 3
    .param p1, "index"    # I
    .param p2, "entry"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .prologue
    .line 1763
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v1, :cond_0

    .line 1764
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1765
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-eqz v1, :cond_0

    .line 1766
    iget-object v1, p2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1767
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v1

    iput v1, p2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    .line 1771
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    return-object p2
.end method

.method private updateTileProvider()V
    .locals 3

    .prologue
    .line 912
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 913
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-nez v0, :cond_1

    .line 914
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eq v1, v2, :cond_0

    .line 915
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    .line 920
    :cond_0
    :goto_0
    return-void

    .line 918
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    goto :goto_0
.end method

.method private updateTileProvider(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V
    .locals 9
    .param p1, "entry"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .prologue
    .line 923
    iget-object v4, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 924
    .local v4, "screenNail":Lcom/sec/android/gallery3d/ui/ScreenNail;
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 926
    .local v1, "fullImage":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iget-boolean v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isBrokenImage:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mBrokenImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_1

    .line 927
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mBrokenImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 928
    .local v5, "width":I
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mBrokenImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 929
    .local v2, "height":I
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    new-instance v7, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mBrokenImage:Landroid/graphics/Bitmap;

    invoke-direct {v7, v8}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v6, v7, v5, v2}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 969
    .end local v2    # "height":I
    .end local v5    # "width":I
    :cond_0
    :goto_0
    return-void

    .line 933
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    iget v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit8 v7, v7, 0x28

    aget-object v3, v6, v7

    .line 935
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x0

    .line 936
    .local v0, "background":Lcom/sec/android/gallery3d/ui/ScreenNail;
    if-eqz v4, :cond_3

    .line 937
    move-object v0, v4

    .line 945
    :cond_2
    :goto_1
    if-eqz v0, :cond_5

    .line 946
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_4

    .line 947
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v7

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v8

    invoke-virtual {v6, v0, v7, v8}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 948
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->setRegionDecoder(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 950
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v6, v1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->setQrd(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 956
    :goto_2
    iget v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 957
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v6

    iput v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    .line 958
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->isAnimating()Z

    move-result v6

    if-nez v6, :cond_0

    .line 959
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->startAnimation()V

    goto :goto_0

    .line 938
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseWSTN()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseBufferThumbnailMode()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 939
    iget-object v6, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v6, :cond_2

    .line 940
    iget-object v0, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    goto :goto_1

    .line 953
    :cond_4
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v7

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v8

    invoke-virtual {v6, v0, v7, v8}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    goto :goto_2

    .line 964
    :cond_5
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eq v6, v7, :cond_0

    .line 965
    iget-object v6, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    goto :goto_0
.end method

.method private uploadScreenNail(I)V
    .locals 7
    .param p1, "offset"    # I

    .prologue
    .line 675
    iget v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v1, v5, p1

    .line 676
    .local v1, "index":I
    iget v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt v1, v5, :cond_0

    iget v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt v1, v5, :cond_1

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 679
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_0

    .line 681
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 682
    .local v0, "e":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 684
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 685
    .local v3, "s":Lcom/sec/android/gallery3d/ui/ScreenNail;
    instance-of v5, v3, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-eqz v5, :cond_0

    .line 686
    check-cast v3, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    .end local v3    # "s":Lcom/sec/android/gallery3d/ui/ScreenNail;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->getTexture()Lcom/sec/android/gallery3d/glrenderer/TiledTexture;

    move-result-object v4

    .line 687
    .local v4, "t":Lcom/sec/android/gallery3d/glrenderer/TiledTexture;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture;->isReady()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->addTexture(Lcom/sec/android/gallery3d/glrenderer/TiledTexture;)V

    goto :goto_0
.end method


# virtual methods
.method public changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1869
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->pause()V

    .line 1870
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1871
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->setCurrentPhoto(Lcom/sec/android/gallery3d/data/Path;I)V

    .line 1872
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->resume()V

    .line 1873
    return-void
.end method

.method public clearImageCacheEntry(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2033
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 2034
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 2035
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedAvailable:Z

    .line 2036
    const/4 v1, -0x1

    iput v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->tileLevel:I

    .line 2037
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    if-eqz v1, :cond_0

    .line 2038
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->clear()V

    .line 2039
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    .line 2042
    :cond_0
    return-void
.end method

.method public completeSlideAnimation()V
    .locals 3

    .prologue
    .line 667
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->resetBrokenImage()V

    .line 669
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;->onPhotoChanged(ILcom/sec/android/gallery3d/data/Path;)V

    .line 672
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 1848
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsAlive:Z

    .line 1849
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    .line 1850
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    .line 1851
    return-void
.end method

.method public excuteFullImageTask(ILcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Z
    .locals 8
    .param p1, "index"    # I
    .param p2, "decoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    const/4 v3, 0x0

    .line 1935
    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p1, v4, :cond_0

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt p1, v4, :cond_1

    .line 1957
    :cond_0
    :goto_0
    return v3

    .line 1938
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 1939
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 1940
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1942
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    const-wide/16 v6, 0x40

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 1943
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1944
    :cond_2
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-nez v3, :cond_3

    .line 1945
    const/4 v2, 0x0

    .line 1946
    .local v2, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    if-eqz p2, :cond_4

    .line 1947
    new-instance v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;

    .end local v2    # "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    invoke-direct {v2, p0, p2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageJob;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 1951
    .restart local v2    # "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    new-instance v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1954
    .end local v2    # "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    :cond_3
    const/4 v3, 0x1

    goto :goto_0

    .line 1949
    .restart local v2    # "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;>;"
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v2

    goto :goto_1
.end method

.method public forceUpdateDrmImage()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1854
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit8 v4, v4, 0x28

    aget-object v1, v3, v4

    .line 1855
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    instance-of v3, v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1864
    :cond_0
    :goto_0
    return v2

    .line 1857
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1858
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 1859
    iget-boolean v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isBrokenImage:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isValidRights(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1860
    iput-boolean v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isBrokenImage:Z

    .line 1861
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1835
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    return v0
.end method

.method public getCurrentBitmap()Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2174
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 2175
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-nez v0, :cond_0

    .line 2182
    :goto_0
    return-object v2

    .line 2177
    :cond_0
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v3, v3, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-nez v3, :cond_2

    .line 2178
    :cond_1
    const-string v3, "PhotoDataAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentBitmap is failed = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2181
    :cond_2
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    .line 2182
    .local v1, "tiledScreenNail":Lcom/sec/android/gallery3d/ui/TiledScreenNail;
    iget-object v2, v1, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getCurrentCacheSize()I
    .locals 1

    .prologue
    .line 1843
    const/16 v0, 0x28

    return v0
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 873
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    return v0
.end method

.method public getCurrentItemPath()Lcom/sec/android/gallery3d/data/Path;
    .locals 1

    .prologue
    .line 2212
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    return-object v0
.end method

.method public getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2

    .prologue
    .line 1784
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    rem-int/lit8 v0, v1, 0x28

    .line 1785
    .local v0, "index":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v1, v1

    if-le v1, v0, :cond_0

    .line 1786
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v1, v1, v0

    .line 1788
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDecodedList()Landroid/util/LongSparseArray;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2023
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 2024
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 2025
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    if-eqz v1, :cond_0

    .line 2026
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    iget-object v1, v1, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    .line 2029
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFullImageListener(Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/sec/android/gallery3d/util/FutureListener;
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            "I)",
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1982
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 1983
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$FullImageListener;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method

.method public getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1877
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 1878
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    goto :goto_0
.end method

.method public getImageHeight()I
    .locals 1

    .prologue
    .line 848
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->getImageHeight()I

    move-result v0

    return v0
.end method

.method public getImageRotation()I
    .locals 3

    .prologue
    .line 1777
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1779
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->rotation:I

    goto :goto_0
.end method

.method public getImageRotation(I)I
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 767
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 768
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitRotation:I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFullImageRotation()I

    move-result v1

    goto :goto_0
.end method

.method public getImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V
    .locals 3
    .param p1, "offset"    # I
    .param p2, "size"    # Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    .prologue
    const/4 v2, 0x0

    .line 755
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 756
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    .line 757
    iput v2, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 758
    iput v2, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 763
    :goto_0
    return-void

    .line 760
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v1

    iput v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 761
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v1

    iput v1, p2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    goto :goto_0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 853
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->getImageWidth()I

    move-result v0

    return v0
.end method

.method public getInitialScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 2060
    if-nez p1, :cond_0

    .line 2061
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 2063
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 614
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-ge p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 622
    :goto_0
    return-object v0

    .line 617
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v0, :cond_2

    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 619
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt p1, v0, :cond_3

    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge p1, v0, :cond_3

    .line 620
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v1, p1, 0x28

    aget-object v0, v0, v1

    goto :goto_0

    .line 617
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 622
    goto :goto_0
.end method

.method public getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1827
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-ge p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v0, :cond_1

    .line 1828
    :cond_0
    const/4 v0, 0x0

    .line 1831
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v1, p1, 0x28

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2015
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 2016
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 2017
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    .line 2019
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLevel()[I
    .locals 2

    .prologue
    .line 2006
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 2007
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 2008
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    if-eqz v1, :cond_0

    .line 2009
    iget-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    iget-object v1, v1, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    .line 2011
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLevelCount()I
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->getLevelCount()I

    move-result v0

    return v0
.end method

.method public getLoadingState(I)I
    .locals 4
    .param p1, "offset"    # I

    .prologue
    const/4 v1, 0x0

    .line 827
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v3, p1

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 828
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-nez v0, :cond_1

    .line 831
    :cond_0
    :goto_0
    return v1

    .line 829
    :cond_1
    iget-boolean v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->failToLoad:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x2

    goto :goto_0

    .line 830
    :cond_2
    iget-object v2, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 878
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v0, v1, p1

    .line 879
    .local v0, "index":I
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentStart:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mContentEnd:I

    if-ge v0, v1, :cond_0

    .line 880
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v2, v0, 0x28

    aget-object v1, v1, v2

    .line 882
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 1839
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 1

    .prologue
    .line 843
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    return-object v0
.end method

.method public getScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 6
    .param p1, "offset"    # I

    .prologue
    .line 712
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int v1, v3, p1

    .line 713
    .local v1, "index":I
    if-ltz v1, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-ge v1, v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v3, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getInitialScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    .line 750
    :goto_0
    return-object v3

    .line 714
    :cond_1
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt v1, v3, :cond_2

    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-ge v1, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 716
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 717
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_3

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getInitialScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    goto :goto_0

    .line 714
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 719
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 720
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-nez v0, :cond_4

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getInitialScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    goto :goto_0

    .line 722
    :cond_4
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-nez v3, :cond_5

    if-nez p1, :cond_5

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_5

    .line 723
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    iput-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 724
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    goto :goto_0

    .line 729
    :cond_5
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-nez v3, :cond_7

    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 730
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->useLoadingProgress(I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 731
    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->newPlaceholderScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 732
    :cond_6
    if-nez p1, :cond_7

    .line 733
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    .line 736
    :cond_7
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseWSTN()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_9

    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ScreenNail;->isShowingPlaceholder()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 737
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x3

    if-gt v3, v4, :cond_9

    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseBufferThumbnailMode()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 739
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    if-nez v3, :cond_8

    .line 740
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    new-instance v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;

    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    invoke-direct {v4, p0, v5, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailBufferJob;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/CacheInterface;I)V

    new-instance v5, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailListener;

    invoke-direct {v5, p0, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$MicroThumbnailListener;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    .line 744
    :cond_8
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_9

    .line 745
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    goto/16 :goto_0

    .line 750
    :cond_9
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    goto/16 :goto_0
.end method

.method public getTile(IIII)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "level"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "tileSize"    # I

    .prologue
    .line 863
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->getTile(IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public is3DTour(I)Z
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 802
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 803
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const-wide/16 v2, 0x400

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    goto :goto_0
.end method

.method public isCamera(I)Z
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 779
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCameraIndex:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDecodedAvailable()Z
    .locals 2

    .prologue
    .line 1998
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 1999
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 2000
    iget-boolean v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedAvailable:Z

    .line 2002
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDeletable(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 822
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 868
    iget v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mSize:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGolfShot(I)Z
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 810
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 811
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->isLoading()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPanorama(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 784
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsPanorama:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStaticCamera(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 789
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isCamera(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsStaticCamera:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideo(I)Z
    .locals 4
    .param p1, "offset"    # I

    .prologue
    const/4 v1, 0x0

    .line 794
    iget v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v2, p1

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 795
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    instance-of v2, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public moveTo(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 702
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateCurrentIndex(I)V

    .line 703
    return-void
.end method

.method public moveToFastest(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 707
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateCurrentIndexFastest(I)V

    .line 708
    return-void
.end method

.method public onAgifPlayRequests()V
    .locals 2

    .prologue
    .line 1818
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    .line 1819
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    if-nez v0, :cond_0

    .line 1824
    :goto_0
    return-void

    .line 1821
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    invoke-virtual {v0}, Lcom/quramsoft/agif/QuramAGIF;->getQURAMWINKUTIL()Lcom/quramsoft/agif/QURAMWINKUTIL;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

    .line 1823
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v1}, Lcom/quramsoft/agif/QURAMWINKUTIL;->updateAGIF(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    goto :goto_0
.end method

.method public pause()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    .line 555
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    .line 557
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v3, :cond_0

    .line 558
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->terminate()V

    .line 559
    iput-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    .line 562
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 563
    .local v0, "currentEntry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 568
    .local v1, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    if-eqz v3, :cond_2

    .line 569
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/decoder/LargeImage;->stopDecodeThread(Z)V

    .line 571
    iput-object v5, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->mLargeImage:Lcom/sec/samsung/gallery/decoder/LargeImage;

    .line 573
    :cond_2
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v3, :cond_3

    .line 574
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 576
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->cancelAllNewInstances()V

    .line 581
    :cond_3
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v3, :cond_4

    .line 582
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 584
    :cond_4
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_5

    if-eq v1, v0, :cond_5

    .line 585
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    .line 589
    :cond_5
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v3, :cond_6

    .line 590
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnailTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 592
    :cond_6
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v3, :cond_1

    .line 593
    iget-object v3, v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->microThumbnail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    goto :goto_0

    .line 597
    .end local v1    # "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    :cond_7
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 598
    if-eqz v0, :cond_9

    .line 599
    iget-object v3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->isScreenNailAvailable(Lcom/sec/android/gallery3d/ui/ScreenNail;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 600
    iput-wide v6, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    .line 602
    :cond_8
    iput-wide v6, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    .line 603
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v4, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 605
    :cond_9
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->clear()V

    .line 606
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->clearLargeImageTilePool()V

    .line 607
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mUploader:Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glrenderer/TiledTexture$Uploader;->clear()V

    .line 609
    return-void
.end method

.method public reload()V
    .locals 1

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v0, :cond_0

    .line 1590
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    .line 1591
    :cond_0
    return-void
.end method

.method public reloadForRotate()V
    .locals 2

    .prologue
    .line 2186
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v1, :cond_0

    .line 2187
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    .line 2193
    :goto_0
    return-void

    .line 2189
    :cond_0
    const-class v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 2190
    .local v0, "threadName":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    .line 2191
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->start()V

    goto :goto_0
.end method

.method public requestFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;ILcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 6
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "index"    # I
    .param p3, "decoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    const/4 v2, 0x0

    .line 1961
    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveStart:I

    if-lt p2, v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mActiveEnd:I

    if-lt p2, v3, :cond_1

    .line 1978
    :cond_0
    :goto_0
    return-object v2

    .line 1963
    :cond_1
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 1964
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItemFromIndex(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 1965
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1967
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x40

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1968
    if-nez p3, :cond_2

    .line 1969
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1970
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object p3

    .end local p3    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    check-cast p3, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 1974
    .restart local p3    # "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    :cond_2
    iput-object p3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImage:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-object v2, p3

    .line 1975
    goto :goto_0

    :cond_3
    move-object v2, p3

    .line 1978
    goto :goto_0
.end method

.method public resetBrokenImage()V
    .locals 3

    .prologue
    .line 1806
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1808
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 1809
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isBrokenImage:Z

    .line 1811
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 539
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    .line 542
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->mIsTilePoolRecycled:Z

    .line 543
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    .line 544
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageRequests()V

    .line 546
    const-class v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 547
    .local v0, "threadName":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;-><init>(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    .line 548
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->start()V

    .line 550
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    .line 551
    return-void
.end method

.method public setCurrentPhoto(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "indexHint"    # I

    .prologue
    .line 887
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    if-ne v1, p1, :cond_1

    .line 899
    :cond_0
    :goto_0
    return-void

    .line 888
    :cond_1
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    .line 889
    iput p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    .line 890
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateSlidingWindow()V

    .line 891
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateImageCache()V

    .line 892
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->fireDataChange()V

    .line 895
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 896
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 897
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mReloadTask:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ReloadTask;->notifyDirty()V

    goto :goto_0
.end method

.method public setDataListener(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    .prologue
    .line 436
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDataListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    .line 437
    return-void
.end method

.method public setFocusHintDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 903
    iput p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintDirection:I

    .line 904
    return-void
.end method

.method public setFocusHintPath(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 908
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mFocusHintPath:Lcom/sec/android/gallery3d/data/Path;

    .line 909
    return-void
.end method

.method public setImageFetchSeq(Z)V
    .locals 10
    .param p1, "bUseLargeThumbnail"    # Z

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v6, 0x1

    .line 1736
    const/4 v3, 0x0

    .line 1737
    .local v3, "k":I
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->getMTNCachingSize()I

    move-result v0

    .line 1738
    .local v0, "cacheSize":I
    mul-int/lit8 v8, v0, 0x2

    if-eqz p1, :cond_0

    move v5, v6

    :goto_0
    add-int v1, v8, v5

    .line 1740
    .local v1, "fetchSeqSize":I
    new-array v5, v1, [Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    iput-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    .line 1741
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "k":I
    .local v4, "k":I
    new-instance v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v8, v9, v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v8, v5, v3

    .line 1742
    if-nez p1, :cond_2

    .line 1743
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "k":I
    .restart local v3    # "k":I
    new-instance v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v8, v9, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v8, v5, v4

    .line 1745
    :goto_1
    const/4 v2, 0x1

    .local v2, "i":I
    move v4, v3

    .end local v3    # "k":I
    .restart local v4    # "k":I
    :goto_2
    if-gt v2, v0, :cond_1

    .line 1746
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "k":I
    .restart local v3    # "k":I
    new-instance v7, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    invoke-direct {v7, v2, v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v7, v5, v4

    .line 1747
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "k":I
    .restart local v4    # "k":I
    new-instance v7, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    neg-int v8, v2

    invoke-direct {v7, v8, v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;-><init>(II)V

    aput-object v7, v5, v3

    .line 1745
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .end local v1    # "fetchSeqSize":I
    .end local v2    # "i":I
    .end local v4    # "k":I
    .restart local v3    # "k":I
    :cond_0
    move v5, v7

    .line 1738
    goto :goto_0

    .line 1749
    .end local v3    # "k":I
    .restart local v1    # "fetchSeqSize":I
    .restart local v2    # "i":I
    .restart local v4    # "k":I
    :cond_1
    return-void

    .end local v2    # "i":I
    :cond_2
    move v3, v4

    .end local v4    # "k":I
    .restart local v3    # "k":I
    goto :goto_1
.end method

.method public setInitialScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;I)V
    .locals 4
    .param p1, "snail"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p2, "rotation"    # I

    .prologue
    .line 2046
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v1, :cond_0

    .line 2047
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/ScreenNail;->recycle()V

    .line 2049
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 2050
    iput p2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mInitRotation:I

    .line 2051
    if-eqz p1, :cond_1

    .line 2052
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mTileProvider:Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;

    invoke-interface {p1}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v2

    invoke-interface {p1}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2057
    :cond_1
    :goto_0
    return-void

    .line 2054
    :catch_0
    move-exception v0

    .line 2055
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setNeedFullImage(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 773
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    .line 774
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 775
    return-void
.end method

.method public updateBrokenImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1793
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mBrokenImage:Landroid/graphics/Bitmap;

    .line 1795
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getPath(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1797
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    .line 1798
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->isBrokenImage:Z

    .line 1799
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->updateTileProvider(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;)V

    .line 1800
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyImageChange(I)V

    .line 1802
    :cond_0
    return-void
.end method

.method public updateImageCacheEntry(Landroid/util/LongSparseArray;II)V
    .locals 2
    .param p2, "index"    # I
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/android/gallery3d/ui/TileImageView$Tile;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1988
    .local p1, "mList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/android/gallery3d/ui/TileImageView$Tile;>;"
    invoke-virtual {p0, p2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getImageEntryByIndex(I)Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    move-result-object v0

    .line 1989
    .local v0, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1990
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedAvailable:Z

    .line 1991
    iput p3, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->tileLevel:I

    .line 1992
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    .line 1993
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->clone()Landroid/util/LongSparseArray;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->decodedTiles:Landroid/util/LongSparseArray;

    .line 1995
    :cond_0
    return-void
.end method

.method public updateImageRequests()V
    .locals 14

    .prologue
    const-wide/16 v12, -0x1

    const/4 v10, 0x0

    .line 1008
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mIsActive:Z

    if-nez v8, :cond_1

    .line 1054
    :cond_0
    return-void

    .line 1012
    :cond_1
    iget v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    if-ltz v8, :cond_0

    .line 1016
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    .line 1017
    .local v1, "currentIndex":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaItem;

    rem-int/lit8 v9, v1, 0x28

    aget-object v5, v8, v9

    .line 1018
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mItemPath:Lcom/sec/android/gallery3d/data/Path;

    if-ne v8, v9, :cond_0

    .line 1024
    const/4 v7, 0x0

    .line 1025
    .local v7, "task":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<*>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    array-length v8, v8

    if-ge v3, v8, :cond_4

    .line 1026
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    aget-object v8, v8, v3

    iget v6, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;->indexOffset:I

    .line 1027
    .local v6, "offset":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageFetchSeq:[Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;

    aget-object v8, v8, v3

    iget v0, v8, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageFetch;->imageBit:I

    .line 1028
    .local v0, "bit":I
    const/4 v8, 0x2

    if-ne v0, v8, :cond_3

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mNeedFullImage:Z

    if-nez v8, :cond_3

    .line 1025
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1031
    :cond_3
    add-int v8, v1, v6

    invoke-direct {p0, v8, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->startTaskIfNeeded(II)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v7

    .line 1032
    if-eqz v7, :cond_2

    .line 1038
    .end local v0    # "bit":I
    .end local v6    # "offset":I
    :cond_4
    iget-object v8, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mImageCache:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;

    .line 1039
    .local v2, "entry":Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;
    iget-object v8, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v8, :cond_6

    iget-object v8, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_6

    .line 1040
    iget-object v8, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 1041
    iput-object v10, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->screenNailTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1042
    iput-wide v12, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedScreenNail:J

    .line 1045
    :cond_6
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1046
    iget-object v8, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v8, :cond_5

    iget-object v8, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-eq v8, v7, :cond_5

    .line 1047
    iget-object v8, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v8}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 1048
    iput-object v10, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->fullImageTask:Lcom/sec/android/gallery3d/util/Future;

    .line 1049
    iput-wide v12, v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter$ImageEntry;->requestedFullImage:J

    goto :goto_1
.end method

.method public useLoadingProgress(I)Z
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 836
    iget v1, p0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mCurrentIndex:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 837
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v1, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-nez v1, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
