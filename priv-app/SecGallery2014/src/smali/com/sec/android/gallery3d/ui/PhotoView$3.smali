.class Lcom/sec/android/gallery3d/ui/PhotoView$3;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/PositionController$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 0

    .prologue
    .line 583
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 588
    return-void
.end method

.method public isHoldingDelete()Z
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHoldingDown()Z
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mHolding:I
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAbsorb(II)V
    .locals 1
    .param p1, "velocity"    # I
    .param p2, "direction"    # I

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/EdgeView;->onAbsorb(II)V

    .line 615
    return-void
.end method

.method public onPull(II)V
    .locals 2
    .param p1, "offset"    # I
    .param p2, "direction"    # I

    .prologue
    .line 602
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/EdgeView;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/EdgeView;->onPull(II)V

    .line 604
    return-void
.end method

.method public onRelease()V
    .locals 2

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/EdgeView;->onRelease()V

    .line 609
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$3;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mEdgeView:Lcom/sec/android/gallery3d/ui/EdgeView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$600(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/EdgeView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/EdgeView;->setVisibility(I)V

    .line 610
    return-void
.end method
