.class public Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;
.super Ljava/lang/Object;
.source "LocalImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/LocalImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocalLargeImageRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field mIsBroken:Z

.field mLocalFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "localFilePath"    # Ljava/lang/String;

    .prologue
    .line 487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mLocalFilePath:Ljava/lang/String;

    .line 489
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 0
    .param p1, "localFilePath"    # Ljava/lang/String;
    .param p2, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mLocalFilePath:Ljava/lang/String;

    .line 494
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 495
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 504
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    if-nez v1, :cond_1

    .line 505
    const/4 v0, 0x0

    .line 516
    :cond_0
    :goto_0
    return-object v0

    .line 507
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mLocalFilePath:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v1

    invoke-static {p1, v2, v3, v1}, Lcom/sec/android/gallery3d/data/DecodeUtils;->createBitmapRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Ljava/lang/String;ZLcom/sec/samsung/gallery/decoder/DecoderInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    .line 510
    .local v0, "decoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mLocalFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 511
    if-nez v0, :cond_0

    .line 512
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->getBrokenImageRegionDecoder(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 480
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    return-object v0
.end method

.method public setBroken(Z)V
    .locals 0
    .param p1, "isBroken"    # Z

    .prologue
    .line 498
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/LocalImage$LocalLargeImageRequest;->mIsBroken:Z

    .line 499
    return-void
.end method
