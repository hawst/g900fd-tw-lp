.class Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/PhotoView$Picture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenNailPicture"
.end annotation


# instance fields
.field private mDiff:I

.field private mIndex:I

.field private mIs3DTour:Z

.field private mIsCamera:Z

.field private mIsDeletable:Z

.field private mIsGolfShot:Z

.field private mIsPanorama:Z

.field private mIsStaticCamera:Z

.field private mIsVideo:Z

.field private mLoadingState:I

.field private mRotation:I

.field private mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

.field private mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoView;I)V
    .locals 2
    .param p2, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 1315
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312
    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mLoadingState:I

    .line 1313
    new-instance v0, Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/ui/PhotoView$Size;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    .line 1339
    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mDiff:I

    .line 1316
    iput p2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    .line 1317
    return-void
.end method

.method private isScreenNailAnimating()Z
    .locals 1

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v0, v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    if-eqz v0, :cond_0

    .line 1438
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v0, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/TextureScreenNail;->isAnimating()Z

    move-result v0

    .line 1441
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    instance-of v0, v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    check-cast v0, Lcom/sec/android/gallery3d/ui/TiledScreenNail;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/TiledScreenNail;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSize()V
    .locals 5

    .prologue
    .line 1457
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsPanorama:Z

    if-eqz v2, :cond_0

    .line 1458
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getPanoramaRotation()I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2200(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    .line 1465
    :goto_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-eqz v2, :cond_2

    .line 1466
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 1467
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 1474
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget v1, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 1475
    .local v1, "w":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget v0, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 1476
    .local v0, "h":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getRotated(III)I
    invoke-static {v3, v1, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2400(III)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->width:I

    .line 1477
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getRotated(III)I
    invoke-static {v3, v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2400(III)I

    move-result v3

    iput v3, v2, Lcom/sec/android/gallery3d/ui/PhotoView$Size;->height:I

    .line 1478
    return-void

    .line 1459
    .end local v0    # "h":I
    .end local v1    # "w":I
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsCamera:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsStaticCamera:Z

    if-nez v2, :cond_1

    .line 1460
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getCameraRotation()I
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2300(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    goto :goto_0

    .line 1462
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageRotation(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    goto :goto_0

    .line 1471
    :cond_2
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-interface {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V

    goto :goto_1
.end method


# virtual methods
.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V
    .locals 30
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 1349
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    if-nez v4, :cond_1

    .line 1352
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPrevBound:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1500(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    if-lt v4, v5, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mNextBound:I
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$1400(Lcom/sec/android/gallery3d/ui/PhotoView;)I

    move-result v5

    if-gt v4, v5, :cond_0

    .line 1353
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->drawPlaceHolder(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V
    invoke-static {v4, v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3600(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V

    .line 1433
    :cond_0
    :goto_0
    return-void

    .line 1357
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v26

    .line 1358
    .local v26, "w":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v20

    .line 1359
    .local v20, "h":I
    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->left:I

    move/from16 v0, v26

    if-ge v4, v0, :cond_2

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->right:I

    if-lez v4, :cond_2

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move/from16 v0, v20

    if-ge v4, v0, :cond_2

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    if-gtz v4, :cond_3

    .line 1360
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    invoke-interface {v4}, Lcom/sec/android/gallery3d/ui/ScreenNail;->noDraw()V

    goto :goto_0

    .line 1364
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PositionController;->getFilmRatio()F

    move-result v19

    .line 1365
    .local v19, "filmRatio":F
    const/16 v27, 0x0

    .line 1367
    .local v27, "wantsCardEffect":Z
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsDeletable:Z

    if-eqz v4, :cond_9

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, v19, v4

    if-nez v4, :cond_9

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    div-int/lit8 v5, v20, 0x2

    if-eq v4, v5, :cond_9

    const/16 v28, 0x1

    .line 1369
    .local v28, "wantsOffsetEffect":Z
    :goto_1
    if-eqz v27, :cond_a

    div-int/lit8 v4, v26, 0x2

    int-to-float v4, v4

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    int-to-float v5, v5

    move/from16 v0, v19

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->interpolate(FFF)F
    invoke-static {v0, v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2900(FFF)F

    move-result v4

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v15, v4

    .line 1372
    .local v15, "cx":I
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerY()I

    move-result v16

    .line 1373
    .local v16, "cy":I
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 1374
    int-to-float v4, v15

    move/from16 v0, v16

    int-to-float v5, v0

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FF)V

    .line 1375
    if-eqz v27, :cond_c

    .line 1378
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1379
    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move/from16 v0, v26

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->calculateMoveOutProgress(III)F
    invoke-static {v4, v5, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2600(III)F

    move-result v23

    .line 1384
    .local v23, "progress":F
    :goto_3
    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    move/from16 v0, v23

    invoke-static {v0, v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(FFF)F

    move-result v23

    .line 1385
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move/from16 v0, v23

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getScrollAlpha(F)F
    invoke-static {v4, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2800(Lcom/sec/android/gallery3d/ui/PhotoView;F)F

    move-result v14

    .line 1386
    .local v14, "alpha":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move/from16 v0, v23

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getScrollScale(F)F
    invoke-static {v4, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2700(Lcom/sec/android/gallery3d/ui/PhotoView;F)F

    move-result v25

    .line 1387
    .local v25, "scale":F
    const/high16 v4, 0x3f800000    # 1.0f

    move/from16 v0, v19

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->interpolate(FFF)F
    invoke-static {v0, v14, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2900(FFF)F

    move-result v14

    .line 1388
    const/high16 v4, 0x3f800000    # 1.0f

    move/from16 v0, v19

    move/from16 v1, v25

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->interpolate(FFF)F
    invoke-static {v0, v1, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2900(FFF)F

    move-result v25

    .line 1389
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyAlpha(F)V

    .line 1390
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v25

    invoke-interface {v0, v1, v2, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->scale(FFF)V

    .line 1396
    .end local v14    # "alpha":F
    .end local v23    # "progress":F
    .end local v25    # "scale":F
    :cond_4
    :goto_4
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 1397
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    if-eqz v4, :cond_5

    .line 1398
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    int-to-float v4, v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v7, v8}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->rotate(FFFF)V

    .line 1400
    :cond_5
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v7

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getRotated(III)I
    invoke-static {v4, v5, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2400(III)I

    move-result v18

    .line 1401
    .local v18, "drawW":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v7

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getRotated(III)I
    invoke-static {v4, v5, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$2400(III)I

    move-result v17

    .line 1403
    .local v17, "drawH":I
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->isUseLargeThumbnail()Z

    move-result v4

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v4

    if-nez v4, :cond_e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mDiff:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    .line 1404
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mDiff:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_d

    const/4 v6, 0x1

    .line 1405
    .local v6, "isNext":Z
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3800(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/CacheInterface;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    move/from16 v0, v18

    neg-int v5, v0

    div-int/lit8 v8, v5, 0x2

    move/from16 v0, v17

    neg-int v5, v0

    div-int/lit8 v9, v5, 0x2

    move/from16 v0, v18

    int-to-float v10, v0

    move/from16 v0, v17

    int-to-float v11, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    int-to-float v12, v5

    move-object/from16 v5, p1

    invoke-virtual/range {v4 .. v12}, Lcom/sec/samsung/gallery/decoder/CacheInterface;->drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIFFF)V

    .line 1418
    .end local v6    # "isNext":Z
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->isScreenNailAnimating()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1419
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 1421
    :cond_6
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 1422
    move/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v24

    .line 1424
    .local v24, "s":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsVideo:Z

    if-nez v4, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIs3DTour:Z

    if-nez v4, :cond_7

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGolfIconSupported:Z

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsGolfShot:Z

    if-eqz v4, :cond_8

    .line 1425
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v29

    .line 1426
    .local v29, "width":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v21

    .line 1428
    .local v21, "height":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 1429
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3200(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v29

    move/from16 v3, v21

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V

    .line 1432
    .end local v21    # "height":I
    .end local v29    # "width":I
    :cond_8
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    goto/16 :goto_0

    .line 1367
    .end local v15    # "cx":I
    .end local v16    # "cy":I
    .end local v17    # "drawH":I
    .end local v18    # "drawW":I
    .end local v24    # "s":I
    .end local v28    # "wantsOffsetEffect":Z
    :cond_9
    const/16 v28, 0x0

    goto/16 :goto_1

    .line 1369
    .restart local v28    # "wantsOffsetEffect":Z
    :cond_a
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerX()I

    move-result v15

    goto/16 :goto_2

    .line 1381
    .restart local v15    # "cx":I
    .restart local v16    # "cy":I
    :cond_b
    div-int/lit8 v4, v26, 0x2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move/from16 v0, v26

    int-to-float v5, v0

    div-float v23, v4, v5

    .restart local v23    # "progress":F
    goto/16 :goto_3

    .line 1391
    .end local v23    # "progress":F
    :cond_c
    if-eqz v28, :cond_4

    .line 1392
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    div-int/lit8 v5, v20, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move/from16 v0, v20

    int-to-float v5, v0

    div-float v22, v4, v5

    .line 1393
    .local v22, "offset":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    move/from16 v0, v22

    # invokes: Lcom/sec/android/gallery3d/ui/PhotoView;->getOffsetAlpha(F)F
    invoke-static {v4, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3000(Lcom/sec/android/gallery3d/ui/PhotoView;F)F

    move-result v14

    .line 1394
    .restart local v14    # "alpha":F
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyAlpha(F)V

    goto/16 :goto_4

    .line 1404
    .end local v14    # "alpha":F
    .end local v22    # "offset":F
    .restart local v17    # "drawH":I
    .restart local v18    # "drawW":I
    :cond_d
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 1408
    :cond_e
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mFilmMode:Z
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3700(Lcom/sec/android/gallery3d/ui/PhotoView;)Z

    move-result v4

    if-nez v4, :cond_10

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mDiff:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    .line 1409
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mDiff:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_f

    const/4 v6, 0x1

    .line 1410
    .restart local v6    # "isNext":Z
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$3900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    move/from16 v0, v18

    neg-int v5, v0

    div-int/lit8 v8, v5, 0x2

    move/from16 v0, v17

    neg-int v5, v0

    div-int/lit8 v9, v5, 0x2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mRotation:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mDiff:I

    move-object/from16 v5, p1

    move/from16 v10, v18

    move/from16 v11, v17

    invoke-virtual/range {v4 .. v13}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIIIII)V

    goto/16 :goto_6

    .line 1409
    .end local v6    # "isNext":Z
    :cond_f
    const/4 v6, 0x0

    goto :goto_7

    .line 1414
    :cond_10
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    move/from16 v0, v18

    neg-int v4, v0

    div-int/lit8 v9, v4, 0x2

    move/from16 v0, v17

    neg-int v4, v0

    div-int/lit8 v10, v4, 0x2

    move-object/from16 v8, p1

    move/from16 v11, v18

    move/from16 v12, v17

    invoke-interface/range {v7 .. v12}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_6
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;I)V
    .locals 0
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "diff"    # I

    .prologue
    .line 1342
    iput p3, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mDiff:I

    .line 1343
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;)V

    .line 1344
    return-void
.end method

.method public forceSize()V
    .locals 3

    .prologue
    .line 1452
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->updateSize()V

    .line 1453
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$900(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PositionController;->forceImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V

    .line 1454
    return-void
.end method

.method public getSize()Lcom/sec/android/gallery3d/ui/PhotoView$Size;
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mSize:Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    return-object v0
.end method

.method public isCamera()Z
    .locals 1

    .prologue
    .line 1482
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsCamera:Z

    return v0
.end method

.method public isDeletable()Z
    .locals 1

    .prologue
    .line 1487
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsDeletable:Z

    return v0
.end method

.method public reload()V
    .locals 2

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isCamera(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsCamera:Z

    .line 1322
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isPanorama(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsPanorama:Z

    .line 1323
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isStaticCamera(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsStaticCamera:Z

    .line 1324
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isVideo(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsVideo:Z

    .line 1325
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->is3DTour(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIs3DTour:Z

    .line 1326
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isGolfShot(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsGolfShot:Z

    .line 1327
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->isDeletable(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIsDeletable:Z

    .line 1328
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getLoadingState(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mLoadingState:I

    .line 1329
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->this$0:Lcom/sec/android/gallery3d/ui/PhotoView;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoView;->mModel:Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->access$700(Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/android/gallery3d/ui/PhotoView$Model;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V

    .line 1330
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->updateSize()V

    .line 1331
    return-void
.end method

.method public setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;)V
    .locals 0
    .param p1, "s"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    .line 1447
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoView$ScreenNailPicture;->mScreenNail:Lcom/sec/android/gallery3d/ui/ScreenNail;

    .line 1448
    return-void
.end method
