.class public Lcom/sec/android/gallery3d/data/LocalMergeAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "LocalMergeAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;
    }
.end annotation


# static fields
.field private static final PAGE_SIZE:I = 0x10

.field public static final PHOTO_FRAME_PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "LocalMergeAlbum"


# instance fields
.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBucketId:I

.field private mCameraAlbum:Z

.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mEventAlbumTimeInfo:Ljava/lang/String;

.field private mFetcher:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

.field private mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

.field private mIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;"
        }
    .end annotation
.end field

.field private mPathOnFileSystem:Ljava/lang/String;

.field private mPhotoFrameAlbum:Z

.field private mSources:[Lcom/sec/android/gallery3d/data/MediaSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/Pictures/Photo frame/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->PHOTO_FRAME_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "sources"    # [Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "[",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v6, 0x0

    .line 83
    array-length v0, p4

    if-nez v0, :cond_0

    const/4 v5, -0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;-><init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 84
    array-length v0, p4

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 86
    const-string v0, "LocalMergeAlbum"

    const-string/jumbo v1, "sources are null. ignore init"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :goto_2
    return-void

    .line 83
    :cond_0
    aget-object v0, p4, v6

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v5

    goto :goto_0

    .line 84
    :cond_1
    aget-object v0, p4, v6

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    sget-object v1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->PHOTO_FRAME_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPhotoFrameAlbum:Z

    goto :goto_2
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/Path;Ljava/util/Comparator;Lcom/sec/android/gallery3d/app/GalleryApp;[Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 7
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p3, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p4, "sources"    # [Lcom/sec/android/gallery3d/data/MediaSet;
    .param p5, "bucketId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/Path;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;",
            "Lcom/sec/android/gallery3d/app/GalleryApp;",
            "[",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v6, 0x0

    .line 69
    const-wide/16 v4, -0x1

    invoke-direct {p0, p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 58
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mCameraAlbum:Z

    .line 60
    iput-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    .line 61
    iput-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 65
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    iput-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    .line 70
    iput-object p3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 71
    iput-object p2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mComparator:Ljava/util/Comparator;

    .line 72
    iput-object p4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .line 73
    iput p5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mBucketId:I

    .line 74
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 75
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->reload()J

    .line 78
    return-void
.end method

.method private getMediaItem(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 18
    .param p1, "start"    # I
    .param p2, "count"    # I
    .param p4, "showAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    .local p3, "showGroupIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v4, 0x0

    .line 241
    .local v4, "head":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/Integer;[I>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    if-eqz v14, :cond_0

    .line 242
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    monitor-enter v15

    .line 243
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    add-int/lit8 v16, p1, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/util/TreeMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v4

    .line 244
    monitor-exit v15

    .line 246
    :cond_0
    if-nez v4, :cond_2

    .line 247
    const/4 v10, 0x0

    .line 343
    :cond_1
    :goto_0
    return-object v10

    .line 244
    :catchall_0
    move-exception v14

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v14

    .line 248
    :cond_2
    invoke-interface {v4}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 249
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v10, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-string v14, "LocalMergeAlbum"

    const-string v15, "Gallery: The head(SortedMap) is empty!"

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/data/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 254
    .end local v10    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_3
    const/4 v9, 0x0

    .line 256
    .local v9, "markPos":I
    :try_start_1
    invoke-interface {v4}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v9

    .line 263
    const/4 v13, 0x0

    .line 265
    .local v13, "subPos":[I
    if-eqz v4, :cond_4

    .line 266
    :try_start_2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v4, v14}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [I

    invoke-virtual {v14}, [I->clone()Ljava/lang/Object;

    move-result-object v14

    move-object v0, v14

    check-cast v0, [I

    move-object v13, v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 272
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v14, v14

    new-array v12, v14, [Lcom/sec/android/gallery3d/data/MediaItem;

    .line 274
    .local v12, "slot":[Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v13, :cond_5

    .line 275
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 276
    .restart local v10    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-string v14, "LocalMergeAlbum"

    const-string v15, "Gallery: subPos is null!"

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/data/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 257
    .end local v10    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v12    # "slot":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v13    # "subPos":[I
    :catch_0
    move-exception v2

    .line 258
    .local v2, "e":Ljava/util/NoSuchElementException;
    invoke-virtual {v2}, Ljava/util/NoSuchElementException;->printStackTrace()V

    .line 259
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .restart local v10    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_0

    .line 268
    .end local v2    # "e":Ljava/util/NoSuchElementException;
    .end local v10    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v13    # "subPos":[I
    :catch_1
    move-exception v2

    .line 269
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 280
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v12    # "slot":[Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v11, v14

    .line 283
    .local v11, "size":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v11, :cond_8

    .line 285
    if-nez p3, :cond_7

    .line 286
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v3, v14, v5

    .line 290
    .local v3, "fetcher":Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;
    :goto_3
    if-eqz v3, :cond_6

    .line 291
    aget v14, v13, v5

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v3, v14, v0, v1}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->getItem(ILjava/util/ArrayList;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v14

    aput-object v14, v12, v5

    .line 283
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 288
    .end local v3    # "fetcher":Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v3, v14, v5

    .restart local v3    # "fetcher":Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;
    goto :goto_3

    .line 295
    .end local v3    # "fetcher":Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;
    :cond_8
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .restart local v10    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v6, 0x1

    .line 298
    .local v6, "isSortByLatest":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    if-eqz v14, :cond_9

    .line 299
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v14}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v14

    if-nez v14, :cond_c

    const/4 v6, 0x1

    .line 301
    :cond_9
    :goto_4
    move v5, v9

    :goto_5
    add-int v14, p1, p2

    if-ge v5, v14, :cond_1

    .line 302
    const/4 v8, -0x1

    .line 303
    .local v8, "k":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_6
    if-ge v7, v11, :cond_e

    .line 304
    aget-object v14, v12, v7

    if-eqz v14, :cond_b

    .line 305
    const/4 v14, -0x1

    if-eq v8, v14, :cond_a

    if-eqz v6, :cond_d

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mComparator:Ljava/util/Comparator;

    aget-object v15, v12, v7

    aget-object v16, v12, v8

    invoke-interface/range {v14 .. v16}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v14

    if-gez v14, :cond_b

    .line 306
    :cond_a
    :goto_7
    move v8, v7

    .line 303
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 299
    .end local v7    # "j":I
    .end local v8    # "k":I
    :cond_c
    const/4 v6, 0x0

    goto :goto_4

    .line 305
    .restart local v7    # "j":I
    .restart local v8    # "k":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mComparator:Ljava/util/Comparator;

    aget-object v15, v12, v7

    aget-object v16, v12, v8

    invoke-interface/range {v14 .. v16}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v14

    if-lez v14, :cond_b

    goto :goto_7

    .line 312
    :cond_e
    const/4 v14, -0x1

    if-eq v8, v14, :cond_1

    .line 315
    aget v14, v13, v8

    add-int/lit8 v14, v14, 0x1

    aput v14, v13, v8

    .line 316
    move/from16 v0, p1

    if-lt v5, v0, :cond_f

    .line 317
    aget-object v14, v12, v8

    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    :cond_f
    if-nez p3, :cond_12

    .line 322
    :try_start_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v14, v14, v8

    if-eqz v14, :cond_10

    .line 323
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v14, v14, v8

    aget v15, v13, v8

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v14, v15, v0, v1}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->getItem(ILjava/util/ArrayList;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v14

    aput-object v14, v12, v8
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2

    .line 336
    :cond_10
    :goto_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    monitor-enter v15

    .line 337
    add-int/lit8 v14, v5, 0x1

    :try_start_4
    rem-int/lit8 v14, v14, 0x10

    if-nez v14, :cond_11

    .line 338
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    add-int/lit8 v16, v5, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual {v13}, [I->clone()Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    :cond_11
    monitor-exit v15
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 301
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5

    .line 326
    :cond_12
    :try_start_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v14, v14, v8

    if-eqz v14, :cond_10

    .line 327
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v14, v14, v8

    aget v15, v13, v8

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v14, v15, v0, v1}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->getItem(ILjava/util/ArrayList;Z)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v14

    aput-object v14, v12, v8
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_8

    .line 330
    :catch_2
    move-exception v2

    .line 331
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 332
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 340
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :catchall_1
    move-exception v14

    :try_start_6
    monitor-exit v15
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v14
.end method

.method private invalidateCache()V
    .locals 7

    .prologue
    .line 121
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v3

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 122
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->invalidate()V

    .line 123
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->invalidate()V

    .line 121
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 125
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->clear()V

    .line 127
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v6, v6

    new-array v6, v6, [I

    invoke-virtual {v3, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    monitor-exit v4

    .line 133
    .end local v2    # "n":I
    :goto_1
    return-void

    .line 128
    .restart local v2    # "n":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    .line 129
    .end local v2    # "n":I
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method private updateData()V
    .locals 6

    .prologue
    .line 103
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v2

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    .line 104
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v2

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    iput-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    .line 105
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v2

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 106
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcher:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    new-instance v3, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v4, v4, v0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    aput-object v3, v2, v0

    .line 107
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    new-instance v3, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v4, v4, v0

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/app/GalleryApp;)V

    aput-object v3, v2, v0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->clear()V

    .line 110
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mIndex:Ljava/util/TreeMap;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v4, v4

    new-array v4, v4, [I

    invoke-virtual {v2, v3, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    return-void
.end method


# virtual methods
.method public changeSources([Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 5
    .param p1, "sources"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 513
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_0

    .line 514
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 515
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 514
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 518
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .line 519
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .restart local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .restart local v2    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 520
    .restart local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 519
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 522
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return-void
.end method

.method public delete()V
    .locals 4

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 405
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->delete()V

    .line 404
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 407
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method

.method public delete(Z)V
    .locals 4
    .param p1, "includeDir"    # Z

    .prologue
    .line 567
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 568
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->delete(Z)V

    .line 567
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 570
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method

.method public getBucketId()I
    .locals 1

    .prologue
    .line 525
    iget v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mBucketId:I

    return v0
.end method

.method public getBurstShotItems(J)Ljava/util/ArrayList;
    .locals 7
    .param p1, "groupId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 683
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 684
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v5, v0, v1

    .line 685
    .local v5, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v5, p1, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getBurstShotItems(J)Ljava/util/ArrayList;

    move-result-object v2

    .line 686
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v2, :cond_0

    .line 687
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 684
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 689
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return-object v4
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 144
    iget v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mBucketId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "bucketId":Ljava/lang/String;
    sget-boolean v1, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_MEDIA_PROVIDER_FILES_TABLE:Z

    if-eqz v1, :cond_0

    .line 146
    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "bucketId"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 152
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "bucketId"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public getEventAlbumTimeInfo()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 645
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 646
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 647
    .local v3, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 648
    .local v4, "source":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 649
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 647
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 651
    .end local v4    # "source":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getTimeString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 653
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    return-object v5
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 2
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItem(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getMediaItemCount()I
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getTotalMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getMediaItemCountForBurstshot(Ljava/util/ArrayList;Z)I
    .locals 6
    .param p2, "showAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "showGroupIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .line 358
    .local v1, "count":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 359
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v5, v4, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 360
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalAlbum;

    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4, p1, p2}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemCountForBurstshot(Ljava/util/ArrayList;Z)I

    move-result v5

    add-int/2addr v1, v5

    .line 358
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 362
    .restart local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v5

    add-int/2addr v1, v5

    goto :goto_1

    .line 365
    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return v1
.end method

.method public getMediaItemFirst()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 546
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v1, 0x0

    .line 547
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v4, 0x0

    .line 549
    .local v4, "newItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v3, v6

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_3

    .line 550
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v6

    if-nez v6, :cond_1

    .line 549
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 552
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->getSortByType()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemFirst(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 553
    .local v2, "mediaLists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 555
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "newItem":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 556
    .restart local v4    # "newItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_2

    iget-object v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mComparator:Ljava/util/Comparator;

    invoke-interface {v6, v4, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v6

    if-gez v6, :cond_0

    .line 558
    :cond_2
    move-object v1, v4

    goto :goto_1

    .line 561
    .end local v2    # "mediaLists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_3
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    return-object v5
.end method

.method public getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 1
    .param p1, "start"    # I
    .param p2, "count"    # I
    .param p4, "showAll"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    .local p3, "showGroupIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItem(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 161
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mCustomName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 162
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mCustomName:Ljava/lang/String;

    .line 178
    :cond_0
    :goto_0
    return-object v0

    .line 165
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mCameraAlbum:Z

    if-eqz v1, :cond_2

    .line 166
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDCIMName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 169
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v1, v1, v3

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/EventAlbum;

    if-eqz v1, :cond_4

    .line 170
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_3

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 178
    .end local v0    # "name":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v1

    if-nez v1, :cond_5

    const-string v1, ""

    :goto_1
    move-object v0, v1

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public getPathOnFileSystem()Ljava/lang/String;
    .locals 2

    .prologue
    .line 581
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    .line 584
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPathOnFileSystem:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedOperations()J
    .locals 6

    .prologue
    .line 394
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v4, v4

    if-nez v4, :cond_0

    const-wide/16 v2, 0x0

    .line 395
    .local v2, "supported":J
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v4

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 396
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v2, v4

    .line 395
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 394
    .end local v0    # "i":I
    .end local v1    # "n":I
    .end local v2    # "supported":J
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 398
    .restart local v0    # "i":I
    .restart local v1    # "n":I
    .restart local v2    # "supported":J
    :cond_1
    return-wide v2
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 2

    .prologue
    .line 694
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    return-object v0
.end method

.method public getTotalMediaItemCount()I
    .locals 6

    .prologue
    .line 349
    const/4 v1, 0x0

    .line 350
    .local v1, "count":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 351
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v5

    add-int/2addr v1, v5

    .line 350
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 353
    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return v1
.end method

.method public hasLocation()Z
    .locals 5

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 675
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->hasLocation()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 676
    const/4 v4, 0x1

    .line 678
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_1
    return v4

    .line 674
    .restart local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 678
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public haveBurstShotImages()Z
    .locals 6

    .prologue
    .line 588
    const/4 v3, 0x0

    .line 589
    .local v3, "result":Z
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 590
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v5, v4, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-eqz v5, :cond_0

    .line 591
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalAlbum;

    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalAlbum;->haveBurstShotImages()Z

    move-result v3

    .line 592
    if-eqz v3, :cond_0

    .line 593
    const/4 v5, 0x1

    .line 597
    :goto_1
    return v5

    .line 589
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v5, v3

    .line 597
    goto :goto_1
.end method

.method public hide(Ljava/lang/String;)Z
    .locals 13
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 602
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v10

    .line 603
    .local v10, "srcAlbumPath":Ljava/lang/String;
    const/4 v5, 0x0

    .line 604
    .local v5, "dstAlbumPath":Ljava/lang/String;
    const/4 v9, 0x0

    .line 605
    .local v9, "srcAlbum":Ljava/io/File;
    const/4 v3, 0x0

    .line 607
    .local v3, "dstAlbum":Ljava/io/File;
    if-eqz v10, :cond_0

    .line 608
    new-instance v9, Ljava/io/File;

    .end local v9    # "srcAlbum":Ljava/io/File;
    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 609
    .restart local v9    # "srcAlbum":Ljava/io/File;
    :cond_0
    if-nez v9, :cond_2

    .line 630
    :cond_1
    :goto_0
    return v11

    .line 612
    :cond_2
    const/4 v1, 0x1

    .line 613
    .local v1, "count":I
    new-instance v2, Ljava/io/File;

    sget-object v12, Lcom/sec/android/gallery3d/data/HiddenSource;->HIDE_PATH:Ljava/lang/String;

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 614
    .local v2, "desAlbumParent":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 615
    .local v4, "dstAlbumName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    .end local v3    # "dstAlbum":Ljava/io/File;
    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 616
    .restart local v3    # "dstAlbum":Ljava/io/File;
    :goto_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 617
    new-instance v3, Ljava/io/File;

    .end local v3    # "dstAlbum":Ljava/io/File;
    invoke-static {v4, v1}, Lcom/sec/android/gallery3d/common/Utils;->addPostfix(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v3, v2, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 618
    .restart local v3    # "dstAlbum":Ljava/io/File;
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 620
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_4

    .line 621
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 623
    :cond_4
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    .line 625
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_2
    if-ge v6, v7, :cond_5

    aget-object v8, v0, v6

    .line 626
    .local v8, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v8, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->hide(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 625
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 630
    .end local v8    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    const/4 v11, 0x1

    goto :goto_0
.end method

.method public invalidateCacheForBurstshot()V
    .locals 3

    .prologue
    .line 136
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v1, v2

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 137
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    if-eqz v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mFetcherForBurstshot:[Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum$FetchCache;->invalidate()V

    .line 136
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_1
    return-void
.end method

.method public isCameraAlbum()Z
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mCameraAlbum:Z

    return v0
.end method

.method public isCameraRoll()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 95
    iget-object v5, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v5, v5

    if-nez v5, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v4

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 97
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 99
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public isLeafAlbum()Z
    .locals 1

    .prologue
    .line 508
    const/4 v0, 0x1

    return v0
.end method

.method public isPhotoFrameAlbum()Z
    .locals 1

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mPhotoFrameAlbum:Z

    return v0
.end method

.method public isSuggestionEvent()Z
    .locals 5

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 659
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->isSuggestionEvent()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 660
    const/4 v4, 0x1

    .line 662
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_1
    return v4

    .line 658
    .restart local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 662
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->notifyContentChanged()V

    .line 388
    return-void
.end method

.method public reload()J
    .locals 8

    .prologue
    .line 370
    monitor-enter p0

    .line 371
    const/4 v0, 0x0

    .line 372
    .local v0, "changed":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    array-length v2, v3

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 373
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mDataVersion:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    const/4 v0, 0x1

    .line 372
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 375
    :cond_1
    if-eqz v0, :cond_2

    .line 376
    invoke-static {}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->nextVersionNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mDataVersion:J

    .line 377
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mEventAlbumTimeInfo:Ljava/lang/String;

    .line 378
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->updateData()V

    .line 379
    invoke-direct {p0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->invalidateCache()V

    .line 381
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mDataVersion:J

    return-wide v4

    .line 381
    .end local v2    # "n":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public rotate(I)V
    .locals 4
    .param p1, "degrees"    # I

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 412
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->rotate(I)V

    .line 411
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 414
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method

.method public setCameraAlbum(Z)V
    .locals 0
    .param p1, "isCamAlbum"    # Z

    .prologue
    .line 529
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mCameraAlbum:Z

    .line 530
    return-void
.end method

.method public setSuggestionEvent(I)V
    .locals 4
    .param p1, "isSuggestion"    # I

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 668
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->setSuggestionEvent(I)V

    .line 667
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 670
    .end local v3    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method

.method public show()Z
    .locals 5

    .prologue
    .line 635
    iget-object v4, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_1

    .line 636
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->mSources:[Lcom/sec/android/gallery3d/data/MediaSet;

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 637
    .local v3, "source":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->show()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    .line 640
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "source":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_1
    return v4

    .line 636
    .restart local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "source":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 640
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "source":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method
