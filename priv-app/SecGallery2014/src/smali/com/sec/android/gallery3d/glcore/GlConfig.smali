.class public Lcom/sec/android/gallery3d/glcore/GlConfig;
.super Ljava/lang/Object;
.source "GlConfig.java"


# static fields
.field public static CRIT_CENTER:I

.field public static CRIT_LEFT_BOTTOM:I

.field public static CRIT_LEFT_TOP:I


# instance fields
.field public mContext:Landroid/content/Context;

.field public mHalfHeight:F

.field public mHalfWidth:F

.field public mHeight:F

.field public mProspectMatix:[F

.field private mTempMatrix1:[F

.field private mTempMatrix2:[F

.field public mWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/gallery3d/glcore/GlConfig;->CRIT_LEFT_TOP:I

    .line 8
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/gallery3d/glcore/GlConfig;->CRIT_LEFT_BOTTOM:I

    .line 9
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/gallery3d/glcore/GlConfig;->CRIT_CENTER:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v1, 0x10

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix1:[F

    .line 17
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    .line 20
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public getProspectMatrix()[F
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mProspectMatix:[F

    return-object v0
.end method

.method public getViewPos(Lcom/sec/android/gallery3d/glcore/GlPos2D;Lcom/sec/android/gallery3d/glcore/GlPos;I)V
    .locals 9
    .param p1, "rendPos"    # Lcom/sec/android/gallery3d/glcore/GlPos2D;
    .param p2, "pos"    # Lcom/sec/android/gallery3d/glcore/GlPos;
    .param p3, "criterion"    # I

    .prologue
    const/16 v8, 0xd

    const/16 v7, 0xc

    const/4 v1, 0x0

    const/16 v6, 0xf

    .line 39
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix1:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix1:[F

    iget v2, p2, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    aput v2, v0, v7

    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix1:[F

    iget v2, p2, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    aput v2, v0, v8

    .line 42
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix1:[F

    const/16 v2, 0xe

    iget v3, p2, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    aput v3, v0, v2

    .line 43
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mProspectMatix:[F

    iget-object v4, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix1:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 44
    sget v0, Lcom/sec/android/gallery3d/glcore/GlConfig;->CRIT_LEFT_TOP:I

    if-ne p3, v0, :cond_0

    .line 45
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v7

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v6

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 46
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v8

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v6

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 54
    :goto_0
    return-void

    .line 47
    :cond_0
    sget v0, Lcom/sec/android/gallery3d/glcore/GlConfig;->CRIT_LEFT_BOTTOM:I

    if-ne p3, v0, :cond_1

    .line 48
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v7

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v6

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 49
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v8

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v2, v2, v6

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    goto :goto_0

    .line 51
    :cond_1
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v1, v1, v7

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v1, v1, v6

    div-float/2addr v0, v1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 52
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v1, v1, v8

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mTempMatrix2:[F

    aget v1, v1, v6

    div-float/2addr v0, v1

    iput v0, p1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    goto :goto_0
.end method

.method public setProspectMatrix([F)V
    .locals 0
    .param p1, "matrix"    # [F

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mProspectMatix:[F

    .line 32
    return-void
.end method

.method public setSize(FF)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 24
    iput p1, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mWidth:F

    .line 25
    iput p2, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHeight:F

    .line 26
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mWidth:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfWidth:F

    .line 27
    iget v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHeight:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/gallery3d/glcore/GlConfig;->mHalfHeight:F

    .line 28
    return-void
.end method
