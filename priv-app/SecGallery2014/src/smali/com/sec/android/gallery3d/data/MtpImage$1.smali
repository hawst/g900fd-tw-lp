.class Lcom/sec/android/gallery3d/data/MtpImage$1;
.super Ljava/lang/Object;
.source "MtpImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/MtpImage;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/MtpImage;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/MtpImage;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/MtpImage$1;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v0, 0x0

    .line 89
    const-string v2, "MtpImage"

    const-string v3, "requestImage start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v2, p0, Lcom/sec/android/gallery3d/data/MtpImage$1;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    # getter for: Lcom/sec/android/gallery3d/data/MtpImage;->mMtpContext:Lcom/sec/android/gallery3d/data/MtpContext;
    invoke-static {v2}, Lcom/sec/android/gallery3d/data/MtpImage;->access$200(Lcom/sec/android/gallery3d/data/MtpImage;)Lcom/sec/android/gallery3d/data/MtpContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MtpContext;->getMtpClient()Lcom/sec/android/gallery3d/data/MtpClient;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/data/MtpImage$1;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    # getter for: Lcom/sec/android/gallery3d/data/MtpImage;->mDeviceId:I
    invoke-static {v3}, Lcom/sec/android/gallery3d/data/MtpImage;->access$000(Lcom/sec/android/gallery3d/data/MtpImage;)I

    move-result v3

    invoke-static {v3}, Landroid/hardware/usb/UsbDevice;->getDeviceName(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/MtpImage$1;->this$0:Lcom/sec/android/gallery3d/data/MtpImage;

    # getter for: Lcom/sec/android/gallery3d/data/MtpImage;->mObjectId:I
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/MtpImage;->access$100(Lcom/sec/android/gallery3d/data/MtpImage;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/data/MtpClient;->getThumbnail(Ljava/lang/String;I)[B

    move-result-object v1

    .line 92
    .local v1, "thumbnail":[B
    if-nez v1, :cond_1

    .line 93
    const-string v2, "MtpImage"

    const-string v3, "decoding thumbnail failed"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_0
    :goto_0
    return-object v0

    .line 97
    :cond_1
    invoke-static {p1, v1, v0}, Lcom/sec/android/gallery3d/data/DecodeUtils;->decode(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;[BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 98
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 99
    const-string v2, "MtpImage"

    const-string/jumbo v3, "thumbnail crop failed"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/MtpImage$1;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
