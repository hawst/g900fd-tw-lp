.class public Lcom/sec/android/gallery3d/data/TimeAllAlbum;
.super Lcom/sec/android/gallery3d/data/MediaSet;
.source "TimeAllAlbum.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private final mSubSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/Path;)V
    .locals 2
    .param p1, "baseSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 20
    invoke-static {}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->nextVersionNumber()J

    move-result-wide v0

    invoke-direct {p0, p2, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;-><init>(Lcom/sec/android/gallery3d/data/Path;J)V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    .line 21
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 22
    return-void
.end method

.method private addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 5
    .param p1, "set"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 66
    const/4 v1, 0x0

    .line 67
    .local v1, "media_set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez p1, :cond_1

    .line 68
    sget-object v3, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    const-string v4, "set is null"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    return-void

    .line 71
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 74
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 75
    if-nez v1, :cond_2

    .line 73
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_1
.end method


# virtual methods
.method public addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 186
    return-void
.end method

.method public getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getCameraBucketId()I
    .locals 2

    .prologue
    .line 175
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 176
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->isCameraPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v1

    .line 180
    :goto_1
    return v1

    .line 175
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getIndexOfItemEx(Lcom/sec/android/gallery3d/data/Path;I)I
    .locals 17
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "hint"    # I

    .prologue
    .line 200
    sget-object v14, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getIndexOfItemEx : path "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "hint "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/16 v2, 0x20

    .line 206
    .local v2, "batchCnt":I
    const/4 v14, 0x0

    add-int/lit8 v15, p2, -0x10

    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 207
    .local v11, "start":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v2}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v9

    .line 208
    .local v9, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->getIndexOf(Lcom/sec/android/gallery3d/data/Path;Ljava/util/ArrayList;)I

    move-result v6

    .line 209
    .local v6, "index":I
    const/4 v14, -0x1

    if-eq v6, v14, :cond_0

    .line 210
    add-int v4, v11, v6

    .line 251
    :goto_0
    return v4

    .line 213
    :cond_0
    if-nez v11, :cond_1

    move v11, v2

    .line 214
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v2}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v9

    .line 215
    if-nez v9, :cond_2

    .line 216
    const/4 v4, -0x1

    goto :goto_0

    .line 213
    :cond_1
    const/4 v11, 0x0

    goto :goto_1

    .line 218
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v8, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v10, 0x0

    .line 220
    .local v10, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v13

    .line 221
    .local v13, "subSetSize":I
    const/4 v12, 0x0

    .line 222
    .local v12, "subItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v13, :cond_5

    .line 224
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v10, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    if-nez v10, :cond_4

    .line 222
    :cond_3
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 225
    :catch_0
    move-exception v3

    .line 226
    .local v3, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v14, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "IndexOutOfBoundsException : invalid index "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", size is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 233
    .end local v3    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_4
    instance-of v14, v10, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v14, :cond_3

    move-object v14, v10

    .line 234
    check-cast v14, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaItems()Ljava/util/ArrayList;

    move-result-object v12

    .line 236
    if-eqz v12, :cond_3

    .line 237
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 241
    :cond_5
    const/4 v4, 0x0

    .line 243
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/Path;

    .line 244
    .local v7, "item":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 245
    sget-object v14, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getIndexOfItemEx() : Found from all item : i "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 248
    :cond_6
    add-int/lit8 v4, v4, 0x1

    .line 249
    goto :goto_4

    .line 250
    .end local v7    # "item":Lcom/sec/android/gallery3d/data/Path;
    :cond_7
    sget-object v14, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getIndexOfItemEx() : find fail after search all item count "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const/4 v4, -0x1

    goto/16 :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItem(II)Ljava/util/ArrayList;
    .locals 11
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    sget-object v8, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getMediaItem : start is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " count is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x0

    .line 91
    .local v4, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    move v3, p2

    .line 92
    .local v3, "remain":I
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    .line 93
    .local v7, "subSetSize":I
    const/4 v6, 0x0

    .line 94
    .local v6, "subItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v7, :cond_5

    .line 96
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .restart local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_1
    if-nez v4, :cond_1

    .line 94
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    .end local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 99
    const/4 v4, 0x0

    .restart local v4    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    goto :goto_1

    .line 104
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    .line 105
    .local v5, "subCount":I
    if-le p1, v5, :cond_2

    .line 106
    sub-int/2addr p1, v5

    .line 107
    goto :goto_2

    .line 109
    :cond_2
    sub-int/2addr v5, p1

    .line 110
    if-le v5, v3, :cond_3

    .line 111
    move v5, v3

    .line 112
    :cond_3
    invoke-virtual {v4, p1, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v6

    .line 113
    if-eqz v6, :cond_4

    .line 114
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 115
    :cond_4
    const/4 p1, 0x0

    .line 116
    sub-int/2addr v3, v5

    .line 117
    if-gtz v3, :cond_0

    .line 123
    .end local v5    # "subCount":I
    :cond_5
    return-object v2
.end method

.method public getMediaItemCount()I
    .locals 2

    .prologue
    .line 60
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v0

    monitor-exit v1

    return v0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMediaItemEx(II)Ljava/util/ArrayList;
    .locals 12
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    sget-object v9, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getMediaItemEx : start is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " count is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v5, 0x0

    .line 131
    .local v5, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    move v4, p2

    .line 132
    .local v4, "remain":I
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v8

    .line 133
    .local v8, "subSetSize":I
    const/4 v7, 0x0

    .line 134
    .local v7, "subItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v8, :cond_5

    .line 136
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    if-nez v5, :cond_1

    .line 134
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 137
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    sget-object v9, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IndexOutOfBoundsException : invalid index "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", size is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 145
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_1
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v6

    .line 146
    .local v6, "subCount":I
    if-le p1, v6, :cond_2

    .line 147
    sub-int/2addr p1, v6

    .line 148
    goto :goto_1

    .line 150
    :cond_2
    sub-int/2addr v6, p1

    .line 151
    if-le v6, v4, :cond_3

    .line 152
    move v6, v4

    .line 153
    :cond_3
    invoke-virtual {v5, p1, v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemEx(II)Ljava/util/ArrayList;

    move-result-object v7

    .line 156
    if-eqz v7, :cond_4

    .line 157
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 158
    :cond_4
    const/4 p1, 0x0

    .line 159
    sub-int/2addr v4, v6

    .line 160
    if-gtz v4, :cond_0

    .line 166
    .end local v6    # "subCount":I
    :cond_5
    return-object v3
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "TimeAll"

    return-object v0
.end method

.method public getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    return-object v0
.end method

.method public onContentDirty()V
    .locals 0

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->notifyContentChanged()V

    .line 56
    return-void
.end method

.method public reload()J
    .locals 8

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 32
    .local v0, "isDirty":Z
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 33
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mDataVersion:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    const/4 v0, 0x1

    .line 34
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    sget-object v1, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reload : mDataVersion is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mDataVersion:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", and isDirty value is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    if-eqz v0, :cond_2

    .line 37
    invoke-static {}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->nextVersionNumber()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mDataVersion:J

    .line 38
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 39
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 45
    :cond_0
    :goto_1
    iget-wide v2, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mDataVersion:J

    return-wide v2

    .line 33
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 34
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 41
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mSubSets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->addMediaSets(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_1
.end method

.method public removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/data/ContentListener;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 191
    return-void
.end method

.method public setSortByType(Lcom/sec/android/gallery3d/app/GalleryApp;IZ)V
    .locals 2
    .param p1, "application"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "type"    # I
    .param p3, "isTime"    # Z

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/TimeAllAlbum;->mBaseSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->setForceReload(Z)V

    .line 172
    :cond_0
    return-void
.end method
