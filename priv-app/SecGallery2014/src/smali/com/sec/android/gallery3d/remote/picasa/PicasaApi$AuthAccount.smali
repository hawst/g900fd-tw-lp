.class public final Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;
.super Ljava/lang/Object;
.source "PicasaApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AuthAccount"
.end annotation


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final authToken:Ljava/lang/String;

.field public final user:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "authToken"    # Ljava/lang/String;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->user:Ljava/lang/String;

    .line 90
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->authToken:Ljava/lang/String;

    .line 91
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi$AuthAccount;->account:Landroid/accounts/Account;

    .line 92
    return-void
.end method
