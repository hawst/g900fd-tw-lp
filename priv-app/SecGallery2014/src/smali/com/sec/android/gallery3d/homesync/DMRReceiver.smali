.class Lcom/sec/android/gallery3d/homesync/DMRReceiver;
.super Ljava/lang/Thread;
.source "DMRReceiver.java"


# instance fields
.field controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

.field mDis:Ljava/io/DataInputStream;

.field mSocket:Ljava/net/Socket;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    return-void
.end method

.method private bytes2float([BI)F
    .locals 12
    .param p1, "arr"    # [B
    .param p2, "start"    # I

    .prologue
    .line 276
    const/4 v2, 0x0

    .line 277
    .local v2, "floatValue":F
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v7, :cond_2

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v7, :cond_2

    .line 278
    const/4 v3, 0x0

    .line 279
    .local v3, "i":I
    const/4 v4, 0x4

    .line 280
    .local v4, "len":I
    const/4 v1, 0x0

    .line 281
    .local v1, "cnt":I
    new-array v6, v4, [B

    .line 283
    .local v6, "tmp":[B
    move v3, p2

    :goto_0
    add-int v7, p2, v4

    if-ge v3, v7, :cond_0

    .line 284
    aget-byte v7, p1, v3

    aput-byte v7, v6, v1

    .line 285
    add-int/lit8 v1, v1, 0x1

    .line 283
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 288
    :cond_0
    const/4 v0, 0x0

    .line 289
    .local v0, "accum":I
    const/4 v3, 0x0

    .line 290
    const/4 v5, 0x0

    .local v5, "shiftBy":I
    :goto_1
    const/16 v7, 0x20

    if-ge v5, v7, :cond_1

    .line 291
    int-to-long v8, v0

    aget-byte v7, v6, v3

    and-int/lit16 v7, v7, 0xff

    int-to-long v10, v7

    shl-long/2addr v10, v5

    or-long/2addr v8, v10

    long-to-int v0, v8

    .line 292
    add-int/lit8 v3, v3, 0x1

    .line 290
    add-int/lit8 v5, v5, 0x8

    goto :goto_1

    .line 294
    :cond_1
    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v2

    .line 297
    .end local v0    # "accum":I
    .end local v1    # "cnt":I
    .end local v3    # "i":I
    .end local v4    # "len":I
    .end local v5    # "shiftBy":I
    .end local v6    # "tmp":[B
    :cond_2
    return v2
.end method

.method private bytes2int([BI)I
    .locals 7
    .param p1, "arr"    # [B
    .param p2, "start"    # I

    .prologue
    .line 301
    const/4 v3, 0x0

    .line 302
    .local v3, "result":I
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v5, :cond_1

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v5, :cond_1

    .line 303
    const/4 v1, 0x0

    .line 304
    .local v1, "i":I
    const/4 v2, 0x4

    .line 305
    .local v2, "len":I
    const/4 v0, 0x0

    .line 306
    .local v0, "cnt":I
    new-array v4, v2, [B

    .line 308
    .local v4, "tmp":[B
    move v1, p2

    :goto_0
    add-int v5, p2, v2

    if-ge v1, v5, :cond_0

    .line 309
    aget-byte v5, p1, v1

    aput-byte v5, v4, v0

    .line 310
    add-int/lit8 v0, v0, 0x1

    .line 308
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 313
    :cond_0
    const/4 v5, 0x0

    aget-byte v5, v4, v5

    shl-int/lit8 v5, v5, 0x18

    const/4 v6, 0x1

    aget-byte v6, v4, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    add-int/2addr v5, v6

    const/4 v6, 0x2

    aget-byte v6, v4, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    add-int/2addr v5, v6

    const/4 v6, 0x3

    aget-byte v6, v4, v6

    and-int/lit16 v6, v6, 0xff

    add-int v3, v5, v6

    .line 316
    .end local v0    # "cnt":I
    .end local v1    # "i":I
    .end local v2    # "len":I
    .end local v4    # "tmp":[B
    :cond_1
    return v3
.end method

.method private receiveAbsZoomPacket([B)V
    .locals 4
    .param p1, "data"    # [B

    .prologue
    .line 248
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v3, :cond_0

    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "posX":F
    const/4 v1, 0x0

    .line 251
    .local v1, "posY":F
    const/4 v2, 0x0

    .line 253
    .local v2, "zoomRatio":F
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->bytes2float([BI)F

    move-result v0

    .line 254
    const/4 v3, 0x5

    invoke-direct {p0, p1, v3}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->bytes2float([BI)F

    move-result v1

    .line 255
    const/16 v3, 0x9

    invoke-direct {p0, p1, v3}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->bytes2float([BI)F

    move-result v2

    .line 257
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    if-eqz v3, :cond_0

    .line 258
    iget-object v3, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    invoke-interface {v3, v0, v1, v2}, Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;->zoom(FFF)V

    .line 261
    .end local v0    # "posX":F
    .end local v1    # "posY":F
    .end local v2    # "zoomRatio":F
    :cond_0
    return-void
.end method

.method private receiveDoubleTabPacket([B)V
    .locals 4
    .param p1, "data"    # [B

    .prologue
    .line 221
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v2, :cond_0

    .line 222
    const/4 v0, 0x0

    .line 223
    .local v0, "posX":F
    const/4 v1, 0x0

    .line 225
    .local v1, "posY":F
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->bytes2float([BI)F

    move-result v0

    .line 226
    const/4 v2, 0x5

    invoke-direct {p0, p1, v2}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->bytes2float([BI)F

    move-result v1

    .line 228
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    if-eqz v2, :cond_0

    .line 229
    iget-object v2, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v1, v3}, Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;->zoom(FFF)V

    .line 232
    .end local v0    # "posX":F
    .end local v1    # "posY":F
    :cond_0
    return-void
.end method

.method private receiveMovePacket([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 264
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 273
    :cond_0
    return-void
.end method

.method private receiveRotatePacket([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 208
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v1, :cond_0

    .line 209
    const/4 v0, 0x0

    .line 211
    .local v0, "angle":I
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->bytes2int([BI)I

    move-result v0

    .line 214
    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    invoke-interface {v1, v0}, Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;->setViewAngle(I)V

    .line 218
    .end local v0    # "angle":I
    :cond_0
    return-void
.end method

.method private receiveScaleBeginPacket([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 128
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 135
    :cond_0
    return-void
.end method

.method private receiveScaleEndPacket([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 155
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 165
    :cond_0
    return-void
.end method

.method private receiveSliderShowPacket([B)V
    .locals 3
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x1

    .line 235
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v1, :cond_0

    .line 238
    aget-byte v1, p1, v2

    if-ne v1, v2, :cond_0

    .line 239
    const/4 v0, 0x1

    .line 245
    :cond_0
    return-void
.end method

.method private receiveTouchBeganPacket([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 168
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 177
    :cond_0
    return-void
.end method

.method private receiveTouchEndedPacket([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 194
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 205
    :cond_0
    return-void
.end method

.method private receiveTouchMovedPacket([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 180
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 191
    :cond_0
    return-void
.end method

.method private receiveZoomPacket([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 138
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 152
    :cond_0
    return-void
.end method


# virtual methods
.method public SetReceiverSocket(Ljava/net/Socket;)V
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    .line 43
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 44
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->mSocket:Ljava/net/Socket;

    .line 46
    :cond_0
    return-void
.end method

.method public registerControllerActionHandler(Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;)V
    .locals 1
    .param p1, "actionHandler"    # Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .prologue
    .line 53
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 54
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .line 56
    :cond_0
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 70
    :try_start_0
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v5, :cond_1

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v5, :cond_1

    .line 71
    new-instance v5, Ljava/io/DataInputStream;

    iget-object v6, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->mSocket:Ljava/net/Socket;

    invoke-virtual {v6}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->mDis:Ljava/io/DataInputStream;

    .line 72
    const/4 v3, 0x0

    .line 73
    .local v3, "readSize":I
    const/16 v4, 0x1d

    .line 74
    .local v4, "size":I
    new-array v1, v4, [B

    .line 76
    .local v1, "data":[B
    const/4 v0, 0x1

    .line 77
    .local v0, "bExit":Z
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 78
    iget-object v5, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v5, v1}, Ljava/io/DataInputStream;->read([B)I

    move-result v3

    .line 79
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 82
    const/4 v5, 0x0

    aget-byte v5, v1, v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 84
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveZoomPacket([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    .end local v0    # "bExit":Z
    .end local v1    # "data":[B
    .end local v3    # "readSize":I
    .end local v4    # "size":I
    :catch_0
    move-exception v2

    .line 123
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 125
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :goto_1
    return-void

    .line 87
    .restart local v0    # "bExit":Z
    .restart local v1    # "data":[B
    .restart local v3    # "readSize":I
    .restart local v4    # "size":I
    :pswitch_1
    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveTouchBeganPacket([B)V

    goto :goto_0

    .line 90
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveTouchMovedPacket([B)V

    goto :goto_0

    .line 93
    :pswitch_3
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveTouchEndedPacket([B)V

    goto :goto_0

    .line 96
    :pswitch_4
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveRotatePacket([B)V

    goto :goto_0

    .line 99
    :pswitch_5
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveScaleBeginPacket([B)V

    goto :goto_0

    .line 102
    :pswitch_6
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveScaleEndPacket([B)V

    goto :goto_0

    .line 105
    :pswitch_7
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveDoubleTabPacket([B)V

    goto :goto_0

    .line 108
    :pswitch_8
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveSliderShowPacket([B)V

    goto :goto_0

    .line 111
    :pswitch_9
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveAbsZoomPacket([B)V

    goto :goto_0

    .line 114
    :pswitch_a
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->receiveMovePacket([B)V

    goto :goto_0

    .line 120
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->mDis:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public unRegisterControllerActionHandler()V
    .locals 1

    .prologue
    .line 62
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/homesync/DMRReceiver;->controllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .line 65
    :cond_0
    return-void
.end method
