.class public Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;
.super Ljava/lang/Object;
.source "PhotoViewIcon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BurstShotIcon"
.end annotation


# instance fields
.field private mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

.field private mRect:Landroid/graphics/Rect;

.field private mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)V
    .locals 0

    .prologue
    .line 836
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    .prologue
    .line 836
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;
    .param p1, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 836
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Lcom/sec/android/gallery3d/glrenderer/Texture;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    .prologue
    .line 836
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glrenderer/Texture;

    .prologue
    .line 836
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    return-object p1
.end method

.method static synthetic access$702(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glrenderer/Texture;

    .prologue
    .line 836
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    return-object p1
.end method


# virtual methods
.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 14
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "type"    # I
    .param p3, "selectonModeBarHeight"    # I

    .prologue
    .line 842
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 843
    .local v6, "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;

    .line 845
    .local v5, "iconRect":Landroid/graphics/Rect;
    packed-switch p2, :pswitch_data_0

    .line 856
    :goto_0
    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->access$400(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v9

    .line 857
    .local v9, "positionController":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getUseViewSize()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 917
    :cond_0
    :goto_1
    return-void

    .line 847
    .end local v9    # "positionController":Lcom/sec/android/gallery3d/ui/PositionController;
    :pswitch_0
    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z
    invoke-static {v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->access$200(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Z

    move-result v12

    if-eqz v12, :cond_1

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 848
    :goto_2
    goto :goto_0

    .line 847
    :cond_1
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    goto :goto_2

    .line 850
    :pswitch_1
    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z
    invoke-static {v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->access$300(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Z

    move-result v12

    if-eqz v12, :cond_2

    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 851
    :goto_3
    goto :goto_0

    .line 850
    :cond_2
    iget-object v6, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;

    goto :goto_3

    .line 860
    .restart local v9    # "positionController":Lcom/sec/android/gallery3d/ui/PositionController;
    :cond_3
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v7

    .line 862
    .local v7, "imageBounds":Landroid/graphics/Rect;
    iget v12, v7, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 863
    .local v8, "left":I
    iget v12, v7, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 865
    .local v10, "top":I
    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->access$500(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v3

    .line 866
    .local v3, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    if-eqz v3, :cond_0

    .line 868
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v12

    add-int v0, v12, p3

    .line 869
    .local v0, "actionBarHeight":I
    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mViewMode:I
    invoke-static {v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->access$600(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)I

    move-result v12

    invoke-virtual {v3, v12}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewIconTopPaddingPixel(I)I

    move-result v2

    .line 870
    .local v2, "detailViewIconTopPadding":I
    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mViewMode:I
    invoke-static {v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->access$600(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)I

    move-result v12

    invoke-virtual {v3, v12}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewIconLeftPaddingPixel(I)I

    move-result v1

    .line 872
    .local v1, "detailViewIconLeftPadding":I
    if-eqz v6, :cond_0

    .line 873
    if-gtz v8, :cond_4

    iget-object v12, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->this$0:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-static {v12}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->access$400(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 874
    :cond_4
    add-int v12, v8, v1

    iput v12, v5, Landroid/graphics/Rect;->left:I

    .line 879
    :goto_4
    packed-switch p2, :pswitch_data_1

    .line 891
    :goto_5
    iget v12, v5, Landroid/graphics/Rect;->left:I

    invoke-interface {v6}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v13

    add-int/2addr v12, v13

    iput v12, v5, Landroid/graphics/Rect;->right:I

    .line 893
    add-int v12, v0, v2

    if-ge v10, v12, :cond_6

    .line 894
    add-int v12, v0, v2

    iput v12, v5, Landroid/graphics/Rect;->top:I

    .line 899
    :goto_6
    invoke-interface {v6}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v4

    .line 900
    .local v4, "h":I
    packed-switch p2, :pswitch_data_2

    .line 914
    :goto_7
    iget v12, v5, Landroid/graphics/Rect;->top:I

    invoke-interface {v6}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v13

    add-int/2addr v12, v13

    iput v12, v5, Landroid/graphics/Rect;->bottom:I

    .line 915
    iget v12, v5, Landroid/graphics/Rect;->left:I

    iget v13, v5, Landroid/graphics/Rect;->top:I

    invoke-interface {v6, p1, v12, v13}, Lcom/sec/android/gallery3d/glrenderer/Texture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto/16 :goto_1

    .line 876
    .end local v4    # "h":I
    :cond_5
    iput v1, v5, Landroid/graphics/Rect;->left:I

    goto :goto_4

    .line 884
    :pswitch_2
    invoke-interface {v6}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v11

    .line 885
    .local v11, "w":I
    iget v12, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v12, v11

    iput v12, v5, Landroid/graphics/Rect;->left:I

    goto :goto_5

    .line 896
    .end local v11    # "w":I
    :cond_6
    add-int v12, v10, v2

    iput v12, v5, Landroid/graphics/Rect;->top:I

    goto :goto_6

    .line 902
    .restart local v4    # "h":I
    :pswitch_3
    iget v12, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v12, v4

    iput v12, v5, Landroid/graphics/Rect;->top:I

    goto :goto_7

    .line 905
    :pswitch_4
    iget v12, v5, Landroid/graphics/Rect;->top:I

    mul-int/lit8 v13, v4, 0x2

    add-int/2addr v12, v13

    iput v12, v5, Landroid/graphics/Rect;->top:I

    goto :goto_7

    .line 908
    :pswitch_5
    iget v12, v5, Landroid/graphics/Rect;->top:I

    mul-int/lit8 v13, v4, 0x3

    add-int/2addr v12, v13

    iput v12, v5, Landroid/graphics/Rect;->top:I

    goto :goto_7

    .line 845
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 879
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 900
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
