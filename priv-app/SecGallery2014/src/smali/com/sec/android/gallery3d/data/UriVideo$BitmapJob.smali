.class Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;
.super Ljava/lang/Object;
.source "UriVideo.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/data/UriVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BitmapJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final mType:I

.field final synthetic this$0:Lcom/sec/android/gallery3d/data/UriVideo;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/data/UriVideo;I)V
    .locals 0
    .param p2, "type"    # I

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput p2, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->mType:I

    .line 114
    return-void
.end method


# virtual methods
.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    const/4 v6, 0x1

    .line 119
    iget v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->mType:I

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->getTargetSize(I)I

    move-result v2

    .line 120
    .local v2, "targetSize":I
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 121
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 122
    const/4 v0, 0x0

    .line 124
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    # getter for: Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/sec/android/gallery3d/data/UriVideo;->access$000(Lcom/sec/android/gallery3d/data/UriVideo;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 125
    const-string/jumbo v3, "test"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mUri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    # getter for: Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/sec/android/gallery3d/data/UriVideo;->access$000(Lcom/sec/android/gallery3d/data/UriVideo;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    # getter for: Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v3}, Lcom/sec/android/gallery3d/data/UriVideo;->access$100(Lcom/sec/android/gallery3d/data/UriVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    # getter for: Lcom/sec/android/gallery3d/data/UriVideo;->mUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriVideo;->access$000(Lcom/sec/android/gallery3d/data/UriVideo;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/BitmapUtils;->getVideoThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 139
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_3

    .line 140
    :cond_0
    const/4 v3, 0x0

    .line 156
    :goto_0
    return-object v3

    .line 129
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/UriVideo;->setBroken(Z)V

    .line 130
    iget v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->mType:I

    if-ne v3, v6, :cond_2

    .line 131
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    # getter for: Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriVideo;->access$100(Lcom/sec/android/gallery3d/data/UriVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200a8

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    .line 134
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    # getter for: Lcom/sec/android/gallery3d/data/UriVideo;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/UriVideo;->access$100(Lcom/sec/android/gallery3d/data/UriVideo;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenMovieThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    .line 143
    :cond_3
    iget v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->mType:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->mType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    .line 144
    :cond_4
    invoke-static {v0, v2, v6}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 151
    :goto_1
    if-eqz v0, :cond_5

    .line 152
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    # setter for: Lcom/sec/android/gallery3d/data/UriVideo;->mWidth:I
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/UriVideo;->access$202(Lcom/sec/android/gallery3d/data/UriVideo;I)I

    .line 153
    iget-object v3, p0, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->this$0:Lcom/sec/android/gallery3d/data/UriVideo;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    # setter for: Lcom/sec/android/gallery3d/data/UriVideo;->mHeight:I
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/UriVideo;->access$302(Lcom/sec/android/gallery3d/data/UriVideo;I)I

    :cond_5
    move-object v3, v0

    .line 156
    goto :goto_0

    .line 147
    :cond_6
    invoke-static {v0, v2, v6}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/data/UriVideo$BitmapJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
