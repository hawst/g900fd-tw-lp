.class public Lcom/sec/android/gallery3d/ui/ImageNoteView;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "ImageNoteView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;,
        Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;
    }
.end annotation


# static fields
.field private static final MSG_IMAGENOTE_ROTATE_GONE:I = 0x8

.field private static final MSG_IMAGENOTE_ROTATE_VISIBLE:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final USE_LATEST_RIGHT:I = -0x1


# instance fields
.field private ImageNoteIconHeight:I

.field private ImageNoteIconWidth:I

.field private LastMaxRight:I

.field private NeedtoUpdateImageNoteIcon:Z

.field private imageNotehandler:Landroid/os/Handler;

.field private isTouched:Z

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private final mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mImageNoteBmp:Landroid/graphics/Bitmap;

.field private mImageNoteIcon:Lcom/sec/android/gallery3d/glrenderer/Texture;

.field private mImageNoteIconPressed:Lcom/sec/android/gallery3d/glrenderer/Texture;

.field private mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

.field private mImageNoteViewListener:Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;

.field private final mMatrix:[F

.field private final mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

.field private final mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

.field private mProgressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field private mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;)V
    .locals 4
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p3, "model"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 95
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    .line 63
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 64
    iput v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconWidth:I

    .line 65
    iput v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconHeight:I

    .line 71
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mMatrix:[F

    .line 73
    iput-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;

    .line 74
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 76
    new-instance v0, Lcom/sec/android/gallery3d/ui/ImageNoteView$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView$1;-><init>(Lcom/sec/android/gallery3d/ui/ImageNoteView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->imageNotehandler:Landroid/os/Handler;

    .line 96
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 97
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 98
    iput-object p3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    .line 99
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 101
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0200a9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteIcon:Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 103
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0200aa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteIconPressed:Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 108
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteIcon:Lcom/sec/android/gallery3d/glrenderer/Texture;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconWidth:I

    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteIcon:Lcom/sec/android/gallery3d/glrenderer/Texture;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconHeight:I

    .line 111
    iput v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->LastMaxRight:I

    .line 112
    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setNeedtoUpdateImageNoteIcon(Z)V

    .line 113
    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setIsPressed(Z)V

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ImageNoteView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/ui/ImageNoteView;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ImageNoteView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ImageNoteView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/gallery3d/ui/ImageNoteView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ImageNoteView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/ImageNoteView;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ImageNoteView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->saveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/ImageNoteView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/ImageNoteView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private calImageNoteIconSize()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 139
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v3, :cond_1

    .line 140
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v2

    .line 141
    .local v2, "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 142
    .local v1, "imageWidth":I
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 143
    .local v0, "imageHeight":I
    iget v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconWidth:I

    if-lt v1, v3, :cond_0

    iget v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconHeight:I

    if-ge v0, v3, :cond_1

    .line 144
    :cond_0
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 147
    .end local v0    # "imageHeight":I
    .end local v1    # "imageWidth":I
    .end local v2    # "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    :goto_0
    return v3

    :cond_1
    const/4 v3, -0x1

    goto :goto_0
.end method

.method private createImageNoteBG(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 21
    .param p1, "photoFile"    # Ljava/lang/String;
    .param p2, "waterMarkerFile"    # Ljava/lang/String;
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 392
    const/16 v9, 0xc8

    .line 393
    .local v9, "DIMMING_ALPHA":I
    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int v18, v3, v4

    .line 394
    .local v18, "nCurWidth":I
    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int v17, v3, v4

    .line 395
    .local v17, "nCurHeight":I
    const/4 v12, 0x0

    .line 398
    .local v12, "bmpResult":Landroid/graphics/Bitmap;
    :try_start_0
    const-string v3, "createImageNoteBG "

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    const/4 v11, 0x0

    .line 401
    .local v11, "bmpPhoto":Landroid/graphics/Bitmap;
    const/4 v3, 0x1

    :try_start_1
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->decodeImageFile(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 402
    const-string v3, " decodeImageFile "

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 403
    const/4 v3, 0x0

    move/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v11, v0, v1, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 404
    const-string v3, " createScaledBitmap "

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409
    :goto_0
    :try_start_2
    const-string v3, " bmpInversedPhoto "

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 411
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 412
    new-instance v14, Landroid/graphics/Canvas;

    invoke-direct {v14, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 413
    .local v14, "canvas":Landroid/graphics/Canvas;
    move-object v12, v11

    .line 421
    :goto_1
    new-instance v20, Landroid/graphics/Paint;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 422
    .local v20, "textPaint":Landroid/graphics/Paint;
    const/16 v3, 0xc8

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 424
    const/4 v13, 0x0

    .line 426
    .local v13, "bmpWaterMarker":Landroid/graphics/Bitmap;
    move/from16 v0, v18

    move/from16 v1, v17

    if-lt v0, v1, :cond_5

    .line 427
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0203fc

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 434
    :goto_2
    const-string v3, " bmpWaterMarker "

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 435
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    move/from16 v0, v18

    if-le v0, v3, :cond_0

    .line 436
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    .line 437
    :cond_0
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move/from16 v0, v17

    if-le v0, v3, :cond_1

    .line 438
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v17

    .line 439
    :cond_1
    const/4 v2, 0x0

    .line 441
    .local v2, "bmpFixedWaterMarker":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_3
    move/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v13, v3, v4, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 447
    :goto_3
    if-eq v2, v13, :cond_2

    .line 448
    :try_start_4
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 450
    :cond_2
    const-string v3, " bmpFixedWaterMarker "

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 453
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 454
    .local v7, "matrix":Landroid/graphics/Matrix;
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 456
    const/4 v10, 0x0

    .line 458
    .local v10, "bmReversedFixedWaterMarker":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_5
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v10

    .line 466
    :goto_4
    if-eqz v10, :cond_3

    .line 467
    :try_start_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 470
    :cond_3
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v14, v10, v3, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 471
    const-string v3, " canvas.drawBitmap "

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 473
    const-string v3, " make photo and water marker "

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 480
    .end local v2    # "bmpFixedWaterMarker":Landroid/graphics/Bitmap;
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v10    # "bmReversedFixedWaterMarker":Landroid/graphics/Bitmap;
    .end local v11    # "bmpPhoto":Landroid/graphics/Bitmap;
    .end local v13    # "bmpWaterMarker":Landroid/graphics/Bitmap;
    .end local v14    # "canvas":Landroid/graphics/Canvas;
    .end local v20    # "textPaint":Landroid/graphics/Paint;
    :goto_5
    return-object v12

    .line 406
    .restart local v11    # "bmpPhoto":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v15

    .line 407
    .local v15, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v15}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 474
    .end local v11    # "bmpPhoto":Landroid/graphics/Bitmap;
    .end local v15    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v15

    .line 475
    .local v15, "e":Ljava/lang/Exception;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0035

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 476
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 477
    const/4 v12, 0x0

    goto :goto_5

    .line 415
    .end local v15    # "e":Ljava/lang/Exception;
    .restart local v11    # "bmpPhoto":Landroid/graphics/Bitmap;
    :cond_4
    :try_start_8
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 416
    .local v16, "mutable_bmpPhoto":Landroid/graphics/Bitmap;
    new-instance v14, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 417
    .restart local v14    # "canvas":Landroid/graphics/Canvas;
    move-object/from16 v12, v16

    .line 418
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 478
    .end local v11    # "bmpPhoto":Landroid/graphics/Bitmap;
    .end local v14    # "canvas":Landroid/graphics/Canvas;
    .end local v16    # "mutable_bmpPhoto":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v3

    throw v3

    .line 430
    .restart local v11    # "bmpPhoto":Landroid/graphics/Bitmap;
    .restart local v13    # "bmpWaterMarker":Landroid/graphics/Bitmap;
    .restart local v14    # "canvas":Landroid/graphics/Canvas;
    .restart local v20    # "textPaint":Landroid/graphics/Paint;
    :cond_5
    :try_start_9
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0203fd

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v13

    goto/16 :goto_2

    .line 443
    .restart local v2    # "bmpFixedWaterMarker":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v19

    .line 444
    .local v19, "ome":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 445
    invoke-virtual/range {v19 .. v19}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_3

    .line 461
    .end local v19    # "ome":Ljava/lang/OutOfMemoryError;
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v10    # "bmReversedFixedWaterMarker":Landroid/graphics/Bitmap;
    :catch_3
    move-exception v15

    .line 462
    .local v15, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 463
    invoke-virtual {v15}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4
.end method

.method private createThreadAndDialog()V
    .locals 2

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/android/gallery3d/ui/ImageNoteView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView$2;-><init>(Lcom/sec/android/gallery3d/ui/ImageNoteView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 538
    return-void
.end method

.method private getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 590
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNeedtoUpdateImageNoteIcon()Z
    .locals 1

    .prologue
    .line 583
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->NeedtoUpdateImageNoteIcon:Z

    return v0
.end method

.method private printLog(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "position"    # Ljava/lang/String;
    .param p2, "enter"    # Z

    .prologue
    .line 599
    return-void
.end method

.method private saveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 557
    const/4 v1, 0x0

    .line 559
    .local v1, "fileCacheItem":Ljava/io/File;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v4, :cond_0

    .line 560
    invoke-static {}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->createBGimage()Ljava/io/File;

    move-result-object v1

    .line 563
    :goto_0
    if-nez v1, :cond_1

    .line 580
    :goto_1
    return-void

    .line 562
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 566
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 567
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 570
    :cond_2
    const/4 v2, 0x0

    .line 572
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 573
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 578
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v2, v3

    .line 579
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1

    .line 575
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 578
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :goto_3
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v4

    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_3

    .line 575
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_2
.end method

.method private setIsPressed(Z)V
    .locals 0
    .param p1, "isPressed"    # Z

    .prologue
    .line 594
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->isTouched:Z

    .line 595
    return-void
.end method

.method private shouldDrawIcon()Z
    .locals 2

    .prologue
    .line 151
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    .line 152
    .local v0, "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->inSlidingAnimation()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isScrolling()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startEditImageNote(I)V
    .locals 7
    .param p1, "type"    # I

    .prologue
    const/4 v6, 0x0

    .line 541
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 542
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v1, v6

    const/4 v2, 0x1

    aput-object v0, v1, v2

    const/4 v2, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 545
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "START_IMAGE_NOTE"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 548
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V

    .line 549
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 550
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setRotateAnim(Lcom/sec/android/gallery3d/anim/FloatAnimation;)V

    .line 552
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->imageNotehandler:Landroid/os/Handler;

    const-wide/16 v4, 0x8fc

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 553
    return-void
.end method


# virtual methods
.method public dismissProgressDialog()V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 367
    :cond_0
    return-void
.end method

.method public draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v2, v0, 0x2

    .line 291
    .local v2, "x":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v3, v0, 0x2

    .line 292
    .local v3, "y":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->getWidth()I

    move-result v4

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->getHeight()I

    move-result v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 295
    .end local v2    # "x":I
    .end local v3    # "y":I
    :cond_0
    return-void
.end method

.method public getCorrectRightFromPhotoView(FFI)I
    .locals 3
    .param p1, "right"    # F
    .param p2, "left"    # F
    .param p3, "maxRight"    # I

    .prologue
    .line 246
    const/4 v2, -0x1

    if-ne p3, v2, :cond_1

    .line 247
    iget v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->LastMaxRight:I

    .line 253
    .local v1, "maxLength":I
    :goto_0
    const/4 v2, 0x0

    cmpg-float v2, p2, v2

    if-gez v2, :cond_2

    .line 254
    sub-float v2, p1, p2

    float-to-int v0, v2

    .line 255
    .local v0, "CorrectRightFromPhotoView":I
    if-le v0, v1, :cond_0

    .line 256
    float-to-int v0, p1

    .line 261
    :cond_0
    :goto_1
    return v0

    .line 249
    .end local v0    # "CorrectRightFromPhotoView":I
    .end local v1    # "maxLength":I
    :cond_1
    iput p3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->LastMaxRight:I

    .line 250
    move v1, p3

    .restart local v1    # "maxLength":I
    goto :goto_0

    .line 259
    :cond_2
    float-to-int v0, p1

    .restart local v0    # "CorrectRightFromPhotoView":I
    goto :goto_1
.end method

.method public getRotateAnim()Lcom/sec/android/gallery3d/anim/FloatAnimation;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    return-object v0
.end method

.method public layout(IIII)V
    .locals 7
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->shouldDrawIcon()Z

    move-result v5

    if-nez v5, :cond_0

    .line 121
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setNeedtoUpdateImageNoteIcon(Z)V

    .line 124
    :cond_0
    move v2, p3

    .line 125
    .local v2, "r":I
    move v4, p2

    .line 126
    .local v4, "t":I
    iget v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconWidth:I

    .line 127
    .local v1, "iconWidth":I
    iget v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconHeight:I

    .line 129
    .local v0, "iconHeight":I
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->calImageNoteIconSize()I

    move-result v3

    .line 130
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_1

    .line 131
    move v0, v3

    move v1, v3

    .line 134
    :cond_1
    sub-int v5, v2, v1

    add-int v6, v4, v0

    invoke-super {p0, v5, v4, v2, v6}, Lcom/sec/android/gallery3d/ui/GLView;->layout(IIII)V

    .line 135
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setIsPressed(Z)V

    .line 136
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    if-eqz v0, :cond_1

    .line 603
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->recycle()V

    .line 606
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 608
    :cond_1
    return-void
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 178
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/16 v4, 0x3e8

    if-ne v3, v4, :cond_1

    .line 179
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v3

    const v4, 0x7f0e02a6

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 198
    :cond_0
    :goto_0
    return v1

    .line 182
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->shouldDrawIcon()Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 183
    goto :goto_0

    .line 185
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->isTouched:Z

    if-nez v2, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 190
    .local v0, "currentPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_3

    instance-of v2, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_3

    .line 191
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setIsPressed(Z)V

    .line 193
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->startRotateOutAnim()V

    .line 198
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public removeRotationAnimation()V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V

    .line 386
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 388
    :cond_0
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 7
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 158
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->shouldDrawIcon()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getNeedtoUpdateImageNoteIcon()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->updateImageNoteIcon()V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget v4, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconWidth:I

    .line 163
    .local v4, "iconWidth":I
    iget v5, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->ImageNoteIconHeight:I

    .line 164
    .local v5, "iconHeight":I
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->calImageNoteIconSize()I

    move-result v6

    .line 165
    .local v6, "size":I
    const/4 v0, -0x1

    if-eq v6, v0, :cond_2

    .line 166
    move v5, v6

    move v4, v6

    .line 168
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->isTouched:Z

    if-eqz v0, :cond_3

    .line 169
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteIconPressed:Lcom/sec/android/gallery3d/glrenderer/Texture;

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/Texture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto :goto_0

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteIcon:Lcom/sec/android/gallery3d/glrenderer/Texture;

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/Texture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto :goto_0
.end method

.method public renderRotateAnim(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;F)V
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "angle"    # F

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 370
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getWidth()I

    move-result v1

    .line 371
    .local v1, "rootViewWidth":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getHeight()I

    move-result v0

    .line 373
    .local v0, "rootViewHeight":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mMatrix:[F

    int-to-float v3, v1

    div-float/2addr v3, v7

    int-to-float v4, v0

    div-float/2addr v4, v7

    const/high16 v5, -0x3b860000    # -1000.0f

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setViewPointMatrix([FFFF)V

    .line 374
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mMatrix:[F

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->multiplyMatrix([FI)V

    .line 376
    int-to-float v2, v1

    div-float/2addr v2, v7

    invoke-interface {p1, v2, v6, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    .line 377
    invoke-interface {p1, p2, v6, v8, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->rotate(FFFF)V

    .line 378
    invoke-interface {p1, v8}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    .line 380
    neg-int v2, v1

    int-to-float v2, v2

    div-float/2addr v2, v7

    invoke-interface {p1, v2, v6, v6}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->translate(FFF)V

    .line 381
    return-void
.end method

.method public runPhotoNoteDialog()V
    .locals 6

    .prologue
    .line 611
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 612
    .local v1, "runPhotoNote":Landroid/app/AlertDialog$Builder;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e002b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 613
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e021c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 615
    .local v0, "body":Ljava/lang/String;
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e00db

    new-instance v5, Lcom/sec/android/gallery3d/ui/ImageNoteView$4;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView$4;-><init>(Lcom/sec/android/gallery3d/ui/ImageNoteView;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e0046

    new-instance v5, Lcom/sec/android/gallery3d/ui/ImageNoteView$3;

    invoke-direct {v5, p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView$3;-><init>(Lcom/sec/android/gallery3d/ui/ImageNoteView;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 626
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 627
    return-void
.end method

.method public setImageNoteViewListener(Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;)V
    .locals 0
    .param p1, "imageNoteViewListener"    # Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteViewListener:Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;

    .line 233
    return-void
.end method

.method public setNeedtoUpdateImageNoteIcon(Z)V
    .locals 0
    .param p1, "needToUpdate"    # Z

    .prologue
    .line 238
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->NeedtoUpdateImageNoteIcon:Z

    .line 239
    return-void
.end method

.method public setPhotoViewIcon(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)V
    .locals 0
    .param p1, "photoViewIcon"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .prologue
    .line 630
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .line 631
    return-void
.end method

.method public setVisibility(I)V
    .locals 7
    .param p1, "visibility"    # I

    .prologue
    const/16 v6, 0x8

    .line 204
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->isAnimating()Z

    move-result v0

    .line 205
    .local v0, "isAnimating":Z
    if-eqz v0, :cond_2

    .line 206
    if-ne p1, v6, :cond_1

    .line 207
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->imageNotehandler:Landroid/os/Handler;

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    if-nez p1, :cond_0

    .line 209
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->imageNotehandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 211
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 212
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_3

    const-wide/32 v2, 0x80000

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_3

    .line 213
    invoke-super {p0, v6}, Lcom/sec/android/gallery3d/ui/GLView;->setVisibility(I)V

    goto :goto_0

    .line 216
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->setVisibility(I)V

    goto :goto_0
.end method

.method public startImageNote()V
    .locals 1

    .prologue
    .line 223
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->startEditImageNote(I)V

    .line 224
    return-void

    .line 223
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startPolaroidFrame()V
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->createThreadAndDialog()V

    .line 228
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->startEditImageNote(I)V

    .line 229
    return-void

    .line 228
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startRotateOutAnim()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 298
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-nez v3, :cond_0

    .line 299
    sget-object v3, Lcom/sec/android/gallery3d/ui/ImageNoteView;->TAG:Ljava/lang/String;

    const-string v4, "mPhotoView is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v2

    .line 304
    .local v2, "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->isFullScreen()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->inSlidingAnimation()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->isScrolling()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 305
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v3

    if-nez v3, :cond_4

    .line 306
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->resetToFullViewNoAnimation()V

    .line 312
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 313
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 314
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_5

    .line 315
    :cond_3
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setIsPressed(Z)V

    goto :goto_0

    .line 308
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    sget-object v3, Lcom/sec/android/gallery3d/ui/ImageNoteView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scroll state : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PositionController;->isScrolling()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 319
    .restart local v0    # "file":Ljava/io/File;
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteViewListener:Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;

    if-eqz v3, :cond_6

    .line 320
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteViewListener:Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;->onStartRotateAnimation()V

    .line 322
    :cond_6
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideContactPopupView()V

    .line 323
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideFaceIndicatorView()V

    .line 324
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->setShowBarState(Z)V

    .line 326
    const-string/jumbo v3, "startRotateOutAnim > startRotateOutAnim "

    invoke-direct {p0, v3, v7}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 327
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->createImageNoteBG(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;

    .line 330
    const-string/jumbo v3, "startRotateOutAnim > createImageNoteBG "

    invoke-direct {p0, v3, v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 332
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;

    if-nez v3, :cond_7

    .line 333
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0035

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 337
    :cond_7
    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteBmp:Landroid/graphics/Bitmap;

    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteTexture:Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    .line 338
    const-string/jumbo v3, "startRotateOutAnim > BitmapTexture "

    invoke-direct {p0, v3, v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->printLog(Ljava/lang/String;Z)V

    .line 340
    new-instance v3, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;-><init>(Lcom/sec/android/gallery3d/ui/ImageNoteView;)V

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView$LoadBackgroundImageTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 341
    new-instance v3, Lcom/sec/android/gallery3d/anim/FloatAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x43340000    # 180.0f

    const/16 v6, 0x2bc

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/gallery3d/anim/FloatAnimation;-><init>(FFI)V

    iput-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 342
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setRotateAnim(Lcom/sec/android/gallery3d/anim/FloatAnimation;)V

    .line 343
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->registerLaunchedAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 344
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->start()V

    goto/16 :goto_0
.end method

.method public stopRotateOutAnimation()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 348
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    if-eqz v1, :cond_1

    .line 349
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setRotateAnim(Lcom/sec/android/gallery3d/anim/FloatAnimation;)V

    .line 350
    iput-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mRotateAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 351
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setIsPressed(Z)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteViewListener:Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mImageNoteViewListener:Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;->onStopRotateAnimation()V

    .line 355
    :cond_0
    const/4 v0, 0x1

    .line 357
    :cond_1
    return v0
.end method

.method public updateImageNoteIcon()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 265
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v2, :cond_2

    .line 266
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setIsPressed(Z)V

    .line 269
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getNeedtoUpdateImageNoteIcon()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 270
    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setNeedtoUpdateImageNoteIcon(Z)V

    .line 272
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 273
    .local v1, "r":Landroid/graphics/Rect;
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, v1, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    const/4 v5, -0x1

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getCorrectRightFromPhotoView(FFI)I

    move-result v3

    invoke-virtual {p0, v6, v2, v3, v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->layout(IIII)V

    .line 277
    .end local v1    # "r":Landroid/graphics/Rect;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 278
    .local v0, "currentPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_3

    const-wide/32 v2, 0x80000

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 279
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isShowSoundSceneIcon()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 280
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/ImageNoteView;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showSoundSceneIcon(Z)V

    .line 281
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setVisibility(I)V

    .line 286
    .end local v0    # "currentPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    :goto_0
    return-void

    .line 283
    .restart local v0    # "currentPhoto":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setVisibility(I)V

    goto :goto_0
.end method
