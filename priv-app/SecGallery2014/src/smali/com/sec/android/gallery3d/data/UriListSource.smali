.class public Lcom/sec/android/gallery3d/data/UriListSource;
.super Lcom/sec/android/gallery3d/data/MediaSource;
.source "UriListSource.java"


# static fields
.field public static final PREFIX:Ljava/lang/String; = "uriList://"

.field public static final SCHEME:Ljava/lang/String; = "uriList"

.field private static final URI_LIST:I


# instance fields
.field private mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 20
    const-string/jumbo v0, "uriList"

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/data/MediaSource;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/UriListSource;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 22
    new-instance v0, Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/data/PathMatcher;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriListSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    .line 23
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriListSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    const-string v1, "/uriList"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/PathMatcher;->add(Ljava/lang/String;I)V

    .line 24
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/data/UriListSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 25
    return-void
.end method


# virtual methods
.method public createMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriListSource;->mMatcher:Lcom/sec/android/gallery3d/data/PathMatcher;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/PathMatcher;->match(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/UriListSource;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    :goto_0
    return-object v0

    .line 31
    :pswitch_0
    new-instance v0, Lcom/sec/android/gallery3d/data/UriListSet;

    iget-object v1, p0, Lcom/sec/android/gallery3d/data/UriListSource;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/gallery3d/data/UriListSet;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/Path;)V

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
