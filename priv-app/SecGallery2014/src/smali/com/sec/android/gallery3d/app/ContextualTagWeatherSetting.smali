.class public Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;
.super Landroid/app/Activity;
.source "ContextualTagWeatherSetting.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;
    }
.end annotation


# static fields
.field public static final CONTEXTUAL_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;

.field public static USER_SELECTED_WEATHER_OFFSET:I = 0x0

.field public static final WEATHER_ID:Ljava/lang/String; = "weather_ID"

.field private static mModeItem:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;

.field private mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

.field private mContext:Landroid/content/Context;

.field private mDate:Ljava/lang/String;

.field private mDateTextView:Landroid/widget/TextView;

.field private mFilePath:Ljava/lang/String;

.field private mImageView:Landroid/widget/ImageView;

.field private mItemId:I

.field private mListPosition:I

.field private mListView:Landroid/widget/ListView;

.field private mLocation:Ljava/lang/String;

.field private mLocationTextView:Landroid/widget/TextView;

.field private mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

.field private mPerson:Ljava/lang/String;

.field private mPersonTextView:Landroid/widget/TextView;

.field private mWeather:Ljava/lang/String;

.field private mWeatherTagEditableStatus:I

.field private mWeatherTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->TAG:Ljava/lang/String;

    .line 80
    const/16 v0, 0x64

    sput v0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->USER_SELECTED_WEATHER_OFFSET:I

    .line 82
    const-string v0, "content://media/external/contextural_tags"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->CONTEXTUAL_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListPosition:I

    .line 85
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 87
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mContext:Landroid/content/Context;

    .line 89
    new-instance v0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$1;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 377
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->setVisiblePreviewText()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListPosition:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 230
    .local v1, "bitmapCroped":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v4

    .line 231
    .local v4, "width":I
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v2

    .line 232
    .local v2, "height":I
    const/high16 v3, 0x3f800000    # 1.0f

    .line 234
    .local v3, "ratio":F
    if-le v4, v2, :cond_0

    .line 235
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_WIDTH_LAND:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v6, v6, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_HEIGHT_LAND:I

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 240
    :goto_0
    invoke-static {p1, v7}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->decodeImageFile(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 241
    if-nez v0, :cond_1

    .line 242
    sget-object v5, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cannot get bitmap from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/Exception;

    invoke-direct {v7}, Ljava/lang/Exception;-><init>()V

    invoke-static {v5, v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 243
    const/4 v5, 0x0

    .line 248
    :goto_1
    return-object v5

    .line 237
    :cond_0
    iget-object v5, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_WIDTH_PORT:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v6, v6, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->THUMBNAIL_HEIGHT_PORT:I

    int-to-float v6, v6

    div-float v3, v5, v6

    goto :goto_0

    .line 246
    :cond_1
    const/4 v5, 0x0

    invoke-static {v0, v3, v5, v7}, Lcom/sec/android/gallery3d/common/BitmapUtils;->cropCenterByRatio(Landroid/graphics/Bitmap;FIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v5, v1

    .line 248
    goto :goto_1
.end method

.method private registerMediaEjectReceiver()V
    .locals 3

    .prologue
    .line 108
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 109
    .local v0, "intentMediaScannerFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 110
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 112
    return-void
.end method

.method private setVisiblePreviewText()V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/16 v7, 0x100

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 304
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v3, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_VERTICAL_TOP_MARGIN:I

    .line 305
    .local v2, "positionY":I
    const/16 v1, 0xa

    .line 308
    .local v1, "positionX":I
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v4

    if-le v3, v4, :cond_0

    .line 309
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v2, v3, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_HORIZONTAL_TOP_MARGIN:I

    .line 310
    const/4 v1, 0x0

    .line 312
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 313
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    mul-int/2addr v3, v2

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v4

    div-int v2, v3, v4

    .line 316
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 317
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPersonTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 318
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocationTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 319
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/ContextualTagSetting;->getContextualTagStatus(Landroid/content/Context;)I

    move-result v0

    .line 323
    .local v0, "checkedItem":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDate:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 327
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocation:Ljava/lang/String;

    if-eqz v3, :cond_2

    and-int/lit16 v3, v0, 0x100

    if-eq v3, v7, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPerson:Ljava/lang/String;

    if-eqz v3, :cond_8

    and-int/lit8 v3, v0, 0x10

    if-ne v3, v8, :cond_8

    .line 328
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_BIG:I

    sub-int/2addr v2, v3

    .line 332
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 334
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPerson:Ljava/lang/String;

    if-eqz v3, :cond_5

    and-int/lit8 v3, v0, 0x10

    if-ne v3, v8, :cond_5

    .line 338
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocation:Ljava/lang/String;

    if-eqz v3, :cond_9

    and-int/lit16 v3, v0, 0x100

    if-ne v3, v7, :cond_9

    .line 339
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_BIG:I

    sub-int/2addr v2, v3

    .line 343
    :goto_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPersonTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 345
    :cond_5
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocation:Ljava/lang/String;

    if-eqz v3, :cond_6

    and-int/lit16 v3, v0, 0x100

    if-ne v3, v7, :cond_6

    .line 349
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v2, v3

    .line 350
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocationTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 352
    :cond_6
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 356
    add-int/lit8 v2, v2, -0x26

    .line 357
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 367
    :cond_7
    return-void

    .line 330
    :cond_8
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v2, v3

    goto :goto_0

    .line 341
    :cond_9
    iget-object v3, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->TEXT_SIZE_SMALL:I

    sub-int/2addr v2, v3

    goto :goto_1
.end method

.method private unregisteMediaEjectReceiver()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 124
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 126
    iput-object p0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mContext:Landroid/content/Context;

    .line 128
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->registerMediaEjectReceiver()V

    .line 130
    const v7, 0x7f030042

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->setContentView(I)V

    .line 132
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mConfig:Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 135
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 136
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 137
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 138
    const/16 v7, 0x1000

    const/16 v8, 0x1000

    invoke-virtual {v0, v7, v8}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 140
    const v7, 0x7f0f00b8

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mImageView:Landroid/widget/ImageView;

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 142
    .local v4, "requestIntent":Landroid/content/Intent;
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 144
    .local v5, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 146
    .local v3, "extra":Landroid/os/Bundle;
    const-string/jumbo v7, "weather"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    .line 147
    const-string v7, "location"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocation:Ljava/lang/String;

    .line 148
    const-string v7, "face"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPerson:Ljava/lang/String;

    .line 149
    const-string v7, "date"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDate:Ljava/lang/String;

    .line 150
    const-string v7, "id"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mItemId:I

    .line 151
    const-string v7, "filepath"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mFilePath:Ljava/lang/String;

    .line 152
    const-string/jumbo v7, "weathertagstatus"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTagEditableStatus:I

    .line 154
    const-string v7, ""

    invoke-virtual {p0, v7}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 155
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "content://media/external/video"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 156
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mImageView:Landroid/widget/ImageView;

    const v8, 0x7f020433

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 161
    :goto_0
    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/String;

    sput-object v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    .line 162
    sget-object v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0229

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 163
    sget-object v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    const/4 v8, 0x1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e022a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 164
    sget-object v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    const/4 v8, 0x2

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e022b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 165
    sget-object v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    const/4 v8, 0x3

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e022c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 166
    sget-object v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    const/4 v8, 0x4

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e022d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 167
    sget-object v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    const/4 v8, 0x5

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e022e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 169
    new-instance v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mContext:Landroid/content/Context;

    sget-object v9, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    invoke-direct {v7, p0, v8, v9}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;Landroid/content/Context;[Ljava/lang/String;)V

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mAdapter:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;

    .line 170
    const v7, 0x7f0f00bd

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;

    .line 171
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mAdapter:Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$ItemAdapter;

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 172
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 173
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v7, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 175
    const v7, 0x7f0f00ba

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocationTextView:Landroid/widget/TextView;

    .line 176
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocationTextView:Landroid/widget/TextView;

    const v8, -0xf0f0f10

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 177
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocationTextView:Landroid/widget/TextView;

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, -0x1000000

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 178
    const v7, 0x7f0f00bb

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPersonTextView:Landroid/widget/TextView;

    .line 179
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPersonTextView:Landroid/widget/TextView;

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, -0x1000000

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 180
    const v7, 0x7f0f00bc

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDateTextView:Landroid/widget/TextView;

    .line 181
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTagBuddy:Z

    if-eqz v7, :cond_3

    .line 182
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDateTextView:Landroid/widget/TextView;

    const v8, -0xf0f10

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 186
    :goto_1
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDateTextView:Landroid/widget/TextView;

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, -0x1000000

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 187
    const v7, 0x7f0f00b9

    invoke-virtual {p0, v7}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    .line 188
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    const v8, -0xf0f0f10

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 189
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, -0x1000000

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 193
    .local v1, "data":Ljava/lang/String;
    if-nez v1, :cond_4

    .line 194
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    const-string/jumbo v8, "weather"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    :goto_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mLocationTextView:Landroid/widget/TextView;

    const-string v8, "location"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mPersonTextView:Landroid/widget/TextView;

    const-string v8, "face"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDateTextView:Landroid/widget/TextView;

    const-string v8, "date"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/16 v8, 0x400

    const/16 v9, 0x400

    invoke-virtual {v7, v8, v9}, Landroid/view/Window;->setFlags(II)V

    .line 208
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->setVisiblePreviewText()V

    .line 210
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v7

    if-eqz v7, :cond_1

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v7, :cond_1

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 212
    .local v2, "deco":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v6

    .line 213
    .local v6, "vto2":Landroid/view/ViewTreeObserver;
    new-instance v7, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$2;

    invoke-direct {v7, p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting$2;-><init>(Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;)V

    invoke-virtual {v6, v7}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 220
    .end local v2    # "deco":Landroid/view/View;
    .end local v6    # "vto2":Landroid/view/ViewTreeObserver;
    :cond_1
    return-void

    .line 158
    .end local v1    # "data":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mImageView:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 184
    :cond_3
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mDateTextView:Landroid/widget/TextView;

    const v8, -0xb43a1d

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 196
    .restart local v1    # "data":Ljava/lang/String;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iput-object v1, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 254
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 255
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0e0046

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 256
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0e020a

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 257
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 371
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 373
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->unregisteMediaEjectReceiver()V

    .line 374
    invoke-static {}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->releaseInstance()V

    .line 375
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 289
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v1

    .line 291
    .local v1, "sb":Landroid/util/SparseBooleanArray;
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-eqz v2, :cond_1

    .line 292
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 293
    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    iget-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeatherTextView:Landroid/widget/TextView;

    sget-object v3, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    sget-object v2, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mModeItem:[Ljava/lang/String;

    aget-object v2, v2, v0

    iput-object v2, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    .line 296
    iput v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListPosition:I

    .line 292
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 300
    .end local v0    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->setVisiblePreviewText()V

    .line 301
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    .line 262
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 284
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    :goto_0
    return v3

    .line 265
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->finish()V

    goto :goto_0

    .line 268
    :sswitch_1
    sget-object v4, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->CONTEXTUAL_URI:Landroid/net/Uri;

    iget v5, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mItemId:I

    int-to-long v6, v5

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 269
    .local v0, "ImageURI":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 270
    .local v2, "values":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 271
    const-string/jumbo v4, "weather_ID"

    iget v5, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListPosition:I

    sget v6, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->USER_SELECTED_WEATHER_OFFSET:I

    add-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v0, v2, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 274
    new-instance v1, Lcom/sec/android/exifparser/MakerNoteParser;

    invoke-direct {v1}, Lcom/sec/android/exifparser/MakerNoteParser;-><init>()V

    .line 275
    .local v1, "parser":Lcom/sec/android/exifparser/MakerNoteParser;
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mFilePath:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/sec/android/exifparser/MakerNoteParser;->open(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 276
    const/16 v4, 0xc

    iget v5, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mListPosition:I

    sget v6, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->USER_SELECTED_WEATHER_OFFSET:I

    add-int/2addr v5, v6

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/exifparser/MakerNoteParser;->setMakerNote(II)V

    .line 277
    invoke-virtual {v1}, Lcom/sec/android/exifparser/MakerNoteParser;->close()V

    .line 281
    .end local v1    # "parser":Lcom/sec/android/exifparser/MakerNoteParser;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->finish()V

    goto :goto_0

    .line 262
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0259 -> :sswitch_0
        0x7f0f025a -> :sswitch_1
    .end sparse-switch
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;->mWeather:Ljava/lang/String;

    return-object v0
.end method
