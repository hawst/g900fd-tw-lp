.class final Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;
.super Ljava/lang/Object;
.source "GalleryProvider.java"

# interfaces
.implements Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/provider/GalleryProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SNSImagePipeDataWriter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/provider/GalleryProvider$PipeDataWriter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final mHasCacheFile:Z

.field private final mImage:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

.field final synthetic this$0:Lcom/sec/android/gallery3d/provider/GalleryProvider;


# direct methods
.method private constructor <init>(Lcom/sec/android/gallery3d/provider/GalleryProvider;Lcom/sec/android/gallery3d/remote/sns/SNSImage;)V
    .locals 1
    .param p2, "image"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->this$0:Lcom/sec/android/gallery3d/provider/GalleryProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525
    iput-object p2, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->mImage:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    .line 526
    invoke-direct {p0}, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->hasCacheFile()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->mHasCacheFile:Z

    .line 527
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/gallery3d/provider/GalleryProvider;Lcom/sec/android/gallery3d/remote/sns/SNSImage;Lcom/sec/android/gallery3d/provider/GalleryProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/gallery3d/provider/GalleryProvider;
    .param p2, "x1"    # Lcom/sec/android/gallery3d/remote/sns/SNSImage;
    .param p3, "x2"    # Lcom/sec/android/gallery3d/provider/GalleryProvider$1;

    .prologue
    .line 520
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;-><init>(Lcom/sec/android/gallery3d/provider/GalleryProvider;Lcom/sec/android/gallery3d/remote/sns/SNSImage;)V

    return-void
.end method

.method private hasCacheFile()Z
    .locals 3

    .prologue
    .line 530
    iget-object v2, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->mImage:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getPhotoTable()Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    move-result-object v0

    .line 531
    .local v0, "entry":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 532
    new-instance v1, Ljava/io/File;

    iget-object v2, v0, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 533
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 534
    const/4 v2, 0x1

    .line 537
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public writeDataToPipe(Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V
    .locals 10
    .param p1, "output"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "args"    # Ljava/lang/Object;

    .prologue
    .line 542
    iget-object v8, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->mImage:Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->getPhotoTable()Lcom/sec/android/gallery3d/remote/sns/PhotoTable;

    move-result-object v3

    .line 543
    .local v3, "entry":Lcom/sec/android/gallery3d/remote/sns/PhotoTable;
    const/4 v4, 0x0

    .line 544
    .local v4, "is":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 547
    .local v6, "os":Ljava/io/OutputStream;
    :try_start_0
    iget-boolean v8, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->mHasCacheFile:Z

    if-eqz v8, :cond_0

    iget-object v8, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    iget-object v9, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->screennail_url:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/remote/sns/SNSImage;->isScreenNailisFull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 548
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v8, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->cache_pathname:Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .end local v4    # "is":Ljava/io/InputStream;
    .local v5, "is":Ljava/io/InputStream;
    move-object v4, v5

    .line 564
    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    :goto_0
    new-instance v7, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v7, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565
    .end local v6    # "os":Ljava/io/OutputStream;
    .local v7, "os":Ljava/io/OutputStream;
    :try_start_1
    # invokes: Lcom/sec/android/gallery3d/provider/GalleryProvider;->dump(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    invoke-static {v4, v7}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->access$400(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 569
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 570
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v6, v7

    .line 572
    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    :goto_1
    return-void

    .line 550
    :cond_0
    :try_start_2
    new-instance v0, Ljava/net/URL;

    iget-object v8, v3, Lcom/sec/android/gallery3d/remote/sns/PhotoTable;->content_url:Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 551
    .local v0, "contentUrl":Ljava/net/URL;
    iget-object v8, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->this$0:Lcom/sec/android/gallery3d/provider/GalleryProvider;

    # invokes: Lcom/sec/android/gallery3d/provider/GalleryProvider;->getDownloadCache()Lcom/sec/android/gallery3d/data/DownloadCache;
    invoke-static {v8}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->access$300(Lcom/sec/android/gallery3d/provider/GalleryProvider;)Lcom/sec/android/gallery3d/data/DownloadCache;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9}, Lcom/sec/android/gallery3d/data/DownloadCache;->lookup(Ljava/net/URL;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/DownloadCache$Entry;

    move-result-object v1

    .line 552
    .local v1, "downloadEntry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    if-eqz v1, :cond_1

    .line 553
    new-instance v5, Ljava/io/FileInputStream;

    iget-object v8, v1, Lcom/sec/android/gallery3d/data/DownloadCache$Entry;->cacheFile:Ljava/io/File;

    invoke-direct {v5, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_0

    .line 556
    :cond_1
    iget-object v8, p0, Lcom/sec/android/gallery3d/provider/GalleryProvider$SNSImagePipeDataWriter;->this$0:Lcom/sec/android/gallery3d/provider/GalleryProvider;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/provider/GalleryProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->allowDownloadBySettings(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 557
    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    goto :goto_0

    .line 559
    :cond_2
    const-string v8, "GalleryProvider"

    const-string v9, "fail to download due to wifi-only settings"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 569
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 570
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 566
    .end local v0    # "contentUrl":Ljava/net/URL;
    .end local v1    # "downloadEntry":Lcom/sec/android/gallery3d/data/DownloadCache$Entry;
    :catch_0
    move-exception v2

    .line 567
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v8, "GalleryProvider"

    const-string v9, "fail to download: "

    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 569
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 570
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .line 569
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_3
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 570
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v8

    .line 569
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v7    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    goto :goto_3

    .line 566
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v7    # "os":Ljava/io/OutputStream;
    :catch_1
    move-exception v2

    move-object v6, v7

    .end local v7    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    goto :goto_2
.end method
