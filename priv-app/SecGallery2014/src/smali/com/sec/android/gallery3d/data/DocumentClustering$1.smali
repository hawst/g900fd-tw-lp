.class Lcom/sec/android/gallery3d/data/DocumentClustering$1;
.super Ljava/lang/Object;
.source "DocumentClustering.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/data/DocumentClustering;->run(Lcom/sec/android/gallery3d/data/MediaSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/data/DocumentClustering;

.field final synthetic val$sItems:[Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;

.field final synthetic val$total:I


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/data/DocumentClustering;I[Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/gallery3d/data/DocumentClustering$1;->this$0:Lcom/sec/android/gallery3d/data/DocumentClustering;

    iput p2, p0, Lcom/sec/android/gallery3d/data/DocumentClustering$1;->val$total:I

    iput-object p3, p0, Lcom/sec/android/gallery3d/data/DocumentClustering$1;->val$sItems:[Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v3, 0x1

    .line 45
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/sec/android/gallery3d/data/DocumentClustering$1;->val$total:I

    if-lt p1, v0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v3

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DocumentClustering$1;->val$sItems:[Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;

    new-instance v1, Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;-><init>(Lcom/sec/android/gallery3d/data/DocumentClustering$1;)V

    aput-object v1, v0, p1

    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DocumentClustering$1;->val$sItems:[Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;

    aget-object v0, v0, p1

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;->path:Lcom/sec/android/gallery3d/data/Path;

    .line 48
    iget-object v0, p0, Lcom/sec/android/gallery3d/data/DocumentClustering$1;->val$sItems:[Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;

    aget-object v0, v0, p1

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/gallery3d/data/DocumentClustering$SmallItem;->filePath:Ljava/lang/String;

    goto :goto_0
.end method
