.class public Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
.super Ljava/lang/Object;
.source "ChangePlayerListDialog.java"

# interfaces
.implements Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;


# static fields
.field private static final SCAN_WIFI_DISPLAY_FINISH:I = 0x2

.field private static final SCAN_WIFI_DISPLAY_REFRESH_TIMEOUT:J = 0x2710L

.field private static final SCAN_WIFI_DISPLAY_START:I = 0x1

.field private static final SEARCH_DLNA_ALLSHARE_CAST:I = 0x0

.field private static final SEARCH_ONLY_DLNA:I = 0x1

.field protected static final TAG:Ljava/lang/String;

.field private static final UNALBE_SEARCH_ALLSHARE_CAST:I = 0x2


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mChangePlayerItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mContext:Landroid/content/Context;

.field private final mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDialog:Landroid/app/AlertDialog;

.field private mIsWifiDisplaySelected:Z

.field private mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mMyDevice:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

.field private mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

.field private mPath:Lcom/sec/android/gallery3d/data/Path;

.field private mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mScanWifiDisplayHandler:Landroid/os/Handler;

.field private mSearchMode:I

.field private mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    .line 83
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$1;-><init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mScanWifiDisplayHandler:Landroid/os/Handler;

    .line 269
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$5;-><init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mChangePlayerItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 123
    iput-object p1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 124
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mContext:Landroid/content/Context;

    .line 125
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 127
    iput-object p3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 128
    iput-object p2, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 129
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 132
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getMyDevice(Landroid/content/Context;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMyDevice:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    .line 135
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const-string v2, "nearby"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 137
    .local v0, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .line 139
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->setOnDeviceChangedListener(Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;)V

    .line 143
    :cond_0
    new-instance v1, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    .line 144
    iget-object v1, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->setOnDeviceChangedListener(Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;)V

    .line 146
    invoke-direct {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->initDialog()V

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->refreshListAdapter()V

    .line 148
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mScanWifiDisplayHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mIsWifiDisplaySelected:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->handleChangePlayer(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;)V

    return-void
.end method

.method private handleChangePlayer(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;)V
    .locals 7
    .param p1, "changePlayerItem"    # Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    .prologue
    .line 286
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDeviceType()I

    move-result v1

    .line 287
    .local v1, "playerType":I
    packed-switch v1, :pswitch_data_0

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 289
    :pswitch_0
    sget-object v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    const-string v4, "clicked : my device"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->disconnectWithPlayDevice()V

    .line 292
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->disconnectWifiDisplay()V

    .line 293
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    goto :goto_0

    .line 297
    :pswitch_1
    sget-object v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    const-string v4, "clicked : nearby device"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->disconnectWifiDisplay()V

    .line 301
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 302
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    iget-object v4, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDeviceID()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3, v4, v5, v0, v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->changePlayer(ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 306
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_0

    .line 309
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 314
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :pswitch_2
    sget-object v3, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    const-string v4, "clicked : wfd device"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    if-nez v3, :cond_0

    .line 318
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->isOnPlaying()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 319
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->disconnectWithPlayDevice()V

    .line 323
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDevice()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    .line 324
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->disconnectWifiDisplay()V

    goto :goto_0

    .line 326
    :cond_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mIsWifiDisplaySelected:Z

    .line 327
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDevice()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/display/WifiDisplay;

    .line 328
    .local v2, "wifiDisplay":Landroid/hardware/display/WifiDisplay;
    iget-object v3, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->connectWifiDisplay(Landroid/hardware/display/WifiDisplay;)V

    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initDialog()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 151
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    if-eqz v5, :cond_1

    .line 152
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getExceptionalCase()I

    move-result v2

    .line 153
    .local v2, "exceptionCase":I
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->isP2pConnected()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x4

    if-eq v5, v2, :cond_0

    const/4 v5, 0x5

    if-eq v5, v2, :cond_0

    if-ne v10, v2, :cond_3

    .line 159
    :cond_0
    iput v10, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    .line 165
    :goto_0
    sget-object v5, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "search mode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    .end local v2    # "exceptionCase":I
    :cond_1
    iget v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    if-nez v5, :cond_2

    .line 169
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mScanWifiDisplayHandler:Landroid/os/Handler;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 172
    :cond_2
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 174
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f03002b

    invoke-virtual {v4, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 175
    .local v3, "headerView":Landroid/view/View;
    const v5, 0x7f0f0070

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    iput-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 177
    const v5, 0x7f030029

    invoke-virtual {v4, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 178
    .local v0, "deviceList":Landroid/view/View;
    const v5, 0x7f0f006b

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 180
    .local v1, "deviceListLayout":Landroid/view/View;
    new-instance v5, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mContext:Landroid/content/Context;

    const v7, 0x7f03002c

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    .line 181
    const v5, 0x102000a

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListView:Landroid/widget/ListView;

    .line 182
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v3, v8, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 183
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mChangePlayerItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 184
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 186
    iget v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    if-ne v5, v11, :cond_5

    .line 187
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0e0120

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0e0121

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x104000a

    invoke-virtual {v5, v6, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    .line 195
    :goto_1
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5, v9}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 198
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$2;

    invoke-direct {v6, p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$2;-><init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 226
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$3;

    invoke-direct {v6, p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$3;-><init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 245
    iget-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$4;

    invoke-direct {v6, p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog$4;-><init>(Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 255
    return-void

    .line 160
    .end local v0    # "deviceList":Landroid/view/View;
    .end local v1    # "deviceListLayout":Landroid/view/View;
    .end local v3    # "headerView":Landroid/view/View;
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .restart local v2    # "exceptionCase":I
    :cond_3
    const/4 v5, 0x7

    if-ne v5, v2, :cond_4

    .line 161
    iput v11, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    goto/16 :goto_0

    .line 163
    :cond_4
    iput v9, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    goto/16 :goto_0

    .line 191
    .end local v2    # "exceptionCase":I
    .restart local v0    # "deviceList":Landroid/view/View;
    .restart local v1    # "deviceListLayout":Landroid/view/View;
    .restart local v3    # "headerView":Landroid/view/View;
    .restart local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_5
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0e0123

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0e012b

    invoke-virtual {v5, v6, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0e0046

    invoke-virtual {v5, v6, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    goto :goto_1
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 267
    :cond_0
    return-void
.end method

.method public onDeviceChanged()V
    .locals 2

    .prologue
    .line 401
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    const-string v1, "onDeviceChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->refreshListAdapter()V

    .line 403
    return-void
.end method

.method public refreshListAdapter()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 337
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    const-string v2, "refresh list adapter"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->clear()V

    .line 342
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getOnPlayingPlayer()Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    move-result-object v6

    .line 343
    .local v6, "currentPlayer":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    if-nez v6, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getCurrentWifiDisplay()Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    move-result-object v6

    .line 346
    :cond_0
    if-eqz v6, :cond_1

    .line 347
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->add(Ljava/lang/Object;)V

    .line 350
    :cond_1
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current player : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMyDevice:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->add(Ljava/lang/Object;)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->isNearbyPlayable(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 356
    const/4 v10, 0x0

    .line 357
    .local v10, "nic":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-eqz v0, :cond_2

    .line 358
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->getDeviceNic()Ljava/lang/String;

    move-result-object v10

    .line 361
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refresh()V

    .line 362
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getDisplayDeviceList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 363
    .local v9, "nearbyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->addAll(Ljava/util/Collection;)V

    .line 367
    .end local v9    # "nearbyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;>;"
    .end local v10    # "nic":Ljava/lang/String;
    :cond_3
    iget v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mSearchMode:I

    if-nez v0, :cond_5

    .line 368
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v11, "wfdDevicesSelected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;>;"
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->isWifiDisplayConnected()Z

    move-result v0

    if-nez v0, :cond_4

    .line 371
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getWifiDisplayAvailable()[Landroid/hardware/display/WifiDisplay;

    move-result-object v12

    .line 373
    .local v12, "wifiDisplays":[Landroid/hardware/display/WifiDisplay;
    const/4 v7, 0x0

    .local v7, "i":I
    array-length v8, v12

    .local v8, "n":I
    :goto_0
    if-ge v7, v8, :cond_4

    .line 374
    aget-object v5, v12, v7

    .line 375
    .local v5, "wifiDisplay":Landroid/hardware/display/WifiDisplay;
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    invoke-virtual {v5}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;-><init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 379
    .end local v5    # "wifiDisplay":Landroid/hardware/display/WifiDisplay;
    .end local v7    # "i":I
    .end local v8    # "n":I
    .end local v12    # "wifiDisplays":[Landroid/hardware/display/WifiDisplay;
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->addAll(Ljava/util/Collection;)V

    .line 383
    .end local v11    # "wfdDevicesSelected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;>;"
    :cond_5
    const/4 v7, 0x0

    .restart local v7    # "i":I
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getCount()I

    move-result v8

    .restart local v8    # "n":I
    :goto_1
    if-ge v7, v8, :cond_6

    .line 384
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "device : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 387
    :cond_6
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "device count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mListAdapter:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    return-void
.end method

.method public setItemToPrepare(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 391
    .local p1, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    :cond_0
    sget-object v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->TAG:Ljava/lang/String;

    const-string v1, "nothing to prepare"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :goto_0
    return-void

    .line 396
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mPrepareMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 261
    :cond_0
    return-void
.end method
