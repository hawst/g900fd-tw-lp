.class Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;
.super Landroid/content/BroadcastReceiver;
.source "HomeSyncController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/homesync/HomeSyncController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    const-string v0, "com.android.spc.image.PLAY_NEXT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$100(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onReceive ACTION_PLAY_NEXT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;->onPlayNext()Z

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    const-string v0, "com.android.spc.image.PLAY_PREVIOUS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 28
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$100(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "send ACTION_PLAY_PREVIOUS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;->onPlayPrevious()Z

    goto :goto_0

    .line 32
    :cond_2
    const-string v0, "com.android.spc.image.START_SLIDESHOW"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 33
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$100(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onReceive ACTION_START_SLIDESHOW"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;->onStartSlideshow()V

    .line 36
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->setStartSlideShow(Z)V

    goto :goto_0

    .line 38
    :cond_3
    const-string v0, "com.android.spc.image.STOP_SLIDESHOW"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 39
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$100(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onReceive ACTION_STOP_SLIDESHOW"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;->onStopSlideshow()V

    .line 42
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->setStartSlideShow(Z)V

    goto/16 :goto_0

    .line 44
    :cond_4
    const-string v0, "com.android.spc.image.STOP_PLAYER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$100(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onReceive ACTION_STOP_PLAYER"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v0, p0, Lcom/sec/android/gallery3d/homesync/HomeSyncController$1;->this$0:Lcom/sec/android/gallery3d/homesync/HomeSyncController;

    # getter for: Lcom/sec/android/gallery3d/homesync/HomeSyncController;->mControlListener:Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;
    invoke-static {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncController;->access$000(Lcom/sec/android/gallery3d/homesync/HomeSyncController;)Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/homesync/HomeSyncControlListener;->onStopPlayer()V

    goto/16 :goto_0
.end method
