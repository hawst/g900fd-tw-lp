.class Lcom/sec/android/gallery3d/ui/ContextualTagView$6;
.super Ljava/lang/Object;
.source "ContextualTagView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/ui/ContextualTagView;->showContextualTagHelpView(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/ui/ContextualTagView;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$6;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dlg"    # Landroid/content/DialogInterface;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$6;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/gallery3d/ui/ContextualTagView;->mContextualTagHelpDialog:Landroid/app/Dialog;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$302(Lcom/sec/android/gallery3d/ui/ContextualTagView;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 407
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$6;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/gallery3d/ui/ContextualTagView;->hide(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->access$000(Lcom/sec/android/gallery3d/ui/ContextualTagView;Z)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$6;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/ContextualTagView$6;->this$0:Lcom/sec/android/gallery3d/ui/ContextualTagView;

    iget-object v1, v1, Lcom/sec/android/gallery3d/ui/ContextualTagView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->setContextualTagView(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 409
    return-void
.end method
