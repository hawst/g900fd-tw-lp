.class public Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
.super Landroid/app/Activity;
.source "AbstractGalleryActivity.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/GalleryContext;
.implements Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

.field private mAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBatchService:Lcom/sec/android/gallery3d/app/BatchService;

.field private mBatchServiceConnection:Landroid/content/ServiceConnection;

.field private mBatchServiceIsBound:Z

.field private mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

.field private mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field private mDisableToggleStatusBar:Z

.field protected mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field protected mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mHapticUtils:Lcom/sec/samsung/gallery/haptic/HapticUtils;

.field private mMountFilter:Landroid/content/IntentFilter;

.field private mMountReceiver:Landroid/content/BroadcastReceiver;

.field private mNewAlbumSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mNewAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

.field private mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

.field private mPanoramaViewHelper:Lcom/sec/android/gallery3d/util/PanoramaViewHelper;

.field private mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

.field private mReceivedResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

.field private mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

.field private mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

.field private mTransitionStore:Lcom/sec/android/gallery3d/app/TransitionStore;

.field private mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    new-instance v0, Lcom/sec/android/gallery3d/app/TransitionStore;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/app/TransitionStore;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mTransitionStore:Lcom/sec/android/gallery3d/app/TransitionStore;

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDisableToggleStatusBar:Z

    .line 84
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 85
    new-instance v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$1;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mMountReceiver:Landroid/content/BroadcastReceiver;

    .line 91
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mMountFilter:Landroid/content/IntentFilter;

    .line 98
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 99
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mHapticUtils:Lcom/sec/samsung/gallery/haptic/HapticUtils;

    .line 100
    iput-object v2, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 399
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceIsBound:Z

    .line 400
    new-instance v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity$2;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/BatchService;)Lcom/sec/android/gallery3d/app/BatchService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/app/BatchService;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchService:Lcom/sec/android/gallery3d/app/BatchService;

    return-object p1
.end method

.method private doBindBatchService()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 415
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/gallery3d/app/BatchService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 416
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceIsBound:Z

    .line 417
    return-void
.end method

.method private doUnbindBatchService()V
    .locals 1

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceIsBound:Z

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 423
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceIsBound:Z

    .line 425
    :cond_0
    return-void
.end method

.method private static setAlertDialogIconAttribute(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p0, "builder"    # Landroid/app/AlertDialog$Builder;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 250
    const v0, 0x1010355

    invoke-virtual {p0, v0}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    .line 251
    return-void
.end method

.method private toggleStatusBarByOrientation()V
    .locals 4

    .prologue
    const/16 v3, 0x400

    .line 373
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDisableToggleStatusBar:Z

    if-eqz v1, :cond_0

    .line 381
    :goto_0
    return-void

    .line 375
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 376
    .local v0, "win":Landroid/view/Window;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 377
    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    .line 379
    :cond_1
    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method private trimThumbnalProxy()V
    .locals 1

    .prologue
    .line 635
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->stop()V

    .line 636
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPhotoThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->stop()V

    .line 637
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->stop()V

    .line 638
    return-void
.end method


# virtual methods
.method protected disableToggleStatusBar()V
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDisableToggleStatusBar:Z

    .line 369
    return-void
.end method

.method public getAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 2

    .prologue
    .line 602
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    if-nez v0, :cond_0

    .line 603
    new-instance v0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const-string v1, "ALBUM_THM_PROXY"

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method public getAndroidContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 153
    return-object p0
.end method

.method public getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-nez v0, :cond_0

    .line 554
    new-instance v0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    return-object v0
.end method

.method public getAudioManager()Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 486
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method public getBatchServiceThreadPoolIfAvailable()Lcom/sec/android/gallery3d/util/ThreadPool;
    .locals 2

    .prologue
    .line 428
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchServiceIsBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchService:Lcom/sec/android/gallery3d/app/BatchService;

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mBatchService:Lcom/sec/android/gallery3d/app/BatchService;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/BatchService;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    return-object v0

    .line 431
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Batch service unavailable"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCacheInterface()Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .locals 2

    .prologue
    .line 561
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    if-nez v0, :cond_0

    .line 562
    new-instance v0, Lcom/sec/samsung/gallery/decoder/CacheInterface;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/decoder/CacheInterface;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    .line 565
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCacheInterface(Lcom/sec/samsung/gallery/decoder/CacheInterface;)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mCacheInterface:Lcom/sec/samsung/gallery/decoder/CacheInterface;

    return-object v0
.end method

.method public getDataManager()Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    return-object v0
.end method

.method public getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    if-nez v0, :cond_0

    .line 573
    new-instance v0, Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 575
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setDecoderInterface(Lcom/sec/samsung/gallery/decoder/DecoderInterface;)V

    .line 576
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    return-object v0
.end method

.method public getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    return-object v0
.end method

.method public getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method public getGalleryActionBar()Lcom/sec/android/gallery3d/app/GalleryActionBar;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    if-nez v0, :cond_0

    .line 351
    new-instance v0, Lcom/sec/android/gallery3d/app/GalleryActionBar;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    return-object v0
.end method

.method public getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .locals 1

    .prologue
    .line 515
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGalleryId()I
    .locals 1

    .prologue
    .line 530
    const/4 v0, 0x0

    return v0
.end method

.method public getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method public getHapticUtils()Lcom/sec/samsung/gallery/haptic/HapticUtils;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mHapticUtils:Lcom/sec/samsung/gallery/haptic/HapticUtils;

    if-nez v0, :cond_0

    .line 493
    new-instance v0, Lcom/sec/samsung/gallery/haptic/HapticUtils;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/haptic/HapticUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mHapticUtils:Lcom/sec/samsung/gallery/haptic/HapticUtils;

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mHapticUtils:Lcom/sec/samsung/gallery/haptic/HapticUtils;

    return-object v0
.end method

.method public getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 3

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mNewAlbumSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v0, :cond_0

    .line 585
    new-instance v1, Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mNewAlbumSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 587
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mNewAlbumSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method public getNewAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mNewAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    if-nez v0, :cond_0

    .line 617
    new-instance v0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const-string v1, "NEW_ALBUM_THM_PROXY"

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mNewAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mNewAlbumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method public getOrientationManager()Lcom/sec/android/gallery3d/app/OrientationManager;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

    return-object v0
.end method

.method public getPanoramaViewHelper()Lcom/sec/android/gallery3d/util/PanoramaViewHelper;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPanoramaViewHelper:Lcom/sec/android/gallery3d/util/PanoramaViewHelper;

    return-object v0
.end method

.method public getPhotoThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    if-nez v0, :cond_0

    .line 610
    new-instance v0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const-string v1, "PHOTO_THM_PROXY"

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 612
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method public getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;
    .locals 1

    .prologue
    .line 540
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSavedResult()Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mReceivedResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    return-object v0
.end method

.method public getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 3

    .prologue
    .line 595
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v0, :cond_0

    .line 596
    new-instance v1, Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryAppImpl;Z)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method public getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    return-object v0
.end method

.method public getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-nez v0, :cond_0

    .line 477
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 478
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->soundSet(I)V

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    return-object v0
.end method

.method public declared-synchronized getStateManager()Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Lcom/sec/android/gallery3d/app/StateManager;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    return-object v0
.end method

.method public getTransitionStore()Lcom/sec/android/gallery3d/app/TransitionStore;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mTransitionStore:Lcom/sec/android/gallery3d/app/TransitionStore;

    return-object v0
.end method

.method public getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-nez v0, :cond_0

    .line 648
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->create()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 650
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    return-object v0
.end method

.method public isContactsChanged()Z
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    return v0
.end method

.method public isFullscreen()Z
    .locals 1

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInTouchMode()Z
    .locals 1

    .prologue
    .line 466
    const/4 v0, 0x0

    return v0
.end method

.method public isLocaleRTL()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 641
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 323
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/app/StateManager;->notifyActivityResult(IILandroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 327
    return-void

    .line 325
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    .line 341
    .local v0, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 343
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->onBackPressed()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    .line 347
    return-void

    .line 345
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->onConfigurationChange(Landroid/content/res/Configuration;)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryActionBar()Lcom/sec/android/gallery3d/app/GalleryActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->onConfigurationChanged()V

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 134
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->toggleStatusBarByOrientation()V

    .line 135
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    new-instance v0, Lcom/sec/android/gallery3d/app/OrientationManager;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/OrientationManager;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

    .line 106
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->toggleStatusBarByOrientation()V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    new-instance v0, Lcom/sec/android/gallery3d/util/PanoramaViewHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/util/PanoramaViewHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPanoramaViewHelper:Lcom/sec/android/gallery3d/util/PanoramaViewHelper;

    .line 109
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPanoramaViewHelper:Lcom/sec/android/gallery3d/util/PanoramaViewHelper;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/PanoramaViewHelper;->onCreate()V

    .line 111
    new-instance v0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setGalleryEventListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnGalleryEventListener;)V

    .line 114
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->createOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 304
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 305
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 307
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->destroy()V

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->destroy()V

    .line 310
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mSoundUtils:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->releaseSoundSet()V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mHapticUtils:Lcom/sec/samsung/gallery/haptic/HapticUtils;

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mHapticUtils:Lcom/sec/samsung/gallery/haptic/HapticUtils;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/haptic/HapticUtils;->closeHaptic()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 317
    return-void

    .line 315
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v0
.end method

.method public onGalleryEventReceived(Lcom/sec/android/gallery3d/app/GalleryEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/sec/android/gallery3d/app/GalleryEvent;

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->galleryEventReceived(Lcom/sec/android/gallery3d/app/GalleryEvent;)V

    .line 457
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    .line 359
    .local v0, "root":Lcom/sec/android/gallery3d/ui/GLRoot;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->lockRenderThread()V

    .line 361
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/StateManager;->itemSelected(Landroid/view/MenuItem;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 363
    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    return v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unlockRenderThread()V

    throw v1
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 284
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 285
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/OrientationManager;->pause()V

    .line 286
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onPause()V

    .line 287
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 289
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onViewPause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 293
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/photos/data/GalleryBitmapPool;->clear()V

    .line 294
    invoke-static {}, Lcom/sec/android/gallery3d/data/MediaItem;->getBytesBufferPool()Lcom/sec/android/gallery3d/data/BytesBufferPool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/BytesBufferPool;->clear()V

    .line 295
    return-void

    .line 291
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->onPostCreate(Landroid/os/Bundle;)V

    .line 142
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 444
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->prepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 266
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 267
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 269
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onViewResume()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 273
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onResume()V

    .line 274
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/OrientationManager;->resume()V

    .line 275
    return-void

    .line 271
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 120
    :try_start_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->saveState(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 126
    return-void

    .line 124
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 218
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mMountReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mMountFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPanoramaViewHelper:Lcom/sec/android/gallery3d/util/PanoramaViewHelper;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/PanoramaViewHelper;->onStart()V

    .line 245
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 255
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 256
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mMountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mPanoramaViewHelper:Lcom/sec/android/gallery3d/util/PanoramaViewHelper;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/util/PanoramaViewHelper;->onStop()V

    .line 262
    return-void
.end method

.method protected onStorageReady()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 210
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 211
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mMountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 213
    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 624
    sget-object v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrimMemory , Level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 626
    const/16 v0, 0xf

    if-lt p1, v0, :cond_0

    const/16 v0, 0x14

    if-gt p1, v0, :cond_0

    .line 628
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->releaseInstance()V

    .line 629
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/photos/data/GalleryBitmapPool;->clear()V

    .line 630
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->trimThumbnalProxy()V

    .line 632
    :cond_0
    return-void
.end method

.method protected onViewPause()V
    .locals 1

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->pause()V

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->pause()V

    .line 300
    return-void
.end method

.method protected onViewResume()V
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->resume()V

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->resume()V

    .line 280
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 450
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->windowFocusChanged(Z)V

    .line 452
    return-void
.end method

.method public printSelectedImage(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 439
    return-void
.end method

.method public purgeSavedResult()V
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mReceivedResult:Lcom/sec/android/gallery3d/app/ActivityState$ResultEntry;

    .line 335
    return-void
.end method

.method public registerFaceRecommendationObserver(Landroid/net/Uri;Z)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "showDialog"    # Z

    .prologue
    .line 521
    return-void
.end method

.method public setContentView(I)V
    .locals 6
    .param p1, "resId"    # I

    .prologue
    .line 189
    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(I)V

    .line 190
    const v1, 0x7f0f00bf

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 191
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_0

    .line 192
    sget-object v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->TAG:Ljava/lang/String;

    const-string v2, "setContentView mGLRootView is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :goto_0
    return-void

    .line 195
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_1

    .line 196
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    sget v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_RED:F

    sget v3, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_GREEN:F

    sget v4, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_BLUE:F

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setGlBackgroundColor(FFFF)V

    goto :goto_0

    .line 198
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->intColorToFloatARGBArray(I)[F

    move-result-object v0

    .line 199
    .local v0, "color":[F
    if-nez v0, :cond_2

    .line 200
    sget-object v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->TAG:Ljava/lang/String;

    const-string v2, "setContentView color is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 203
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->mGLRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v2, 0x1

    aget v2, v0, v2

    const/4 v3, 0x2

    aget v3, v0, v3

    const/4 v4, 0x3

    aget v4, v0, v4

    const/4 v5, 0x0

    aget v5, v0, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setGlBackgroundColor(FFFF)V

    goto :goto_0
.end method
