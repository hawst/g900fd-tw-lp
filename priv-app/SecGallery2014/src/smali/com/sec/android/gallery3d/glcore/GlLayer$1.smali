.class Lcom/sec/android/gallery3d/glcore/GlLayer$1;
.super Ljava/lang/Object;
.source "GlLayer.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/glcore/GlLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()Z
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onClicked()Z

    move-result v0

    return v0
.end method

.method public onGenericMotionTouch(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onGenericMotionCancel()V

    .line 557
    const/4 v0, 0x1

    return v0
.end method

.method public onLongClick(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onLongClicked(II)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 531
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onMoved(II)Z

    move-result v0

    return v0
.end method

.method public onPress(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onPressed(II)Z

    move-result v0

    return v0
.end method

.method public onRelease(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 541
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onReleased(IIII)Z

    move-result v0

    return v0
.end method

.method public onScroll(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/android/gallery3d/glcore/GlLayer$1;->this$0:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlLayer;->onScrolled(IIII)Z

    move-result v0

    return v0
.end method
