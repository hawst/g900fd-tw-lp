.class public Lcom/sec/android/gallery3d/remote/nearby/AsfIntent;
.super Ljava/lang/Object;
.source "AsfIntent.java"


# static fields
.field public static final ACTION_IMAGEVIEWER:Ljava/lang/String; = "com.samsung.android.allshare.intent.action.IMAGEVIEWER"

.field private static final EXTRA_ITEM:Ljava/lang/String; = "com.samsung.android.allshare.intent.extra.ITEM"

.field public static final EXTRA_NEARBY_ENABLED:Ljava/lang/String; = "com.samsung.android.allshare.intent.extra.ALLSHARE_ENABLED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getContentType(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 34
    const-string v0, "com.samsung.android.allshare.intent.extra.ITEM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getItem(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    const-string v0, "com.samsung.android.allshare.intent.extra.ITEM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public static final getUri(Landroid/content/Intent;)Landroid/net/Uri;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 30
    const-string v0, "com.samsung.android.allshare.intent.extra.ITEM"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final isAllShareIntentApi(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    const-string v0, "com.samsung.android.allshare.intent.action.IMAGEVIEWER"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
