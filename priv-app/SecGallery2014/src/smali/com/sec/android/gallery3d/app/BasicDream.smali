.class public Lcom/sec/android/gallery3d/app/BasicDream;
.super Landroid/app/Activity;
.source "BasicDream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/app/BasicDream$BasicDreamView;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "BasicDream"


# instance fields
.field private mPlugged:Z

.field private final mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mPlugged:Z

    .line 75
    new-instance v0, Lcom/sec/android/gallery3d/app/BasicDream$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/app/BasicDream$1;-><init>(Lcom/sec/android/gallery3d/app/BasicDream;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/gallery3d/app/BasicDream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/BasicDream;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mPlugged:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/gallery3d/app/BasicDream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/gallery3d/app/BasicDream;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mPlugged:Z

    return p1
.end method


# virtual methods
.method protected getContentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mView:Landroid/view/View;

    return-object v0
.end method

.method protected invalidate()V
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BasicDream;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 136
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 139
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 114
    const-string v0, "BasicDream"

    const-string v1, "exiting onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BasicDream;->finish()V

    .line 116
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 100
    new-instance v1, Lcom/sec/android/gallery3d/app/BasicDream$BasicDreamView;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/gallery3d/app/BasicDream$BasicDreamView;-><init>(Lcom/sec/android/gallery3d/app/BasicDream;Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/app/BasicDream;->setContentView(Landroid/view/View;)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BasicDream;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x80001

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 105
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 106
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/gallery3d/app/BasicDream;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 108
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 121
    iget-object v0, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mPowerIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/app/BasicDream;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 122
    return-void
.end method

.method public onUserInteraction()V
    .locals 2

    .prologue
    .line 142
    const-string v0, "BasicDream"

    const-string v1, "exiting onUserInteraction"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/BasicDream;->finish()V

    .line 144
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    .line 131
    iput-object p1, p0, Lcom/sec/android/gallery3d/app/BasicDream;->mView:Landroid/view/View;

    .line 132
    return-void
.end method
