.class public Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
.super Ljava/lang/Object;
.source "PhotoViewIcon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;,
        Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;
    }
.end annotation


# static fields
.field private static final BURST_PLAY_DURATOIN_DEFAULT:I = 0x1

.field private static final BURST_PLAY_DURATOIN_X02:I = 0x0

.field private static final BURST_PLAY_DURATOIN_X05:I = 0x1

.field private static final BURST_PLAY_DURATOIN_X10:I = 0x2

.field private static final PREFIX_HAS_SEC_SEQUENCESHOT_APP:Ljava/lang/String; = "has-sec-sequenceshot-app"

.field private static final PREFIX_SEC_SEQUENCESHOT_APP_UPDATE:Ljava/lang/String; = "sec-sequenceshot-app-update"

.field private static final SEQUENCESHOT_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.camera.shootingmode.sequence"

.field private static final SOUNDSCENE_IC_DURATION:J = 0x1f4L

.field private static final TAG:Ljava/lang/String;

.field public static final TRANS_NONE:I


# instance fields
.field private bBurstShotPlayIconPressed:Z

.field private bBurstShotSettingIconPressed:Z

.field private bSoundSceneIconPressed:Z

.field private m2dDisplayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private m2dDisplayPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private m3DPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private m3DPanoramaPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private m3DTourIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private m3dDisplayAvailable:Z

.field private m3dDisplayEnabled:Z

.field private m3dDisplayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private m3dDisplayIconPressed:Z

.field private m3dDisplayPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mBestFaceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mBestPhotoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private final mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

.field private mContext:Landroid/content/Context;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mDramaShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mEraserIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mFocusedIndex:I

.field private mIconFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

.field private mIconFocusRect:Landroid/graphics/Rect;

.field private mIconRect:Landroid/graphics/Rect;

.field private mIs3DPanoramaIconPressed:Z

.field private mIsIconFocused:Z

.field private mIsMagicShotIconPressed:Z

.field private mIsOutOfFocusIconPressed:Z

.field private mIsPanoramaIconPressed:Z

.field private mIsSequenceIconPressed:Z

.field private mListener:Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

.field private mMagicShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mMagicShotPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mNextTimeMillis:J

.field private mOutOfFocusIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mOutOfFocusPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mPicMotionIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

.field private mPreviousTimeMillis:J

.field private mPrivateIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkBlurayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkCameraIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkPcIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkPhoneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkPhoneWindowIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkSpcIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkTabIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkTvIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSLinkUnknownIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSequenceShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSequenceShotPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mShow3DPanoramaIcon:Z

.field private mShow3DTourIcon:Z

.field private mShowBestFaceIcon:Z

.field private mShowBestPhotoIcon:Z

.field private mShowBurstShotPlayIcon:Z

.field private mShowDramaShotIcon:Z

.field private mShowEraserIcon:Z

.field private mShowMagicShotIcon:Z

.field private mShowOutOfFocusIcon:Z

.field private mShowPanoramaIcon:Z

.field private mShowPicMotionIcon:Z

.field private mShowPrivateIcon:Z

.field private mShowSLinkBlurayIcon:Z

.field private mShowSLinkCameraIcon:Z

.field private mShowSLinkPcIcon:Z

.field private mShowSLinkPhoneIcon:Z

.field private mShowSLinkPhoneWindowIcon:Z

.field private mShowSLinkSpcIcon:Z

.field private mShowSLinkTabIcon:Z

.field private mShowSLinkTvIcon:Z

.field private mShowSLinkUnknownIcon:Z

.field private mShowSequenceShotIcon:Z

.field private mShowSoundSceneIcon:Z

.field private mShowVideoPlayIcon:Z

.field private mSoundSceneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSoundScenePressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mSwitch:I

.field private mVideoPlayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

.field private mViewMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PositionController;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "positionController"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mViewMode:I

    .line 69
    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSwitch:I

    .line 115
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    .line 118
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    .line 126
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayEnabled:Z

    .line 163
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    .line 173
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    .line 174
    iput-object p2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 175
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->initPhotoViewIcon()V

    .line 178
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Lcom/sec/android/gallery3d/ui/PositionController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)Lcom/sec/samsung/gallery/util/DimensionUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mViewMode:I

    return v0
.end method

.method private checkActionDownRange(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1102
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1106
    const/4 v0, 0x1

    .line 1109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkActionMoveRange(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1113
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1117
    :cond_0
    const/4 v0, 0x1

    .line 1120
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleBurstShotSpeed(I)V
    .locals 5
    .param p1, "speed"    # I

    .prologue
    const/4 v4, 0x1

    .line 935
    if-nez p1, :cond_0

    .line 936
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f02015e

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$102(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 937
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f02015f

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$702(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 938
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$002(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 952
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mListener:Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;->notifyInvalidate()V

    .line 953
    return-void

    .line 939
    :cond_0
    if-ne p1, v4, :cond_1

    .line 940
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f020160

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$102(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 941
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f020161

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$702(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 942
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$002(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    goto :goto_0

    .line 943
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 944
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f020162

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$102(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 945
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f020163

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$702(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 946
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$002(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    goto :goto_0

    .line 948
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f020164

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$102(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 949
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v3, 0x7f020165

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$702(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 950
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, v4

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$002(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    goto/16 :goto_0
.end method

.method private initBurstShotIcon()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 921
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    array-length v1, v2

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 922
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    new-instance v3, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    invoke-direct {v3, p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;-><init>(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)V

    aput-object v3, v2, v0

    .line 921
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 925
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v2, v2, v6

    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v5, 0x7f02015c

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$102(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 926
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v2, v2, v6

    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v5, 0x7f02015d

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$702(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 927
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v2, v2, v6

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$002(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 929
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v2, v2, v7

    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v5, 0x7f020162

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$102(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 930
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v2, v2, v7

    new-instance v3, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v5, 0x7f020163

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mPressedTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$702(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Lcom/sec/android/gallery3d/glrenderer/Texture;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    .line 931
    iget-object v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v2, v2, v7

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    # setter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$002(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 932
    return-void
.end method

.method private static isSequenceShotDownloaded(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 793
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v4

    .line 794
    .local v4, "version":I
    const-string v6, "sec-sequenceshot-app-update"

    invoke-static {p0, v6, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    .line 796
    .local v3, "updateKey":I
    if-eq v3, v4, :cond_0

    .line 797
    const/4 v2, 0x0

    .line 798
    .local v2, "info":Landroid/content/pm/ApplicationInfo;
    const/4 v1, 0x0

    .line 800
    .local v1, "enabled":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.camera.shootingmode.sequence"

    const/16 v8, 0x80

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 801
    iget-boolean v1, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    const-string v5, "sec-sequenceshot-app-update"

    invoke-static {p0, v5, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 811
    const-string v5, "has-sec-sequenceshot-app"

    invoke-static {p0, v5, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 814
    .end local v1    # "enabled":Z
    .end local v2    # "info":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return v1

    .line 803
    .restart local v1    # "enabled":Z
    .restart local v2    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v0

    .line 804
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_1
    sget-object v6, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->TAG:Ljava/lang/String;

    const-string v7, "isSequenceShotDownloaded() : NullPointerException"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 810
    const-string v6, "sec-sequenceshot-app-update"

    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 811
    const-string v6, "has-sec-sequenceshot-app"

    invoke-static {p0, v6, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    move v1, v5

    goto :goto_0

    .line 806
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 807
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    sget-object v6, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->TAG:Ljava/lang/String;

    const-string v7, "isSequenceShotDownloaded() : NameNotFoundException"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 810
    const-string v6, "sec-sequenceshot-app-update"

    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 811
    const-string v6, "has-sec-sequenceshot-app"

    invoke-static {p0, v6, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    move v1, v5

    goto :goto_0

    .line 810
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v5

    const-string v6, "sec-sequenceshot-app-update"

    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 811
    const-string v6, "has-sec-sequenceshot-app"

    invoke-static {p0, v6, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    throw v5

    .line 814
    .end local v1    # "enabled":Z
    .end local v2    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_0
    const-string v5, "has-sec-sequenceshot-app"

    const/4 v6, 0x1

    invoke-static {p0, v5, v6}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method private setBlinkAniTexture()Lcom/sec/android/gallery3d/glrenderer/Texture;
    .locals 6

    .prologue
    .line 424
    sget-boolean v1, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isPlayingState:Z

    if-eqz v1, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->mEndTime:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_4

    .line 426
    iget-wide v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mNextTimeMillis:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 427
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPreviousTimeMillis:J

    .line 428
    iget-wide v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPreviousTimeMillis:J

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mNextTimeMillis:J

    .line 429
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSwitch:I

    mul-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSwitch:I

    .line 431
    :cond_0
    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSwitch:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 432
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSoundScenePressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 436
    .local v0, "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mListener:Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;->notifyInvalidate()V

    .line 440
    :goto_1
    return-object v0

    .line 432
    .end local v0    # "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSoundSceneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_0

    .line 434
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSoundScenePressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .restart local v0    # "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :goto_2
    goto :goto_0

    .end local v0    # "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 438
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSoundScenePressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .restart local v0    # "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :goto_3
    goto :goto_1

    .end local v0    # "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSoundSceneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto :goto_3
.end method

.method private showBurstShotIcon(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 667
    if-eqz p1, :cond_0

    .line 668
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getPlaySpeed(Landroid/content/Context;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->handleBurstShotSpeed(I)V

    .line 670
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    .line 671
    return-void
.end method

.method private speakIconGenericEvent(II)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1083
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isShowSoundSceneIcon()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isShowMagicShotIcon()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowOutOfFocusIcon:Z

    if-eqz v3, :cond_2

    .line 1084
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    .line 1085
    .local v1, "isInside":Z
    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showFocus(Z)V

    .line 1086
    if-eqz v1, :cond_2

    .line 1087
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1088
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isShowSoundSceneIcon()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1089
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02a7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1095
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1096
    .local v0, "button":Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v3

    const-string v4, ". "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    .line 1099
    .end local v0    # "button":Ljava/lang/String;
    .end local v1    # "isInside":Z
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    return-void

    .line 1090
    .restart local v1    # "isInside":Z
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isShowMagicShotIcon()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1091
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02a8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1092
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowOutOfFocusIcon:Z

    if-eqz v3, :cond_1

    .line 1093
    iget-object v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02a9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public IsPressedIcon()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1125
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    if-eqz v1, :cond_1

    .line 1144
    :cond_0
    :goto_0
    return v0

    .line 1127
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsPanoramaIconPressed:Z

    if-nez v1, :cond_0

    .line 1129
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIs3DPanoramaIconPressed:Z

    if-nez v1, :cond_0

    .line 1131
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsOutOfFocusIconPressed:Z

    if-nez v1, :cond_0

    .line 1133
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsSequenceIconPressed:Z

    if-nez v1, :cond_0

    .line 1135
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIconPressed:Z

    if-nez v1, :cond_0

    .line 1137
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsMagicShotIconPressed:Z

    if-nez v1, :cond_0

    .line 1139
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z

    if-nez v1, :cond_0

    .line 1141
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z

    if-nez v1, :cond_0

    .line 1144
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkPlayButton(IIIILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1148
    if-nez p5, :cond_0

    .line 1161
    :goto_0
    return v3

    .line 1150
    :cond_0
    const/4 v0, 0x0

    .line 1151
    .local v0, "isPicasaVideo":Z
    instance-of v4, p5, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v4, :cond_1

    .line 1152
    invoke-virtual {p5}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    move v0, v2

    .line 1154
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isVideoPlayIcon()Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v0, :cond_5

    :cond_2
    move v1, v2

    .line 1155
    .local v1, "playVideo":Z
    :goto_2
    if-eqz v1, :cond_3

    .line 1159
    div-int/lit8 v4, p3, 0x2

    sub-int v4, p1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0xc

    if-gt v4, p3, :cond_6

    div-int/lit8 v4, p4, 0x2

    sub-int v4, p2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0xc

    if-gt v4, p4, :cond_6

    move v1, v2

    :cond_3
    :goto_3
    move v3, v1

    .line 1161
    goto :goto_0

    .end local v1    # "playVideo":Z
    :cond_4
    move v0, v3

    .line 1152
    goto :goto_1

    :cond_5
    move v1, v3

    .line 1154
    goto :goto_2

    .restart local v1    # "playVideo":Z
    :cond_6
    move v1, v3

    .line 1159
    goto :goto_3
.end method

.method public checkPlaySoundScene(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1165
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isShowSoundSceneIcon()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isSoundSceneIconPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1166
    const/4 v0, 0x1

    .line 1168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public drawVideoPlayIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;III)V
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "side"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 448
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    invoke-static {p3, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v4, v0, 0x7

    .line 455
    .local v4, "s":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mVideoPlayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    neg-int v1, v4

    div-int/lit8 v2, v1, 0x2

    neg-int v1, v4

    div-int/lit8 v3, v1, 0x2

    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 456
    return-void

    .line 451
    .end local v4    # "s":I
    :cond_0
    invoke-static {p3, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4004000000000000L    # 2.5

    div-double/2addr v0, v2

    double-to-int v4, v0

    .restart local v4    # "s":I
    goto :goto_0
.end method

.method public getIconRect(I)Landroid/graphics/Rect;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v0, v0, p1

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public hideIcon(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p1, "currentPhoto"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 515
    if-nez p1, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 517
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v0, :cond_3

    :cond_2
    const-wide/16 v2, 0x400

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_3
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowVideoPlayIcon:Z

    .line 520
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    if-eqz v0, :cond_4

    .line 521
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSequenceShotIcon:Z

    .line 524
    :cond_4
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSoundSceneIcon:Z

    .line 525
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowMagicShotIcon:Z

    .line 526
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestPhotoIcon:Z

    .line 527
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestFaceIcon:Z

    .line 528
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowEraserIcon:Z

    .line 529
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowDramaShotIcon:Z

    .line 530
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPicMotionIcon:Z

    .line 531
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DTourIcon:Z

    .line 532
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowOutOfFocusIcon:Z

    .line 534
    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showBurstShotIcon(Z)V

    .line 536
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePanoramaSlideshow:Z

    if-eqz v0, :cond_5

    .line 537
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    .line 540
    :cond_5
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSLinkIcon:Z

    if-eqz v0, :cond_6

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_6

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->hideSLinkIcon()V

    .line 544
    :cond_6
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v0, :cond_7

    .line 545
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPrivateIcon:Z

    .line 548
    :cond_7
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v0, :cond_0

    .line 549
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DPanoramaIcon:Z

    goto :goto_0

    :cond_8
    move v0, v1

    .line 517
    goto :goto_1
.end method

.method public hideSLinkIcon()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 623
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkBlurayIcon:Z

    .line 624
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkCameraIcon:Z

    .line 625
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneIcon:Z

    .line 626
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneWindowIcon:Z

    .line 627
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTabIcon:Z

    .line 628
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPcIcon:Z

    .line 629
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPcIcon:Z

    .line 630
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTvIcon:Z

    .line 631
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkUnknownIcon:Z

    .line 632
    return-void
.end method

.method protected initPhotoViewIcon()V
    .locals 4

    .prologue
    const v3, 0x7f020170

    .line 193
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020180

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mVideoPlayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 195
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020178

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSoundSceneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 196
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020179

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSoundScenePressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 198
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->initBurstShotIcon()V

    .line 200
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f0200e1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 201
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f0200e2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 202
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f0200df

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m2dDisplayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 203
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f0200e0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m2dDisplayPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 205
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f0201e1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 206
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020143

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3DPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 207
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020144

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3DPanoramaPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 209
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f02015a

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mMagicShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 210
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f02015b

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mMagicShotPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 212
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraShotAndMore:Z

    if-eqz v0, :cond_0

    .line 213
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f02014b

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBestPhotoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 214
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020149

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBestFaceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 215
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020153

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mEraserIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 216
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020151

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mDramaShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 217
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f02014f

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPicMotionIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 220
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020145

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3DTourIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 222
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020156

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mOutOfFocusIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 223
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020157

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mOutOfFocusPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 225
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020155

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPrivateIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 227
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020158

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSequenceShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 228
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020159

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSequenceShotPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 230
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSLinkIcon:Z

    if-eqz v0, :cond_1

    .line 231
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020166

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkBlurayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 232
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020168

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkCameraIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 233
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkPhoneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 234
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkPhoneWindowIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 235
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020172

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkTabIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 236
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f02016e

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkPcIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 237
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f02016a

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkSpcIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 238
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020174

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkTvIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 239
    new-instance v0, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v2, 0x7f020176

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkUnknownIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 241
    :cond_1
    return-void
.end method

.method public is3DPanoramaIconPressed()Z
    .locals 1

    .prologue
    .line 706
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIs3DPanoramaIconPressed:Z

    return v0
.end method

.method public is3dDisplayEnabled()Z
    .locals 1

    .prologue
    .line 714
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayEnabled:Z

    return v0
.end method

.method public isBurstShotPlayIconPressed()Z
    .locals 1

    .prologue
    .line 682
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z

    return v0
.end method

.method public isBurstShotSettingIconPressed()Z
    .locals 1

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z

    return v0
.end method

.method public isMagicShotIconPressed()Z
    .locals 1

    .prologue
    .line 726
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsMagicShotIconPressed:Z

    return v0
.end method

.method public isOutOfFocusIconPressed()Z
    .locals 1

    .prologue
    .line 782
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsOutOfFocusIconPressed:Z

    return v0
.end method

.method public isPanoramaIconPressed()Z
    .locals 1

    .prologue
    .line 698
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsPanoramaIconPressed:Z

    return v0
.end method

.method public isSequenceIconPressed()Z
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isSequenceShotDownloaded(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 819
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsSequenceIconPressed:Z

    .line 821
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsSequenceIconPressed:Z

    return v0
.end method

.method public isShow3DTourIcon()Z
    .locals 1

    .prologue
    .line 774
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DTourIcon:Z

    return v0
.end method

.method public isShowBestFaceIcon()Z
    .locals 1

    .prologue
    .line 742
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestFaceIcon:Z

    return v0
.end method

.method public isShowBestPhotoIcon()Z
    .locals 1

    .prologue
    .line 734
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestPhotoIcon:Z

    return v0
.end method

.method public isShowBurstShotIcon()Z
    .locals 1

    .prologue
    .line 674
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    return v0
.end method

.method public isShowEraserIcon()Z
    .locals 1

    .prologue
    .line 750
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowEraserIcon:Z

    return v0
.end method

.method public isShowMagicShotIcon()Z
    .locals 1

    .prologue
    .line 766
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowMagicShotIcon:Z

    return v0
.end method

.method public isShowPanoramaIcon()Z
    .locals 1

    .prologue
    .line 647
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DPanoramaIcon:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowSoundSceneIcon()Z
    .locals 1

    .prologue
    .line 655
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSoundSceneIcon:Z

    return v0
.end method

.method public isSoundSceneIconPressed()Z
    .locals 1

    .prologue
    .line 663
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    return v0
.end method

.method public isVideoPlayIcon()Z
    .locals 1

    .prologue
    .line 459
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowVideoPlayIcon:Z

    return v0
.end method

.method public onGenericMotion(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v6, 0x7f0e02f3

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1173
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v0, v4

    .line 1174
    .local v0, "ex":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v1, v4

    .line 1176
    .local v1, "ey":I
    if-ne v0, v5, :cond_0

    if-ne v1, v5, :cond_0

    .line 1177
    invoke-virtual {p0, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showFocus(Z)V

    .line 1229
    :goto_0
    return v2

    .line 1181
    :cond_0
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1182
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    if-eqz v4, :cond_2

    .line 1183
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1215
    :cond_1
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showFocus(Z)V

    goto :goto_0

    .line 1184
    :cond_2
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DPanoramaIcon:Z

    if-eqz v4, :cond_3

    .line 1185
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    .line 1186
    :cond_3
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSoundSceneIcon:Z

    if-eqz v4, :cond_4

    .line 1187
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e027e

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    .line 1188
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DTourIcon:Z

    if-eqz v4, :cond_5

    .line 1189
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e04ab

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    .line 1190
    :cond_5
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestPhotoIcon:Z

    if-eqz v4, :cond_6

    .line 1191
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e016f

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    .line 1192
    :cond_6
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestFaceIcon:Z

    if-eqz v4, :cond_7

    .line 1193
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e02f2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    .line 1194
    :cond_7
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowEraserIcon:Z

    if-eqz v4, :cond_1

    .line 1195
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e0206

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    .line 1217
    :cond_8
    iget-boolean v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    if-eqz v4, :cond_a

    .line 1218
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v4, v4, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1219
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showFocus(ZI)V

    .line 1220
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e01b2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1223
    :cond_9
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v4, v4, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1224
    invoke-virtual {p0, v2, v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showFocus(ZI)V

    .line 1225
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e0173

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_a
    move v2, v3

    .line 1229
    goto/16 :goto_0
.end method

.method public rendererPhotoViewIcon(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V
    .locals 23
    .param p1, "c"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "transitionMode"    # I
    .param p3, "selectonModeBarHeight"    # I

    .prologue
    .line 244
    move-object/from16 v2, p1

    .line 245
    .local v2, "canvas":Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v14

    .line 246
    .local v14, "imageBounds":Landroid/graphics/Rect;
    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v16, v0

    .line 247
    .local v16, "left":I
    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    .line 249
    .local v21, "top":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 250
    .local v10, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    if-nez v10, :cond_1

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    invoke-virtual {v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v1

    add-int v7, v1, p3

    .line 253
    .local v7, "actionBarHeight":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mViewMode:I

    invoke-virtual {v10, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewIconTopPaddingPixel(I)I

    move-result v9

    .line 254
    .local v9, "detailViewIconTopPadding":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mViewMode:I

    invoke-virtual {v10, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewIconLeftPaddingPixel(I)I

    move-result v8

    .line 256
    .local v8, "detailViewIconLeftPadding":I
    const/4 v13, 0x0

    .line 257
    .local v13, "iconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconRect:Landroid/graphics/Rect;

    .line 258
    .local v12, "iconRect":Landroid/graphics/Rect;
    const/16 v17, 0x0

    .line 259
    .local v17, "previousIconMargin":I
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    if-eqz v1, :cond_2

    .line 260
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 263
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DPanoramaIcon:Z

    if-eqz v1, :cond_3

    .line 264
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIs3DPanoramaIconPressed:Z

    if-eqz v1, :cond_21

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3DPanoramaPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 267
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSoundSceneIcon:Z

    if-eqz v1, :cond_4

    .line 268
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setBlinkAniTexture()Lcom/sec/android/gallery3d/glrenderer/Texture;

    move-result-object v13

    .line 271
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayAvailable:Z

    if-eqz v1, :cond_5

    .line 272
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayEnabled:Z

    if-eqz v1, :cond_23

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIconPressed:Z

    if-eqz v1, :cond_22

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 277
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowMagicShotIcon:Z

    if-eqz v1, :cond_6

    .line 278
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsMagicShotIconPressed:Z

    if-eqz v1, :cond_25

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mMagicShotPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 281
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestPhotoIcon:Z

    if-eqz v1, :cond_7

    .line 282
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBestPhotoIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 285
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestFaceIcon:Z

    if-eqz v1, :cond_8

    .line 286
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBestFaceIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 289
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowEraserIcon:Z

    if-eqz v1, :cond_9

    .line 290
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mEraserIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 292
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowDramaShotIcon:Z

    if-eqz v1, :cond_a

    .line 293
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mDramaShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 295
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPicMotionIcon:Z

    if-eqz v1, :cond_b

    .line 296
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPicMotionIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 299
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DTourIcon:Z

    if-eqz v1, :cond_c

    .line 300
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3DTourIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 303
    :cond_c
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSLinkIcon:Z

    if-eqz v1, :cond_15

    .line 304
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkBlurayIcon:Z

    if-eqz v1, :cond_d

    .line 305
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkBlurayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 307
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkCameraIcon:Z

    if-eqz v1, :cond_e

    .line 308
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkCameraIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 310
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneIcon:Z

    if-eqz v1, :cond_f

    .line 311
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkPhoneIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 313
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneWindowIcon:Z

    if-eqz v1, :cond_10

    .line 314
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkPhoneWindowIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 316
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTabIcon:Z

    if-eqz v1, :cond_11

    .line 317
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkTabIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 319
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPcIcon:Z

    if-eqz v1, :cond_12

    .line 320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkPcIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 322
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkSpcIcon:Z

    if-eqz v1, :cond_13

    .line 323
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkSpcIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 325
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTvIcon:Z

    if-eqz v1, :cond_14

    .line 326
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkTvIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 328
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkUnknownIcon:Z

    if-eqz v1, :cond_15

    .line 329
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSLinkUnknownIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 332
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowOutOfFocusIcon:Z

    if-eqz v1, :cond_16

    .line 333
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsOutOfFocusIconPressed:Z

    if-eqz v1, :cond_26

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mOutOfFocusPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 336
    :cond_16
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSequenceShotIcon:Z

    if-eqz v1, :cond_17

    .line 337
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isSequenceIconPressed()Z

    move-result v1

    if-eqz v1, :cond_27

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSequenceShotPressIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    .line 340
    :cond_17
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPrivateIcon:Z

    if-eqz v1, :cond_1a

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPrivateIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    move-object/from16 v19, v0

    .line 342
    .local v19, "privateIconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    .line 343
    .local v18, "privateIconRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 344
    .local v20, "res":Landroid/content/res/Resources;
    const v1, 0x7f0d02de

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 345
    .local v11, "iconGap":I
    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v1

    add-int v17, v1, v11

    .line 347
    if-gtz v16, :cond_18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 348
    :cond_18
    add-int v1, v16, v8

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 353
    :goto_6
    add-int v1, v7, v9

    move/from16 v0, v21

    if-ge v0, v1, :cond_29

    .line 354
    add-int v1, v7, v9

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 359
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    if-eqz v1, :cond_19

    .line 360
    iget v1, v14, Landroid/graphics/Rect;->bottom:I

    iget v3, v14, Landroid/graphics/Rect;->top:I

    sub-int v15, v1, v3

    .line 361
    .local v15, "imageHeight":I
    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v1

    add-int/2addr v1, v9

    if-ge v15, v1, :cond_19

    .line 362
    div-int/lit8 v1, v15, 0x2

    add-int v1, v1, v21

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 366
    .end local v15    # "imageHeight":I
    :cond_19
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v3

    add-int/2addr v1, v3

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 367
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/Rect;->top:I

    invoke-interface/range {v19 .. v19}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v3

    add-int/2addr v1, v3

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 369
    move-object/from16 v0, v18

    iget v1, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, v19

    invoke-interface {v0, v2, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/Texture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 372
    .end local v11    # "iconGap":I
    .end local v18    # "privateIconRect":Landroid/graphics/Rect;
    .end local v19    # "privateIconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    .end local v20    # "res":Landroid/content/res/Resources;
    :cond_1a
    if-eqz v13, :cond_2c

    .line 373
    if-gtz v16, :cond_1b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 374
    :cond_1b
    add-int v1, v16, v8

    add-int v1, v1, v17

    iput v1, v12, Landroid/graphics/Rect;->left:I

    .line 379
    :goto_8
    add-int v1, v7, v9

    move/from16 v0, v21

    if-ge v0, v1, :cond_2b

    .line 380
    add-int v1, v7, v9

    iput v1, v12, Landroid/graphics/Rect;->top:I

    .line 385
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    if-eqz v1, :cond_1c

    .line 386
    iget v1, v14, Landroid/graphics/Rect;->bottom:I

    iget v3, v14, Landroid/graphics/Rect;->top:I

    sub-int v15, v1, v3

    .line 387
    .restart local v15    # "imageHeight":I
    invoke-interface {v13}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v1

    add-int/2addr v1, v9

    if-ge v15, v1, :cond_1c

    .line 388
    div-int/lit8 v1, v15, 0x2

    add-int v1, v1, v21

    invoke-interface {v13}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    iput v1, v12, Landroid/graphics/Rect;->top:I

    .line 392
    .end local v15    # "imageHeight":I
    :cond_1c
    iget v1, v12, Landroid/graphics/Rect;->left:I

    invoke-interface {v13}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v3

    add-int/2addr v1, v3

    add-int v1, v1, v17

    iput v1, v12, Landroid/graphics/Rect;->right:I

    .line 393
    iget v1, v12, Landroid/graphics/Rect;->top:I

    invoke-interface {v13}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, v12, Landroid/graphics/Rect;->bottom:I

    .line 394
    iget v1, v12, Landroid/graphics/Rect;->left:I

    iget v3, v12, Landroid/graphics/Rect;->top:I

    invoke-interface {v13, v2, v1, v3}, Lcom/sec/android/gallery3d/glrenderer/Texture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 396
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsIconFocused:Z

    if-eqz v1, :cond_1e

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    if-nez v1, :cond_1d

    .line 398
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    .line 399
    :cond_1d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    iget v3, v12, Landroid/graphics/Rect;->left:I

    iget v4, v12, Landroid/graphics/Rect;->top:I

    invoke-interface {v13}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v5

    invoke-interface {v13}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v6

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 410
    :cond_1e
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsIconFocused:Z

    if-eqz v1, :cond_20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    if-eqz v1, :cond_20

    .line 411
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    if-nez v1, :cond_1f

    .line 412
    new-instance v1, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    const v4, 0x7f02026e

    invoke-direct {v1, v3, v4}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    .line 413
    :cond_1f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusBox:Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/gallery3d/glrenderer/NinePatchTexture;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 416
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    if-eqz v1, :cond_0

    .line 417
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    const/4 v3, 0x0

    move/from16 v0, p3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 418
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    const/4 v3, 0x1

    move/from16 v0, p3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    goto/16 :goto_0

    .line 264
    :cond_21
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3DPanoramaIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto/16 :goto_1

    .line 272
    :cond_22
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto/16 :goto_2

    :cond_23
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIconPressed:Z

    if-eqz v1, :cond_24

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m2dDisplayPressedIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto/16 :goto_2

    :cond_24
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m2dDisplayIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto/16 :goto_2

    .line 278
    :cond_25
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mMagicShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto/16 :goto_3

    .line 333
    :cond_26
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mOutOfFocusIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto/16 :goto_4

    .line 337
    :cond_27
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mSequenceShotIcon:Lcom/sec/android/gallery3d/glrenderer/ResourceTexture;

    goto/16 :goto_5

    .line 350
    .restart local v11    # "iconGap":I
    .restart local v18    # "privateIconRect":Landroid/graphics/Rect;
    .restart local v19    # "privateIconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    .restart local v20    # "res":Landroid/content/res/Resources;
    :cond_28
    move-object/from16 v0, v18

    iput v8, v0, Landroid/graphics/Rect;->left:I

    goto/16 :goto_6

    .line 356
    :cond_29
    add-int v1, v21, v9

    move-object/from16 v0, v18

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto/16 :goto_7

    .line 376
    .end local v11    # "iconGap":I
    .end local v18    # "privateIconRect":Landroid/graphics/Rect;
    .end local v19    # "privateIconTexture":Lcom/sec/android/gallery3d/glrenderer/Texture;
    .end local v20    # "res":Landroid/content/res/Resources;
    :cond_2a
    add-int v1, v8, v17

    iput v1, v12, Landroid/graphics/Rect;->left:I

    goto/16 :goto_8

    .line 382
    :cond_2b
    add-int v1, v21, v9

    iput v1, v12, Landroid/graphics/Rect;->top:I

    goto/16 :goto_9

    .line 401
    :cond_2c
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    if-eqz v1, :cond_1e

    .line 402
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    if-nez v1, :cond_2d

    .line 403
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    .line 404
    :cond_2d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIconFocusRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mFocusedIndex:I

    aget-object v3, v3, v4

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mFocusedIndex:I

    aget-object v4, v4, v5

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mFocusedIndex:I

    aget-object v5, v5, v6

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$100(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mFocusedIndex:I

    move/from16 v22, v0

    aget-object v6, v6, v22

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mTexture:Lcom/sec/android/gallery3d/glrenderer/Texture;
    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$100(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Lcom/sec/android/gallery3d/glrenderer/Texture;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/gallery3d/glrenderer/Texture;->getHeight()I

    move-result v6

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_a
.end method

.method public set3DPanoramaIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 702
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIs3DPanoramaIconPressed:Z

    .line 703
    return-void
.end method

.method public set3dDisplayAvailable(Z)V
    .locals 0
    .param p1, "available"    # Z

    .prologue
    .line 718
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayAvailable:Z

    .line 719
    return-void
.end method

.method public set3dDisplayIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 710
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIconPressed:Z

    .line 711
    return-void
.end method

.method public setBurstShotPlayIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 678
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z

    .line 679
    return-void
.end method

.method public setBurstShotSettingIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 686
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z

    .line 687
    return-void
.end method

.method public setListener(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mListener:Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

    .line 182
    return-void
.end method

.method public setMagicShotIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 730
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsMagicShotIconPressed:Z

    .line 731
    return-void
.end method

.method public setOutOfFocusIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 778
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsOutOfFocusIconPressed:Z

    .line 779
    return-void
.end method

.method public setPanoramaIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 694
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsPanoramaIconPressed:Z

    .line 695
    return-void
.end method

.method public setSequenceIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 786
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsSequenceIconPressed:Z

    .line 787
    return-void
.end method

.method public setSoundSceneIconPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 659
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    .line 660
    return-void
.end method

.method public show3DTourIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 770
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DTourIcon:Z

    .line 771
    return-void
.end method

.method public showBestFaceIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 746
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestFaceIcon:Z

    .line 747
    return-void
.end method

.method public showBestPhotoIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 738
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestPhotoIcon:Z

    .line 739
    return-void
.end method

.method public showDramaShotIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 758
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowDramaShotIcon:Z

    .line 759
    return-void
.end method

.method public showEraserIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 754
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowEraserIcon:Z

    .line 755
    return-void
.end method

.method public showFocus(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 956
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showFocus(ZI)V

    .line 957
    return-void
.end method

.method public showFocus(ZI)V
    .locals 2
    .param p1, "show"    # Z
    .param p2, "index"    # I

    .prologue
    .line 960
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsIconFocused:Z

    if-ne p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mFocusedIndex:I

    if-eq v1, p2, :cond_2

    .line 961
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v1, :cond_1

    .line 962
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    .line 963
    .local v0, "accProv":Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;
    if-eqz p1, :cond_3

    .line 964
    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->setFocusedIndex(I)V

    .line 968
    .end local v0    # "accProv":Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;
    :cond_1
    :goto_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsIconFocused:Z

    .line 969
    iput p2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mFocusedIndex:I

    .line 970
    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mListener:Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;->notifyInvalidate()V

    .line 972
    :cond_2
    return-void

    .line 966
    .restart local v0    # "accProv":Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;
    :cond_3
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->setFocusedIndex(I)V

    goto :goto_0
.end method

.method public showIcon(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "currentPhoto"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const-wide/16 v6, 0x400

    const/4 v2, 0x0

    .line 463
    if-nez p1, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {p1, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_3
    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowVideoPlayIcon:Z

    .line 468
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    if-eqz v1, :cond_c

    .line 469
    const-wide/32 v4, 0x40000

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSequenceShotIcon:Z

    .line 474
    :goto_2
    const-wide/16 v4, 0x10

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSoundSceneIcon:Z

    .line 475
    const-wide/16 v4, 0x800

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowMagicShotIcon:Z

    .line 476
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraShotAndMore:Z

    if-eqz v1, :cond_4

    .line 477
    const-wide/16 v4, 0x1000

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestPhotoIcon:Z

    .line 478
    const-wide/16 v4, 0x2000

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBestFaceIcon:Z

    .line 479
    const-wide/16 v4, 0x4000

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowEraserIcon:Z

    .line 480
    const-wide/32 v4, 0x8000

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowDramaShotIcon:Z

    .line 481
    const-wide/32 v4, 0x10000

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPicMotionIcon:Z

    .line 484
    :cond_4
    invoke-virtual {p1, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DTourIcon:Z

    .line 485
    const-wide/32 v4, 0x20000

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowOutOfFocusIcon:Z

    .line 487
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->updateBurstShotIcon(Lcom/sec/android/gallery3d/data/MediaItem;)I

    .line 489
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePanoramaSlideshow:Z

    if-eqz v1, :cond_6

    .line 490
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v1, :cond_d

    invoke-static {p1}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->is3DPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 491
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    .line 497
    :cond_6
    :goto_3
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSLinkIcon:Z

    if-eqz v1, :cond_7

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v1, :cond_7

    .line 498
    invoke-virtual {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showSLinkIcon(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 501
    :cond_7
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v1, :cond_8

    .line 502
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPrivateIcon:Z

    .line 505
    :cond_8
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-eqz v1, :cond_0

    .line 506
    const/4 v0, 0x0

    .line 507
    .local v0, "show":Z
    invoke-static {p1}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->is3DPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 508
    :cond_9
    const/4 v0, 0x1

    .line 510
    :cond_a
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DPanoramaIcon:Z

    goto/16 :goto_0

    .end local v0    # "show":Z
    :cond_b
    move v1, v2

    .line 465
    goto/16 :goto_1

    .line 471
    :cond_c
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSequenceShotIcon:Z

    goto/16 :goto_2

    .line 493
    :cond_d
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    goto :goto_3
.end method

.method public showMagicShotIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 722
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowMagicShotIcon:Z

    .line 723
    return-void
.end method

.method public showPicMotionIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 762
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPicMotionIcon:Z

    .line 763
    return-void
.end method

.method public showPrivateIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 825
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPrivateIcon:Z

    .line 826
    return-void
.end method

.method public showSLinkIcon(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 586
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkBlurayIcon:Z

    .line 587
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkCameraIcon:Z

    .line 588
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneIcon:Z

    .line 589
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneWindowIcon:Z

    .line 590
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTabIcon:Z

    .line 591
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPcIcon:Z

    .line 592
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPcIcon:Z

    .line 593
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTvIcon:Z

    .line 594
    iput-boolean v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkUnknownIcon:Z

    .line 596
    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-nez v1, :cond_0

    .line 597
    sget-object v1, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "showSLinkIcon : item is not SLinkImage"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return-void

    .line 600
    .restart local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    check-cast p1, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .end local p1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getDeviceType()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    .line 601
    .local v0, "deviceType":Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_1

    .line 602
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkBlurayIcon:Z

    goto :goto_0

    .line 603
    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->CAMERA:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_2

    .line 604
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkCameraIcon:Z

    goto :goto_0

    .line 605
    :cond_2
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_3

    .line 606
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneIcon:Z

    goto :goto_0

    .line 607
    :cond_3
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE_WINDOWS:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_4

    .line 608
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPhoneWindowIcon:Z

    goto :goto_0

    .line 609
    :cond_4
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_5

    .line 610
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTabIcon:Z

    goto :goto_0

    .line 611
    :cond_5
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_6

    .line 612
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkPcIcon:Z

    goto :goto_0

    .line 613
    :cond_6
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_7

    .line 614
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkSpcIcon:Z

    goto :goto_0

    .line 615
    :cond_7
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v0, :cond_8

    .line 616
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkTvIcon:Z

    goto :goto_0

    .line 618
    :cond_8
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSLinkUnknownIcon:Z

    goto :goto_0
.end method

.method public showSequenceIcon(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 829
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    if-eqz v0, :cond_0

    .line 830
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSequenceShotIcon:Z

    .line 834
    :goto_0
    return-void

    .line 832
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSequenceShotIcon:Z

    goto :goto_0
.end method

.method public showSoundSceneIcon(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 651
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSoundSceneIcon:Z

    .line 652
    return-void
.end method

.method public touchAction(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 975
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1079
    :cond_0
    :goto_0
    return-void

    .line 977
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->speakIconGenericEvent(II)V

    goto :goto_0

    .line 981
    :sswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->checkActionDownRange(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 982
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSoundSceneIcon:Z

    if-eqz v0, :cond_1

    .line 983
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    .line 986
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayAvailable:Z

    if-eqz v0, :cond_2

    .line 987
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mListener:Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;->notifyInvalidate()V

    .line 988
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIconPressed:Z

    .line 991
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowPanoramaIcon:Z

    if-eqz v0, :cond_3

    .line 992
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsPanoramaIconPressed:Z

    .line 995
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShow3DPanoramaIcon:Z

    if-eqz v0, :cond_4

    .line 996
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIs3DPanoramaIconPressed:Z

    .line 999
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowOutOfFocusIcon:Z

    if-eqz v0, :cond_5

    .line 1000
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsOutOfFocusIconPressed:Z

    .line 1003
    :cond_5
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowSequenceShotIcon:Z

    if-eqz v0, :cond_6

    .line 1004
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsSequenceIconPressed:Z

    .line 1007
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowMagicShotIcon:Z

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMagicShotIconEnter:Z

    if-eqz v0, :cond_7

    .line 1008
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsMagicShotIconPressed:Z

    .line 1013
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mShowBurstShotPlayIcon:Z

    if-eqz v0, :cond_0

    .line 1014
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_9

    .line 1018
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z

    .line 1026
    :cond_8
    :goto_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mListener:Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;->notifyInvalidate()V

    goto/16 :goto_0

    .line 1019
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_8

    .line 1023
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->TAG:Ljava/lang/String;

    const-string v1, "Test : MotionEvent.ACTION_DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1024
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z

    goto :goto_1

    .line 1031
    :sswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->checkActionMoveRange(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1032
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    if-eqz v0, :cond_a

    .line 1033
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bSoundSceneIconPressed:Z

    .line 1036
    :cond_a
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIconPressed:Z

    if-eqz v0, :cond_b

    .line 1037
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->m3dDisplayIconPressed:Z

    .line 1040
    :cond_b
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsPanoramaIconPressed:Z

    if-eqz v0, :cond_c

    .line 1041
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsPanoramaIconPressed:Z

    .line 1044
    :cond_c
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIs3DPanoramaIconPressed:Z

    if-eqz v0, :cond_d

    .line 1045
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIs3DPanoramaIconPressed:Z

    .line 1048
    :cond_d
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsOutOfFocusIconPressed:Z

    if-eqz v0, :cond_e

    .line 1049
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsOutOfFocusIconPressed:Z

    .line 1052
    :cond_e
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsSequenceIconPressed:Z

    if-eqz v0, :cond_f

    .line 1053
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsSequenceIconPressed:Z

    .line 1056
    :cond_f
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsMagicShotIconPressed:Z

    if-eqz v0, :cond_10

    .line 1057
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mIsMagicShotIconPressed:Z

    .line 1061
    :cond_10
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_11

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_11

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_11

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v2

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_12

    .line 1066
    :cond_11
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotPlayIconPressed:Z

    .line 1069
    :cond_12
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mBurstShotIcon:[Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;

    aget-object v1, v1, v3

    # getter for: Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->mRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;->access$000(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$BurstShotIcon;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1074
    :cond_13
    sget-object v0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->TAG:Ljava/lang/String;

    const-string v1, "Test : MotionEvent.ACTION_MOVE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->bBurstShotSettingIconPressed:Z

    goto/16 :goto_0

    .line 975
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3e8 -> :sswitch_0
    .end sparse-switch
.end method

.method public updateBurstShotIcon(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 11
    .param p1, "currentPhoto"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v6, 0x1

    const/4 v10, 0x0

    const-wide/16 v8, 0x200

    .line 554
    const/4 v3, 0x1

    .line 556
    .local v3, "slideShowPlaySpeed":I
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBurstShotSettingIcon:Z

    if-eqz v4, :cond_0

    .line 557
    if-eqz p1, :cond_3

    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v4, :cond_3

    move-object v4, p1

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 559
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getBurstshotItems(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/util/ArrayList;

    move-result-object v1

    .line 561
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v6, :cond_2

    .line 562
    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showBurstShotIcon(Z)V

    .line 578
    .end local v1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isShowBurstShotIcon()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 579
    iget-object v4, p0, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getPlaySpeed(Landroid/content/Context;)I

    move-result v3

    .line 582
    :cond_1
    return v3

    .line 564
    .restart local v1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_2
    invoke-direct {p0, v10}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showBurstShotIcon(Z)V

    goto :goto_0

    .line 566
    .end local v1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_3
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    instance-of v4, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_5

    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/data/MediaItem;->hasPendingAttribute(J)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    move-object v4, p1

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalImage;->getFilePath()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    move-object v4, p1

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalImage;->getFilePath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/DCIM/Camera"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 569
    invoke-virtual {p1, v8, v9, v10}, Lcom/sec/android/gallery3d/data/MediaItem;->setPendingAttribute(JZ)V

    .line 570
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "group_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, "selection":Ljava/lang/String;
    move-object v4, p1

    .line 571
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->isBurstShotImage(Ljava/lang/String;)Z

    move-result v0

    .line 572
    .local v0, "isBurstShot":Z
    invoke-virtual {p1, v8, v9, v0}, Lcom/sec/android/gallery3d/data/MediaItem;->setAttribute(JZ)V

    .line 573
    invoke-direct {p0, v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showBurstShotIcon(Z)V

    goto :goto_0

    .line 575
    .end local v0    # "isBurstShot":Z
    .end local v2    # "selection":Ljava/lang/String;
    :cond_5
    invoke-direct {p0, v10}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showBurstShotIcon(Z)V

    goto :goto_0
.end method
